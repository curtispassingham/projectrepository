
DROP TYPE "RIB_XItemVATDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemVATDesc_REC";
/
DROP TYPE "RIB_XISCLocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCLocDesc_REC";
/
DROP TYPE "RIB_XItemUDADtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemUDADtl_REC";
/
DROP TYPE "RIB_XItemImage_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemImage_REC";
/
DROP TYPE "RIB_XItemSeason_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSeason_REC";
/
DROP TYPE "RIB_XIZPDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XIZPDesc_REC";
/
DROP TYPE "RIB_XISCDimDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCDimDesc_REC";
/
DROP TYPE "RIB_XItemBOMDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemBOMDesc_REC";
/
DROP TYPE "RIB_XItemSupCtyDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyDesc_REC";
/
DROP TYPE "RIB_XItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupDesc_REC";
/
DROP TYPE "RIB_XItemCtryDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCtryDesc_REC";
/
DROP TYPE "RIB_XItemCostDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCostDesc_REC";
/
DROP TYPE "RIB_LangOfXItemImage_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImage_REC";
/
DROP TYPE "RIB_LangOfXItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupDesc_REC";
/
DROP TYPE "RIB_LangOfXItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemDesc_REC";
/
DROP TYPE "RIB_XItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemDesc_REC";
/
DROP TYPE "RIB_XItemVATDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemVATDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  vat_type varchar2(1),
  vat_region number(6),
  vat_code varchar2(6),
  active_date date,
  reverse_vat_ind varchar2(1),
  LocOfXItemVATDesc "RIB_LocOfXItemVATDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
) return self as result
,constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
, reverse_vat_ind varchar2
) return self as result
,constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
, reverse_vat_ind varchar2
, LocOfXItemVATDesc "RIB_LocOfXItemVATDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemVATDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_type') := vat_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_region') := vat_region;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_code') := vat_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'active_date') := active_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'reverse_vat_ind') := reverse_vat_ind;
  l_new_pre :=i_prefix||'LocOfXItemVATDesc.';
  LocOfXItemVATDesc.appendNodeValues( i_prefix||'LocOfXItemVATDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.vat_type := vat_type;
self.vat_region := vat_region;
self.vat_code := vat_code;
self.active_date := active_date;
RETURN;
end;
constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
, reverse_vat_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.vat_type := vat_type;
self.vat_region := vat_region;
self.vat_code := vat_code;
self.active_date := active_date;
self.reverse_vat_ind := reverse_vat_ind;
RETURN;
end;
constructor function "RIB_XItemVATDesc_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
, reverse_vat_ind varchar2
, LocOfXItemVATDesc "RIB_LocOfXItemVATDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.vat_type := vat_type;
self.vat_region := vat_region;
self.vat_code := vat_code;
self.active_date := active_date;
self.reverse_vat_ind := reverse_vat_ind;
self.LocOfXItemVATDesc := LocOfXItemVATDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XISCLocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XISCLocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_id number(10),
  unit_cost number(20,4),
  negotiated_item_cost number(20,4),
  primary_loc_ind varchar2(1),
  pickup_lead_time number(4),
  round_lvl varchar2(6),
  round_to_case_pct number(12,4),
  round_to_layer_pct number(12,4),
  round_to_pallet_pct number(12,4),
  round_to_inner_pct number(12,4),
  supp_hier_lvl_1 varchar2(10),
  supp_hier_lvl_2 varchar2(10),
  supp_hier_lvl_3 varchar2(10),
  LocOfXISCLocDesc "RIB_LocOfXISCLocDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
) return self as result
,constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, LocOfXISCLocDesc "RIB_LocOfXISCLocDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XISCLocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_id') := hier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'negotiated_item_cost') := negotiated_item_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_loc_ind') := primary_loc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_lead_time') := pickup_lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_lvl') := round_lvl;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_case_pct') := round_to_case_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_layer_pct') := round_to_layer_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_pallet_pct') := round_to_pallet_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_inner_pct') := round_to_inner_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_1') := supp_hier_lvl_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_2') := supp_hier_lvl_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_3') := supp_hier_lvl_3;
  l_new_pre :=i_prefix||'LocOfXISCLocDesc.';
  LocOfXISCLocDesc.appendNodeValues( i_prefix||'LocOfXISCLocDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.round_to_inner_pct := round_to_inner_pct;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.round_to_inner_pct := round_to_inner_pct;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.round_to_inner_pct := round_to_inner_pct;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.round_to_inner_pct := round_to_inner_pct;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
RETURN;
end;
constructor function "RIB_XISCLocDesc_REC"
(
  rib_oid number
, hier_id number
, unit_cost number
, negotiated_item_cost number
, primary_loc_ind varchar2
, pickup_lead_time number
, round_lvl varchar2
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, round_to_inner_pct number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, LocOfXISCLocDesc "RIB_LocOfXISCLocDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.unit_cost := unit_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.primary_loc_ind := primary_loc_ind;
self.pickup_lead_time := pickup_lead_time;
self.round_lvl := round_lvl;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.round_to_inner_pct := round_to_inner_pct;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.LocOfXISCLocDesc := LocOfXISCLocDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemUDADtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemUDADtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uda_id number(5),
  display_type varchar2(2),
  uda_date date,
  uda_value varchar2(30),
  uda_text varchar2(250),
  create_datetime date,
  last_update_datetime date,
  last_update_id varchar2(30),
  LocOfXItemUDADtl "RIB_LocOfXItemUDADtl_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result
,constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemUDADtl "RIB_LocOfXItemUDADtl_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemUDADtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_id') := uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_type') := display_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_date') := uda_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value') := uda_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_text') := uda_text;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_datetime') := last_update_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_id') := last_update_id;
  l_new_pre :=i_prefix||'LocOfXItemUDADtl.';
  LocOfXItemUDADtl.appendNodeValues( i_prefix||'LocOfXItemUDADtl');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
self.uda_text := uda_text;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
self.uda_text := uda_text;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
self.uda_text := uda_text;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
self.uda_text := uda_text;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
RETURN;
end;
constructor function "RIB_XItemUDADtl_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_date date
, uda_value varchar2
, uda_text varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemUDADtl "RIB_LocOfXItemUDADtl_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_date := uda_date;
self.uda_value := uda_value;
self.uda_text := uda_text;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemUDADtl := LocOfXItemUDADtl;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemImage_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImage_TBL" AS TABLE OF "RIB_LangOfXItemImage_REC";
/
DROP TYPE "RIB_XItemImage_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemImage_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  image_name varchar2(120),
  image_addr varchar2(255),
  image_desc varchar2(40),
  create_datetime date,
  last_update_datetime date,
  last_update_id varchar2(30),
  LocOfXItemImage "RIB_LocOfXItemImage_REC",
  image_type varchar2(6),
  primary_ind varchar2(1),
  display_priority number(4),
  LangOfXItemImage_TBL "RIB_LangOfXItemImage_TBL",   -- Size of "RIB_LangOfXItemImage_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
, display_priority number
) return self as result
,constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
, display_priority number
, LangOfXItemImage_TBL "RIB_LangOfXItemImage_TBL"  -- Size of "RIB_LangOfXItemImage_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemImage_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'image_name') := image_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_addr') := image_addr;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_desc') := image_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_datetime') := last_update_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_id') := last_update_id;
  l_new_pre :=i_prefix||'LocOfXItemImage.';
  LocOfXItemImage.appendNodeValues( i_prefix||'LocOfXItemImage');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_type') := image_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_ind') := primary_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_priority') := display_priority;
  IF LangOfXItemImage_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemImage_TBL.';
    FOR INDX IN LangOfXItemImage_TBL.FIRST()..LangOfXItemImage_TBL.LAST() LOOP
      LangOfXItemImage_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemImage_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemImage := LocOfXItemImage;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemImage := LocOfXItemImage;
self.image_type := image_type;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemImage := LocOfXItemImage;
self.image_type := image_type;
self.primary_ind := primary_ind;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
, display_priority number
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemImage := LocOfXItemImage;
self.image_type := image_type;
self.primary_ind := primary_ind;
self.display_priority := display_priority;
RETURN;
end;
constructor function "RIB_XItemImage_REC"
(
  rib_oid number
, image_name varchar2
, image_addr varchar2
, image_desc varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, LocOfXItemImage "RIB_LocOfXItemImage_REC"
, image_type varchar2
, primary_ind varchar2
, display_priority number
, LangOfXItemImage_TBL "RIB_LangOfXItemImage_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.image_addr := image_addr;
self.image_desc := image_desc;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.LocOfXItemImage := LocOfXItemImage;
self.image_type := image_type;
self.primary_ind := primary_ind;
self.display_priority := display_priority;
self.LangOfXItemImage_TBL := LangOfXItemImage_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemSeason_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSeason_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  season_id number(3),
  phase_id number(3),
  item_season_seq_no number(4),
  diff_id varchar2(10),
  create_datetime date,
  last_update_datetime date,
  last_update_id varchar2(30),
  color number(4),
  LocOfXItemSeason "RIB_LocOfXItemSeason_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, color number
) return self as result
,constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, color number
, LocOfXItemSeason "RIB_LocOfXItemSeason_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSeason_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'season_id') := season_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'phase_id') := phase_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_season_seq_no') := item_season_seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_datetime') := last_update_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_id') := last_update_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'color') := color;
  l_new_pre :=i_prefix||'LocOfXItemSeason.';
  LocOfXItemSeason.appendNodeValues( i_prefix||'LocOfXItemSeason');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, color number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.color := color;
RETURN;
end;
constructor function "RIB_XItemSeason_REC"
(
  rib_oid number
, season_id number
, phase_id number
, item_season_seq_no number
, diff_id varchar2
, create_datetime date
, last_update_datetime date
, last_update_id varchar2
, color number
, LocOfXItemSeason "RIB_LocOfXItemSeason_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.item_season_seq_no := item_season_seq_no;
self.diff_id := diff_id;
self.create_datetime := create_datetime;
self.last_update_datetime := last_update_datetime;
self.last_update_id := last_update_id;
self.color := color;
self.LocOfXItemSeason := LocOfXItemSeason;
RETURN;
end;
END;
/
DROP TYPE "RIB_XIZPDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XIZPDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_id number(10),
  base_retail_ind varchar2(1),
  selling_unit_retail number(20,4),
  selling_uom varchar2(4),
  multi_selling_uom varchar2(4),
  country_id varchar2(3),
  currency_code varchar2(3),
  multi_units number(12,4),
  multi_unit_retail number(20,4),
  LocOfXIZPDesc "RIB_LocOfXIZPDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
, multi_unit_retail number
) return self as result
,constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
, multi_unit_retail number
, LocOfXIZPDesc "RIB_LocOfXIZPDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XIZPDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_id') := hier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'base_retail_ind') := base_retail_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_retail') := selling_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_selling_uom') := multi_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_retail') := multi_unit_retail;
  l_new_pre :=i_prefix||'LocOfXIZPDesc.';
  LocOfXIZPDesc.appendNodeValues( i_prefix||'LocOfXIZPDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
self.country_id := country_id;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
self.country_id := country_id;
self.currency_code := currency_code;
self.multi_units := multi_units;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
, multi_unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
self.country_id := country_id;
self.currency_code := currency_code;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
RETURN;
end;
constructor function "RIB_XIZPDesc_REC"
(
  rib_oid number
, hier_id number
, base_retail_ind varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_selling_uom varchar2
, country_id varchar2
, currency_code varchar2
, multi_units number
, multi_unit_retail number
, LocOfXIZPDesc "RIB_LocOfXIZPDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
self.base_retail_ind := base_retail_ind;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_selling_uom := multi_selling_uom;
self.country_id := country_id;
self.currency_code := currency_code;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
self.LocOfXIZPDesc := LocOfXIZPDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XISCDimDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XISCDimDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dim_object varchar2(6),
  tare_weight number(12,4),
  tare_type varchar2(6),
  lwh_uom varchar2(4),
  length number(12,4),
  width number(12,4),
  dim_height number(12,4),
  liquid_volume number(12,4),
  liquid_volume_uom varchar2(4),
  stat_cube varchar2(12),
  weight_uom varchar2(4),
  weight number(12,4),
  net_weight varchar2(12),
  presentation_method varchar2(6),
  LocOfXISCDimDesc "RIB_LocOfXISCDimDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
, presentation_method varchar2
) return self as result
,constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
, presentation_method varchar2
, LocOfXISCDimDesc "RIB_LocOfXISCDimDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XISCDimDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_object') := dim_object;
  rib_obj_util.g_RIB_element_values(i_prefix||'tare_weight') := tare_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'tare_type') := tare_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'lwh_uom') := lwh_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_height') := dim_height;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume') := liquid_volume;
  rib_obj_util.g_RIB_element_values(i_prefix||'liquid_volume_uom') := liquid_volume_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'stat_cube') := stat_cube;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_weight') := net_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'presentation_method') := presentation_method;
  l_new_pre :=i_prefix||'LocOfXISCDimDesc.';
  LocOfXISCDimDesc.appendNodeValues( i_prefix||'LocOfXISCDimDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.weight_uom := weight_uom;
self.weight := weight;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.weight_uom := weight_uom;
self.weight := weight;
self.net_weight := net_weight;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
, presentation_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.weight_uom := weight_uom;
self.weight := weight;
self.net_weight := net_weight;
self.presentation_method := presentation_method;
RETURN;
end;
constructor function "RIB_XISCDimDesc_REC"
(
  rib_oid number
, dim_object varchar2
, tare_weight number
, tare_type varchar2
, lwh_uom varchar2
, length number
, width number
, dim_height number
, liquid_volume number
, liquid_volume_uom varchar2
, stat_cube varchar2
, weight_uom varchar2
, weight number
, net_weight varchar2
, presentation_method varchar2
, LocOfXISCDimDesc "RIB_LocOfXISCDimDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
self.tare_weight := tare_weight;
self.tare_type := tare_type;
self.lwh_uom := lwh_uom;
self.length := length;
self.width := width;
self.dim_height := dim_height;
self.liquid_volume := liquid_volume;
self.liquid_volume_uom := liquid_volume_uom;
self.stat_cube := stat_cube;
self.weight_uom := weight_uom;
self.weight := weight;
self.net_weight := net_weight;
self.presentation_method := presentation_method;
self.LocOfXISCDimDesc := LocOfXISCDimDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemBOMDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemBOMDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  component_item varchar2(25),
  pack_qty number(12,4),
  LocOfXItemBOMDesc "RIB_LocOfXItemBOMDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemBOMDesc_REC"
(
  rib_oid number
, component_item varchar2
, pack_qty number
) return self as result
,constructor function "RIB_XItemBOMDesc_REC"
(
  rib_oid number
, component_item varchar2
, pack_qty number
, LocOfXItemBOMDesc "RIB_LocOfXItemBOMDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemBOMDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'component_item') := component_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_qty') := pack_qty;
  l_new_pre :=i_prefix||'LocOfXItemBOMDesc.';
  LocOfXItemBOMDesc.appendNodeValues( i_prefix||'LocOfXItemBOMDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemBOMDesc_REC"
(
  rib_oid number
, component_item varchar2
, pack_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.component_item := component_item;
self.pack_qty := pack_qty;
RETURN;
end;
constructor function "RIB_XItemBOMDesc_REC"
(
  rib_oid number
, component_item varchar2
, pack_qty number
, LocOfXItemBOMDesc "RIB_LocOfXItemBOMDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.component_item := component_item;
self.pack_qty := pack_qty;
self.LocOfXItemBOMDesc := LocOfXItemBOMDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCostDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCostDesc_TBL" AS TABLE OF "RIB_XItemCostDesc_REC";
/
DROP TYPE "RIB_XISCLocDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCLocDesc_TBL" AS TABLE OF "RIB_XISCLocDesc_REC";
/
DROP TYPE "RIB_XISCDimDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCDimDesc_TBL" AS TABLE OF "RIB_XISCDimDesc_REC";
/
DROP TYPE "RIB_XItemSupCtyDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  origin_country_id varchar2(3),
  primary_country_ind varchar2(1),
  unit_cost number(20,4),
  XItemCostDesc_TBL "RIB_XItemCostDesc_TBL",   -- Size of "RIB_XItemCostDesc_TBL" is unbounded
  XISCLocDesc_TBL "RIB_XISCLocDesc_TBL",   -- Size of "RIB_XISCLocDesc_TBL" is unbounded
  lead_time number(4),
  pickup_lead_time number(4),
  min_order_qty number(12,4),
  max_order_qty number(12,4),
  supp_hier_lvl_1 varchar2(10),
  supp_hier_lvl_2 varchar2(10),
  supp_hier_lvl_3 varchar2(10),
  default_uop varchar2(6),
  supp_pack_size number(12,4),
  inner_pack_size number(12,4),
  ti number(12,4),
  hi number(12,4),
  XISCDimDesc_TBL "RIB_XISCDimDesc_TBL",   -- Size of "RIB_XISCDimDesc_TBL" is unbounded
  cost_uom varchar2(4),
  tolerance_type varchar2(6),
  min_tolerance number(12,4),
  max_tolerance number(12,4),
  supp_hier_type_1 varchar2(6),
  supp_hier_type_2 varchar2(6),
  supp_hier_type_3 varchar2(6),
  round_lvl varchar2(6),
  round_to_inner_pct number(12,4),
  round_to_case_pct number(12,4),
  round_to_layer_pct number(12,4),
  round_to_pallet_pct number(12,4),
  packing_method varchar2(6),
  LocOfXItemSupCtyDesc "RIB_LocOfXItemSupCtyDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, packing_method varchar2
) return self as result
,constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"  -- Size of "RIB_XItemCostDesc_TBL" is unbounded
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"  -- Size of "RIB_XISCLocDesc_TBL" is unbounded
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"  -- Size of "RIB_XISCDimDesc_TBL" is unbounded
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, packing_method varchar2
, LocOfXItemSupCtyDesc "RIB_LocOfXItemSupCtyDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSupCtyDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_country_ind') := primary_country_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  IF XItemCostDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemCostDesc_TBL.';
    FOR INDX IN XItemCostDesc_TBL.FIRST()..XItemCostDesc_TBL.LAST() LOOP
      XItemCostDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemCostDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XISCLocDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XISCLocDesc_TBL.';
    FOR INDX IN XISCLocDesc_TBL.FIRST()..XISCLocDesc_TBL.LAST() LOOP
      XISCLocDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XISCLocDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'lead_time') := lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_lead_time') := pickup_lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'min_order_qty') := min_order_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_order_qty') := max_order_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_1') := supp_hier_lvl_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_2') := supp_hier_lvl_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_lvl_3') := supp_hier_lvl_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_uop') := default_uop;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_pack_size') := supp_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'inner_pack_size') := inner_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'ti') := ti;
  rib_obj_util.g_RIB_element_values(i_prefix||'hi') := hi;
  IF XISCDimDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XISCDimDesc_TBL.';
    FOR INDX IN XISCDimDesc_TBL.FIRST()..XISCDimDesc_TBL.LAST() LOOP
      XISCDimDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XISCDimDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_uom') := cost_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tolerance_type') := tolerance_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'min_tolerance') := min_tolerance;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_tolerance') := max_tolerance;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_type_1') := supp_hier_type_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_type_2') := supp_hier_type_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_hier_type_3') := supp_hier_type_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_lvl') := round_lvl;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_inner_pct') := round_to_inner_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_case_pct') := round_to_case_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_layer_pct') := round_to_layer_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_to_pallet_pct') := round_to_pallet_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'packing_method') := packing_method;
  l_new_pre :=i_prefix||'LocOfXItemSupCtyDesc.';
  LocOfXItemSupCtyDesc.appendNodeValues( i_prefix||'LocOfXItemSupCtyDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
self.round_to_case_pct := round_to_case_pct;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, packing_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.packing_method := packing_method;
RETURN;
end;
constructor function "RIB_XItemSupCtyDesc_REC"
(
  rib_oid number
, origin_country_id varchar2
, primary_country_ind varchar2
, unit_cost number
, XItemCostDesc_TBL "RIB_XItemCostDesc_TBL"
, XISCLocDesc_TBL "RIB_XISCLocDesc_TBL"
, lead_time number
, pickup_lead_time number
, min_order_qty number
, max_order_qty number
, supp_hier_lvl_1 varchar2
, supp_hier_lvl_2 varchar2
, supp_hier_lvl_3 varchar2
, default_uop varchar2
, supp_pack_size number
, inner_pack_size number
, ti number
, hi number
, XISCDimDesc_TBL "RIB_XISCDimDesc_TBL"
, cost_uom varchar2
, tolerance_type varchar2
, min_tolerance number
, max_tolerance number
, supp_hier_type_1 varchar2
, supp_hier_type_2 varchar2
, supp_hier_type_3 varchar2
, round_lvl varchar2
, round_to_inner_pct number
, round_to_case_pct number
, round_to_layer_pct number
, round_to_pallet_pct number
, packing_method varchar2
, LocOfXItemSupCtyDesc "RIB_LocOfXItemSupCtyDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.XItemCostDesc_TBL := XItemCostDesc_TBL;
self.XISCLocDesc_TBL := XISCLocDesc_TBL;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.supp_hier_lvl_1 := supp_hier_lvl_1;
self.supp_hier_lvl_2 := supp_hier_lvl_2;
self.supp_hier_lvl_3 := supp_hier_lvl_3;
self.default_uop := default_uop;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.ti := ti;
self.hi := hi;
self.XISCDimDesc_TBL := XISCDimDesc_TBL;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.min_tolerance := min_tolerance;
self.max_tolerance := max_tolerance;
self.supp_hier_type_1 := supp_hier_type_1;
self.supp_hier_type_2 := supp_hier_type_2;
self.supp_hier_type_3 := supp_hier_type_3;
self.round_lvl := round_lvl;
self.round_to_inner_pct := round_to_inner_pct;
self.round_to_case_pct := round_to_case_pct;
self.round_to_layer_pct := round_to_layer_pct;
self.round_to_pallet_pct := round_to_pallet_pct;
self.packing_method := packing_method;
self.LocOfXItemSupCtyDesc := LocOfXItemSupCtyDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemSupCtyDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyDesc_TBL" AS TABLE OF "RIB_XItemSupCtyDesc_REC";
/
DROP TYPE "RIB_XItmSupCtyMfrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItmSupCtyMfrDesc_TBL" AS TABLE OF "RIB_XItmSupCtyMfrDesc_REC";
/
DROP TYPE "RIB_LangOfXItemSupDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupDesc_TBL" AS TABLE OF "RIB_LangOfXItemSupDesc_REC";
/
DROP TYPE "RIB_XItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier varchar2(10),
  primary_supp_ind varchar2(3),
  vpn varchar2(30),
  supp_label varchar2(15),
  XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL",   -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
  consignment_rate number(12,4),
  supp_discontinue_date date,
  direct_ship_ind varchar2(1),
  pallet_name varchar2(6),
  case_name varchar2(6),
  inner_name varchar2(6),
  XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL",   -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
  primary_case_size varchar2(6),
  supp_diff_1 varchar2(120),
  supp_diff_2 varchar2(120),
  supp_diff_3 varchar2(120),
  supp_diff_4 varchar2(120),
  concession_rate number(12,4),
  LocOfXItemSupDesc "RIB_LocOfXItemSupDesc_REC",
  LangOfXItemSupDesc_TBL "RIB_LangOfXItemSupDesc_TBL",   -- Size of "RIB_LangOfXItemSupDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
, LocOfXItemSupDesc "RIB_LocOfXItemSupDesc_REC"
) return self as result
,constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"  -- Size of "RIB_XItemSupCtyDesc_TBL" is unbounded
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"  -- Size of "RIB_XItmSupCtyMfrDesc_TBL" is unbounded
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
, LocOfXItemSupDesc "RIB_LocOfXItemSupDesc_REC"
, LangOfXItemSupDesc_TBL "RIB_LangOfXItemSupDesc_TBL"  -- Size of "RIB_LangOfXItemSupDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supp_ind') := primary_supp_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'vpn') := vpn;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_label') := supp_label;
  IF XItemSupCtyDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSupCtyDesc_TBL.';
    FOR INDX IN XItemSupCtyDesc_TBL.FIRST()..XItemSupCtyDesc_TBL.LAST() LOOP
      XItemSupCtyDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSupCtyDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'consignment_rate') := consignment_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_discontinue_date') := supp_discontinue_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'direct_ship_ind') := direct_ship_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'pallet_name') := pallet_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_name') := case_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'inner_name') := inner_name;
  IF XItmSupCtyMfrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItmSupCtyMfrDesc_TBL.';
    FOR INDX IN XItmSupCtyMfrDesc_TBL.FIRST()..XItmSupCtyMfrDesc_TBL.LAST() LOOP
      XItmSupCtyMfrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItmSupCtyMfrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_case_size') := primary_case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_1') := supp_diff_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_2') := supp_diff_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_3') := supp_diff_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_4') := supp_diff_4;
  rib_obj_util.g_RIB_element_values(i_prefix||'concession_rate') := concession_rate;
  l_new_pre :=i_prefix||'LocOfXItemSupDesc.';
  LocOfXItemSupDesc.appendNodeValues( i_prefix||'LocOfXItemSupDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LangOfXItemSupDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemSupDesc_TBL.';
    FOR INDX IN LangOfXItemSupDesc_TBL.FIRST()..LangOfXItemSupDesc_TBL.LAST() LOOP
      LangOfXItemSupDesc_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemSupDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.concession_rate := concession_rate;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
, LocOfXItemSupDesc "RIB_LocOfXItemSupDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.concession_rate := concession_rate;
self.LocOfXItemSupDesc := LocOfXItemSupDesc;
RETURN;
end;
constructor function "RIB_XItemSupDesc_REC"
(
  rib_oid number
, supplier varchar2
, primary_supp_ind varchar2
, vpn varchar2
, supp_label varchar2
, XItemSupCtyDesc_TBL "RIB_XItemSupCtyDesc_TBL"
, consignment_rate number
, supp_discontinue_date date
, direct_ship_ind varchar2
, pallet_name varchar2
, case_name varchar2
, inner_name varchar2
, XItmSupCtyMfrDesc_TBL "RIB_XItmSupCtyMfrDesc_TBL"
, primary_case_size varchar2
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, concession_rate number
, LocOfXItemSupDesc "RIB_LocOfXItemSupDesc_REC"
, LangOfXItemSupDesc_TBL "RIB_LangOfXItemSupDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.primary_supp_ind := primary_supp_ind;
self.vpn := vpn;
self.supp_label := supp_label;
self.XItemSupCtyDesc_TBL := XItemSupCtyDesc_TBL;
self.consignment_rate := consignment_rate;
self.supp_discontinue_date := supp_discontinue_date;
self.direct_ship_ind := direct_ship_ind;
self.pallet_name := pallet_name;
self.case_name := case_name;
self.inner_name := inner_name;
self.XItmSupCtyMfrDesc_TBL := XItmSupCtyMfrDesc_TBL;
self.primary_case_size := primary_case_size;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.concession_rate := concession_rate;
self.LocOfXItemSupDesc := LocOfXItemSupDesc;
self.LangOfXItemSupDesc_TBL := LangOfXItemSupDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCtryDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemCtryDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  country_id varchar2(3),
  LocOfXItemCtryDesc "RIB_LocOfXItemCtryDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemCtryDesc_REC"
(
  rib_oid number
, country_id varchar2
) return self as result
,constructor function "RIB_XItemCtryDesc_REC"
(
  rib_oid number
, country_id varchar2
, LocOfXItemCtryDesc "RIB_LocOfXItemCtryDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemCtryDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  l_new_pre :=i_prefix||'LocOfXItemCtryDesc.';
  LocOfXItemCtryDesc.appendNodeValues( i_prefix||'LocOfXItemCtryDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemCtryDesc_REC"
(
  rib_oid number
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_XItemCtryDesc_REC"
(
  rib_oid number
, country_id varchar2
, LocOfXItemCtryDesc "RIB_LocOfXItemCtryDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
self.LocOfXItemCtryDesc := LocOfXItemCtryDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCostDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemCostDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_country_id varchar2(3),
  primary_dlvy_ctry_ind varchar2(1),
  nic_static_ind varchar2(1),
  base_cost number(20,4),
  negotiated_item_cost number(20,4),
  LocOfXItemCostDesc "RIB_LocOfXItemCostDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
) return self as result
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
) return self as result
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
) return self as result
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
) return self as result
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
, negotiated_item_cost number
) return self as result
,constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
, negotiated_item_cost number
, LocOfXItemCostDesc "RIB_LocOfXItemCostDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemCostDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_country_id') := delivery_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_dlvy_ctry_ind') := primary_dlvy_ctry_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'nic_static_ind') := nic_static_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'base_cost') := base_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'negotiated_item_cost') := negotiated_item_cost;
  l_new_pre :=i_prefix||'LocOfXItemCostDesc.';
  LocOfXItemCostDesc.appendNodeValues( i_prefix||'LocOfXItemCostDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
RETURN;
end;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
self.primary_dlvy_ctry_ind := primary_dlvy_ctry_ind;
RETURN;
end;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
self.primary_dlvy_ctry_ind := primary_dlvy_ctry_ind;
self.nic_static_ind := nic_static_ind;
RETURN;
end;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
self.primary_dlvy_ctry_ind := primary_dlvy_ctry_ind;
self.nic_static_ind := nic_static_ind;
self.base_cost := base_cost;
RETURN;
end;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
, negotiated_item_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
self.primary_dlvy_ctry_ind := primary_dlvy_ctry_ind;
self.nic_static_ind := nic_static_ind;
self.base_cost := base_cost;
self.negotiated_item_cost := negotiated_item_cost;
RETURN;
end;
constructor function "RIB_XItemCostDesc_REC"
(
  rib_oid number
, delivery_country_id varchar2
, primary_dlvy_ctry_ind varchar2
, nic_static_ind varchar2
, base_cost number
, negotiated_item_cost number
, LocOfXItemCostDesc "RIB_LocOfXItemCostDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
self.primary_dlvy_ctry_ind := primary_dlvy_ctry_ind;
self.nic_static_ind := nic_static_ind;
self.base_cost := base_cost;
self.negotiated_item_cost := negotiated_item_cost;
self.LocOfXItemCostDesc := LocOfXItemCostDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemImage_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImage_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  image_desc varchar2(40),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemImage_REC"
(
  rib_oid number
, lang number
, image_desc varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemImage_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_desc') := image_desc;
END appendNodeValues;
constructor function "RIB_LangOfXItemImage_REC"
(
  rib_oid number
, lang number
, image_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.image_desc := image_desc;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  supp_diff_1 varchar2(120),
  supp_diff_2 varchar2(120),
  supp_diff_3 varchar2(120),
  supp_diff_4 varchar2(120),
  supp_label varchar2(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
) return self as result
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
) return self as result
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result
,constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, supp_label varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_1') := supp_diff_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_2') := supp_diff_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_3') := supp_diff_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_diff_4') := supp_diff_4;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_label') := supp_label;
END appendNodeValues;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
RETURN;
end;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.supp_diff_1 := supp_diff_1;
RETURN;
end;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
RETURN;
end;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
RETURN;
end;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
RETURN;
end;
constructor function "RIB_LangOfXItemSupDesc_REC"
(
  rib_oid number
, lang number
, supp_diff_1 varchar2
, supp_diff_2 varchar2
, supp_diff_3 varchar2
, supp_diff_4 varchar2
, supp_label varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.supp_diff_1 := supp_diff_1;
self.supp_diff_2 := supp_diff_2;
self.supp_diff_3 := supp_diff_3;
self.supp_diff_4 := supp_diff_4;
self.supp_label := supp_label;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  short_desc varchar2(120),
  item_desc varchar2(250),
  item_desc_secondary varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemDesc_REC"
(
  rib_oid number
, lang number
, short_desc varchar2
, item_desc varchar2
) return self as result
,constructor function "RIB_LangOfXItemDesc_REC"
(
  rib_oid number
, lang number
, short_desc varchar2
, item_desc varchar2
, item_desc_secondary varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_desc') := short_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_desc') := item_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_desc_secondary') := item_desc_secondary;
END appendNodeValues;
constructor function "RIB_LangOfXItemDesc_REC"
(
  rib_oid number
, lang number
, short_desc varchar2
, item_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.short_desc := short_desc;
self.item_desc := item_desc;
RETURN;
end;
constructor function "RIB_LangOfXItemDesc_REC"
(
  rib_oid number
, lang number
, short_desc varchar2
, item_desc varchar2
, item_desc_secondary varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
self.short_desc := short_desc;
self.item_desc := item_desc;
self.item_desc_secondary := item_desc_secondary;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCtryDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCtryDesc_TBL" AS TABLE OF "RIB_XItemCtryDesc_REC";
/
DROP TYPE "RIB_XItemSupDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupDesc_TBL" AS TABLE OF "RIB_XItemSupDesc_REC";
/
DROP TYPE "RIB_XItemBOMDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemBOMDesc_TBL" AS TABLE OF "RIB_XItemBOMDesc_REC";
/
DROP TYPE "RIB_XItemVATDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemVATDesc_TBL" AS TABLE OF "RIB_XItemVATDesc_REC";
/
DROP TYPE "RIB_XIZPDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XIZPDesc_TBL" AS TABLE OF "RIB_XIZPDesc_REC";
/
DROP TYPE "RIB_XItemUDADtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemUDADtl_TBL" AS TABLE OF "RIB_XItemUDADtl_REC";
/
DROP TYPE "RIB_XItemSeason_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSeason_TBL" AS TABLE OF "RIB_XItemSeason_REC";
/
DROP TYPE "RIB_XItemImage_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemImage_TBL" AS TABLE OF "RIB_XItemImage_REC";
/
DROP TYPE "RIB_LangOfXItemDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemDesc_TBL" AS TABLE OF "RIB_LangOfXItemDesc_REC";
/
DROP TYPE "RIB_XItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  item_parent varchar2(25),
  item_grandparent varchar2(25),
  pack_ind varchar2(1),
  item_level number(1),
  tran_level number(1),
  diff_1 varchar2(10),
  diff_2 varchar2(10),
  diff_3 varchar2(10),
  diff_4 varchar2(10),
  dept number(4),
  class number(4),
  subclass number(4),
  item_desc varchar2(250),
  iscloc_hier_level varchar2(2),
  izp_hier_level varchar2(2),
  short_desc varchar2(120),
  cost_zone_group_id number(4),
  standard_uom varchar2(4),
  store_ord_mult varchar2(1),
  forecast_ind varchar2(1),
  simple_pack_ind varchar2(1),
  contains_inner_ind varchar2(1),
  sellable_ind varchar2(1),
  orderable_ind varchar2(1),
  pack_type varchar2(1),
  order_as_type varchar2(1),
  comments varchar2(2000),
  create_datetime date,
  XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL",   -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
  XItemSupDesc_TBL "RIB_XItemSupDesc_TBL",   -- Size of "RIB_XItemSupDesc_TBL" is unbounded
  XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL",   -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
  XItemVATDesc_TBL "RIB_XItemVATDesc_TBL",   -- Size of "RIB_XItemVATDesc_TBL" is unbounded
  XIZPDesc_TBL "RIB_XIZPDesc_TBL",   -- Size of "RIB_XIZPDesc_TBL" is unbounded
  XItemUDADtl_TBL "RIB_XItemUDADtl_TBL",   -- Size of "RIB_XItemUDADtl_TBL" is unbounded
  XItemSeason_TBL "RIB_XItemSeason_TBL",   -- Size of "RIB_XItemSeason_TBL" is unbounded
  XItemImage_TBL "RIB_XItemImage_TBL",   -- Size of "RIB_XItemImage_TBL" is unbounded
  status varchar2(1),
  uom_conv_factor number(20,10),
  package_size number(12,4),
  handling_temp varchar2(6),
  handling_sensitivity varchar2(6),
  mfg_rec_retail number(20,4),
  waste_type varchar2(6),
  waste_pct number(12,4),
  item_number_type varchar2(6),
  catch_weight_ind varchar2(1),
  const_dimen_ind varchar2(1),
  gift_wrap_ind varchar2(1),
  ship_alone_ind varchar2(1),
  ext_source_system varchar2(6),
  size_group1 number(4),
  size_group2 number(4),
  size1 varchar2(6),
  size2 varchar2(6),
  color varchar2(24),
  system_ind varchar2(1),
  upc_supplement number(5),
  upc_type varchar2(5),
  primary_upc_ind varchar2(1),
  primary_repl_ind varchar2(1),
  item_aggregate_ind varchar2(1),
  diff_1_aggregate_ind varchar2(1),
  diff_2_aggregate_ind varchar2(1),
  diff_3_aggregate_ind varchar2(1),
  diff_4_aggregate_ind varchar2(1),
  perishable_ind varchar2(1),
  notional_pack_ind varchar2(1),
  soh_inquiry_at_pack_ind varchar2(1),
  aip_case_type varchar2(6),
  order_type varchar2(6),
  sale_type varchar2(6),
  catch_weight_uom varchar2(4),
  deposit_item_type varchar2(6),
  inventory_ind varchar2(1),
  item_xform_ind varchar2(1),
  container_item varchar2(25),
  package_uom varchar2(4),
  format_id varchar2(1),
  prefix number(2),
  brand varchar2(120),
  product_classification varchar2(6),
  item_desc_secondary varchar2(250),
  desc_up varchar2(250),
  merchandise_ind varchar2(1),
  original_retail number(20,4),
  retail_label_type varchar2(6),
  retail_label_value number(20,4),
  default_waste_pct number(12,4),
  item_service_level varchar2(6),
  check_uda_ind varchar2(1),
  deposit_in_price_per_uom varchar2(6),
  attempt_rms_load varchar2(6),
  LocOfXItemDesc "RIB_LocOfXItemDesc_REC",
  LangOfXItemDesc_TBL "RIB_LangOfXItemDesc_TBL",   -- Size of "RIB_LangOfXItemDesc_TBL" is unbounded
  ExtOfXItemDesc_TBL "RIB_ExtOfXItemDesc_TBL",   -- Size of "RIB_ExtOfXItemDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
, LangOfXItemDesc_TBL "RIB_LangOfXItemDesc_TBL"  -- Size of "RIB_LangOfXItemDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"  -- Size of "RIB_XItemCtryDesc_TBL" is unbounded
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"  -- Size of "RIB_XItemSupDesc_TBL" is unbounded
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"  -- Size of "RIB_XItemBOMDesc_TBL" is unbounded
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"  -- Size of "RIB_XItemVATDesc_TBL" is unbounded
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"  -- Size of "RIB_XIZPDesc_TBL" is unbounded
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"  -- Size of "RIB_XItemUDADtl_TBL" is unbounded
, XItemSeason_TBL "RIB_XItemSeason_TBL"  -- Size of "RIB_XItemSeason_TBL" is unbounded
, XItemImage_TBL "RIB_XItemImage_TBL"  -- Size of "RIB_XItemImage_TBL" is unbounded
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
, LangOfXItemDesc_TBL "RIB_LangOfXItemDesc_TBL"  -- Size of "RIB_LangOfXItemDesc_TBL" is unbounded
, ExtOfXItemDesc_TBL "RIB_ExtOfXItemDesc_TBL"  -- Size of "RIB_ExtOfXItemDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemDesc') := "ns_name_XItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_parent') := item_parent;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_grandparent') := item_grandparent;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_ind') := pack_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_level') := item_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'tran_level') := tran_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_1') := diff_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_2') := diff_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_3') := diff_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_4') := diff_4;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'class') := class;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass') := subclass;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_desc') := item_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'iscloc_hier_level') := iscloc_hier_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'izp_hier_level') := izp_hier_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_desc') := short_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_zone_group_id') := cost_zone_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'standard_uom') := standard_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_ord_mult') := store_ord_mult;
  rib_obj_util.g_RIB_element_values(i_prefix||'forecast_ind') := forecast_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'simple_pack_ind') := simple_pack_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'contains_inner_ind') := contains_inner_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'sellable_ind') := sellable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'orderable_ind') := orderable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_type') := pack_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_as_type') := order_as_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_datetime') := create_datetime;
  IF XItemCtryDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemCtryDesc_TBL.';
    FOR INDX IN XItemCtryDesc_TBL.FIRST()..XItemCtryDesc_TBL.LAST() LOOP
      XItemCtryDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemCtryDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemSupDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSupDesc_TBL.';
    FOR INDX IN XItemSupDesc_TBL.FIRST()..XItemSupDesc_TBL.LAST() LOOP
      XItemSupDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSupDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemBOMDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemBOMDesc_TBL.';
    FOR INDX IN XItemBOMDesc_TBL.FIRST()..XItemBOMDesc_TBL.LAST() LOOP
      XItemBOMDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemBOMDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemVATDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemVATDesc_TBL.';
    FOR INDX IN XItemVATDesc_TBL.FIRST()..XItemVATDesc_TBL.LAST() LOOP
      XItemVATDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XItemVATDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XIZPDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XIZPDesc_TBL.';
    FOR INDX IN XIZPDesc_TBL.FIRST()..XIZPDesc_TBL.LAST() LOOP
      XIZPDesc_TBL(indx).appendNodeValues( i_prefix||indx||'XIZPDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemUDADtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemUDADtl_TBL.';
    FOR INDX IN XItemUDADtl_TBL.FIRST()..XItemUDADtl_TBL.LAST() LOOP
      XItemUDADtl_TBL(indx).appendNodeValues( i_prefix||indx||'XItemUDADtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemSeason_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSeason_TBL.';
    FOR INDX IN XItemSeason_TBL.FIRST()..XItemSeason_TBL.LAST() LOOP
      XItemSeason_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSeason_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemImage_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemImage_TBL.';
    FOR INDX IN XItemImage_TBL.FIRST()..XItemImage_TBL.LAST() LOOP
      XItemImage_TBL(indx).appendNodeValues( i_prefix||indx||'XItemImage_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom_conv_factor') := uom_conv_factor;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_size') := package_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'handling_temp') := handling_temp;
  rib_obj_util.g_RIB_element_values(i_prefix||'handling_sensitivity') := handling_sensitivity;
  rib_obj_util.g_RIB_element_values(i_prefix||'mfg_rec_retail') := mfg_rec_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'waste_type') := waste_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'waste_pct') := waste_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_number_type') := item_number_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'catch_weight_ind') := catch_weight_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'const_dimen_ind') := const_dimen_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_wrap_ind') := gift_wrap_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_alone_ind') := ship_alone_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_source_system') := ext_source_system;
  rib_obj_util.g_RIB_element_values(i_prefix||'size_group1') := size_group1;
  rib_obj_util.g_RIB_element_values(i_prefix||'size_group2') := size_group2;
  rib_obj_util.g_RIB_element_values(i_prefix||'size1') := size1;
  rib_obj_util.g_RIB_element_values(i_prefix||'size2') := size2;
  rib_obj_util.g_RIB_element_values(i_prefix||'color') := color;
  rib_obj_util.g_RIB_element_values(i_prefix||'system_ind') := system_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'upc_supplement') := upc_supplement;
  rib_obj_util.g_RIB_element_values(i_prefix||'upc_type') := upc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_upc_ind') := primary_upc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_repl_ind') := primary_repl_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_aggregate_ind') := item_aggregate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_1_aggregate_ind') := diff_1_aggregate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_2_aggregate_ind') := diff_2_aggregate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_3_aggregate_ind') := diff_3_aggregate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_4_aggregate_ind') := diff_4_aggregate_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'perishable_ind') := perishable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'notional_pack_ind') := notional_pack_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'soh_inquiry_at_pack_ind') := soh_inquiry_at_pack_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'aip_case_type') := aip_case_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'sale_type') := sale_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'catch_weight_uom') := catch_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'deposit_item_type') := deposit_item_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'inventory_ind') := inventory_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_xform_ind') := item_xform_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_item') := container_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_uom') := package_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_id') := format_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'prefix') := prefix;
  rib_obj_util.g_RIB_element_values(i_prefix||'brand') := brand;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_classification') := product_classification;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_desc_secondary') := item_desc_secondary;
  rib_obj_util.g_RIB_element_values(i_prefix||'desc_up') := desc_up;
  rib_obj_util.g_RIB_element_values(i_prefix||'merchandise_ind') := merchandise_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'original_retail') := original_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_label_type') := retail_label_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_label_value') := retail_label_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_waste_pct') := default_waste_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_service_level') := item_service_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'check_uda_ind') := check_uda_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'deposit_in_price_per_uom') := deposit_in_price_per_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'attempt_rms_load') := attempt_rms_load;
  l_new_pre :=i_prefix||'LocOfXItemDesc.';
  LocOfXItemDesc.appendNodeValues( i_prefix||'LocOfXItemDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LangOfXItemDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemDesc_TBL.';
    FOR INDX IN LangOfXItemDesc_TBL.FIRST()..LangOfXItemDesc_TBL.LAST() LOOP
      LangOfXItemDesc_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ExtOfXItemDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ExtOfXItemDesc_TBL.';
    FOR INDX IN ExtOfXItemDesc_TBL.FIRST()..ExtOfXItemDesc_TBL.LAST() LOOP
      ExtOfXItemDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ExtOfXItemDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
self.deposit_in_price_per_uom := deposit_in_price_per_uom;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
self.deposit_in_price_per_uom := deposit_in_price_per_uom;
self.attempt_rms_load := attempt_rms_load;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
self.deposit_in_price_per_uom := deposit_in_price_per_uom;
self.attempt_rms_load := attempt_rms_load;
self.LocOfXItemDesc := LocOfXItemDesc;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
, LangOfXItemDesc_TBL "RIB_LangOfXItemDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
self.deposit_in_price_per_uom := deposit_in_price_per_uom;
self.attempt_rms_load := attempt_rms_load;
self.LocOfXItemDesc := LocOfXItemDesc;
self.LangOfXItemDesc_TBL := LangOfXItemDesc_TBL;
RETURN;
end;
constructor function "RIB_XItemDesc_REC"
(
  rib_oid number
, item varchar2
, item_parent varchar2
, item_grandparent varchar2
, pack_ind varchar2
, item_level number
, tran_level number
, diff_1 varchar2
, diff_2 varchar2
, diff_3 varchar2
, diff_4 varchar2
, dept number
, class number
, subclass number
, item_desc varchar2
, iscloc_hier_level varchar2
, izp_hier_level varchar2
, short_desc varchar2
, cost_zone_group_id number
, standard_uom varchar2
, store_ord_mult varchar2
, forecast_ind varchar2
, simple_pack_ind varchar2
, contains_inner_ind varchar2
, sellable_ind varchar2
, orderable_ind varchar2
, pack_type varchar2
, order_as_type varchar2
, comments varchar2
, create_datetime date
, XItemCtryDesc_TBL "RIB_XItemCtryDesc_TBL"
, XItemSupDesc_TBL "RIB_XItemSupDesc_TBL"
, XItemBOMDesc_TBL "RIB_XItemBOMDesc_TBL"
, XItemVATDesc_TBL "RIB_XItemVATDesc_TBL"
, XIZPDesc_TBL "RIB_XIZPDesc_TBL"
, XItemUDADtl_TBL "RIB_XItemUDADtl_TBL"
, XItemSeason_TBL "RIB_XItemSeason_TBL"
, XItemImage_TBL "RIB_XItemImage_TBL"
, status varchar2
, uom_conv_factor number
, package_size number
, handling_temp varchar2
, handling_sensitivity varchar2
, mfg_rec_retail number
, waste_type varchar2
, waste_pct number
, item_number_type varchar2
, catch_weight_ind varchar2
, const_dimen_ind varchar2
, gift_wrap_ind varchar2
, ship_alone_ind varchar2
, ext_source_system varchar2
, size_group1 number
, size_group2 number
, size1 varchar2
, size2 varchar2
, color varchar2
, system_ind varchar2
, upc_supplement number
, upc_type varchar2
, primary_upc_ind varchar2
, primary_repl_ind varchar2
, item_aggregate_ind varchar2
, diff_1_aggregate_ind varchar2
, diff_2_aggregate_ind varchar2
, diff_3_aggregate_ind varchar2
, diff_4_aggregate_ind varchar2
, perishable_ind varchar2
, notional_pack_ind varchar2
, soh_inquiry_at_pack_ind varchar2
, aip_case_type varchar2
, order_type varchar2
, sale_type varchar2
, catch_weight_uom varchar2
, deposit_item_type varchar2
, inventory_ind varchar2
, item_xform_ind varchar2
, container_item varchar2
, package_uom varchar2
, format_id varchar2
, prefix number
, brand varchar2
, product_classification varchar2
, item_desc_secondary varchar2
, desc_up varchar2
, merchandise_ind varchar2
, original_retail number
, retail_label_type varchar2
, retail_label_value number
, default_waste_pct number
, item_service_level varchar2
, check_uda_ind varchar2
, deposit_in_price_per_uom varchar2
, attempt_rms_load varchar2
, LocOfXItemDesc "RIB_LocOfXItemDesc_REC"
, LangOfXItemDesc_TBL "RIB_LangOfXItemDesc_TBL"
, ExtOfXItemDesc_TBL "RIB_ExtOfXItemDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.item_parent := item_parent;
self.item_grandparent := item_grandparent;
self.pack_ind := pack_ind;
self.item_level := item_level;
self.tran_level := tran_level;
self.diff_1 := diff_1;
self.diff_2 := diff_2;
self.diff_3 := diff_3;
self.diff_4 := diff_4;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.item_desc := item_desc;
self.iscloc_hier_level := iscloc_hier_level;
self.izp_hier_level := izp_hier_level;
self.short_desc := short_desc;
self.cost_zone_group_id := cost_zone_group_id;
self.standard_uom := standard_uom;
self.store_ord_mult := store_ord_mult;
self.forecast_ind := forecast_ind;
self.simple_pack_ind := simple_pack_ind;
self.contains_inner_ind := contains_inner_ind;
self.sellable_ind := sellable_ind;
self.orderable_ind := orderable_ind;
self.pack_type := pack_type;
self.order_as_type := order_as_type;
self.comments := comments;
self.create_datetime := create_datetime;
self.XItemCtryDesc_TBL := XItemCtryDesc_TBL;
self.XItemSupDesc_TBL := XItemSupDesc_TBL;
self.XItemBOMDesc_TBL := XItemBOMDesc_TBL;
self.XItemVATDesc_TBL := XItemVATDesc_TBL;
self.XIZPDesc_TBL := XIZPDesc_TBL;
self.XItemUDADtl_TBL := XItemUDADtl_TBL;
self.XItemSeason_TBL := XItemSeason_TBL;
self.XItemImage_TBL := XItemImage_TBL;
self.status := status;
self.uom_conv_factor := uom_conv_factor;
self.package_size := package_size;
self.handling_temp := handling_temp;
self.handling_sensitivity := handling_sensitivity;
self.mfg_rec_retail := mfg_rec_retail;
self.waste_type := waste_type;
self.waste_pct := waste_pct;
self.item_number_type := item_number_type;
self.catch_weight_ind := catch_weight_ind;
self.const_dimen_ind := const_dimen_ind;
self.gift_wrap_ind := gift_wrap_ind;
self.ship_alone_ind := ship_alone_ind;
self.ext_source_system := ext_source_system;
self.size_group1 := size_group1;
self.size_group2 := size_group2;
self.size1 := size1;
self.size2 := size2;
self.color := color;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.upc_type := upc_type;
self.primary_upc_ind := primary_upc_ind;
self.primary_repl_ind := primary_repl_ind;
self.item_aggregate_ind := item_aggregate_ind;
self.diff_1_aggregate_ind := diff_1_aggregate_ind;
self.diff_2_aggregate_ind := diff_2_aggregate_ind;
self.diff_3_aggregate_ind := diff_3_aggregate_ind;
self.diff_4_aggregate_ind := diff_4_aggregate_ind;
self.perishable_ind := perishable_ind;
self.notional_pack_ind := notional_pack_ind;
self.soh_inquiry_at_pack_ind := soh_inquiry_at_pack_ind;
self.aip_case_type := aip_case_type;
self.order_type := order_type;
self.sale_type := sale_type;
self.catch_weight_uom := catch_weight_uom;
self.deposit_item_type := deposit_item_type;
self.inventory_ind := inventory_ind;
self.item_xform_ind := item_xform_ind;
self.container_item := container_item;
self.package_uom := package_uom;
self.format_id := format_id;
self.prefix := prefix;
self.brand := brand;
self.product_classification := product_classification;
self.item_desc_secondary := item_desc_secondary;
self.desc_up := desc_up;
self.merchandise_ind := merchandise_ind;
self.original_retail := original_retail;
self.retail_label_type := retail_label_type;
self.retail_label_value := retail_label_value;
self.default_waste_pct := default_waste_pct;
self.item_service_level := item_service_level;
self.check_uda_ind := check_uda_ind;
self.deposit_in_price_per_uom := deposit_in_price_per_uom;
self.attempt_rms_load := attempt_rms_load;
self.LocOfXItemDesc := LocOfXItemDesc;
self.LangOfXItemDesc_TBL := LangOfXItemDesc_TBL;
self.ExtOfXItemDesc_TBL := ExtOfXItemDesc_TBL;
RETURN;
end;
END;
/
