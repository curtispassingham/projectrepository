DROP TYPE "RIB_FopModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopModVo_REC";
/
DROP TYPE "RIB_FopItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopItmMod_REC";
/
DROP TYPE "RIB_FopSubMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopSubMod_REC";
/
DROP TYPE "RIB_FopItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FopItmMod_TBL" AS TABLE OF "RIB_FopItmMod_REC";
/
DROP TYPE "RIB_FopModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  pick_id number(12),
  FopItmMod_TBL "RIB_FopItmMod_TBL",   -- Size of "RIB_FopItmMod_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopModVo_REC"
(
  rib_oid number
, pick_id number
) return self as result
,constructor function "RIB_FopModVo_REC"
(
  rib_oid number
, pick_id number
, FopItmMod_TBL "RIB_FopItmMod_TBL"  -- Size of "RIB_FopItmMod_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopModVo') := "ns_name_FopModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_id') := pick_id;
  IF FopItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FopItmMod_TBL.';
    FOR INDX IN FopItmMod_TBL.FIRST()..FopItmMod_TBL.LAST() LOOP
      FopItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'FopItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FopModVo_REC"
(
  rib_oid number
, pick_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
RETURN;
end;
constructor function "RIB_FopModVo_REC"
(
  rib_oid number
, pick_id number
, FopItmMod_TBL "RIB_FopItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.FopItmMod_TBL := FopItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_FopSubMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FopSubMod_TBL" AS TABLE OF "RIB_FopSubMod_REC";
/
DROP TYPE "RIB_FopItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  quantity number(20,4),
  FopSubMod_TBL "RIB_FopSubMod_TBL",   -- Size of "RIB_FopSubMod_TBL" is 100
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopItmMod_REC"
(
  rib_oid number
, line_id number
, quantity number
) return self as result
,constructor function "RIB_FopItmMod_REC"
(
  rib_oid number
, line_id number
, quantity number
, FopSubMod_TBL "RIB_FopSubMod_TBL"  -- Size of "RIB_FopSubMod_TBL" is 100
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopModVo') := "ns_name_FopModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  IF FopSubMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FopSubMod_TBL.';
    FOR INDX IN FopSubMod_TBL.FIRST()..FopSubMod_TBL.LAST() LOOP
      FopSubMod_TBL(indx).appendNodeValues( i_prefix||indx||'FopSubMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FopItmMod_REC"
(
  rib_oid number
, line_id number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_FopItmMod_REC"
(
  rib_oid number
, line_id number
, quantity number
, FopSubMod_TBL "RIB_FopSubMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.quantity := quantity;
self.FopSubMod_TBL := FopSubMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_FopSubMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopSubMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  quantity number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopSubMod_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopSubMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopModVo') := "ns_name_FopModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
END appendNodeValues;
constructor function "RIB_FopSubMod_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
RETURN;
end;
END;
/
