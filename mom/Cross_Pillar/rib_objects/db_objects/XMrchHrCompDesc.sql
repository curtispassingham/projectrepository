DROP TYPE "RIB_XMrchHrCompDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XMrchHrCompDesc_REC";
/
DROP TYPE "RIB_XMrchHrCompDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XMrchHrCompDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XMrchHrCompDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  company number(4),
  company_name varchar2(120),
  add_1 varchar2(240),
  add_2 varchar2(240),
  add_3 varchar2(240),
  city varchar2(120),
  state varchar2(3),
  country_code varchar2(3),
  postal_code varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XMrchHrCompDesc_REC"
(
  rib_oid number
, company number
, company_name varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_code varchar2
) return self as result
,constructor function "RIB_XMrchHrCompDesc_REC"
(
  rib_oid number
, company number
, company_name varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, postal_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XMrchHrCompDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XMrchHrCompDesc') := "ns_name_XMrchHrCompDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'company') := company;
  rib_obj_util.g_RIB_element_values(i_prefix||'company_name') := company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_1') := add_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_2') := add_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_3') := add_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_code') := country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
END appendNodeValues;
constructor function "RIB_XMrchHrCompDesc_REC"
(
  rib_oid number
, company number
, company_name varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company := company;
self.company_name := company_name;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_code := country_code;
RETURN;
end;
constructor function "RIB_XMrchHrCompDesc_REC"
(
  rib_oid number
, company number
, company_name varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_code varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.company := company;
self.company_name := company_name;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_code := country_code;
self.postal_code := postal_code;
RETURN;
end;
END;
/
