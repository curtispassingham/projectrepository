@@ContactDesc.sql;
/
@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_ManifestDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestDesc_REC";
/
DROP TYPE "RIB_ManifestSrc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestSrc_REC";
/
DROP TYPE "RIB_ManifestDest_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestDest_REC";
/
DROP TYPE "RIB_ManifestBill_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestBill_REC";
/
DROP TYPE "RIB_ManifestItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestItm_REC";
/
DROP TYPE "RIB_ManifestItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestItm_TBL" AS TABLE OF "RIB_ManifestItm_REC";
/
DROP TYPE "RIB_ManifestDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  source_id number(10),
  transaction_type varchar2(30), -- transaction_type is enumeration field, valid values are [FULFILLMENT_ORDER_DELIVERY, RETURN, TRANSFER] (all lower-case)
  transaction_id number(12),
  customer_order_id varchar2(48),
  shipping_cost number(12,4),
  shipping_cost_currency varchar2(3),
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  package_height number(12,4),
  package_width number(12,4),
  package_length number(12,4),
  package_uom varchar2(4),
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  delivery_date date,
  printer_uri varchar2(128),
  label_quantity number(2),
  user_name varchar2(128),
  ManifestSrc "RIB_ManifestSrc_REC",
  ManifestDest "RIB_ManifestDest_REC",
  ManifestBill "RIB_ManifestBill_REC",
  ManifestItm_TBL "RIB_ManifestItm_TBL",   -- Size of "RIB_ManifestItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestDesc_REC"
(
  rib_oid number
, source_id number
, transaction_type varchar2
, transaction_id number
, customer_order_id varchar2
, shipping_cost number
, shipping_cost_currency varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, package_height number
, package_width number
, package_length number
, package_uom varchar2
, package_weight number
, package_weight_uom varchar2
, delivery_date date
, printer_uri varchar2
, label_quantity number
, user_name varchar2
, ManifestSrc "RIB_ManifestSrc_REC"
, ManifestDest "RIB_ManifestDest_REC"
, ManifestBill "RIB_ManifestBill_REC"
, ManifestItm_TBL "RIB_ManifestItm_TBL"  -- Size of "RIB_ManifestItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestDesc') := "ns_name_ManifestDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'source_id') := source_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_cost') := shipping_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_cost_currency') := shipping_cost_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_height') := package_height;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_width') := package_width;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_length') := package_length;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_uom') := package_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer_uri') := printer_uri;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_quantity') := label_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  l_new_pre :=i_prefix||'ManifestSrc.';
  ManifestSrc.appendNodeValues( i_prefix||'ManifestSrc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ManifestDest.';
  ManifestDest.appendNodeValues( i_prefix||'ManifestDest');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ManifestBill.';
  ManifestBill.appendNodeValues( i_prefix||'ManifestBill');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ManifestItm_TBL.';
  FOR INDX IN ManifestItm_TBL.FIRST()..ManifestItm_TBL.LAST() LOOP
    ManifestItm_TBL(indx).appendNodeValues( i_prefix||indx||'ManifestItm_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ManifestDesc_REC"
(
  rib_oid number
, source_id number
, transaction_type varchar2
, transaction_id number
, customer_order_id varchar2
, shipping_cost number
, shipping_cost_currency varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, package_height number
, package_width number
, package_length number
, package_uom varchar2
, package_weight number
, package_weight_uom varchar2
, delivery_date date
, printer_uri varchar2
, label_quantity number
, user_name varchar2
, ManifestSrc "RIB_ManifestSrc_REC"
, ManifestDest "RIB_ManifestDest_REC"
, ManifestBill "RIB_ManifestBill_REC"
, ManifestItm_TBL "RIB_ManifestItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.source_id := source_id;
self.transaction_type := transaction_type;
self.transaction_id := transaction_id;
self.customer_order_id := customer_order_id;
self.shipping_cost := shipping_cost;
self.shipping_cost_currency := shipping_cost_currency;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.package_height := package_height;
self.package_width := package_width;
self.package_length := package_length;
self.package_uom := package_uom;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.delivery_date := delivery_date;
self.printer_uri := printer_uri;
self.label_quantity := label_quantity;
self.user_name := user_name;
self.ManifestSrc := ManifestSrc;
self.ManifestDest := ManifestDest;
self.ManifestBill := ManifestBill;
self.ManifestItm_TBL := ManifestItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ManifestSrc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestSrc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  address_type varchar2(2),
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestSrc_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, address_type varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestSrc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestDesc') := "ns_name_ManifestDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_type') := address_type;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ManifestSrc_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, address_type varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.address_type := address_type;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ManifestDest_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestDest_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  address_type varchar2(2),
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestDest_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, address_type varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestDest_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestDesc') := "ns_name_ManifestDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_type') := address_type;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ManifestDest_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, address_type varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.address_type := address_type;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ManifestBill_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestBill_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ContactDesc "RIB_ContactDesc_REC",
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestBill_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestBill_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestDesc') := "ns_name_ManifestDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ManifestBill_REC"
(
  rib_oid number
, ContactDesc "RIB_ContactDesc_REC"
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ContactDesc := ContactDesc;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ManifestItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  item_description varchar2(400),
  item_quantity number(12,4),
  unit_of_measure varchar2(4),
  unit_cost_currency varchar2(3),
  unit_cost_amount number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
) return self as result
,constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
, unit_cost_currency varchar2
) return self as result
,constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
, unit_cost_currency varchar2
, unit_cost_amount number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestDesc') := "ns_name_ManifestDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_quantity') := item_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost_currency') := unit_cost_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost_amount') := unit_cost_amount;
END appendNodeValues;
constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_description := item_description;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
, unit_cost_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_description := item_description;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
self.unit_cost_currency := unit_cost_currency;
RETURN;
end;
constructor function "RIB_ManifestItm_REC"
(
  rib_oid number
, item_id varchar2
, item_description varchar2
, item_quantity number
, unit_of_measure varchar2
, unit_cost_currency varchar2
, unit_cost_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.item_description := item_description;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
self.unit_cost_currency := unit_cost_currency;
self.unit_cost_amount := unit_cost_amount;
RETURN;
end;
END;
/
