DROP TYPE "RIB_ManifestCloseVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestCloseVo_REC";
/
DROP TYPE "RIB_ManifestCloseVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestCloseVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestCloseVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  carrier_track_info_col "RIB_carrier_track_info_col_TBL",   -- Size of "RIB_carrier_track_info_col_TBL" is 1000
  ship_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
) return self as result
,constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result
,constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_track_info_col "RIB_carrier_track_info_col_TBL"  -- Size of "RIB_carrier_track_info_col_TBL" is 1000
) return self as result
,constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_track_info_col "RIB_carrier_track_info_col_TBL"  -- Size of "RIB_carrier_track_info_col_TBL" is 1000
, ship_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestCloseVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestCloseVo') := "ns_name_ManifestCloseVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  IF carrier_track_info_col IS NOT NULL THEN
    FOR INDX IN carrier_track_info_col.FIRST()..carrier_track_info_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'carrier_track_info_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'carrier_track_info_col'||'.'):=carrier_track_info_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_date') := ship_date;
END appendNodeValues;
constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
RETURN;
end;
constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_track_info_col "RIB_carrier_track_info_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_track_info_col := carrier_track_info_col;
RETURN;
end;
constructor function "RIB_ManifestCloseVo_REC"
(
  rib_oid number
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_track_info_col "RIB_carrier_track_info_col_TBL"
, ship_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_track_info_col := carrier_track_info_col;
self.ship_date := ship_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_carrier_track_info_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_carrier_track_info_col_TBL" AS TABLE OF varchar2(120);
/
