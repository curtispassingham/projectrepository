DROP TYPE "RIB_PhoneDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PhoneDesc_REC";
/
DROP TYPE "RIB_PhoneDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PhoneDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PhoneDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  phone_id number(22),
  phone_number varchar2(30),
  phone_type varchar2(6), -- phone_type is enumeration field, valid values are [HOME, WORK, MOBILE, FAX, PAGER, TELEX, OTHER] (all lower-case)
  phone_extension varchar2(30),
  primary_phone_ind varchar2(5), --primary_phone_ind is boolean field, valid values are true,false (all lower-case) 
  contact_preference_code varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
) return self as result
,constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
) return self as result
,constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
) return self as result
,constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
, primary_phone_ind varchar2  --primary_phone_ind is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
, primary_phone_ind varchar2  --primary_phone_ind is boolean field, valid values are true,false (all lower-case)
, contact_preference_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PhoneDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PhoneDesc') := "ns_name_PhoneDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_id') := phone_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_type') := phone_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_extension') := phone_extension;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_phone_ind') := primary_phone_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_preference_code') := contact_preference_code;
END appendNodeValues;
constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phone_id := phone_id;
self.phone_number := phone_number;
RETURN;
end;
constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phone_id := phone_id;
self.phone_number := phone_number;
self.phone_type := phone_type;
RETURN;
end;
constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phone_id := phone_id;
self.phone_number := phone_number;
self.phone_type := phone_type;
self.phone_extension := phone_extension;
RETURN;
end;
constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
, primary_phone_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phone_id := phone_id;
self.phone_number := phone_number;
self.phone_type := phone_type;
self.phone_extension := phone_extension;
self.primary_phone_ind := primary_phone_ind;
RETURN;
end;
constructor function "RIB_PhoneDesc_REC"
(
  rib_oid number
, phone_id number
, phone_number varchar2
, phone_type varchar2
, phone_extension varchar2
, primary_phone_ind varchar2
, contact_preference_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phone_id := phone_id;
self.phone_number := phone_number;
self.phone_type := phone_type;
self.phone_extension := phone_extension;
self.primary_phone_ind := primary_phone_ind;
self.contact_preference_code := contact_preference_code;
RETURN;
end;
END;
/
