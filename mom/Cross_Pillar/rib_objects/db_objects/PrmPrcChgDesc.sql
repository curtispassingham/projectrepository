DROP TYPE "RIB_PrmPrcChgThrDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtl_REC";
/
DROP TYPE "RIB_PrmPrcChgGetItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgGetItem_REC";
/
DROP TYPE "RIB_PrmPrcChgSmp_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmp_REC";
/
DROP TYPE "RIB_PrmPrcChgBuyItem_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyItem_REC";
/
DROP TYPE "RIB_PrmPrcChgBuyGet_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyGet_REC";
/
DROP TYPE "RIB_PrmPrcChgThr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThr_REC";
/
DROP TYPE "RIB_PrmPrcChgDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtl_REC";
/
DROP TYPE "RIB_PrmPrcChgDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDesc_REC";
/
DROP TYPE "RIB_PrmPrcChgThrDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  threshold_value number(20),
  prm_chg_value number(20),
  prm_chg_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgThrDtl_REC"
(
  rib_oid number
, threshold_value number
, prm_chg_value number
) return self as result
,constructor function "RIB_PrmPrcChgThrDtl_REC"
(
  rib_oid number
, threshold_value number
, prm_chg_value number
, prm_chg_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgThrDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'threshold_value') := threshold_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_value') := prm_chg_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_uom') := prm_chg_uom;
END appendNodeValues;
constructor function "RIB_PrmPrcChgThrDtl_REC"
(
  rib_oid number
, threshold_value number
, prm_chg_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.threshold_value := threshold_value;
self.prm_chg_value := prm_chg_value;
RETURN;
end;
constructor function "RIB_PrmPrcChgThrDtl_REC"
(
  rib_oid number
, threshold_value number
, prm_chg_value number
, prm_chg_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.threshold_value := threshold_value;
self.prm_chg_value := prm_chg_value;
self.prm_chg_uom := prm_chg_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgGetItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgGetItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgGetItem_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgGetItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_PrmPrcChgGetItem_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgSmp_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmp_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  promo_selling_retail number(20),
  promo_selling_uom varchar2(4000),
  prm_chg_type varchar2(1),
  prm_chg_value number(20),
  prm_chg_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgSmp_REC"
(
  rib_oid number
, item varchar2
, promo_selling_retail number
, promo_selling_uom varchar2
, prm_chg_type varchar2
, prm_chg_value number
) return self as result
,constructor function "RIB_PrmPrcChgSmp_REC"
(
  rib_oid number
, item varchar2
, promo_selling_retail number
, promo_selling_uom varchar2
, prm_chg_type varchar2
, prm_chg_value number
, prm_chg_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgSmp_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_selling_retail') := promo_selling_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_selling_uom') := promo_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_type') := prm_chg_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_value') := prm_chg_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_uom') := prm_chg_uom;
END appendNodeValues;
constructor function "RIB_PrmPrcChgSmp_REC"
(
  rib_oid number
, item varchar2
, promo_selling_retail number
, promo_selling_uom varchar2
, prm_chg_type varchar2
, prm_chg_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.promo_selling_retail := promo_selling_retail;
self.promo_selling_uom := promo_selling_uom;
self.prm_chg_type := prm_chg_type;
self.prm_chg_value := prm_chg_value;
RETURN;
end;
constructor function "RIB_PrmPrcChgSmp_REC"
(
  rib_oid number
, item varchar2
, promo_selling_retail number
, promo_selling_uom varchar2
, prm_chg_type varchar2
, prm_chg_value number
, prm_chg_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.promo_selling_retail := promo_selling_retail;
self.promo_selling_uom := promo_selling_uom;
self.prm_chg_type := prm_chg_type;
self.prm_chg_value := prm_chg_value;
self.prm_chg_uom := prm_chg_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgBuyItem_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyItem_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgBuyItem_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgBuyItem_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_PrmPrcChgBuyItem_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgGetItem_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgGetItem_TBL" AS TABLE OF "RIB_PrmPrcChgGetItem_REC";
/
DROP TYPE "RIB_PrmPrcChgBuyItem_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyItem_TBL" AS TABLE OF "RIB_PrmPrcChgBuyItem_REC";
/
DROP TYPE "RIB_PrmPrcChgBuyGet_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyGet_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  prm_chg_type varchar2(1),
  prm_chg_value number(20),
  prm_chg_uom varchar2(4),
  buy_qty number(20),
  all_ind number(1),
  PrmPrcChgGetItem_TBL "RIB_PrmPrcChgGetItem_TBL",   -- Size of "RIB_PrmPrcChgGetItem_TBL" is unbounded
  PrmPrcChgBuyItem_TBL "RIB_PrmPrcChgBuyItem_TBL",   -- Size of "RIB_PrmPrcChgBuyItem_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgBuyGet_REC"
(
  rib_oid number
, prm_chg_type varchar2
, prm_chg_value number
, prm_chg_uom varchar2
, buy_qty number
, all_ind number
, PrmPrcChgGetItem_TBL "RIB_PrmPrcChgGetItem_TBL"  -- Size of "RIB_PrmPrcChgGetItem_TBL" is unbounded
, PrmPrcChgBuyItem_TBL "RIB_PrmPrcChgBuyItem_TBL"  -- Size of "RIB_PrmPrcChgBuyItem_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgBuyGet_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_type') := prm_chg_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_value') := prm_chg_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_uom') := prm_chg_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'buy_qty') := buy_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'all_ind') := all_ind;
  l_new_pre :=i_prefix||'PrmPrcChgGetItem_TBL.';
  FOR INDX IN PrmPrcChgGetItem_TBL.FIRST()..PrmPrcChgGetItem_TBL.LAST() LOOP
    PrmPrcChgGetItem_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgGetItem_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'PrmPrcChgBuyItem_TBL.';
  FOR INDX IN PrmPrcChgBuyItem_TBL.FIRST()..PrmPrcChgBuyItem_TBL.LAST() LOOP
    PrmPrcChgBuyItem_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgBuyItem_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrmPrcChgBuyGet_REC"
(
  rib_oid number
, prm_chg_type varchar2
, prm_chg_value number
, prm_chg_uom varchar2
, buy_qty number
, all_ind number
, PrmPrcChgGetItem_TBL "RIB_PrmPrcChgGetItem_TBL"
, PrmPrcChgBuyItem_TBL "RIB_PrmPrcChgBuyItem_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.prm_chg_type := prm_chg_type;
self.prm_chg_value := prm_chg_value;
self.prm_chg_uom := prm_chg_uom;
self.buy_qty := buy_qty;
self.all_ind := all_ind;
self.PrmPrcChgGetItem_TBL := PrmPrcChgGetItem_TBL;
self.PrmPrcChgBuyItem_TBL := PrmPrcChgBuyItem_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgThrDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtl_TBL" AS TABLE OF "RIB_PrmPrcChgThrDtl_REC";
/
DROP TYPE "RIB_PrmPrcChgThr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  threshold_id number(6),
  threshold_name varchar2(30),
  qualification_type number(1),
  threshold_type varchar2(1),
  prm_chg_type varchar2(1),
  PrmPrcChgThrDtl_TBL "RIB_PrmPrcChgThrDtl_TBL",   -- Size of "RIB_PrmPrcChgThrDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgThr_REC"
(
  rib_oid number
, item varchar2
, threshold_id number
, threshold_name varchar2
, qualification_type number
, threshold_type varchar2
, prm_chg_type varchar2
, PrmPrcChgThrDtl_TBL "RIB_PrmPrcChgThrDtl_TBL"  -- Size of "RIB_PrmPrcChgThrDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgThr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'threshold_id') := threshold_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'threshold_name') := threshold_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'qualification_type') := qualification_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'threshold_type') := threshold_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'prm_chg_type') := prm_chg_type;
  l_new_pre :=i_prefix||'PrmPrcChgThrDtl_TBL.';
  FOR INDX IN PrmPrcChgThrDtl_TBL.FIRST()..PrmPrcChgThrDtl_TBL.LAST() LOOP
    PrmPrcChgThrDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgThrDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrmPrcChgThr_REC"
(
  rib_oid number
, item varchar2
, threshold_id number
, threshold_name varchar2
, qualification_type number
, threshold_type varchar2
, prm_chg_type varchar2
, PrmPrcChgThrDtl_TBL "RIB_PrmPrcChgThrDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.threshold_id := threshold_id;
self.threshold_name := threshold_name;
self.qualification_type := qualification_type;
self.threshold_type := threshold_type;
self.prm_chg_type := prm_chg_type;
self.PrmPrcChgThrDtl_TBL := PrmPrcChgThrDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgSmp_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmp_TBL" AS TABLE OF "RIB_PrmPrcChgSmp_REC";
/
DROP TYPE "RIB_PrmPrcChgThr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThr_TBL" AS TABLE OF "RIB_PrmPrcChgThr_REC";
/
DROP TYPE "RIB_PrmPrcChgBuyGet_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgBuyGet_TBL" AS TABLE OF "RIB_PrmPrcChgBuyGet_REC";
/
DROP TYPE "RIB_PrmPrcChgDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  promo_id number(10),
  promo_name varchar2(40),
  promo_description varchar2(160),
  promo_comp_id number(10),
  promo_comp_desc varchar2(40),
  promo_comp_detail_id number(10),
  apply_order number(1),
  start_date date,
  end_date date,
  PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL",   -- Size of "RIB_PrmPrcChgSmp_TBL" is unbounded
  PrmPrcChgThr_TBL "RIB_PrmPrcChgThr_TBL",   -- Size of "RIB_PrmPrcChgThr_TBL" is unbounded
  PrmPrcChgBuyGet_TBL "RIB_PrmPrcChgBuyGet_TBL",   -- Size of "RIB_PrmPrcChgBuyGet_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
) return self as result
,constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
) return self as result
,constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"  -- Size of "RIB_PrmPrcChgSmp_TBL" is unbounded
) return self as result
,constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"  -- Size of "RIB_PrmPrcChgSmp_TBL" is unbounded
, PrmPrcChgThr_TBL "RIB_PrmPrcChgThr_TBL"  -- Size of "RIB_PrmPrcChgThr_TBL" is unbounded
) return self as result
,constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"  -- Size of "RIB_PrmPrcChgSmp_TBL" is unbounded
, PrmPrcChgThr_TBL "RIB_PrmPrcChgThr_TBL"  -- Size of "RIB_PrmPrcChgThr_TBL" is unbounded
, PrmPrcChgBuyGet_TBL "RIB_PrmPrcChgBuyGet_TBL"  -- Size of "RIB_PrmPrcChgBuyGet_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_id') := promo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_name') := promo_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_description') := promo_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_id') := promo_comp_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_desc') := promo_comp_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_detail_id') := promo_comp_detail_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'apply_order') := apply_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  IF PrmPrcChgSmp_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrmPrcChgSmp_TBL.';
    FOR INDX IN PrmPrcChgSmp_TBL.FIRST()..PrmPrcChgSmp_TBL.LAST() LOOP
      PrmPrcChgSmp_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgSmp_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF PrmPrcChgThr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrmPrcChgThr_TBL.';
    FOR INDX IN PrmPrcChgThr_TBL.FIRST()..PrmPrcChgThr_TBL.LAST() LOOP
      PrmPrcChgThr_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgThr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF PrmPrcChgBuyGet_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrmPrcChgBuyGet_TBL.';
    FOR INDX IN PrmPrcChgBuyGet_TBL.FIRST()..PrmPrcChgBuyGet_TBL.LAST() LOOP
      PrmPrcChgBuyGet_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgBuyGet_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_detail_id := promo_comp_detail_id;
self.apply_order := apply_order;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_detail_id := promo_comp_detail_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_detail_id := promo_comp_detail_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.PrmPrcChgSmp_TBL := PrmPrcChgSmp_TBL;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"
, PrmPrcChgThr_TBL "RIB_PrmPrcChgThr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_detail_id := promo_comp_detail_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.PrmPrcChgSmp_TBL := PrmPrcChgSmp_TBL;
self.PrmPrcChgThr_TBL := PrmPrcChgThr_TBL;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtl_REC"
(
  rib_oid number
, promo_id number
, promo_name varchar2
, promo_description varchar2
, promo_comp_id number
, promo_comp_desc varchar2
, promo_comp_detail_id number
, apply_order number
, start_date date
, end_date date
, PrmPrcChgSmp_TBL "RIB_PrmPrcChgSmp_TBL"
, PrmPrcChgThr_TBL "RIB_PrmPrcChgThr_TBL"
, PrmPrcChgBuyGet_TBL "RIB_PrmPrcChgBuyGet_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_id := promo_id;
self.promo_name := promo_name;
self.promo_description := promo_description;
self.promo_comp_id := promo_comp_id;
self.promo_comp_desc := promo_comp_desc;
self.promo_comp_detail_id := promo_comp_detail_id;
self.apply_order := apply_order;
self.start_date := start_date;
self.end_date := end_date;
self.PrmPrcChgSmp_TBL := PrmPrcChgSmp_TBL;
self.PrmPrcChgThr_TBL := PrmPrcChgThr_TBL;
self.PrmPrcChgBuyGet_TBL := PrmPrcChgBuyGet_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtl_TBL" AS TABLE OF "RIB_PrmPrcChgDtl_REC";
/
DROP TYPE "RIB_PrmPrcChgDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1),
  PrmPrcChgDtl_TBL "RIB_PrmPrcChgDtl_TBL",   -- Size of "RIB_PrmPrcChgDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgDesc_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, PrmPrcChgDtl_TBL "RIB_PrmPrcChgDtl_TBL"  -- Size of "RIB_PrmPrcChgDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgDesc') := "ns_name_PrmPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  l_new_pre :=i_prefix||'PrmPrcChgDtl_TBL.';
  FOR INDX IN PrmPrcChgDtl_TBL.FIRST()..PrmPrcChgDtl_TBL.LAST() LOOP
    PrmPrcChgDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrmPrcChgDesc_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, PrmPrcChgDtl_TBL "RIB_PrmPrcChgDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.PrmPrcChgDtl_TBL := PrmPrcChgDtl_TBL;
RETURN;
end;
END;
/
