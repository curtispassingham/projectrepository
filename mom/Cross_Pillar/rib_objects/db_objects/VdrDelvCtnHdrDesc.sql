DROP TYPE "RIB_VdrDelvCtnHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnHdrDesc_REC";
/
DROP TYPE "RIB_VdrDelvCtnHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(12),
  store_id number(10),
  carton_id number(12),
  carton_external_id varchar2(128),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, RECEIVED, DAMAGED, MISSING, CANCELED, UNKNOWN] (all lower-case)
  number_of_line_items number(10),
  number_of_documents number(10),
  customer_order_related varchar2(20), -- customer_order_related is enumeration field, valid values are [YES, MIX, NO] (all lower-case)
  discrepant varchar2(5), --discrepant is boolean field, valid values are true,false (all lower-case) 
  uin_required varchar2(5), --uin_required is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, carton_external_id varchar2
, status varchar2
, number_of_line_items number
, number_of_documents number
, customer_order_related varchar2
, discrepant varchar2  --discrepant is boolean field, valid values are true,false (all lower-case)
, uin_required varchar2  --uin_required is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnHdrDesc') := "ns_name_VdrDelvCtnHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_external_id') := carton_external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_line_items') := number_of_line_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_documents') := number_of_documents;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_related') := customer_order_related;
  rib_obj_util.g_RIB_element_values(i_prefix||'discrepant') := discrepant;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_required') := uin_required;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, carton_id number
, carton_external_id varchar2
, status varchar2
, number_of_line_items number
, number_of_documents number
, customer_order_related varchar2
, discrepant varchar2
, uin_required varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.carton_id := carton_id;
self.carton_external_id := carton_external_id;
self.status := status;
self.number_of_line_items := number_of_line_items;
self.number_of_documents := number_of_documents;
self.customer_order_related := customer_order_related;
self.discrepant := discrepant;
self.uin_required := uin_required;
RETURN;
end;
END;
/
