DROP TYPE "RIB_VendorAddrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorAddrDesc_REC";
/
DROP TYPE "RIB_VendorAddrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VendorAddrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VendorAddrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  module varchar2(4),
  key_value_1 varchar2(20),
  key_value_2 varchar2(20),
  seq_no number(4),
  addr_type varchar2(2),
  primary_addr_ind varchar2(1),
  add_1 varchar2(240),
  add_2 varchar2(240),
  add_3 varchar2(240),
  city varchar2(120),
  city_id varchar2(10),
  state varchar2(3),
  country_id varchar2(3),
  jurisdiction_code varchar2(10),
  post varchar2(30),
  contact_name varchar2(120),
  contact_phone varchar2(20),
  contact_telex varchar2(20),
  contact_fax varchar2(20),
  contact_email varchar2(100),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
) return self as result
,constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VendorAddrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VendorAddrDesc') := "ns_name_VendorAddrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'module') := module;
  rib_obj_util.g_RIB_element_values(i_prefix||'key_value_1') := key_value_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'key_value_2') := key_value_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_type') := addr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_addr_ind') := primary_addr_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_1') := add_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_2') := add_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_3') := add_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'city_id') := city_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'post') := post;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_name') := contact_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_phone') := contact_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_telex') := contact_telex;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_fax') := contact_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_email') := contact_email;
END appendNodeValues;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
self.contact_name := contact_name;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
RETURN;
end;
constructor function "RIB_VendorAddrDesc_REC"
(
  rib_oid number
, module varchar2
, key_value_1 varchar2
, key_value_2 varchar2
, seq_no number
, addr_type varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, city_id varchar2
, state varchar2
, country_id varchar2
, jurisdiction_code varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.module := module;
self.key_value_1 := key_value_1;
self.key_value_2 := key_value_2;
self.seq_no := seq_no;
self.addr_type := addr_type;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.city_id := city_id;
self.state := state;
self.country_id := country_id;
self.jurisdiction_code := jurisdiction_code;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
RETURN;
end;
END;
/
