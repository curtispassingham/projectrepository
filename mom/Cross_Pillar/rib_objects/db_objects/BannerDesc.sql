DROP TYPE "RIB_BannerDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BannerDesc_REC";
/
DROP TYPE "RIB_BannerDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BannerDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BannerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  banner_id number(4),
  banner_name varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BannerDesc_REC"
(
  rib_oid number
, banner_id number
, banner_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BannerDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BannerDesc') := "ns_name_BannerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_id') := banner_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'banner_name') := banner_name;
END appendNodeValues;
constructor function "RIB_BannerDesc_REC"
(
  rib_oid number
, banner_id number
, banner_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.banner_id := banner_id;
self.banner_name := banner_name;
RETURN;
end;
END;
/
