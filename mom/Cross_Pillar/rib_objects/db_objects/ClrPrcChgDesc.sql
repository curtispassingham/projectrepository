DROP TYPE "RIB_ClrPrcChgDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtl_REC";
/
DROP TYPE "RIB_ClrPrcChgDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDesc_REC";
/
DROP TYPE "RIB_ClrPrcChgDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ClrPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  clearance_id number(15),
  item varchar2(25),
  effective_date date,
  selling_unit_retail number(20,4),
  selling_uom varchar2(4),
  selling_currency varchar2(3),
  reset_clearance_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ClrPrcChgDtl_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
, effective_date date
, selling_unit_retail number
, selling_uom varchar2
, selling_currency varchar2
) return self as result
,constructor function "RIB_ClrPrcChgDtl_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
, effective_date date
, selling_unit_retail number
, selling_uom varchar2
, selling_currency varchar2
, reset_clearance_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ClrPrcChgDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ClrPrcChgDesc') := "ns_name_ClrPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'clearance_id') := clearance_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_retail') := selling_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_currency') := selling_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'reset_clearance_id') := reset_clearance_id;
END appendNodeValues;
constructor function "RIB_ClrPrcChgDtl_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
, effective_date date
, selling_unit_retail number
, selling_uom varchar2
, selling_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.clearance_id := clearance_id;
self.item := item;
self.effective_date := effective_date;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.selling_currency := selling_currency;
RETURN;
end;
constructor function "RIB_ClrPrcChgDtl_REC"
(
  rib_oid number
, clearance_id number
, item varchar2
, effective_date date
, selling_unit_retail number
, selling_uom varchar2
, selling_currency varchar2
, reset_clearance_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.clearance_id := clearance_id;
self.item := item;
self.effective_date := effective_date;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.selling_currency := selling_currency;
self.reset_clearance_id := reset_clearance_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_ClrPrcChgDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDtl_TBL" AS TABLE OF "RIB_ClrPrcChgDtl_REC";
/
DROP TYPE "RIB_ClrPrcChgDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ClrPrcChgDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ClrPrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1),
  ClrPrcChgDtl_TBL "RIB_ClrPrcChgDtl_TBL",   -- Size of "RIB_ClrPrcChgDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ClrPrcChgDesc_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, ClrPrcChgDtl_TBL "RIB_ClrPrcChgDtl_TBL"  -- Size of "RIB_ClrPrcChgDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ClrPrcChgDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ClrPrcChgDesc') := "ns_name_ClrPrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  l_new_pre :=i_prefix||'ClrPrcChgDtl_TBL.';
  FOR INDX IN ClrPrcChgDtl_TBL.FIRST()..ClrPrcChgDtl_TBL.LAST() LOOP
    ClrPrcChgDtl_TBL(indx).appendNodeValues( i_prefix||indx||'ClrPrcChgDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ClrPrcChgDesc_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, ClrPrcChgDtl_TBL "RIB_ClrPrcChgDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.ClrPrcChgDtl_TBL := ClrPrcChgDtl_TBL;
RETURN;
end;
END;
/
