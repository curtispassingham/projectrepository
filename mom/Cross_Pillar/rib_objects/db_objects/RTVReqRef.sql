DROP TYPE "RIB_RTVReqDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqDtlRef_REC";
/
DROP TYPE "RIB_RTVReqRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqRef_REC";
/
DROP TYPE "RIB_RTVReqDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVReqDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVReqRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(8),
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVReqDtlRef_REC"
(
  rib_oid number
, seq_no number
) return self as result
,constructor function "RIB_RTVReqDtlRef_REC"
(
  rib_oid number
, seq_no number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVReqDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVReqRef') := "ns_name_RTVReqRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_RTVReqDtlRef_REC"
(
  rib_oid number
, seq_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
RETURN;
end;
constructor function "RIB_RTVReqDtlRef_REC"
(
  rib_oid number
, seq_no number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_RTVReqDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqDtlRef_TBL" AS TABLE OF "RIB_RTVReqDtlRef_REC";
/
DROP TYPE "RIB_RTVReqRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVReqRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVReqRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  rtv_order_no number(10),
  RTVReqDtlRef_TBL "RIB_RTVReqDtlRef_TBL",   -- Size of "RIB_RTVReqDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVReqRef_REC"
(
  rib_oid number
, rtv_order_no number
) return self as result
,constructor function "RIB_RTVReqRef_REC"
(
  rib_oid number
, rtv_order_no number
, RTVReqDtlRef_TBL "RIB_RTVReqDtlRef_TBL"  -- Size of "RIB_RTVReqDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVReqRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVReqRef') := "ns_name_RTVReqRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'rtv_order_no') := rtv_order_no;
  IF RTVReqDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RTVReqDtlRef_TBL.';
    FOR INDX IN RTVReqDtlRef_TBL.FIRST()..RTVReqDtlRef_TBL.LAST() LOOP
      RTVReqDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'RTVReqDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_RTVReqRef_REC"
(
  rib_oid number
, rtv_order_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
RETURN;
end;
constructor function "RIB_RTVReqRef_REC"
(
  rib_oid number
, rtv_order_no number
, RTVReqDtlRef_TBL "RIB_RTVReqDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.RTVReqDtlRef_TBL := RTVReqDtlRef_TBL;
RETURN;
end;
END;
/
