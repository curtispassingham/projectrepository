DROP TYPE "RIB_VdrDelvHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvHdrDesc_REC";
/
DROP TYPE "RIB_VdrDelvHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(12),
  store_id number(10),
  supplier_id number(10),
  supplier_name varchar2(240),
  asn varchar2(128),
  origin_type varchar2(20), -- origin_type is enumeration field, valid values are [ASN, DEXNEX, MANUAL, PO, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, RECEIVED, CANCELED, REJECTED, UNKNOWN] (all lower-case)
  expected_arrival_date date,
  total_cartons number(9),
  line_item_received_total number(9),
  missing_cartons varchar2(5), --missing_cartons is boolean field, valid values are true,false (all lower-case) 
  customer_order_related varchar2(5), --customer_order_related is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, asn varchar2
, origin_type varchar2
, status varchar2
, expected_arrival_date date
, total_cartons number
, line_item_received_total number
, missing_cartons varchar2  --missing_cartons is boolean field, valid values are true,false (all lower-case)
, customer_order_related varchar2  --customer_order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvHdrDesc') := "ns_name_VdrDelvHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_name') := supplier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'asn') := asn;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_type') := origin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'expected_arrival_date') := expected_arrival_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cartons') := total_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_received_total') := line_item_received_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'missing_cartons') := missing_cartons;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_related') := customer_order_related;
END appendNodeValues;
constructor function "RIB_VdrDelvHdrDesc_REC"
(
  rib_oid number
, delivery_id number
, store_id number
, supplier_id number
, supplier_name varchar2
, asn varchar2
, origin_type varchar2
, status varchar2
, expected_arrival_date date
, total_cartons number
, line_item_received_total number
, missing_cartons varchar2
, customer_order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.supplier_name := supplier_name;
self.asn := asn;
self.origin_type := origin_type;
self.status := status;
self.expected_arrival_date := expected_arrival_date;
self.total_cartons := total_cartons;
self.line_item_received_total := line_item_received_total;
self.missing_cartons := missing_cartons;
self.customer_order_related := customer_order_related;
RETURN;
end;
END;
/
