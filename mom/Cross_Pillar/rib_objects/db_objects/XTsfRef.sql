DROP TYPE "RIB_XTsfDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfDtlRef_REC";
/
DROP TYPE "RIB_XTsfRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfRef_REC";
/
DROP TYPE "RIB_XTsfDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XTsfDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XTsfRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XTsfDtlRef_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XTsfDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XTsfRef') := "ns_name_XTsfRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_XTsfDtlRef_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_XTsfDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XTsfDtlRef_TBL" AS TABLE OF "RIB_XTsfDtlRef_REC";
/
DROP TYPE "RIB_XTsfRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XTsfRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XTsfRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_no number(12),
  XTsfDtlRef_TBL "RIB_XTsfDtlRef_TBL",   -- Size of "RIB_XTsfDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XTsfRef_REC"
(
  rib_oid number
, tsf_no number
) return self as result
,constructor function "RIB_XTsfRef_REC"
(
  rib_oid number
, tsf_no number
, XTsfDtlRef_TBL "RIB_XTsfDtlRef_TBL"  -- Size of "RIB_XTsfDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XTsfRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XTsfRef') := "ns_name_XTsfRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_no') := tsf_no;
  IF XTsfDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XTsfDtlRef_TBL.';
    FOR INDX IN XTsfDtlRef_TBL.FIRST()..XTsfDtlRef_TBL.LAST() LOOP
      XTsfDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'XTsfDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XTsfRef_REC"
(
  rib_oid number
, tsf_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
RETURN;
end;
constructor function "RIB_XTsfRef_REC"
(
  rib_oid number
, tsf_no number
, XTsfDtlRef_TBL "RIB_XTsfDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.XTsfDtlRef_TBL := XTsfDtlRef_TBL;
RETURN;
end;
END;
/
