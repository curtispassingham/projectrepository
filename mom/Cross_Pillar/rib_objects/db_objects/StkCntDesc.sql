DROP TYPE "RIB_StkCntDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntDesc_REC";
/
DROP TYPE "RIB_CountChild_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CountChild_REC";
/
DROP TYPE "RIB_CountChild_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CountChild_TBL" AS TABLE OF "RIB_CountChild_REC";
/
DROP TYPE "RIB_StkCntDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCntDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  stock_count_id number(12),
  store_id number(10),
  product_group_id number(10),
  schedule_id number(10),
  count_type varchar2(20), -- count_type is enumeration field, valid values are [UNIT, UNIT_AMOUNT, PROBLEM_LINE, ADHOC, RMS_SYNC, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, COUNT_SCHEDULED, COUNT_IN_PROGRESS, COUNT_COMPLETE, RECOUNT_SCHEDULED, RECOUNT_IN_PROGRESS, RECOUNT_COMPLETE, APPROVAL_SCHEDULED, APPROVAL_IN_PROGRESS, APPROVAL_PROCESSING, APPROVAL_AUTHORIZED, APPROVAL_COMPLETE, CANCELED, UNKNOWN] (all lower-case)
  display_status varchar2(20), -- display_status is enumeration field, valid values are [NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY] (all lower-case)
  counting_method varchar2(20), -- counting_method is enumeration field, valid values are [GUIDED, UNGUIDED, THIRD_PARTY, AUTO, UNKNOWN] (all lower-case)
  stock_count_time_frame varchar2(20), -- stock_count_time_frame is enumeration field, valid values are [BEFORE_STORE_OPEN, AFTER_STORE_CLOSE, NONE] (all lower-case)
  group_type varchar2(20), -- group_type is enumeration field, valid values are [DEPARTMENT, CLASS, SUBCLASS, LOCATION, NONE] (all lower-case)
  count_desc varchar2(255),
  group_desc varchar2(128),
  schedule_date date,
  count_start_time number(18),
  last_updated_time date,
  export_user varchar2(40),
  variance_count number(12),
  variance_percent number(12),
  variance_value number(12),
  recount_active varchar2(5), --recount_active is boolean field, valid values are true,false (all lower-case) 
  auto_authorize varchar2(5), --auto_authorize is boolean field, valid values are true,false (all lower-case) 
  pbl_pick_less_suggested varchar2(5), --pbl_pick_less_suggested is boolean field, valid values are true,false (all lower-case) 
  pbl_repl_less_suggested varchar2(5), --pbl_repl_less_suggested is boolean field, valid values are true,false (all lower-case) 
  pbl_repl_neg_available varchar2(5), --pbl_repl_neg_available is boolean field, valid values are true,false (all lower-case) 
  pbl_repl_discrepant_uin varchar2(5), --pbl_repl_discrepant_uin is boolean field, valid values are true,false (all lower-case) 
  include_active_items varchar2(5), --include_active_items is boolean field, valid values are true,false (all lower-case) 
  include_inactive_items varchar2(5), --include_inactive_items is boolean field, valid values are true,false (all lower-case) 
  include_discontinued_items varchar2(5), --include_discontinued_items is boolean field, valid values are true,false (all lower-case) 
  include_deleted_items varchar2(5), --include_deleted_items is boolean field, valid values are true,false (all lower-case) 
  include_soh_zero varchar2(5), --include_soh_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_less_than_zero varchar2(5), --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_greater_than_zero varchar2(5), --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case) 
  total_on_count number(12),
  left_to_count number(12),
  all_items varchar2(5), --all_items is boolean field, valid values are true,false (all lower-case) 
  CountChild_TBL "RIB_CountChild_TBL",   -- Size of "RIB_CountChild_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCntDesc_REC"
(
  rib_oid number
, stock_count_id number
, store_id number
, product_group_id number
, schedule_id number
, count_type varchar2
, status varchar2
, display_status varchar2
, counting_method varchar2
, stock_count_time_frame varchar2
, group_type varchar2
, count_desc varchar2
, group_desc varchar2
, schedule_date date
, count_start_time number
, last_updated_time date
, export_user varchar2
, variance_count number
, variance_percent number
, variance_value number
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pbl_pick_less_suggested varchar2  --pbl_pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, pbl_repl_less_suggested varchar2  --pbl_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, pbl_repl_neg_available varchar2  --pbl_repl_neg_available is boolean field, valid values are true,false (all lower-case)
, pbl_repl_discrepant_uin varchar2  --pbl_repl_discrepant_uin is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, total_on_count number
, left_to_count number
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_StkCntDesc_REC"
(
  rib_oid number
, stock_count_id number
, store_id number
, product_group_id number
, schedule_id number
, count_type varchar2
, status varchar2
, display_status varchar2
, counting_method varchar2
, stock_count_time_frame varchar2
, group_type varchar2
, count_desc varchar2
, group_desc varchar2
, schedule_date date
, count_start_time number
, last_updated_time date
, export_user varchar2
, variance_count number
, variance_percent number
, variance_value number
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pbl_pick_less_suggested varchar2  --pbl_pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, pbl_repl_less_suggested varchar2  --pbl_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, pbl_repl_neg_available varchar2  --pbl_repl_neg_available is boolean field, valid values are true,false (all lower-case)
, pbl_repl_discrepant_uin varchar2  --pbl_repl_discrepant_uin is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, total_on_count number
, left_to_count number
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, CountChild_TBL "RIB_CountChild_TBL"  -- Size of "RIB_CountChild_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCntDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntDesc') := "ns_name_StkCntDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_count_id') := stock_count_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_id') := schedule_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_type') := count_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_status') := display_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'counting_method') := counting_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_count_time_frame') := stock_count_time_frame;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_type') := group_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_desc') := count_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_desc') := group_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_date') := schedule_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_start_time') := count_start_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_updated_time') := last_updated_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'export_user') := export_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_count') := variance_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_percent') := variance_percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_value') := variance_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'recount_active') := recount_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_authorize') := auto_authorize;
  rib_obj_util.g_RIB_element_values(i_prefix||'pbl_pick_less_suggested') := pbl_pick_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'pbl_repl_less_suggested') := pbl_repl_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'pbl_repl_neg_available') := pbl_repl_neg_available;
  rib_obj_util.g_RIB_element_values(i_prefix||'pbl_repl_discrepant_uin') := pbl_repl_discrepant_uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_active_items') := include_active_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_inactive_items') := include_inactive_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_discontinued_items') := include_discontinued_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_deleted_items') := include_deleted_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_zero') := include_soh_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_less_than_zero') := include_soh_less_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_greater_than_zero') := include_soh_greater_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_on_count') := total_on_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'left_to_count') := left_to_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'all_items') := all_items;
  IF CountChild_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CountChild_TBL.';
    FOR INDX IN CountChild_TBL.FIRST()..CountChild_TBL.LAST() LOOP
      CountChild_TBL(indx).appendNodeValues( i_prefix||indx||'CountChild_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StkCntDesc_REC"
(
  rib_oid number
, stock_count_id number
, store_id number
, product_group_id number
, schedule_id number
, count_type varchar2
, status varchar2
, display_status varchar2
, counting_method varchar2
, stock_count_time_frame varchar2
, group_type varchar2
, count_desc varchar2
, group_desc varchar2
, schedule_date date
, count_start_time number
, last_updated_time date
, export_user varchar2
, variance_count number
, variance_percent number
, variance_value number
, recount_active varchar2
, auto_authorize varchar2
, pbl_pick_less_suggested varchar2
, pbl_repl_less_suggested varchar2
, pbl_repl_neg_available varchar2
, pbl_repl_discrepant_uin varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, total_on_count number
, left_to_count number
, all_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.stock_count_id := stock_count_id;
self.store_id := store_id;
self.product_group_id := product_group_id;
self.schedule_id := schedule_id;
self.count_type := count_type;
self.status := status;
self.display_status := display_status;
self.counting_method := counting_method;
self.stock_count_time_frame := stock_count_time_frame;
self.group_type := group_type;
self.count_desc := count_desc;
self.group_desc := group_desc;
self.schedule_date := schedule_date;
self.count_start_time := count_start_time;
self.last_updated_time := last_updated_time;
self.export_user := export_user;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pbl_pick_less_suggested := pbl_pick_less_suggested;
self.pbl_repl_less_suggested := pbl_repl_less_suggested;
self.pbl_repl_neg_available := pbl_repl_neg_available;
self.pbl_repl_discrepant_uin := pbl_repl_discrepant_uin;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.total_on_count := total_on_count;
self.left_to_count := left_to_count;
self.all_items := all_items;
RETURN;
end;
constructor function "RIB_StkCntDesc_REC"
(
  rib_oid number
, stock_count_id number
, store_id number
, product_group_id number
, schedule_id number
, count_type varchar2
, status varchar2
, display_status varchar2
, counting_method varchar2
, stock_count_time_frame varchar2
, group_type varchar2
, count_desc varchar2
, group_desc varchar2
, schedule_date date
, count_start_time number
, last_updated_time date
, export_user varchar2
, variance_count number
, variance_percent number
, variance_value number
, recount_active varchar2
, auto_authorize varchar2
, pbl_pick_less_suggested varchar2
, pbl_repl_less_suggested varchar2
, pbl_repl_neg_available varchar2
, pbl_repl_discrepant_uin varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, total_on_count number
, left_to_count number
, all_items varchar2
, CountChild_TBL "RIB_CountChild_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.stock_count_id := stock_count_id;
self.store_id := store_id;
self.product_group_id := product_group_id;
self.schedule_id := schedule_id;
self.count_type := count_type;
self.status := status;
self.display_status := display_status;
self.counting_method := counting_method;
self.stock_count_time_frame := stock_count_time_frame;
self.group_type := group_type;
self.count_desc := count_desc;
self.group_desc := group_desc;
self.schedule_date := schedule_date;
self.count_start_time := count_start_time;
self.last_updated_time := last_updated_time;
self.export_user := export_user;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pbl_pick_less_suggested := pbl_pick_less_suggested;
self.pbl_repl_less_suggested := pbl_repl_less_suggested;
self.pbl_repl_neg_available := pbl_repl_neg_available;
self.pbl_repl_discrepant_uin := pbl_repl_discrepant_uin;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.total_on_count := total_on_count;
self.left_to_count := left_to_count;
self.all_items := all_items;
self.CountChild_TBL := CountChild_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_CountChild_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CountChild_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  child_id varchar2(25),
  description varchar2(40),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, COUNT_SCHEDULED, COUNT_IN_PROGRESS, COUNT_COMPLETE, RECOUNT_SCHEDULED, RECOUNT_IN_PROGRESS, RECOUNT_COMPLETE, APPROVAL_SCHEDULED, APPROVAL_IN_PROGRESS, APPROVAL_PROCESSING, APPROVAL_AUTHORIZED, APPROVAL_COMPLETE, CANCELED, UNKNOWN] (all lower-case)
  display_status varchar2(20), -- display_status is enumeration field, valid values are [NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY] (all lower-case)
  left_to_count number(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CountChild_REC"
(
  rib_oid number
, child_id varchar2
, description varchar2
, status varchar2
, display_status varchar2
, left_to_count number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CountChild_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntDesc') := "ns_name_StkCntDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'child_id') := child_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_status') := display_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'left_to_count') := left_to_count;
END appendNodeValues;
constructor function "RIB_CountChild_REC"
(
  rib_oid number
, child_id varchar2
, description varchar2
, status varchar2
, display_status varchar2
, left_to_count number
) return self as result is
begin
self.rib_oid := rib_oid;
self.child_id := child_id;
self.description := description;
self.status := status;
self.display_status := display_status;
self.left_to_count := left_to_count;
RETURN;
end;
END;
/
