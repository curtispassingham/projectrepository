DROP TYPE "RIB_AllocDtlTckt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtlTckt_REC";
/
DROP TYPE "RIB_AllocDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtl_REC";
/
DROP TYPE "RIB_AllocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDesc_REC";
/
DROP TYPE "RIB_AllocDtlTckt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AllocDtlTckt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  comp_item varchar2(25),
  comp_price number(20,4),
  comp_selling_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AllocDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
, comp_selling_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AllocDtlTckt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AllocDesc') := "ns_name_AllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_item') := comp_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_price') := comp_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_selling_uom') := comp_selling_uom;
END appendNodeValues;
constructor function "RIB_AllocDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
, comp_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.comp_item := comp_item;
self.comp_price := comp_price;
self.comp_selling_uom := comp_selling_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_AllocDtlTckt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtlTckt_TBL" AS TABLE OF "RIB_AllocDtlTckt_REC";
/
DROP TYPE "RIB_AllocDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AllocDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_to_loc number(10),
  to_loc varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  qty_allocated number(12,4),
  price number(20,4),
  selling_uom varchar2(4),
  priority number(4),
  store_ord_mult varchar2(1),
  in_store_date date,
  rush_flag varchar2(1),
  AllocDtlTckt_TBL "RIB_AllocDtlTckt_TBL",   -- Size of "RIB_AllocDtlTckt_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
) return self as result
,constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
) return self as result
,constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
, rush_flag varchar2
) return self as result
,constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
, rush_flag varchar2
, AllocDtlTckt_TBL "RIB_AllocDtlTckt_TBL"  -- Size of "RIB_AllocDtlTckt_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AllocDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AllocDesc') := "ns_name_AllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_to_loc') := physical_to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_allocated') := qty_allocated;
  rib_obj_util.g_RIB_element_values(i_prefix||'price') := price;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_ord_mult') := store_ord_mult;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_store_date') := in_store_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'rush_flag') := rush_flag;
  IF AllocDtlTckt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AllocDtlTckt_TBL.';
    FOR INDX IN AllocDtlTckt_TBL.FIRST()..AllocDtlTckt_TBL.LAST() LOOP
      AllocDtlTckt_TBL(indx).appendNodeValues( i_prefix||indx||'AllocDtlTckt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.qty_allocated := qty_allocated;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.store_ord_mult := store_ord_mult;
RETURN;
end;
constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.qty_allocated := qty_allocated;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.store_ord_mult := store_ord_mult;
self.in_store_date := in_store_date;
RETURN;
end;
constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
, rush_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.qty_allocated := qty_allocated;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.store_ord_mult := store_ord_mult;
self.in_store_date := in_store_date;
self.rush_flag := rush_flag;
RETURN;
end;
constructor function "RIB_AllocDtl_REC"
(
  rib_oid number
, physical_to_loc number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, qty_allocated number
, price number
, selling_uom varchar2
, priority number
, store_ord_mult varchar2
, in_store_date date
, rush_flag varchar2
, AllocDtlTckt_TBL "RIB_AllocDtlTckt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_to_loc := physical_to_loc;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.qty_allocated := qty_allocated;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.store_ord_mult := store_ord_mult;
self.in_store_date := in_store_date;
self.rush_flag := rush_flag;
self.AllocDtlTckt_TBL := AllocDtlTckt_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_AllocDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtl_TBL" AS TABLE OF "RIB_AllocDtl_REC";
/
DROP TYPE "RIB_AllocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AllocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  alloc_no number(10),
  doc_type varchar2(1),
  physical_wh number(10),
  wh number(10),
  item varchar2(25),
  pick_not_before_date date,
  pick_not_after_date date,
  order_type varchar2(9),
  order_no varchar2(12),
  order_doc_type varchar2(1),
  event varchar2(6),
  event_description varchar2(1000),
  priority number(4),
  ticket_type_id varchar2(4),
  AllocDtl_TBL "RIB_AllocDtl_TBL",   -- Size of "RIB_AllocDtl_TBL" is unbounded
  context_type varchar2(6),
  context_value varchar2(25),
  alloc_status varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"  -- Size of "RIB_AllocDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"  -- Size of "RIB_AllocDtl_TBL" is unbounded
, context_type varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"  -- Size of "RIB_AllocDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
) return self as result
,constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"  -- Size of "RIB_AllocDtl_TBL" is unbounded
, context_type varchar2
, context_value varchar2
, alloc_status varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AllocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AllocDesc') := "ns_name_AllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_no') := alloc_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh') := wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_before_date') := pick_not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_after_date') := pick_not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_doc_type') := order_doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'event') := event;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_description') := event_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_type_id') := ticket_type_id;
  IF AllocDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AllocDtl_TBL.';
    FOR INDX IN AllocDtl_TBL.FIRST()..AllocDtl_TBL.LAST() LOOP
      AllocDtl_TBL(indx).appendNodeValues( i_prefix||indx||'AllocDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type') := context_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_status') := alloc_status;
END appendNodeValues;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
self.ticket_type_id := ticket_type_id;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
self.ticket_type_id := ticket_type_id;
self.AllocDtl_TBL := AllocDtl_TBL;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"
, context_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
self.ticket_type_id := ticket_type_id;
self.AllocDtl_TBL := AllocDtl_TBL;
self.context_type := context_type;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"
, context_type varchar2
, context_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
self.ticket_type_id := ticket_type_id;
self.AllocDtl_TBL := AllocDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
RETURN;
end;
constructor function "RIB_AllocDesc_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, order_no varchar2
, order_doc_type varchar2
, event varchar2
, event_description varchar2
, priority number
, ticket_type_id varchar2
, AllocDtl_TBL "RIB_AllocDtl_TBL"
, context_type varchar2
, context_value varchar2
, alloc_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.order_no := order_no;
self.order_doc_type := order_doc_type;
self.event := event;
self.event_description := event_description;
self.priority := priority;
self.ticket_type_id := ticket_type_id;
self.AllocDtl_TBL := AllocDtl_TBL;
self.context_type := context_type;
self.context_value := context_value;
self.alloc_status := alloc_status;
RETURN;
end;
END;
/
