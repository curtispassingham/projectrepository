DROP TYPE "RIB_Principal_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Principal_REC";
/
DROP TYPE "RIB_MessageContext_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_MessageContext_REC";
/
DROP TYPE "RIB_ServiceOpContext_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ServiceOpContext_REC";
/
DROP TYPE "RIB_Property_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Property_REC";
/
DROP TYPE "RIB_Principal_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Principal_REC" UNDER RIB_OBJECT (
  "ns_version_v1" varchar2(1), -- This local variable name specifies the version of the Principal. 
  "ns_name_ServiceOpContext" varchar2(1), -- This local variable name specifies the type of the Principal
  "ns_type_services" varchar2(1), -- This local variable name specifies the type of the Principal (BO or BM)
  "ns_location_integration" varchar2(1), -- This local variable name specifies the location of the Principal (custom or base)
  "ns_level_nontop" varchar2(1), -- This local variable name specifies the level of the Principal (toplevel or nontoplevel)
  name varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Principal_REC"
(
  rib_oid number
, name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Principal_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpContext') := "ns_name_ServiceOpContext";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
END appendNodeValues;
constructor function "RIB_Principal_REC"
(
  rib_oid number
, name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
RETURN;
end;
END;
/
DROP TYPE "RIB_Property_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_Property_TBL" AS TABLE OF "RIB_Property_REC";
/
DROP TYPE "RIB_MessageContext_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MessageContext_REC" UNDER RIB_OBJECT (
  "ns_version_v1" varchar2(1), -- This local variable name specifies the version of the MessageContext. 
  "ns_name_ServiceOpContext" varchar2(1), -- This local variable name specifies the type of the MessageContext
  "ns_type_services" varchar2(1), -- This local variable name specifies the type of the MessageContext (BO or BM)
  "ns_location_integration" varchar2(1), -- This local variable name specifies the location of the MessageContext (custom or base)
  "ns_level_nontop" varchar2(1), -- This local variable name specifies the level of the MessageContext (toplevel or nontoplevel)
  Property_TBL "RIB_Property_TBL",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MessageContext_REC"
(
  rib_oid number
, Property_TBL "RIB_Property_TBL"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MessageContext_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpContext') := "ns_name_ServiceOpContext";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF Property_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'Property_TBL.';
    FOR INDX IN Property_TBL.FIRST()..Property_TBL.LAST() LOOP
      Property_TBL(indx).appendNodeValues( i_prefix||indx||'Property_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_MessageContext_REC"
(
  rib_oid number
, Property_TBL "RIB_Property_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.Property_TBL := Property_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ServiceOpContext_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ServiceOpContext_REC" UNDER RIB_OBJECT (
  "ns_version_v1" varchar2(1), -- This local variable name specifies the version of the ServiceOpContext. 
  "ns_name_ServiceOpContext" varchar2(1), -- This local variable name specifies the type of the ServiceOpContext
  "ns_type_services" varchar2(1), -- This local variable name specifies the type of the ServiceOpContext (BO or BM)
  "ns_location_integration" varchar2(1), -- This local variable name specifies the location of the ServiceOpContext (custom or base)
  "ns_level_top" varchar2(1), -- This local variable name specifies the level of the ServiceOpContext (toplevel or nontoplevel)
  Principal "RIB_Principal_REC",
  MessageContext "RIB_MessageContext_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ServiceOpContext_REC"
(
  rib_oid number
, Principal "RIB_Principal_REC"
, MessageContext "RIB_MessageContext_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ServiceOpContext_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpContext') := "ns_name_ServiceOpContext";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'Principal.';
  Principal.appendNodeValues( i_prefix||'Principal');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'MessageContext.';
  MessageContext.appendNodeValues( i_prefix||'MessageContext');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ServiceOpContext_REC"
(
  rib_oid number
, Principal "RIB_Principal_REC"
, MessageContext "RIB_MessageContext_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.Principal := Principal;
self.MessageContext := MessageContext;
RETURN;
end;
END;
/
DROP TYPE "RIB_Property_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Property_REC" UNDER RIB_OBJECT (
  "ns_version_v1" varchar2(1), -- This local variable name specifies the version of the Property. 
  "ns_name_ServiceOpContext" varchar2(1), -- This local variable name specifies the type of the Property
  "ns_type_services" varchar2(1), -- This local variable name specifies the type of the Property (BO or BM)
  "ns_location_integration" varchar2(1), -- This local variable name specifies the location of the Property (custom or base)
  "ns_level_nontop" varchar2(1), -- This local variable name specifies the level of the Property (toplevel or nontoplevel)
  name varchar2(255),
  value varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Property_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Property_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ServiceOpContext') := "ns_name_ServiceOpContext";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_services') := "ns_type_services";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_integration') := "ns_location_integration";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_Property_REC"
(
  rib_oid number
, name varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.name := name;
self.value := value;
RETURN;
end;
END;
/
