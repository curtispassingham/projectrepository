DROP TYPE "RIB_InvAdjustUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAdjustUin_REC";
/
DROP TYPE "RIB_InvAdjustDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAdjustDtl_REC";
/
DROP TYPE "RIB_InvAdjustDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAdjustDesc_REC";
/
DROP TYPE "RIB_InvAdjustUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAdjustUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAdjustDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAdjustUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAdjustUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAdjustDesc') := "ns_name_InvAdjustDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_InvAdjustUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_InvAdjustUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAdjustUin_TBL" AS TABLE OF "RIB_InvAdjustUin_REC";
/
DROP TYPE "RIB_InvAdjustDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAdjustDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAdjustDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  adjustment_reason_code number(4),
  unit_qty number(12,4),
  transshipment_nbr varchar2(30),
  from_disposition varchar2(4),
  to_disposition varchar2(4),
  from_trouble_code varchar2(9),
  to_trouble_code varchar2(9),
  from_wip_code varchar2(9),
  to_wip_code varchar2(9),
  transaction_code number(4),
  user_id varchar2(30),
  create_date date,
  po_nbr varchar2(12),
  doc_type varchar2(1),
  aux_reason_code varchar2(4),
  weight number(12,4),
  weight_uom varchar2(4),
  unit_cost number(20,4),
  InvAdjustUin_TBL "RIB_InvAdjustUin_TBL",   -- Size of "RIB_InvAdjustUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
, unit_cost number
) return self as result
,constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
, unit_cost number
, InvAdjustUin_TBL "RIB_InvAdjustUin_TBL"  -- Size of "RIB_InvAdjustUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAdjustDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAdjustDesc') := "ns_name_InvAdjustDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'adjustment_reason_code') := adjustment_reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'transshipment_nbr') := transshipment_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_trouble_code') := from_trouble_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_trouble_code') := to_trouble_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_wip_code') := from_wip_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_wip_code') := to_wip_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_code') := transaction_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'aux_reason_code') := aux_reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  IF InvAdjustUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InvAdjustUin_TBL.';
    FOR INDX IN InvAdjustUin_TBL.FIRST()..InvAdjustUin_TBL.LAST() LOOP
      InvAdjustUin_TBL(indx).appendNodeValues( i_prefix||indx||'InvAdjustUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
self.aux_reason_code := aux_reason_code;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
self.aux_reason_code := aux_reason_code;
self.weight := weight;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
self.aux_reason_code := aux_reason_code;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
self.aux_reason_code := aux_reason_code;
self.weight := weight;
self.weight_uom := weight_uom;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_InvAdjustDtl_REC"
(
  rib_oid number
, item_id varchar2
, adjustment_reason_code number
, unit_qty number
, transshipment_nbr varchar2
, from_disposition varchar2
, to_disposition varchar2
, from_trouble_code varchar2
, to_trouble_code varchar2
, from_wip_code varchar2
, to_wip_code varchar2
, transaction_code number
, user_id varchar2
, create_date date
, po_nbr varchar2
, doc_type varchar2
, aux_reason_code varchar2
, weight number
, weight_uom varchar2
, unit_cost number
, InvAdjustUin_TBL "RIB_InvAdjustUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.adjustment_reason_code := adjustment_reason_code;
self.unit_qty := unit_qty;
self.transshipment_nbr := transshipment_nbr;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.from_trouble_code := from_trouble_code;
self.to_trouble_code := to_trouble_code;
self.from_wip_code := from_wip_code;
self.to_wip_code := to_wip_code;
self.transaction_code := transaction_code;
self.user_id := user_id;
self.create_date := create_date;
self.po_nbr := po_nbr;
self.doc_type := doc_type;
self.aux_reason_code := aux_reason_code;
self.weight := weight;
self.weight_uom := weight_uom;
self.unit_cost := unit_cost;
self.InvAdjustUin_TBL := InvAdjustUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InvAdjustDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InvAdjustDtl_TBL" AS TABLE OF "RIB_InvAdjustDtl_REC";
/
DROP TYPE "RIB_InvAdjustDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_InvAdjustDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_InvAdjustDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dc_dest_id varchar2(10),
  InvAdjustDtl_TBL "RIB_InvAdjustDtl_TBL",   -- Size of "RIB_InvAdjustDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_InvAdjustDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, InvAdjustDtl_TBL "RIB_InvAdjustDtl_TBL"  -- Size of "RIB_InvAdjustDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_InvAdjustDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_InvAdjustDesc') := "ns_name_InvAdjustDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  l_new_pre :=i_prefix||'InvAdjustDtl_TBL.';
  FOR INDX IN InvAdjustDtl_TBL.FIRST()..InvAdjustDtl_TBL.LAST() LOOP
    InvAdjustDtl_TBL(indx).appendNodeValues( i_prefix||indx||'InvAdjustDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_InvAdjustDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, InvAdjustDtl_TBL "RIB_InvAdjustDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.InvAdjustDtl_TBL := InvAdjustDtl_TBL;
RETURN;
end;
END;
/
