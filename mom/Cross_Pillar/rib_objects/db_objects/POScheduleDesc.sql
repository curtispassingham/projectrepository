DROP TYPE "RIB_POScheduleDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_POScheduleDtl_REC";
/
DROP TYPE "RIB_POSchedule_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_POSchedule_REC";
/
DROP TYPE "RIB_POScheduleDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_POScheduleDesc_REC";
/
DROP TYPE "RIB_POScheduleDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_POScheduleDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_POScheduleDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  consolidate_qty number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_POScheduleDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result
,constructor function "RIB_POScheduleDtl_REC"
(
  rib_oid number
, item_id varchar2
, consolidate_qty number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_POScheduleDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_POScheduleDesc') := "ns_name_POScheduleDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'consolidate_qty') := consolidate_qty;
END appendNodeValues;
constructor function "RIB_POScheduleDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_POScheduleDtl_REC"
(
  rib_oid number
, item_id varchar2
, consolidate_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.consolidate_qty := consolidate_qty;
RETURN;
end;
END;
/
DROP TYPE "RIB_POScheduleDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_POScheduleDtl_TBL" AS TABLE OF "RIB_POScheduleDtl_REC";
/
DROP TYPE "RIB_POSchedule_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_POSchedule_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_POScheduleDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  requisition_nbr varchar2(12),
  requisition_type varchar2(1),
  POScheduleDtl_TBL "RIB_POScheduleDtl_TBL",   -- Size of "RIB_POScheduleDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
) return self as result
,constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
, requisition_type varchar2
) return self as result
,constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
, requisition_type varchar2
, POScheduleDtl_TBL "RIB_POScheduleDtl_TBL"  -- Size of "RIB_POScheduleDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_POSchedule_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_POScheduleDesc') := "ns_name_POScheduleDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'requisition_nbr') := requisition_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'requisition_type') := requisition_type;
  IF POScheduleDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'POScheduleDtl_TBL.';
    FOR INDX IN POScheduleDtl_TBL.FIRST()..POScheduleDtl_TBL.LAST() LOOP
      POScheduleDtl_TBL(indx).appendNodeValues( i_prefix||indx||'POScheduleDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.requisition_nbr := requisition_nbr;
RETURN;
end;
constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
, requisition_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.requisition_nbr := requisition_nbr;
self.requisition_type := requisition_type;
RETURN;
end;
constructor function "RIB_POSchedule_REC"
(
  rib_oid number
, requisition_nbr varchar2
, requisition_type varchar2
, POScheduleDtl_TBL "RIB_POScheduleDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.requisition_nbr := requisition_nbr;
self.requisition_type := requisition_type;
self.POScheduleDtl_TBL := POScheduleDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_POSchedule_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_POSchedule_TBL" AS TABLE OF "RIB_POSchedule_REC";
/
DROP TYPE "RIB_POScheduleDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_POScheduleDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_POScheduleDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  schedule_nbr number(8),
  physical_wh varchar2(10),
  POSchedule_TBL "RIB_POSchedule_TBL",   -- Size of "RIB_POSchedule_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_POScheduleDesc_REC"
(
  rib_oid number
, schedule_nbr number
, physical_wh varchar2
, POSchedule_TBL "RIB_POSchedule_TBL"  -- Size of "RIB_POSchedule_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_POScheduleDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_POScheduleDesc') := "ns_name_POScheduleDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'schedule_nbr') := schedule_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  l_new_pre :=i_prefix||'POSchedule_TBL.';
  FOR INDX IN POSchedule_TBL.FIRST()..POSchedule_TBL.LAST() LOOP
    POSchedule_TBL(indx).appendNodeValues( i_prefix||indx||'POSchedule_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_POScheduleDesc_REC"
(
  rib_oid number
, schedule_nbr number
, physical_wh varchar2
, POSchedule_TBL "RIB_POSchedule_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.schedule_nbr := schedule_nbr;
self.physical_wh := physical_wh;
self.POSchedule_TBL := POSchedule_TBL;
RETURN;
end;
END;
/
