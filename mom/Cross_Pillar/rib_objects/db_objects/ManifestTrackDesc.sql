DROP TYPE "RIB_ManifestTrackDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ManifestTrackDesc_REC";
/
DROP TYPE "RIB_ManifestTrackDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ManifestTrackDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ManifestTrackDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  transaction_type varchar2(30), -- transaction_type is enumeration field, valid values are [FULFILLMENT_ORDER_DELIVERY, RETURN, TRANSFER] (all lower-case)
  transaction_id number(12),
  shipment_id varchar2(120),
  container_id varchar2(20),
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  carrier_tracking_number varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ManifestTrackDesc_REC"
(
  rib_oid number
, transaction_type varchar2
, transaction_id number
, shipment_id varchar2
, container_id varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_tracking_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ManifestTrackDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ManifestTrackDesc') := "ns_name_ManifestTrackDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_tracking_number') := carrier_tracking_number;
END appendNodeValues;
constructor function "RIB_ManifestTrackDesc_REC"
(
  rib_oid number
, transaction_type varchar2
, transaction_id number
, shipment_id varchar2
, container_id varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_tracking_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.transaction_type := transaction_type;
self.transaction_id := transaction_id;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_tracking_number := carrier_tracking_number;
RETURN;
end;
END;
/
