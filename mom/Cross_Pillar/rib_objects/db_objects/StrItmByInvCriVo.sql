DROP TYPE "RIB_StrItmByInvCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmByInvCriVo_REC";
/
DROP TYPE "RIB_StrItmByInvCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmByInvCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmByInvCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  include_non_ranged varchar2(5), --include_non_ranged is boolean field, valid values are true,false (all lower-case) 
  item_description varchar2(250),
  search_limit number(3),
  inventory_status varchar2(20), -- inventory_status is enumeration field, valid values are [AVAILABLE, UNAVAILABLE] (all lower-case)
  nonsellable_qty_type_id number(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmByInvCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, inventory_status varchar2
) return self as result
,constructor function "RIB_StrItmByInvCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2  --include_non_ranged is boolean field, valid values are true,false (all lower-case)
, item_description varchar2
, search_limit number
, inventory_status varchar2
, nonsellable_qty_type_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmByInvCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmByInvCriVo') := "ns_name_StrItmByInvCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_non_ranged') := include_non_ranged;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'search_limit') := search_limit;
  rib_obj_util.g_RIB_element_values(i_prefix||'inventory_status') := inventory_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsellable_qty_type_id') := nonsellable_qty_type_id;
END appendNodeValues;
constructor function "RIB_StrItmByInvCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, inventory_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.inventory_status := inventory_status;
RETURN;
end;
constructor function "RIB_StrItmByInvCriVo_REC"
(
  rib_oid number
, store_id number
, department_id number
, class_id number
, subclass_id number
, include_non_ranged varchar2
, item_description varchar2
, search_limit number
, inventory_status varchar2
, nonsellable_qty_type_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
self.include_non_ranged := include_non_ranged;
self.item_description := item_description;
self.search_limit := search_limit;
self.inventory_status := inventory_status;
self.nonsellable_qty_type_id := nonsellable_qty_type_id;
RETURN;
end;
END;
/
