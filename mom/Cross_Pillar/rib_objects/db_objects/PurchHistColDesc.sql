@@PurchHistDesc.sql;
/
DROP TYPE "RIB_PurchHistColDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchHistColDesc_REC";
/
DROP TYPE "RIB_PurchHistDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchHistDesc_TBL" AS TABLE OF "RIB_PurchHistDesc_REC";
/
DROP TYPE "RIB_PurchHistColDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PurchHistColDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PurchHistColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  collection_size number(3),
  PurchHistDesc_TBL "RIB_PurchHistDesc_TBL",   -- Size of "RIB_PurchHistDesc_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PurchHistColDesc_REC"
(
  rib_oid number
, collection_size number
) return self as result
,constructor function "RIB_PurchHistColDesc_REC"
(
  rib_oid number
, collection_size number
, PurchHistDesc_TBL "RIB_PurchHistDesc_TBL"  -- Size of "RIB_PurchHistDesc_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PurchHistColDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PurchHistColDesc') := "ns_name_PurchHistColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
  IF PurchHistDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PurchHistDesc_TBL.';
    FOR INDX IN PurchHistDesc_TBL.FIRST()..PurchHistDesc_TBL.LAST() LOOP
      PurchHistDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PurchHistDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PurchHistColDesc_REC"
(
  rib_oid number
, collection_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
RETURN;
end;
constructor function "RIB_PurchHistColDesc_REC"
(
  rib_oid number
, collection_size number
, PurchHistDesc_TBL "RIB_PurchHistDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.collection_size := collection_size;
self.PurchHistDesc_TBL := PurchHistDesc_TBL;
RETURN;
end;
END;
/
