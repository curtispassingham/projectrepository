DROP TYPE "RIB_MrchHrClsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MrchHrClsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_MrchHrClsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  class number(4),
  class_name varchar2(120),
  class_vat_ind varchar2(1),
  dept number(4),
  ExtOfMrchHrClsDesc "RIB_ExtOfMrchHrClsDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MrchHrClsDesc_REC"
(
  rib_oid number
, class number
, class_name varchar2
, class_vat_ind varchar2
, dept number
) return self as result
,constructor function "RIB_MrchHrClsDesc_REC"
(
  rib_oid number
, class number
, class_name varchar2
, class_vat_ind varchar2
, dept number
, ExtOfMrchHrClsDesc "RIB_ExtOfMrchHrClsDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MrchHrClsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_MrchHrClsDesc') := "ns_name_MrchHrClsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'class') := class;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_name') := class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_vat_ind') := class_vat_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  l_new_pre :=i_prefix||'ExtOfMrchHrClsDesc.';
  ExtOfMrchHrClsDesc.appendNodeValues( i_prefix||'ExtOfMrchHrClsDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_MrchHrClsDesc_REC"
(
  rib_oid number
, class number
, class_name varchar2
, class_vat_ind varchar2
, dept number
) return self as result is
begin
self.rib_oid := rib_oid;
self.class := class;
self.class_name := class_name;
self.class_vat_ind := class_vat_ind;
self.dept := dept;
RETURN;
end;
constructor function "RIB_MrchHrClsDesc_REC"
(
  rib_oid number
, class number
, class_name varchar2
, class_vat_ind varchar2
, dept number
, ExtOfMrchHrClsDesc "RIB_ExtOfMrchHrClsDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.class := class;
self.class_name := class_name;
self.class_vat_ind := class_vat_ind;
self.dept := dept;
self.ExtOfMrchHrClsDesc := ExtOfMrchHrClsDesc;
RETURN;
end;
END;
/
