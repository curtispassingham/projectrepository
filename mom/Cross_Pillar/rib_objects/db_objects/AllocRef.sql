DROP TYPE "RIB_AllocDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtlRef_REC";
/
DROP TYPE "RIB_AllocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocRef_REC";
/
DROP TYPE "RIB_AllocDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AllocDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AllocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  to_loc varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
) return self as result
,constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
) return self as result
,constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
) return self as result
,constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AllocDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AllocRef') := "ns_name_AllocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
END appendNodeValues;
constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
RETURN;
end;
constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
RETURN;
end;
constructor function "RIB_AllocDtlRef_REC"
(
  rib_oid number
, to_loc varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_AllocDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AllocDtlRef_TBL" AS TABLE OF "RIB_AllocDtlRef_REC";
/
DROP TYPE "RIB_AllocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AllocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AllocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  alloc_no number(10),
  doc_type varchar2(1),
  physical_wh number(10),
  wh number(10),
  item varchar2(25),
  AllocDtlRef_TBL "RIB_AllocDtlRef_TBL",   -- Size of "RIB_AllocDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AllocRef_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
) return self as result
,constructor function "RIB_AllocRef_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, AllocDtlRef_TBL "RIB_AllocDtlRef_TBL"  -- Size of "RIB_AllocDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AllocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AllocRef') := "ns_name_AllocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_no') := alloc_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'wh') := wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  IF AllocDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AllocDtlRef_TBL.';
    FOR INDX IN AllocDtlRef_TBL.FIRST()..AllocDtlRef_TBL.LAST() LOOP
      AllocDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'AllocDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AllocRef_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
RETURN;
end;
constructor function "RIB_AllocRef_REC"
(
  rib_oid number
, alloc_no number
, doc_type varchar2
, physical_wh number
, wh number
, item varchar2
, AllocDtlRef_TBL "RIB_AllocDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.doc_type := doc_type;
self.physical_wh := physical_wh;
self.wh := wh;
self.item := item;
self.AllocDtlRef_TBL := AllocDtlRef_TBL;
RETURN;
end;
END;
/
