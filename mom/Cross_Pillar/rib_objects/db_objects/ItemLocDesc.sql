DROP TYPE "RIB_ItemLocVirt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirt_REC";
/
DROP TYPE "RIB_ItemLocVirtRepl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRepl_REC";
/
DROP TYPE "RIB_ItemLocPhys_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocPhys_REC";
/
DROP TYPE "RIB_ItemLocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocDesc_REC";
/
DROP TYPE "RIB_ItemLocVirt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocVirt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  loc number(10),
  loc_type varchar2(1),
  local_item_desc varchar2(250),
  local_short_desc varchar2(120),
  status varchar2(1),
  primary_supp number(10),
  primary_cntry varchar2(3),
  receive_as_type varchar2(1),
  taxable_ind varchar2(1),
  source_method varchar2(1),
  source_wh number(10),
  unit_retail number(20,4),
  selling_unit_retail number(20,4),
  selling_uom varchar2(4),
  store_price_ind varchar2(1),
  purchase_type varchar2(6),
  uin_type varchar2(6),
  uin_label varchar2(6),
  capture_time varchar2(6),
  ext_uin_ind varchar2(1),
  ranged_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocVirt_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, local_item_desc varchar2
, local_short_desc varchar2
, status varchar2
, primary_supp number
, primary_cntry varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, source_method varchar2
, source_wh number
, unit_retail number
, selling_unit_retail number
, selling_uom varchar2
, store_price_ind varchar2
, purchase_type varchar2
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, ranged_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocVirt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocDesc') := "ns_name_ItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'local_item_desc') := local_item_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'local_short_desc') := local_short_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supp') := primary_supp;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_cntry') := primary_cntry;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_as_type') := receive_as_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxable_ind') := taxable_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_method') := source_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_wh') := source_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_retail') := unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_retail') := selling_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_price_ind') := store_price_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type') := purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_type') := uin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_label') := uin_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'capture_time') := capture_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_uin_ind') := ext_uin_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ranged_ind') := ranged_ind;
END appendNodeValues;
constructor function "RIB_ItemLocVirt_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, local_item_desc varchar2
, local_short_desc varchar2
, status varchar2
, primary_supp number
, primary_cntry varchar2
, receive_as_type varchar2
, taxable_ind varchar2
, source_method varchar2
, source_wh number
, unit_retail number
, selling_unit_retail number
, selling_uom varchar2
, store_price_ind varchar2
, purchase_type varchar2
, uin_type varchar2
, uin_label varchar2
, capture_time varchar2
, ext_uin_ind varchar2
, ranged_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.local_item_desc := local_item_desc;
self.local_short_desc := local_short_desc;
self.status := status;
self.primary_supp := primary_supp;
self.primary_cntry := primary_cntry;
self.receive_as_type := receive_as_type;
self.taxable_ind := taxable_ind;
self.source_method := source_method;
self.source_wh := source_wh;
self.unit_retail := unit_retail;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.store_price_ind := store_price_ind;
self.purchase_type := purchase_type;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.capture_time := capture_time;
self.ext_uin_ind := ext_uin_ind;
self.ranged_ind := ranged_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocVirtRepl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRepl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  loc number(10),
  loc_type varchar2(1),
  primary_repl_supplier number(10),
  repl_method varchar2(6),
  reject_store_order_ind varchar2(1),
  next_delivery_date date,
  multiple_runs_per_day_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
) return self as result
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
) return self as result
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
) return self as result
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
) return self as result
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
, next_delivery_date date
) return self as result
,constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
, next_delivery_date date
, multiple_runs_per_day_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocVirtRepl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocDesc') := "ns_name_ItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_repl_supplier') := primary_repl_supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'repl_method') := repl_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'reject_store_order_ind') := reject_store_order_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'next_delivery_date') := next_delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'multiple_runs_per_day_ind') := multiple_runs_per_day_ind;
END appendNodeValues;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.primary_repl_supplier := primary_repl_supplier;
RETURN;
end;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.primary_repl_supplier := primary_repl_supplier;
self.repl_method := repl_method;
RETURN;
end;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.primary_repl_supplier := primary_repl_supplier;
self.repl_method := repl_method;
self.reject_store_order_ind := reject_store_order_ind;
RETURN;
end;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
, next_delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.primary_repl_supplier := primary_repl_supplier;
self.repl_method := repl_method;
self.reject_store_order_ind := reject_store_order_ind;
self.next_delivery_date := next_delivery_date;
RETURN;
end;
constructor function "RIB_ItemLocVirtRepl_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, primary_repl_supplier number
, repl_method varchar2
, reject_store_order_ind varchar2
, next_delivery_date date
, multiple_runs_per_day_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.primary_repl_supplier := primary_repl_supplier;
self.repl_method := repl_method;
self.reject_store_order_ind := reject_store_order_ind;
self.next_delivery_date := next_delivery_date;
self.multiple_runs_per_day_ind := multiple_runs_per_day_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocVirt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirt_TBL" AS TABLE OF "RIB_ItemLocVirt_REC";
/
DROP TYPE "RIB_ItemLocVirtRepl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRepl_TBL" AS TABLE OF "RIB_ItemLocVirtRepl_REC";
/
DROP TYPE "RIB_ItemLocPhys_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocPhys_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_loc number(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  returnable_ind varchar2(1),
  ItemLocVirt_TBL "RIB_ItemLocVirt_TBL",   -- Size of "RIB_ItemLocVirt_TBL" is unbounded
  ItemLocVirtRepl_TBL "RIB_ItemLocVirtRepl_TBL",   -- Size of "RIB_ItemLocVirtRepl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
) return self as result
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
) return self as result
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
) return self as result
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
, ItemLocVirt_TBL "RIB_ItemLocVirt_TBL"  -- Size of "RIB_ItemLocVirt_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
, ItemLocVirt_TBL "RIB_ItemLocVirt_TBL"  -- Size of "RIB_ItemLocVirt_TBL" is unbounded
, ItemLocVirtRepl_TBL "RIB_ItemLocVirtRepl_TBL"  -- Size of "RIB_ItemLocVirtRepl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocPhys_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocDesc') := "ns_name_ItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_loc') := physical_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'returnable_ind') := returnable_ind;
  IF ItemLocVirt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemLocVirt_TBL.';
    FOR INDX IN ItemLocVirt_TBL.FIRST()..ItemLocVirt_TBL.LAST() LOOP
      ItemLocVirt_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocVirt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemLocVirtRepl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemLocVirtRepl_TBL.';
    FOR INDX IN ItemLocVirtRepl_TBL.FIRST()..ItemLocVirtRepl_TBL.LAST() LOOP
      ItemLocVirtRepl_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocVirtRepl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
RETURN;
end;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
RETURN;
end;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.returnable_ind := returnable_ind;
RETURN;
end;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
, ItemLocVirt_TBL "RIB_ItemLocVirt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.returnable_ind := returnable_ind;
self.ItemLocVirt_TBL := ItemLocVirt_TBL;
RETURN;
end;
constructor function "RIB_ItemLocPhys_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, returnable_ind varchar2
, ItemLocVirt_TBL "RIB_ItemLocVirt_TBL"
, ItemLocVirtRepl_TBL "RIB_ItemLocVirtRepl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.returnable_ind := returnable_ind;
self.ItemLocVirt_TBL := ItemLocVirt_TBL;
self.ItemLocVirtRepl_TBL := ItemLocVirtRepl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocPhys_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocPhys_TBL" AS TABLE OF "RIB_ItemLocPhys_REC";
/
DROP TYPE "RIB_ItemLocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  ItemLocPhys_TBL "RIB_ItemLocPhys_TBL",   -- Size of "RIB_ItemLocPhys_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocDesc_REC"
(
  rib_oid number
, item varchar2
, ItemLocPhys_TBL "RIB_ItemLocPhys_TBL"  -- Size of "RIB_ItemLocPhys_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocDesc') := "ns_name_ItemLocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  l_new_pre :=i_prefix||'ItemLocPhys_TBL.';
  FOR INDX IN ItemLocPhys_TBL.FIRST()..ItemLocPhys_TBL.LAST() LOOP
    ItemLocPhys_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocPhys_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItemLocDesc_REC"
(
  rib_oid number
, item varchar2
, ItemLocPhys_TBL "RIB_ItemLocPhys_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ItemLocPhys_TBL := ItemLocPhys_TBL;
RETURN;
end;
END;
/
