@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_StkCntChdDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntChdDesc_REC";
/
DROP TYPE "RIB_StkCntChdLineItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntChdLineItm_REC";
/
DROP TYPE "RIB_StkCntUinNum_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntUinNum_REC";
/
DROP TYPE "RIB_StkCntChdLineItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntChdLineItm_TBL" AS TABLE OF "RIB_StkCntChdLineItm_REC";
/
DROP TYPE "RIB_StkCntChdDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCntChdDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntChdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  stock_count_id number(12),
  stock_count_child_id number(12),
  count_desc varchar2(255),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, COUNT_SCHEDULED, COUNT_IN_PROGRESS, COUNT_COMPLETE, RECOUNT_SCHEDULED, RECOUNT_IN_PROGRESS, RECOUNT_COMPLETE, APPROVAL_SCHEDULED, APPROVAL_IN_PROGRESS, APPROVAL_PROCESSING, APPROVAL_AUTHORIZED, APPROVAL_COMPLETE, CANCELED, UNKNOWN] (all lower-case)
  display_status varchar2(20), -- display_status is enumeration field, valid values are [NONE, NEW, ACTIVE, IN_PROGRESS, PENDING, PROCESSING, AUTHORIZED, COMPLETED, CANCELED, VIEW_ONLY] (all lower-case)
  area varchar2(20), -- area is enumeration field, valid values are [NO_LOCATION, SHOPFLOOR, BACKROOM, NO_VALUE] (all lower-case)
  left_to_count number(12),
  StkCntChdLineItm_TBL "RIB_StkCntChdLineItm_TBL",   -- Size of "RIB_StkCntChdLineItm_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCntChdDesc_REC"
(
  rib_oid number
, stock_count_id number
, stock_count_child_id number
, count_desc varchar2
, status varchar2
, display_status varchar2
, area varchar2
, left_to_count number
, StkCntChdLineItm_TBL "RIB_StkCntChdLineItm_TBL"  -- Size of "RIB_StkCntChdLineItm_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCntChdDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntChdDesc') := "ns_name_StkCntChdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_count_id') := stock_count_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_count_child_id') := stock_count_child_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_desc') := count_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_status') := display_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'area') := area;
  rib_obj_util.g_RIB_element_values(i_prefix||'left_to_count') := left_to_count;
  l_new_pre :=i_prefix||'StkCntChdLineItm_TBL.';
  FOR INDX IN StkCntChdLineItm_TBL.FIRST()..StkCntChdLineItm_TBL.LAST() LOOP
    StkCntChdLineItm_TBL(indx).appendNodeValues( i_prefix||indx||'StkCntChdLineItm_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_StkCntChdDesc_REC"
(
  rib_oid number
, stock_count_id number
, stock_count_child_id number
, count_desc varchar2
, status varchar2
, display_status varchar2
, area varchar2
, left_to_count number
, StkCntChdLineItm_TBL "RIB_StkCntChdLineItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.stock_count_id := stock_count_id;
self.stock_count_child_id := stock_count_child_id;
self.count_desc := count_desc;
self.status := status;
self.display_status := display_status;
self.area := area;
self.left_to_count := left_to_count;
self.StkCntChdLineItm_TBL := StkCntChdLineItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StkCntUinNum_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StkCntUinNum_TBL" AS TABLE OF "RIB_StkCntUinNum_REC";
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_StkCntChdLineItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCntChdLineItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntChdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_item_id number(12),
  item_id varchar2(25),
  count_snapshot number(12),
  recount_snapshot number(12),
  stock_counted number(12),
  stock_recounted number(12),
  stock_counted_total number(12),
  stock_recounted_total number(12),
  stock_approved number(12),
  count_timestamp date,
  recount_timestamp date,
  approved_timestamp date,
  StkCntUinNum_TBL "RIB_StkCntUinNum_TBL",   -- Size of "RIB_StkCntUinNum_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  primary_location varchar2(5), --primary_location is boolean field, valid values are true,false (all lower-case) 
  breakable_component varchar2(5), --breakable_component is boolean field, valid values are true,false (all lower-case) 
  multi_located varchar2(5), --multi_located is boolean field, valid values are true,false (all lower-case) 
  discrepant varchar2(5), --discrepant is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCntChdLineItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, count_snapshot number
, recount_snapshot number
, stock_counted number
, stock_recounted number
, stock_counted_total number
, stock_recounted_total number
, stock_approved number
, count_timestamp date
, recount_timestamp date
, approved_timestamp date
, StkCntUinNum_TBL "RIB_StkCntUinNum_TBL"  -- Size of "RIB_StkCntUinNum_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, primary_location varchar2  --primary_location is boolean field, valid values are true,false (all lower-case)
, breakable_component varchar2  --breakable_component is boolean field, valid values are true,false (all lower-case)
, multi_located varchar2  --multi_located is boolean field, valid values are true,false (all lower-case)
, discrepant varchar2  --discrepant is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCntChdLineItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntChdDesc') := "ns_name_StkCntChdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_id') := line_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_snapshot') := count_snapshot;
  rib_obj_util.g_RIB_element_values(i_prefix||'recount_snapshot') := recount_snapshot;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_counted') := stock_counted;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_recounted') := stock_recounted;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_counted_total') := stock_counted_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_recounted_total') := stock_recounted_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_approved') := stock_approved;
  rib_obj_util.g_RIB_element_values(i_prefix||'count_timestamp') := count_timestamp;
  rib_obj_util.g_RIB_element_values(i_prefix||'recount_timestamp') := recount_timestamp;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_timestamp') := approved_timestamp;
  IF StkCntUinNum_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StkCntUinNum_TBL.';
    FOR INDX IN StkCntUinNum_TBL.FIRST()..StkCntUinNum_TBL.LAST() LOOP
      StkCntUinNum_TBL(indx).appendNodeValues( i_prefix||indx||'StkCntUinNum_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_location') := primary_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'breakable_component') := breakable_component;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_located') := multi_located;
  rib_obj_util.g_RIB_element_values(i_prefix||'discrepant') := discrepant;
END appendNodeValues;
constructor function "RIB_StkCntChdLineItm_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, count_snapshot number
, recount_snapshot number
, stock_counted number
, stock_recounted number
, stock_counted_total number
, stock_recounted_total number
, stock_approved number
, count_timestamp date
, recount_timestamp date
, approved_timestamp date
, StkCntUinNum_TBL "RIB_StkCntUinNum_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, primary_location varchar2
, breakable_component varchar2
, multi_located varchar2
, discrepant varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.count_snapshot := count_snapshot;
self.recount_snapshot := recount_snapshot;
self.stock_counted := stock_counted;
self.stock_recounted := stock_recounted;
self.stock_counted_total := stock_counted_total;
self.stock_recounted_total := stock_recounted_total;
self.stock_approved := stock_approved;
self.count_timestamp := count_timestamp;
self.recount_timestamp := recount_timestamp;
self.approved_timestamp := approved_timestamp;
self.StkCntUinNum_TBL := StkCntUinNum_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.primary_location := primary_location;
self.breakable_component := breakable_component;
self.multi_located := multi_located;
self.discrepant := discrepant;
RETURN;
end;
END;
/
DROP TYPE "RIB_StkCntUinNum_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkCntUinNum_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkCntChdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin_detail_id number(12),
  serial_number varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkCntUinNum_REC"
(
  rib_oid number
, uin_detail_id number
, serial_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkCntUinNum_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkCntChdDesc') := "ns_name_StkCntChdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_detail_id') := uin_detail_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
END appendNodeValues;
constructor function "RIB_StkCntUinNum_REC"
(
  rib_oid number
, uin_detail_id number
, serial_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin_detail_id := uin_detail_id;
self.serial_number := serial_number;
RETURN;
end;
END;
/
