@@LocOfFulfilOrdCustDesc.sql;
/
DROP TYPE "RIB_FulfilOrdCustDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdCustDesc_REC";
/
DROP TYPE "RIB_FulfilOrdCustDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FulfilOrdCustDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FulfilOrdCustDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_no varchar2(14),
  deliver_first_name varchar2(120),
  deliver_phonetic_first varchar2(120),
  deliver_last_name varchar2(120),
  deliver_phonetic_last varchar2(120),
  deliver_preferred_name varchar2(120),
  deliver_company_name varchar2(120),
  deliver_add1 varchar2(240),
  deliver_add2 varchar2(240),
  deliver_add3 varchar2(240),
  deliver_county varchar2(250),
  deliver_city varchar2(120),
  deliver_state varchar2(3),
  deliver_country_id varchar2(3),
  deliver_post varchar2(30),
  deliver_jurisdiction varchar2(10),
  deliver_phone varchar2(20),
  deliver_email varchar2(120),
  bill_first_name varchar2(120),
  bill_phonetic_first varchar2(120),
  bill_last_name varchar2(120),
  bill_phonetic_last varchar2(120),
  bill_preferred_name varchar2(120),
  bill_company_name varchar2(120),
  bill_add1 varchar2(240),
  bill_add2 varchar2(240),
  bill_add3 varchar2(240),
  bill_county varchar2(250),
  bill_city varchar2(120),
  bill_state varchar2(3),
  bill_country_id varchar2(3),
  bill_post varchar2(30),
  bill_jurisdiction varchar2(10),
  bill_phone varchar2(20),
  bill_email varchar2(120),
  LocOfFulfilOrdCustDesc "RIB_LocOfFulfilOrdCustDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
, bill_email varchar2
) return self as result
,constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
, bill_email varchar2
, LocOfFulfilOrdCustDesc "RIB_LocOfFulfilOrdCustDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FulfilOrdCustDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FulfilOrdCustDesc') := "ns_name_FulfilOrdCustDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_no') := customer_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_first_name') := deliver_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phonetic_first') := deliver_phonetic_first;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_last_name') := deliver_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phonetic_last') := deliver_phonetic_last;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_preferred_name') := deliver_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_company_name') := deliver_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add1') := deliver_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add2') := deliver_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add3') := deliver_add3;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_county') := deliver_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_city') := deliver_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_state') := deliver_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_country_id') := deliver_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_post') := deliver_post;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_jurisdiction') := deliver_jurisdiction;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phone') := deliver_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_email') := deliver_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_first_name') := bill_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phonetic_first') := bill_phonetic_first;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_last_name') := bill_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phonetic_last') := bill_phonetic_last;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_preferred_name') := bill_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_company_name') := bill_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add1') := bill_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add2') := bill_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add3') := bill_add3;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_county') := bill_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_city') := bill_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_state') := bill_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_country_id') := bill_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_post') := bill_post;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_jurisdiction') := bill_jurisdiction;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phone') := bill_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_email') := bill_email;
  l_new_pre :=i_prefix||'LocOfFulfilOrdCustDesc.';
  LocOfFulfilOrdCustDesc.appendNodeValues( i_prefix||'LocOfFulfilOrdCustDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
self.bill_post := bill_post;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
self.bill_post := bill_post;
self.bill_jurisdiction := bill_jurisdiction;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
self.bill_post := bill_post;
self.bill_jurisdiction := bill_jurisdiction;
self.bill_phone := bill_phone;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
, bill_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
self.bill_post := bill_post;
self.bill_jurisdiction := bill_jurisdiction;
self.bill_phone := bill_phone;
self.bill_email := bill_email;
RETURN;
end;
constructor function "RIB_FulfilOrdCustDesc_REC"
(
  rib_oid number
, customer_no varchar2
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_country_id varchar2
, deliver_post varchar2
, deliver_jurisdiction varchar2
, deliver_phone varchar2
, deliver_email varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_state varchar2
, bill_country_id varchar2
, bill_post varchar2
, bill_jurisdiction varchar2
, bill_phone varchar2
, bill_email varchar2
, LocOfFulfilOrdCustDesc "RIB_LocOfFulfilOrdCustDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_no := customer_no;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_country_id := deliver_country_id;
self.deliver_post := deliver_post;
self.deliver_jurisdiction := deliver_jurisdiction;
self.deliver_phone := deliver_phone;
self.deliver_email := deliver_email;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_state := bill_state;
self.bill_country_id := bill_country_id;
self.bill_post := bill_post;
self.bill_jurisdiction := bill_jurisdiction;
self.bill_phone := bill_phone;
self.bill_email := bill_email;
self.LocOfFulfilOrdCustDesc := LocOfFulfilOrdCustDesc;
RETURN;
end;
END;
/
