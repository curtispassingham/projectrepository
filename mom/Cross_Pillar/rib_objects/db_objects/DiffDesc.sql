DROP TYPE "RIB_DiffDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DiffDesc_REC";
/
DROP TYPE "RIB_DiffDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DiffDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DiffDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  diff_id varchar2(10),
  diff_type varchar2(6),
  diff_desc varchar2(120),
  industry_code varchar2(10),
  industry_subcode varchar2(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
) return self as result
,constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
, industry_code varchar2
) return self as result
,constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
, industry_code varchar2
, industry_subcode varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DiffDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DiffDesc') := "ns_name_DiffDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type') := diff_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_desc') := diff_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'industry_code') := industry_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'industry_subcode') := industry_subcode;
END appendNodeValues;
constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
self.diff_type := diff_type;
self.diff_desc := diff_desc;
RETURN;
end;
constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
, industry_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
self.diff_type := diff_type;
self.diff_desc := diff_desc;
self.industry_code := industry_code;
RETURN;
end;
constructor function "RIB_DiffDesc_REC"
(
  rib_oid number
, diff_id varchar2
, diff_type varchar2
, diff_desc varchar2
, industry_code varchar2
, industry_subcode varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.diff_id := diff_id;
self.diff_type := diff_type;
self.diff_desc := diff_desc;
self.industry_code := industry_code;
self.industry_subcode := industry_subcode;
RETURN;
end;
END;
/
