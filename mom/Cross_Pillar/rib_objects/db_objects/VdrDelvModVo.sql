DROP TYPE "RIB_VdrDelvModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvModVo_REC";
/
DROP TYPE "RIB_VdrDelvModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvModNote_REC";
/
DROP TYPE "RIB_VdrDelvModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvModNote_TBL" AS TABLE OF "RIB_VdrDelvModNote_REC";
/
DROP TYPE "RIB_VdrDelvModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_id number(12),
  invoice_number varchar2(128),
  invoice_date date,
  invoice_cost number(12,4),
  carrier_name varchar2(128),
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [CORPORATE, THIRD_PARTY, NO_VALUE] (all lower-case)
  carrier_code varchar2(4),
  license_plate varchar2(128),
  source_address varchar2(1000),
  freight_id varchar2(128),
  bol_external_id varchar2(128),
  VdrDelvModNote_TBL "RIB_VdrDelvModNote_TBL",   -- Size of "RIB_VdrDelvModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
) return self as result
,constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, VdrDelvModNote_TBL "RIB_VdrDelvModNote_TBL"  -- Size of "RIB_VdrDelvModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvModVo') := "ns_name_VdrDelvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_id') := delivery_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_number') := invoice_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_date') := invoice_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'invoice_cost') := invoice_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'license_plate') := license_plate;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_address') := source_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_id') := freight_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bol_external_id') := bol_external_id;
  IF VdrDelvModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvModNote_TBL.';
    FOR INDX IN VdrDelvModNote_TBL.FIRST()..VdrDelvModNote_TBL.LAST() LOOP
      VdrDelvModNote_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
self.freight_id := freight_id;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
self.freight_id := freight_id;
self.bol_external_id := bol_external_id;
RETURN;
end;
constructor function "RIB_VdrDelvModVo_REC"
(
  rib_oid number
, delivery_id number
, invoice_number varchar2
, invoice_date date
, invoice_cost number
, carrier_name varchar2
, carrier_type varchar2
, carrier_code varchar2
, license_plate varchar2
, source_address varchar2
, freight_id varchar2
, bol_external_id varchar2
, VdrDelvModNote_TBL "RIB_VdrDelvModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_id := delivery_id;
self.invoice_number := invoice_number;
self.invoice_date := invoice_date;
self.invoice_cost := invoice_cost;
self.carrier_name := carrier_name;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.license_plate := license_plate;
self.source_address := source_address;
self.freight_id := freight_id;
self.bol_external_id := bol_external_id;
self.VdrDelvModNote_TBL := VdrDelvModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_VdrDelvModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvModVo') := "ns_name_VdrDelvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_VdrDelvModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
