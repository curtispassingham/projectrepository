DROP TYPE "RIB_XAllocDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocDtlRef_REC";
/
DROP TYPE "RIB_XAllocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocRef_REC";
/
DROP TYPE "RIB_XAllocDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XAllocDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XAllocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  to_loc number(10),
  to_loc_type varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XAllocDtlRef_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XAllocDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XAllocRef') := "ns_name_XAllocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
END appendNodeValues;
constructor function "RIB_XAllocDtlRef_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_XAllocDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocDtlRef_TBL" AS TABLE OF "RIB_XAllocDtlRef_REC";
/
DROP TYPE "RIB_XAllocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XAllocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XAllocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  alloc_no number(10),
  XAllocDtlRef_TBL "RIB_XAllocDtlRef_TBL",   -- Size of "RIB_XAllocDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XAllocRef_REC"
(
  rib_oid number
, alloc_no number
) return self as result
,constructor function "RIB_XAllocRef_REC"
(
  rib_oid number
, alloc_no number
, XAllocDtlRef_TBL "RIB_XAllocDtlRef_TBL"  -- Size of "RIB_XAllocDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XAllocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XAllocRef') := "ns_name_XAllocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_no') := alloc_no;
  IF XAllocDtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XAllocDtlRef_TBL.';
    FOR INDX IN XAllocDtlRef_TBL.FIRST()..XAllocDtlRef_TBL.LAST() LOOP
      XAllocDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'XAllocDtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XAllocRef_REC"
(
  rib_oid number
, alloc_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
RETURN;
end;
constructor function "RIB_XAllocRef_REC"
(
  rib_oid number
, alloc_no number
, XAllocDtlRef_TBL "RIB_XAllocDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.XAllocDtlRef_TBL := XAllocDtlRef_TBL;
RETURN;
end;
END;
/
