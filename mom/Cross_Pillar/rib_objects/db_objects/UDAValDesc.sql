DROP TYPE "RIB_UDAValDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_UDAValDesc_REC";
/
DROP TYPE "RIB_UDAValDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_UDAValDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_UDAValDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uda_id number(5),
  uda_value varchar2(30),
  uda_value_desc varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_UDAValDesc_REC"
(
  rib_oid number
, uda_id number
, uda_value varchar2
, uda_value_desc varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_UDAValDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_UDAValDesc') := "ns_name_UDAValDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_id') := uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value') := uda_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value_desc') := uda_value_desc;
END appendNodeValues;
constructor function "RIB_UDAValDesc_REC"
(
  rib_oid number
, uda_id number
, uda_value varchar2
, uda_value_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_value := uda_value;
self.uda_value_desc := uda_value_desc;
RETURN;
end;
END;
/
