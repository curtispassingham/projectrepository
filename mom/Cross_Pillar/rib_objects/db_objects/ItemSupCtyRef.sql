DROP TYPE "RIB_ItemSupCtyRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemSupCtyRef_REC";
/
DROP TYPE "RIB_ItemSupCtyRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemSupCtyRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemSupCtyRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  supplier varchar2(10),
  origin_country_id varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemSupCtyRef_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemSupCtyRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemSupCtyRef') := "ns_name_ItemSupCtyRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
END appendNodeValues;
constructor function "RIB_ItemSupCtyRef_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
RETURN;
end;
END;
/
