DROP TYPE "RIB_TsfDtlTckt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtlTckt_REC";
/
DROP TYPE "RIB_TsfDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtl_REC";
/
DROP TYPE "RIB_TsfDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDesc_REC";
/
DROP TYPE "RIB_TsfDtlTckt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDtlTckt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  comp_item varchar2(25),
  comp_price number(20,4),
  comp_selling_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
) return self as result
,constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
) return self as result
,constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
, comp_selling_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDtlTckt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDesc') := "ns_name_TsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_item') := comp_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_price') := comp_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'comp_selling_uom') := comp_selling_uom;
END appendNodeValues;
constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.comp_item := comp_item;
RETURN;
end;
constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.comp_item := comp_item;
self.comp_price := comp_price;
RETURN;
end;
constructor function "RIB_TsfDtlTckt_REC"
(
  rib_oid number
, comp_item varchar2
, comp_price number
, comp_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.comp_item := comp_item;
self.comp_price := comp_price;
self.comp_selling_uom := comp_selling_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDtlTckt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtlTckt_TBL" AS TABLE OF "RIB_TsfDtlTckt_REC";
/
DROP TYPE "RIB_TsfDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  tsf_qty number(12,4),
  price number(20,4),
  selling_uom varchar2(4),
  priority number(4),
  expedite_flag varchar2(1),
  store_ord_mult varchar2(1),
  tsf_po_link_no number(12),
  ticket_type_id varchar2(4),
  TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL",   -- Size of "RIB_TsfDtlTckt_TBL" is unbounded
  inv_status number(2),
  transaction_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"  -- Size of "RIB_TsfDtlTckt_TBL" is unbounded
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"  -- Size of "RIB_TsfDtlTckt_TBL" is unbounded
, inv_status number
) return self as result
,constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"  -- Size of "RIB_TsfDtlTckt_TBL" is unbounded
, inv_status number
, transaction_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDesc') := "ns_name_TsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_qty') := tsf_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'price') := price;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'expedite_flag') := expedite_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_ord_mult') := store_ord_mult;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_po_link_no') := tsf_po_link_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_type_id') := ticket_type_id;
  IF TsfDtlTckt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDtlTckt_TBL.';
    FOR INDX IN TsfDtlTckt_TBL.FIRST()..TsfDtlTckt_TBL.LAST() LOOP
      TsfDtlTckt_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDtlTckt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_status') := inv_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_uom') := transaction_uom;
END appendNodeValues;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
self.tsf_po_link_no := tsf_po_link_no;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
self.tsf_po_link_no := tsf_po_link_no;
self.ticket_type_id := ticket_type_id;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
self.tsf_po_link_no := tsf_po_link_no;
self.ticket_type_id := ticket_type_id;
self.TsfDtlTckt_TBL := TsfDtlTckt_TBL;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"
, inv_status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
self.tsf_po_link_no := tsf_po_link_no;
self.ticket_type_id := ticket_type_id;
self.TsfDtlTckt_TBL := TsfDtlTckt_TBL;
self.inv_status := inv_status;
RETURN;
end;
constructor function "RIB_TsfDtl_REC"
(
  rib_oid number
, item varchar2
, tsf_qty number
, price number
, selling_uom varchar2
, priority number
, expedite_flag varchar2
, store_ord_mult varchar2
, tsf_po_link_no number
, ticket_type_id varchar2
, TsfDtlTckt_TBL "RIB_TsfDtlTckt_TBL"
, inv_status number
, transaction_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.tsf_qty := tsf_qty;
self.price := price;
self.selling_uom := selling_uom;
self.priority := priority;
self.expedite_flag := expedite_flag;
self.store_ord_mult := store_ord_mult;
self.tsf_po_link_no := tsf_po_link_no;
self.ticket_type_id := ticket_type_id;
self.TsfDtlTckt_TBL := TsfDtlTckt_TBL;
self.inv_status := inv_status;
self.transaction_uom := transaction_uom;
RETURN;
end;
END;
/
DROP TYPE "RIB_TsfDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfDtl_TBL" AS TABLE OF "RIB_TsfDtl_REC";
/
DROP TYPE "RIB_TsfDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_no number(12),
  doc_type varchar2(1),
  physical_from_loc number(10),
  from_loc_type varchar2(1),
  from_store_type varchar2(1),
  from_stockholding_ind varchar2(1),
  from_loc varchar2(10),
  physical_to_loc number(10),
  to_loc_type varchar2(1),
  to_store_type varchar2(1),
  to_stockholding_ind varchar2(1),
  to_loc varchar2(10),
  tsf_type varchar2(6),
  pick_not_before_date date,
  pick_not_after_date date,
  order_type varchar2(9),
  priority number(4),
  break_by_distro varchar2(1),
  delivery_date date,
  cust_name varchar2(40),
  deliver_add1 varchar2(240),
  deliver_add2 varchar2(240),
  deliver_city varchar2(120),
  deliver_state varchar2(3),
  deliver_post varchar2(30),
  deliver_country_id varchar2(3),
  message varchar2(2000),
  TsfDtl_TBL "RIB_TsfDtl_TBL",   -- Size of "RIB_TsfDtl_TBL" is unbounded
  tsf_parent_no number(12),
  exp_dc_date date,
  approval_id varchar2(30),
  approval_date date,
  from_loc_tsf_entity number(10),
  to_loc_tsf_entity number(10),
  inv_type varchar2(6),
  tsf_status varchar2(1),
  not_after_date date,
  context_type varchar2(6),
  context_value varchar2(25),
  delivery_slot_id varchar2(15),
  delivery_slot_desc varchar2(240),
  cust_order_nbr varchar2(48),
  fulfill_order_nbr varchar2(48),
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  consumer_delivery_date date,
  consumer_delivery_time date,
  deliver_first_name varchar2(120),
  deliver_phonetic_first varchar2(120),
  deliver_last_name varchar2(120),
  deliver_phonetic_last varchar2(120),
  deliver_preferred_name varchar2(120),
  deliver_company_name varchar2(120),
  deliver_add3 varchar2(240),
  deliver_county varchar2(250),
  deliver_phone varchar2(20),
  bill_first_name varchar2(120),
  bill_phonetic_first varchar2(120),
  bill_last_name varchar2(120),
  bill_phonetic_last varchar2(120),
  bill_preferred_name varchar2(120),
  bill_company_name varchar2(120),
  bill_add1 varchar2(240),
  bill_add2 varchar2(240),
  bill_add3 varchar2(240),
  bill_county varchar2(250),
  bill_city varchar2(120),
  bill_country varchar2(3),
  bill_post varchar2(30),
  bill_state varchar2(3),
  bill_phone varchar2(20),
  partial_delivery_ind varchar2(1),
  consumer_direct varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfDesc_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, from_loc varchar2
, physical_to_loc number
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, to_loc varchar2
, tsf_type varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, priority number
, break_by_distro varchar2
, delivery_date date
, cust_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_post varchar2
, deliver_country_id varchar2
, message varchar2
, TsfDtl_TBL "RIB_TsfDtl_TBL"  -- Size of "RIB_TsfDtl_TBL" is unbounded
, tsf_parent_no number
, exp_dc_date date
, approval_id varchar2
, approval_date date
, from_loc_tsf_entity number
, to_loc_tsf_entity number
, inv_type varchar2
, tsf_status varchar2
, not_after_date date
, context_type varchar2
, context_value varchar2
, delivery_slot_id varchar2
, delivery_slot_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_phone varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_country varchar2
, bill_post varchar2
, bill_state varchar2
, bill_phone varchar2
, partial_delivery_ind varchar2
, consumer_direct varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfDesc') := "ns_name_TsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_no') := tsf_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_from_loc') := physical_from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_type') := from_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_store_type') := from_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_stockholding_ind') := from_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_to_loc') := physical_to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_store_type') := to_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_stockholding_ind') := to_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_type') := tsf_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_before_date') := pick_not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_not_after_date') := pick_not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'break_by_distro') := break_by_distro;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_name') := cust_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add1') := deliver_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add2') := deliver_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_city') := deliver_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_state') := deliver_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_post') := deliver_post;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_country_id') := deliver_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'message') := message;
  IF TsfDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfDtl_TBL.';
    FOR INDX IN TsfDtl_TBL.FIRST()..TsfDtl_TBL.LAST() LOOP
      TsfDtl_TBL(indx).appendNodeValues( i_prefix||indx||'TsfDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_parent_no') := tsf_parent_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'exp_dc_date') := exp_dc_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_id') := approval_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_date') := approval_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc_tsf_entity') := from_loc_tsf_entity;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_tsf_entity') := to_loc_tsf_entity;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_type') := inv_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_status') := tsf_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type') := context_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_slot_id') := delivery_slot_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_slot_desc') := delivery_slot_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_nbr') := fulfill_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_date') := consumer_delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_time') := consumer_delivery_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_first_name') := deliver_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phonetic_first') := deliver_phonetic_first;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_last_name') := deliver_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phonetic_last') := deliver_phonetic_last;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_preferred_name') := deliver_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_company_name') := deliver_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_add3') := deliver_add3;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_county') := deliver_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'deliver_phone') := deliver_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_first_name') := bill_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phonetic_first') := bill_phonetic_first;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_last_name') := bill_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phonetic_last') := bill_phonetic_last;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_preferred_name') := bill_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_company_name') := bill_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add1') := bill_add1;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add2') := bill_add2;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_add3') := bill_add3;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_county') := bill_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_city') := bill_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_country') := bill_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_post') := bill_post;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_state') := bill_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_phone') := bill_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_ind') := partial_delivery_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_direct') := consumer_direct;
END appendNodeValues;
constructor function "RIB_TsfDesc_REC"
(
  rib_oid number
, tsf_no number
, doc_type varchar2
, physical_from_loc number
, from_loc_type varchar2
, from_store_type varchar2
, from_stockholding_ind varchar2
, from_loc varchar2
, physical_to_loc number
, to_loc_type varchar2
, to_store_type varchar2
, to_stockholding_ind varchar2
, to_loc varchar2
, tsf_type varchar2
, pick_not_before_date date
, pick_not_after_date date
, order_type varchar2
, priority number
, break_by_distro varchar2
, delivery_date date
, cust_name varchar2
, deliver_add1 varchar2
, deliver_add2 varchar2
, deliver_city varchar2
, deliver_state varchar2
, deliver_post varchar2
, deliver_country_id varchar2
, message varchar2
, TsfDtl_TBL "RIB_TsfDtl_TBL"
, tsf_parent_no number
, exp_dc_date date
, approval_id varchar2
, approval_date date
, from_loc_tsf_entity number
, to_loc_tsf_entity number
, inv_type varchar2
, tsf_status varchar2
, not_after_date date
, context_type varchar2
, context_value varchar2
, delivery_slot_id varchar2
, delivery_slot_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, deliver_first_name varchar2
, deliver_phonetic_first varchar2
, deliver_last_name varchar2
, deliver_phonetic_last varchar2
, deliver_preferred_name varchar2
, deliver_company_name varchar2
, deliver_add3 varchar2
, deliver_county varchar2
, deliver_phone varchar2
, bill_first_name varchar2
, bill_phonetic_first varchar2
, bill_last_name varchar2
, bill_phonetic_last varchar2
, bill_preferred_name varchar2
, bill_company_name varchar2
, bill_add1 varchar2
, bill_add2 varchar2
, bill_add3 varchar2
, bill_county varchar2
, bill_city varchar2
, bill_country varchar2
, bill_post varchar2
, bill_state varchar2
, bill_phone varchar2
, partial_delivery_ind varchar2
, consumer_direct varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_no := tsf_no;
self.doc_type := doc_type;
self.physical_from_loc := physical_from_loc;
self.from_loc_type := from_loc_type;
self.from_store_type := from_store_type;
self.from_stockholding_ind := from_stockholding_ind;
self.from_loc := from_loc;
self.physical_to_loc := physical_to_loc;
self.to_loc_type := to_loc_type;
self.to_store_type := to_store_type;
self.to_stockholding_ind := to_stockholding_ind;
self.to_loc := to_loc;
self.tsf_type := tsf_type;
self.pick_not_before_date := pick_not_before_date;
self.pick_not_after_date := pick_not_after_date;
self.order_type := order_type;
self.priority := priority;
self.break_by_distro := break_by_distro;
self.delivery_date := delivery_date;
self.cust_name := cust_name;
self.deliver_add1 := deliver_add1;
self.deliver_add2 := deliver_add2;
self.deliver_city := deliver_city;
self.deliver_state := deliver_state;
self.deliver_post := deliver_post;
self.deliver_country_id := deliver_country_id;
self.message := message;
self.TsfDtl_TBL := TsfDtl_TBL;
self.tsf_parent_no := tsf_parent_no;
self.exp_dc_date := exp_dc_date;
self.approval_id := approval_id;
self.approval_date := approval_date;
self.from_loc_tsf_entity := from_loc_tsf_entity;
self.to_loc_tsf_entity := to_loc_tsf_entity;
self.inv_type := inv_type;
self.tsf_status := tsf_status;
self.not_after_date := not_after_date;
self.context_type := context_type;
self.context_value := context_value;
self.delivery_slot_id := delivery_slot_id;
self.delivery_slot_desc := delivery_slot_desc;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.consumer_delivery_date := consumer_delivery_date;
self.consumer_delivery_time := consumer_delivery_time;
self.deliver_first_name := deliver_first_name;
self.deliver_phonetic_first := deliver_phonetic_first;
self.deliver_last_name := deliver_last_name;
self.deliver_phonetic_last := deliver_phonetic_last;
self.deliver_preferred_name := deliver_preferred_name;
self.deliver_company_name := deliver_company_name;
self.deliver_add3 := deliver_add3;
self.deliver_county := deliver_county;
self.deliver_phone := deliver_phone;
self.bill_first_name := bill_first_name;
self.bill_phonetic_first := bill_phonetic_first;
self.bill_last_name := bill_last_name;
self.bill_phonetic_last := bill_phonetic_last;
self.bill_preferred_name := bill_preferred_name;
self.bill_company_name := bill_company_name;
self.bill_add1 := bill_add1;
self.bill_add2 := bill_add2;
self.bill_add3 := bill_add3;
self.bill_county := bill_county;
self.bill_city := bill_city;
self.bill_country := bill_country;
self.bill_post := bill_post;
self.bill_state := bill_state;
self.bill_phone := bill_phone;
self.partial_delivery_ind := partial_delivery_ind;
self.consumer_direct := consumer_direct;
RETURN;
end;
END;
/
