@@AddrRef.sql;
/
DROP TYPE "RIB_PartnerRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PartnerRef_REC";
/
DROP TYPE "RIB_AddrRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrRef_TBL" AS TABLE OF "RIB_AddrRef_REC";
/
DROP TYPE "RIB_PartnerRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PartnerRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PartnerRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  partner_type varchar2(6),
  partner_id varchar2(10),
  AddrRef_TBL "RIB_AddrRef_TBL",   -- Size of "RIB_AddrRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PartnerRef_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
) return self as result
,constructor function "RIB_PartnerRef_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, AddrRef_TBL "RIB_AddrRef_TBL"  -- Size of "RIB_AddrRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PartnerRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PartnerRef') := "ns_name_PartnerRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_type') := partner_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_id') := partner_id;
  IF AddrRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrRef_TBL.';
    FOR INDX IN AddrRef_TBL.FIRST()..AddrRef_TBL.LAST() LOOP
      AddrRef_TBL(indx).appendNodeValues( i_prefix||indx||'AddrRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PartnerRef_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.partner_type := partner_type;
self.partner_id := partner_id;
RETURN;
end;
constructor function "RIB_PartnerRef_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, AddrRef_TBL "RIB_AddrRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.partner_type := partner_type;
self.partner_id := partner_id;
self.AddrRef_TBL := AddrRef_TBL;
RETURN;
end;
END;
/
