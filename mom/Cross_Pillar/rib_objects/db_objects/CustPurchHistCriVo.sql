DROP TYPE "RIB_CustPurchHistCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustPurchHistCriVo_REC";
/
DROP TYPE "RIB_CustPurchHistCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustPurchHistCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustPurchHistCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  search_loc_id varchar2(40),
  search_loc_type varchar2(1), -- search_loc_type is enumeration field, valid values are [S, O] (all lower-case)
  customer_id varchar2(14),
  user_id varchar2(30),
  start_date date,
  end_date date,
  states_to_fetch "RIB_states_to_fetch_TBL",   -- Size of "RIB_states_to_fetch_TBL" is 10
  line_item_population varchar2(5), -- line_item_population is enumeration field, valid values are [NONE, FIRST, ALL] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
) return self as result
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
) return self as result
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
) return self as result
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
) return self as result
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
, states_to_fetch "RIB_states_to_fetch_TBL"  -- Size of "RIB_states_to_fetch_TBL" is 10
) return self as result
,constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
, states_to_fetch "RIB_states_to_fetch_TBL"  -- Size of "RIB_states_to_fetch_TBL" is 10
, line_item_population varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustPurchHistCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustPurchHistCriVo') := "ns_name_CustPurchHistCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'search_loc_id') := search_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'search_loc_type') := search_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_id') := customer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  IF states_to_fetch IS NOT NULL THEN
    FOR INDX IN states_to_fetch.FIRST()..states_to_fetch.LAST() LOOP
      l_new_pre :=i_prefix||indx||'states_to_fetch'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'states_to_fetch'||'.'):=states_to_fetch(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_population') := line_item_population;
END appendNodeValues;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
RETURN;
end;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
self.user_id := user_id;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
self.user_id := user_id;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
, states_to_fetch "RIB_states_to_fetch_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
self.user_id := user_id;
self.start_date := start_date;
self.end_date := end_date;
self.states_to_fetch := states_to_fetch;
RETURN;
end;
constructor function "RIB_CustPurchHistCriVo_REC"
(
  rib_oid number
, search_loc_id varchar2
, search_loc_type varchar2
, customer_id varchar2
, user_id varchar2
, start_date date
, end_date date
, states_to_fetch "RIB_states_to_fetch_TBL"
, line_item_population varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.customer_id := customer_id;
self.user_id := user_id;
self.start_date := start_date;
self.end_date := end_date;
self.states_to_fetch := states_to_fetch;
self.line_item_population := line_item_population;
RETURN;
end;
END;
/
DROP TYPE "RIB_states_to_fetch_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_states_to_fetch_TBL" AS TABLE OF varchar2(20);
/
