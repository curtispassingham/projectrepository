
DROP TYPE "RIB_ItemSupCtyDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemSupCtyDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemSupCtyDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  supplier varchar2(10),
  origin_country_id varchar2(3),
  primary_supp_ind varchar2(3),
  primary_country_ind varchar2(1),
  unit_cost number(20,4),
  lead_time number(4),
  pickup_lead_time number(4),
  supp_pack_size number(12,4),
  inner_pack_size number(12,4),
  round_lvl varchar2(6),
  min_order_qty number(12,4),
  max_order_qty number(12,4),
  packing_method varchar2(6),
  default_uop varchar2(6),
  ti number(12,4),
  hi number(12,4),
  cost_uom varchar2(4),
  tolerance_type varchar2(6),
  max_tolerance number(12,4),
  min_tolerance number(12,4),
  ExtOfItemSupCtyDesc "RIB_ExtOfItemSupCtyDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
) return self as result
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
) return self as result
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
) return self as result
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
) return self as result
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
, min_tolerance number
) return self as result
,constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
, min_tolerance number
, ExtOfItemSupCtyDesc "RIB_ExtOfItemSupCtyDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemSupCtyDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemSupCtyDesc') := "ns_name_ItemSupCtyDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supp_ind') := primary_supp_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_country_ind') := primary_country_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'lead_time') := lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_lead_time') := pickup_lead_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_pack_size') := supp_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'inner_pack_size') := inner_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_lvl') := round_lvl;
  rib_obj_util.g_RIB_element_values(i_prefix||'min_order_qty') := min_order_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_order_qty') := max_order_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'packing_method') := packing_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_uop') := default_uop;
  rib_obj_util.g_RIB_element_values(i_prefix||'ti') := ti;
  rib_obj_util.g_RIB_element_values(i_prefix||'hi') := hi;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_uom') := cost_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tolerance_type') := tolerance_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_tolerance') := max_tolerance;
  rib_obj_util.g_RIB_element_values(i_prefix||'min_tolerance') := min_tolerance;
  l_new_pre :=i_prefix||'ExtOfItemSupCtyDesc.';
  ExtOfItemSupCtyDesc.appendNodeValues( i_prefix||'ExtOfItemSupCtyDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
RETURN;
end;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
self.cost_uom := cost_uom;
RETURN;
end;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
RETURN;
end;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.max_tolerance := max_tolerance;
RETURN;
end;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
, min_tolerance number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.max_tolerance := max_tolerance;
self.min_tolerance := min_tolerance;
RETURN;
end;
constructor function "RIB_ItemSupCtyDesc_REC"
(
  rib_oid number
, item varchar2
, supplier varchar2
, origin_country_id varchar2
, primary_supp_ind varchar2
, primary_country_ind varchar2
, unit_cost number
, lead_time number
, pickup_lead_time number
, supp_pack_size number
, inner_pack_size number
, round_lvl varchar2
, min_order_qty number
, max_order_qty number
, packing_method varchar2
, default_uop varchar2
, ti number
, hi number
, cost_uom varchar2
, tolerance_type varchar2
, max_tolerance number
, min_tolerance number
, ExtOfItemSupCtyDesc "RIB_ExtOfItemSupCtyDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.primary_supp_ind := primary_supp_ind;
self.primary_country_ind := primary_country_ind;
self.unit_cost := unit_cost;
self.lead_time := lead_time;
self.pickup_lead_time := pickup_lead_time;
self.supp_pack_size := supp_pack_size;
self.inner_pack_size := inner_pack_size;
self.round_lvl := round_lvl;
self.min_order_qty := min_order_qty;
self.max_order_qty := max_order_qty;
self.packing_method := packing_method;
self.default_uop := default_uop;
self.ti := ti;
self.hi := hi;
self.cost_uom := cost_uom;
self.tolerance_type := tolerance_type;
self.max_tolerance := max_tolerance;
self.min_tolerance := min_tolerance;
self.ExtOfItemSupCtyDesc := ExtOfItemSupCtyDesc;
RETURN;
end;
END;
/
