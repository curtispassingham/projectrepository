DROP TYPE "RIB_ForpModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpModVo_REC";
/
DROP TYPE "RIB_ForpItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpItmMod_REC";
/
DROP TYPE "RIB_ForpItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpItmMod_TBL" AS TABLE OF "RIB_ForpItmMod_REC";
/
DROP TYPE "RIB_ForpModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ForpModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ForpModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reverse_pick_id number(12),
  ForpItmMod_TBL "RIB_ForpItmMod_TBL",   -- Size of "RIB_ForpItmMod_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ForpModVo_REC"
(
  rib_oid number
, reverse_pick_id number
) return self as result
,constructor function "RIB_ForpModVo_REC"
(
  rib_oid number
, reverse_pick_id number
, ForpItmMod_TBL "RIB_ForpItmMod_TBL"  -- Size of "RIB_ForpItmMod_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ForpModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ForpModVo') := "ns_name_ForpModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'reverse_pick_id') := reverse_pick_id;
  IF ForpItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ForpItmMod_TBL.';
    FOR INDX IN ForpItmMod_TBL.FIRST()..ForpItmMod_TBL.LAST() LOOP
      ForpItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'ForpItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ForpModVo_REC"
(
  rib_oid number
, reverse_pick_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.reverse_pick_id := reverse_pick_id;
RETURN;
end;
constructor function "RIB_ForpModVo_REC"
(
  rib_oid number
, reverse_pick_id number
, ForpItmMod_TBL "RIB_ForpItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.reverse_pick_id := reverse_pick_id;
self.ForpItmMod_TBL := ForpItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ForpItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ForpItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ForpModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  fulfillment_order_line_id number(12),
  quantity number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ForpItmMod_REC"
(
  rib_oid number
, fulfillment_order_line_id number
, quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ForpItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ForpModVo') := "ns_name_ForpModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_line_id') := fulfillment_order_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
END appendNodeValues;
constructor function "RIB_ForpItmMod_REC"
(
  rib_oid number
, fulfillment_order_line_id number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfillment_order_line_id := fulfillment_order_line_id;
self.quantity := quantity;
RETURN;
end;
END;
/
