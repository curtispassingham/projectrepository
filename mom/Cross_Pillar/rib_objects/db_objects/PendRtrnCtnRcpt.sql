DROP TYPE "RIB_PendRtrnCtnRcpt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PendRtrnCtnRcpt_REC";
/
DROP TYPE "RIB_PendRtrnCtnRcpt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PendRtrnCtnRcpt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PendRtrnCtnRcpt" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_wh number(10),
  rma_nbr varchar2(20),
  cust_order_nbr varchar2(48),
  receipt_date date,
  container_id varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PendRtrnCtnRcpt_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, cust_order_nbr varchar2
, receipt_date date
, container_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PendRtrnCtnRcpt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PendRtrnCtnRcpt') := "ns_name_PendRtrnCtnRcpt";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_wh') := physical_wh;
  rib_obj_util.g_RIB_element_values(i_prefix||'rma_nbr') := rma_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
END appendNodeValues;
constructor function "RIB_PendRtrnCtnRcpt_REC"
(
  rib_oid number
, physical_wh number
, rma_nbr varchar2
, cust_order_nbr varchar2
, receipt_date date
, container_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_wh := physical_wh;
self.rma_nbr := rma_nbr;
self.cust_order_nbr := cust_order_nbr;
self.receipt_date := receipt_date;
self.container_id := container_id;
RETURN;
end;
END;
/
