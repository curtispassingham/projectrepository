DROP TYPE "RIB_DSDDtlUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDtlUin_REC";
/
DROP TYPE "RIB_DSDDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDtl_REC";
/
DROP TYPE "RIB_DSDNonMerch_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDNonMerch_REC";
/
DROP TYPE "RIB_DSDReceipt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDReceipt_REC";
/
DROP TYPE "RIB_DSDReceiptDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDReceiptDesc_REC";
/
DROP TYPE "RIB_DSDDtlUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDDtlUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDDtlUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDReceiptDesc') := "ns_name_DSDReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_DSDDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_DSDDtlUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDtlUin_TBL" AS TABLE OF "RIB_DSDDtlUin_REC";
/
DROP TYPE "RIB_DSDDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  qty_received number(12,4),
  unit_cost number(20,4),
  weight number(12,4),
  weight_uom varchar2(4),
  DSDDtlUin_TBL "RIB_DSDDtlUin_TBL",   -- Size of "RIB_DSDDtlUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
) return self as result
,constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
) return self as result
,constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
) return self as result
,constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
, weight_uom varchar2
, DSDDtlUin_TBL "RIB_DSDDtlUin_TBL"  -- Size of "RIB_DSDDtlUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDReceiptDesc') := "ns_name_DSDReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_received') := qty_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  IF DSDDtlUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DSDDtlUin_TBL.';
    FOR INDX IN DSDDtlUin_TBL.FIRST()..DSDDtlUin_TBL.LAST() LOOP
      DSDDtlUin_TBL(indx).appendNodeValues( i_prefix||indx||'DSDDtlUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_received := qty_received;
RETURN;
end;
constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_received := qty_received;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_received := qty_received;
self.unit_cost := unit_cost;
self.weight := weight;
RETURN;
end;
constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_received := qty_received;
self.unit_cost := unit_cost;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_DSDDtl_REC"
(
  rib_oid number
, item varchar2
, qty_received number
, unit_cost number
, weight number
, weight_uom varchar2
, DSDDtlUin_TBL "RIB_DSDDtlUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.qty_received := qty_received;
self.unit_cost := unit_cost;
self.weight := weight;
self.weight_uom := weight_uom;
self.DSDDtlUin_TBL := DSDDtlUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_DSDNonMerch_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDNonMerch_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  non_merch_code varchar2(6),
  non_merch_amt number(20),
  vat_code varchar2(6),
  service_perf_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDNonMerch_REC"
(
  rib_oid number
, non_merch_code varchar2
, non_merch_amt number
, vat_code varchar2
, service_perf_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDNonMerch_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDReceiptDesc') := "ns_name_DSDReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'non_merch_code') := non_merch_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'non_merch_amt') := non_merch_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_code') := vat_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'service_perf_ind') := service_perf_ind;
END appendNodeValues;
constructor function "RIB_DSDNonMerch_REC"
(
  rib_oid number
, non_merch_code varchar2
, non_merch_amt number
, vat_code varchar2
, service_perf_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.non_merch_code := non_merch_code;
self.non_merch_amt := non_merch_amt;
self.vat_code := vat_code;
self.service_perf_ind := service_perf_ind;
RETURN;
end;
END;
/
DROP TYPE "RIB_DSDDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDDtl_TBL" AS TABLE OF "RIB_DSDDtl_REC";
/
DROP TYPE "RIB_DSDNonMerch_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDNonMerch_TBL" AS TABLE OF "RIB_DSDNonMerch_REC";
/
DROP TYPE "RIB_DSDReceipt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDReceipt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier varchar2(10),
  origin_country_id varchar2(3),
  store number(10),
  dept number(4),
  currency_code varchar2(3),
  paid_ind varchar2(1),
  ext_ref_no varchar2(30),
  proof_of_delivery_no varchar2(30),
  payment_ref_no varchar2(16),
  payment_date date,
  invc_ind varchar2(1),
  deals_ind varchar2(1),
  DSDDtl_TBL "RIB_DSDDtl_TBL",   -- Size of "RIB_DSDDtl_TBL" is unbounded
  DSDNonMerch_TBL "RIB_DSDNonMerch_TBL",   -- Size of "RIB_DSDNonMerch_TBL" is unbounded
  ext_receipt_no varchar2(17),
  receipt_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
) return self as result
,constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"  -- Size of "RIB_DSDDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"  -- Size of "RIB_DSDDtl_TBL" is unbounded
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"  -- Size of "RIB_DSDNonMerch_TBL" is unbounded
) return self as result
,constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"  -- Size of "RIB_DSDDtl_TBL" is unbounded
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"  -- Size of "RIB_DSDNonMerch_TBL" is unbounded
, ext_receipt_no varchar2
) return self as result
,constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"  -- Size of "RIB_DSDDtl_TBL" is unbounded
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"  -- Size of "RIB_DSDNonMerch_TBL" is unbounded
, ext_receipt_no varchar2
, receipt_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDReceipt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDReceiptDesc') := "ns_name_DSDReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_ind') := paid_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_ref_no') := ext_ref_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'proof_of_delivery_no') := proof_of_delivery_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_ref_no') := payment_ref_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_date') := payment_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_ind') := invc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'deals_ind') := deals_ind;
  IF DSDDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DSDDtl_TBL.';
    FOR INDX IN DSDDtl_TBL.FIRST()..DSDDtl_TBL.LAST() LOOP
      DSDDtl_TBL(indx).appendNodeValues( i_prefix||indx||'DSDDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF DSDNonMerch_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'DSDNonMerch_TBL.';
    FOR INDX IN DSDNonMerch_TBL.FIRST()..DSDNonMerch_TBL.LAST() LOOP
      DSDNonMerch_TBL(indx).appendNodeValues( i_prefix||indx||'DSDNonMerch_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_receipt_no') := ext_receipt_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_date') := receipt_date;
END appendNodeValues;
constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.invc_ind := invc_ind;
self.deals_ind := deals_ind;
RETURN;
end;
constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.invc_ind := invc_ind;
self.deals_ind := deals_ind;
self.DSDDtl_TBL := DSDDtl_TBL;
RETURN;
end;
constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.invc_ind := invc_ind;
self.deals_ind := deals_ind;
self.DSDDtl_TBL := DSDDtl_TBL;
self.DSDNonMerch_TBL := DSDNonMerch_TBL;
RETURN;
end;
constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"
, ext_receipt_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.invc_ind := invc_ind;
self.deals_ind := deals_ind;
self.DSDDtl_TBL := DSDDtl_TBL;
self.DSDNonMerch_TBL := DSDNonMerch_TBL;
self.ext_receipt_no := ext_receipt_no;
RETURN;
end;
constructor function "RIB_DSDReceipt_REC"
(
  rib_oid number
, supplier varchar2
, origin_country_id varchar2
, store number
, dept number
, currency_code varchar2
, paid_ind varchar2
, ext_ref_no varchar2
, proof_of_delivery_no varchar2
, payment_ref_no varchar2
, payment_date date
, invc_ind varchar2
, deals_ind varchar2
, DSDDtl_TBL "RIB_DSDDtl_TBL"
, DSDNonMerch_TBL "RIB_DSDNonMerch_TBL"
, ext_receipt_no varchar2
, receipt_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.origin_country_id := origin_country_id;
self.store := store;
self.dept := dept;
self.currency_code := currency_code;
self.paid_ind := paid_ind;
self.ext_ref_no := ext_ref_no;
self.proof_of_delivery_no := proof_of_delivery_no;
self.payment_ref_no := payment_ref_no;
self.payment_date := payment_date;
self.invc_ind := invc_ind;
self.deals_ind := deals_ind;
self.DSDDtl_TBL := DSDDtl_TBL;
self.DSDNonMerch_TBL := DSDNonMerch_TBL;
self.ext_receipt_no := ext_receipt_no;
self.receipt_date := receipt_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_DSDReceipt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_DSDReceipt_TBL" AS TABLE OF "RIB_DSDReceipt_REC";
/
DROP TYPE "RIB_DSDReceiptDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DSDReceiptDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DSDReceiptDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  DSDReceipt_TBL "RIB_DSDReceipt_TBL",   -- Size of "RIB_DSDReceipt_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DSDReceiptDesc_REC"
(
  rib_oid number
, DSDReceipt_TBL "RIB_DSDReceipt_TBL"  -- Size of "RIB_DSDReceipt_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DSDReceiptDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DSDReceiptDesc') := "ns_name_DSDReceiptDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'DSDReceipt_TBL.';
  FOR INDX IN DSDReceipt_TBL.FIRST()..DSDReceipt_TBL.LAST() LOOP
    DSDReceipt_TBL(indx).appendNodeValues( i_prefix||indx||'DSDReceipt_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_DSDReceiptDesc_REC"
(
  rib_oid number
, DSDReceipt_TBL "RIB_DSDReceipt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.DSDReceipt_TBL := DSDReceipt_TBL;
RETURN;
end;
END;
/
