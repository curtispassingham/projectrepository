@@CustPurchItmHdrDesc.sql;
/
DROP TYPE "RIB_CustPurchItmHdrColDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustPurchItmHdrColDesc_REC";
/
DROP TYPE "RIB_CustPurchItmHdrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustPurchItmHdrDesc_TBL" AS TABLE OF "RIB_CustPurchItmHdrDesc_REC";
/
DROP TYPE "RIB_CustPurchItmHdrColDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustPurchItmHdrColDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustPurchItmHdrColDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustPurchItmHdrDesc_TBL "RIB_CustPurchItmHdrDesc_TBL",   -- Size of "RIB_CustPurchItmHdrDesc_TBL" is unbounded
  collection_size number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustPurchItmHdrColDesc_REC"
(
  rib_oid number
, CustPurchItmHdrDesc_TBL "RIB_CustPurchItmHdrDesc_TBL"  -- Size of "RIB_CustPurchItmHdrDesc_TBL" is unbounded
, collection_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustPurchItmHdrColDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustPurchItmHdrColDesc') := "ns_name_CustPurchItmHdrColDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'CustPurchItmHdrDesc_TBL.';
  FOR INDX IN CustPurchItmHdrDesc_TBL.FIRST()..CustPurchItmHdrDesc_TBL.LAST() LOOP
    CustPurchItmHdrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'CustPurchItmHdrDesc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'collection_size') := collection_size;
END appendNodeValues;
constructor function "RIB_CustPurchItmHdrColDesc_REC"
(
  rib_oid number
, CustPurchItmHdrDesc_TBL "RIB_CustPurchItmHdrDesc_TBL"
, collection_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustPurchItmHdrDesc_TBL := CustPurchItmHdrDesc_TBL;
self.collection_size := collection_size;
RETURN;
end;
END;
/
