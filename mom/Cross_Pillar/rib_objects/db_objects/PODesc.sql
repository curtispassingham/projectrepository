DROP TYPE "RIB_POVirtualDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_POVirtualDtl_REC";
/
DROP TYPE "RIB_PODtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PODtl_REC";
/
DROP TYPE "RIB_PODesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PODesc_REC";
/
DROP TYPE "RIB_POVirtualDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_POVirtualDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location_type varchar2(1),
  location number(10),
  qty_ordered number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_POVirtualDtl_REC"
(
  rib_oid number
, location_type varchar2
, location number
) return self as result
,constructor function "RIB_POVirtualDtl_REC"
(
  rib_oid number
, location_type varchar2
, location number
, qty_ordered number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_POVirtualDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PODesc') := "ns_name_PODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location_type') := location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_ordered') := qty_ordered;
END appendNodeValues;
constructor function "RIB_POVirtualDtl_REC"
(
  rib_oid number
, location_type varchar2
, location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.location_type := location_type;
self.location := location;
RETURN;
end;
constructor function "RIB_POVirtualDtl_REC"
(
  rib_oid number
, location_type varchar2
, location number
, qty_ordered number
) return self as result is
begin
self.rib_oid := rib_oid;
self.location_type := location_type;
self.location := location;
self.qty_ordered := qty_ordered;
RETURN;
end;
END;
/
DROP TYPE "RIB_POVirtualDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_POVirtualDtl_TBL" AS TABLE OF "RIB_POVirtualDtl_REC";
/
DROP TYPE "RIB_PODtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PODtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  ref_item varchar2(25),
  physical_location_type varchar2(1),
  physical_location number(10),
  physical_store_type varchar2(1),
  physical_stockholding_ind varchar2(1),
  physical_qty_ordered number(12,4),
  transaction_uom varchar2(4),
  unit_cost number(20,4),
  origin_country_id varchar2(3),
  supp_pack_size number(12,4),
  earliest_ship_date date,
  latest_ship_date date,
  pickup_loc varchar2(250),
  pickup_no varchar2(25),
  packing_method varchar2(6),
  round_lvl varchar2(6),
  door_ind varchar2(1),
  priority_level number(1),
  new_item varchar2(1),
  quarantine varchar2(1),
  rcvd_unit_qty number(12,4),
  tsf_po_link_id number(12),
  POVirtualDtl_TBL "RIB_POVirtualDtl_TBL",   -- Size of "RIB_POVirtualDtl_TBL" is unbounded
  cost_source varchar2(4),
  est_in_stock_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"  -- Size of "RIB_POVirtualDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"  -- Size of "RIB_POVirtualDtl_TBL" is unbounded
, cost_source varchar2
) return self as result
,constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"  -- Size of "RIB_POVirtualDtl_TBL" is unbounded
, cost_source varchar2
, est_in_stock_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PODtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PODesc') := "ns_name_PODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_item') := ref_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_location_type') := physical_location_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_location') := physical_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_store_type') := physical_store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_stockholding_ind') := physical_stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_qty_ordered') := physical_qty_ordered;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_uom') := transaction_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supp_pack_size') := supp_pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'earliest_ship_date') := earliest_ship_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'latest_ship_date') := latest_ship_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_loc') := pickup_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_no') := pickup_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'packing_method') := packing_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'round_lvl') := round_lvl;
  rib_obj_util.g_RIB_element_values(i_prefix||'door_ind') := door_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority_level') := priority_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_item') := new_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'quarantine') := quarantine;
  rib_obj_util.g_RIB_element_values(i_prefix||'rcvd_unit_qty') := rcvd_unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_po_link_id') := tsf_po_link_id;
  IF POVirtualDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'POVirtualDtl_TBL.';
    FOR INDX IN POVirtualDtl_TBL.FIRST()..POVirtualDtl_TBL.LAST() LOOP
      POVirtualDtl_TBL(indx).appendNodeValues( i_prefix||indx||'POVirtualDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'cost_source') := cost_source;
  rib_obj_util.g_RIB_element_values(i_prefix||'est_in_stock_date') := est_in_stock_date;
END appendNodeValues;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
self.rcvd_unit_qty := rcvd_unit_qty;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
self.rcvd_unit_qty := rcvd_unit_qty;
self.tsf_po_link_id := tsf_po_link_id;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
self.rcvd_unit_qty := rcvd_unit_qty;
self.tsf_po_link_id := tsf_po_link_id;
self.POVirtualDtl_TBL := POVirtualDtl_TBL;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"
, cost_source varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
self.rcvd_unit_qty := rcvd_unit_qty;
self.tsf_po_link_id := tsf_po_link_id;
self.POVirtualDtl_TBL := POVirtualDtl_TBL;
self.cost_source := cost_source;
RETURN;
end;
constructor function "RIB_PODtl_REC"
(
  rib_oid number
, item varchar2
, ref_item varchar2
, physical_location_type varchar2
, physical_location number
, physical_store_type varchar2
, physical_stockholding_ind varchar2
, physical_qty_ordered number
, transaction_uom varchar2
, unit_cost number
, origin_country_id varchar2
, supp_pack_size number
, earliest_ship_date date
, latest_ship_date date
, pickup_loc varchar2
, pickup_no varchar2
, packing_method varchar2
, round_lvl varchar2
, door_ind varchar2
, priority_level number
, new_item varchar2
, quarantine varchar2
, rcvd_unit_qty number
, tsf_po_link_id number
, POVirtualDtl_TBL "RIB_POVirtualDtl_TBL"
, cost_source varchar2
, est_in_stock_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ref_item := ref_item;
self.physical_location_type := physical_location_type;
self.physical_location := physical_location;
self.physical_store_type := physical_store_type;
self.physical_stockholding_ind := physical_stockholding_ind;
self.physical_qty_ordered := physical_qty_ordered;
self.transaction_uom := transaction_uom;
self.unit_cost := unit_cost;
self.origin_country_id := origin_country_id;
self.supp_pack_size := supp_pack_size;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.packing_method := packing_method;
self.round_lvl := round_lvl;
self.door_ind := door_ind;
self.priority_level := priority_level;
self.new_item := new_item;
self.quarantine := quarantine;
self.rcvd_unit_qty := rcvd_unit_qty;
self.tsf_po_link_id := tsf_po_link_id;
self.POVirtualDtl_TBL := POVirtualDtl_TBL;
self.cost_source := cost_source;
self.est_in_stock_date := est_in_stock_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_PODtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PODtl_TBL" AS TABLE OF "RIB_PODtl_REC";
/
DROP TYPE "RIB_PODesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PODesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PODesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  doc_type varchar2(1),
  order_no varchar2(12),
  order_type varchar2(9),
  order_type_desc varchar2(250),
  dept number(4),
  dept_name varchar2(120),
  buyer number(4),
  buyer_name varchar2(120),
  supplier varchar2(10),
  promotion number(10),
  prom_desc varchar2(160),
  qc_ind varchar2(1),
  not_before_date date,
  not_after_date date,
  otb_eow_date date,
  earliest_ship_date date,
  latest_ship_date date,
  close_date date,
  terms varchar2(15),
  terms_code varchar2(50),
  freight_terms varchar2(30),
  cust_order varchar2(1),
  payment_method varchar2(6),
  payment_method_desc varchar2(250),
  backhaul_type varchar2(6),
  backhaul_type_desc varchar2(250),
  backhaul_allowance number(20,4),
  ship_method varchar2(6),
  ship_method_desc varchar2(250),
  purchase_type varchar2(6),
  purchase_type_desc varchar2(250),
  status varchar2(1),
  ship_pay_method varchar2(2),
  ship_pay_method_desc varchar2(250),
  fob_trans_res varchar2(2),
  fob_trans_res_code_desc varchar2(250),
  fob_trans_res_desc varchar2(250),
  fob_title_pass varchar2(2),
  fob_title_pass_code_desc varchar2(250),
  fob_title_pass_desc varchar2(250),
  vendor_order_no varchar2(15),
  exchange_rate number(20,10),
  factory varchar2(10),
  factory_desc varchar2(240),
  agent varchar2(10),
  agent_desc varchar2(240),
  discharge_port varchar2(5),
  discharge_port_desc varchar2(150),
  lading_port varchar2(5),
  lading_port_desc varchar2(150),
  freight_contract_no varchar2(10),
  po_type varchar2(4),
  po_type_desc varchar2(120),
  pre_mark_ind varchar2(1),
  currency_code varchar2(3),
  contract_no number(6),
  pickup_loc varchar2(250),
  pickup_no varchar2(25),
  pickup_date date,
  app_datetime date,
  comment_desc varchar2(2000),
  cust_order_nbr varchar2(48),
  fulfill_order_nbr varchar2(48),
  PODtl_TBL "RIB_PODtl_TBL",   -- Size of "RIB_PODtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
) return self as result
,constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, PODtl_TBL "RIB_PODtl_TBL"  -- Size of "RIB_PODtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PODesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PODesc') := "ns_name_PODesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'doc_type') := doc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type_desc') := order_type_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept_name') := dept_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'buyer') := buyer;
  rib_obj_util.g_RIB_element_values(i_prefix||'buyer_name') := buyer_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion') := promotion;
  rib_obj_util.g_RIB_element_values(i_prefix||'prom_desc') := prom_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'qc_ind') := qc_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_before_date') := not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'otb_eow_date') := otb_eow_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'earliest_ship_date') := earliest_ship_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'latest_ship_date') := latest_ship_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'close_date') := close_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms_code') := terms_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_terms') := freight_terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order') := cust_order;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_method') := payment_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'payment_method_desc') := payment_method_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'backhaul_type') := backhaul_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'backhaul_type_desc') := backhaul_type_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'backhaul_allowance') := backhaul_allowance;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_method') := ship_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_method_desc') := ship_method_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type') := purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type_desc') := purchase_type_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_pay_method') := ship_pay_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_pay_method_desc') := ship_pay_method_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_trans_res') := fob_trans_res;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_trans_res_code_desc') := fob_trans_res_code_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_trans_res_desc') := fob_trans_res_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_title_pass') := fob_title_pass;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_title_pass_code_desc') := fob_title_pass_code_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'fob_title_pass_desc') := fob_title_pass_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendor_order_no') := vendor_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'exchange_rate') := exchange_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'factory') := factory;
  rib_obj_util.g_RIB_element_values(i_prefix||'factory_desc') := factory_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'agent') := agent;
  rib_obj_util.g_RIB_element_values(i_prefix||'agent_desc') := agent_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'discharge_port') := discharge_port;
  rib_obj_util.g_RIB_element_values(i_prefix||'discharge_port_desc') := discharge_port_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'lading_port') := lading_port;
  rib_obj_util.g_RIB_element_values(i_prefix||'lading_port_desc') := lading_port_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_contract_no') := freight_contract_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_type') := po_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'po_type_desc') := po_type_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'pre_mark_ind') := pre_mark_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'contract_no') := contract_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_loc') := pickup_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_no') := pickup_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'pickup_date') := pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'app_datetime') := app_datetime;
  rib_obj_util.g_RIB_element_values(i_prefix||'comment_desc') := comment_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_nbr') := cust_order_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_nbr') := fulfill_order_nbr;
  IF PODtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PODtl_TBL.';
    FOR INDX IN PODtl_TBL.FIRST()..PODtl_TBL.LAST() LOOP
      PODtl_TBL(indx).appendNodeValues( i_prefix||indx||'PODtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
self.app_datetime := app_datetime;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
self.app_datetime := app_datetime;
self.comment_desc := comment_desc;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
self.app_datetime := app_datetime;
self.comment_desc := comment_desc;
self.cust_order_nbr := cust_order_nbr;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
self.app_datetime := app_datetime;
self.comment_desc := comment_desc;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
RETURN;
end;
constructor function "RIB_PODesc_REC"
(
  rib_oid number
, doc_type varchar2
, order_no varchar2
, order_type varchar2
, order_type_desc varchar2
, dept number
, dept_name varchar2
, buyer number
, buyer_name varchar2
, supplier varchar2
, promotion number
, prom_desc varchar2
, qc_ind varchar2
, not_before_date date
, not_after_date date
, otb_eow_date date
, earliest_ship_date date
, latest_ship_date date
, close_date date
, terms varchar2
, terms_code varchar2
, freight_terms varchar2
, cust_order varchar2
, payment_method varchar2
, payment_method_desc varchar2
, backhaul_type varchar2
, backhaul_type_desc varchar2
, backhaul_allowance number
, ship_method varchar2
, ship_method_desc varchar2
, purchase_type varchar2
, purchase_type_desc varchar2
, status varchar2
, ship_pay_method varchar2
, ship_pay_method_desc varchar2
, fob_trans_res varchar2
, fob_trans_res_code_desc varchar2
, fob_trans_res_desc varchar2
, fob_title_pass varchar2
, fob_title_pass_code_desc varchar2
, fob_title_pass_desc varchar2
, vendor_order_no varchar2
, exchange_rate number
, factory varchar2
, factory_desc varchar2
, agent varchar2
, agent_desc varchar2
, discharge_port varchar2
, discharge_port_desc varchar2
, lading_port varchar2
, lading_port_desc varchar2
, freight_contract_no varchar2
, po_type varchar2
, po_type_desc varchar2
, pre_mark_ind varchar2
, currency_code varchar2
, contract_no number
, pickup_loc varchar2
, pickup_no varchar2
, pickup_date date
, app_datetime date
, comment_desc varchar2
, cust_order_nbr varchar2
, fulfill_order_nbr varchar2
, PODtl_TBL "RIB_PODtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.doc_type := doc_type;
self.order_no := order_no;
self.order_type := order_type;
self.order_type_desc := order_type_desc;
self.dept := dept;
self.dept_name := dept_name;
self.buyer := buyer;
self.buyer_name := buyer_name;
self.supplier := supplier;
self.promotion := promotion;
self.prom_desc := prom_desc;
self.qc_ind := qc_ind;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.otb_eow_date := otb_eow_date;
self.earliest_ship_date := earliest_ship_date;
self.latest_ship_date := latest_ship_date;
self.close_date := close_date;
self.terms := terms;
self.terms_code := terms_code;
self.freight_terms := freight_terms;
self.cust_order := cust_order;
self.payment_method := payment_method;
self.payment_method_desc := payment_method_desc;
self.backhaul_type := backhaul_type;
self.backhaul_type_desc := backhaul_type_desc;
self.backhaul_allowance := backhaul_allowance;
self.ship_method := ship_method;
self.ship_method_desc := ship_method_desc;
self.purchase_type := purchase_type;
self.purchase_type_desc := purchase_type_desc;
self.status := status;
self.ship_pay_method := ship_pay_method;
self.ship_pay_method_desc := ship_pay_method_desc;
self.fob_trans_res := fob_trans_res;
self.fob_trans_res_code_desc := fob_trans_res_code_desc;
self.fob_trans_res_desc := fob_trans_res_desc;
self.fob_title_pass := fob_title_pass;
self.fob_title_pass_code_desc := fob_title_pass_code_desc;
self.fob_title_pass_desc := fob_title_pass_desc;
self.vendor_order_no := vendor_order_no;
self.exchange_rate := exchange_rate;
self.factory := factory;
self.factory_desc := factory_desc;
self.agent := agent;
self.agent_desc := agent_desc;
self.discharge_port := discharge_port;
self.discharge_port_desc := discharge_port_desc;
self.lading_port := lading_port;
self.lading_port_desc := lading_port_desc;
self.freight_contract_no := freight_contract_no;
self.po_type := po_type;
self.po_type_desc := po_type_desc;
self.pre_mark_ind := pre_mark_ind;
self.currency_code := currency_code;
self.contract_no := contract_no;
self.pickup_loc := pickup_loc;
self.pickup_no := pickup_no;
self.pickup_date := pickup_date;
self.app_datetime := app_datetime;
self.comment_desc := comment_desc;
self.cust_order_nbr := cust_order_nbr;
self.fulfill_order_nbr := fulfill_order_nbr;
self.PODtl_TBL := PODtl_TBL;
RETURN;
end;
END;
/
