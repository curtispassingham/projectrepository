DROP TYPE "RIB_SODtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SODtlRef_REC";
/
DROP TYPE "RIB_SORef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SORef_REC";
/
DROP TYPE "RIB_SODtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SODtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SORef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dest_id varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  item_id varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SODtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SODtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SORef') := "ns_name_SORef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
END appendNodeValues;
constructor function "RIB_SODtlRef_REC"
(
  rib_oid number
, dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.item_id := item_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_SODtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SODtlRef_TBL" AS TABLE OF "RIB_SODtlRef_REC";
/
DROP TYPE "RIB_SORef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SORef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SORef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  distro_nbr varchar2(12),
  document_type varchar2(1),
  dc_dest_id varchar2(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  SODtlRef_TBL "RIB_SODtlRef_TBL",   -- Size of "RIB_SODtlRef_TBL" is unbounded
  distro_parent_no varchar2(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
) return self as result
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
) return self as result
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
) return self as result
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, SODtlRef_TBL "RIB_SODtlRef_TBL"  -- Size of "RIB_SODtlRef_TBL" is unbounded
) return self as result
,constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, SODtlRef_TBL "RIB_SODtlRef_TBL"  -- Size of "RIB_SODtlRef_TBL" is unbounded
, distro_parent_no varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SORef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SORef') := "ns_name_SORef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_type') := document_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  IF SODtlRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SODtlRef_TBL.';
    FOR INDX IN SODtlRef_TBL.FIRST()..SODtlRef_TBL.LAST() LOOP
      SODtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'SODtlRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_parent_no') := distro_parent_no;
END appendNodeValues;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
RETURN;
end;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
RETURN;
end;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
RETURN;
end;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
RETURN;
end;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, SODtlRef_TBL "RIB_SODtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.SODtlRef_TBL := SODtlRef_TBL;
RETURN;
end;
constructor function "RIB_SORef_REC"
(
  rib_oid number
, distro_nbr varchar2
, document_type varchar2
, dc_dest_id varchar2
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, SODtlRef_TBL "RIB_SODtlRef_TBL"
, distro_parent_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.distro_nbr := distro_nbr;
self.document_type := document_type;
self.dc_dest_id := dc_dest_id;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.SODtlRef_TBL := SODtlRef_TBL;
self.distro_parent_no := distro_parent_no;
RETURN;
end;
END;
/
