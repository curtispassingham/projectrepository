@@FulfilOrdCustDesc.sql;
/
@@FulfilOrdDtl.sql;
/
DROP TYPE "RIB_FulfilOrdDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDesc_REC";
/
DROP TYPE "RIB_FulfilOrdDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDtl_TBL" AS TABLE OF "RIB_FulfilOrdDtl_REC";
/
DROP TYPE "RIB_FulfilOrdDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FulfilOrdDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_no varchar2(48),
  fulfill_order_no varchar2(48),
  source_loc_type varchar2(2), -- source_loc_type is enumeration field, valid values are [SU, ST, WH] (all lower-case)
  source_loc_id number(10),
  fulfill_loc_type varchar2(1), -- fulfill_loc_type is enumeration field, valid values are [S, V] (all lower-case)
  fulfill_loc_id number(10),
  partial_delivery_ind varchar2(1), -- partial_delivery_ind is enumeration field, valid values are [Y, N] (all lower-case)
  delivery_type varchar2(1), -- delivery_type is enumeration field, valid values are [S, C] (all lower-case)
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  consumer_delivery_date date,
  consumer_delivery_time date,
  delivery_charges number(20,4),
  delivery_charges_curr varchar2(3),
  comments varchar2(2000),
  FulfilOrdCustDesc "RIB_FulfilOrdCustDesc_REC",
  FulfilOrdDtl_TBL "RIB_FulfilOrdDtl_TBL",   -- Size of "RIB_FulfilOrdDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FulfilOrdDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, source_loc_type varchar2
, source_loc_id number
, fulfill_loc_type varchar2
, fulfill_loc_id number
, partial_delivery_ind varchar2
, delivery_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, delivery_charges number
, delivery_charges_curr varchar2
, comments varchar2
, FulfilOrdCustDesc "RIB_FulfilOrdCustDesc_REC"
, FulfilOrdDtl_TBL "RIB_FulfilOrdDtl_TBL"  -- Size of "RIB_FulfilOrdDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FulfilOrdDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FulfilOrdDesc') := "ns_name_FulfilOrdDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_no') := customer_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_no') := fulfill_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_loc_type') := source_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_loc_id') := source_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_type') := fulfill_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_id') := fulfill_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_ind') := partial_delivery_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_type') := delivery_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_date') := consumer_delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'consumer_delivery_time') := consumer_delivery_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_charges') := delivery_charges;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_charges_curr') := delivery_charges_curr;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  l_new_pre :=i_prefix||'FulfilOrdCustDesc.';
  FulfilOrdCustDesc.appendNodeValues( i_prefix||'FulfilOrdCustDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'FulfilOrdDtl_TBL.';
  FOR INDX IN FulfilOrdDtl_TBL.FIRST()..FulfilOrdDtl_TBL.LAST() LOOP
    FulfilOrdDtl_TBL(indx).appendNodeValues( i_prefix||indx||'FulfilOrdDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_FulfilOrdDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, source_loc_type varchar2
, source_loc_id number
, fulfill_loc_type varchar2
, fulfill_loc_id number
, partial_delivery_ind varchar2
, delivery_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, consumer_delivery_date date
, consumer_delivery_time date
, delivery_charges number
, delivery_charges_curr varchar2
, comments varchar2
, FulfilOrdCustDesc "RIB_FulfilOrdCustDesc_REC"
, FulfilOrdDtl_TBL "RIB_FulfilOrdDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.source_loc_type := source_loc_type;
self.source_loc_id := source_loc_id;
self.fulfill_loc_type := fulfill_loc_type;
self.fulfill_loc_id := fulfill_loc_id;
self.partial_delivery_ind := partial_delivery_ind;
self.delivery_type := delivery_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.consumer_delivery_date := consumer_delivery_date;
self.consumer_delivery_time := consumer_delivery_time;
self.delivery_charges := delivery_charges;
self.delivery_charges_curr := delivery_charges_curr;
self.comments := comments;
self.FulfilOrdCustDesc := FulfilOrdCustDesc;
self.FulfilOrdDtl_TBL := FulfilOrdDtl_TBL;
RETURN;
end;
END;
/
