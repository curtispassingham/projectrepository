@@NameValPairRBO.sql;
/
DROP TYPE "RIB_PrdItemRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrdItemRBO_REC";
/
DROP TYPE "RIB_NameValPairRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_NameValPairRBO_TBL" AS TABLE OF "RIB_NameValPairRBO_REC";
/
DROP TYPE "RIB_PrdItemRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrdItemRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrdItemRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_code varchar2(25),
  item_description varchar2(250),
  item_origin varchar2(1),
  item_utilization varchar2(1),
  is_transformed_item varchar2(1),
  fiscal_classification_code varchar2(25),
  ext_fiscal_class_code varchar2(25),
  ipi_exception_code varchar2(25),
  product_type varchar2(25),
  state_of_manufacture varchar2(3),
  pharma_list_type varchar2(6),
  NameValPairRBO_TBL "RIB_NameValPairRBO_TBL",   -- Size of "RIB_NameValPairRBO_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
) return self as result
,constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"  -- Size of "RIB_NameValPairRBO_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrdItemRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrdItemRBO') := "ns_name_PrdItemRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_code') := item_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_description') := item_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_origin') := item_origin;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_utilization') := item_utilization;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_transformed_item') := is_transformed_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_classification_code') := fiscal_classification_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_fiscal_class_code') := ext_fiscal_class_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_exception_code') := ipi_exception_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_type') := product_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_of_manufacture') := state_of_manufacture;
  rib_obj_util.g_RIB_element_values(i_prefix||'pharma_list_type') := pharma_list_type;
  IF NameValPairRBO_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'NameValPairRBO_TBL.';
    FOR INDX IN NameValPairRBO_TBL.FIRST()..NameValPairRBO_TBL.LAST() LOOP
      NameValPairRBO_TBL(indx).appendNodeValues( i_prefix||indx||'NameValPairRBO_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
self.ipi_exception_code := ipi_exception_code;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
self.ipi_exception_code := ipi_exception_code;
self.product_type := product_type;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
self.ipi_exception_code := ipi_exception_code;
self.product_type := product_type;
self.state_of_manufacture := state_of_manufacture;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
self.ipi_exception_code := ipi_exception_code;
self.product_type := product_type;
self.state_of_manufacture := state_of_manufacture;
self.pharma_list_type := pharma_list_type;
RETURN;
end;
constructor function "RIB_PrdItemRBO_REC"
(
  rib_oid number
, item_code varchar2
, item_description varchar2
, item_origin varchar2
, item_utilization varchar2
, is_transformed_item varchar2
, fiscal_classification_code varchar2
, ext_fiscal_class_code varchar2
, ipi_exception_code varchar2
, product_type varchar2
, state_of_manufacture varchar2
, pharma_list_type varchar2
, NameValPairRBO_TBL "RIB_NameValPairRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_code := item_code;
self.item_description := item_description;
self.item_origin := item_origin;
self.item_utilization := item_utilization;
self.is_transformed_item := is_transformed_item;
self.fiscal_classification_code := fiscal_classification_code;
self.ext_fiscal_class_code := ext_fiscal_class_code;
self.ipi_exception_code := ipi_exception_code;
self.product_type := product_type;
self.state_of_manufacture := state_of_manufacture;
self.pharma_list_type := pharma_list_type;
self.NameValPairRBO_TBL := NameValPairRBO_TBL;
RETURN;
end;
END;
/
