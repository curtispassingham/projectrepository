DROP TYPE "RIB_SiUinDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SiUinDesc_REC";
/
DROP TYPE "RIB_SiUinDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SiUinDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SiUinDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  uin_type varchar2(20), -- uin_type is enumeration field, valid values are [SERIAL_NUMBER, AGSN, UNKNOWN] (all lower-case)
  item_id varchar2(25),
  store_id number(10),
  status varchar2(30), -- status is enumeration field, valid values are [CUSTOMER_RESERVED, CUSTOMER_FULFILLED, IN_RECEIVING, IN_STOCK, MISSING, RESERVED_FOR_SHIPPING, REMOVE_FROM_INVENTORY, SHIPPED_TO_FINISHER, SHIPPED_TO_VENDOR, SHIPPED_TO_WAREHOUSE, SHIPPED_TO_STORE, SOLD, UNAVAILABLE, UNCONFIRMED, UNKNOWN] (all lower-case)
  transaction_id varchar2(128),
  transaction_type varchar2(30), -- transaction_type is enumeration field, valid values are [CREATE_RETURN, CREATE_TRANSFER, CUSTOMER_ORDER, DIRECT_DELIVERY_RECEIPT, DISPATCH_RETURN, DISPATCH_TRANSFER, INVENTORY_ADJUSTMENT, MANUAL, POS_SALE, POS_RETURN, POS_SALES_VOID, POS_RETURN_VOID, RECEIVE_TRANSFER, RECEIPT_ADJUSTMENT, STOCK_COUNT, STOCK_RECOUNT, STOCK_COUNT_AUTHORIZED, UIN_WEB_SERVICE, WAREHOUSE_DELIVERY_RECEIPT, UNKNOWN] (all lower-case)
  carton_id varchar2(128),
  nonsellable_type_id number(12),
  damaged varchar2(5), --damaged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SiUinDesc_REC"
(
  rib_oid number
, uin varchar2
, uin_type varchar2
, item_id varchar2
, store_id number
, status varchar2
, transaction_id varchar2
, transaction_type varchar2
, carton_id varchar2
, nonsellable_type_id number
, damaged varchar2  --damaged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SiUinDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SiUinDesc') := "ns_name_SiUinDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_type') := uin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsellable_type_id') := nonsellable_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged') := damaged;
END appendNodeValues;
constructor function "RIB_SiUinDesc_REC"
(
  rib_oid number
, uin varchar2
, uin_type varchar2
, item_id varchar2
, store_id number
, status varchar2
, transaction_id varchar2
, transaction_type varchar2
, carton_id varchar2
, nonsellable_type_id number
, damaged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.uin_type := uin_type;
self.item_id := item_id;
self.store_id := store_id;
self.status := status;
self.transaction_id := transaction_id;
self.transaction_type := transaction_type;
self.carton_id := carton_id;
self.nonsellable_type_id := nonsellable_type_id;
self.damaged := damaged;
RETURN;
end;
END;
/
