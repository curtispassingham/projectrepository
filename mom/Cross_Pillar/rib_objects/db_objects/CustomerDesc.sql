@@LocaleDesc.sql;
/
@@ContactDesc.sql;
/
@@PhoneDesc.sql;
/
@@EmailDesc.sql;
/
@@CardDesc.sql;
/
@@EmployeeRef.sql;
/
@@CustomerRef.sql;
/
@@GeoAddrDesc.sql;
/
DROP TYPE "RIB_CustomerDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerDesc_REC";
/
DROP TYPE "RIB_AlternateKeys_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AlternateKeys_REC";
/
DROP TYPE "RIB_CreatedBy_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CreatedBy_REC";
/
DROP TYPE "RIB_AddrBook_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrBook_REC";
/
DROP TYPE "RIB_AddrBookEntry_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrBookEntry_REC";
/
DROP TYPE "RIB_CustomerPhones_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerPhones_REC";
/
DROP TYPE "RIB_CustomerEmails_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerEmails_REC";
/
DROP TYPE "RIB_Cards_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_Cards_REC";
/
DROP TYPE "RIB_CustomerSegment_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerSegment_REC";
/
DROP TYPE "RIB_CustomerPromotion_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerPromotion_REC";
/
DROP TYPE "RIB_CustomerOrg_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerOrg_REC";
/
DROP TYPE "RIB_AlternateKeys_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AlternateKeys_TBL" AS TABLE OF "RIB_AlternateKeys_REC";
/
DROP TYPE "RIB_CustomerSegment_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerSegment_TBL" AS TABLE OF "RIB_CustomerSegment_REC";
/
DROP TYPE "RIB_CustomerPromotion_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerPromotion_TBL" AS TABLE OF "RIB_CustomerPromotion_REC";
/
DROP TYPE "RIB_CustomerDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerRef "RIB_CustomerRef_REC",
  AlternateKeys_TBL "RIB_AlternateKeys_TBL",   -- Size of "RIB_AlternateKeys_TBL" is 999
  customer_type varchar2(8), -- customer_type is enumeration field, valid values are [REGULAR, BUSINESS] (all lower-case)
  first_name varchar2(120),
  last_name varchar2(120),
  middle_name varchar2(120),
  company_name varchar2(250),
  contact_by_mail varchar2(5), --contact_by_mail is boolean field, valid values are true,false (all lower-case) 
  contact_by_phone varchar2(5), --contact_by_phone is boolean field, valid values are true,false (all lower-case) 
  contact_by_email varchar2(5), --contact_by_email is boolean field, valid values are true,false (all lower-case) 
  contact_by_fax varchar2(5), --contact_by_fax is boolean field, valid values are true,false (all lower-case) 
  gender varchar2(11), -- gender is enumeration field, valid values are [FEMALE, MALE, UNSPECIFIED] (all lower-case)
  ethnicity varchar2(16), -- ethnicity is enumeration field, valid values are [WHITE, AMERICAN INDIAN, ALASKA NATIVE, ASIAN, NATIVE HAWAIAN, PACIFIC ISLANDER, BLACK, HISPANIC_LATINO, OTHER] (all lower-case)
  marital_status varchar2(11), -- marital_status is enumeration field, valid values are [MARRIED, UNMARRIED, UNSPECIFIED] (all lower-case)
  birth_date date,
  anniversary_date date,
  annual_income number(20,4),
  net_worth number(20,4),
  highest_education_level varchar2(17), -- highest_education_level is enumeration field, valid values are [HIGH SCHOOL, UNDERGRADUATE, GRADUATE, VOCATIONAL SCHOOL, OTHER] (all lower-case)
  industry varchar2(120),
  created_by_loc varchar2(7), -- created_by_loc is enumeration field, valid values are [WEB, STORE, RENTED, UNKNOWN] (all lower-case)
  receipt_preference varchar2(5), -- receipt_preference is enumeration field, valid values are [NONE, PRINT, EMAIL, BOTH] (all lower-case)
  tax_id varchar2(1000),
  tax_certificate varchar2(230),
  tax_exempt_reason_code varchar2(20),
  job_title varchar2(100),
  CreatedBy "RIB_CreatedBy_REC",
  CustomerPhones "RIB_CustomerPhones_REC",
  CustomerEmails "RIB_CustomerEmails_REC",
  AddrBook "RIB_AddrBook_REC",
  CustomerSegment_TBL "RIB_CustomerSegment_TBL",   -- Size of "RIB_CustomerSegment_TBL" is 999
  CustomerPromotion_TBL "RIB_CustomerPromotion_TBL",   -- Size of "RIB_CustomerPromotion_TBL" is 999
  Cards "RIB_Cards_REC",
  CustomerOrg "RIB_CustomerOrg_REC",
  LocaleDesc "RIB_LocaleDesc_REC",
  create_timestamp date,
  update_timestamp date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
, Cards "RIB_Cards_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
, create_timestamp date
) return self as result
,constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"  -- Size of "RIB_AlternateKeys_TBL" is 999
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2  --contact_by_mail is boolean field, valid values are true,false (all lower-case)
, contact_by_phone varchar2  --contact_by_phone is boolean field, valid values are true,false (all lower-case)
, contact_by_email varchar2  --contact_by_email is boolean field, valid values are true,false (all lower-case)
, contact_by_fax varchar2  --contact_by_fax is boolean field, valid values are true,false (all lower-case)
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"  -- Size of "RIB_CustomerSegment_TBL" is 999
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"  -- Size of "RIB_CustomerPromotion_TBL" is 999
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
, create_timestamp date
, update_timestamp date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF AlternateKeys_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AlternateKeys_TBL.';
    FOR INDX IN AlternateKeys_TBL.FIRST()..AlternateKeys_TBL.LAST() LOOP
      AlternateKeys_TBL(indx).appendNodeValues( i_prefix||indx||'AlternateKeys_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_type') := customer_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'middle_name') := middle_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'company_name') := company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_by_mail') := contact_by_mail;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_by_phone') := contact_by_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_by_email') := contact_by_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_by_fax') := contact_by_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'gender') := gender;
  rib_obj_util.g_RIB_element_values(i_prefix||'ethnicity') := ethnicity;
  rib_obj_util.g_RIB_element_values(i_prefix||'marital_status') := marital_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'birth_date') := birth_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'anniversary_date') := anniversary_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'annual_income') := annual_income;
  rib_obj_util.g_RIB_element_values(i_prefix||'net_worth') := net_worth;
  rib_obj_util.g_RIB_element_values(i_prefix||'highest_education_level') := highest_education_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'industry') := industry;
  rib_obj_util.g_RIB_element_values(i_prefix||'created_by_loc') := created_by_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_preference') := receipt_preference;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_id') := tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_certificate') := tax_certificate;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_exempt_reason_code') := tax_exempt_reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'job_title') := job_title;
  l_new_pre :=i_prefix||'CreatedBy.';
  CreatedBy.appendNodeValues( i_prefix||'CreatedBy');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustomerPhones.';
  CustomerPhones.appendNodeValues( i_prefix||'CustomerPhones');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustomerEmails.';
  CustomerEmails.appendNodeValues( i_prefix||'CustomerEmails');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'AddrBook.';
  AddrBook.appendNodeValues( i_prefix||'AddrBook');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF CustomerSegment_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustomerSegment_TBL.';
    FOR INDX IN CustomerSegment_TBL.FIRST()..CustomerSegment_TBL.LAST() LOOP
      CustomerSegment_TBL(indx).appendNodeValues( i_prefix||indx||'CustomerSegment_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF CustomerPromotion_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CustomerPromotion_TBL.';
    FOR INDX IN CustomerPromotion_TBL.FIRST()..CustomerPromotion_TBL.LAST() LOOP
      CustomerPromotion_TBL(indx).appendNodeValues( i_prefix||indx||'CustomerPromotion_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'Cards.';
  Cards.appendNodeValues( i_prefix||'Cards');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustomerOrg.';
  CustomerOrg.appendNodeValues( i_prefix||'CustomerOrg');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocaleDesc.';
  LocaleDesc.appendNodeValues( i_prefix||'LocaleDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_timestamp') := create_timestamp;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_timestamp') := update_timestamp;
END appendNodeValues;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
, Cards "RIB_Cards_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
self.Cards := Cards;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
self.Cards := Cards;
self.CustomerOrg := CustomerOrg;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
self.Cards := Cards;
self.CustomerOrg := CustomerOrg;
self.LocaleDesc := LocaleDesc;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
, create_timestamp date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
self.Cards := Cards;
self.CustomerOrg := CustomerOrg;
self.LocaleDesc := LocaleDesc;
self.create_timestamp := create_timestamp;
RETURN;
end;
constructor function "RIB_CustomerDesc_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
, AlternateKeys_TBL "RIB_AlternateKeys_TBL"
, customer_type varchar2
, first_name varchar2
, last_name varchar2
, middle_name varchar2
, company_name varchar2
, contact_by_mail varchar2
, contact_by_phone varchar2
, contact_by_email varchar2
, contact_by_fax varchar2
, gender varchar2
, ethnicity varchar2
, marital_status varchar2
, birth_date date
, anniversary_date date
, annual_income number
, net_worth number
, highest_education_level varchar2
, industry varchar2
, created_by_loc varchar2
, receipt_preference varchar2
, tax_id varchar2
, tax_certificate varchar2
, tax_exempt_reason_code varchar2
, job_title varchar2
, CreatedBy "RIB_CreatedBy_REC"
, CustomerPhones "RIB_CustomerPhones_REC"
, CustomerEmails "RIB_CustomerEmails_REC"
, AddrBook "RIB_AddrBook_REC"
, CustomerSegment_TBL "RIB_CustomerSegment_TBL"
, CustomerPromotion_TBL "RIB_CustomerPromotion_TBL"
, Cards "RIB_Cards_REC"
, CustomerOrg "RIB_CustomerOrg_REC"
, LocaleDesc "RIB_LocaleDesc_REC"
, create_timestamp date
, update_timestamp date
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
self.AlternateKeys_TBL := AlternateKeys_TBL;
self.customer_type := customer_type;
self.first_name := first_name;
self.last_name := last_name;
self.middle_name := middle_name;
self.company_name := company_name;
self.contact_by_mail := contact_by_mail;
self.contact_by_phone := contact_by_phone;
self.contact_by_email := contact_by_email;
self.contact_by_fax := contact_by_fax;
self.gender := gender;
self.ethnicity := ethnicity;
self.marital_status := marital_status;
self.birth_date := birth_date;
self.anniversary_date := anniversary_date;
self.annual_income := annual_income;
self.net_worth := net_worth;
self.highest_education_level := highest_education_level;
self.industry := industry;
self.created_by_loc := created_by_loc;
self.receipt_preference := receipt_preference;
self.tax_id := tax_id;
self.tax_certificate := tax_certificate;
self.tax_exempt_reason_code := tax_exempt_reason_code;
self.job_title := job_title;
self.CreatedBy := CreatedBy;
self.CustomerPhones := CustomerPhones;
self.CustomerEmails := CustomerEmails;
self.AddrBook := AddrBook;
self.CustomerSegment_TBL := CustomerSegment_TBL;
self.CustomerPromotion_TBL := CustomerPromotion_TBL;
self.Cards := Cards;
self.CustomerOrg := CustomerOrg;
self.LocaleDesc := LocaleDesc;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
RETURN;
end;
END;
/
DROP TYPE "RIB_AlternateKeys_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AlternateKeys_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  type varchar2(30),
  value varchar2(64),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AlternateKeys_REC"
(
  rib_oid number
, type varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AlternateKeys_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_AlternateKeys_REC"
(
  rib_oid number
, type varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.type := type;
self.value := value;
RETURN;
end;
END;
/
DROP TYPE "RIB_CreatedBy_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CreatedBy_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EmployeeRef "RIB_EmployeeRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CreatedBy_REC"
(
  rib_oid number
, EmployeeRef "RIB_EmployeeRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CreatedBy_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EmployeeRef.';
  EmployeeRef.appendNodeValues( i_prefix||'EmployeeRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CreatedBy_REC"
(
  rib_oid number
, EmployeeRef "RIB_EmployeeRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EmployeeRef := EmployeeRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_AddrBookEntry_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrBookEntry_TBL" AS TABLE OF "RIB_AddrBookEntry_REC";
/
DROP TYPE "RIB_AddrBook_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AddrBook_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  AddrBookEntry_TBL "RIB_AddrBookEntry_TBL",   -- Size of "RIB_AddrBookEntry_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AddrBook_REC"
(
  rib_oid number
, AddrBookEntry_TBL "RIB_AddrBookEntry_TBL"  -- Size of "RIB_AddrBookEntry_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AddrBook_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF AddrBookEntry_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrBookEntry_TBL.';
    FOR INDX IN AddrBookEntry_TBL.FIRST()..AddrBookEntry_TBL.LAST() LOOP
      AddrBookEntry_TBL(indx).appendNodeValues( i_prefix||indx||'AddrBookEntry_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_AddrBook_REC"
(
  rib_oid number
, AddrBookEntry_TBL "RIB_AddrBookEntry_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.AddrBookEntry_TBL := AddrBookEntry_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_AddrBookEntry_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AddrBookEntry_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  address_alias varchar2(250),
  primary_addr_ind varchar2(5), --primary_addr_ind is boolean field, valid values are true,false (all lower-case) 
  primary_billing varchar2(5), --primary_billing is boolean field, valid values are true,false (all lower-case) 
  primary_shipping varchar2(5), --primary_shipping is boolean field, valid values are true,false (all lower-case) 
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  ContactDesc "RIB_ContactDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AddrBookEntry_REC"
(
  rib_oid number
, address_alias varchar2
, primary_addr_ind varchar2  --primary_addr_ind is boolean field, valid values are true,false (all lower-case)
, primary_billing varchar2  --primary_billing is boolean field, valid values are true,false (all lower-case)
, primary_shipping varchar2  --primary_shipping is boolean field, valid values are true,false (all lower-case)
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
,constructor function "RIB_AddrBookEntry_REC"
(
  rib_oid number
, address_alias varchar2
, primary_addr_ind varchar2  --primary_addr_ind is boolean field, valid values are true,false (all lower-case)
, primary_billing varchar2  --primary_billing is boolean field, valid values are true,false (all lower-case)
, primary_shipping varchar2  --primary_shipping is boolean field, valid values are true,false (all lower-case)
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ContactDesc "RIB_ContactDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AddrBookEntry_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'address_alias') := address_alias;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_addr_ind') := primary_addr_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_billing') := primary_billing;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_shipping') := primary_shipping;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ContactDesc.';
  ContactDesc.appendNodeValues( i_prefix||'ContactDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_AddrBookEntry_REC"
(
  rib_oid number
, address_alias varchar2
, primary_addr_ind varchar2
, primary_billing varchar2
, primary_shipping varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.address_alias := address_alias;
self.primary_addr_ind := primary_addr_ind;
self.primary_billing := primary_billing;
self.primary_shipping := primary_shipping;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
constructor function "RIB_AddrBookEntry_REC"
(
  rib_oid number
, address_alias varchar2
, primary_addr_ind varchar2
, primary_billing varchar2
, primary_shipping varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
, ContactDesc "RIB_ContactDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.address_alias := address_alias;
self.primary_addr_ind := primary_addr_ind;
self.primary_billing := primary_billing;
self.primary_shipping := primary_shipping;
self.GeoAddrDesc := GeoAddrDesc;
self.ContactDesc := ContactDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_PhoneDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PhoneDesc_TBL" AS TABLE OF "RIB_PhoneDesc_REC";
/
DROP TYPE "RIB_CustomerPhones_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerPhones_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  PhoneDesc_TBL "RIB_PhoneDesc_TBL",   -- Size of "RIB_PhoneDesc_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerPhones_REC"
(
  rib_oid number
, PhoneDesc_TBL "RIB_PhoneDesc_TBL"  -- Size of "RIB_PhoneDesc_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerPhones_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF PhoneDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PhoneDesc_TBL.';
    FOR INDX IN PhoneDesc_TBL.FIRST()..PhoneDesc_TBL.LAST() LOOP
      PhoneDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PhoneDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_CustomerPhones_REC"
(
  rib_oid number
, PhoneDesc_TBL "RIB_PhoneDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.PhoneDesc_TBL := PhoneDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_EmailDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_EmailDesc_TBL" AS TABLE OF "RIB_EmailDesc_REC";
/
DROP TYPE "RIB_CustomerEmails_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerEmails_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EmailDesc_TBL "RIB_EmailDesc_TBL",   -- Size of "RIB_EmailDesc_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerEmails_REC"
(
  rib_oid number
, EmailDesc_TBL "RIB_EmailDesc_TBL"  -- Size of "RIB_EmailDesc_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerEmails_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF EmailDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'EmailDesc_TBL.';
    FOR INDX IN EmailDesc_TBL.FIRST()..EmailDesc_TBL.LAST() LOOP
      EmailDesc_TBL(indx).appendNodeValues( i_prefix||indx||'EmailDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_CustomerEmails_REC"
(
  rib_oid number
, EmailDesc_TBL "RIB_EmailDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EmailDesc_TBL := EmailDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_CardDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_CardDesc_TBL" AS TABLE OF "RIB_CardDesc_REC";
/
DROP TYPE "RIB_Cards_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_Cards_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CardDesc_TBL "RIB_CardDesc_TBL",   -- Size of "RIB_CardDesc_TBL" is 99
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_Cards_REC"
(
  rib_oid number
, CardDesc_TBL "RIB_CardDesc_TBL"  -- Size of "RIB_CardDesc_TBL" is 99
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_Cards_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF CardDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'CardDesc_TBL.';
    FOR INDX IN CardDesc_TBL.FIRST()..CardDesc_TBL.LAST() LOOP
      CardDesc_TBL(indx).appendNodeValues( i_prefix||indx||'CardDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_Cards_REC"
(
  rib_oid number
, CardDesc_TBL "RIB_CardDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CardDesc_TBL := CardDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerSegment_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerSegment_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_segment_id number(22),
  customer_segment_name varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerSegment_REC"
(
  rib_oid number
, customer_segment_id number
) return self as result
,constructor function "RIB_CustomerSegment_REC"
(
  rib_oid number
, customer_segment_id number
, customer_segment_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerSegment_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_segment_id') := customer_segment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_segment_name') := customer_segment_name;
END appendNodeValues;
constructor function "RIB_CustomerSegment_REC"
(
  rib_oid number
, customer_segment_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_segment_id := customer_segment_id;
RETURN;
end;
constructor function "RIB_CustomerSegment_REC"
(
  rib_oid number
, customer_segment_id number
, customer_segment_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_segment_id := customer_segment_id;
self.customer_segment_name := customer_segment_name;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerPromotion_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerPromotion_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_promotion_id number(22),
  customer_promotion_name varchar2(250),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerPromotion_REC"
(
  rib_oid number
, customer_promotion_id number
) return self as result
,constructor function "RIB_CustomerPromotion_REC"
(
  rib_oid number
, customer_promotion_id number
, customer_promotion_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerPromotion_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_promotion_id') := customer_promotion_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_promotion_name') := customer_promotion_name;
END appendNodeValues;
constructor function "RIB_CustomerPromotion_REC"
(
  rib_oid number
, customer_promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_promotion_id := customer_promotion_id;
RETURN;
end;
constructor function "RIB_CustomerPromotion_REC"
(
  rib_oid number
, customer_promotion_id number
, customer_promotion_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_promotion_id := customer_promotion_id;
self.customer_promotion_name := customer_promotion_name;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerOrg_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerOrg_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustomerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  organization_id varchar2(14),
  organization_name varchar2(240),
  organization_type varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
) return self as result
,constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
, organization_name varchar2
) return self as result
,constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
, organization_name varchar2
, organization_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerOrg_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustomerDesc') := "ns_name_CustomerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'organization_id') := organization_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'organization_name') := organization_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'organization_type') := organization_type;
END appendNodeValues;
constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.organization_id := organization_id;
RETURN;
end;
constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
, organization_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.organization_id := organization_id;
self.organization_name := organization_name;
RETURN;
end;
constructor function "RIB_CustomerOrg_REC"
(
  rib_oid number
, organization_id varchar2
, organization_name varchar2
, organization_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.organization_id := organization_id;
self.organization_name := organization_name;
self.organization_type := organization_type;
RETURN;
end;
END;
/
