DROP TYPE "RIB_StsTsfDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfDesc_REC";
/
DROP TYPE "RIB_StsTsfItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfItm_REC";
/
DROP TYPE "RIB_StsTsfDescNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfDescNote_REC";
/
DROP TYPE "RIB_StsTsfItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfItm_TBL" AS TABLE OF "RIB_StsTsfItm_REC";
/
DROP TYPE "RIB_StsTsfDescNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfDescNote_TBL" AS TABLE OF "RIB_StsTsfDescNote_REC";
/
DROP TYPE "RIB_StsTsfDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_id number(15),
  ext_tsf_id varchar2(128),
  store_id number(10),
  src_loc_type varchar2(20), -- src_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, UNKNOWN] (all lower-case)
  src_loc_id number(10),
  dest_loc_type varchar2(20), -- dest_loc_type is enumeration field, valid values are [STORE, WAREHOUSE, FINISHER, UNKNOWN] (all lower-case)
  dest_loc_id number(10),
  orgin_type varchar2(8), -- orgin_type is enumeration field, valid values are [EXTERNAL, INTERNAL, ADHOC, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW_REQUEST, REQUESTED, REQUEST_IN_PROGRESS, REJECTED, CANCELED_REQUEST, TRANSFER_IN_PROGRESS, APPROVED, IN_SHIPPING, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  not_after_date date,
  auth_code varchar2(12),
  context_type_id varchar2(15),
  context_value varchar2(25),
  partial_delivery_allowed varchar2(5), --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case) 
  customer_order_id varchar2(128),
  fulfillment_order_id varchar2(128),
  request_user varchar2(250),
  approval_user varchar2(250),
  create_user varchar2(250),
  update_user varchar2(250),
  request_date date,
  approval_date date,
  create_date date,
  update_date date,
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  StsTsfItm_TBL "RIB_StsTsfItm_TBL",   -- Size of "RIB_StsTsfItm_TBL" is 5000
  StsTsfDescNote_TBL "RIB_StsTsfDescNote_TBL",   -- Size of "RIB_StsTsfDescNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, StsTsfItm_TBL "RIB_StsTsfItm_TBL"  -- Size of "RIB_StsTsfItm_TBL" is 5000
) return self as result
,constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2  --partial_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, StsTsfItm_TBL "RIB_StsTsfItm_TBL"  -- Size of "RIB_StsTsfItm_TBL" is 5000
, StsTsfDescNote_TBL "RIB_StsTsfDescNote_TBL"  -- Size of "RIB_StsTsfDescNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfDesc') := "ns_name_StsTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_tsf_id') := ext_tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_type') := src_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_loc_id') := src_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_type') := dest_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_loc_id') := dest_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'orgin_type') := orgin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'auth_code') := auth_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_type_id') := context_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'context_value') := context_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'partial_delivery_allowed') := partial_delivery_allowed;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfillment_order_id') := fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_user') := request_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_user') := approval_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user') := create_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_user') := update_user;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_date') := request_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'approval_date') := approval_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
  IF StsTsfItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfItm_TBL.';
    FOR INDX IN StsTsfItm_TBL.FIRST()..StsTsfItm_TBL.LAST() LOOP
      StsTsfItm_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StsTsfDescNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfDescNote_TBL.';
    FOR INDX IN StsTsfDescNote_TBL.FIRST()..StsTsfDescNote_TBL.LAST() LOOP
      StsTsfDescNote_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfDescNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.ext_tsf_id := ext_tsf_id;
self.store_id := store_id;
self.src_loc_type := src_loc_type;
self.src_loc_id := src_loc_id;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.orgin_type := orgin_type;
self.status := status;
self.not_after_date := not_after_date;
self.auth_code := auth_code;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.request_user := request_user;
self.approval_user := approval_user;
self.create_user := create_user;
self.update_user := update_user;
self.request_date := request_date;
self.approval_date := approval_date;
self.create_date := create_date;
self.update_date := update_date;
self.use_available := use_available;
RETURN;
end;
constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2
, StsTsfItm_TBL "RIB_StsTsfItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.ext_tsf_id := ext_tsf_id;
self.store_id := store_id;
self.src_loc_type := src_loc_type;
self.src_loc_id := src_loc_id;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.orgin_type := orgin_type;
self.status := status;
self.not_after_date := not_after_date;
self.auth_code := auth_code;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.request_user := request_user;
self.approval_user := approval_user;
self.create_user := create_user;
self.update_user := update_user;
self.request_date := request_date;
self.approval_date := approval_date;
self.create_date := create_date;
self.update_date := update_date;
self.use_available := use_available;
self.StsTsfItm_TBL := StsTsfItm_TBL;
RETURN;
end;
constructor function "RIB_StsTsfDesc_REC"
(
  rib_oid number
, tsf_id number
, ext_tsf_id varchar2
, store_id number
, src_loc_type varchar2
, src_loc_id number
, dest_loc_type varchar2
, dest_loc_id number
, orgin_type varchar2
, status varchar2
, not_after_date date
, auth_code varchar2
, context_type_id varchar2
, context_value varchar2
, partial_delivery_allowed varchar2
, customer_order_id varchar2
, fulfillment_order_id varchar2
, request_user varchar2
, approval_user varchar2
, create_user varchar2
, update_user varchar2
, request_date date
, approval_date date
, create_date date
, update_date date
, use_available varchar2
, StsTsfItm_TBL "RIB_StsTsfItm_TBL"
, StsTsfDescNote_TBL "RIB_StsTsfDescNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.ext_tsf_id := ext_tsf_id;
self.store_id := store_id;
self.src_loc_type := src_loc_type;
self.src_loc_id := src_loc_id;
self.dest_loc_type := dest_loc_type;
self.dest_loc_id := dest_loc_id;
self.orgin_type := orgin_type;
self.status := status;
self.not_after_date := not_after_date;
self.auth_code := auth_code;
self.context_type_id := context_type_id;
self.context_value := context_value;
self.partial_delivery_allowed := partial_delivery_allowed;
self.customer_order_id := customer_order_id;
self.fulfillment_order_id := fulfillment_order_id;
self.request_user := request_user;
self.approval_user := approval_user;
self.create_user := create_user;
self.update_user := update_user;
self.request_date := request_date;
self.approval_date := approval_date;
self.create_date := create_date;
self.update_date := update_date;
self.use_available := use_available;
self.StsTsfItm_TBL := StsTsfItm_TBL;
self.StsTsfDescNote_TBL := StsTsfDescNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StsTsfItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  requested_quantity number(20,4),
  approved_quantity number(20,4),
  shipping_quantity number(20,4),
  shipped_quantity number(20,4),
  received_quantity number(20,4),
  damaged_quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_quantity number
, approved_quantity number
, shipping_quantity number
, shipped_quantity number
, received_quantity number
, damaged_quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfDesc') := "ns_name_StsTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_quantity') := requested_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_quantity') := approved_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_quantity') := shipping_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipped_quantity') := shipped_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_quantity') := received_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged_quantity') := damaged_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_StsTsfItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_quantity number
, approved_quantity number
, shipping_quantity number
, shipped_quantity number
, received_quantity number
, damaged_quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.requested_quantity := requested_quantity;
self.approved_quantity := approved_quantity;
self.shipping_quantity := shipping_quantity;
self.shipped_quantity := shipped_quantity;
self.received_quantity := received_quantity;
self.damaged_quantity := damaged_quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
DROP TYPE "RIB_StsTsfDescNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfDescNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfDescNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfDesc') := "ns_name_StsTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_StsTsfDescNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
