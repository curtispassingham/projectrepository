@@FiscDocChnkRBO.sql;
/
DROP TYPE "RIB_FiscDocColRBM_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocColRBM_REC";
/
DROP TYPE "RIB_FiscDocChnkRBO_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscDocChnkRBO_TBL" AS TABLE OF "RIB_FiscDocChnkRBO_REC";
/
DROP TYPE "RIB_FiscDocColRBM_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscDocColRBM_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscDocColRBM" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL",   -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
  thread_use varchar2(1),
  logs varchar2(4000),
  vendor_type varchar2(10),
  country varchar2(2),
  transaction_type varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
) return self as result
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
, thread_use varchar2
) return self as result
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
, thread_use varchar2
, logs varchar2
) return self as result
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
) return self as result
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
, country varchar2
) return self as result
,constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"  -- Size of "RIB_FiscDocChnkRBO_TBL" is unbounded
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
, country varchar2
, transaction_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscDocColRBM_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscDocColRBM') := "ns_name_FiscDocColRBM";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'FiscDocChnkRBO_TBL.';
  FOR INDX IN FiscDocChnkRBO_TBL.FIRST()..FiscDocChnkRBO_TBL.LAST() LOOP
    FiscDocChnkRBO_TBL(indx).appendNodeValues( i_prefix||indx||'FiscDocChnkRBO_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'thread_use') := thread_use;
  rib_obj_util.g_RIB_element_values(i_prefix||'logs') := logs;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendor_type') := vendor_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
END appendNodeValues;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
RETURN;
end;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
, thread_use varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
self.thread_use := thread_use;
RETURN;
end;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
, thread_use varchar2
, logs varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
self.thread_use := thread_use;
self.logs := logs;
RETURN;
end;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
self.thread_use := thread_use;
self.logs := logs;
self.vendor_type := vendor_type;
RETURN;
end;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
, country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
self.thread_use := thread_use;
self.logs := logs;
self.vendor_type := vendor_type;
self.country := country;
RETURN;
end;
constructor function "RIB_FiscDocColRBM_REC"
(
  rib_oid number
, FiscDocChnkRBO_TBL "RIB_FiscDocChnkRBO_TBL"
, thread_use varchar2
, logs varchar2
, vendor_type varchar2
, country varchar2
, transaction_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.FiscDocChnkRBO_TBL := FiscDocChnkRBO_TBL;
self.thread_use := thread_use;
self.logs := logs;
self.vendor_type := vendor_type;
self.country := country;
self.transaction_type := transaction_type;
RETURN;
end;
END;
/
