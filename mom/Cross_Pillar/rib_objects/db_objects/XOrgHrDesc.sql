@@XOrgHrLocTrt.sql;
/
DROP TYPE "RIB_XOrgHrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XOrgHrDesc_REC";
/
DROP TYPE "RIB_XOrgHrLocTrt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XOrgHrLocTrt_TBL" AS TABLE OF "RIB_XOrgHrLocTrt_REC";
/
DROP TYPE "RIB_XOrgHrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XOrgHrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XOrgHrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_value number(10),
  hier_desc varchar2(120),
  hier_level varchar2(2),
  parent_id number(10),
  mgr_name varchar2(120),
  currency_code varchar2(3),
  XOrgHrLocTrt_TBL "RIB_XOrgHrLocTrt_TBL",   -- Size of "RIB_XOrgHrLocTrt_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
) return self as result
,constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
) return self as result
,constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
) return self as result
,constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
, currency_code varchar2
) return self as result
,constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
, currency_code varchar2
, XOrgHrLocTrt_TBL "RIB_XOrgHrLocTrt_TBL"  -- Size of "RIB_XOrgHrLocTrt_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XOrgHrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XOrgHrDesc') := "ns_name_XOrgHrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_value') := hier_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_desc') := hier_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_level') := hier_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'parent_id') := parent_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'mgr_name') := mgr_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  IF XOrgHrLocTrt_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XOrgHrLocTrt_TBL.';
    FOR INDX IN XOrgHrLocTrt_TBL.FIRST()..XOrgHrLocTrt_TBL.LAST() LOOP
      XOrgHrLocTrt_TBL(indx).appendNodeValues( i_prefix||indx||'XOrgHrLocTrt_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.hier_desc := hier_desc;
self.hier_level := hier_level;
RETURN;
end;
constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.hier_desc := hier_desc;
self.hier_level := hier_level;
self.parent_id := parent_id;
RETURN;
end;
constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.hier_desc := hier_desc;
self.hier_level := hier_level;
self.parent_id := parent_id;
self.mgr_name := mgr_name;
RETURN;
end;
constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.hier_desc := hier_desc;
self.hier_level := hier_level;
self.parent_id := parent_id;
self.mgr_name := mgr_name;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_XOrgHrDesc_REC"
(
  rib_oid number
, hier_value number
, hier_desc varchar2
, hier_level varchar2
, parent_id number
, mgr_name varchar2
, currency_code varchar2
, XOrgHrLocTrt_TBL "RIB_XOrgHrLocTrt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
self.hier_desc := hier_desc;
self.hier_level := hier_level;
self.parent_id := parent_id;
self.mgr_name := mgr_name;
self.currency_code := currency_code;
self.XOrgHrLocTrt_TBL := XOrgHrLocTrt_TBL;
RETURN;
end;
END;
/
