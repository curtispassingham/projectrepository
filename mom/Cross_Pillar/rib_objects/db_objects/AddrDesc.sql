DROP TYPE "RIB_AddrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrDesc_REC";
/
DROP TYPE "RIB_AddrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_AddrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_AddrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  city_id varchar2(10),
  state_name varchar2(120),
  country_name varchar2(120),
  addr varchar2(32),
  addr_type varchar2(2),
  primary_addr_type_ind varchar2(1),
  primary_addr_ind varchar2(1),
  add_1 varchar2(240),
  add_2 varchar2(240),
  add_3 varchar2(240),
  city varchar2(120),
  state varchar2(3),
  country_id varchar2(3),
  post varchar2(30),
  contact_name varchar2(120),
  contact_phone varchar2(20),
  contact_telex varchar2(20),
  contact_fax varchar2(20),
  contact_email varchar2(100),
  oracle_vendor_site_id number(15),
  county varchar2(250),
  jurisdiction_code varchar2(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
, county varchar2
) return self as result
,constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
, county varchar2
, jurisdiction_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_AddrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_AddrDesc') := "ns_name_AddrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'city_id') := city_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_name') := state_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_name') := country_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr') := addr;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_type') := addr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_addr_type_ind') := primary_addr_type_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_addr_ind') := primary_addr_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_1') := add_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_2') := add_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'add_3') := add_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'post') := post;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_name') := contact_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_phone') := contact_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_telex') := contact_telex;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_fax') := contact_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_email') := contact_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'oracle_vendor_site_id') := oracle_vendor_site_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'county') := county;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
END appendNodeValues;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.oracle_vendor_site_id := oracle_vendor_site_id;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
, county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.oracle_vendor_site_id := oracle_vendor_site_id;
self.county := county;
RETURN;
end;
constructor function "RIB_AddrDesc_REC"
(
  rib_oid number
, city_id varchar2
, state_name varchar2
, country_name varchar2
, addr varchar2
, addr_type varchar2
, primary_addr_type_ind varchar2
, primary_addr_ind varchar2
, add_1 varchar2
, add_2 varchar2
, add_3 varchar2
, city varchar2
, state varchar2
, country_id varchar2
, post varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_telex varchar2
, contact_fax varchar2
, contact_email varchar2
, oracle_vendor_site_id number
, county varchar2
, jurisdiction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.city_id := city_id;
self.state_name := state_name;
self.country_name := country_name;
self.addr := addr;
self.addr_type := addr_type;
self.primary_addr_type_ind := primary_addr_type_ind;
self.primary_addr_ind := primary_addr_ind;
self.add_1 := add_1;
self.add_2 := add_2;
self.add_3 := add_3;
self.city := city;
self.state := state;
self.country_id := country_id;
self.post := post;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_telex := contact_telex;
self.contact_fax := contact_fax;
self.contact_email := contact_email;
self.oracle_vendor_site_id := oracle_vendor_site_id;
self.county := county;
self.jurisdiction_code := jurisdiction_code;
RETURN;
end;
END;
/
