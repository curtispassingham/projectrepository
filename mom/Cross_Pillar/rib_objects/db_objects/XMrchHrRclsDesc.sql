DROP TYPE "RIB_XMrchHrRclsDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XMrchHrRclsDesc_REC";
/
DROP TYPE "RIB_XMrchHrRclsDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XMrchHrRclsDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XMrchHrRclsDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  merch_hier_level varchar2(1),
  merch_hier_id number(4),
  merch_hier_parent_id number(4),
  merch_hier_grandparent_id number(4),
  merch_hier_name varchar2(120),
  effective_date date,
  action_type varchar2(1),
  buyer number(4),
  purchase_type number(1),
  total_market_amt number(24,4),
  merch number(4),
  bud_mkup number(12,4),
  profit_calc_type number(1),
  markup_calc_type varchar2(2),
  otb_calc_type varchar2(1),
  max_avg_counter number(5),
  avg_tolerance_pct number(12,4),
  bud_int number(12,4),
  dept_vat_incl_ind varchar2(1),
  class_vat_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
, dept_vat_incl_ind varchar2
) return self as result
,constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
, dept_vat_incl_ind varchar2
, class_vat_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XMrchHrRclsDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XMrchHrRclsDesc') := "ns_name_XMrchHrRclsDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'merch_hier_level') := merch_hier_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'merch_hier_id') := merch_hier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'merch_hier_parent_id') := merch_hier_parent_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'merch_hier_grandparent_id') := merch_hier_grandparent_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'merch_hier_name') := merch_hier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'action_type') := action_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'buyer') := buyer;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type') := purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_market_amt') := total_market_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'merch') := merch;
  rib_obj_util.g_RIB_element_values(i_prefix||'bud_mkup') := bud_mkup;
  rib_obj_util.g_RIB_element_values(i_prefix||'profit_calc_type') := profit_calc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'markup_calc_type') := markup_calc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'otb_calc_type') := otb_calc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'max_avg_counter') := max_avg_counter;
  rib_obj_util.g_RIB_element_values(i_prefix||'avg_tolerance_pct') := avg_tolerance_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'bud_int') := bud_int;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept_vat_incl_ind') := dept_vat_incl_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_vat_ind') := class_vat_ind;
END appendNodeValues;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
self.max_avg_counter := max_avg_counter;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
self.max_avg_counter := max_avg_counter;
self.avg_tolerance_pct := avg_tolerance_pct;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
self.max_avg_counter := max_avg_counter;
self.avg_tolerance_pct := avg_tolerance_pct;
self.bud_int := bud_int;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
, dept_vat_incl_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
self.max_avg_counter := max_avg_counter;
self.avg_tolerance_pct := avg_tolerance_pct;
self.bud_int := bud_int;
self.dept_vat_incl_ind := dept_vat_incl_ind;
RETURN;
end;
constructor function "RIB_XMrchHrRclsDesc_REC"
(
  rib_oid number
, merch_hier_level varchar2
, merch_hier_id number
, merch_hier_parent_id number
, merch_hier_grandparent_id number
, merch_hier_name varchar2
, effective_date date
, action_type varchar2
, buyer number
, purchase_type number
, total_market_amt number
, merch number
, bud_mkup number
, profit_calc_type number
, markup_calc_type varchar2
, otb_calc_type varchar2
, max_avg_counter number
, avg_tolerance_pct number
, bud_int number
, dept_vat_incl_ind varchar2
, class_vat_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.merch_hier_level := merch_hier_level;
self.merch_hier_id := merch_hier_id;
self.merch_hier_parent_id := merch_hier_parent_id;
self.merch_hier_grandparent_id := merch_hier_grandparent_id;
self.merch_hier_name := merch_hier_name;
self.effective_date := effective_date;
self.action_type := action_type;
self.buyer := buyer;
self.purchase_type := purchase_type;
self.total_market_amt := total_market_amt;
self.merch := merch;
self.bud_mkup := bud_mkup;
self.profit_calc_type := profit_calc_type;
self.markup_calc_type := markup_calc_type;
self.otb_calc_type := otb_calc_type;
self.max_avg_counter := max_avg_counter;
self.avg_tolerance_pct := avg_tolerance_pct;
self.bud_int := bud_int;
self.dept_vat_incl_ind := dept_vat_incl_ind;
self.class_vat_ind := class_vat_ind;
RETURN;
end;
END;
/
