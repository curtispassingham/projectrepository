DROP TYPE "RIB_XAllocDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocDtl_REC";
/
DROP TYPE "RIB_XAlloc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XAlloc_REC";
/
DROP TYPE "RIB_XAllocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocDesc_REC";
/
DROP TYPE "RIB_XAllocDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XAllocDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XAllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  to_loc number(10),
  to_loc_type varchar2(1),
  qty_allocated number(12,4),
  in_store_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XAllocDtl_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
, qty_allocated number
) return self as result
,constructor function "RIB_XAllocDtl_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
, qty_allocated number
, in_store_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XAllocDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XAllocDesc') := "ns_name_XAllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc') := to_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_loc_type') := to_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'qty_allocated') := qty_allocated;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_store_date') := in_store_date;
END appendNodeValues;
constructor function "RIB_XAllocDtl_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
, qty_allocated number
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.qty_allocated := qty_allocated;
RETURN;
end;
constructor function "RIB_XAllocDtl_REC"
(
  rib_oid number
, to_loc number
, to_loc_type varchar2
, qty_allocated number
, in_store_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_loc := to_loc;
self.to_loc_type := to_loc_type;
self.qty_allocated := qty_allocated;
self.in_store_date := in_store_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_XAllocDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XAllocDtl_TBL" AS TABLE OF "RIB_XAllocDtl_REC";
/
DROP TYPE "RIB_XAlloc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XAlloc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XAllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  alloc_no number(10),
  alloc_desc varchar2(300),
  order_no number(12),
  item varchar2(25),
  from_loc number(10),
  release_date date,
  origin_ind varchar2(6),
  XAllocDtl_TBL "RIB_XAllocDtl_TBL",   -- Size of "RIB_XAllocDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
) return self as result
,constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
) return self as result
,constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
, origin_ind varchar2
) return self as result
,constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
, origin_ind varchar2
, XAllocDtl_TBL "RIB_XAllocDtl_TBL"  -- Size of "RIB_XAllocDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XAlloc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XAllocDesc') := "ns_name_XAllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_no') := alloc_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'alloc_desc') := alloc_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_no') := order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_loc') := from_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'release_date') := release_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_ind') := origin_ind;
  IF XAllocDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XAllocDtl_TBL.';
    FOR INDX IN XAllocDtl_TBL.FIRST()..XAllocDtl_TBL.LAST() LOOP
      XAllocDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XAllocDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.alloc_desc := alloc_desc;
self.order_no := order_no;
self.item := item;
self.from_loc := from_loc;
RETURN;
end;
constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.alloc_desc := alloc_desc;
self.order_no := order_no;
self.item := item;
self.from_loc := from_loc;
self.release_date := release_date;
RETURN;
end;
constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
, origin_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.alloc_desc := alloc_desc;
self.order_no := order_no;
self.item := item;
self.from_loc := from_loc;
self.release_date := release_date;
self.origin_ind := origin_ind;
RETURN;
end;
constructor function "RIB_XAlloc_REC"
(
  rib_oid number
, alloc_no number
, alloc_desc varchar2
, order_no number
, item varchar2
, from_loc number
, release_date date
, origin_ind varchar2
, XAllocDtl_TBL "RIB_XAllocDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.alloc_no := alloc_no;
self.alloc_desc := alloc_desc;
self.order_no := order_no;
self.item := item;
self.from_loc := from_loc;
self.release_date := release_date;
self.origin_ind := origin_ind;
self.XAllocDtl_TBL := XAllocDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_XAlloc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XAlloc_TBL" AS TABLE OF "RIB_XAlloc_REC";
/
DROP TYPE "RIB_XAllocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XAllocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XAllocDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  XAlloc_TBL "RIB_XAlloc_TBL",   -- Size of "RIB_XAlloc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XAllocDesc_REC"
(
  rib_oid number
, XAlloc_TBL "RIB_XAlloc_TBL"  -- Size of "RIB_XAlloc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XAllocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XAllocDesc') := "ns_name_XAllocDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'XAlloc_TBL.';
  FOR INDX IN XAlloc_TBL.FIRST()..XAlloc_TBL.LAST() LOOP
    XAlloc_TBL(indx).appendNodeValues( i_prefix||indx||'XAlloc_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_XAllocDesc_REC"
(
  rib_oid number
, XAlloc_TBL "RIB_XAlloc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.XAlloc_TBL := XAlloc_TBL;
RETURN;
end;
END;
/
