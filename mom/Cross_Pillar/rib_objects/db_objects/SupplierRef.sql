@@ExtOfSupplierRef.sql;
/
@@LocOfSupplierRef.sql;
/
DROP TYPE "RIB_SupplierRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierRef_REC";
/
DROP TYPE "RIB_SupplierSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierSite_REC";
/
DROP TYPE "RIB_SupplierSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierSiteAddr_REC";
/
DROP TYPE "RIB_SupplierSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierSite_TBL" AS TABLE OF "RIB_SupplierSite_REC";
/
DROP TYPE "RIB_LocOfSupplierRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierRef_TBL" AS TABLE OF "RIB_LocOfSupplierRef_REC";
/
DROP TYPE "RIB_SupplierRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupplierRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  sup_xref_key varchar2(32),
  supplier_id number(10),
  SupplierSite_TBL "RIB_SupplierSite_TBL",   -- Size of "RIB_SupplierSite_TBL" is unbounded
  ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC",
  LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL",   -- Size of "RIB_LocOfSupplierRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
) return self as result
,constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"  -- Size of "RIB_SupplierSite_TBL" is unbounded
) return self as result
,constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"  -- Size of "RIB_SupplierSite_TBL" is unbounded
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
) return self as result
,constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"  -- Size of "RIB_SupplierSite_TBL" is unbounded
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
, LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL"  -- Size of "RIB_LocOfSupplierRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupplierRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierRef') := "ns_name_SupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'sup_xref_key') := sup_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  IF SupplierSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SupplierSite_TBL.';
    FOR INDX IN SupplierSite_TBL.FIRST()..SupplierSite_TBL.LAST() LOOP
      SupplierSite_TBL(indx).appendNodeValues( i_prefix||indx||'SupplierSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'ExtOfSupplierRef.';
  ExtOfSupplierRef.appendNodeValues( i_prefix||'ExtOfSupplierRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfSupplierRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfSupplierRef_TBL.';
    FOR INDX IN LocOfSupplierRef_TBL.FIRST()..LocOfSupplierRef_TBL.LAST() LOOP
      LocOfSupplierRef_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfSupplierRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
RETURN;
end;
constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupplierSite_TBL := SupplierSite_TBL;
RETURN;
end;
constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupplierSite_TBL := SupplierSite_TBL;
self.ExtOfSupplierRef := ExtOfSupplierRef;
RETURN;
end;
constructor function "RIB_SupplierRef_REC"
(
  rib_oid number
, sup_xref_key varchar2
, supplier_id number
, SupplierSite_TBL "RIB_SupplierSite_TBL"
, ExtOfSupplierRef "RIB_ExtOfSupplierRef_REC"
, LocOfSupplierRef_TBL "RIB_LocOfSupplierRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.sup_xref_key := sup_xref_key;
self.supplier_id := supplier_id;
self.SupplierSite_TBL := SupplierSite_TBL;
self.ExtOfSupplierRef := ExtOfSupplierRef;
self.LocOfSupplierRef_TBL := LocOfSupplierRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_SupplierSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SupplierSiteAddr_TBL" AS TABLE OF "RIB_SupplierSiteAddr_REC";
/
DROP TYPE "RIB_SupplierSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupplierSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supsite_xref_key varchar2(32),
  supplier_site_id number(10),
  SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL",   -- Size of "RIB_SupplierSiteAddr_TBL" is unbounded
  ExtOfSupplierSite "RIB_ExtOfSupplierSite_REC",
  LocOfSupplierSite "RIB_LocOfSupplierSite_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"  -- Size of "RIB_SupplierSiteAddr_TBL" is unbounded
) return self as result
,constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"  -- Size of "RIB_SupplierSiteAddr_TBL" is unbounded
, ExtOfSupplierSite "RIB_ExtOfSupplierSite_REC"
) return self as result
,constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"  -- Size of "RIB_SupplierSiteAddr_TBL" is unbounded
, ExtOfSupplierSite "RIB_ExtOfSupplierSite_REC"
, LocOfSupplierSite "RIB_LocOfSupplierSite_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupplierSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierRef') := "ns_name_SupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supsite_xref_key') := supsite_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_site_id') := supplier_site_id;
  l_new_pre :=i_prefix||'SupplierSiteAddr_TBL.';
  FOR INDX IN SupplierSiteAddr_TBL.FIRST()..SupplierSiteAddr_TBL.LAST() LOOP
    SupplierSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'SupplierSiteAddr_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'ExtOfSupplierSite.';
  ExtOfSupplierSite.appendNodeValues( i_prefix||'ExtOfSupplierSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocOfSupplierSite.';
  LocOfSupplierSite.appendNodeValues( i_prefix||'LocOfSupplierSite');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupplierSiteAddr_TBL := SupplierSiteAddr_TBL;
RETURN;
end;
constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"
, ExtOfSupplierSite "RIB_ExtOfSupplierSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupplierSiteAddr_TBL := SupplierSiteAddr_TBL;
self.ExtOfSupplierSite := ExtOfSupplierSite;
RETURN;
end;
constructor function "RIB_SupplierSite_REC"
(
  rib_oid number
, supsite_xref_key varchar2
, supplier_site_id number
, SupplierSiteAddr_TBL "RIB_SupplierSiteAddr_TBL"
, ExtOfSupplierSite "RIB_ExtOfSupplierSite_REC"
, LocOfSupplierSite "RIB_LocOfSupplierSite_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supsite_xref_key := supsite_xref_key;
self.supplier_site_id := supplier_site_id;
self.SupplierSiteAddr_TBL := SupplierSiteAddr_TBL;
self.ExtOfSupplierSite := ExtOfSupplierSite;
self.LocOfSupplierSite := LocOfSupplierSite;
RETURN;
end;
END;
/
DROP TYPE "RIB_SupplierSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SupplierSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  addr_xref_key varchar2(32),
  addr_key number(11),
  ExtOfSupplierSiteAddr "RIB_ExtOfSupplierSiteAddr_REC",
  LocOfSupplierSiteAddr "RIB_LocOfSupplierSiteAddr_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
) return self as result
,constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, ExtOfSupplierSiteAddr "RIB_ExtOfSupplierSiteAddr_REC"
) return self as result
,constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, ExtOfSupplierSiteAddr "RIB_ExtOfSupplierSiteAddr_REC"
, LocOfSupplierSiteAddr "RIB_LocOfSupplierSiteAddr_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SupplierSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SupplierRef') := "ns_name_SupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_xref_key') := addr_xref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_key') := addr_key;
  l_new_pre :=i_prefix||'ExtOfSupplierSiteAddr.';
  ExtOfSupplierSiteAddr.appendNodeValues( i_prefix||'ExtOfSupplierSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'LocOfSupplierSiteAddr.';
  LocOfSupplierSiteAddr.appendNodeValues( i_prefix||'LocOfSupplierSiteAddr');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
RETURN;
end;
constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, ExtOfSupplierSiteAddr "RIB_ExtOfSupplierSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.ExtOfSupplierSiteAddr := ExtOfSupplierSiteAddr;
RETURN;
end;
constructor function "RIB_SupplierSiteAddr_REC"
(
  rib_oid number
, addr_xref_key varchar2
, addr_key number
, ExtOfSupplierSiteAddr "RIB_ExtOfSupplierSiteAddr_REC"
, LocOfSupplierSiteAddr "RIB_LocOfSupplierSiteAddr_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_xref_key := addr_xref_key;
self.addr_key := addr_key;
self.ExtOfSupplierSiteAddr := ExtOfSupplierSiteAddr;
self.LocOfSupplierSiteAddr := LocOfSupplierSiteAddr;
RETURN;
end;
END;
/
