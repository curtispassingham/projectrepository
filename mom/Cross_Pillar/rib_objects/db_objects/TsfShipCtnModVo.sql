@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_TsfShipCtnModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnModVo_REC";
/
DROP TYPE "RIB_TsfShipCtnSizeMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnSizeMod_REC";
/
DROP TYPE "RIB_TsfShipCtnItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItmMod_REC";
/
DROP TYPE "RIB_TsfShipCtnModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnModNote_REC";
/
DROP TYPE "RIB_TsfShipCtnItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItmMod_TBL" AS TABLE OF "RIB_TsfShipCtnItmMod_REC";
/
DROP TYPE "RIB_TsfShipCtnModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnModNote_TBL" AS TABLE OF "RIB_TsfShipCtnModNote_REC";
/
DROP TYPE "RIB_TsfShipCtnModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_ship_carton_id number(15),
  external_id varchar2(128),
  hierarchy_level varchar2(20), -- hierarchy_level is enumeration field, valid values are [NONE, DEPT, CLASS, SUBCLASS] (all lower-case)
  TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC",
  weight number(20,4),
  tracking_number varchar2(128),
  use_available varchar2(5), --use_available is boolean field, valid values are true,false (all lower-case) 
  TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL",   -- Size of "RIB_TsfShipCtnItmMod_TBL" is 1000
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 1000
  TsfShipCtnModNote_TBL "RIB_TsfShipCtnModNote_TBL",   -- Size of "RIB_TsfShipCtnModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"  -- Size of "RIB_TsfShipCtnItmMod_TBL" is 1000
) return self as result
,constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"  -- Size of "RIB_TsfShipCtnItmMod_TBL" is 1000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
) return self as result
,constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2  --use_available is boolean field, valid values are true,false (all lower-case)
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"  -- Size of "RIB_TsfShipCtnItmMod_TBL" is 1000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
, TsfShipCtnModNote_TBL "RIB_TsfShipCtnModNote_TBL"  -- Size of "RIB_TsfShipCtnModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnModVo') := "ns_name_TsfShipCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_ship_carton_id') := tsf_ship_carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'hierarchy_level') := hierarchy_level;
  l_new_pre :=i_prefix||'TsfShipCtnSizeMod.';
  TsfShipCtnSizeMod.appendNodeValues( i_prefix||'TsfShipCtnSizeMod');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'use_available') := use_available;
  IF TsfShipCtnItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfShipCtnItmMod_TBL.';
    FOR INDX IN TsfShipCtnItmMod_TBL.FIRST()..TsfShipCtnItmMod_TBL.LAST() LOOP
      TsfShipCtnItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'TsfShipCtnItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF TsfShipCtnModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'TsfShipCtnModNote_TBL.';
    FOR INDX IN TsfShipCtnModNote_TBL.FIRST()..TsfShipCtnModNote_TBL.LAST() LOOP
      TsfShipCtnModNote_TBL(indx).appendNodeValues( i_prefix||indx||'TsfShipCtnModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.external_id := external_id;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSizeMod := TsfShipCtnSizeMod;
self.weight := weight;
self.tracking_number := tracking_number;
self.use_available := use_available;
RETURN;
end;
constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.external_id := external_id;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSizeMod := TsfShipCtnSizeMod;
self.weight := weight;
self.tracking_number := tracking_number;
self.use_available := use_available;
self.TsfShipCtnItmMod_TBL := TsfShipCtnItmMod_TBL;
RETURN;
end;
constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.external_id := external_id;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSizeMod := TsfShipCtnSizeMod;
self.weight := weight;
self.tracking_number := tracking_number;
self.use_available := use_available;
self.TsfShipCtnItmMod_TBL := TsfShipCtnItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
constructor function "RIB_TsfShipCtnModVo_REC"
(
  rib_oid number
, tsf_ship_carton_id number
, external_id varchar2
, hierarchy_level varchar2
, TsfShipCtnSizeMod "RIB_TsfShipCtnSizeMod_REC"
, weight number
, tracking_number varchar2
, use_available varchar2
, TsfShipCtnItmMod_TBL "RIB_TsfShipCtnItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, TsfShipCtnModNote_TBL "RIB_TsfShipCtnModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_ship_carton_id := tsf_ship_carton_id;
self.external_id := external_id;
self.hierarchy_level := hierarchy_level;
self.TsfShipCtnSizeMod := TsfShipCtnSizeMod;
self.weight := weight;
self.tracking_number := tracking_number;
self.use_available := use_available;
self.TsfShipCtnItmMod_TBL := TsfShipCtnItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
self.TsfShipCtnModNote_TBL := TsfShipCtnModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_TsfShipCtnSizeMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnSizeMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id number(15),
  store_id number(10),
  description varchar2(128),
  height number(20,4),
  width number(20,4),
  length number(20,4),
  unit_of_measure varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnSizeMod_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnSizeMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnModVo') := "ns_name_TsfShipCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'height') := height;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
END appendNodeValues;
constructor function "RIB_TsfShipCtnSizeMod_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.store_id := store_id;
self.description := description;
self.height := height;
self.width := width;
self.length := length;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_TsfShipCtnItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_item_id number(15),
  item_id varchar2(25),
  case_size number(10,2),
  quantity number(10),
  shipment_reason_id number(15),
  transfer_id number(12),
  uin_col "RIB_uin_col_TBL",   -- Size of "RIB_uin_col_TBL" is 5000
  removed_uin_col "RIB_removed_uin_col_TBL",   -- Size of "RIB_removed_uin_col_TBL" is 5000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 5000
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
) return self as result
,constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 5000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 5000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 5000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnModVo') := "ns_name_TsfShipCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_id') := line_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_reason_id') := shipment_reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_id') := transfer_id;
  IF uin_col IS NOT NULL THEN
    FOR INDX IN uin_col.FIRST()..uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'uin_col'||'.'):=uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_uin_col IS NOT NULL THEN
    FOR INDX IN removed_uin_col.FIRST()..removed_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_uin_col'||'.'):=removed_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.uin_col := uin_col;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.uin_col := uin_col;
self.removed_uin_col := removed_uin_col;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.uin_col := uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_TsfShipCtnItmMod_REC"
(
  rib_oid number
, line_item_id number
, item_id varchar2
, case_size number
, quantity number
, shipment_reason_id number
, transfer_id number
, uin_col "RIB_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_item_id := line_item_id;
self.item_id := item_id;
self.case_size := case_size;
self.quantity := quantity;
self.shipment_reason_id := shipment_reason_id;
self.transfer_id := transfer_id;
self.uin_col := uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_TsfShipCtnModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnModVo') := "ns_name_TsfShipCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_TsfShipCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
