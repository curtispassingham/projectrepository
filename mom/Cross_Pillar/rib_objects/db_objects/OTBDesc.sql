DROP TYPE "RIB_OTB_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OTB_REC";
/
DROP TYPE "RIB_OTBDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OTBDesc_REC";
/
DROP TYPE "RIB_OTB_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OTB_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_OTBDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  po_nbr varchar2(12),
  dept number(4),
  class varchar2(7),
  subclass varchar2(4),
  unit_retail number(20,4),
  unit_cost number(20,4),
  receipt_qty number(12,4),
  approved_qty number(12,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OTB_REC"
(
  rib_oid number
, po_nbr varchar2
, dept number
, class varchar2
, subclass varchar2
, unit_retail number
, unit_cost number
, receipt_qty number
, approved_qty number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OTB_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_OTBDesc') := "ns_name_OTBDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'po_nbr') := po_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'dept') := dept;
  rib_obj_util.g_RIB_element_values(i_prefix||'class') := class;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass') := subclass;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_retail') := unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'receipt_qty') := receipt_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'approved_qty') := approved_qty;
END appendNodeValues;
constructor function "RIB_OTB_REC"
(
  rib_oid number
, po_nbr varchar2
, dept number
, class varchar2
, subclass varchar2
, unit_retail number
, unit_cost number
, receipt_qty number
, approved_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.po_nbr := po_nbr;
self.dept := dept;
self.class := class;
self.subclass := subclass;
self.unit_retail := unit_retail;
self.unit_cost := unit_cost;
self.receipt_qty := receipt_qty;
self.approved_qty := approved_qty;
RETURN;
end;
END;
/
DROP TYPE "RIB_OTB_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_OTB_TBL" AS TABLE OF "RIB_OTB_REC";
/
DROP TYPE "RIB_OTBDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OTBDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_OTBDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  OTB_TBL "RIB_OTB_TBL",   -- Size of "RIB_OTB_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OTBDesc_REC"
(
  rib_oid number
, OTB_TBL "RIB_OTB_TBL"  -- Size of "RIB_OTB_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OTBDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_OTBDesc') := "ns_name_OTBDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'OTB_TBL.';
  FOR INDX IN OTB_TBL.FIRST()..OTB_TBL.LAST() LOOP
    OTB_TBL(indx).appendNodeValues( i_prefix||indx||'OTB_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_OTBDesc_REC"
(
  rib_oid number
, OTB_TBL "RIB_OTB_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.OTB_TBL := OTB_TBL;
RETURN;
end;
END;
/
