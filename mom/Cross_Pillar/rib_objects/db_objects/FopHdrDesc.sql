DROP TYPE "RIB_FopHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FopHdrDesc_REC";
/
DROP TYPE "RIB_FopHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FopHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FopHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  pick_id number(12),
  create_date date,
  create_user_name varchar2(128),
  complete_date date,
  complete_user_name varchar2(128),
  pick_type varchar2(20), -- pick_type is enumeration field, valid values are [ORDER, BIN, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  total_quantity number(20,4),
  actual_quantity number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FopHdrDesc_REC"
(
  rib_oid number
, pick_id number
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
, pick_type varchar2
, status varchar2
, total_quantity number
, actual_quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FopHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FopHdrDesc') := "ns_name_FopHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_id') := pick_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'complete_date') := complete_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'complete_user_name') := complete_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_type') := pick_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_quantity') := total_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'actual_quantity') := actual_quantity;
END appendNodeValues;
constructor function "RIB_FopHdrDesc_REC"
(
  rib_oid number
, pick_id number
, create_date date
, create_user_name varchar2
, complete_date date
, complete_user_name varchar2
, pick_type varchar2
, status varchar2
, total_quantity number
, actual_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.pick_id := pick_id;
self.create_date := create_date;
self.create_user_name := create_user_name;
self.complete_date := complete_date;
self.complete_user_name := complete_user_name;
self.pick_type := pick_type;
self.status := status;
self.total_quantity := total_quantity;
self.actual_quantity := actual_quantity;
RETURN;
end;
END;
/
