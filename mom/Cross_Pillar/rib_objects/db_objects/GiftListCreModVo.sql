@@GeoAddrDesc.sql;
/
@@CustomerRef.sql;
/
@@GiftListItemsCreVo.sql;
/
@@GiftListItemsModVo.sql;
/
@@GiftListItemsDelVo.sql;
/
DROP TYPE "RIB_GiftListCreModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListCreModVo_REC";
/
DROP TYPE "RIB_GiftListOwnerCreVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListOwnerCreVo_REC";
/
DROP TYPE "RIB_GiftListOwnerModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GiftListOwnerModVo_REC";
/
DROP TYPE "RIB_ShippingAddrCreVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShippingAddrCreVo_REC";
/
DROP TYPE "RIB_ShippingAddrModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShippingAddrModVo_REC";
/
DROP TYPE "RIB_ShippingAddrDelVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShippingAddrDelVo_REC";
/
DROP TYPE "RIB_GiftListCreModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListCreModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  gift_list_id varchar2(40),
  CustomerRef "RIB_CustomerRef_REC",
  gift_list_name varchar2(254),
  event_type varchar2(40),
  event_date date,
  published_flag varchar2(5), --published_flag is boolean field, valid values are true,false (all lower-case) 
  public_flag varchar2(5), --public_flag is boolean field, valid values are true,false (all lower-case) 
  location varchar2(40),
  comments varchar2(254),
  instructions varchar2(254),
  image_data varchar2(254),
  expiration_date date,
  order_id varchar2(40),
  GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC",
  GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC",
  GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC",
  GiftListItemsModVo "RIB_GiftListItemsModVo_REC",
  GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC",
  ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC",
  ShippingAddrModVo "RIB_ShippingAddrModVo_REC",
  ShippingAddrDelVo "RIB_ShippingAddrDelVo_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
, ShippingAddrModVo "RIB_ShippingAddrModVo_REC"
) return self as result
,constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2  --published_flag is boolean field, valid values are true,false (all lower-case)
, public_flag varchar2  --public_flag is boolean field, valid values are true,false (all lower-case)
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
, ShippingAddrModVo "RIB_ShippingAddrModVo_REC"
, ShippingAddrDelVo "RIB_ShippingAddrDelVo_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListCreModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_list_id') := gift_list_id;
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_list_name') := gift_list_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_type') := event_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_date') := event_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'published_flag') := published_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'public_flag') := public_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'instructions') := instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'image_data') := image_data;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration_date') := expiration_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_id') := order_id;
  l_new_pre :=i_prefix||'GiftListOwnerCreVo.';
  GiftListOwnerCreVo.appendNodeValues( i_prefix||'GiftListOwnerCreVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftListOwnerModVo.';
  GiftListOwnerModVo.appendNodeValues( i_prefix||'GiftListOwnerModVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftListItemsCreVo.';
  GiftListItemsCreVo.appendNodeValues( i_prefix||'GiftListItemsCreVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftListItemsModVo.';
  GiftListItemsModVo.appendNodeValues( i_prefix||'GiftListItemsModVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'GiftListItemsDelVo.';
  GiftListItemsDelVo.appendNodeValues( i_prefix||'GiftListItemsDelVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ShippingAddrCreVo.';
  ShippingAddrCreVo.appendNodeValues( i_prefix||'ShippingAddrCreVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ShippingAddrModVo.';
  ShippingAddrModVo.appendNodeValues( i_prefix||'ShippingAddrModVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'ShippingAddrDelVo.';
  ShippingAddrDelVo.appendNodeValues( i_prefix||'ShippingAddrDelVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
self.GiftListItemsModVo := GiftListItemsModVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
self.GiftListItemsModVo := GiftListItemsModVo;
self.GiftListItemsDelVo := GiftListItemsDelVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
self.GiftListItemsModVo := GiftListItemsModVo;
self.GiftListItemsDelVo := GiftListItemsDelVo;
self.ShippingAddrCreVo := ShippingAddrCreVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
, ShippingAddrModVo "RIB_ShippingAddrModVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
self.GiftListItemsModVo := GiftListItemsModVo;
self.GiftListItemsDelVo := GiftListItemsDelVo;
self.ShippingAddrCreVo := ShippingAddrCreVo;
self.ShippingAddrModVo := ShippingAddrModVo;
RETURN;
end;
constructor function "RIB_GiftListCreModVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, CustomerRef "RIB_CustomerRef_REC"
, gift_list_name varchar2
, event_type varchar2
, event_date date
, published_flag varchar2
, public_flag varchar2
, location varchar2
, comments varchar2
, instructions varchar2
, image_data varchar2
, expiration_date date
, order_id varchar2
, GiftListOwnerCreVo "RIB_GiftListOwnerCreVo_REC"
, GiftListOwnerModVo "RIB_GiftListOwnerModVo_REC"
, GiftListItemsCreVo "RIB_GiftListItemsCreVo_REC"
, GiftListItemsModVo "RIB_GiftListItemsModVo_REC"
, GiftListItemsDelVo "RIB_GiftListItemsDelVo_REC"
, ShippingAddrCreVo "RIB_ShippingAddrCreVo_REC"
, ShippingAddrModVo "RIB_ShippingAddrModVo_REC"
, ShippingAddrDelVo "RIB_ShippingAddrDelVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.CustomerRef := CustomerRef;
self.gift_list_name := gift_list_name;
self.event_type := event_type;
self.event_date := event_date;
self.published_flag := published_flag;
self.public_flag := public_flag;
self.location := location;
self.comments := comments;
self.instructions := instructions;
self.image_data := image_data;
self.expiration_date := expiration_date;
self.order_id := order_id;
self.GiftListOwnerCreVo := GiftListOwnerCreVo;
self.GiftListOwnerModVo := GiftListOwnerModVo;
self.GiftListItemsCreVo := GiftListItemsCreVo;
self.GiftListItemsModVo := GiftListItemsModVo;
self.GiftListItemsDelVo := GiftListItemsDelVo;
self.ShippingAddrCreVo := ShippingAddrCreVo;
self.ShippingAddrModVo := ShippingAddrModVo;
self.ShippingAddrDelVo := ShippingAddrDelVo;
RETURN;
end;
END;
/
DROP TYPE "RIB_GiftListOwnerCreVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListOwnerCreVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerRef "RIB_CustomerRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListOwnerCreVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListOwnerCreVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_GiftListOwnerCreVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_GiftListOwnerModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GiftListOwnerModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerRef "RIB_CustomerRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GiftListOwnerModVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GiftListOwnerModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'CustomerRef.';
  CustomerRef.appendNodeValues( i_prefix||'CustomerRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_GiftListOwnerModVo_REC"
(
  rib_oid number
, CustomerRef "RIB_CustomerRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerRef := CustomerRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShippingAddrCreVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShippingAddrCreVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShippingAddrCreVo_REC"
(
  rib_oid number
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShippingAddrCreVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ShippingAddrCreVo_REC"
(
  rib_oid number
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShippingAddrModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShippingAddrModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShippingAddrModVo_REC"
(
  rib_oid number
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShippingAddrModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ShippingAddrModVo_REC"
(
  rib_oid number
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShippingAddrDelVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShippingAddrDelVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GiftListCreModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  gift_list_id varchar2(40),
  GeoAddrDesc "RIB_GeoAddrDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShippingAddrDelVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShippingAddrDelVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GiftListCreModVo') := "ns_name_GiftListCreModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_list_id') := gift_list_id;
  l_new_pre :=i_prefix||'GeoAddrDesc.';
  GeoAddrDesc.appendNodeValues( i_prefix||'GeoAddrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ShippingAddrDelVo_REC"
(
  rib_oid number
, gift_list_id varchar2
, GeoAddrDesc "RIB_GeoAddrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.gift_list_id := gift_list_id;
self.GeoAddrDesc := GeoAddrDesc;
RETURN;
end;
END;
/
