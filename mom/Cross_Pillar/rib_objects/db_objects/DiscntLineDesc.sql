DROP TYPE "RIB_DiscntLineDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DiscntLineDesc_REC";
/
DROP TYPE "RIB_DiscntLineDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DiscntLineDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_DiscntLineDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_no number(4),
  accounting_method varchar2(8), -- accounting_method is enumeration field, valid values are [DISCOUNT, MARKDOWN] (all lower-case)
  advanced_pricing_rule_flag varchar2(1), -- advanced_pricing_rule_flag is enumeration field, valid values are [Y, N] (all lower-case)
  assignment_basis varchar2(4), -- assignment_basis is enumeration field, valid values are [MANU, CUST, ITEM, CPON, EMPL, OTHR] (all lower-case)
  damage_discount_flag varchar2(1), -- damage_discount_flag is enumeration field, valid values are [Y, N] (all lower-case)
  currency_code varchar2(3),
  discount_amount number(20,4),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  returned_discount_amount number(20,4),
  unit_discount_amount number(20,4),
  discount_employee_id varchar2(10),
  discount_method varchar2(3), -- discount_method is enumeration field, valid values are [PCT, AMT] (all lower-case)
  discount_rate number(5,2),
  discount_rule_id varchar2(22),
  discount_scope varchar2(3), -- discount_scope is enumeration field, valid values are [ORD, ITM] (all lower-case)
  included_in_bestdeal_flag varchar2(1), -- included_in_bestdeal_flag is enumeration field, valid values are [Y, N] (all lower-case)
  discount_reason_code varchar2(20),
  store_coupon_id varchar2(255),
  promotion_component_detail_id number(22),
  promotion_component_id number(22),
  promotion_id number(22),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
) return self as result
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
) return self as result
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
) return self as result
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
) return self as result
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
, promotion_component_id number
) return self as result
,constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
, promotion_component_id number
, promotion_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DiscntLineDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_DiscntLineDesc') := "ns_name_DiscntLineDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_no') := line_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'accounting_method') := accounting_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'advanced_pricing_rule_flag') := advanced_pricing_rule_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'assignment_basis') := assignment_basis;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_discount_flag') := damage_discount_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_amount') := discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_discount_amount') := returned_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_discount_amount') := unit_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_employee_id') := discount_employee_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_method') := discount_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_rate') := discount_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_rule_id') := discount_rule_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_scope') := discount_scope;
  rib_obj_util.g_RIB_element_values(i_prefix||'included_in_bestdeal_flag') := included_in_bestdeal_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_reason_code') := discount_reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_coupon_id') := store_coupon_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_component_detail_id') := promotion_component_detail_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_component_id') := promotion_component_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
END appendNodeValues;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
RETURN;
end;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
self.discount_reason_code := discount_reason_code;
RETURN;
end;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
self.discount_reason_code := discount_reason_code;
self.store_coupon_id := store_coupon_id;
RETURN;
end;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
self.discount_reason_code := discount_reason_code;
self.store_coupon_id := store_coupon_id;
self.promotion_component_detail_id := promotion_component_detail_id;
RETURN;
end;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
, promotion_component_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
self.discount_reason_code := discount_reason_code;
self.store_coupon_id := store_coupon_id;
self.promotion_component_detail_id := promotion_component_detail_id;
self.promotion_component_id := promotion_component_id;
RETURN;
end;
constructor function "RIB_DiscntLineDesc_REC"
(
  rib_oid number
, line_no number
, accounting_method varchar2
, advanced_pricing_rule_flag varchar2
, assignment_basis varchar2
, damage_discount_flag varchar2
, currency_code varchar2
, discount_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, unit_discount_amount number
, discount_employee_id varchar2
, discount_method varchar2
, discount_rate number
, discount_rule_id varchar2
, discount_scope varchar2
, included_in_bestdeal_flag varchar2
, discount_reason_code varchar2
, store_coupon_id varchar2
, promotion_component_detail_id number
, promotion_component_id number
, promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.accounting_method := accounting_method;
self.advanced_pricing_rule_flag := advanced_pricing_rule_flag;
self.assignment_basis := assignment_basis;
self.damage_discount_flag := damage_discount_flag;
self.currency_code := currency_code;
self.discount_amount := discount_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.unit_discount_amount := unit_discount_amount;
self.discount_employee_id := discount_employee_id;
self.discount_method := discount_method;
self.discount_rate := discount_rate;
self.discount_rule_id := discount_rule_id;
self.discount_scope := discount_scope;
self.included_in_bestdeal_flag := included_in_bestdeal_flag;
self.discount_reason_code := discount_reason_code;
self.store_coupon_id := store_coupon_id;
self.promotion_component_detail_id := promotion_component_detail_id;
self.promotion_component_id := promotion_component_id;
self.promotion_id := promotion_id;
RETURN;
end;
END;
/
