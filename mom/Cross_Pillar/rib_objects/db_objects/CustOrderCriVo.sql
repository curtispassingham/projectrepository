DROP TYPE "RIB_CustOrderCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrderCriVo_REC";
/
DROP TYPE "RIB_OrderCriteria_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OrderCriteria_REC";
/
DROP TYPE "RIB_CustomerCri_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerCri_REC";
/
DROP TYPE "RIB_CustomerInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustomerInfo_REC";
/
DROP TYPE "RIB_CreditDebitCri_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CreditDebitCri_REC";
/
DROP TYPE "RIB_MaskedCardNumber_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_MaskedCardNumber_REC";
/
DROP TYPE "RIB_OrderRequestor_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OrderRequestor_REC";
/
DROP TYPE "RIB_OrderItemRequestor_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OrderItemRequestor_REC";
/
DROP TYPE "RIB_DateRange_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_DateRange_REC";
/
DROP TYPE "RIB_CustOrderCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrderCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  search_loc_id number(10),
  search_loc_type varchar2(1), -- search_loc_type is enumeration field, valid values are [S] (all lower-case)
  OrderCriteria "RIB_OrderCriteria_REC",
  OrderRequestor "RIB_OrderRequestor_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrderCriVo_REC"
(
  rib_oid number
, search_loc_id number
, search_loc_type varchar2
, OrderCriteria "RIB_OrderCriteria_REC"
, OrderRequestor "RIB_OrderRequestor_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrderCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'search_loc_id') := search_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'search_loc_type') := search_loc_type;
  l_new_pre :=i_prefix||'OrderCriteria.';
  OrderCriteria.appendNodeValues( i_prefix||'OrderCriteria');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'OrderRequestor.';
  OrderRequestor.appendNodeValues( i_prefix||'OrderRequestor');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrderCriVo_REC"
(
  rib_oid number
, search_loc_id number
, search_loc_type varchar2
, OrderCriteria "RIB_OrderCriteria_REC"
, OrderRequestor "RIB_OrderRequestor_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_loc_id := search_loc_id;
self.search_loc_type := search_loc_type;
self.OrderCriteria := OrderCriteria;
self.OrderRequestor := OrderRequestor;
RETURN;
end;
END;
/
DROP TYPE "RIB_OrderCriteria_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OrderCriteria_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  search_type varchar2(10), -- search_type is enumeration field, valid values are [ORDERID, CUSTOMER, CHARGECARD] (all lower-case)
  customer_order_id varchar2(48),
  CustomerCri "RIB_CustomerCri_REC",
  CreditDebitCri "RIB_CreditDebitCri_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
) return self as result
,constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
) return self as result
,constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
, CustomerCri "RIB_CustomerCri_REC"
) return self as result
,constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
, CustomerCri "RIB_CustomerCri_REC"
, CreditDebitCri "RIB_CreditDebitCri_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OrderCriteria_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'search_type') := search_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  l_new_pre :=i_prefix||'CustomerCri.';
  CustomerCri.appendNodeValues( i_prefix||'CustomerCri');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CreditDebitCri.';
  CreditDebitCri.appendNodeValues( i_prefix||'CreditDebitCri');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
RETURN;
end;
constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.customer_order_id := customer_order_id;
RETURN;
end;
constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
, CustomerCri "RIB_CustomerCri_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.customer_order_id := customer_order_id;
self.CustomerCri := CustomerCri;
RETURN;
end;
constructor function "RIB_OrderCriteria_REC"
(
  rib_oid number
, search_type varchar2
, customer_order_id varchar2
, CustomerCri "RIB_CustomerCri_REC"
, CreditDebitCri "RIB_CreditDebitCri_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.search_type := search_type;
self.customer_order_id := customer_order_id;
self.CustomerCri := CustomerCri;
self.CreditDebitCri := CreditDebitCri;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerCri_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerCri_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  CustomerInfo "RIB_CustomerInfo_REC",
  customer_id varchar2(14),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerCri_REC"
(
  rib_oid number
, CustomerInfo "RIB_CustomerInfo_REC"
) return self as result
,constructor function "RIB_CustomerCri_REC"
(
  rib_oid number
, CustomerInfo "RIB_CustomerInfo_REC"
, customer_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerCri_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'CustomerInfo.';
  CustomerInfo.appendNodeValues( i_prefix||'CustomerInfo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_id') := customer_id;
END appendNodeValues;
constructor function "RIB_CustomerCri_REC"
(
  rib_oid number
, CustomerInfo "RIB_CustomerInfo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerInfo := CustomerInfo;
RETURN;
end;
constructor function "RIB_CustomerCri_REC"
(
  rib_oid number
, CustomerInfo "RIB_CustomerInfo_REC"
, customer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.CustomerInfo := CustomerInfo;
self.customer_id := customer_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_CustomerInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustomerInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  first_name varchar2(120),
  last_name varchar2(120),
  company_name varchar2(120),
  postal_code varchar2(30),
  phone_number varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
) return self as result
,constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
) return self as result
,constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
) return self as result
,constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
, postal_code varchar2
) return self as result
,constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
, postal_code varchar2
, phone_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustomerInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'company_name') := company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'phone_number') := phone_number;
END appendNodeValues;
constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
RETURN;
end;
constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
RETURN;
end;
constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.company_name := company_name;
RETURN;
end;
constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.company_name := company_name;
self.postal_code := postal_code;
RETURN;
end;
constructor function "RIB_CustomerInfo_REC"
(
  rib_oid number
, first_name varchar2
, last_name varchar2
, company_name varchar2
, postal_code varchar2
, phone_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.first_name := first_name;
self.last_name := last_name;
self.company_name := company_name;
self.postal_code := postal_code;
self.phone_number := phone_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_CreditDebitCri_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CreditDebitCri_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  card_token varchar2(100),
  MaskedCardNumber "RIB_MaskedCardNumber_REC",
  item_id varchar2(25),
  DateRange "RIB_DateRange_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CreditDebitCri_REC"
(
  rib_oid number
, card_token varchar2
, MaskedCardNumber "RIB_MaskedCardNumber_REC"
, item_id varchar2
) return self as result
,constructor function "RIB_CreditDebitCri_REC"
(
  rib_oid number
, card_token varchar2
, MaskedCardNumber "RIB_MaskedCardNumber_REC"
, item_id varchar2
, DateRange "RIB_DateRange_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CreditDebitCri_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'card_token') := card_token;
  l_new_pre :=i_prefix||'MaskedCardNumber.';
  MaskedCardNumber.appendNodeValues( i_prefix||'MaskedCardNumber');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  l_new_pre :=i_prefix||'DateRange.';
  DateRange.appendNodeValues( i_prefix||'DateRange');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CreditDebitCri_REC"
(
  rib_oid number
, card_token varchar2
, MaskedCardNumber "RIB_MaskedCardNumber_REC"
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_token := card_token;
self.MaskedCardNumber := MaskedCardNumber;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_CreditDebitCri_REC"
(
  rib_oid number
, card_token varchar2
, MaskedCardNumber "RIB_MaskedCardNumber_REC"
, item_id varchar2
, DateRange "RIB_DateRange_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.card_token := card_token;
self.MaskedCardNumber := MaskedCardNumber;
self.item_id := item_id;
self.DateRange := DateRange;
RETURN;
end;
END;
/
DROP TYPE "RIB_MaskedCardNumber_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MaskedCardNumber_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  leading_digits varchar2(6),
  trailing_digits varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MaskedCardNumber_REC"
(
  rib_oid number
, leading_digits varchar2
, trailing_digits varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MaskedCardNumber_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'leading_digits') := leading_digits;
  rib_obj_util.g_RIB_element_values(i_prefix||'trailing_digits') := trailing_digits;
END appendNodeValues;
constructor function "RIB_MaskedCardNumber_REC"
(
  rib_oid number
, leading_digits varchar2
, trailing_digits varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.leading_digits := leading_digits;
self.trailing_digits := trailing_digits;
RETURN;
end;
END;
/
DROP TYPE "RIB_OrderRequestor_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OrderRequestor_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  request_order_header_flag varchar2(1), -- request_order_header_flag is enumeration field, valid values are [Y, N] (all lower-case)
  request_order_item_flag varchar2(5), -- request_order_item_flag is enumeration field, valid values are [NONE, FIRST, ALL] (all lower-case)
  request_order_fulfillment_flag varchar2(1), -- request_order_fulfillment_flag is enumeration field, valid values are [Y, N] (all lower-case)
  request_order_delivery_flag varchar2(1), -- request_order_delivery_flag is enumeration field, valid values are [Y, N] (all lower-case)
  request_order_payment_flag varchar2(1), -- request_order_payment_flag is enumeration field, valid values are [Y, N] (all lower-case)
  OrderItemRequestor "RIB_OrderItemRequestor_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OrderRequestor_REC"
(
  rib_oid number
, request_order_header_flag varchar2
, request_order_item_flag varchar2
, request_order_fulfillment_flag varchar2
, request_order_delivery_flag varchar2
, request_order_payment_flag varchar2
) return self as result
,constructor function "RIB_OrderRequestor_REC"
(
  rib_oid number
, request_order_header_flag varchar2
, request_order_item_flag varchar2
, request_order_fulfillment_flag varchar2
, request_order_delivery_flag varchar2
, request_order_payment_flag varchar2
, OrderItemRequestor "RIB_OrderItemRequestor_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OrderRequestor_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'request_order_header_flag') := request_order_header_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_order_item_flag') := request_order_item_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_order_fulfillment_flag') := request_order_fulfillment_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_order_delivery_flag') := request_order_delivery_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_order_payment_flag') := request_order_payment_flag;
  l_new_pre :=i_prefix||'OrderItemRequestor.';
  OrderItemRequestor.appendNodeValues( i_prefix||'OrderItemRequestor');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_OrderRequestor_REC"
(
  rib_oid number
, request_order_header_flag varchar2
, request_order_item_flag varchar2
, request_order_fulfillment_flag varchar2
, request_order_delivery_flag varchar2
, request_order_payment_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.request_order_header_flag := request_order_header_flag;
self.request_order_item_flag := request_order_item_flag;
self.request_order_fulfillment_flag := request_order_fulfillment_flag;
self.request_order_delivery_flag := request_order_delivery_flag;
self.request_order_payment_flag := request_order_payment_flag;
RETURN;
end;
constructor function "RIB_OrderRequestor_REC"
(
  rib_oid number
, request_order_header_flag varchar2
, request_order_item_flag varchar2
, request_order_fulfillment_flag varchar2
, request_order_delivery_flag varchar2
, request_order_payment_flag varchar2
, OrderItemRequestor "RIB_OrderItemRequestor_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.request_order_header_flag := request_order_header_flag;
self.request_order_item_flag := request_order_item_flag;
self.request_order_fulfillment_flag := request_order_fulfillment_flag;
self.request_order_delivery_flag := request_order_delivery_flag;
self.request_order_payment_flag := request_order_payment_flag;
self.OrderItemRequestor := OrderItemRequestor;
RETURN;
end;
END;
/
DROP TYPE "RIB_OrderItemRequestor_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OrderItemRequestor_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  request_item_header_flag varchar2(1), -- request_item_header_flag is enumeration field, valid values are [Y, N] (all lower-case)
  request_item_discount_flag varchar2(1), -- request_item_discount_flag is enumeration field, valid values are [Y, N] (all lower-case)
  request_item_tax_flag varchar2(1), -- request_item_tax_flag is enumeration field, valid values are [Y, N] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OrderItemRequestor_REC"
(
  rib_oid number
, request_item_header_flag varchar2
, request_item_discount_flag varchar2
, request_item_tax_flag varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OrderItemRequestor_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'request_item_header_flag') := request_item_header_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_item_discount_flag') := request_item_discount_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'request_item_tax_flag') := request_item_tax_flag;
END appendNodeValues;
constructor function "RIB_OrderItemRequestor_REC"
(
  rib_oid number
, request_item_header_flag varchar2
, request_item_discount_flag varchar2
, request_item_tax_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.request_item_header_flag := request_item_header_flag;
self.request_item_discount_flag := request_item_discount_flag;
self.request_item_tax_flag := request_item_tax_flag;
RETURN;
end;
END;
/
DROP TYPE "RIB_DateRange_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_DateRange_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  start_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_DateRange_REC"
(
  rib_oid number
, start_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_DateRange_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderCriVo') := "ns_name_CustOrderCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_DateRange_REC"
(
  rib_oid number
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
END;
/
