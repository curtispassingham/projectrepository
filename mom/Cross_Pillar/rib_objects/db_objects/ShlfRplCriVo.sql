DROP TYPE "RIB_ShlfRplCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfRplCriVo_REC";
/
DROP TYPE "RIB_ShlfRplCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfRplCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfRplCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  replenishment_id varchar2(15),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETE, CANCELED, PENDING_ALTERED, NO_VALUE] (all lower-case)
  type varchar2(20), -- type is enumeration field, valid values are [CAPACITY, SALES, AD_HOC, DISPLAY, NO_VALUE] (all lower-case)
  product_group_id number(12),
  item_id varchar2(25),
  replenishment_mode varchar2(20), -- replenishment_mode is enumeration field, valid values are [END_OF_DAY, WITHIN_DAY, NO_VALUE] (all lower-case)
  shelf_adjust_id number(15),
  user_name varchar2(128),
  from_date date,
  to_date date,
  department_id number(12),
  class_id number(12),
  subclass_id number(12),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
, class_id number
) return self as result
,constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
, class_id number
, subclass_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfRplCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfRplCriVo') := "ns_name_ShlfRplCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_id') := replenishment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_mode') := replenishment_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_adjust_id') := shelf_adjust_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_date') := from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_date') := to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
END appendNodeValues;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.from_date := from_date;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.from_date := from_date;
self.to_date := to_date;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.from_date := from_date;
self.to_date := to_date;
self.department_id := department_id;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
, class_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.from_date := from_date;
self.to_date := to_date;
self.department_id := department_id;
self.class_id := class_id;
RETURN;
end;
constructor function "RIB_ShlfRplCriVo_REC"
(
  rib_oid number
, store_id number
, replenishment_id varchar2
, status varchar2
, type varchar2
, product_group_id number
, item_id varchar2
, replenishment_mode varchar2
, shelf_adjust_id number
, user_name varchar2
, from_date date
, to_date date
, department_id number
, class_id number
, subclass_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.replenishment_id := replenishment_id;
self.status := status;
self.type := type;
self.product_group_id := product_group_id;
self.item_id := item_id;
self.replenishment_mode := replenishment_mode;
self.shelf_adjust_id := shelf_adjust_id;
self.user_name := user_name;
self.from_date := from_date;
self.to_date := to_date;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
RETURN;
end;
END;
/
