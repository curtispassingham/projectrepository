DROP TYPE "RIB_TsfShipCtnHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnHdrDesc_REC";
/
DROP TYPE "RIB_TsfShipHdrCtnSize_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TsfShipHdrCtnSize_REC";
/
DROP TYPE "RIB_TsfShipCtnHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipCtnHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carton_id number(15),
  external_id varchar2(128),
  store_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, SHIPPED, CANCELED, UNKNOWN, NO_VALUE] (all lower-case)
  TsfShipHdrCtnSize "RIB_TsfShipHdrCtnSize_REC",
  weight number(12,4),
  weight_uom varchar2(4),
  tracking_number varchar2(128),
  skus number(10),
  transfer_ids "RIB_transfer_ids_TBL",   -- Size of "RIB_transfer_ids_TBL" is 999
  order_related varchar2(5), --order_related is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipCtnHdrDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, store_id number
, status varchar2
, TsfShipHdrCtnSize "RIB_TsfShipHdrCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, skus number
, transfer_ids "RIB_transfer_ids_TBL"  -- Size of "RIB_transfer_ids_TBL" is 999
, order_related varchar2  --order_related is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipCtnHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnHdrDesc') := "ns_name_TsfShipCtnHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  l_new_pre :=i_prefix||'TsfShipHdrCtnSize.';
  TsfShipHdrCtnSize.appendNodeValues( i_prefix||'TsfShipHdrCtnSize');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'skus') := skus;
  IF transfer_ids IS NOT NULL THEN
    FOR INDX IN transfer_ids.FIRST()..transfer_ids.LAST() LOOP
      l_new_pre :=i_prefix||indx||'transfer_ids'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'transfer_ids'||'.'):=transfer_ids(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_related') := order_related;
END appendNodeValues;
constructor function "RIB_TsfShipCtnHdrDesc_REC"
(
  rib_oid number
, carton_id number
, external_id varchar2
, store_id number
, status varchar2
, TsfShipHdrCtnSize "RIB_TsfShipHdrCtnSize_REC"
, weight number
, weight_uom varchar2
, tracking_number varchar2
, skus number
, transfer_ids "RIB_transfer_ids_TBL"
, order_related varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.external_id := external_id;
self.store_id := store_id;
self.status := status;
self.TsfShipHdrCtnSize := TsfShipHdrCtnSize;
self.weight := weight;
self.weight_uom := weight_uom;
self.tracking_number := tracking_number;
self.skus := skus;
self.transfer_ids := transfer_ids;
self.order_related := order_related;
RETURN;
end;
END;
/
DROP TYPE "RIB_transfer_ids_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_transfer_ids_TBL" AS TABLE OF number(15);
/
DROP TYPE "RIB_TsfShipHdrCtnSize_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TsfShipHdrCtnSize_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TsfShipCtnHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id number(15),
  store_id number(10),
  description varchar2(128),
  height number(20,4),
  width number(20,4),
  length number(20,4),
  unit_of_measure varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TsfShipHdrCtnSize_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TsfShipHdrCtnSize_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TsfShipCtnHdrDesc') := "ns_name_TsfShipCtnHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'height') := height;
  rib_obj_util.g_RIB_element_values(i_prefix||'width') := width;
  rib_obj_util.g_RIB_element_values(i_prefix||'length') := length;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
END appendNodeValues;
constructor function "RIB_TsfShipHdrCtnSize_REC"
(
  rib_oid number
, id number
, store_id number
, description varchar2
, height number
, width number
, length number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.store_id := store_id;
self.description := description;
self.height := height;
self.width := width;
self.length := length;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
END;
/
