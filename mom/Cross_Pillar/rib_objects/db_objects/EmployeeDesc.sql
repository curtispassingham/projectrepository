DROP TYPE "RIB_EmployeeDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_EmployeeDesc_REC";
/
DROP TYPE "RIB_EmployeeDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_EmployeeDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_EmployeeDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_location varchar2(10),
  to_location varchar2(10),
  employee_id varchar2(10),
  shift number(10),
  name varchar2(30),
  department varchar2(30),
  direct varchar2(1),
  active varchar2(1),
  employee_class varchar2(10),
  employee_type varchar2(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_EmployeeDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, employee_id varchar2
, shift number
, name varchar2
, department varchar2
, direct varchar2
, active varchar2
, employee_class varchar2
, employee_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_EmployeeDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_EmployeeDesc') := "ns_name_EmployeeDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_location') := from_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_location') := to_location;
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_id') := employee_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shift') := shift;
  rib_obj_util.g_RIB_element_values(i_prefix||'name') := name;
  rib_obj_util.g_RIB_element_values(i_prefix||'department') := department;
  rib_obj_util.g_RIB_element_values(i_prefix||'direct') := direct;
  rib_obj_util.g_RIB_element_values(i_prefix||'active') := active;
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_class') := employee_class;
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_type') := employee_type;
END appendNodeValues;
constructor function "RIB_EmployeeDesc_REC"
(
  rib_oid number
, from_location varchar2
, to_location varchar2
, employee_id varchar2
, shift number
, name varchar2
, department varchar2
, direct varchar2
, active varchar2
, employee_class varchar2
, employee_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_location := from_location;
self.to_location := to_location;
self.employee_id := employee_id;
self.shift := shift;
self.name := name;
self.department := department;
self.direct := direct;
self.active := active;
self.employee_class := employee_class;
self.employee_type := employee_type;
RETURN;
end;
END;
/
