@@AddrDesc.sql;
/
@@PartnerOUDesc.sql;
/
DROP TYPE "RIB_PartnerDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PartnerDesc_REC";
/
DROP TYPE "RIB_PartnerOUDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PartnerOUDesc_TBL" AS TABLE OF "RIB_PartnerOUDesc_REC";
/
DROP TYPE "RIB_AddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_AddrDesc_TBL" AS TABLE OF "RIB_AddrDesc_REC";
/
DROP TYPE "RIB_PartnerDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PartnerDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PartnerDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  partner_type varchar2(6),
  partner_id varchar2(10),
  partner_desc varchar2(240),
  currency_code varchar2(3),
  lang number(6),
  status varchar2(1),
  contact_name varchar2(120),
  contact_phone varchar2(20),
  contact_fax varchar2(20),
  contact_telex varchar2(20),
  contact_email varchar2(100),
  mfg_id varchar2(18),
  principal_country_id varchar2(3),
  line_of_credit number(20,4),
  outstanding_credit number(20,4),
  open_credit number(20,4),
  ytd_credit number(20,4),
  ytd_drawdowns number(20,4),
  tax_id varchar2(18),
  terms varchar2(15),
  service_perf_req_ind varchar2(1),
  invc_pay_loc varchar2(6),
  invc_receive_loc varchar2(6),
  import_country_id varchar2(3),
  primary_ia_ind varchar2(1),
  PartnerOUDesc_TBL "RIB_PartnerOUDesc_TBL",   -- Size of "RIB_PartnerOUDesc_TBL" is unbounded
  AddrDesc_TBL "RIB_AddrDesc_TBL",   -- Size of "RIB_AddrDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
) return self as result
,constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
, PartnerOUDesc_TBL "RIB_PartnerOUDesc_TBL"  -- Size of "RIB_PartnerOUDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
, PartnerOUDesc_TBL "RIB_PartnerOUDesc_TBL"  -- Size of "RIB_PartnerOUDesc_TBL" is unbounded
, AddrDesc_TBL "RIB_AddrDesc_TBL"  -- Size of "RIB_AddrDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PartnerDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PartnerDesc') := "ns_name_PartnerDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_type') := partner_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_id') := partner_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'partner_desc') := partner_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_name') := contact_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_phone') := contact_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_fax') := contact_fax;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_telex') := contact_telex;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_email') := contact_email;
  rib_obj_util.g_RIB_element_values(i_prefix||'mfg_id') := mfg_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'principal_country_id') := principal_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_of_credit') := line_of_credit;
  rib_obj_util.g_RIB_element_values(i_prefix||'outstanding_credit') := outstanding_credit;
  rib_obj_util.g_RIB_element_values(i_prefix||'open_credit') := open_credit;
  rib_obj_util.g_RIB_element_values(i_prefix||'ytd_credit') := ytd_credit;
  rib_obj_util.g_RIB_element_values(i_prefix||'ytd_drawdowns') := ytd_drawdowns;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_id') := tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'terms') := terms;
  rib_obj_util.g_RIB_element_values(i_prefix||'service_perf_req_ind') := service_perf_req_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_pay_loc') := invc_pay_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'invc_receive_loc') := invc_receive_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'import_country_id') := import_country_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_ia_ind') := primary_ia_ind;
  IF PartnerOUDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PartnerOUDesc_TBL.';
    FOR INDX IN PartnerOUDesc_TBL.FIRST()..PartnerOUDesc_TBL.LAST() LOOP
      PartnerOUDesc_TBL(indx).appendNodeValues( i_prefix||indx||'PartnerOUDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF AddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'AddrDesc_TBL.';
    FOR INDX IN AddrDesc_TBL.FIRST()..AddrDesc_TBL.LAST() LOOP
      AddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'AddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.partner_type := partner_type;
self.partner_id := partner_id;
self.partner_desc := partner_desc;
self.currency_code := currency_code;
self.lang := lang;
self.status := status;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.mfg_id := mfg_id;
self.principal_country_id := principal_country_id;
self.line_of_credit := line_of_credit;
self.outstanding_credit := outstanding_credit;
self.open_credit := open_credit;
self.ytd_credit := ytd_credit;
self.ytd_drawdowns := ytd_drawdowns;
self.tax_id := tax_id;
self.terms := terms;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.import_country_id := import_country_id;
self.primary_ia_ind := primary_ia_ind;
RETURN;
end;
constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
, PartnerOUDesc_TBL "RIB_PartnerOUDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.partner_type := partner_type;
self.partner_id := partner_id;
self.partner_desc := partner_desc;
self.currency_code := currency_code;
self.lang := lang;
self.status := status;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.mfg_id := mfg_id;
self.principal_country_id := principal_country_id;
self.line_of_credit := line_of_credit;
self.outstanding_credit := outstanding_credit;
self.open_credit := open_credit;
self.ytd_credit := ytd_credit;
self.ytd_drawdowns := ytd_drawdowns;
self.tax_id := tax_id;
self.terms := terms;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.import_country_id := import_country_id;
self.primary_ia_ind := primary_ia_ind;
self.PartnerOUDesc_TBL := PartnerOUDesc_TBL;
RETURN;
end;
constructor function "RIB_PartnerDesc_REC"
(
  rib_oid number
, partner_type varchar2
, partner_id varchar2
, partner_desc varchar2
, currency_code varchar2
, lang number
, status varchar2
, contact_name varchar2
, contact_phone varchar2
, contact_fax varchar2
, contact_telex varchar2
, contact_email varchar2
, mfg_id varchar2
, principal_country_id varchar2
, line_of_credit number
, outstanding_credit number
, open_credit number
, ytd_credit number
, ytd_drawdowns number
, tax_id varchar2
, terms varchar2
, service_perf_req_ind varchar2
, invc_pay_loc varchar2
, invc_receive_loc varchar2
, import_country_id varchar2
, primary_ia_ind varchar2
, PartnerOUDesc_TBL "RIB_PartnerOUDesc_TBL"
, AddrDesc_TBL "RIB_AddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.partner_type := partner_type;
self.partner_id := partner_id;
self.partner_desc := partner_desc;
self.currency_code := currency_code;
self.lang := lang;
self.status := status;
self.contact_name := contact_name;
self.contact_phone := contact_phone;
self.contact_fax := contact_fax;
self.contact_telex := contact_telex;
self.contact_email := contact_email;
self.mfg_id := mfg_id;
self.principal_country_id := principal_country_id;
self.line_of_credit := line_of_credit;
self.outstanding_credit := outstanding_credit;
self.open_credit := open_credit;
self.ytd_credit := ytd_credit;
self.ytd_drawdowns := ytd_drawdowns;
self.tax_id := tax_id;
self.terms := terms;
self.service_perf_req_ind := service_perf_req_ind;
self.invc_pay_loc := invc_pay_loc;
self.invc_receive_loc := invc_receive_loc;
self.import_country_id := import_country_id;
self.primary_ia_ind := primary_ia_ind;
self.PartnerOUDesc_TBL := PartnerOUDesc_TBL;
self.AddrDesc_TBL := AddrDesc_TBL;
RETURN;
end;
END;
/
