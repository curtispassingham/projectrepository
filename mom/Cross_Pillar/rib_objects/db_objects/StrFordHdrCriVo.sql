DROP TYPE "RIB_StrFordHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrFordHdrCriVo_REC";
/
DROP TYPE "RIB_StrFordHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrFordHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrFordHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  release_from_date date,
  release_to_date date,
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, ACTIVE, NO_VALUE] (all lower-case)
  int_fulfillment_order_id number(12),
  ext_fulfillment_order_id varchar2(128),
  customer_order_id varchar2(128),
  bin_id varchar2(128),
  order_type varchar2(20), -- order_type is enumeration field, valid values are [LAYAWAY, PICKUP_AND_DELIVERY, CUSTOMER_ORDER, PENDING_PURCHASE, SPECIAL_ORDER, WEB_ORDER, NO_VALUE] (all lower-case)
  customer_name varchar2(128),
  item_id varchar2(25),
  store_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrFordHdrCriVo_REC"
(
  rib_oid number
, release_from_date date
, release_to_date date
, status varchar2
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, bin_id varchar2
, order_type varchar2
, customer_name varchar2
, item_id varchar2
, store_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrFordHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrFordHdrCriVo') := "ns_name_StrFordHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'release_from_date') := release_from_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'release_to_date') := release_to_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'int_fulfillment_order_id') := int_fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_fulfillment_order_id') := ext_fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bin_id') := bin_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_name') := customer_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
END appendNodeValues;
constructor function "RIB_StrFordHdrCriVo_REC"
(
  rib_oid number
, release_from_date date
, release_to_date date
, status varchar2
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, bin_id varchar2
, order_type varchar2
, customer_name varchar2
, item_id varchar2
, store_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.release_from_date := release_from_date;
self.release_to_date := release_to_date;
self.status := status;
self.int_fulfillment_order_id := int_fulfillment_order_id;
self.ext_fulfillment_order_id := ext_fulfillment_order_id;
self.customer_order_id := customer_order_id;
self.bin_id := bin_id;
self.order_type := order_type;
self.customer_name := customer_name;
self.item_id := item_id;
self.store_id := store_id;
RETURN;
end;
END;
/
