@@EOfBrPayTermRef.sql;
/
DROP TYPE "RIB_BrPayTermRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrPayTermRef_REC";
/
DROP TYPE "RIB_BrTermsSeq_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrTermsSeq_REC";
/
DROP TYPE "RIB_BrPayTermRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrPayTermRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrPayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrPayTermRef "RIB_EOfBrPayTermRef_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrPayTermRef_REC"
(
  rib_oid number
, EOfBrPayTermRef "RIB_EOfBrPayTermRef_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrPayTermRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrPayTermRef') := "ns_name_BrPayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'EOfBrPayTermRef.';
  EOfBrPayTermRef.appendNodeValues( i_prefix||'EOfBrPayTermRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrPayTermRef_REC"
(
  rib_oid number
, EOfBrPayTermRef "RIB_EOfBrPayTermRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrPayTermRef := EOfBrPayTermRef;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrTermsSeq_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrTermsSeq_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrPayTermRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  EOfBrTermsSeq "RIB_EOfBrTermsSeq_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrTermsSeq_REC"
(
  rib_oid number
, EOfBrTermsSeq "RIB_EOfBrTermsSeq_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrTermsSeq_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrPayTermRef') := "ns_name_BrPayTermRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'EOfBrTermsSeq.';
  EOfBrTermsSeq.appendNodeValues( i_prefix||'EOfBrTermsSeq');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrTermsSeq_REC"
(
  rib_oid number
, EOfBrTermsSeq "RIB_EOfBrTermsSeq_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.EOfBrTermsSeq := EOfBrTermsSeq;
RETURN;
end;
END;
/
