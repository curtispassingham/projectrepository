@@InSupplierRef.sql;
/
@@BrSupplierRef.sql;
/
DROP TYPE "RIB_LocOfSupplierRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierRef_REC";
/
DROP TYPE "RIB_LocOfSupplierSite_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierSite_REC";
/
DROP TYPE "RIB_LocOfSupplierSiteAddr_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierSiteAddr_REC";
/
DROP TYPE "RIB_InSupplierRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierRef_TBL" AS TABLE OF "RIB_InSupplierRef_REC";
/
DROP TYPE "RIB_BrSupplierRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupplierRef_TBL" AS TABLE OF "RIB_BrSupplierRef_REC";
/
DROP TYPE "RIB_LocOfSupplierRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupplierRef_TBL "RIB_InSupplierRef_TBL",   -- Size of "RIB_InSupplierRef_TBL" is unbounded
  BrSupplierRef_TBL "RIB_BrSupplierRef_TBL",   -- Size of "RIB_BrSupplierRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupplierRef_REC"
(
  rib_oid number
, InSupplierRef_TBL "RIB_InSupplierRef_TBL"  -- Size of "RIB_InSupplierRef_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupplierRef_REC"
(
  rib_oid number
, InSupplierRef_TBL "RIB_InSupplierRef_TBL"  -- Size of "RIB_InSupplierRef_TBL" is unbounded
, BrSupplierRef_TBL "RIB_BrSupplierRef_TBL"  -- Size of "RIB_BrSupplierRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupplierRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierRef') := "ns_name_LocOfSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF InSupplierRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupplierRef_TBL.';
    FOR INDX IN InSupplierRef_TBL.FIRST()..InSupplierRef_TBL.LAST() LOOP
      InSupplierRef_TBL(indx).appendNodeValues( i_prefix||indx||'InSupplierRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupplierRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupplierRef_TBL.';
    FOR INDX IN BrSupplierRef_TBL.FIRST()..BrSupplierRef_TBL.LAST() LOOP
      BrSupplierRef_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupplierRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupplierRef_REC"
(
  rib_oid number
, InSupplierRef_TBL "RIB_InSupplierRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierRef_TBL := InSupplierRef_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupplierRef_REC"
(
  rib_oid number
, InSupplierRef_TBL "RIB_InSupplierRef_TBL"
, BrSupplierRef_TBL "RIB_BrSupplierRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierRef_TBL := InSupplierRef_TBL;
self.BrSupplierRef_TBL := BrSupplierRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupplierSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierSite_TBL" AS TABLE OF "RIB_InSupplierSite_REC";
/
DROP TYPE "RIB_BrSupplierSite_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupplierSite_TBL" AS TABLE OF "RIB_BrSupplierSite_REC";
/
DROP TYPE "RIB_LocOfSupplierSite_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierSite_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupplierSite_TBL "RIB_InSupplierSite_TBL",   -- Size of "RIB_InSupplierSite_TBL" is unbounded
  BrSupplierSite_TBL "RIB_BrSupplierSite_TBL",   -- Size of "RIB_BrSupplierSite_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupplierSite_REC"
(
  rib_oid number
, InSupplierSite_TBL "RIB_InSupplierSite_TBL"  -- Size of "RIB_InSupplierSite_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupplierSite_REC"
(
  rib_oid number
, InSupplierSite_TBL "RIB_InSupplierSite_TBL"  -- Size of "RIB_InSupplierSite_TBL" is unbounded
, BrSupplierSite_TBL "RIB_BrSupplierSite_TBL"  -- Size of "RIB_BrSupplierSite_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupplierSite_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierRef') := "ns_name_LocOfSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupplierSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupplierSite_TBL.';
    FOR INDX IN InSupplierSite_TBL.FIRST()..InSupplierSite_TBL.LAST() LOOP
      InSupplierSite_TBL(indx).appendNodeValues( i_prefix||indx||'InSupplierSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupplierSite_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupplierSite_TBL.';
    FOR INDX IN BrSupplierSite_TBL.FIRST()..BrSupplierSite_TBL.LAST() LOOP
      BrSupplierSite_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupplierSite_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupplierSite_REC"
(
  rib_oid number
, InSupplierSite_TBL "RIB_InSupplierSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierSite_TBL := InSupplierSite_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupplierSite_REC"
(
  rib_oid number
, InSupplierSite_TBL "RIB_InSupplierSite_TBL"
, BrSupplierSite_TBL "RIB_BrSupplierSite_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierSite_TBL := InSupplierSite_TBL;
self.BrSupplierSite_TBL := BrSupplierSite_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_InSupplierSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_InSupplierSiteAddr_TBL" AS TABLE OF "RIB_InSupplierSiteAddr_REC";
/
DROP TYPE "RIB_BrSupplierSiteAddr_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrSupplierSiteAddr_TBL" AS TABLE OF "RIB_BrSupplierSiteAddr_REC";
/
DROP TYPE "RIB_LocOfSupplierSiteAddr_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfSupplierSiteAddr_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfSupplierRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  InSupplierSiteAddr_TBL "RIB_InSupplierSiteAddr_TBL",   -- Size of "RIB_InSupplierSiteAddr_TBL" is unbounded
  BrSupplierSiteAddr_TBL "RIB_BrSupplierSiteAddr_TBL",   -- Size of "RIB_BrSupplierSiteAddr_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfSupplierSiteAddr_REC"
(
  rib_oid number
, InSupplierSiteAddr_TBL "RIB_InSupplierSiteAddr_TBL"  -- Size of "RIB_InSupplierSiteAddr_TBL" is unbounded
) return self as result
,constructor function "RIB_LocOfSupplierSiteAddr_REC"
(
  rib_oid number
, InSupplierSiteAddr_TBL "RIB_InSupplierSiteAddr_TBL"  -- Size of "RIB_InSupplierSiteAddr_TBL" is unbounded
, BrSupplierSiteAddr_TBL "RIB_BrSupplierSiteAddr_TBL"  -- Size of "RIB_BrSupplierSiteAddr_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfSupplierSiteAddr_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfSupplierRef') := "ns_name_LocOfSupplierRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF InSupplierSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'InSupplierSiteAddr_TBL.';
    FOR INDX IN InSupplierSiteAddr_TBL.FIRST()..InSupplierSiteAddr_TBL.LAST() LOOP
      InSupplierSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'InSupplierSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF BrSupplierSiteAddr_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrSupplierSiteAddr_TBL.';
    FOR INDX IN BrSupplierSiteAddr_TBL.FIRST()..BrSupplierSiteAddr_TBL.LAST() LOOP
      BrSupplierSiteAddr_TBL(indx).appendNodeValues( i_prefix||indx||'BrSupplierSiteAddr_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfSupplierSiteAddr_REC"
(
  rib_oid number
, InSupplierSiteAddr_TBL "RIB_InSupplierSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierSiteAddr_TBL := InSupplierSiteAddr_TBL;
RETURN;
end;
constructor function "RIB_LocOfSupplierSiteAddr_REC"
(
  rib_oid number
, InSupplierSiteAddr_TBL "RIB_InSupplierSiteAddr_TBL"
, BrSupplierSiteAddr_TBL "RIB_BrSupplierSiteAddr_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.InSupplierSiteAddr_TBL := InSupplierSiteAddr_TBL;
self.BrSupplierSiteAddr_TBL := BrSupplierSiteAddr_TBL;
RETURN;
end;
END;
/
