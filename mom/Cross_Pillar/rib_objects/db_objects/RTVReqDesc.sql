DROP TYPE "RIB_RTVReqDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqDtl_REC";
/
DROP TYPE "RIB_RTVReqDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqDesc_REC";
/
DROP TYPE "RIB_RTVReqDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVReqDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVReqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(8),
  item varchar2(25),
  shipment number(12),
  inv_status number(2),
  rtv_qty number(12,4),
  unit_cost number(20,4),
  reason varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVReqDtl_REC"
(
  rib_oid number
, seq_no number
, item varchar2
, shipment number
, inv_status number
, rtv_qty number
, unit_cost number
, reason varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVReqDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVReqDesc') := "ns_name_RTVReqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment') := shipment;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_status') := inv_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'rtv_qty') := rtv_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason') := reason;
END appendNodeValues;
constructor function "RIB_RTVReqDtl_REC"
(
  rib_oid number
, seq_no number
, item varchar2
, shipment number
, inv_status number
, rtv_qty number
, unit_cost number
, reason varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.item := item;
self.shipment := shipment;
self.inv_status := inv_status;
self.rtv_qty := rtv_qty;
self.unit_cost := unit_cost;
self.reason := reason;
RETURN;
end;
END;
/
DROP TYPE "RIB_RTVReqDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVReqDtl_TBL" AS TABLE OF "RIB_RTVReqDtl_REC";
/
DROP TYPE "RIB_RTVReqDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVReqDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVReqDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  rtv_order_no number(12),
  supplier varchar2(10),
  status_ind number(2),
  loc number(10),
  loc_type varchar2(1),
  physical_loc number(10),
  total_order_amt number(20,4),
  ship_to_addr_line1 varchar2(240),
  ship_to_addr_line2 varchar2(240),
  ship_to_addr_line3 varchar2(240),
  ship_to_city varchar2(120),
  state varchar2(3),
  ship_to_country_code varchar2(3),
  ship_to_postal_code varchar2(30),
  ret_auth_nbr varchar2(12),
  ret_courier varchar2(250),
  freight_charge number(20,4),
  creation_date date,
  completed_date date,
  handling_pct number(12,4),
  handling_cost number(20,4),
  ext_ref_no varchar2(14),
  comment_desc varchar2(2000),
  RTVReqDtl_TBL "RIB_RTVReqDtl_TBL",   -- Size of "RIB_RTVReqDtl_TBL" is unbounded
  not_after_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
) return self as result
,constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
) return self as result
,constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
) return self as result
,constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
, RTVReqDtl_TBL "RIB_RTVReqDtl_TBL"  -- Size of "RIB_RTVReqDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
, RTVReqDtl_TBL "RIB_RTVReqDtl_TBL"  -- Size of "RIB_RTVReqDtl_TBL" is unbounded
, not_after_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVReqDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVReqDesc') := "ns_name_RTVReqDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'rtv_order_no') := rtv_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'status_ind') := status_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_loc') := physical_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_order_amt') := total_order_amt;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_addr_line1') := ship_to_addr_line1;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_addr_line2') := ship_to_addr_line2;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_addr_line3') := ship_to_addr_line3;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_city') := ship_to_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_country_code') := ship_to_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_postal_code') := ship_to_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_auth_nbr') := ret_auth_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'ret_courier') := ret_courier;
  rib_obj_util.g_RIB_element_values(i_prefix||'freight_charge') := freight_charge;
  rib_obj_util.g_RIB_element_values(i_prefix||'creation_date') := creation_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_date') := completed_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'handling_pct') := handling_pct;
  rib_obj_util.g_RIB_element_values(i_prefix||'handling_cost') := handling_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_ref_no') := ext_ref_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'comment_desc') := comment_desc;
  IF RTVReqDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RTVReqDtl_TBL.';
    FOR INDX IN RTVReqDtl_TBL.FIRST()..RTVReqDtl_TBL.LAST() LOOP
      RTVReqDtl_TBL(indx).appendNodeValues( i_prefix||indx||'RTVReqDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
END appendNodeValues;
constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.supplier := supplier;
self.status_ind := status_ind;
self.loc := loc;
self.loc_type := loc_type;
self.physical_loc := physical_loc;
self.total_order_amt := total_order_amt;
self.ship_to_addr_line1 := ship_to_addr_line1;
self.ship_to_addr_line2 := ship_to_addr_line2;
self.ship_to_addr_line3 := ship_to_addr_line3;
self.ship_to_city := ship_to_city;
self.state := state;
self.ship_to_country_code := ship_to_country_code;
self.ship_to_postal_code := ship_to_postal_code;
self.ret_auth_nbr := ret_auth_nbr;
self.ret_courier := ret_courier;
self.freight_charge := freight_charge;
self.creation_date := creation_date;
self.completed_date := completed_date;
self.handling_pct := handling_pct;
self.handling_cost := handling_cost;
RETURN;
end;
constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.supplier := supplier;
self.status_ind := status_ind;
self.loc := loc;
self.loc_type := loc_type;
self.physical_loc := physical_loc;
self.total_order_amt := total_order_amt;
self.ship_to_addr_line1 := ship_to_addr_line1;
self.ship_to_addr_line2 := ship_to_addr_line2;
self.ship_to_addr_line3 := ship_to_addr_line3;
self.ship_to_city := ship_to_city;
self.state := state;
self.ship_to_country_code := ship_to_country_code;
self.ship_to_postal_code := ship_to_postal_code;
self.ret_auth_nbr := ret_auth_nbr;
self.ret_courier := ret_courier;
self.freight_charge := freight_charge;
self.creation_date := creation_date;
self.completed_date := completed_date;
self.handling_pct := handling_pct;
self.handling_cost := handling_cost;
self.ext_ref_no := ext_ref_no;
RETURN;
end;
constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.supplier := supplier;
self.status_ind := status_ind;
self.loc := loc;
self.loc_type := loc_type;
self.physical_loc := physical_loc;
self.total_order_amt := total_order_amt;
self.ship_to_addr_line1 := ship_to_addr_line1;
self.ship_to_addr_line2 := ship_to_addr_line2;
self.ship_to_addr_line3 := ship_to_addr_line3;
self.ship_to_city := ship_to_city;
self.state := state;
self.ship_to_country_code := ship_to_country_code;
self.ship_to_postal_code := ship_to_postal_code;
self.ret_auth_nbr := ret_auth_nbr;
self.ret_courier := ret_courier;
self.freight_charge := freight_charge;
self.creation_date := creation_date;
self.completed_date := completed_date;
self.handling_pct := handling_pct;
self.handling_cost := handling_cost;
self.ext_ref_no := ext_ref_no;
self.comment_desc := comment_desc;
RETURN;
end;
constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
, RTVReqDtl_TBL "RIB_RTVReqDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.supplier := supplier;
self.status_ind := status_ind;
self.loc := loc;
self.loc_type := loc_type;
self.physical_loc := physical_loc;
self.total_order_amt := total_order_amt;
self.ship_to_addr_line1 := ship_to_addr_line1;
self.ship_to_addr_line2 := ship_to_addr_line2;
self.ship_to_addr_line3 := ship_to_addr_line3;
self.ship_to_city := ship_to_city;
self.state := state;
self.ship_to_country_code := ship_to_country_code;
self.ship_to_postal_code := ship_to_postal_code;
self.ret_auth_nbr := ret_auth_nbr;
self.ret_courier := ret_courier;
self.freight_charge := freight_charge;
self.creation_date := creation_date;
self.completed_date := completed_date;
self.handling_pct := handling_pct;
self.handling_cost := handling_cost;
self.ext_ref_no := ext_ref_no;
self.comment_desc := comment_desc;
self.RTVReqDtl_TBL := RTVReqDtl_TBL;
RETURN;
end;
constructor function "RIB_RTVReqDesc_REC"
(
  rib_oid number
, rtv_order_no number
, supplier varchar2
, status_ind number
, loc number
, loc_type varchar2
, physical_loc number
, total_order_amt number
, ship_to_addr_line1 varchar2
, ship_to_addr_line2 varchar2
, ship_to_addr_line3 varchar2
, ship_to_city varchar2
, state varchar2
, ship_to_country_code varchar2
, ship_to_postal_code varchar2
, ret_auth_nbr varchar2
, ret_courier varchar2
, freight_charge number
, creation_date date
, completed_date date
, handling_pct number
, handling_cost number
, ext_ref_no varchar2
, comment_desc varchar2
, RTVReqDtl_TBL "RIB_RTVReqDtl_TBL"
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.rtv_order_no := rtv_order_no;
self.supplier := supplier;
self.status_ind := status_ind;
self.loc := loc;
self.loc_type := loc_type;
self.physical_loc := physical_loc;
self.total_order_amt := total_order_amt;
self.ship_to_addr_line1 := ship_to_addr_line1;
self.ship_to_addr_line2 := ship_to_addr_line2;
self.ship_to_addr_line3 := ship_to_addr_line3;
self.ship_to_city := ship_to_city;
self.state := state;
self.ship_to_country_code := ship_to_country_code;
self.ship_to_postal_code := ship_to_postal_code;
self.ret_auth_nbr := ret_auth_nbr;
self.ret_courier := ret_courier;
self.freight_charge := freight_charge;
self.creation_date := creation_date;
self.completed_date := completed_date;
self.handling_pct := handling_pct;
self.handling_cost := handling_cost;
self.ext_ref_no := ext_ref_no;
self.comment_desc := comment_desc;
self.RTVReqDtl_TBL := RTVReqDtl_TBL;
self.not_after_date := not_after_date;
RETURN;
end;
END;
/
