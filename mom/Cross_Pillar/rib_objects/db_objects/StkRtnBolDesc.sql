DROP TYPE "RIB_StkRtnBolDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StkRtnBolDesc_REC";
/
DROP TYPE "RIB_StkRtnBolDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StkRtnBolDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StkRtnBolDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bill_of_lading_motive_id varchar2(120),
  bill_of_lading_tax_id varchar2(18),
  requested_pickup_date date,
  carrier_role varchar2(20), -- carrier_role is enumeration field, valid values are [SENDER, RECEIVER, THIRD_PARTY] (all lower-case)
  carrier_code varchar2(25),
  carrier_service_code varchar2(25),
  carrier_name varchar2(240),
  carrier_address varchar2(2000),
  ship_to_address_type varchar2(10), -- ship_to_address_type is enumeration field, valid values are [BUSINESS, POSTAL, RETURNS, ORDER, INVOICE, REMITTANCE] (all lower-case)
  alternate_ship_to_address varchar2(2000),
  tracking_number varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result
,constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result
,constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StkRtnBolDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StkRtnBolDesc') := "ns_name_StkRtnBolDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_motive_id') := bill_of_lading_motive_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_tax_id') := bill_of_lading_tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pickup_date') := requested_pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_role') := carrier_role;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_address') := carrier_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_type') := ship_to_address_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_ship_to_address') := alternate_ship_to_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
END appendNodeValues;
constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
RETURN;
end;
constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
RETURN;
end;
constructor function "RIB_StkRtnBolDesc_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
self.tracking_number := tracking_number;
RETURN;
end;
END;
/
