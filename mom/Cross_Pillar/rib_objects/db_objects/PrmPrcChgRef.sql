DROP TYPE "RIB_PrmPrcChgThrDtlRe_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtlRe_REC";
/
DROP TYPE "RIB_PrmPrcChgSmpDtlRe_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmpDtlRe_REC";
/
DROP TYPE "RIB_PrmPrcChgDtlRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtlRef_REC";
/
DROP TYPE "RIB_PrmPrcChgRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgRef_REC";
/
DROP TYPE "RIB_PrmPrcChgThrDtlRe_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtlRe_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgThrDtlRe_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgThrDtlRe_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgRef') := "ns_name_PrmPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_PrmPrcChgThrDtlRe_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgSmpDtlRe_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmpDtlRe_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgSmpDtlRe_REC"
(
  rib_oid number
, item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgSmpDtlRe_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgRef') := "ns_name_PrmPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
END appendNodeValues;
constructor function "RIB_PrmPrcChgSmpDtlRe_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgSmpDtlRe_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgSmpDtlRe_TBL" AS TABLE OF "RIB_PrmPrcChgSmpDtlRe_REC";
/
DROP TYPE "RIB_PrmPrcChgThrDtlRe_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgThrDtlRe_TBL" AS TABLE OF "RIB_PrmPrcChgThrDtlRe_REC";
/
DROP TYPE "RIB_PrmPrcChgDtlRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtlRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  promo_comp_detail_id number(10),
  PrmPrcChgSmpDtlRe_TBL "RIB_PrmPrcChgSmpDtlRe_TBL",   -- Size of "RIB_PrmPrcChgSmpDtlRe_TBL" is unbounded
  PrmPrcChgThrDtlRe_TBL "RIB_PrmPrcChgThrDtlRe_TBL",   -- Size of "RIB_PrmPrcChgThrDtlRe_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
) return self as result
,constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
, PrmPrcChgSmpDtlRe_TBL "RIB_PrmPrcChgSmpDtlRe_TBL"  -- Size of "RIB_PrmPrcChgSmpDtlRe_TBL" is unbounded
) return self as result
,constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
, PrmPrcChgSmpDtlRe_TBL "RIB_PrmPrcChgSmpDtlRe_TBL"  -- Size of "RIB_PrmPrcChgSmpDtlRe_TBL" is unbounded
, PrmPrcChgThrDtlRe_TBL "RIB_PrmPrcChgThrDtlRe_TBL"  -- Size of "RIB_PrmPrcChgThrDtlRe_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgDtlRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgRef') := "ns_name_PrmPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_detail_id') := promo_comp_detail_id;
  IF PrmPrcChgSmpDtlRe_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrmPrcChgSmpDtlRe_TBL.';
    FOR INDX IN PrmPrcChgSmpDtlRe_TBL.FIRST()..PrmPrcChgSmpDtlRe_TBL.LAST() LOOP
      PrmPrcChgSmpDtlRe_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgSmpDtlRe_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF PrmPrcChgThrDtlRe_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrmPrcChgThrDtlRe_TBL.';
    FOR INDX IN PrmPrcChgThrDtlRe_TBL.FIRST()..PrmPrcChgThrDtlRe_TBL.LAST() LOOP
      PrmPrcChgThrDtlRe_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgThrDtlRe_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_comp_detail_id := promo_comp_detail_id;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
, PrmPrcChgSmpDtlRe_TBL "RIB_PrmPrcChgSmpDtlRe_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_comp_detail_id := promo_comp_detail_id;
self.PrmPrcChgSmpDtlRe_TBL := PrmPrcChgSmpDtlRe_TBL;
RETURN;
end;
constructor function "RIB_PrmPrcChgDtlRef_REC"
(
  rib_oid number
, promo_comp_detail_id number
, PrmPrcChgSmpDtlRe_TBL "RIB_PrmPrcChgSmpDtlRe_TBL"
, PrmPrcChgThrDtlRe_TBL "RIB_PrmPrcChgThrDtlRe_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.promo_comp_detail_id := promo_comp_detail_id;
self.PrmPrcChgSmpDtlRe_TBL := PrmPrcChgSmpDtlRe_TBL;
self.PrmPrcChgThrDtlRe_TBL := PrmPrcChgThrDtlRe_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrmPrcChgDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgDtlRef_TBL" AS TABLE OF "RIB_PrmPrcChgDtlRef_REC";
/
DROP TYPE "RIB_PrmPrcChgRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrmPrcChgRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrmPrcChgRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1),
  PrmPrcChgDtlRef_TBL "RIB_PrmPrcChgDtlRef_TBL",   -- Size of "RIB_PrmPrcChgDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrmPrcChgRef_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, PrmPrcChgDtlRef_TBL "RIB_PrmPrcChgDtlRef_TBL"  -- Size of "RIB_PrmPrcChgDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrmPrcChgRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrmPrcChgRef') := "ns_name_PrmPrcChgRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  l_new_pre :=i_prefix||'PrmPrcChgDtlRef_TBL.';
  FOR INDX IN PrmPrcChgDtlRef_TBL.FIRST()..PrmPrcChgDtlRef_TBL.LAST() LOOP
    PrmPrcChgDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'PrmPrcChgDtlRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrmPrcChgRef_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, PrmPrcChgDtlRef_TBL "RIB_PrmPrcChgDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.PrmPrcChgDtlRef_TBL := PrmPrcChgDtlRef_TBL;
RETURN;
end;
END;
/
