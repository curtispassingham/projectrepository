DROP TYPE "RIB_StrFordDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrFordDesc_REC";
/
DROP TYPE "RIB_StrFordCust_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrFordCust_REC";
/
DROP TYPE "RIB_StrFordItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrFordItm_REC";
/
DROP TYPE "RIB_StrFordItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrFordItm_TBL" AS TABLE OF "RIB_StrFordItm_REC";
/
DROP TYPE "RIB_StrFordDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrFordDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrFordDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  int_fulfillment_order_id number(12),
  ext_fulfillment_order_id varchar2(128),
  customer_order_id varchar2(128),
  order_type varchar2(20), -- order_type is enumeration field, valid values are [LAYAWAY, PICKUP_AND_DELIVERY, CUSTOMER_ORDER, PENDING_PURCHASE, SPECIAL_ORDER, WEB_ORDER, UNKNOWN] (all lower-case)
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_date date,
  release_date date,
  delivery_date date,
  delivery_type varchar2(20), -- delivery_type is enumeration field, valid values are [PICKUP, SHIPMENT, UNKNOWN] (all lower-case)
  delivery_carrier_code varchar2(128),
  delivery_service_code varchar2(128),
  allow_partial_delivery varchar2(5), --allow_partial_delivery is boolean field, valid values are true,false (all lower-case) 
  comments varchar2(2000),
  StrFordCust "RIB_StrFordCust_REC",
  StrFordItm_TBL "RIB_StrFordItm_TBL",   -- Size of "RIB_StrFordItm_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2  --allow_partial_delivery is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2  --allow_partial_delivery is boolean field, valid values are true,false (all lower-case)
, comments varchar2
) return self as result
,constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2  --allow_partial_delivery is boolean field, valid values are true,false (all lower-case)
, comments varchar2
, StrFordCust "RIB_StrFordCust_REC"
) return self as result
,constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2  --allow_partial_delivery is boolean field, valid values are true,false (all lower-case)
, comments varchar2
, StrFordCust "RIB_StrFordCust_REC"
, StrFordItm_TBL "RIB_StrFordItm_TBL"  -- Size of "RIB_StrFordItm_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrFordDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrFordDesc') := "ns_name_StrFordDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'int_fulfillment_order_id') := int_fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ext_fulfillment_order_id') := ext_fulfillment_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_type') := order_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'release_date') := release_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_type') := delivery_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_carrier_code') := delivery_carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_service_code') := delivery_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'allow_partial_delivery') := allow_partial_delivery;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  l_new_pre :=i_prefix||'StrFordCust.';
  StrFordCust.appendNodeValues( i_prefix||'StrFordCust');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF StrFordItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrFordItm_TBL.';
    FOR INDX IN StrFordItm_TBL.FIRST()..StrFordItm_TBL.LAST() LOOP
      StrFordItm_TBL(indx).appendNodeValues( i_prefix||indx||'StrFordItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfillment_order_id := int_fulfillment_order_id;
self.ext_fulfillment_order_id := ext_fulfillment_order_id;
self.customer_order_id := customer_order_id;
self.order_type := order_type;
self.status := status;
self.create_date := create_date;
self.release_date := release_date;
self.delivery_date := delivery_date;
self.delivery_type := delivery_type;
self.delivery_carrier_code := delivery_carrier_code;
self.delivery_service_code := delivery_service_code;
self.allow_partial_delivery := allow_partial_delivery;
RETURN;
end;
constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfillment_order_id := int_fulfillment_order_id;
self.ext_fulfillment_order_id := ext_fulfillment_order_id;
self.customer_order_id := customer_order_id;
self.order_type := order_type;
self.status := status;
self.create_date := create_date;
self.release_date := release_date;
self.delivery_date := delivery_date;
self.delivery_type := delivery_type;
self.delivery_carrier_code := delivery_carrier_code;
self.delivery_service_code := delivery_service_code;
self.allow_partial_delivery := allow_partial_delivery;
self.comments := comments;
RETURN;
end;
constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2
, comments varchar2
, StrFordCust "RIB_StrFordCust_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfillment_order_id := int_fulfillment_order_id;
self.ext_fulfillment_order_id := ext_fulfillment_order_id;
self.customer_order_id := customer_order_id;
self.order_type := order_type;
self.status := status;
self.create_date := create_date;
self.release_date := release_date;
self.delivery_date := delivery_date;
self.delivery_type := delivery_type;
self.delivery_carrier_code := delivery_carrier_code;
self.delivery_service_code := delivery_service_code;
self.allow_partial_delivery := allow_partial_delivery;
self.comments := comments;
self.StrFordCust := StrFordCust;
RETURN;
end;
constructor function "RIB_StrFordDesc_REC"
(
  rib_oid number
, int_fulfillment_order_id number
, ext_fulfillment_order_id varchar2
, customer_order_id varchar2
, order_type varchar2
, status varchar2
, create_date date
, release_date date
, delivery_date date
, delivery_type varchar2
, delivery_carrier_code varchar2
, delivery_service_code varchar2
, allow_partial_delivery varchar2
, comments varchar2
, StrFordCust "RIB_StrFordCust_REC"
, StrFordItm_TBL "RIB_StrFordItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfillment_order_id := int_fulfillment_order_id;
self.ext_fulfillment_order_id := ext_fulfillment_order_id;
self.customer_order_id := customer_order_id;
self.order_type := order_type;
self.status := status;
self.create_date := create_date;
self.release_date := release_date;
self.delivery_date := delivery_date;
self.delivery_type := delivery_type;
self.delivery_carrier_code := delivery_carrier_code;
self.delivery_service_code := delivery_service_code;
self.allow_partial_delivery := allow_partial_delivery;
self.comments := comments;
self.StrFordCust := StrFordCust;
self.StrFordItm_TBL := StrFordItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrFordCust_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrFordCust_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrFordDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_id varchar2(14),
  delivery_first_name varchar2(120),
  delivery_phonetic_first_name varchar2(120),
  delivery_last_name varchar2(120),
  delivery_phonetic_last_name varchar2(120),
  delivery_preferred_name varchar2(120),
  delivery_company_name varchar2(120),
  delivery_address1 varchar2(240),
  delivery_address2 varchar2(240),
  delivery_address3 varchar2(240),
  delivery_county varchar2(120),
  delivery_city varchar2(120),
  delivery_state varchar2(3),
  delivery_postal_code varchar2(30),
  delivery_country varchar2(3),
  delivery_phone varchar2(20),
  billing_first_name varchar2(120),
  billing_phonetic_first_name varchar2(120),
  billing_last_name varchar2(120),
  billing_phonetic_last_name varchar2(120),
  billing_preferred_name varchar2(120),
  billing_company_name varchar2(120),
  billing_address1 varchar2(240),
  billing_address2 varchar2(240),
  billing_address3 varchar2(240),
  billing_county varchar2(120),
  billing_city varchar2(120),
  billing_state varchar2(3),
  billing_postal_code varchar2(30),
  billing_country varchar2(3),
  billing_phone varchar2(20),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
, billing_country varchar2
) return self as result
,constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
, billing_country varchar2
, billing_phone varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrFordCust_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrFordDesc') := "ns_name_StrFordDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_id') := customer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_first_name') := delivery_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_phonetic_first_name') := delivery_phonetic_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_last_name') := delivery_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_phonetic_last_name') := delivery_phonetic_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_preferred_name') := delivery_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_company_name') := delivery_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_address1') := delivery_address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_address2') := delivery_address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_address3') := delivery_address3;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_county') := delivery_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_city') := delivery_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_state') := delivery_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_postal_code') := delivery_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_country') := delivery_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_phone') := delivery_phone;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_first_name') := billing_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_phonetic_first_name') := billing_phonetic_first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_last_name') := billing_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_phonetic_last_name') := billing_phonetic_last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_preferred_name') := billing_preferred_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_company_name') := billing_company_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_address1') := billing_address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_address2') := billing_address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_address3') := billing_address3;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_county') := billing_county;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_city') := billing_city;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_state') := billing_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_postal_code') := billing_postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_country') := billing_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'billing_phone') := billing_phone;
END appendNodeValues;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
self.billing_city := billing_city;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
self.billing_city := billing_city;
self.billing_state := billing_state;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
self.billing_city := billing_city;
self.billing_state := billing_state;
self.billing_postal_code := billing_postal_code;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
, billing_country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
self.billing_city := billing_city;
self.billing_state := billing_state;
self.billing_postal_code := billing_postal_code;
self.billing_country := billing_country;
RETURN;
end;
constructor function "RIB_StrFordCust_REC"
(
  rib_oid number
, customer_id varchar2
, delivery_first_name varchar2
, delivery_phonetic_first_name varchar2
, delivery_last_name varchar2
, delivery_phonetic_last_name varchar2
, delivery_preferred_name varchar2
, delivery_company_name varchar2
, delivery_address1 varchar2
, delivery_address2 varchar2
, delivery_address3 varchar2
, delivery_county varchar2
, delivery_city varchar2
, delivery_state varchar2
, delivery_postal_code varchar2
, delivery_country varchar2
, delivery_phone varchar2
, billing_first_name varchar2
, billing_phonetic_first_name varchar2
, billing_last_name varchar2
, billing_phonetic_last_name varchar2
, billing_preferred_name varchar2
, billing_company_name varchar2
, billing_address1 varchar2
, billing_address2 varchar2
, billing_address3 varchar2
, billing_county varchar2
, billing_city varchar2
, billing_state varchar2
, billing_postal_code varchar2
, billing_country varchar2
, billing_phone varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
self.delivery_first_name := delivery_first_name;
self.delivery_phonetic_first_name := delivery_phonetic_first_name;
self.delivery_last_name := delivery_last_name;
self.delivery_phonetic_last_name := delivery_phonetic_last_name;
self.delivery_preferred_name := delivery_preferred_name;
self.delivery_company_name := delivery_company_name;
self.delivery_address1 := delivery_address1;
self.delivery_address2 := delivery_address2;
self.delivery_address3 := delivery_address3;
self.delivery_county := delivery_county;
self.delivery_city := delivery_city;
self.delivery_state := delivery_state;
self.delivery_postal_code := delivery_postal_code;
self.delivery_country := delivery_country;
self.delivery_phone := delivery_phone;
self.billing_first_name := billing_first_name;
self.billing_phonetic_first_name := billing_phonetic_first_name;
self.billing_last_name := billing_last_name;
self.billing_phonetic_last_name := billing_phonetic_last_name;
self.billing_preferred_name := billing_preferred_name;
self.billing_company_name := billing_company_name;
self.billing_address1 := billing_address1;
self.billing_address2 := billing_address2;
self.billing_address3 := billing_address3;
self.billing_county := billing_county;
self.billing_city := billing_city;
self.billing_state := billing_state;
self.billing_postal_code := billing_postal_code;
self.billing_country := billing_country;
self.billing_phone := billing_phone;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrFordItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrFordItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrFordDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  description varchar2(250),
  substitute_item_id varchar2(25),
  preferred_uom varchar2(40),
  standard_uom varchar2(40),
  order_qty number(9,2),
  picked_qty number(9,2),
  delivered_qty number(9,2),
  canceled_qty number(9,2),
  remaining_qty number(9,2),
  reserved_qty number(9,2),
  allow_substitution varchar2(5), --allow_substitution is boolean field, valid values are true,false (all lower-case) 
  create_date date,
  last_update_date date,
  comments varchar2(512),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrFordItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, substitute_item_id varchar2
, preferred_uom varchar2
, standard_uom varchar2
, order_qty number
, picked_qty number
, delivered_qty number
, canceled_qty number
, remaining_qty number
, reserved_qty number
, allow_substitution varchar2  --allow_substitution is boolean field, valid values are true,false (all lower-case)
, create_date date
, last_update_date date
) return self as result
,constructor function "RIB_StrFordItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, substitute_item_id varchar2
, preferred_uom varchar2
, standard_uom varchar2
, order_qty number
, picked_qty number
, delivered_qty number
, canceled_qty number
, remaining_qty number
, reserved_qty number
, allow_substitution varchar2  --allow_substitution is boolean field, valid values are true,false (all lower-case)
, create_date date
, last_update_date date
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrFordItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrFordDesc') := "ns_name_StrFordDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'substitute_item_id') := substitute_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'preferred_uom') := preferred_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'standard_uom') := standard_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_qty') := order_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'picked_qty') := picked_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivered_qty') := delivered_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'canceled_qty') := canceled_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'remaining_qty') := remaining_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'reserved_qty') := reserved_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'allow_substitution') := allow_substitution;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_update_date') := last_update_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_StrFordItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, substitute_item_id varchar2
, preferred_uom varchar2
, standard_uom varchar2
, order_qty number
, picked_qty number
, delivered_qty number
, canceled_qty number
, remaining_qty number
, reserved_qty number
, allow_substitution varchar2
, create_date date
, last_update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.substitute_item_id := substitute_item_id;
self.preferred_uom := preferred_uom;
self.standard_uom := standard_uom;
self.order_qty := order_qty;
self.picked_qty := picked_qty;
self.delivered_qty := delivered_qty;
self.canceled_qty := canceled_qty;
self.remaining_qty := remaining_qty;
self.reserved_qty := reserved_qty;
self.allow_substitution := allow_substitution;
self.create_date := create_date;
self.last_update_date := last_update_date;
RETURN;
end;
constructor function "RIB_StrFordItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, description varchar2
, substitute_item_id varchar2
, preferred_uom varchar2
, standard_uom varchar2
, order_qty number
, picked_qty number
, delivered_qty number
, canceled_qty number
, remaining_qty number
, reserved_qty number
, allow_substitution varchar2
, create_date date
, last_update_date date
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.description := description;
self.substitute_item_id := substitute_item_id;
self.preferred_uom := preferred_uom;
self.standard_uom := standard_uom;
self.order_qty := order_qty;
self.picked_qty := picked_qty;
self.delivered_qty := delivered_qty;
self.canceled_qty := canceled_qty;
self.remaining_qty := remaining_qty;
self.reserved_qty := reserved_qty;
self.allow_substitution := allow_substitution;
self.create_date := create_date;
self.last_update_date := last_update_date;
self.comments := comments;
RETURN;
end;
END;
/
