DROP TYPE "RIB_EmailDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_EmailDesc_REC";
/
DROP TYPE "RIB_EmailDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_EmailDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_EmailDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  email_id number(22),
  email_type varchar2(5), -- email_type is enumeration field, valid values are [HOME, WORK, OTHER] (all lower-case)
  primary_email_ind varchar2(5), --primary_email_ind is boolean field, valid values are true,false (all lower-case) 
  email_address varchar2(64),
  format_preference_code varchar2(5), -- format_preference_code is enumeration field, valid values are [TEXT, HTML, OTHER] (all lower-case)
  contact_preference_code varchar2(120),
  validated_email_flag varchar2(5), --validated_email_flag is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2  --primary_email_ind is boolean field, valid values are true,false (all lower-case)
, email_address varchar2
) return self as result
,constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2  --primary_email_ind is boolean field, valid values are true,false (all lower-case)
, email_address varchar2
, format_preference_code varchar2
) return self as result
,constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2  --primary_email_ind is boolean field, valid values are true,false (all lower-case)
, email_address varchar2
, format_preference_code varchar2
, contact_preference_code varchar2
) return self as result
,constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2  --primary_email_ind is boolean field, valid values are true,false (all lower-case)
, email_address varchar2
, format_preference_code varchar2
, contact_preference_code varchar2
, validated_email_flag varchar2  --validated_email_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_EmailDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_EmailDesc') := "ns_name_EmailDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'email_id') := email_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_type') := email_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_email_ind') := primary_email_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'email_address') := email_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_preference_code') := format_preference_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_preference_code') := contact_preference_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'validated_email_flag') := validated_email_flag;
END appendNodeValues;
constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2
, email_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.email_id := email_id;
self.email_type := email_type;
self.primary_email_ind := primary_email_ind;
self.email_address := email_address;
RETURN;
end;
constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2
, email_address varchar2
, format_preference_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.email_id := email_id;
self.email_type := email_type;
self.primary_email_ind := primary_email_ind;
self.email_address := email_address;
self.format_preference_code := format_preference_code;
RETURN;
end;
constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2
, email_address varchar2
, format_preference_code varchar2
, contact_preference_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.email_id := email_id;
self.email_type := email_type;
self.primary_email_ind := primary_email_ind;
self.email_address := email_address;
self.format_preference_code := format_preference_code;
self.contact_preference_code := contact_preference_code;
RETURN;
end;
constructor function "RIB_EmailDesc_REC"
(
  rib_oid number
, email_id number
, email_type varchar2
, primary_email_ind varchar2
, email_address varchar2
, format_preference_code varchar2
, contact_preference_code varchar2
, validated_email_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.email_id := email_id;
self.email_type := email_type;
self.primary_email_ind := primary_email_ind;
self.email_address := email_address;
self.format_preference_code := format_preference_code;
self.contact_preference_code := contact_preference_code;
self.validated_email_flag := validated_email_flag;
RETURN;
end;
END;
/
