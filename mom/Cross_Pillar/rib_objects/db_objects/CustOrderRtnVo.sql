@@CustOrdItmRtColVo.sql;
/
DROP TYPE "RIB_CustOrderRtnVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrderRtnVo_REC";
/
DROP TYPE "RIB_CustOrderRtnVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrderRtnVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderRtnVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_id varchar2(48),
  currency_code varchar2(3),
  returned_amount number(20,4),
  returned_discount_amount number(20,4),
  returned_tax_amount number(20,4),
  returned_inclusive_tax_amount number(20,4),
  update_timestamp date,
  CustOrdItmRtColVo "RIB_CustOrdItmRtColVo_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrderRtnVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, returned_amount number
, returned_discount_amount number
, returned_tax_amount number
, returned_inclusive_tax_amount number
, update_timestamp date
, CustOrdItmRtColVo "RIB_CustOrdItmRtColVo_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrderRtnVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderRtnVo') := "ns_name_CustOrderRtnVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_amount') := returned_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_discount_amount') := returned_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_tax_amount') := returned_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_inclusive_tax_amount') := returned_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_timestamp') := update_timestamp;
  l_new_pre :=i_prefix||'CustOrdItmRtColVo.';
  CustOrdItmRtColVo.appendNodeValues( i_prefix||'CustOrdItmRtColVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrderRtnVo_REC"
(
  rib_oid number
, customer_order_id varchar2
, currency_code varchar2
, returned_amount number
, returned_discount_amount number
, returned_tax_amount number
, returned_inclusive_tax_amount number
, update_timestamp date
, CustOrdItmRtColVo "RIB_CustOrdItmRtColVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.currency_code := currency_code;
self.returned_amount := returned_amount;
self.returned_discount_amount := returned_discount_amount;
self.returned_tax_amount := returned_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.update_timestamp := update_timestamp;
self.CustOrdItmRtColVo := CustOrdItmRtColVo;
RETURN;
end;
END;
/
