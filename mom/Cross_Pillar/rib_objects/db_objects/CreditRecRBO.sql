DROP TYPE "RIB_CreditRecRBO_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CreditRecRBO_REC";
/
DROP TYPE "RIB_CreditRecRBO_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CreditRecRBO_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CreditRecRBO" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  src_federal_tax_reg_id varchar2(25),
  dst_federal_tax_reg_id varchar2(25),
  src_ref_federal_tax_reg_id varchar2(25),
  item_code varchar2(25),
  item_id varchar2(25),
  document_series varchar2(25),
  document_number varchar2(25),
  fiscal_document_date date,
  ref_document_series varchar2(25),
  ref_document_number varchar2(25),
  ref_fiscal_document_date date,
  quantity number(12,4),
  quantity_recovered number(12,4),
  unit_of_measure varchar2(25),
  total_cost number(20,4),
  icms_taxable_basis_amount number(20,4),
  icms_tax_amount number(20,4),
  icmsst_taxable_basis_amount number(20,4),
  icmsst_tax_amount number(20,4),
  icmsste_taxable_basis_amount number(20,4),
  icmsste_tax_amount number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
, icmsste_taxable_basis_amount number
) return self as result
,constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
, icmsste_taxable_basis_amount number
, icmsste_tax_amount number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CreditRecRBO_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CreditRecRBO') := "ns_name_CreditRecRBO";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'src_federal_tax_reg_id') := src_federal_tax_reg_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dst_federal_tax_reg_id') := dst_federal_tax_reg_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'src_ref_federal_tax_reg_id') := src_ref_federal_tax_reg_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_code') := item_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_series') := document_series;
  rib_obj_util.g_RIB_element_values(i_prefix||'document_number') := document_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_document_date') := fiscal_document_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_document_series') := ref_document_series;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_document_number') := ref_document_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'ref_fiscal_document_date') := ref_fiscal_document_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_recovered') := quantity_recovered;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'total_cost') := total_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_taxable_basis_amount') := icms_taxable_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_tax_amount') := icms_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'icmsst_taxable_basis_amount') := icmsst_taxable_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'icmsst_tax_amount') := icmsst_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'icmsste_taxable_basis_amount') := icmsste_taxable_basis_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'icmsste_tax_amount') := icmsste_tax_amount;
END appendNodeValues;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
self.icms_tax_amount := icms_tax_amount;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
self.icms_tax_amount := icms_tax_amount;
self.icmsst_taxable_basis_amount := icmsst_taxable_basis_amount;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
self.icms_tax_amount := icms_tax_amount;
self.icmsst_taxable_basis_amount := icmsst_taxable_basis_amount;
self.icmsst_tax_amount := icmsst_tax_amount;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
, icmsste_taxable_basis_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
self.icms_tax_amount := icms_tax_amount;
self.icmsst_taxable_basis_amount := icmsst_taxable_basis_amount;
self.icmsst_tax_amount := icmsst_tax_amount;
self.icmsste_taxable_basis_amount := icmsste_taxable_basis_amount;
RETURN;
end;
constructor function "RIB_CreditRecRBO_REC"
(
  rib_oid number
, src_federal_tax_reg_id varchar2
, dst_federal_tax_reg_id varchar2
, src_ref_federal_tax_reg_id varchar2
, item_code varchar2
, item_id varchar2
, document_series varchar2
, document_number varchar2
, fiscal_document_date date
, ref_document_series varchar2
, ref_document_number varchar2
, ref_fiscal_document_date date
, quantity number
, quantity_recovered number
, unit_of_measure varchar2
, total_cost number
, icms_taxable_basis_amount number
, icms_tax_amount number
, icmsst_taxable_basis_amount number
, icmsst_tax_amount number
, icmsste_taxable_basis_amount number
, icmsste_tax_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.src_federal_tax_reg_id := src_federal_tax_reg_id;
self.dst_federal_tax_reg_id := dst_federal_tax_reg_id;
self.src_ref_federal_tax_reg_id := src_ref_federal_tax_reg_id;
self.item_code := item_code;
self.item_id := item_id;
self.document_series := document_series;
self.document_number := document_number;
self.fiscal_document_date := fiscal_document_date;
self.ref_document_series := ref_document_series;
self.ref_document_number := ref_document_number;
self.ref_fiscal_document_date := ref_fiscal_document_date;
self.quantity := quantity;
self.quantity_recovered := quantity_recovered;
self.unit_of_measure := unit_of_measure;
self.total_cost := total_cost;
self.icms_taxable_basis_amount := icms_taxable_basis_amount;
self.icms_tax_amount := icms_tax_amount;
self.icmsst_taxable_basis_amount := icmsst_taxable_basis_amount;
self.icmsst_tax_amount := icmsst_tax_amount;
self.icmsste_taxable_basis_amount := icmsste_taxable_basis_amount;
self.icmsste_tax_amount := icmsste_tax_amount;
RETURN;
end;
END;
/
