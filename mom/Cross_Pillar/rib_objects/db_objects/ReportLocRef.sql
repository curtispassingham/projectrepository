@@ExtOfReportLocRef.sql;
/
@@LocOfReportLocRef.sql;
/
DROP TYPE "RIB_ReportLocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReportLocRef_REC";
/
DROP TYPE "RIB_LocOfReportLocRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfReportLocRef_TBL" AS TABLE OF "RIB_LocOfReportLocRef_REC";
/
DROP TYPE "RIB_ReportLocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReportLocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ReportLocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  report_ref_key varchar2(32),
  report_url varchar2(256),
  ExtOfReportLocRef "RIB_ExtOfReportLocRef_REC",
  LocOfReportLocRef_TBL "RIB_LocOfReportLocRef_TBL",   -- Size of "RIB_LocOfReportLocRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
) return self as result
,constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
, ExtOfReportLocRef "RIB_ExtOfReportLocRef_REC"
) return self as result
,constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
, ExtOfReportLocRef "RIB_ExtOfReportLocRef_REC"
, LocOfReportLocRef_TBL "RIB_LocOfReportLocRef_TBL"  -- Size of "RIB_LocOfReportLocRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReportLocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ReportLocRef') := "ns_name_ReportLocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'report_ref_key') := report_ref_key;
  rib_obj_util.g_RIB_element_values(i_prefix||'report_url') := report_url;
  l_new_pre :=i_prefix||'ExtOfReportLocRef.';
  ExtOfReportLocRef.appendNodeValues( i_prefix||'ExtOfReportLocRef');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF LocOfReportLocRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocOfReportLocRef_TBL.';
    FOR INDX IN LocOfReportLocRef_TBL.FIRST()..LocOfReportLocRef_TBL.LAST() LOOP
      LocOfReportLocRef_TBL(indx).appendNodeValues( i_prefix||indx||'LocOfReportLocRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.report_ref_key := report_ref_key;
self.report_url := report_url;
RETURN;
end;
constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
, ExtOfReportLocRef "RIB_ExtOfReportLocRef_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.report_ref_key := report_ref_key;
self.report_url := report_url;
self.ExtOfReportLocRef := ExtOfReportLocRef;
RETURN;
end;
constructor function "RIB_ReportLocRef_REC"
(
  rib_oid number
, report_ref_key varchar2
, report_url varchar2
, ExtOfReportLocRef "RIB_ExtOfReportLocRef_REC"
, LocOfReportLocRef_TBL "RIB_LocOfReportLocRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.report_ref_key := report_ref_key;
self.report_url := report_url;
self.ExtOfReportLocRef := ExtOfReportLocRef;
self.LocOfReportLocRef_TBL := LocOfReportLocRef_TBL;
RETURN;
end;
END;
/
