DROP TYPE "RIB_StrItmHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmHdrDesc_REC";
/
DROP TYPE "RIB_StrItmHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  store_id number(10),
  item_type varchar2(30), -- item_type is enumeration field, valid values are [ITEM, CONSIGNMENT, CONCESSION, NON_INVENTORY, PACK_SIMPLE, PACK_COMPLEX, PACK_SIMPLE_BREAKABLE, PACK_COMPLEX_BREAKABLE, UNKNOWN] (all lower-case)
  short_description varchar2(120),
  long_description varchar2(250),
  department_name varchar2(120),
  class_name varchar2(120),
  subclass_name varchar2(120),
  primary_supplier_id varchar2(128),
  primary_supplier_name varchar2(40),
  ranged varchar2(5), --ranged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmHdrDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, item_type varchar2
, short_description varchar2
, long_description varchar2
, department_name varchar2
, class_name varchar2
, subclass_name varchar2
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, ranged varchar2  --ranged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmHdrDesc') := "ns_name_StrItmHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_type') := item_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_description') := short_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'long_description') := long_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_name') := department_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_name') := class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_name') := subclass_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supplier_id') := primary_supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supplier_name') := primary_supplier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'ranged') := ranged;
END appendNodeValues;
constructor function "RIB_StrItmHdrDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, item_type varchar2
, short_description varchar2
, long_description varchar2
, department_name varchar2
, class_name varchar2
, subclass_name varchar2
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, ranged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.item_type := item_type;
self.short_description := short_description;
self.long_description := long_description;
self.department_name := department_name;
self.class_name := class_name;
self.subclass_name := subclass_name;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.ranged := ranged;
RETURN;
end;
END;
/
