DROP TYPE "RIB_FiscalFDNQryRBM_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FiscalFDNQryRBM_REC";
/
DROP TYPE "RIB_FiscalFDNQryRBM_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FiscalFDNQryRBM_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FiscalFDNQryRBM" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  fiscal_code varchar2(30),
  cut_off_date date,
  begin_index number(9),
  end_index number(9),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
) return self as result
,constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
) return self as result
,constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
, begin_index number
) return self as result
,constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
, begin_index number
, end_index number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FiscalFDNQryRBM_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FiscalFDNQryRBM') := "ns_name_FiscalFDNQryRBM";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_code') := fiscal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'cut_off_date') := cut_off_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'begin_index') := begin_index;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_index') := end_index;
END appendNodeValues;
constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
RETURN;
end;
constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.cut_off_date := cut_off_date;
RETURN;
end;
constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
, begin_index number
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.cut_off_date := cut_off_date;
self.begin_index := begin_index;
RETURN;
end;
constructor function "RIB_FiscalFDNQryRBM_REC"
(
  rib_oid number
, fiscal_code varchar2
, cut_off_date date
, begin_index number
, end_index number
) return self as result is
begin
self.rib_oid := rib_oid;
self.fiscal_code := fiscal_code;
self.cut_off_date := cut_off_date;
self.begin_index := begin_index;
self.end_index := end_index;
RETURN;
end;
END;
/
