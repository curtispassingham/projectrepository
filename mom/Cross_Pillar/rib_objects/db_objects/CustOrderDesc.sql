@@LocaleDesc.sql;
/
@@CustomerDesc.sql;
/
@@CustOrdItmColDesc.sql;
/
@@CustOrderRtnColVo.sql;
/
@@CustOrdFulColDesc.sql;
/
@@CustOrdDelColDesc.sql;
/
@@PaymentColDesc.sql;
/
DROP TYPE "RIB_CustOrderDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrderDesc_REC";
/
DROP TYPE "RIB_CustOrderDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrderDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrderDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_id varchar2(48),
  external_ref_id varchar2(120),
  currency_code varchar2(3),
  LocaleDesc "RIB_LocaleDesc_REC",
  order_desc varchar2(250),
  order_status varchar2(9), -- order_status is enumeration field, valid values are [NEW, OPEN, PARTIAL, HELD, SUSPENDED, CANCELED, COMPLETED, FILLED, ERROR] (all lower-case)
  initiate_loc_type varchar2(1), -- initiate_loc_type is enumeration field, valid values are [O, S] (all lower-case)
  initiate_loc_id number(10),
  initiate_country_code varchar2(3),
  grand_total number(20,4),
  sub_total number(20,4),
  tax_total number(20,4),
  inclusive_tax_total number(20,4),
  shipping_charge_total number(20,4),
  discount_total number(20,4),
  completed_amount number(20,4),
  cancelled_amount number(20,4),
  returned_amount number(20,4),
  completed_discount_amount number(20,4),
  cancelled_discount_amount number(20,4),
  returned_discount_amount number(20,4),
  completed_tax_amount number(20,4),
  cancelled_tax_amount number(20,4),
  returned_tax_amount number(20,4),
  completed_inclusive_tax_amount number(20,4),
  cancelled_inclusive_tax_amount number(20,4),
  returned_inclusive_tax_amount number(20,4),
  paid_amount number(20,4),
  rounding_adjustment number(20,4),
  refund_amount_offset_by_sale number(20,4),
  CustomerDesc "RIB_CustomerDesc_REC",
  gift_receipt_assigned varchar2(1), -- gift_receipt_assigned is enumeration field, valid values are [Y, N] (all lower-case)
  default_gift_registry_id varchar2(14),
  age_restricted_dob date,
  create_timestamp date,
  update_timestamp date,
  CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC",
  CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC",
  CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC",
  CustOrdDelColDesc "RIB_CustOrdDelColDesc_REC",
  PaymentColDesc "RIB_PaymentColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
) return self as result
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
) return self as result
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
) return self as result
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
) return self as result
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
, CustOrdDelColDesc "RIB_CustOrdDelColDesc_REC"
) return self as result
,constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
, CustOrdDelColDesc "RIB_CustOrdDelColDesc_REC"
, PaymentColDesc "RIB_PaymentColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrderDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrderDesc') := "ns_name_CustOrderDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_ref_id') := external_ref_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  l_new_pre :=i_prefix||'LocaleDesc.';
  LocaleDesc.appendNodeValues( i_prefix||'LocaleDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_desc') := order_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_status') := order_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'initiate_loc_type') := initiate_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'initiate_loc_id') := initiate_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'initiate_country_code') := initiate_country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'grand_total') := grand_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'sub_total') := sub_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_total') := tax_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'inclusive_tax_total') := inclusive_tax_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipping_charge_total') := shipping_charge_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_total') := discount_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_amount') := completed_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_amount') := cancelled_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_amount') := returned_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_discount_amount') := completed_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_discount_amount') := cancelled_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_discount_amount') := returned_discount_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_tax_amount') := completed_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_tax_amount') := cancelled_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_tax_amount') := returned_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_inclusive_tax_amount') := completed_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_inclusive_tax_amount') := cancelled_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_inclusive_tax_amount') := returned_inclusive_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'paid_amount') := paid_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'rounding_adjustment') := rounding_adjustment;
  rib_obj_util.g_RIB_element_values(i_prefix||'refund_amount_offset_by_sale') := refund_amount_offset_by_sale;
  l_new_pre :=i_prefix||'CustomerDesc.';
  CustomerDesc.appendNodeValues( i_prefix||'CustomerDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_receipt_assigned') := gift_receipt_assigned;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_gift_registry_id') := default_gift_registry_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'age_restricted_dob') := age_restricted_dob;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_timestamp') := create_timestamp;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_timestamp') := update_timestamp;
  l_new_pre :=i_prefix||'CustOrdItmColDesc.';
  CustOrdItmColDesc.appendNodeValues( i_prefix||'CustOrdItmColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustOrderRtnColVo.';
  CustOrderRtnColVo.appendNodeValues( i_prefix||'CustOrderRtnColVo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustOrdFulColDesc.';
  CustOrdFulColDesc.appendNodeValues( i_prefix||'CustOrdFulColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'CustOrdDelColDesc.';
  CustOrdDelColDesc.appendNodeValues( i_prefix||'CustOrdDelColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PaymentColDesc.';
  PaymentColDesc.appendNodeValues( i_prefix||'PaymentColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
RETURN;
end;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
self.CustOrdItmColDesc := CustOrdItmColDesc;
RETURN;
end;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
self.CustOrdItmColDesc := CustOrdItmColDesc;
self.CustOrderRtnColVo := CustOrderRtnColVo;
RETURN;
end;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
self.CustOrdItmColDesc := CustOrdItmColDesc;
self.CustOrderRtnColVo := CustOrderRtnColVo;
self.CustOrdFulColDesc := CustOrdFulColDesc;
RETURN;
end;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
, CustOrdDelColDesc "RIB_CustOrdDelColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
self.CustOrdItmColDesc := CustOrdItmColDesc;
self.CustOrderRtnColVo := CustOrderRtnColVo;
self.CustOrdFulColDesc := CustOrdFulColDesc;
self.CustOrdDelColDesc := CustOrdDelColDesc;
RETURN;
end;
constructor function "RIB_CustOrderDesc_REC"
(
  rib_oid number
, customer_order_id varchar2
, external_ref_id varchar2
, currency_code varchar2
, LocaleDesc "RIB_LocaleDesc_REC"
, order_desc varchar2
, order_status varchar2
, initiate_loc_type varchar2
, initiate_loc_id number
, initiate_country_code varchar2
, grand_total number
, sub_total number
, tax_total number
, inclusive_tax_total number
, shipping_charge_total number
, discount_total number
, completed_amount number
, cancelled_amount number
, returned_amount number
, completed_discount_amount number
, cancelled_discount_amount number
, returned_discount_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, completed_inclusive_tax_amount number
, cancelled_inclusive_tax_amount number
, returned_inclusive_tax_amount number
, paid_amount number
, rounding_adjustment number
, refund_amount_offset_by_sale number
, CustomerDesc "RIB_CustomerDesc_REC"
, gift_receipt_assigned varchar2
, default_gift_registry_id varchar2
, age_restricted_dob date
, create_timestamp date
, update_timestamp date
, CustOrdItmColDesc "RIB_CustOrdItmColDesc_REC"
, CustOrderRtnColVo "RIB_CustOrderRtnColVo_REC"
, CustOrdFulColDesc "RIB_CustOrdFulColDesc_REC"
, CustOrdDelColDesc "RIB_CustOrdDelColDesc_REC"
, PaymentColDesc "RIB_PaymentColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_id := customer_order_id;
self.external_ref_id := external_ref_id;
self.currency_code := currency_code;
self.LocaleDesc := LocaleDesc;
self.order_desc := order_desc;
self.order_status := order_status;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.initiate_country_code := initiate_country_code;
self.grand_total := grand_total;
self.sub_total := sub_total;
self.tax_total := tax_total;
self.inclusive_tax_total := inclusive_tax_total;
self.shipping_charge_total := shipping_charge_total;
self.discount_total := discount_total;
self.completed_amount := completed_amount;
self.cancelled_amount := cancelled_amount;
self.returned_amount := returned_amount;
self.completed_discount_amount := completed_discount_amount;
self.cancelled_discount_amount := cancelled_discount_amount;
self.returned_discount_amount := returned_discount_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.completed_inclusive_tax_amount := completed_inclusive_tax_amount;
self.cancelled_inclusive_tax_amount := cancelled_inclusive_tax_amount;
self.returned_inclusive_tax_amount := returned_inclusive_tax_amount;
self.paid_amount := paid_amount;
self.rounding_adjustment := rounding_adjustment;
self.refund_amount_offset_by_sale := refund_amount_offset_by_sale;
self.CustomerDesc := CustomerDesc;
self.gift_receipt_assigned := gift_receipt_assigned;
self.default_gift_registry_id := default_gift_registry_id;
self.age_restricted_dob := age_restricted_dob;
self.create_timestamp := create_timestamp;
self.update_timestamp := update_timestamp;
self.CustOrdItmColDesc := CustOrdItmColDesc;
self.CustOrderRtnColVo := CustOrderRtnColVo;
self.CustOrdFulColDesc := CustOrdFulColDesc;
self.CustOrdDelColDesc := CustOrdDelColDesc;
self.PaymentColDesc := PaymentColDesc;
RETURN;
end;
END;
/
