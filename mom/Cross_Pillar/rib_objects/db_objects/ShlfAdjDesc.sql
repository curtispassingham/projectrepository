DROP TYPE "RIB_ShlfAdjDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjDesc_REC";
/
DROP TYPE "RIB_ShlfAdjItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjItm_REC";
/
DROP TYPE "RIB_ShlfAdjItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ShlfAdjItm_TBL" AS TABLE OF "RIB_ShlfAdjItm_REC";
/
DROP TYPE "RIB_ShlfAdjDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfAdjDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfAdjDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shelf_adjustment_id number(15),
  store_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, COMPLETE, UNKNOWN] (all lower-case)
  type varchar2(20), -- type is enumeration field, valid values are [SHOP_FLOOR_ADJUST, BACK_ROOM_ADJUST, DISPLAY_LIST, ADHOC_REPLENISH, UNKNOWN] (all lower-case)
  user_name varchar2(128),
  create_date date,
  update_date date,
  ShlfAdjItm_TBL "RIB_ShlfAdjItm_TBL",   -- Size of "RIB_ShlfAdjItm_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfAdjDesc_REC"
(
  rib_oid number
, shelf_adjustment_id number
, store_id number
, status varchar2
, type varchar2
, user_name varchar2
, create_date date
, update_date date
) return self as result
,constructor function "RIB_ShlfAdjDesc_REC"
(
  rib_oid number
, shelf_adjustment_id number
, store_id number
, status varchar2
, type varchar2
, user_name varchar2
, create_date date
, update_date date
, ShlfAdjItm_TBL "RIB_ShlfAdjItm_TBL"  -- Size of "RIB_ShlfAdjItm_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfAdjDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfAdjDesc') := "ns_name_ShlfAdjDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_adjustment_id') := shelf_adjustment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_name') := user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'update_date') := update_date;
  IF ShlfAdjItm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ShlfAdjItm_TBL.';
    FOR INDX IN ShlfAdjItm_TBL.FIRST()..ShlfAdjItm_TBL.LAST() LOOP
      ShlfAdjItm_TBL(indx).appendNodeValues( i_prefix||indx||'ShlfAdjItm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ShlfAdjDesc_REC"
(
  rib_oid number
, shelf_adjustment_id number
, store_id number
, status varchar2
, type varchar2
, user_name varchar2
, create_date date
, update_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shelf_adjustment_id := shelf_adjustment_id;
self.store_id := store_id;
self.status := status;
self.type := type;
self.user_name := user_name;
self.create_date := create_date;
self.update_date := update_date;
RETURN;
end;
constructor function "RIB_ShlfAdjDesc_REC"
(
  rib_oid number
, shelf_adjustment_id number
, store_id number
, status varchar2
, type varchar2
, user_name varchar2
, create_date date
, update_date date
, ShlfAdjItm_TBL "RIB_ShlfAdjItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shelf_adjustment_id := shelf_adjustment_id;
self.store_id := store_id;
self.status := status;
self.type := type;
self.user_name := user_name;
self.create_date := create_date;
self.update_date := update_date;
self.ShlfAdjItm_TBL := ShlfAdjItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ShlfAdjItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShlfAdjItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShlfAdjDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  requested_pick_amount number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShlfAdjItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_pick_amount number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShlfAdjItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShlfAdjDesc') := "ns_name_ShlfAdjDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pick_amount') := requested_pick_amount;
END appendNodeValues;
constructor function "RIB_ShlfAdjItm_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, requested_pick_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.requested_pick_amount := requested_pick_amount;
RETURN;
end;
END;
/
