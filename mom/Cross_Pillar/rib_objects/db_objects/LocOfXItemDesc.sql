@@BrXItemDesc.sql;
/
DROP TYPE "RIB_LocOfXItemVATDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemVATDesc_REC";
/
DROP TYPE "RIB_LocOfXISCLocDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXISCLocDesc_REC";
/
DROP TYPE "RIB_LocOfXItemUDADtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemUDADtl_REC";
/
DROP TYPE "RIB_LocOfXItemImage_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemImage_REC";
/
DROP TYPE "RIB_LocOfXItemSeason_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSeason_REC";
/
DROP TYPE "RIB_LocOfXIZPDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXIZPDesc_REC";
/
DROP TYPE "RIB_LocOfXISCDimDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXISCDimDesc_REC";
/
DROP TYPE "RIB_LocOfXItemBOMDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemBOMDesc_REC";
/
DROP TYPE "RIB_LocOfXItemSupCtyDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSupCtyDesc_REC";
/
DROP TYPE "RIB_LocOfXItemSupDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSupDesc_REC";
/
DROP TYPE "RIB_LocOfXItemCtryDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemCtryDesc_REC";
/
DROP TYPE "RIB_LocOfXItemCostDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemCostDesc_REC";
/
DROP TYPE "RIB_LocOfXItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocOfXItemDesc_REC";
/
DROP TYPE "RIB_BrXItemVATDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemVATDesc_TBL" AS TABLE OF "RIB_BrXItemVATDesc_REC";
/
DROP TYPE "RIB_LocOfXItemVATDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemVATDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemVATDesc_TBL "RIB_BrXItemVATDesc_TBL",   -- Size of "RIB_BrXItemVATDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemVATDesc_REC"
(
  rib_oid number
, BrXItemVATDesc_TBL "RIB_BrXItemVATDesc_TBL"  -- Size of "RIB_BrXItemVATDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemVATDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemVATDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemVATDesc_TBL.';
    FOR INDX IN BrXItemVATDesc_TBL.FIRST()..BrXItemVATDesc_TBL.LAST() LOOP
      BrXItemVATDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemVATDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemVATDesc_REC"
(
  rib_oid number
, BrXItemVATDesc_TBL "RIB_BrXItemVATDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemVATDesc_TBL := BrXItemVATDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXISCLocDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXISCLocDesc_TBL" AS TABLE OF "RIB_BrXISCLocDesc_REC";
/
DROP TYPE "RIB_LocOfXISCLocDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXISCLocDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXISCLocDesc_TBL "RIB_BrXISCLocDesc_TBL",   -- Size of "RIB_BrXISCLocDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXISCLocDesc_REC"
(
  rib_oid number
, BrXISCLocDesc_TBL "RIB_BrXISCLocDesc_TBL"  -- Size of "RIB_BrXISCLocDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXISCLocDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXISCLocDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXISCLocDesc_TBL.';
    FOR INDX IN BrXISCLocDesc_TBL.FIRST()..BrXISCLocDesc_TBL.LAST() LOOP
      BrXISCLocDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXISCLocDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXISCLocDesc_REC"
(
  rib_oid number
, BrXISCLocDesc_TBL "RIB_BrXISCLocDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXISCLocDesc_TBL := BrXISCLocDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemUDADtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemUDADtl_TBL" AS TABLE OF "RIB_BrXItemUDADtl_REC";
/
DROP TYPE "RIB_LocOfXItemUDADtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemUDADtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemUDADtl_TBL "RIB_BrXItemUDADtl_TBL",   -- Size of "RIB_BrXItemUDADtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemUDADtl_REC"
(
  rib_oid number
, BrXItemUDADtl_TBL "RIB_BrXItemUDADtl_TBL"  -- Size of "RIB_BrXItemUDADtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemUDADtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemUDADtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemUDADtl_TBL.';
    FOR INDX IN BrXItemUDADtl_TBL.FIRST()..BrXItemUDADtl_TBL.LAST() LOOP
      BrXItemUDADtl_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemUDADtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemUDADtl_REC"
(
  rib_oid number
, BrXItemUDADtl_TBL "RIB_BrXItemUDADtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemUDADtl_TBL := BrXItemUDADtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemImage_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemImage_TBL" AS TABLE OF "RIB_BrXItemImage_REC";
/
DROP TYPE "RIB_LocOfXItemImage_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemImage_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemImage_TBL "RIB_BrXItemImage_TBL",   -- Size of "RIB_BrXItemImage_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemImage_REC"
(
  rib_oid number
, BrXItemImage_TBL "RIB_BrXItemImage_TBL"  -- Size of "RIB_BrXItemImage_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemImage_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemImage_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemImage_TBL.';
    FOR INDX IN BrXItemImage_TBL.FIRST()..BrXItemImage_TBL.LAST() LOOP
      BrXItemImage_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemImage_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemImage_REC"
(
  rib_oid number
, BrXItemImage_TBL "RIB_BrXItemImage_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemImage_TBL := BrXItemImage_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSeason_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSeason_TBL" AS TABLE OF "RIB_BrXItemSeason_REC";
/
DROP TYPE "RIB_LocOfXItemSeason_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSeason_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemSeason_TBL "RIB_BrXItemSeason_TBL",   -- Size of "RIB_BrXItemSeason_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemSeason_REC"
(
  rib_oid number
, BrXItemSeason_TBL "RIB_BrXItemSeason_TBL"  -- Size of "RIB_BrXItemSeason_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemSeason_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemSeason_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemSeason_TBL.';
    FOR INDX IN BrXItemSeason_TBL.FIRST()..BrXItemSeason_TBL.LAST() LOOP
      BrXItemSeason_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemSeason_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemSeason_REC"
(
  rib_oid number
, BrXItemSeason_TBL "RIB_BrXItemSeason_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemSeason_TBL := BrXItemSeason_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXIZPDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXIZPDesc_TBL" AS TABLE OF "RIB_BrXIZPDesc_REC";
/
DROP TYPE "RIB_LocOfXIZPDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXIZPDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXIZPDesc_TBL "RIB_BrXIZPDesc_TBL",   -- Size of "RIB_BrXIZPDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXIZPDesc_REC"
(
  rib_oid number
, BrXIZPDesc_TBL "RIB_BrXIZPDesc_TBL"  -- Size of "RIB_BrXIZPDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXIZPDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXIZPDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXIZPDesc_TBL.';
    FOR INDX IN BrXIZPDesc_TBL.FIRST()..BrXIZPDesc_TBL.LAST() LOOP
      BrXIZPDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXIZPDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXIZPDesc_REC"
(
  rib_oid number
, BrXIZPDesc_TBL "RIB_BrXIZPDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXIZPDesc_TBL := BrXIZPDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXISCDimDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXISCDimDesc_TBL" AS TABLE OF "RIB_BrXISCDimDesc_REC";
/
DROP TYPE "RIB_LocOfXISCDimDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXISCDimDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXISCDimDesc_TBL "RIB_BrXISCDimDesc_TBL",   -- Size of "RIB_BrXISCDimDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXISCDimDesc_REC"
(
  rib_oid number
, BrXISCDimDesc_TBL "RIB_BrXISCDimDesc_TBL"  -- Size of "RIB_BrXISCDimDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXISCDimDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXISCDimDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXISCDimDesc_TBL.';
    FOR INDX IN BrXISCDimDesc_TBL.FIRST()..BrXISCDimDesc_TBL.LAST() LOOP
      BrXISCDimDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXISCDimDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXISCDimDesc_REC"
(
  rib_oid number
, BrXISCDimDesc_TBL "RIB_BrXISCDimDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXISCDimDesc_TBL := BrXISCDimDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemBOMDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemBOMDesc_TBL" AS TABLE OF "RIB_BrXItemBOMDesc_REC";
/
DROP TYPE "RIB_LocOfXItemBOMDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemBOMDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemBOMDesc_TBL "RIB_BrXItemBOMDesc_TBL",   -- Size of "RIB_BrXItemBOMDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemBOMDesc_REC"
(
  rib_oid number
, BrXItemBOMDesc_TBL "RIB_BrXItemBOMDesc_TBL"  -- Size of "RIB_BrXItemBOMDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemBOMDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemBOMDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemBOMDesc_TBL.';
    FOR INDX IN BrXItemBOMDesc_TBL.FIRST()..BrXItemBOMDesc_TBL.LAST() LOOP
      BrXItemBOMDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemBOMDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemBOMDesc_REC"
(
  rib_oid number
, BrXItemBOMDesc_TBL "RIB_BrXItemBOMDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemBOMDesc_TBL := BrXItemBOMDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSupCtyDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSupCtyDesc_TBL" AS TABLE OF "RIB_BrXItemSupCtyDesc_REC";
/
DROP TYPE "RIB_LocOfXItemSupCtyDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSupCtyDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemSupCtyDesc_TBL "RIB_BrXItemSupCtyDesc_TBL",   -- Size of "RIB_BrXItemSupCtyDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemSupCtyDesc_REC"
(
  rib_oid number
, BrXItemSupCtyDesc_TBL "RIB_BrXItemSupCtyDesc_TBL"  -- Size of "RIB_BrXItemSupCtyDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemSupCtyDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemSupCtyDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemSupCtyDesc_TBL.';
    FOR INDX IN BrXItemSupCtyDesc_TBL.FIRST()..BrXItemSupCtyDesc_TBL.LAST() LOOP
      BrXItemSupCtyDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemSupCtyDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemSupCtyDesc_REC"
(
  rib_oid number
, BrXItemSupCtyDesc_TBL "RIB_BrXItemSupCtyDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemSupCtyDesc_TBL := BrXItemSupCtyDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemSupDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemSupDesc_TBL" AS TABLE OF "RIB_BrXItemSupDesc_REC";
/
DROP TYPE "RIB_LocOfXItemSupDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemSupDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemSupDesc_TBL "RIB_BrXItemSupDesc_TBL",   -- Size of "RIB_BrXItemSupDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemSupDesc_REC"
(
  rib_oid number
, BrXItemSupDesc_TBL "RIB_BrXItemSupDesc_TBL"  -- Size of "RIB_BrXItemSupDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemSupDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemSupDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemSupDesc_TBL.';
    FOR INDX IN BrXItemSupDesc_TBL.FIRST()..BrXItemSupDesc_TBL.LAST() LOOP
      BrXItemSupDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemSupDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemSupDesc_REC"
(
  rib_oid number
, BrXItemSupDesc_TBL "RIB_BrXItemSupDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemSupDesc_TBL := BrXItemSupDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemCtryDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemCtryDesc_TBL" AS TABLE OF "RIB_BrXItemCtryDesc_REC";
/
DROP TYPE "RIB_LocOfXItemCtryDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemCtryDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemCtryDesc_TBL "RIB_BrXItemCtryDesc_TBL",   -- Size of "RIB_BrXItemCtryDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemCtryDesc_REC"
(
  rib_oid number
, BrXItemCtryDesc_TBL "RIB_BrXItemCtryDesc_TBL"  -- Size of "RIB_BrXItemCtryDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemCtryDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemCtryDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemCtryDesc_TBL.';
    FOR INDX IN BrXItemCtryDesc_TBL.FIRST()..BrXItemCtryDesc_TBL.LAST() LOOP
      BrXItemCtryDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemCtryDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemCtryDesc_REC"
(
  rib_oid number
, BrXItemCtryDesc_TBL "RIB_BrXItemCtryDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemCtryDesc_TBL := BrXItemCtryDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemCostDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemCostDesc_TBL" AS TABLE OF "RIB_BrXItemCostDesc_REC";
/
DROP TYPE "RIB_LocOfXItemCostDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemCostDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemCostDesc_TBL "RIB_BrXItemCostDesc_TBL",   -- Size of "RIB_BrXItemCostDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemCostDesc_REC"
(
  rib_oid number
, BrXItemCostDesc_TBL "RIB_BrXItemCostDesc_TBL"  -- Size of "RIB_BrXItemCostDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemCostDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF BrXItemCostDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemCostDesc_TBL.';
    FOR INDX IN BrXItemCostDesc_TBL.FIRST()..BrXItemCostDesc_TBL.LAST() LOOP
      BrXItemCostDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemCostDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemCostDesc_REC"
(
  rib_oid number
, BrXItemCostDesc_TBL "RIB_BrXItemCostDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemCostDesc_TBL := BrXItemCostDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_BrXItemDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrXItemDesc_TBL" AS TABLE OF "RIB_BrXItemDesc_REC";
/
DROP TYPE "RIB_LocOfXItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocOfXItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocOfXItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  BrXItemDesc_TBL "RIB_BrXItemDesc_TBL",   -- Size of "RIB_BrXItemDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocOfXItemDesc_REC"
(
  rib_oid number
, BrXItemDesc_TBL "RIB_BrXItemDesc_TBL"  -- Size of "RIB_BrXItemDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocOfXItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocOfXItemDesc') := "ns_name_LocOfXItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  IF BrXItemDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrXItemDesc_TBL.';
    FOR INDX IN BrXItemDesc_TBL.FIRST()..BrXItemDesc_TBL.LAST() LOOP
      BrXItemDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrXItemDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_LocOfXItemDesc_REC"
(
  rib_oid number
, BrXItemDesc_TBL "RIB_BrXItemDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.BrXItemDesc_TBL := BrXItemDesc_TBL;
RETURN;
end;
END;
/
