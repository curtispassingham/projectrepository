DROP TYPE "RIB_RelatedItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RelatedItemDesc_REC";
/
DROP TYPE "RIB_RelatedItemDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RelatedItemDtl_REC";
/
DROP TYPE "RIB_RelatedItemDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RelatedItemDtl_TBL" AS TABLE OF "RIB_RelatedItemDtl_REC";
/
DROP TYPE "RIB_RelatedItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RelatedItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RelatedItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  relationship_id number(15),
  relationship_name varchar2(120),
  relationship_type varchar2(6),
  mandatory_ind varchar2(1), -- mandatory_ind is enumeration field, valid values are [Y, N] (all lower-case)
  RelatedItemDtl_TBL "RIB_RelatedItemDtl_TBL",   -- Size of "RIB_RelatedItemDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RelatedItemDesc_REC"
(
  rib_oid number
, item varchar2
, relationship_id number
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
) return self as result
,constructor function "RIB_RelatedItemDesc_REC"
(
  rib_oid number
, item varchar2
, relationship_id number
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, RelatedItemDtl_TBL "RIB_RelatedItemDtl_TBL"  -- Size of "RIB_RelatedItemDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RelatedItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RelatedItemDesc') := "ns_name_RelatedItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_id') := relationship_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_name') := relationship_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'relationship_type') := relationship_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'mandatory_ind') := mandatory_ind;
  IF RelatedItemDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RelatedItemDtl_TBL.';
    FOR INDX IN RelatedItemDtl_TBL.FIRST()..RelatedItemDtl_TBL.LAST() LOOP
      RelatedItemDtl_TBL(indx).appendNodeValues( i_prefix||indx||'RelatedItemDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_RelatedItemDesc_REC"
(
  rib_oid number
, item varchar2
, relationship_id number
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.relationship_id := relationship_id;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
RETURN;
end;
constructor function "RIB_RelatedItemDesc_REC"
(
  rib_oid number
, item varchar2
, relationship_id number
, relationship_name varchar2
, relationship_type varchar2
, mandatory_ind varchar2
, RelatedItemDtl_TBL "RIB_RelatedItemDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.relationship_id := relationship_id;
self.relationship_name := relationship_name;
self.relationship_type := relationship_type;
self.mandatory_ind := mandatory_ind;
self.RelatedItemDtl_TBL := RelatedItemDtl_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_RelatedItemDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RelatedItemDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RelatedItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  related_item varchar2(25),
  priority number(4),
  effective_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
) return self as result
,constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
) return self as result
,constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
, effective_date date
) return self as result
,constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
, effective_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RelatedItemDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RelatedItemDesc') := "ns_name_RelatedItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'related_item') := related_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'priority') := priority;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
RETURN;
end;
constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.priority := priority;
RETURN;
end;
constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
, effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.priority := priority;
self.effective_date := effective_date;
RETURN;
end;
constructor function "RIB_RelatedItemDtl_REC"
(
  rib_oid number
, related_item varchar2
, priority number
, effective_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.related_item := related_item;
self.priority := priority;
self.effective_date := effective_date;
self.end_date := end_date;
RETURN;
end;
END;
/
