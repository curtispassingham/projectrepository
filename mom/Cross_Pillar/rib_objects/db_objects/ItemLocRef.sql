DROP TYPE "RIB_ItemLocVirtRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRef_REC";
/
DROP TYPE "RIB_ItemLocPhysRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocPhysRef_REC";
/
DROP TYPE "RIB_ItemLocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocRef_REC";
/
DROP TYPE "RIB_ItemLocVirtRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  loc number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocVirtRef_REC"
(
  rib_oid number
, loc number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocVirtRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocRef') := "ns_name_ItemLocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
END appendNodeValues;
constructor function "RIB_ItemLocVirtRef_REC"
(
  rib_oid number
, loc number
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocVirtRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocVirtRef_TBL" AS TABLE OF "RIB_ItemLocVirtRef_REC";
/
DROP TYPE "RIB_ItemLocPhysRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocPhysRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  physical_loc number(10),
  loc_type varchar2(1),
  store_type varchar2(1),
  stockholding_ind varchar2(1),
  ItemLocVirtRef_TBL "RIB_ItemLocVirtRef_TBL",   -- Size of "RIB_ItemLocVirtRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocPhysRef_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, ItemLocVirtRef_TBL "RIB_ItemLocVirtRef_TBL"  -- Size of "RIB_ItemLocVirtRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocPhysRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocRef') := "ns_name_ItemLocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'physical_loc') := physical_loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_type') := store_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'stockholding_ind') := stockholding_ind;
  l_new_pre :=i_prefix||'ItemLocVirtRef_TBL.';
  FOR INDX IN ItemLocVirtRef_TBL.FIRST()..ItemLocVirtRef_TBL.LAST() LOOP
    ItemLocVirtRef_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocVirtRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItemLocPhysRef_REC"
(
  rib_oid number
, physical_loc number
, loc_type varchar2
, store_type varchar2
, stockholding_ind varchar2
, ItemLocVirtRef_TBL "RIB_ItemLocVirtRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.physical_loc := physical_loc;
self.loc_type := loc_type;
self.store_type := store_type;
self.stockholding_ind := stockholding_ind;
self.ItemLocVirtRef_TBL := ItemLocVirtRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemLocPhysRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemLocPhysRef_TBL" AS TABLE OF "RIB_ItemLocPhysRef_REC";
/
DROP TYPE "RIB_ItemLocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemLocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemLocRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  ItemLocPhysRef_TBL "RIB_ItemLocPhysRef_TBL",   -- Size of "RIB_ItemLocPhysRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemLocRef_REC"
(
  rib_oid number
, item varchar2
, ItemLocPhysRef_TBL "RIB_ItemLocPhysRef_TBL"  -- Size of "RIB_ItemLocPhysRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemLocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemLocRef') := "ns_name_ItemLocRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  l_new_pre :=i_prefix||'ItemLocPhysRef_TBL.';
  FOR INDX IN ItemLocPhysRef_TBL.FIRST()..ItemLocPhysRef_TBL.LAST() LOOP
    ItemLocPhysRef_TBL(indx).appendNodeValues( i_prefix||indx||'ItemLocPhysRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItemLocRef_REC"
(
  rib_oid number
, item varchar2
, ItemLocPhysRef_TBL "RIB_ItemLocPhysRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.ItemLocPhysRef_TBL := ItemLocPhysRef_TBL;
RETURN;
end;
END;
/
