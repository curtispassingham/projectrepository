DROP TYPE "RIB_GLAcctRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GLAcctRef_REC";
/
DROP TYPE "RIB_GLAcctRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GLAcctRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GLAcctRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  requesting_system varchar2(25),
  set_of_books_id number(15),
  ccid number(15),
  segment1 varchar2(25),
  segment2 varchar2(25),
  segment3 varchar2(25),
  segment4 varchar2(25),
  segment5 varchar2(25),
  segment6 varchar2(25),
  segment7 varchar2(25),
  segment8 varchar2(25),
  segment9 varchar2(25),
  segment10 varchar2(25),
  segment11 varchar2(25),
  segment12 varchar2(25),
  segment13 varchar2(25),
  segment14 varchar2(25),
  segment15 varchar2(25),
  segment16 varchar2(25),
  segment17 varchar2(25),
  segment18 varchar2(25),
  segment19 varchar2(25),
  segment20 varchar2(25),
  account_status varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GLAcctRef_REC"
(
  rib_oid number
, requesting_system varchar2
, set_of_books_id number
, ccid number
, segment1 varchar2
, segment2 varchar2
, segment3 varchar2
, segment4 varchar2
, segment5 varchar2
, segment6 varchar2
, segment7 varchar2
, segment8 varchar2
, segment9 varchar2
, segment10 varchar2
, segment11 varchar2
, segment12 varchar2
, segment13 varchar2
, segment14 varchar2
, segment15 varchar2
, segment16 varchar2
, segment17 varchar2
, segment18 varchar2
, segment19 varchar2
, segment20 varchar2
, account_status varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GLAcctRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GLAcctRef') := "ns_name_GLAcctRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'requesting_system') := requesting_system;
  rib_obj_util.g_RIB_element_values(i_prefix||'set_of_books_id') := set_of_books_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'ccid') := ccid;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment1') := segment1;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment2') := segment2;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment3') := segment3;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment4') := segment4;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment5') := segment5;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment6') := segment6;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment7') := segment7;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment8') := segment8;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment9') := segment9;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment10') := segment10;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment11') := segment11;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment12') := segment12;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment13') := segment13;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment14') := segment14;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment15') := segment15;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment16') := segment16;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment17') := segment17;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment18') := segment18;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment19') := segment19;
  rib_obj_util.g_RIB_element_values(i_prefix||'segment20') := segment20;
  rib_obj_util.g_RIB_element_values(i_prefix||'account_status') := account_status;
END appendNodeValues;
constructor function "RIB_GLAcctRef_REC"
(
  rib_oid number
, requesting_system varchar2
, set_of_books_id number
, ccid number
, segment1 varchar2
, segment2 varchar2
, segment3 varchar2
, segment4 varchar2
, segment5 varchar2
, segment6 varchar2
, segment7 varchar2
, segment8 varchar2
, segment9 varchar2
, segment10 varchar2
, segment11 varchar2
, segment12 varchar2
, segment13 varchar2
, segment14 varchar2
, segment15 varchar2
, segment16 varchar2
, segment17 varchar2
, segment18 varchar2
, segment19 varchar2
, segment20 varchar2
, account_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.requesting_system := requesting_system;
self.set_of_books_id := set_of_books_id;
self.ccid := ccid;
self.segment1 := segment1;
self.segment2 := segment2;
self.segment3 := segment3;
self.segment4 := segment4;
self.segment5 := segment5;
self.segment6 := segment6;
self.segment7 := segment7;
self.segment8 := segment8;
self.segment9 := segment9;
self.segment10 := segment10;
self.segment11 := segment11;
self.segment12 := segment12;
self.segment13 := segment13;
self.segment14 := segment14;
self.segment15 := segment15;
self.segment16 := segment16;
self.segment17 := segment17;
self.segment18 := segment18;
self.segment19 := segment19;
self.segment20 := segment20;
self.account_status := account_status;
RETURN;
end;
END;
/
