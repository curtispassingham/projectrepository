DROP TYPE "RIB_ProdGrpModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpModVo_REC";
/
DROP TYPE "RIB_ProdGrpModHrchy_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpModHrchy_REC";
/
DROP TYPE "RIB_RemovedHrchy_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RemovedHrchy_REC";
/
DROP TYPE "RIB_ProdGrpModHrchy_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ProdGrpModHrchy_TBL" AS TABLE OF "RIB_ProdGrpModHrchy_REC";
/
DROP TYPE "RIB_RemovedHrchy_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RemovedHrchy_TBL" AS TABLE OF "RIB_RemovedHrchy_REC";
/
DROP TYPE "RIB_ProdGrpModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  product_group_id number(10),
  group_type varchar2(40), -- group_type is enumeration field, valid values are [STOCK_COUNT_UNIT, STOCK_COUNT_UNIT_AMOUNT, STOCK_COUNT_PROBLEM_LINE, STOCK_COUNT_WASTAGE, STOCK_COUNT_RMS_SYNC, SHELF_REPLENISHMENT, ITEM_REQUEST, AUTO_TICKET_PRINT, UNKNOWN, NO_VALUE] (all lower-case)
  description varchar2(250),
  store_id number(10),
  status varchar2(20), -- status is enumeration field, valid values are [ACTIVE, CANCELED] (all lower-case)
  uom_mode varchar2(20), -- uom_mode is enumeration field, valid values are [STANDARD, CASES, PREFERRED] (all lower-case)
  variance_count number(12),
  variance_percent number(12,2),
  variance_value number(12,2),
  counting_method varchar2(20), -- counting_method is enumeration field, valid values are [GUIDED, UNGUIDED, THIRD_PARTY, AUTO, UNKNOWN, NO_VALUE] (all lower-case)
  breakdown_type varchar2(20), -- breakdown_type is enumeration field, valid values are [DEPARTMENT, CLASS, SUBCLASS, LOCATION, NONE, UNKNOWN, NO_VALUE] (all lower-case)
  diff_type varchar2(20), -- diff_type is enumeration field, valid values are [DIFF1, DIFF2, DIFF3, DIFF4, UNKNOWN, NO_VALUE] (all lower-case)
  all_items varchar2(5), --all_items is boolean field, valid values are true,false (all lower-case) 
  recount_active varchar2(5), --recount_active is boolean field, valid values are true,false (all lower-case) 
  auto_authorize varchar2(5), --auto_authorize is boolean field, valid values are true,false (all lower-case) 
  pick_less_suggested varchar2(5), --pick_less_suggested is boolean field, valid values are true,false (all lower-case) 
  shelf_repl_less_suggested varchar2(5), --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case) 
  negative_available varchar2(5), --negative_available is boolean field, valid values are true,false (all lower-case) 
  uin_discrepancy varchar2(5), --uin_discrepancy is boolean field, valid values are true,false (all lower-case) 
  include_active_items varchar2(5), --include_active_items is boolean field, valid values are true,false (all lower-case) 
  include_inactive_items varchar2(5), --include_inactive_items is boolean field, valid values are true,false (all lower-case) 
  include_discontinued_items varchar2(5), --include_discontinued_items is boolean field, valid values are true,false (all lower-case) 
  include_deleted_items varchar2(5), --include_deleted_items is boolean field, valid values are true,false (all lower-case) 
  include_soh_zero varchar2(5), --include_soh_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_less_than_zero varchar2(5), --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case) 
  include_soh_greater_than_zero varchar2(5), --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case) 
  auto_replenishment varchar2(5), --auto_replenishment is boolean field, valid values are true,false (all lower-case) 
  refresh_ticket_quantity varchar2(5), --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case) 
  days_to_expire_request number(12),
  days_to_request_delivery number(12),
  ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL",   -- Size of "RIB_ProdGrpModHrchy_TBL" is 1000
  RemovedHrchy_TBL "RIB_RemovedHrchy_TBL",   -- Size of "RIB_RemovedHrchy_TBL" is 1000
  added_item_id_col "RIB_added_item_id_col_TBL",   -- Size of "RIB_added_item_id_col_TBL" is 1000
  removed_item_id_col "RIB_removed_item_id_col_TBL",   -- Size of "RIB_removed_item_id_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"  -- Size of "RIB_ProdGrpModHrchy_TBL" is 1000
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"  -- Size of "RIB_ProdGrpModHrchy_TBL" is 1000
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"  -- Size of "RIB_RemovedHrchy_TBL" is 1000
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"  -- Size of "RIB_ProdGrpModHrchy_TBL" is 1000
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"  -- Size of "RIB_RemovedHrchy_TBL" is 1000
, added_item_id_col "RIB_added_item_id_col_TBL"  -- Size of "RIB_added_item_id_col_TBL" is 1000
) return self as result
,constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2  --all_items is boolean field, valid values are true,false (all lower-case)
, recount_active varchar2  --recount_active is boolean field, valid values are true,false (all lower-case)
, auto_authorize varchar2  --auto_authorize is boolean field, valid values are true,false (all lower-case)
, pick_less_suggested varchar2  --pick_less_suggested is boolean field, valid values are true,false (all lower-case)
, shelf_repl_less_suggested varchar2  --shelf_repl_less_suggested is boolean field, valid values are true,false (all lower-case)
, negative_available varchar2  --negative_available is boolean field, valid values are true,false (all lower-case)
, uin_discrepancy varchar2  --uin_discrepancy is boolean field, valid values are true,false (all lower-case)
, include_active_items varchar2  --include_active_items is boolean field, valid values are true,false (all lower-case)
, include_inactive_items varchar2  --include_inactive_items is boolean field, valid values are true,false (all lower-case)
, include_discontinued_items varchar2  --include_discontinued_items is boolean field, valid values are true,false (all lower-case)
, include_deleted_items varchar2  --include_deleted_items is boolean field, valid values are true,false (all lower-case)
, include_soh_zero varchar2  --include_soh_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_less_than_zero varchar2  --include_soh_less_than_zero is boolean field, valid values are true,false (all lower-case)
, include_soh_greater_than_zero varchar2  --include_soh_greater_than_zero is boolean field, valid values are true,false (all lower-case)
, auto_replenishment varchar2  --auto_replenishment is boolean field, valid values are true,false (all lower-case)
, refresh_ticket_quantity varchar2  --refresh_ticket_quantity is boolean field, valid values are true,false (all lower-case)
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"  -- Size of "RIB_ProdGrpModHrchy_TBL" is 1000
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"  -- Size of "RIB_RemovedHrchy_TBL" is 1000
, added_item_id_col "RIB_added_item_id_col_TBL"  -- Size of "RIB_added_item_id_col_TBL" is 1000
, removed_item_id_col "RIB_removed_item_id_col_TBL"  -- Size of "RIB_removed_item_id_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpModVo') := "ns_name_ProdGrpModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'product_group_id') := product_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'group_type') := group_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'description') := description;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'uom_mode') := uom_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_count') := variance_count;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_percent') := variance_percent;
  rib_obj_util.g_RIB_element_values(i_prefix||'variance_value') := variance_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'counting_method') := counting_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'breakdown_type') := breakdown_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type') := diff_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'all_items') := all_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'recount_active') := recount_active;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_authorize') := auto_authorize;
  rib_obj_util.g_RIB_element_values(i_prefix||'pick_less_suggested') := pick_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'shelf_repl_less_suggested') := shelf_repl_less_suggested;
  rib_obj_util.g_RIB_element_values(i_prefix||'negative_available') := negative_available;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_discrepancy') := uin_discrepancy;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_active_items') := include_active_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_inactive_items') := include_inactive_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_discontinued_items') := include_discontinued_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_deleted_items') := include_deleted_items;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_zero') := include_soh_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_less_than_zero') := include_soh_less_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'include_soh_greater_than_zero') := include_soh_greater_than_zero;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_replenishment') := auto_replenishment;
  rib_obj_util.g_RIB_element_values(i_prefix||'refresh_ticket_quantity') := refresh_ticket_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'days_to_expire_request') := days_to_expire_request;
  rib_obj_util.g_RIB_element_values(i_prefix||'days_to_request_delivery') := days_to_request_delivery;
  IF ProdGrpModHrchy_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ProdGrpModHrchy_TBL.';
    FOR INDX IN ProdGrpModHrchy_TBL.FIRST()..ProdGrpModHrchy_TBL.LAST() LOOP
      ProdGrpModHrchy_TBL(indx).appendNodeValues( i_prefix||indx||'ProdGrpModHrchy_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF RemovedHrchy_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RemovedHrchy_TBL.';
    FOR INDX IN RemovedHrchy_TBL.FIRST()..RemovedHrchy_TBL.LAST() LOOP
      RemovedHrchy_TBL(indx).appendNodeValues( i_prefix||indx||'RemovedHrchy_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF added_item_id_col IS NOT NULL THEN
    FOR INDX IN added_item_id_col.FIRST()..added_item_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'added_item_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'added_item_id_col'||'.'):=added_item_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_item_id_col IS NOT NULL THEN
    FOR INDX IN removed_item_id_col.FIRST()..removed_item_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_item_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_item_id_col'||'.'):=removed_item_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpModHrchy_TBL := ProdGrpModHrchy_TBL;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpModHrchy_TBL := ProdGrpModHrchy_TBL;
self.RemovedHrchy_TBL := RemovedHrchy_TBL;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"
, added_item_id_col "RIB_added_item_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpModHrchy_TBL := ProdGrpModHrchy_TBL;
self.RemovedHrchy_TBL := RemovedHrchy_TBL;
self.added_item_id_col := added_item_id_col;
RETURN;
end;
constructor function "RIB_ProdGrpModVo_REC"
(
  rib_oid number
, product_group_id number
, group_type varchar2
, description varchar2
, store_id number
, status varchar2
, uom_mode varchar2
, variance_count number
, variance_percent number
, variance_value number
, counting_method varchar2
, breakdown_type varchar2
, diff_type varchar2
, all_items varchar2
, recount_active varchar2
, auto_authorize varchar2
, pick_less_suggested varchar2
, shelf_repl_less_suggested varchar2
, negative_available varchar2
, uin_discrepancy varchar2
, include_active_items varchar2
, include_inactive_items varchar2
, include_discontinued_items varchar2
, include_deleted_items varchar2
, include_soh_zero varchar2
, include_soh_less_than_zero varchar2
, include_soh_greater_than_zero varchar2
, auto_replenishment varchar2
, refresh_ticket_quantity varchar2
, days_to_expire_request number
, days_to_request_delivery number
, ProdGrpModHrchy_TBL "RIB_ProdGrpModHrchy_TBL"
, RemovedHrchy_TBL "RIB_RemovedHrchy_TBL"
, added_item_id_col "RIB_added_item_id_col_TBL"
, removed_item_id_col "RIB_removed_item_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.product_group_id := product_group_id;
self.group_type := group_type;
self.description := description;
self.store_id := store_id;
self.status := status;
self.uom_mode := uom_mode;
self.variance_count := variance_count;
self.variance_percent := variance_percent;
self.variance_value := variance_value;
self.counting_method := counting_method;
self.breakdown_type := breakdown_type;
self.diff_type := diff_type;
self.all_items := all_items;
self.recount_active := recount_active;
self.auto_authorize := auto_authorize;
self.pick_less_suggested := pick_less_suggested;
self.shelf_repl_less_suggested := shelf_repl_less_suggested;
self.negative_available := negative_available;
self.uin_discrepancy := uin_discrepancy;
self.include_active_items := include_active_items;
self.include_inactive_items := include_inactive_items;
self.include_discontinued_items := include_discontinued_items;
self.include_deleted_items := include_deleted_items;
self.include_soh_zero := include_soh_zero;
self.include_soh_less_than_zero := include_soh_less_than_zero;
self.include_soh_greater_than_zero := include_soh_greater_than_zero;
self.auto_replenishment := auto_replenishment;
self.refresh_ticket_quantity := refresh_ticket_quantity;
self.days_to_expire_request := days_to_expire_request;
self.days_to_request_delivery := days_to_request_delivery;
self.ProdGrpModHrchy_TBL := ProdGrpModHrchy_TBL;
self.RemovedHrchy_TBL := RemovedHrchy_TBL;
self.added_item_id_col := added_item_id_col;
self.removed_item_id_col := removed_item_id_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_added_item_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_added_item_id_col_TBL" AS TABLE OF varchar2(25);
/
DROP TYPE "RIB_removed_item_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_item_id_col_TBL" AS TABLE OF varchar2(25);
/
DROP TYPE "RIB_ProdGrpModHrchy_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ProdGrpModHrchy_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  department_id number(12),
  class_id number(10),
  subclass_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result
,constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result
,constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ProdGrpModHrchy_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpModVo') := "ns_name_ProdGrpModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
END appendNodeValues;
constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
RETURN;
end;
constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
RETURN;
end;
constructor function "RIB_ProdGrpModHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_RemovedHrchy_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RemovedHrchy_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ProdGrpModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  department_id number(12),
  class_id number(10),
  subclass_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result
,constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result
,constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RemovedHrchy_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ProdGrpModVo') := "ns_name_ProdGrpModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
END appendNodeValues;
constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
RETURN;
end;
constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
RETURN;
end;
constructor function "RIB_RemovedHrchy_REC"
(
  rib_oid number
, department_id number
, class_id number
, subclass_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.department_id := department_id;
self.class_id := class_id;
self.subclass_id := subclass_id;
RETURN;
end;
END;
/
