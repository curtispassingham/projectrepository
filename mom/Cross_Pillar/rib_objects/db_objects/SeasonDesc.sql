DROP TYPE "RIB_SeasonDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SeasonDtl_REC";
/
DROP TYPE "RIB_SeasonDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_SeasonDesc_REC";
/
DROP TYPE "RIB_SeasonDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SeasonDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SeasonDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  phase_id number(3),
  phase_desc varchar2(120),
  start_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
) return self as result
,constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
) return self as result
,constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
, start_date date
) return self as result
,constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
, start_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SeasonDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SeasonDesc') := "ns_name_SeasonDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'phase_id') := phase_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'phase_desc') := phase_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.phase_id := phase_id;
RETURN;
end;
constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.phase_id := phase_id;
self.phase_desc := phase_desc;
RETURN;
end;
constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.phase_id := phase_id;
self.phase_desc := phase_desc;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_SeasonDtl_REC"
(
  rib_oid number
, phase_id number
, phase_desc varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.phase_id := phase_id;
self.phase_desc := phase_desc;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_SeasonDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_SeasonDtl_TBL" AS TABLE OF "RIB_SeasonDtl_REC";
/
DROP TYPE "RIB_SeasonDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_SeasonDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_SeasonDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  season_id number(3),
  season_desc varchar2(120),
  start_date date,
  end_date date,
  SeasonDtl_TBL "RIB_SeasonDtl_TBL",   -- Size of "RIB_SeasonDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
) return self as result
,constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
) return self as result
,constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
) return self as result
,constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
, end_date date
) return self as result
,constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
, end_date date
, SeasonDtl_TBL "RIB_SeasonDtl_TBL"  -- Size of "RIB_SeasonDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_SeasonDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_SeasonDesc') := "ns_name_SeasonDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'season_id') := season_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'season_desc') := season_desc;
  rib_obj_util.g_RIB_element_values(i_prefix||'start_date') := start_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
  IF SeasonDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'SeasonDtl_TBL.';
    FOR INDX IN SeasonDtl_TBL.FIRST()..SeasonDtl_TBL.LAST() LOOP
      SeasonDtl_TBL(indx).appendNodeValues( i_prefix||indx||'SeasonDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
RETURN;
end;
constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.season_desc := season_desc;
RETURN;
end;
constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.season_desc := season_desc;
self.start_date := start_date;
RETURN;
end;
constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.season_desc := season_desc;
self.start_date := start_date;
self.end_date := end_date;
RETURN;
end;
constructor function "RIB_SeasonDesc_REC"
(
  rib_oid number
, season_id number
, season_desc varchar2
, start_date date
, end_date date
, SeasonDtl_TBL "RIB_SeasonDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.season_desc := season_desc;
self.start_date := start_date;
self.end_date := end_date;
self.SeasonDtl_TBL := SeasonDtl_TBL;
RETURN;
end;
END;
/
