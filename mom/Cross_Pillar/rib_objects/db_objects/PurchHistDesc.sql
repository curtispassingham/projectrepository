@@LocaleDesc.sql;
/
@@PurchItmHdrColDesc.sql;
/
DROP TYPE "RIB_PurchHistDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PurchHistDesc_REC";
/
DROP TYPE "RIB_PurchHistDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PurchHistDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PurchHistDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  initiate_loc_type varchar2(1), -- initiate_loc_type is enumeration field, valid values are [W, S, M, O] (all lower-case)
  initiate_loc_id varchar2(10),
  purchase_type varchar2(1), -- purchase_type is enumeration field, valid values are [T, O] (all lower-case)
  id varchar2(48),
  currency_code varchar2(3),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, PARTIAL, CANCELED, COMPLETED, FILLED, INPROCESS, DELIVERED, FAILED, UNKNOWN, PENDING] (all lower-case)
  grand_total number(20,4),
  submitted_date date,
  number_of_items number(10),
  PurchItmHdrColDesc "RIB_PurchItmHdrColDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PurchHistDesc_REC"
(
  rib_oid number
, initiate_loc_type varchar2
, initiate_loc_id varchar2
, purchase_type varchar2
, id varchar2
, currency_code varchar2
, status varchar2
, grand_total number
, submitted_date date
, number_of_items number
) return self as result
,constructor function "RIB_PurchHistDesc_REC"
(
  rib_oid number
, initiate_loc_type varchar2
, initiate_loc_id varchar2
, purchase_type varchar2
, id varchar2
, currency_code varchar2
, status varchar2
, grand_total number
, submitted_date date
, number_of_items number
, PurchItmHdrColDesc "RIB_PurchItmHdrColDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PurchHistDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PurchHistDesc') := "ns_name_PurchHistDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'initiate_loc_type') := initiate_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'initiate_loc_id') := initiate_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_type') := purchase_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'grand_total') := grand_total;
  rib_obj_util.g_RIB_element_values(i_prefix||'submitted_date') := submitted_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'number_of_items') := number_of_items;
  l_new_pre :=i_prefix||'PurchItmHdrColDesc.';
  PurchItmHdrColDesc.appendNodeValues( i_prefix||'PurchItmHdrColDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_PurchHistDesc_REC"
(
  rib_oid number
, initiate_loc_type varchar2
, initiate_loc_id varchar2
, purchase_type varchar2
, id varchar2
, currency_code varchar2
, status varchar2
, grand_total number
, submitted_date date
, number_of_items number
) return self as result is
begin
self.rib_oid := rib_oid;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.purchase_type := purchase_type;
self.id := id;
self.currency_code := currency_code;
self.status := status;
self.grand_total := grand_total;
self.submitted_date := submitted_date;
self.number_of_items := number_of_items;
RETURN;
end;
constructor function "RIB_PurchHistDesc_REC"
(
  rib_oid number
, initiate_loc_type varchar2
, initiate_loc_id varchar2
, purchase_type varchar2
, id varchar2
, currency_code varchar2
, status varchar2
, grand_total number
, submitted_date date
, number_of_items number
, PurchItmHdrColDesc "RIB_PurchItmHdrColDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.initiate_loc_type := initiate_loc_type;
self.initiate_loc_id := initiate_loc_id;
self.purchase_type := purchase_type;
self.id := id;
self.currency_code := currency_code;
self.status := status;
self.grand_total := grand_total;
self.submitted_date := submitted_date;
self.number_of_items := number_of_items;
self.PurchItmHdrColDesc := PurchItmHdrColDesc;
RETURN;
end;
END;
/
