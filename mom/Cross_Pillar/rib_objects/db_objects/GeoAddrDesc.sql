DROP TYPE "RIB_GeoAddrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GeoAddrDesc_REC";
/
DROP TYPE "RIB_GeoAddrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GeoAddrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GeoAddrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  addr_id number(22),
  addr_type varchar2(8), -- addr_type is enumeration field, valid values are [HOME, WORK, SHIPPING, BILLING, OTHER] (all lower-case)
  address_alias varchar2(250),
  address_1 varchar2(240),
  address_2 varchar2(240),
  address_3 varchar2(240),
  address_4 varchar2(240),
  address_5 varchar2(240),
  city varchar2(120),
  county varchar2(250),
  state_code varchar2(3),
  state_name varchar2(120),
  country_code varchar2(3),
  country_name varchar2(120),
  postal_code varchar2(30),
  jurisdiction_code varchar2(10),
  contact_preference_code varchar2(120),
  validated_addr_flag varchar2(5), --validated_addr_flag is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
, contact_preference_code varchar2
) return self as result
,constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
, contact_preference_code varchar2
, validated_addr_flag varchar2  --validated_addr_flag is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GeoAddrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GeoAddrDesc') := "ns_name_GeoAddrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_id') := addr_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'addr_type') := addr_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_alias') := address_alias;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_1') := address_1;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_2') := address_2;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_3') := address_3;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_4') := address_4;
  rib_obj_util.g_RIB_element_values(i_prefix||'address_5') := address_5;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'county') := county;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_code') := state_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_name') := state_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_code') := country_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_name') := country_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'jurisdiction_code') := jurisdiction_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'contact_preference_code') := contact_preference_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'validated_addr_flag') := validated_addr_flag;
END appendNodeValues;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
self.country_name := country_name;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
self.country_name := country_name;
self.postal_code := postal_code;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
self.country_name := country_name;
self.postal_code := postal_code;
self.jurisdiction_code := jurisdiction_code;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
, contact_preference_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
self.country_name := country_name;
self.postal_code := postal_code;
self.jurisdiction_code := jurisdiction_code;
self.contact_preference_code := contact_preference_code;
RETURN;
end;
constructor function "RIB_GeoAddrDesc_REC"
(
  rib_oid number
, addr_id number
, addr_type varchar2
, address_alias varchar2
, address_1 varchar2
, address_2 varchar2
, address_3 varchar2
, address_4 varchar2
, address_5 varchar2
, city varchar2
, county varchar2
, state_code varchar2
, state_name varchar2
, country_code varchar2
, country_name varchar2
, postal_code varchar2
, jurisdiction_code varchar2
, contact_preference_code varchar2
, validated_addr_flag varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.addr_id := addr_id;
self.addr_type := addr_type;
self.address_alias := address_alias;
self.address_1 := address_1;
self.address_2 := address_2;
self.address_3 := address_3;
self.address_4 := address_4;
self.address_5 := address_5;
self.city := city;
self.county := county;
self.state_code := state_code;
self.state_name := state_name;
self.country_code := country_code;
self.country_name := country_name;
self.postal_code := postal_code;
self.jurisdiction_code := jurisdiction_code;
self.contact_preference_code := contact_preference_code;
self.validated_addr_flag := validated_addr_flag;
RETURN;
end;
END;
/
