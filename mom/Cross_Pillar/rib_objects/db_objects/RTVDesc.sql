DROP TYPE "RIB_RTVDtlUin_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVDtlUin_REC";
/
DROP TYPE "RIB_RTVDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVDtl_REC";
/
DROP TYPE "RIB_RTVDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVDesc_REC";
/
DROP TYPE "RIB_RTVDtlUin_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVDtlUin_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  status number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVDtlUin_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVDesc') := "ns_name_RTVDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
END appendNodeValues;
constructor function "RIB_RTVDtlUin_REC"
(
  rib_oid number
, uin varchar2
, status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.status := status;
RETURN;
end;
END;
/
DROP TYPE "RIB_RTVDtlUin_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVDtlUin_TBL" AS TABLE OF "RIB_RTVDtlUin_REC";
/
DROP TYPE "RIB_RTVDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  unit_qty number(12,4),
  container_qty number(6),
  from_disposition varchar2(4),
  to_disposition varchar2(4),
  unit_cost number(20,4),
  reason varchar2(6),
  weight number(12,4),
  weight_uom varchar2(4),
  gross_cost number(20,4),
  RTVDtlUin_TBL "RIB_RTVDtlUin_TBL",   -- Size of "RIB_RTVDtlUin_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
, gross_cost number
) return self as result
,constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
, gross_cost number
, RTVDtlUin_TBL "RIB_RTVDtlUin_TBL"  -- Size of "RIB_RTVDtlUin_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVDesc') := "ns_name_RTVDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_qty') := unit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_qty') := container_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'from_disposition') := from_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_disposition') := to_disposition;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason') := reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight_uom') := weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'gross_cost') := gross_cost;
  IF RTVDtlUin_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RTVDtlUin_TBL.';
    FOR INDX IN RTVDtlUin_TBL.FIRST()..RTVDtlUin_TBL.LAST() LOOP
      RTVDtlUin_TBL(indx).appendNodeValues( i_prefix||indx||'RTVDtlUin_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
self.reason := reason;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
self.reason := reason;
self.weight := weight;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
self.reason := reason;
self.weight := weight;
self.weight_uom := weight_uom;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
, gross_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
self.reason := reason;
self.weight := weight;
self.weight_uom := weight_uom;
self.gross_cost := gross_cost;
RETURN;
end;
constructor function "RIB_RTVDtl_REC"
(
  rib_oid number
, item_id varchar2
, unit_qty number
, container_qty number
, from_disposition varchar2
, to_disposition varchar2
, unit_cost number
, reason varchar2
, weight number
, weight_uom varchar2
, gross_cost number
, RTVDtlUin_TBL "RIB_RTVDtlUin_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.unit_qty := unit_qty;
self.container_qty := container_qty;
self.from_disposition := from_disposition;
self.to_disposition := to_disposition;
self.unit_cost := unit_cost;
self.reason := reason;
self.weight := weight;
self.weight_uom := weight_uom;
self.gross_cost := gross_cost;
self.RTVDtlUin_TBL := RTVDtlUin_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_RTVDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RTVDtl_TBL" AS TABLE OF "RIB_RTVDtl_REC";
/
DROP TYPE "RIB_RTVDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RTVDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RTVDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dc_dest_id varchar2(10),
  rtv_id varchar2(14),
  rtn_auth_nbr varchar2(12),
  vendor_nbr varchar2(10),
  ship_address1 varchar2(240),
  ship_address2 varchar2(240),
  ship_address3 varchar2(240),
  state varchar2(3),
  city varchar2(120),
  shipto_zip varchar2(30),
  country varchar2(3),
  creation_ts date,
  comments varchar2(2000),
  rtv_order_no number(10),
  RTVDtl_TBL "RIB_RTVDtl_TBL",   -- Size of "RIB_RTVDtl_TBL" is unbounded
  status_ind varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
, RTVDtl_TBL "RIB_RTVDtl_TBL"  -- Size of "RIB_RTVDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
, RTVDtl_TBL "RIB_RTVDtl_TBL"  -- Size of "RIB_RTVDtl_TBL" is unbounded
, status_ind varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RTVDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RTVDesc') := "ns_name_RTVDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'rtv_id') := rtv_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'rtn_auth_nbr') := rtn_auth_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendor_nbr') := vendor_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address1') := ship_address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address2') := ship_address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_address3') := ship_address3;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipto_zip') := shipto_zip;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'creation_ts') := creation_ts;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'rtv_order_no') := rtv_order_no;
  IF RTVDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RTVDtl_TBL.';
    FOR INDX IN RTVDtl_TBL.FIRST()..RTVDtl_TBL.LAST() LOOP
      RTVDtl_TBL(indx).appendNodeValues( i_prefix||indx||'RTVDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'status_ind') := status_ind;
END appendNodeValues;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
self.creation_ts := creation_ts;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
self.creation_ts := creation_ts;
self.comments := comments;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
self.creation_ts := creation_ts;
self.comments := comments;
self.rtv_order_no := rtv_order_no;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
, RTVDtl_TBL "RIB_RTVDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
self.creation_ts := creation_ts;
self.comments := comments;
self.rtv_order_no := rtv_order_no;
self.RTVDtl_TBL := RTVDtl_TBL;
RETURN;
end;
constructor function "RIB_RTVDesc_REC"
(
  rib_oid number
, dc_dest_id varchar2
, rtv_id varchar2
, rtn_auth_nbr varchar2
, vendor_nbr varchar2
, ship_address1 varchar2
, ship_address2 varchar2
, ship_address3 varchar2
, state varchar2
, city varchar2
, shipto_zip varchar2
, country varchar2
, creation_ts date
, comments varchar2
, rtv_order_no number
, RTVDtl_TBL "RIB_RTVDtl_TBL"
, status_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dc_dest_id := dc_dest_id;
self.rtv_id := rtv_id;
self.rtn_auth_nbr := rtn_auth_nbr;
self.vendor_nbr := vendor_nbr;
self.ship_address1 := ship_address1;
self.ship_address2 := ship_address2;
self.ship_address3 := ship_address3;
self.state := state;
self.city := city;
self.shipto_zip := shipto_zip;
self.country := country;
self.creation_ts := creation_ts;
self.comments := comments;
self.rtv_order_no := rtv_order_no;
self.RTVDtl_TBL := RTVDtl_TBL;
self.status_ind := status_ind;
RETURN;
end;
END;
/
