DROP TYPE "RIB_StrVnsHdrCriVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsHdrCriVo_REC";
/
DROP TYPE "RIB_StrVnsHdrCriVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsHdrCriVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsHdrCriVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  shipment_id number(15),
  supplier_id number(10),
  item_id varchar2(25),
  authorization_code varchar2(12),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, SUBMITTED, SHIPPED, CANCELED, ACTIVE, NO_VALUE] (all lower-case)
  reason_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsHdrCriVo_REC"
(
  rib_oid number
, store_id number
, shipment_id number
, supplier_id number
, item_id varchar2
, authorization_code varchar2
, status varchar2
) return self as result
,constructor function "RIB_StrVnsHdrCriVo_REC"
(
  rib_oid number
, store_id number
, shipment_id number
, supplier_id number
, item_id varchar2
, authorization_code varchar2
, status varchar2
, reason_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsHdrCriVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsHdrCriVo') := "ns_name_StrVnsHdrCriVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
END appendNodeValues;
constructor function "RIB_StrVnsHdrCriVo_REC"
(
  rib_oid number
, store_id number
, shipment_id number
, supplier_id number
, item_id varchar2
, authorization_code varchar2
, status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.shipment_id := shipment_id;
self.supplier_id := supplier_id;
self.item_id := item_id;
self.authorization_code := authorization_code;
self.status := status;
RETURN;
end;
constructor function "RIB_StrVnsHdrCriVo_REC"
(
  rib_oid number
, store_id number
, shipment_id number
, supplier_id number
, item_id varchar2
, authorization_code varchar2
, status varchar2
, reason_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.shipment_id := shipment_id;
self.supplier_id := supplier_id;
self.item_id := item_id;
self.authorization_code := authorization_code;
self.status := status;
self.reason_id := reason_id;
RETURN;
end;
END;
/
