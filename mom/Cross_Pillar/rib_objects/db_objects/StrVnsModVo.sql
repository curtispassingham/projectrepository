DROP TYPE "RIB_StrVnsModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsModVo_REC";
/
DROP TYPE "RIB_StrVnsModBol_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsModBol_REC";
/
DROP TYPE "RIB_StrVnsModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsModNote_REC";
/
DROP TYPE "RIB_StrVnsModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsModNote_TBL" AS TABLE OF "RIB_StrVnsModNote_REC";
/
DROP TYPE "RIB_StrVnsModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  store_id number(10),
  supplier_id number(10),
  return_context varchar2(20), -- return_context is enumeration field, valid values are [INVALID_DELIVERY, SEASONAL, DAMAGED, NONE] (all lower-case)
  authorization_code varchar2(12),
  not_after_date date,
  StrVnsModBol "RIB_StrVnsModBol_REC",
  StrVnsModNote_TBL "RIB_StrVnsModNote_TBL",   -- Size of "RIB_StrVnsModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
) return self as result
,constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
) return self as result
,constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
) return self as result
,constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, StrVnsModBol "RIB_StrVnsModBol_REC"
) return self as result
,constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, StrVnsModBol "RIB_StrVnsModBol_REC"
, StrVnsModNote_TBL "RIB_StrVnsModNote_TBL"  -- Size of "RIB_StrVnsModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsModVo') := "ns_name_StrVnsModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_context') := return_context;
  rib_obj_util.g_RIB_element_values(i_prefix||'authorization_code') := authorization_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  l_new_pre :=i_prefix||'StrVnsModBol.';
  StrVnsModBol.appendNodeValues( i_prefix||'StrVnsModBol');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF StrVnsModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVnsModNote_TBL.';
    FOR INDX IN StrVnsModNote_TBL.FIRST()..StrVnsModNote_TBL.LAST() LOOP
      StrVnsModNote_TBL(indx).appendNodeValues( i_prefix||indx||'StrVnsModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.return_context := return_context;
RETURN;
end;
constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.return_context := return_context;
self.authorization_code := authorization_code;
RETURN;
end;
constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
RETURN;
end;
constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, StrVnsModBol "RIB_StrVnsModBol_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.StrVnsModBol := StrVnsModBol;
RETURN;
end;
constructor function "RIB_StrVnsModVo_REC"
(
  rib_oid number
, shipment_id number
, store_id number
, supplier_id number
, return_context varchar2
, authorization_code varchar2
, not_after_date date
, StrVnsModBol "RIB_StrVnsModBol_REC"
, StrVnsModNote_TBL "RIB_StrVnsModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.store_id := store_id;
self.supplier_id := supplier_id;
self.return_context := return_context;
self.authorization_code := authorization_code;
self.not_after_date := not_after_date;
self.StrVnsModBol := StrVnsModBol;
self.StrVnsModNote_TBL := StrVnsModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVnsModBol_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsModBol_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bill_of_lading_motive_id varchar2(120),
  bill_of_lading_tax_id varchar2(18),
  requested_pickup_date date,
  carrier_type varchar2(20), -- carrier_type is enumeration field, valid values are [SENDER, RECEIVER, THIRD_PARTY] (all lower-case)
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  carrier_name varchar2(128),
  carrier_address varchar2(2000),
  ship_to_address_type varchar2(10), -- ship_to_address_type is enumeration field, valid values are [BUSINESS, POSTAL, RETURNS, ORDER, INVOICE, REMITTANCE, BILLING, DELIVERY] (all lower-case)
  alternate_ship_to_address varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsModBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result
,constructor function "RIB_StrVnsModBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsModBol_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsModVo') := "ns_name_StrVnsModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_motive_id') := bill_of_lading_motive_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_tax_id') := bill_of_lading_tax_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pickup_date') := requested_pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_type') := carrier_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_address') := carrier_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'ship_to_address_type') := ship_to_address_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_ship_to_address') := alternate_ship_to_address;
END appendNodeValues;
constructor function "RIB_StrVnsModBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
RETURN;
end;
constructor function "RIB_StrVnsModBol_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, bill_of_lading_tax_id varchar2
, requested_pickup_date date
, carrier_type varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, ship_to_address_type varchar2
, alternate_ship_to_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.bill_of_lading_tax_id := bill_of_lading_tax_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_type := carrier_type;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.ship_to_address_type := ship_to_address_type;
self.alternate_ship_to_address := alternate_ship_to_address;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrVnsModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsModVo') := "ns_name_StrVnsModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_StrVnsModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
