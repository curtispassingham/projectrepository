@@RetItemIdentDesc.sql;
/
@@RetTendTypeDesc.sql;
/
@@RetMsgExtDesc.sql;
/
@@RetStoreLangDesc.sql;
/
@@TransIdDesc.sql;
/
DROP TYPE "RIB_RetAuthDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RetAuthDesc_REC";
/
DROP TYPE "RIB_ReturnRequest_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ReturnRequest_REC";
/
DROP TYPE "RIB_ItemReturnInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnInfo_REC";
/
DROP TYPE "RIB_ItemTransactionInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTransactionInfo_REC";
/
DROP TYPE "RIB_OriginalTender_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_OriginalTender_REC";
/
DROP TYPE "RIB_RetCustomerInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_RetCustomerInfo_REC";
/
DROP TYPE "RIB_TransactionId_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TransactionId_REC";
/
DROP TYPE "RIB_PositiveId_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PositiveId_REC";
/
DROP TYPE "RIB_MoreCustomerInfo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_MoreCustomerInfo_REC";
/
DROP TYPE "RIB_RetAuthDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RetAuthDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ReturnRequest "RIB_ReturnRequest_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RetAuthDesc_REC"
(
  rib_oid number
, ReturnRequest "RIB_ReturnRequest_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RetAuthDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'ReturnRequest.';
  ReturnRequest.appendNodeValues( i_prefix||'ReturnRequest');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_RetAuthDesc_REC"
(
  rib_oid number
, ReturnRequest "RIB_ReturnRequest_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ReturnRequest := ReturnRequest;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnInfo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemReturnInfo_TBL" AS TABLE OF "RIB_ItemReturnInfo_REC";
/
DROP TYPE "RIB_ReturnRequest_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ReturnRequest_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL",   -- Size of "RIB_ItemReturnInfo_TBL" is unbounded
  return_store_id varchar2(5),
  return_workstation_id varchar2(3),
  return_business_date date,
  return_date date,
  currency_iso_code varchar2(3),
  RetStoreLangDesc "RIB_RetStoreLangDesc_REC",
  employee_id varchar2(10),
  customer_type varchar2(25),
  RetCustomerInfo "RIB_RetCustomerInfo_REC",
  MoreCustomerInfo "RIB_MoreCustomerInfo_REC",
  PositiveId "RIB_PositiveId_REC",
  transaction_type varchar2(20),
  return_ticket_id varchar2(30),
  RetMsgExtDesc "RIB_RetMsgExtDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"  -- Size of "RIB_ItemReturnInfo_TBL" is unbounded
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
) return self as result
,constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"  -- Size of "RIB_ItemReturnInfo_TBL" is unbounded
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
, return_ticket_id varchar2
) return self as result
,constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"  -- Size of "RIB_ItemReturnInfo_TBL" is unbounded
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
, return_ticket_id varchar2
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ReturnRequest_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ItemReturnInfo_TBL.';
  FOR INDX IN ItemReturnInfo_TBL.FIRST()..ItemReturnInfo_TBL.LAST() LOOP
    ItemReturnInfo_TBL(indx).appendNodeValues( i_prefix||indx||'ItemReturnInfo_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_store_id') := return_store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_workstation_id') := return_workstation_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_business_date') := return_business_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_date') := return_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_iso_code') := currency_iso_code;
  l_new_pre :=i_prefix||'RetStoreLangDesc.';
  RetStoreLangDesc.appendNodeValues( i_prefix||'RetStoreLangDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'employee_id') := employee_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_type') := customer_type;
  l_new_pre :=i_prefix||'RetCustomerInfo.';
  RetCustomerInfo.appendNodeValues( i_prefix||'RetCustomerInfo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'MoreCustomerInfo.';
  MoreCustomerInfo.appendNodeValues( i_prefix||'MoreCustomerInfo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'PositiveId.';
  PositiveId.appendNodeValues( i_prefix||'PositiveId');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_type') := transaction_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_ticket_id') := return_ticket_id;
  l_new_pre :=i_prefix||'RetMsgExtDesc.';
  RetMsgExtDesc.appendNodeValues( i_prefix||'RetMsgExtDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemReturnInfo_TBL := ItemReturnInfo_TBL;
self.return_store_id := return_store_id;
self.return_workstation_id := return_workstation_id;
self.return_business_date := return_business_date;
self.return_date := return_date;
self.currency_iso_code := currency_iso_code;
self.RetStoreLangDesc := RetStoreLangDesc;
self.employee_id := employee_id;
self.customer_type := customer_type;
self.RetCustomerInfo := RetCustomerInfo;
self.MoreCustomerInfo := MoreCustomerInfo;
self.PositiveId := PositiveId;
self.transaction_type := transaction_type;
RETURN;
end;
constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
, return_ticket_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemReturnInfo_TBL := ItemReturnInfo_TBL;
self.return_store_id := return_store_id;
self.return_workstation_id := return_workstation_id;
self.return_business_date := return_business_date;
self.return_date := return_date;
self.currency_iso_code := currency_iso_code;
self.RetStoreLangDesc := RetStoreLangDesc;
self.employee_id := employee_id;
self.customer_type := customer_type;
self.RetCustomerInfo := RetCustomerInfo;
self.MoreCustomerInfo := MoreCustomerInfo;
self.PositiveId := PositiveId;
self.transaction_type := transaction_type;
self.return_ticket_id := return_ticket_id;
RETURN;
end;
constructor function "RIB_ReturnRequest_REC"
(
  rib_oid number
, ItemReturnInfo_TBL "RIB_ItemReturnInfo_TBL"
, return_store_id varchar2
, return_workstation_id varchar2
, return_business_date date
, return_date date
, currency_iso_code varchar2
, RetStoreLangDesc "RIB_RetStoreLangDesc_REC"
, employee_id varchar2
, customer_type varchar2
, RetCustomerInfo "RIB_RetCustomerInfo_REC"
, MoreCustomerInfo "RIB_MoreCustomerInfo_REC"
, PositiveId "RIB_PositiveId_REC"
, transaction_type varchar2
, return_ticket_id varchar2
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemReturnInfo_TBL := ItemReturnInfo_TBL;
self.return_store_id := return_store_id;
self.return_workstation_id := return_workstation_id;
self.return_business_date := return_business_date;
self.return_date := return_date;
self.currency_iso_code := currency_iso_code;
self.RetStoreLangDesc := RetStoreLangDesc;
self.employee_id := employee_id;
self.customer_type := customer_type;
self.RetCustomerInfo := RetCustomerInfo;
self.MoreCustomerInfo := MoreCustomerInfo;
self.PositiveId := PositiveId;
self.transaction_type := transaction_type;
self.return_ticket_id := return_ticket_id;
self.RetMsgExtDesc := RetMsgExtDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemReturnInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemReturnInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemTransactionInfo "RIB_ItemTransactionInfo_REC",
  RetItemIdentDesc "RIB_RetItemIdentDesc_REC",
  return_reason varchar2(255),
  quantity number(11),
  amount_paid_per_unit number(15),
  amount_per_unit_in_base_cur number(15),
  requested_adjusted_price number(15),
  requested_price_in_base_cur number(15),
  serial_number varchar2(10),
  item_condition varchar2(255),
  manually_entered varchar2(5), --manually_entered is boolean field, valid values are true,false (all lower-case) 
  RetMsgExtDesc "RIB_RetMsgExtDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemReturnInfo_REC"
(
  rib_oid number
, ItemTransactionInfo "RIB_ItemTransactionInfo_REC"
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, return_reason varchar2
, quantity number
, amount_paid_per_unit number
, amount_per_unit_in_base_cur number
, requested_adjusted_price number
, requested_price_in_base_cur number
, serial_number varchar2
, item_condition varchar2
, manually_entered varchar2  --manually_entered is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ItemReturnInfo_REC"
(
  rib_oid number
, ItemTransactionInfo "RIB_ItemTransactionInfo_REC"
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, return_reason varchar2
, quantity number
, amount_paid_per_unit number
, amount_per_unit_in_base_cur number
, requested_adjusted_price number
, requested_price_in_base_cur number
, serial_number varchar2
, item_condition varchar2
, manually_entered varchar2  --manually_entered is boolean field, valid values are true,false (all lower-case)
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemReturnInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'ItemTransactionInfo.';
  ItemTransactionInfo.appendNodeValues( i_prefix||'ItemTransactionInfo');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  l_new_pre :=i_prefix||'RetItemIdentDesc.';
  RetItemIdentDesc.appendNodeValues( i_prefix||'RetItemIdentDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'return_reason') := return_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount_paid_per_unit') := amount_paid_per_unit;
  rib_obj_util.g_RIB_element_values(i_prefix||'amount_per_unit_in_base_cur') := amount_per_unit_in_base_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_adjusted_price') := requested_adjusted_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_price_in_base_cur') := requested_price_in_base_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_number') := serial_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_condition') := item_condition;
  rib_obj_util.g_RIB_element_values(i_prefix||'manually_entered') := manually_entered;
  l_new_pre :=i_prefix||'RetMsgExtDesc.';
  RetMsgExtDesc.appendNodeValues( i_prefix||'RetMsgExtDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ItemReturnInfo_REC"
(
  rib_oid number
, ItemTransactionInfo "RIB_ItemTransactionInfo_REC"
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, return_reason varchar2
, quantity number
, amount_paid_per_unit number
, amount_per_unit_in_base_cur number
, requested_adjusted_price number
, requested_price_in_base_cur number
, serial_number varchar2
, item_condition varchar2
, manually_entered varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemTransactionInfo := ItemTransactionInfo;
self.RetItemIdentDesc := RetItemIdentDesc;
self.return_reason := return_reason;
self.quantity := quantity;
self.amount_paid_per_unit := amount_paid_per_unit;
self.amount_per_unit_in_base_cur := amount_per_unit_in_base_cur;
self.requested_adjusted_price := requested_adjusted_price;
self.requested_price_in_base_cur := requested_price_in_base_cur;
self.serial_number := serial_number;
self.item_condition := item_condition;
self.manually_entered := manually_entered;
RETURN;
end;
constructor function "RIB_ItemReturnInfo_REC"
(
  rib_oid number
, ItemTransactionInfo "RIB_ItemTransactionInfo_REC"
, RetItemIdentDesc "RIB_RetItemIdentDesc_REC"
, return_reason varchar2
, quantity number
, amount_paid_per_unit number
, amount_per_unit_in_base_cur number
, requested_adjusted_price number
, requested_price_in_base_cur number
, serial_number varchar2
, item_condition varchar2
, manually_entered varchar2
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemTransactionInfo := ItemTransactionInfo;
self.RetItemIdentDesc := RetItemIdentDesc;
self.return_reason := return_reason;
self.quantity := quantity;
self.amount_paid_per_unit := amount_paid_per_unit;
self.amount_per_unit_in_base_cur := amount_per_unit_in_base_cur;
self.requested_adjusted_price := requested_adjusted_price;
self.requested_price_in_base_cur := requested_price_in_base_cur;
self.serial_number := serial_number;
self.item_condition := item_condition;
self.manually_entered := manually_entered;
self.RetMsgExtDesc := RetMsgExtDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItemTransactionInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemTransactionInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  receipted varchar2(5), --receipted is boolean field, valid values are true,false (all lower-case) 
  TransactionId "RIB_TransactionId_REC",
  found varchar2(5), --found is boolean field, valid values are true,false (all lower-case) 
  valid_at_point_of_return varchar2(5), --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case) 
  gift_receipt varchar2(5), --gift_receipt is boolean field, valid values are true,false (all lower-case) 
  purchase_date date,
  delivery_date date,
  validation_amount number(15),
  validation_amount_in_base_cur number(15),
  OriginalTender "RIB_OriginalTender_REC",
  sale_quantity number(11),
  RetMsgExtDesc "RIB_RetMsgExtDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
, validation_amount number
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
, sale_quantity number
) return self as result
,constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2  --receipted is boolean field, valid values are true,false (all lower-case)
, TransactionId "RIB_TransactionId_REC"
, found varchar2  --found is boolean field, valid values are true,false (all lower-case)
, valid_at_point_of_return varchar2  --valid_at_point_of_return is boolean field, valid values are true,false (all lower-case)
, gift_receipt varchar2  --gift_receipt is boolean field, valid values are true,false (all lower-case)
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
, sale_quantity number
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemTransactionInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'receipted') := receipted;
  l_new_pre :=i_prefix||'TransactionId.';
  TransactionId.appendNodeValues( i_prefix||'TransactionId');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'found') := found;
  rib_obj_util.g_RIB_element_values(i_prefix||'valid_at_point_of_return') := valid_at_point_of_return;
  rib_obj_util.g_RIB_element_values(i_prefix||'gift_receipt') := gift_receipt;
  rib_obj_util.g_RIB_element_values(i_prefix||'purchase_date') := purchase_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'validation_amount') := validation_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'validation_amount_in_base_cur') := validation_amount_in_base_cur;
  l_new_pre :=i_prefix||'OriginalTender.';
  OriginalTender.appendNodeValues( i_prefix||'OriginalTender');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  rib_obj_util.g_RIB_element_values(i_prefix||'sale_quantity') := sale_quantity;
  l_new_pre :=i_prefix||'RetMsgExtDesc.';
  RetMsgExtDesc.appendNodeValues( i_prefix||'RetMsgExtDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
, validation_amount number
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
self.validation_amount := validation_amount;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
self.validation_amount := validation_amount;
self.validation_amount_in_base_cur := validation_amount_in_base_cur;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
self.validation_amount := validation_amount;
self.validation_amount_in_base_cur := validation_amount_in_base_cur;
self.OriginalTender := OriginalTender;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
, sale_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
self.validation_amount := validation_amount;
self.validation_amount_in_base_cur := validation_amount_in_base_cur;
self.OriginalTender := OriginalTender;
self.sale_quantity := sale_quantity;
RETURN;
end;
constructor function "RIB_ItemTransactionInfo_REC"
(
  rib_oid number
, receipted varchar2
, TransactionId "RIB_TransactionId_REC"
, found varchar2
, valid_at_point_of_return varchar2
, gift_receipt varchar2
, purchase_date date
, delivery_date date
, validation_amount number
, validation_amount_in_base_cur number
, OriginalTender "RIB_OriginalTender_REC"
, sale_quantity number
, RetMsgExtDesc "RIB_RetMsgExtDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.receipted := receipted;
self.TransactionId := TransactionId;
self.found := found;
self.valid_at_point_of_return := valid_at_point_of_return;
self.gift_receipt := gift_receipt;
self.purchase_date := purchase_date;
self.delivery_date := delivery_date;
self.validation_amount := validation_amount;
self.validation_amount_in_base_cur := validation_amount_in_base_cur;
self.OriginalTender := OriginalTender;
self.sale_quantity := sale_quantity;
self.RetMsgExtDesc := RetMsgExtDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_RetTendTypeDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RetTendTypeDesc_TBL" AS TABLE OF "RIB_RetTendTypeDesc_REC";
/
DROP TYPE "RIB_OriginalTender_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_OriginalTender_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL",   -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_OriginalTender_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"  -- Size of "RIB_RetTendTypeDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_OriginalTender_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  IF RetTendTypeDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RetTendTypeDesc_TBL.';
    FOR INDX IN RetTendTypeDesc_TBL.FIRST()..RetTendTypeDesc_TBL.LAST() LOOP
      RetTendTypeDesc_TBL(indx).appendNodeValues( i_prefix||indx||'RetTendTypeDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_OriginalTender_REC"
(
  rib_oid number
, RetTendTypeDesc_TBL "RIB_RetTendTypeDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.RetTendTypeDesc_TBL := RetTendTypeDesc_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_RetCustomerInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_RetCustomerInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_id varchar2(14),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_RetCustomerInfo_REC"
(
  rib_oid number
, customer_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_RetCustomerInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_id') := customer_id;
END appendNodeValues;
constructor function "RIB_RetCustomerInfo_REC"
(
  rib_oid number
, customer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_id := customer_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_TransactionId_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TransactionId_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  TransIdDesc "RIB_TransIdDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TransactionId_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TransactionId_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'TransIdDesc.';
  TransIdDesc.appendNodeValues( i_prefix||'TransIdDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_TransactionId_REC"
(
  rib_oid number
, TransIdDesc "RIB_TransIdDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.TransIdDesc := TransIdDesc;
RETURN;
end;
END;
/
DROP TYPE "RIB_PositiveId_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PositiveId_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  id varchar2(1000),
  type varchar2(50),
  issuer_country varchar2(2),
  issuer_state varchar2(3),
  issued date,
  expiration date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
) return self as result
,constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
, issued date
) return self as result
,constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
, issued date
, expiration date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PositiveId_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'id') := id;
  rib_obj_util.g_RIB_element_values(i_prefix||'type') := type;
  rib_obj_util.g_RIB_element_values(i_prefix||'issuer_country') := issuer_country;
  rib_obj_util.g_RIB_element_values(i_prefix||'issuer_state') := issuer_state;
  rib_obj_util.g_RIB_element_values(i_prefix||'issued') := issued;
  rib_obj_util.g_RIB_element_values(i_prefix||'expiration') := expiration;
END appendNodeValues;
constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.type := type;
self.issuer_country := issuer_country;
self.issuer_state := issuer_state;
RETURN;
end;
constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
, issued date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.type := type;
self.issuer_country := issuer_country;
self.issuer_state := issuer_state;
self.issued := issued;
RETURN;
end;
constructor function "RIB_PositiveId_REC"
(
  rib_oid number
, id varchar2
, type varchar2
, issuer_country varchar2
, issuer_state varchar2
, issued date
, expiration date
) return self as result is
begin
self.rib_oid := rib_oid;
self.id := id;
self.type := type;
self.issuer_country := issuer_country;
self.issuer_state := issuer_state;
self.issued := issued;
self.expiration := expiration;
RETURN;
end;
END;
/
DROP TYPE "RIB_MoreCustomerInfo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_MoreCustomerInfo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_RetAuthDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  last_name varchar2(120),
  first_name varchar2(120),
  middle_name varchar2(120),
  gender varchar2(11), -- gender is enumeration field, valid values are [FEMALE, MALE, UNSPECIFIED] (all lower-case)
  birth_date varchar2(10),
  address1 varchar2(240),
  address2 varchar2(240),
  city varchar2(120),
  state varchar2(30),
  postal_code varchar2(30),
  country varchar2(30),
  telephone_area_code varchar2(3),
  telephone_local_number varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
) return self as result
,constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
, telephone_area_code varchar2
) return self as result
,constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
, telephone_area_code varchar2
, telephone_local_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_MoreCustomerInfo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_RetAuthDesc') := "ns_name_RetAuthDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'last_name') := last_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'first_name') := first_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'middle_name') := middle_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'gender') := gender;
  rib_obj_util.g_RIB_element_values(i_prefix||'birth_date') := birth_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'address1') := address1;
  rib_obj_util.g_RIB_element_values(i_prefix||'address2') := address2;
  rib_obj_util.g_RIB_element_values(i_prefix||'city') := city;
  rib_obj_util.g_RIB_element_values(i_prefix||'state') := state;
  rib_obj_util.g_RIB_element_values(i_prefix||'postal_code') := postal_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'country') := country;
  rib_obj_util.g_RIB_element_values(i_prefix||'telephone_area_code') := telephone_area_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'telephone_local_number') := telephone_local_number;
END appendNodeValues;
constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.last_name := last_name;
self.first_name := first_name;
self.middle_name := middle_name;
self.gender := gender;
self.birth_date := birth_date;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.postal_code := postal_code;
self.country := country;
RETURN;
end;
constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
, telephone_area_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.last_name := last_name;
self.first_name := first_name;
self.middle_name := middle_name;
self.gender := gender;
self.birth_date := birth_date;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.postal_code := postal_code;
self.country := country;
self.telephone_area_code := telephone_area_code;
RETURN;
end;
constructor function "RIB_MoreCustomerInfo_REC"
(
  rib_oid number
, last_name varchar2
, first_name varchar2
, middle_name varchar2
, gender varchar2
, birth_date varchar2
, address1 varchar2
, address2 varchar2
, city varchar2
, state varchar2
, postal_code varchar2
, country varchar2
, telephone_area_code varchar2
, telephone_local_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.last_name := last_name;
self.first_name := first_name;
self.middle_name := middle_name;
self.gender := gender;
self.birth_date := birth_date;
self.address1 := address1;
self.address2 := address2;
self.city := city;
self.state := state;
self.postal_code := postal_code;
self.country := country;
self.telephone_area_code := telephone_area_code;
self.telephone_local_number := telephone_local_number;
RETURN;
end;
END;
/
