DROP TYPE "RIB_StrItmDtlDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlDesc_REC";
/
DROP TYPE "RIB_StrItmDtlNslQty_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlNslQty_REC";
/
DROP TYPE "RIB_StrItmDtlUDA_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlUDA_REC";
/
DROP TYPE "RIB_StrItmDtlSup_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlSup_REC";
/
DROP TYPE "RIB_StrItmDtlImg_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlImg_REC";
/
DROP TYPE "RIB_StrItmDtlNslQty_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlNslQty_TBL" AS TABLE OF "RIB_StrItmDtlNslQty_REC";
/
DROP TYPE "RIB_StrItmDtlUDA_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlUDA_TBL" AS TABLE OF "RIB_StrItmDtlUDA_REC";
/
DROP TYPE "RIB_StrItmDtlSup_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlSup_TBL" AS TABLE OF "RIB_StrItmDtlSup_REC";
/
DROP TYPE "RIB_StrItmDtlImg_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrItmDtlImg_TBL" AS TABLE OF "RIB_StrItmDtlImg_REC";
/
DROP TYPE "RIB_StrItmDtlDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmDtlDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmDtlDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  store_id number(10),
  parent_id varchar2(25),
  item_type varchar2(30), -- item_type is enumeration field, valid values are [ITEM, CONSIGNMENT, CONCESSION, NON_INVENTORY, PACK_SIMPLE, PACK_COMPLEX, PACK_SIMPLE_BREAKABLE, PACK_COMPLEX_BREAKABLE, UNKNOWN] (all lower-case)
  sellable varchar2(5), --sellable is boolean field, valid values are true,false (all lower-case) 
  orderable varchar2(5), --orderable is boolean field, valid values are true,false (all lower-case) 
  short_description varchar2(120),
  long_description varchar2(250),
  department_id varchar2(12),
  department_name varchar2(120),
  class_id varchar2(12),
  class_name varchar2(120),
  subclass_id varchar2(12),
  subclass_name varchar2(120),
  unit_of_measure varchar2(40),
  status varchar2(20), -- status is enumeration field, valid values are [ACTIVE, INACTIVE, DISCONTINUED, DELETED, AUTO_STOCKABLE, NON_RANGED, UNKNOWN, NO_VALUE] (all lower-case)
  default_case_size number(12,4),
  uin_type varchar2(20), -- uin_type is enumeration field, valid values are [SERIAL, AGSN, UNKNOWN] (all lower-case)
  uin_label varchar2(128),
  unavailable_qty number(12,4),
  nonsellable_qty number(12,4),
  customer_reserved_qty number(12,4),
  in_transit_qty number(12,4),
  stock_in_delivery_bay number(12,4),
  stock_in_backroom number(12,4),
  stock_on_shopfloor number(12,4),
  stock_on_hand number(12,4),
  available_inventory number(12,4),
  transfer_reserved_qty number(12,4),
  vendor_return_qty number(12,4),
  ordered_qty number(12,4),
  received_today_qty number(12,4),
  last_received_date date,
  primary_supplier_id varchar2(128),
  primary_supplier_name varchar2(240),
  primary_sequence_id varchar2(12),
  primary_sequence_name varchar2(250),
  upc varchar2(128),
  vpn varchar2(128),
  suggested_ticket_type varchar2(6),
  store_order_allowed varchar2(5), --store_order_allowed is boolean field, valid values are true,false (all lower-case) 
  store_order_replenish_type varchar2(5), --store_order_replenish_type is boolean field, valid values are true,false (all lower-case) 
  multiple_delivery_allowed varchar2(5), --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case) 
  soh_inquiry_at_pack_level varchar2(5), --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case) 
  diff_type1 varchar2(10),
  diff_description1 varchar2(255),
  diff_type2 varchar2(10),
  diff_description2 varchar2(255),
  diff_type3 varchar2(10),
  diff_description3 varchar2(255),
  diff_type4 varchar2(10),
  diff_description4 varchar2(255),
  price_type varchar2(20), -- price_type is enumeration field, valid values are [CLEARANCE, PROMOTIONAL, PERMANENT, UNKNOWN, NO_VALUE] (all lower-case)
  promotion_type varchar2(10), -- promotion_type is enumeration field, valid values are [COMPLEX, SIMPLE, THRESHOLD, CREDIT, UNKNOWN, NO_VALUE] (all lower-case)
  retail_price number(12,4),
  retail_currency varchar2(3),
  retail_effective_date date,
  suggest_retail_cur varchar2(3),
  suggest_retail_value number(12,4),
  replenishment_method varchar2(6),
  multi_unit_quantity number(12,4),
  multi_unit_price number(12,4),
  multi_unit_currency varchar2(3),
  multi_unit_selling_uom varchar2(20),
  next_delivery_date date,
  StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL",   -- Size of "RIB_StrItmDtlNslQty_TBL" is unbounded
  StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL",   -- Size of "RIB_StrItmDtlUDA_TBL" is unbounded
  StrItmDtlSup_TBL "RIB_StrItmDtlSup_TBL",   -- Size of "RIB_StrItmDtlSup_TBL" is unbounded
  StrItmDtlImg_TBL "RIB_StrItmDtlImg_TBL",   -- Size of "RIB_StrItmDtlImg_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"  -- Size of "RIB_StrItmDtlNslQty_TBL" is unbounded
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"  -- Size of "RIB_StrItmDtlNslQty_TBL" is unbounded
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"  -- Size of "RIB_StrItmDtlUDA_TBL" is unbounded
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"  -- Size of "RIB_StrItmDtlNslQty_TBL" is unbounded
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"  -- Size of "RIB_StrItmDtlUDA_TBL" is unbounded
, StrItmDtlSup_TBL "RIB_StrItmDtlSup_TBL"  -- Size of "RIB_StrItmDtlSup_TBL" is unbounded
) return self as result
,constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2  --sellable is boolean field, valid values are true,false (all lower-case)
, orderable varchar2  --orderable is boolean field, valid values are true,false (all lower-case)
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2  --store_order_allowed is boolean field, valid values are true,false (all lower-case)
, store_order_replenish_type varchar2  --store_order_replenish_type is boolean field, valid values are true,false (all lower-case)
, multiple_delivery_allowed varchar2  --multiple_delivery_allowed is boolean field, valid values are true,false (all lower-case)
, soh_inquiry_at_pack_level varchar2  --soh_inquiry_at_pack_level is boolean field, valid values are true,false (all lower-case)
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"  -- Size of "RIB_StrItmDtlNslQty_TBL" is unbounded
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"  -- Size of "RIB_StrItmDtlUDA_TBL" is unbounded
, StrItmDtlSup_TBL "RIB_StrItmDtlSup_TBL"  -- Size of "RIB_StrItmDtlSup_TBL" is unbounded
, StrItmDtlImg_TBL "RIB_StrItmDtlImg_TBL"  -- Size of "RIB_StrItmDtlImg_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmDtlDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmDtlDesc') := "ns_name_StrItmDtlDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'parent_id') := parent_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_type') := item_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'sellable') := sellable;
  rib_obj_util.g_RIB_element_values(i_prefix||'orderable') := orderable;
  rib_obj_util.g_RIB_element_values(i_prefix||'short_description') := short_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'long_description') := long_description;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_id') := department_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'department_name') := department_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_id') := class_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'class_name') := class_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_id') := subclass_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'subclass_name') := subclass_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'default_case_size') := default_case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_type') := uin_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin_label') := uin_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'unavailable_qty') := unavailable_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsellable_qty') := nonsellable_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_reserved_qty') := customer_reserved_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'in_transit_qty') := in_transit_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_in_delivery_bay') := stock_in_delivery_bay;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_in_backroom') := stock_in_backroom;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_on_shopfloor') := stock_on_shopfloor;
  rib_obj_util.g_RIB_element_values(i_prefix||'stock_on_hand') := stock_on_hand;
  rib_obj_util.g_RIB_element_values(i_prefix||'available_inventory') := available_inventory;
  rib_obj_util.g_RIB_element_values(i_prefix||'transfer_reserved_qty') := transfer_reserved_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'vendor_return_qty') := vendor_return_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'ordered_qty') := ordered_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'received_today_qty') := received_today_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'last_received_date') := last_received_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supplier_id') := primary_supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_supplier_name') := primary_supplier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_sequence_id') := primary_sequence_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_sequence_name') := primary_sequence_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'upc') := upc;
  rib_obj_util.g_RIB_element_values(i_prefix||'vpn') := vpn;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggested_ticket_type') := suggested_ticket_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_order_allowed') := store_order_allowed;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_order_replenish_type') := store_order_replenish_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'multiple_delivery_allowed') := multiple_delivery_allowed;
  rib_obj_util.g_RIB_element_values(i_prefix||'soh_inquiry_at_pack_level') := soh_inquiry_at_pack_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type1') := diff_type1;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description1') := diff_description1;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type2') := diff_type2;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description2') := diff_description2;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type3') := diff_type3;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description3') := diff_description3;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_type4') := diff_type4;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_description4') := diff_description4;
  rib_obj_util.g_RIB_element_values(i_prefix||'price_type') := price_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_type') := promotion_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_price') := retail_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_currency') := retail_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'retail_effective_date') := retail_effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_cur') := suggest_retail_cur;
  rib_obj_util.g_RIB_element_values(i_prefix||'suggest_retail_value') := suggest_retail_value;
  rib_obj_util.g_RIB_element_values(i_prefix||'replenishment_method') := replenishment_method;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_quantity') := multi_unit_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_price') := multi_unit_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_currency') := multi_unit_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_selling_uom') := multi_unit_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'next_delivery_date') := next_delivery_date;
  IF StrItmDtlNslQty_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrItmDtlNslQty_TBL.';
    FOR INDX IN StrItmDtlNslQty_TBL.FIRST()..StrItmDtlNslQty_TBL.LAST() LOOP
      StrItmDtlNslQty_TBL(indx).appendNodeValues( i_prefix||indx||'StrItmDtlNslQty_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrItmDtlUDA_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrItmDtlUDA_TBL.';
    FOR INDX IN StrItmDtlUDA_TBL.FIRST()..StrItmDtlUDA_TBL.LAST() LOOP
      StrItmDtlUDA_TBL(indx).appendNodeValues( i_prefix||indx||'StrItmDtlUDA_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrItmDtlSup_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrItmDtlSup_TBL.';
    FOR INDX IN StrItmDtlSup_TBL.FIRST()..StrItmDtlSup_TBL.LAST() LOOP
      StrItmDtlSup_TBL(indx).appendNodeValues( i_prefix||indx||'StrItmDtlSup_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrItmDtlImg_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrItmDtlImg_TBL.';
    FOR INDX IN StrItmDtlImg_TBL.FIRST()..StrItmDtlImg_TBL.LAST() LOOP
      StrItmDtlImg_TBL(indx).appendNodeValues( i_prefix||indx||'StrItmDtlImg_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.next_delivery_date := next_delivery_date;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.next_delivery_date := next_delivery_date;
self.StrItmDtlNslQty_TBL := StrItmDtlNslQty_TBL;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.next_delivery_date := next_delivery_date;
self.StrItmDtlNslQty_TBL := StrItmDtlNslQty_TBL;
self.StrItmDtlUDA_TBL := StrItmDtlUDA_TBL;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"
, StrItmDtlSup_TBL "RIB_StrItmDtlSup_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.next_delivery_date := next_delivery_date;
self.StrItmDtlNslQty_TBL := StrItmDtlNslQty_TBL;
self.StrItmDtlUDA_TBL := StrItmDtlUDA_TBL;
self.StrItmDtlSup_TBL := StrItmDtlSup_TBL;
RETURN;
end;
constructor function "RIB_StrItmDtlDesc_REC"
(
  rib_oid number
, item_id varchar2
, store_id number
, parent_id varchar2
, item_type varchar2
, sellable varchar2
, orderable varchar2
, short_description varchar2
, long_description varchar2
, department_id varchar2
, department_name varchar2
, class_id varchar2
, class_name varchar2
, subclass_id varchar2
, subclass_name varchar2
, unit_of_measure varchar2
, status varchar2
, default_case_size number
, uin_type varchar2
, uin_label varchar2
, unavailable_qty number
, nonsellable_qty number
, customer_reserved_qty number
, in_transit_qty number
, stock_in_delivery_bay number
, stock_in_backroom number
, stock_on_shopfloor number
, stock_on_hand number
, available_inventory number
, transfer_reserved_qty number
, vendor_return_qty number
, ordered_qty number
, received_today_qty number
, last_received_date date
, primary_supplier_id varchar2
, primary_supplier_name varchar2
, primary_sequence_id varchar2
, primary_sequence_name varchar2
, upc varchar2
, vpn varchar2
, suggested_ticket_type varchar2
, store_order_allowed varchar2
, store_order_replenish_type varchar2
, multiple_delivery_allowed varchar2
, soh_inquiry_at_pack_level varchar2
, diff_type1 varchar2
, diff_description1 varchar2
, diff_type2 varchar2
, diff_description2 varchar2
, diff_type3 varchar2
, diff_description3 varchar2
, diff_type4 varchar2
, diff_description4 varchar2
, price_type varchar2
, promotion_type varchar2
, retail_price number
, retail_currency varchar2
, retail_effective_date date
, suggest_retail_cur varchar2
, suggest_retail_value number
, replenishment_method varchar2
, multi_unit_quantity number
, multi_unit_price number
, multi_unit_currency varchar2
, multi_unit_selling_uom varchar2
, next_delivery_date date
, StrItmDtlNslQty_TBL "RIB_StrItmDtlNslQty_TBL"
, StrItmDtlUDA_TBL "RIB_StrItmDtlUDA_TBL"
, StrItmDtlSup_TBL "RIB_StrItmDtlSup_TBL"
, StrItmDtlImg_TBL "RIB_StrItmDtlImg_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.store_id := store_id;
self.parent_id := parent_id;
self.item_type := item_type;
self.sellable := sellable;
self.orderable := orderable;
self.short_description := short_description;
self.long_description := long_description;
self.department_id := department_id;
self.department_name := department_name;
self.class_id := class_id;
self.class_name := class_name;
self.subclass_id := subclass_id;
self.subclass_name := subclass_name;
self.unit_of_measure := unit_of_measure;
self.status := status;
self.default_case_size := default_case_size;
self.uin_type := uin_type;
self.uin_label := uin_label;
self.unavailable_qty := unavailable_qty;
self.nonsellable_qty := nonsellable_qty;
self.customer_reserved_qty := customer_reserved_qty;
self.in_transit_qty := in_transit_qty;
self.stock_in_delivery_bay := stock_in_delivery_bay;
self.stock_in_backroom := stock_in_backroom;
self.stock_on_shopfloor := stock_on_shopfloor;
self.stock_on_hand := stock_on_hand;
self.available_inventory := available_inventory;
self.transfer_reserved_qty := transfer_reserved_qty;
self.vendor_return_qty := vendor_return_qty;
self.ordered_qty := ordered_qty;
self.received_today_qty := received_today_qty;
self.last_received_date := last_received_date;
self.primary_supplier_id := primary_supplier_id;
self.primary_supplier_name := primary_supplier_name;
self.primary_sequence_id := primary_sequence_id;
self.primary_sequence_name := primary_sequence_name;
self.upc := upc;
self.vpn := vpn;
self.suggested_ticket_type := suggested_ticket_type;
self.store_order_allowed := store_order_allowed;
self.store_order_replenish_type := store_order_replenish_type;
self.multiple_delivery_allowed := multiple_delivery_allowed;
self.soh_inquiry_at_pack_level := soh_inquiry_at_pack_level;
self.diff_type1 := diff_type1;
self.diff_description1 := diff_description1;
self.diff_type2 := diff_type2;
self.diff_description2 := diff_description2;
self.diff_type3 := diff_type3;
self.diff_description3 := diff_description3;
self.diff_type4 := diff_type4;
self.diff_description4 := diff_description4;
self.price_type := price_type;
self.promotion_type := promotion_type;
self.retail_price := retail_price;
self.retail_currency := retail_currency;
self.retail_effective_date := retail_effective_date;
self.suggest_retail_cur := suggest_retail_cur;
self.suggest_retail_value := suggest_retail_value;
self.replenishment_method := replenishment_method;
self.multi_unit_quantity := multi_unit_quantity;
self.multi_unit_price := multi_unit_price;
self.multi_unit_currency := multi_unit_currency;
self.multi_unit_selling_uom := multi_unit_selling_uom;
self.next_delivery_date := next_delivery_date;
self.StrItmDtlNslQty_TBL := StrItmDtlNslQty_TBL;
self.StrItmDtlUDA_TBL := StrItmDtlUDA_TBL;
self.StrItmDtlSup_TBL := StrItmDtlSup_TBL;
self.StrItmDtlImg_TBL := StrItmDtlImg_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrItmDtlNslQty_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmDtlNslQty_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmDtlDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  nonsell_qty_type_id number(12),
  quantity number(20,4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmDtlNslQty_REC"
(
  rib_oid number
, nonsell_qty_type_id number
, quantity number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmDtlNslQty_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmDtlDesc') := "ns_name_StrItmDtlDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'nonsell_qty_type_id') := nonsell_qty_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
END appendNodeValues;
constructor function "RIB_StrItmDtlNslQty_REC"
(
  rib_oid number
, nonsell_qty_type_id number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.nonsell_qty_type_id := nonsell_qty_type_id;
self.quantity := quantity;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrItmDtlUDA_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmDtlUDA_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmDtlDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uda_id number(5),
  uda_value varchar2(255),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmDtlUDA_REC"
(
  rib_oid number
, uda_id number
, uda_value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmDtlUDA_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmDtlDesc') := "ns_name_StrItmDtlDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_id') := uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value') := uda_value;
END appendNodeValues;
constructor function "RIB_StrItmDtlUDA_REC"
(
  rib_oid number
, uda_id number
, uda_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.uda_value := uda_value;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrItmDtlSup_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmDtlSup_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmDtlDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier_id varchar2(128),
  supplier_name varchar2(240),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmDtlSup_REC"
(
  rib_oid number
, supplier_id varchar2
, supplier_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmDtlSup_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmDtlDesc') := "ns_name_StrItmDtlDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_id') := supplier_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier_name') := supplier_name;
END appendNodeValues;
constructor function "RIB_StrItmDtlSup_REC"
(
  rib_oid number
, supplier_id varchar2
, supplier_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier_id := supplier_id;
self.supplier_name := supplier_name;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrItmDtlImg_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrItmDtlImg_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrItmDtlDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  url varchar2(1000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrItmDtlImg_REC"
(
  rib_oid number
, url varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrItmDtlImg_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrItmDtlDesc') := "ns_name_StrItmDtlDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'url') := url;
END appendNodeValues;
constructor function "RIB_StrItmDtlImg_REC"
(
  rib_oid number
, url varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.url := url;
RETURN;
end;
END;
/
