DROP TYPE "RIB_ForpHdrDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ForpHdrDesc_REC";
/
DROP TYPE "RIB_ForpHdrDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ForpHdrDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ForpHdrDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reverse_pick_id number(12),
  status varchar2(20), -- status is enumeration field, valid values are [NEW, IN_PROGRESS, COMPLETED, CANCELED, UNKNOWN] (all lower-case)
  create_user_name varchar2(128),
  create_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ForpHdrDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_user_name varchar2
, create_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ForpHdrDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ForpHdrDesc') := "ns_name_ForpHdrDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'reverse_pick_id') := reverse_pick_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'status') := status;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_user_name') := create_user_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
END appendNodeValues;
constructor function "RIB_ForpHdrDesc_REC"
(
  rib_oid number
, reverse_pick_id number
, status varchar2
, create_user_name varchar2
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.reverse_pick_id := reverse_pick_id;
self.status := status;
self.create_user_name := create_user_name;
self.create_date := create_date;
RETURN;
end;
END;
/
