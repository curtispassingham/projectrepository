@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_VdrDelvCtnModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnModVo_REC";
/
DROP TYPE "RIB_VdrDelvCtnItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmMod_REC";
/
DROP TYPE "RIB_VdrDvCtnItmUinMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDvCtnItmUinMod_REC";
/
DROP TYPE "RIB_VdrDelvCtnModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnModNote_REC";
/
DROP TYPE "RIB_VdrDelvCtnItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmMod_TBL" AS TABLE OF "RIB_VdrDelvCtnItmMod_REC";
/
DROP TYPE "RIB_VdrDelvCtnModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnModNote_TBL" AS TABLE OF "RIB_VdrDelvCtnModNote_REC";
/
DROP TYPE "RIB_VdrDelvCtnModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  carton_id number(12),
  reference_id varchar2(128),
  damaged_reason varchar2(128),
  serial_code number(18),
  tracking_number varchar2(128),
  damage_remaining varchar2(5), --damage_remaining is boolean field, valid values are true,false (all lower-case) 
  receive_shopfloor varchar2(5), --receive_shopfloor is boolean field, valid values are true,false (all lower-case) 
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 1000
  VdrDelvCtnItmMod_TBL "RIB_VdrDelvCtnItmMod_TBL",   -- Size of "RIB_VdrDelvCtnItmMod_TBL" is 1000
  VdrDelvCtnModNote_TBL "RIB_VdrDelvCtnModNote_TBL",   -- Size of "RIB_VdrDelvCtnModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
, VdrDelvCtnItmMod_TBL "RIB_VdrDelvCtnItmMod_TBL"  -- Size of "RIB_VdrDelvCtnItmMod_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2  --damage_remaining is boolean field, valid values are true,false (all lower-case)
, receive_shopfloor varchar2  --receive_shopfloor is boolean field, valid values are true,false (all lower-case)
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 1000
, VdrDelvCtnItmMod_TBL "RIB_VdrDelvCtnItmMod_TBL"  -- Size of "RIB_VdrDelvCtnItmMod_TBL" is 1000
, VdrDelvCtnModNote_TBL "RIB_VdrDelvCtnModNote_TBL"  -- Size of "RIB_VdrDelvCtnModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnModVo') := "ns_name_VdrDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_id') := carton_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reference_id') := reference_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged_reason') := damaged_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'serial_code') := serial_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'damage_remaining') := damage_remaining;
  rib_obj_util.g_RIB_element_values(i_prefix||'receive_shopfloor') := receive_shopfloor;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF VdrDelvCtnItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvCtnItmMod_TBL.';
    FOR INDX IN VdrDelvCtnItmMod_TBL.FIRST()..VdrDelvCtnItmMod_TBL.LAST() LOOP
      VdrDelvCtnItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvCtnItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF VdrDelvCtnModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDelvCtnModNote_TBL.';
    FOR INDX IN VdrDelvCtnModNote_TBL.FIRST()..VdrDelvCtnModNote_TBL.LAST() LOOP
      VdrDelvCtnModNote_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDelvCtnModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
RETURN;
end;
constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, VdrDelvCtnItmMod_TBL "RIB_VdrDelvCtnItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.removed_line_id_col := removed_line_id_col;
self.VdrDelvCtnItmMod_TBL := VdrDelvCtnItmMod_TBL;
RETURN;
end;
constructor function "RIB_VdrDelvCtnModVo_REC"
(
  rib_oid number
, carton_id number
, reference_id varchar2
, damaged_reason varchar2
, serial_code number
, tracking_number varchar2
, damage_remaining varchar2
, receive_shopfloor varchar2
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, VdrDelvCtnItmMod_TBL "RIB_VdrDelvCtnItmMod_TBL"
, VdrDelvCtnModNote_TBL "RIB_VdrDelvCtnModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.carton_id := carton_id;
self.reference_id := reference_id;
self.damaged_reason := damaged_reason;
self.serial_code := serial_code;
self.tracking_number := tracking_number;
self.damage_remaining := damage_remaining;
self.receive_shopfloor := receive_shopfloor;
self.removed_line_id_col := removed_line_id_col;
self.VdrDelvCtnItmMod_TBL := VdrDelvCtnItmMod_TBL;
self.VdrDelvCtnModNote_TBL := VdrDelvCtnModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_VdrDvCtnItmUinMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VdrDvCtnItmUinMod_TBL" AS TABLE OF "RIB_VdrDvCtnItmUinMod_REC";
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_VdrDelvCtnItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  store_id number(10),
  item_id varchar2(25),
  case_size number(10,2),
  unit_cost_override number(12,4),
  quantity_received number(20,4),
  quantity_damaged number(20,4),
  removed_uin_col "RIB_removed_uin_col_TBL",   -- Size of "RIB_removed_uin_col_TBL" is 1000
  VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL",   -- Size of "RIB_VdrDvCtnItmUinMod_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"  -- Size of "RIB_VdrDvCtnItmUinMod_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"  -- Size of "RIB_VdrDvCtnItmUinMod_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
,constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"  -- Size of "RIB_VdrDvCtnItmUinMod_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnModVo') := "ns_name_VdrDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost_override') := unit_cost_override;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_received') := quantity_received;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity_damaged') := quantity_damaged;
  IF removed_uin_col IS NOT NULL THEN
    FOR INDX IN removed_uin_col.FIRST()..removed_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_uin_col'||'.'):=removed_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF VdrDvCtnItmUinMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VdrDvCtnItmUinMod_TBL.';
    FOR INDX IN VdrDvCtnItmUinMod_TBL.FIRST()..VdrDvCtnItmUinMod_TBL.LAST() LOOP
      VdrDvCtnItmUinMod_TBL(indx).appendNodeValues( i_prefix||indx||'VdrDvCtnItmUinMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.removed_uin_col := removed_uin_col;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.removed_uin_col := removed_uin_col;
self.VdrDvCtnItmUinMod_TBL := VdrDvCtnItmUinMod_TBL;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.removed_uin_col := removed_uin_col;
self.VdrDvCtnItmUinMod_TBL := VdrDvCtnItmUinMod_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_VdrDelvCtnItmMod_REC"
(
  rib_oid number
, line_id number
, store_id number
, item_id varchar2
, case_size number
, unit_cost_override number
, quantity_received number
, quantity_damaged number
, removed_uin_col "RIB_removed_uin_col_TBL"
, VdrDvCtnItmUinMod_TBL "RIB_VdrDvCtnItmUinMod_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.store_id := store_id;
self.item_id := item_id;
self.case_size := case_size;
self.unit_cost_override := unit_cost_override;
self.quantity_received := quantity_received;
self.quantity_damaged := quantity_damaged;
self.removed_uin_col := removed_uin_col;
self.VdrDvCtnItmUinMod_TBL := VdrDvCtnItmUinMod_TBL;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_VdrDvCtnItmUinMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDvCtnItmUinMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uin varchar2(128),
  received varchar2(5), --received is boolean field, valid values are true,false (all lower-case) 
  damaged varchar2(5), --damaged is boolean field, valid values are true,false (all lower-case) 
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDvCtnItmUinMod_REC"
(
  rib_oid number
, uin varchar2
, received varchar2  --received is boolean field, valid values are true,false (all lower-case)
, damaged varchar2  --damaged is boolean field, valid values are true,false (all lower-case)
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDvCtnItmUinMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnModVo') := "ns_name_VdrDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'received') := received;
  rib_obj_util.g_RIB_element_values(i_prefix||'damaged') := damaged;
END appendNodeValues;
constructor function "RIB_VdrDvCtnItmUinMod_REC"
(
  rib_oid number
, uin varchar2
, received varchar2
, damaged varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uin := uin;
self.received := received;
self.damaged := damaged;
RETURN;
end;
END;
/
DROP TYPE "RIB_VdrDelvCtnModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VdrDelvCtnModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VdrDelvCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VdrDelvCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VdrDelvCtnModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VdrDelvCtnModVo') := "ns_name_VdrDelvCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_VdrDelvCtnModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
