DROP TYPE "RIB_ItmTktBatchDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmTktBatchDesc_REC";
/
DROP TYPE "RIB_ItmBatchTkt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBatchTkt_REC";
/
DROP TYPE "RIB_ItmBatchTktUda_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBatchTktUda_REC";
/
DROP TYPE "RIB_ItmBatchTkt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBatchTkt_TBL" AS TABLE OF "RIB_ItmBatchTkt_REC";
/
DROP TYPE "RIB_ItmTktBatchDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmTktBatchDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktBatchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  external_id varchar2(128),
  transaction_id varchar2(128),
  printer varchar2(128),
  printer_source varchar2(20), -- printer_source is enumeration field, valid values are [INTERNAL, EXTERNAL] (all lower-case)
  print_date date,
  ticket_type varchar2(20), -- ticket_type is enumeration field, valid values are [SHELF_LABEL, ITEM_TICKET, AGSN, UNKNOWN] (all lower-case)
  ticket_reason varchar2(20), -- ticket_reason is enumeration field, valid values are [TICKET_GENERAL, RECEIVED_DELIVERIES, EXPECTED_DELIVERIES, PRICE_CLEARANCE, PRICE_CHANGE, PRICE_PROMOTION, PURCHASE_ORDER, UNKNOWN] (all lower-case)
  auto_refresh_qty varchar2(5), --auto_refresh_qty is boolean field, valid values are true,false (all lower-case) 
  ItmBatchTkt_TBL "RIB_ItmBatchTkt_TBL",   -- Size of "RIB_ItmBatchTkt_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmTktBatchDesc_REC"
(
  rib_oid number
, store_id number
, external_id varchar2
, transaction_id varchar2
, printer varchar2
, printer_source varchar2
, print_date date
, ticket_type varchar2
, ticket_reason varchar2
, auto_refresh_qty varchar2  --auto_refresh_qty is boolean field, valid values are true,false (all lower-case)
, ItmBatchTkt_TBL "RIB_ItmBatchTkt_TBL"  -- Size of "RIB_ItmBatchTkt_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmTktBatchDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktBatchDesc') := "ns_name_ItmTktBatchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer') := printer;
  rib_obj_util.g_RIB_element_values(i_prefix||'printer_source') := printer_source;
  rib_obj_util.g_RIB_element_values(i_prefix||'print_date') := print_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_type') := ticket_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'ticket_reason') := ticket_reason;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_refresh_qty') := auto_refresh_qty;
  l_new_pre :=i_prefix||'ItmBatchTkt_TBL.';
  FOR INDX IN ItmBatchTkt_TBL.FIRST()..ItmBatchTkt_TBL.LAST() LOOP
    ItmBatchTkt_TBL(indx).appendNodeValues( i_prefix||indx||'ItmBatchTkt_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_ItmTktBatchDesc_REC"
(
  rib_oid number
, store_id number
, external_id varchar2
, transaction_id varchar2
, printer varchar2
, printer_source varchar2
, print_date date
, ticket_type varchar2
, ticket_reason varchar2
, auto_refresh_qty varchar2
, ItmBatchTkt_TBL "RIB_ItmBatchTkt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.external_id := external_id;
self.transaction_id := transaction_id;
self.printer := printer;
self.printer_source := printer_source;
self.print_date := print_date;
self.ticket_type := ticket_type;
self.ticket_reason := ticket_reason;
self.auto_refresh_qty := auto_refresh_qty;
self.ItmBatchTkt_TBL := ItmBatchTkt_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_ItmBatchTktUda_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItmBatchTktUda_TBL" AS TABLE OF "RIB_ItmBatchTktUda_REC";
/
DROP TYPE "RIB_ItmBatchTkt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmBatchTkt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktBatchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  quantity number(10),
  format_id number(10),
  label_price number(12,4),
  label_currency varchar2(3),
  external_po_id varchar2(128),
  promotion_id number(10),
  effective_date date,
  multi_units number(12,4),
  multi_label_price number(12,4),
  multi_label_currency varchar2(3),
  multi_unit_change varchar2(5), --multi_unit_change is boolean field, valid values are true,false (all lower-case) 
  country_manufacture varchar2(3),
  print_order number(10),
  uin_col "RIB_uin_col_TBL",   -- Size of "RIB_uin_col_TBL" is 999
  ItmBatchTktUda_TBL "RIB_ItmBatchTktUda_TBL",   -- Size of "RIB_ItmBatchTktUda_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, country_manufacture varchar2
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, country_manufacture varchar2
, print_order number
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, country_manufacture varchar2
, print_order number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 999
) return self as result
,constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2  --multi_unit_change is boolean field, valid values are true,false (all lower-case)
, country_manufacture varchar2
, print_order number
, uin_col "RIB_uin_col_TBL"  -- Size of "RIB_uin_col_TBL" is 999
, ItmBatchTktUda_TBL "RIB_ItmBatchTktUda_TBL"  -- Size of "RIB_ItmBatchTktUda_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmBatchTkt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktBatchDesc') := "ns_name_ItmTktBatchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'format_id') := format_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_price') := label_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'label_currency') := label_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_po_id') := external_po_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promotion_id') := promotion_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_label_price') := multi_label_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_label_currency') := multi_label_currency;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_change') := multi_unit_change;
  rib_obj_util.g_RIB_element_values(i_prefix||'country_manufacture') := country_manufacture;
  rib_obj_util.g_RIB_element_values(i_prefix||'print_order') := print_order;
  IF uin_col IS NOT NULL THEN
    FOR INDX IN uin_col.FIRST()..uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'uin_col'||'.'):=uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItmBatchTktUda_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItmBatchTktUda_TBL.';
    FOR INDX IN ItmBatchTktUda_TBL.FIRST()..ItmBatchTktUda_TBL.LAST() LOOP
      ItmBatchTktUda_TBL(indx).appendNodeValues( i_prefix||indx||'ItmBatchTktUda_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, country_manufacture varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.country_manufacture := country_manufacture;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, country_manufacture varchar2
, print_order number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.country_manufacture := country_manufacture;
self.print_order := print_order;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, country_manufacture varchar2
, print_order number
, uin_col "RIB_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.country_manufacture := country_manufacture;
self.print_order := print_order;
self.uin_col := uin_col;
RETURN;
end;
constructor function "RIB_ItmBatchTkt_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, format_id number
, label_price number
, label_currency varchar2
, external_po_id varchar2
, promotion_id number
, effective_date date
, multi_units number
, multi_label_price number
, multi_label_currency varchar2
, multi_unit_change varchar2
, country_manufacture varchar2
, print_order number
, uin_col "RIB_uin_col_TBL"
, ItmBatchTktUda_TBL "RIB_ItmBatchTktUda_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.format_id := format_id;
self.label_price := label_price;
self.label_currency := label_currency;
self.external_po_id := external_po_id;
self.promotion_id := promotion_id;
self.effective_date := effective_date;
self.multi_units := multi_units;
self.multi_label_price := multi_label_price;
self.multi_label_currency := multi_label_currency;
self.multi_unit_change := multi_unit_change;
self.country_manufacture := country_manufacture;
self.print_order := print_order;
self.uin_col := uin_col;
self.ItmBatchTktUda_TBL := ItmBatchTktUda_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_ItmBatchTktUda_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItmBatchTktUda_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItmTktBatchDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  label varchar2(128),
  value varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItmBatchTktUda_REC"
(
  rib_oid number
, label varchar2
, value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItmBatchTktUda_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItmTktBatchDesc') := "ns_name_ItmTktBatchDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'label') := label;
  rib_obj_util.g_RIB_element_values(i_prefix||'value') := value;
END appendNodeValues;
constructor function "RIB_ItmBatchTktUda_REC"
(
  rib_oid number
, label varchar2
, value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.label := label;
self.value := value;
RETURN;
end;
END;
/
