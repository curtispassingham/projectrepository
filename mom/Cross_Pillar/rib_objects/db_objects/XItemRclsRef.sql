@@XItemRclsDtl.sql;
/
DROP TYPE "RIB_XItemRclsRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemRclsRef_REC";
/
DROP TYPE "RIB_XItemRclsDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemRclsDtl_TBL" AS TABLE OF "RIB_XItemRclsDtl_REC";
/
DROP TYPE "RIB_XItemRclsRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemRclsRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRclsRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  reclass_no number(4),
  reclass_date date,
  purge_all_ind varchar2(1),
  XItemRclsDtl_TBL "RIB_XItemRclsDtl_TBL",   -- Size of "RIB_XItemRclsDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
) return self as result
,constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
) return self as result
,constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
, purge_all_ind varchar2
) return self as result
,constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
, purge_all_ind varchar2
, XItemRclsDtl_TBL "RIB_XItemRclsDtl_TBL"  -- Size of "RIB_XItemRclsDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemRclsRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRclsRef') := "ns_name_XItemRclsRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'reclass_no') := reclass_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'reclass_date') := reclass_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'purge_all_ind') := purge_all_ind;
  IF XItemRclsDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemRclsDtl_TBL.';
    FOR INDX IN XItemRclsDtl_TBL.FIRST()..XItemRclsDtl_TBL.LAST() LOOP
      XItemRclsDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XItemRclsDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.reclass_no := reclass_no;
RETURN;
end;
constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.reclass_no := reclass_no;
self.reclass_date := reclass_date;
RETURN;
end;
constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
, purge_all_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.reclass_no := reclass_no;
self.reclass_date := reclass_date;
self.purge_all_ind := purge_all_ind;
RETURN;
end;
constructor function "RIB_XItemRclsRef_REC"
(
  rib_oid number
, reclass_no number
, reclass_date date
, purge_all_ind varchar2
, XItemRclsDtl_TBL "RIB_XItemRclsDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.reclass_no := reclass_no;
self.reclass_date := reclass_date;
self.purge_all_ind := purge_all_ind;
self.XItemRclsDtl_TBL := XItemRclsDtl_TBL;
RETURN;
end;
END;
/
