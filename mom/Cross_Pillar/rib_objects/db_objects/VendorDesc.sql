@@VendorHdrDesc.sql;
/
@@VendorAddrDesc.sql;
/
@@VendorOUDesc.sql;
/
DROP TYPE "RIB_VendorDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorDesc_REC";
/
DROP TYPE "RIB_VendorAddrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorAddrDesc_TBL" AS TABLE OF "RIB_VendorAddrDesc_REC";
/
DROP TYPE "RIB_VendorOUDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_VendorOUDesc_TBL" AS TABLE OF "RIB_VendorOUDesc_REC";
/
DROP TYPE "RIB_VendorDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_VendorDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_VendorDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  VendorHdrDesc "RIB_VendorHdrDesc_REC",
  VendorAddrDesc_TBL "RIB_VendorAddrDesc_TBL",   -- Size of "RIB_VendorAddrDesc_TBL" is unbounded
  VendorOUDesc_TBL "RIB_VendorOUDesc_TBL",   -- Size of "RIB_VendorOUDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
) return self as result
,constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
, VendorAddrDesc_TBL "RIB_VendorAddrDesc_TBL"  -- Size of "RIB_VendorAddrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
, VendorAddrDesc_TBL "RIB_VendorAddrDesc_TBL"  -- Size of "RIB_VendorAddrDesc_TBL" is unbounded
, VendorOUDesc_TBL "RIB_VendorOUDesc_TBL"  -- Size of "RIB_VendorOUDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_VendorDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_VendorDesc') := "ns_name_VendorDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'VendorHdrDesc.';
  VendorHdrDesc.appendNodeValues( i_prefix||'VendorHdrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF VendorAddrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VendorAddrDesc_TBL.';
    FOR INDX IN VendorAddrDesc_TBL.FIRST()..VendorAddrDesc_TBL.LAST() LOOP
      VendorAddrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'VendorAddrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF VendorOUDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'VendorOUDesc_TBL.';
    FOR INDX IN VendorOUDesc_TBL.FIRST()..VendorOUDesc_TBL.LAST() LOOP
      VendorOUDesc_TBL(indx).appendNodeValues( i_prefix||indx||'VendorOUDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.VendorHdrDesc := VendorHdrDesc;
RETURN;
end;
constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
, VendorAddrDesc_TBL "RIB_VendorAddrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.VendorHdrDesc := VendorHdrDesc;
self.VendorAddrDesc_TBL := VendorAddrDesc_TBL;
RETURN;
end;
constructor function "RIB_VendorDesc_REC"
(
  rib_oid number
, VendorHdrDesc "RIB_VendorHdrDesc_REC"
, VendorAddrDesc_TBL "RIB_VendorAddrDesc_TBL"
, VendorOUDesc_TBL "RIB_VendorOUDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.VendorHdrDesc := VendorHdrDesc;
self.VendorAddrDesc_TBL := VendorAddrDesc_TBL;
self.VendorOUDesc_TBL := VendorOUDesc_TBL;
RETURN;
end;
END;
/
