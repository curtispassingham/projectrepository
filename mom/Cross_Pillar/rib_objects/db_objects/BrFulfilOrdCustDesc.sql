@@EOfBrFulfilOrdCustDesc.sql;
/
@@BrFulfilOrdPmtDesc.sql;
/
DROP TYPE "RIB_BrFulfilOrdCustDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_BrFulfilOrdCustDesc_REC";
/
DROP TYPE "RIB_BrFulfilOrdPmtDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_BrFulfilOrdPmtDesc_TBL" AS TABLE OF "RIB_BrFulfilOrdPmtDesc_REC";
/
DROP TYPE "RIB_BrFulfilOrdCustDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_BrFulfilOrdCustDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_BrFulfilOrdCustDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_localization" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  cpf varchar2(120),
  neighborhood varchar2(120),
  ie_exempt varchar2(1),
  presence_ind number(4),
  taxpayer_type varchar2(1),
  fiscal_address_information varchar2(20),
  individual_taxpayer_id varchar2(20),
  corporate_taxpayer_id varchar2(20),
  free_zone_of_manaus_inscptn_id varchar2(20),
  city_inscription_id varchar2(20),
  state_inscription_id varchar2(20),
  contributor_type varchar2(2),
  tax_exception_type varchar2(2),
  iss_contributor varchar2(1),
  ipi_contributor varchar2(1),
  icms_contributor varchar2(1),
  pis_contributor varchar2(1),
  cofins_contributor varchar2(1),
  is_income_range_eligible varchar2(1),
  is_distributor_manufacturer varchar2(1),
  customers_cnae varchar2(20),
  BrFulfilOrdPmtDesc_TBL "RIB_BrFulfilOrdPmtDesc_TBL",   -- Size of "RIB_BrFulfilOrdPmtDesc_TBL" is unbounded
  EOfBrFulfilOrdCustDesc "RIB_EOfBrFulfilOrdCustDesc_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
, BrFulfilOrdPmtDesc_TBL "RIB_BrFulfilOrdPmtDesc_TBL"  -- Size of "RIB_BrFulfilOrdPmtDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
, BrFulfilOrdPmtDesc_TBL "RIB_BrFulfilOrdPmtDesc_TBL"  -- Size of "RIB_BrFulfilOrdPmtDesc_TBL" is unbounded
, EOfBrFulfilOrdCustDesc "RIB_EOfBrFulfilOrdCustDesc_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_BrFulfilOrdCustDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_BrFulfilOrdCustDesc') := "ns_name_BrFulfilOrdCustDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_localization') := "ns_location_localization";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'cpf') := cpf;
  rib_obj_util.g_RIB_element_values(i_prefix||'neighborhood') := neighborhood;
  rib_obj_util.g_RIB_element_values(i_prefix||'ie_exempt') := ie_exempt;
  rib_obj_util.g_RIB_element_values(i_prefix||'presence_ind') := presence_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxpayer_type') := taxpayer_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'fiscal_address_information') := fiscal_address_information;
  rib_obj_util.g_RIB_element_values(i_prefix||'individual_taxpayer_id') := individual_taxpayer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'corporate_taxpayer_id') := corporate_taxpayer_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'free_zone_of_manaus_inscptn_id') := free_zone_of_manaus_inscptn_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'city_inscription_id') := city_inscription_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'state_inscription_id') := state_inscription_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'contributor_type') := contributor_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_exception_type') := tax_exception_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'iss_contributor') := iss_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'ipi_contributor') := ipi_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'icms_contributor') := icms_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'pis_contributor') := pis_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'cofins_contributor') := cofins_contributor;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_income_range_eligible') := is_income_range_eligible;
  rib_obj_util.g_RIB_element_values(i_prefix||'is_distributor_manufacturer') := is_distributor_manufacturer;
  rib_obj_util.g_RIB_element_values(i_prefix||'customers_cnae') := customers_cnae;
  IF BrFulfilOrdPmtDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'BrFulfilOrdPmtDesc_TBL.';
    FOR INDX IN BrFulfilOrdPmtDesc_TBL.FIRST()..BrFulfilOrdPmtDesc_TBL.LAST() LOOP
      BrFulfilOrdPmtDesc_TBL(indx).appendNodeValues( i_prefix||indx||'BrFulfilOrdPmtDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'EOfBrFulfilOrdCustDesc.';
  EOfBrFulfilOrdCustDesc.appendNodeValues( i_prefix||'EOfBrFulfilOrdCustDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
self.is_income_range_eligible := is_income_range_eligible;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distributor_manufacturer := is_distributor_manufacturer;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distributor_manufacturer := is_distributor_manufacturer;
self.customers_cnae := customers_cnae;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
, BrFulfilOrdPmtDesc_TBL "RIB_BrFulfilOrdPmtDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distributor_manufacturer := is_distributor_manufacturer;
self.customers_cnae := customers_cnae;
self.BrFulfilOrdPmtDesc_TBL := BrFulfilOrdPmtDesc_TBL;
RETURN;
end;
constructor function "RIB_BrFulfilOrdCustDesc_REC"
(
  rib_oid number
, cpf varchar2
, neighborhood varchar2
, ie_exempt varchar2
, presence_ind number
, taxpayer_type varchar2
, fiscal_address_information varchar2
, individual_taxpayer_id varchar2
, corporate_taxpayer_id varchar2
, free_zone_of_manaus_inscptn_id varchar2
, city_inscription_id varchar2
, state_inscription_id varchar2
, contributor_type varchar2
, tax_exception_type varchar2
, iss_contributor varchar2
, ipi_contributor varchar2
, icms_contributor varchar2
, pis_contributor varchar2
, cofins_contributor varchar2
, is_income_range_eligible varchar2
, is_distributor_manufacturer varchar2
, customers_cnae varchar2
, BrFulfilOrdPmtDesc_TBL "RIB_BrFulfilOrdPmtDesc_TBL"
, EOfBrFulfilOrdCustDesc "RIB_EOfBrFulfilOrdCustDesc_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.cpf := cpf;
self.neighborhood := neighborhood;
self.ie_exempt := ie_exempt;
self.presence_ind := presence_ind;
self.taxpayer_type := taxpayer_type;
self.fiscal_address_information := fiscal_address_information;
self.individual_taxpayer_id := individual_taxpayer_id;
self.corporate_taxpayer_id := corporate_taxpayer_id;
self.free_zone_of_manaus_inscptn_id := free_zone_of_manaus_inscptn_id;
self.city_inscription_id := city_inscription_id;
self.state_inscription_id := state_inscription_id;
self.contributor_type := contributor_type;
self.tax_exception_type := tax_exception_type;
self.iss_contributor := iss_contributor;
self.ipi_contributor := ipi_contributor;
self.icms_contributor := icms_contributor;
self.pis_contributor := pis_contributor;
self.cofins_contributor := cofins_contributor;
self.is_income_range_eligible := is_income_range_eligible;
self.is_distributor_manufacturer := is_distributor_manufacturer;
self.customers_cnae := customers_cnae;
self.BrFulfilOrdPmtDesc_TBL := BrFulfilOrdPmtDesc_TBL;
self.EOfBrFulfilOrdCustDesc := EOfBrFulfilOrdCustDesc;
RETURN;
end;
END;
/
