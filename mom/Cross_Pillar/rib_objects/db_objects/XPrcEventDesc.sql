DROP TYPE "RIB_XPrcEventDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XPrcEventDtl_REC";
/
DROP TYPE "RIB_XPrcEventDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XPrcEventDesc_REC";
/
DROP TYPE "RIB_XPrcEventDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XPrcEventDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XPrcEventDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_value number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XPrcEventDtl_REC"
(
  rib_oid number
, hier_value number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XPrcEventDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XPrcEventDesc') := "ns_name_XPrcEventDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_value') := hier_value;
END appendNodeValues;
constructor function "RIB_XPrcEventDtl_REC"
(
  rib_oid number
, hier_value number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_value := hier_value;
RETURN;
end;
END;
/
DROP TYPE "RIB_XPrcEventDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XPrcEventDtl_TBL" AS TABLE OF "RIB_XPrcEventDtl_REC";
/
DROP TYPE "RIB_XPrcEventDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XPrcEventDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XPrcEventDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  event_type varchar2(6),
  event_id number(15),
  item varchar2(25),
  item_level number(1),
  diff_id varchar2(10),
  hier_level varchar2(4),
  XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL",   -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
  effective_date date,
  currency_code varchar2(3),
  selling_unit_retail number(20,4),
  selling_uom varchar2(4),
  multi_units number(12,4),
  multi_unit_retail number(20,4),
  multi_selling_uom varchar2(4),
  promo_unit_retail number(20,4),
  promo_selling_uom varchar2(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, promo_unit_retail number
) return self as result
,constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"  -- Size of "RIB_XPrcEventDtl_TBL" is unbounded
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, promo_unit_retail number
, promo_selling_uom varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XPrcEventDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XPrcEventDesc') := "ns_name_XPrcEventDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'event_type') := event_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'event_id') := event_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_level') := item_level;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_level') := hier_level;
  IF XPrcEventDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XPrcEventDtl_TBL.';
    FOR INDX IN XPrcEventDtl_TBL.FIRST()..XPrcEventDtl_TBL.LAST() LOOP
      XPrcEventDtl_TBL(indx).appendNodeValues( i_prefix||indx||'XPrcEventDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_unit_retail') := selling_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'selling_uom') := selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_units') := multi_units;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_unit_retail') := multi_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'multi_selling_uom') := multi_selling_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_unit_retail') := promo_unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_selling_uom') := promo_selling_uom;
END appendNodeValues;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
self.multi_selling_uom := multi_selling_uom;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, promo_unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
self.multi_selling_uom := multi_selling_uom;
self.promo_unit_retail := promo_unit_retail;
RETURN;
end;
constructor function "RIB_XPrcEventDesc_REC"
(
  rib_oid number
, event_type varchar2
, event_id number
, item varchar2
, item_level number
, diff_id varchar2
, hier_level varchar2
, XPrcEventDtl_TBL "RIB_XPrcEventDtl_TBL"
, effective_date date
, currency_code varchar2
, selling_unit_retail number
, selling_uom varchar2
, multi_units number
, multi_unit_retail number
, multi_selling_uom varchar2
, promo_unit_retail number
, promo_selling_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.event_type := event_type;
self.event_id := event_id;
self.item := item;
self.item_level := item_level;
self.diff_id := diff_id;
self.hier_level := hier_level;
self.XPrcEventDtl_TBL := XPrcEventDtl_TBL;
self.effective_date := effective_date;
self.currency_code := currency_code;
self.selling_unit_retail := selling_unit_retail;
self.selling_uom := selling_uom;
self.multi_units := multi_units;
self.multi_unit_retail := multi_unit_retail;
self.multi_selling_uom := multi_selling_uom;
self.promo_unit_retail := promo_unit_retail;
self.promo_selling_uom := promo_selling_uom;
RETURN;
end;
END;
/
