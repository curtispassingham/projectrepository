DROP TYPE "RIB_StsTsfApvModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfApvModVo_REC";
/
DROP TYPE "RIB_StsTsfApvItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfApvItmMod_REC";
/
DROP TYPE "RIB_StsTsfApvModNote_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfApvModNote_REC";
/
DROP TYPE "RIB_StsTsfApvItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfApvItmMod_TBL" AS TABLE OF "RIB_StsTsfApvItmMod_REC";
/
DROP TYPE "RIB_StsTsfApvModNote_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StsTsfApvModNote_TBL" AS TABLE OF "RIB_StsTsfApvModNote_REC";
/
DROP TYPE "RIB_StsTsfApvModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfApvModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfApvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  tsf_id number(15),
  store_id number(10),
  StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL",   -- Size of "RIB_StsTsfApvItmMod_TBL" is 5000
  removed_line_id_col "RIB_removed_line_id_col_TBL",   -- Size of "RIB_removed_line_id_col_TBL" is 5000
  StsTsfApvModNote_TBL "RIB_StsTsfApvModNote_TBL",   -- Size of "RIB_StsTsfApvModNote_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
) return self as result
,constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"  -- Size of "RIB_StsTsfApvItmMod_TBL" is 5000
) return self as result
,constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"  -- Size of "RIB_StsTsfApvItmMod_TBL" is 5000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
) return self as result
,constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"  -- Size of "RIB_StsTsfApvItmMod_TBL" is 5000
, removed_line_id_col "RIB_removed_line_id_col_TBL"  -- Size of "RIB_removed_line_id_col_TBL" is 5000
, StsTsfApvModNote_TBL "RIB_StsTsfApvModNote_TBL"  -- Size of "RIB_StsTsfApvModNote_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfApvModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfApvModVo') := "ns_name_StsTsfApvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'tsf_id') := tsf_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  IF StsTsfApvItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfApvItmMod_TBL.';
    FOR INDX IN StsTsfApvItmMod_TBL.FIRST()..StsTsfApvItmMod_TBL.LAST() LOOP
      StsTsfApvItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfApvItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_line_id_col IS NOT NULL THEN
    FOR INDX IN removed_line_id_col.FIRST()..removed_line_id_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_line_id_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_line_id_col'||'.'):=removed_line_id_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StsTsfApvModNote_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StsTsfApvModNote_TBL.';
    FOR INDX IN StsTsfApvModNote_TBL.FIRST()..StsTsfApvModNote_TBL.LAST() LOOP
      StsTsfApvModNote_TBL(indx).appendNodeValues( i_prefix||indx||'StsTsfApvModNote_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
RETURN;
end;
constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.StsTsfApvItmMod_TBL := StsTsfApvItmMod_TBL;
RETURN;
end;
constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.StsTsfApvItmMod_TBL := StsTsfApvItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
RETURN;
end;
constructor function "RIB_StsTsfApvModVo_REC"
(
  rib_oid number
, tsf_id number
, store_id number
, StsTsfApvItmMod_TBL "RIB_StsTsfApvItmMod_TBL"
, removed_line_id_col "RIB_removed_line_id_col_TBL"
, StsTsfApvModNote_TBL "RIB_StsTsfApvModNote_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.tsf_id := tsf_id;
self.store_id := store_id;
self.StsTsfApvItmMod_TBL := StsTsfApvItmMod_TBL;
self.removed_line_id_col := removed_line_id_col;
self.StsTsfApvModNote_TBL := StsTsfApvModNote_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_removed_line_id_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_line_id_col_TBL" AS TABLE OF number(12);
/
DROP TYPE "RIB_StsTsfApvItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfApvItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfApvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(12),
  item_id varchar2(25),
  quantity number(20,4),
  case_size number(10,2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfApvItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, quantity number
, case_size number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfApvItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfApvModVo') := "ns_name_StsTsfApvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
END appendNodeValues;
constructor function "RIB_StsTsfApvItmMod_REC"
(
  rib_oid number
, line_id number
, item_id varchar2
, quantity number
, case_size number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.item_id := item_id;
self.quantity := quantity;
self.case_size := case_size;
RETURN;
end;
END;
/
DROP TYPE "RIB_StsTsfApvModNote_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StsTsfApvModNote_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StsTsfApvModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  note varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StsTsfApvModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StsTsfApvModNote_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StsTsfApvModVo') := "ns_name_StsTsfApvModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'note') := note;
END appendNodeValues;
constructor function "RIB_StsTsfApvModNote_REC"
(
  rib_oid number
, note varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.note := note;
RETURN;
end;
END;
/
