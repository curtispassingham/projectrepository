@@FulfilOrdCfmDtl.sql;
/
DROP TYPE "RIB_FulfilOrdCfmDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdCfmDesc_REC";
/
DROP TYPE "RIB_FulfilOrdCfmDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdCfmDtl_TBL" AS TABLE OF "RIB_FulfilOrdCfmDtl_REC";
/
DROP TYPE "RIB_FulfilOrdCfmDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FulfilOrdCfmDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FulfilOrdCfmDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_no varchar2(48),
  fulfill_order_no varchar2(48),
  confirm_type varchar2(1), -- confirm_type is enumeration field, valid values are [P, X] (all lower-case)
  confirm_no number(12),
  FulfilOrdCfmDtl_TBL "RIB_FulfilOrdCfmDtl_TBL",   -- Size of "RIB_FulfilOrdCfmDtl_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
) return self as result
,constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
, confirm_no number
) return self as result
,constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
, confirm_no number
, FulfilOrdCfmDtl_TBL "RIB_FulfilOrdCfmDtl_TBL"  -- Size of "RIB_FulfilOrdCfmDtl_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FulfilOrdCfmDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FulfilOrdCfmDesc') := "ns_name_FulfilOrdCfmDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_no') := customer_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_no') := fulfill_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'confirm_type') := confirm_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'confirm_no') := confirm_no;
  IF FulfilOrdCfmDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FulfilOrdCfmDtl_TBL.';
    FOR INDX IN FulfilOrdCfmDtl_TBL.FIRST()..FulfilOrdCfmDtl_TBL.LAST() LOOP
      FulfilOrdCfmDtl_TBL(indx).appendNodeValues( i_prefix||indx||'FulfilOrdCfmDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.confirm_type := confirm_type;
RETURN;
end;
constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
, confirm_no number
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.confirm_type := confirm_type;
self.confirm_no := confirm_no;
RETURN;
end;
constructor function "RIB_FulfilOrdCfmDesc_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, confirm_type varchar2
, confirm_no number
, FulfilOrdCfmDtl_TBL "RIB_FulfilOrdCfmDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.confirm_type := confirm_type;
self.confirm_no := confirm_no;
self.FulfilOrdCfmDtl_TBL := FulfilOrdCfmDtl_TBL;
RETURN;
end;
END;
/
