@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_FodModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodModVo_REC";
/
DROP TYPE "RIB_FodBolMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodBolMod_REC";
/
DROP TYPE "RIB_FodItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FodItmMod_REC";
/
DROP TYPE "RIB_FodItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FodItmMod_TBL" AS TABLE OF "RIB_FodItmMod_REC";
/
DROP TYPE "RIB_FodModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  int_fulfill_order_delivery_id number(12),
  notes "RIB_notes_TBL",   -- Size of "RIB_notes_TBL" is 10
  FodBolMod "RIB_FodBolMod_REC",
  FodItmMod_TBL "RIB_FodItmMod_TBL",   -- Size of "RIB_FodItmMod_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
) return self as result
,constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"  -- Size of "RIB_notes_TBL" is 10
) return self as result
,constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"  -- Size of "RIB_notes_TBL" is 10
, FodBolMod "RIB_FodBolMod_REC"
) return self as result
,constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"  -- Size of "RIB_notes_TBL" is 10
, FodBolMod "RIB_FodBolMod_REC"
, FodItmMod_TBL "RIB_FodItmMod_TBL"  -- Size of "RIB_FodItmMod_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodModVo') := "ns_name_FodModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'int_fulfill_order_delivery_id') := int_fulfill_order_delivery_id;
  IF notes IS NOT NULL THEN
    FOR INDX IN notes.FIRST()..notes.LAST() LOOP
      l_new_pre :=i_prefix||indx||'notes'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'notes'||'.'):=notes(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'FodBolMod.';
  FodBolMod.appendNodeValues( i_prefix||'FodBolMod');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF FodItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'FodItmMod_TBL.';
    FOR INDX IN FodItmMod_TBL.FIRST()..FodItmMod_TBL.LAST() LOOP
      FodItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'FodItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
RETURN;
end;
constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.notes := notes;
RETURN;
end;
constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"
, FodBolMod "RIB_FodBolMod_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.notes := notes;
self.FodBolMod := FodBolMod;
RETURN;
end;
constructor function "RIB_FodModVo_REC"
(
  rib_oid number
, int_fulfill_order_delivery_id number
, notes "RIB_notes_TBL"
, FodBolMod "RIB_FodBolMod_REC"
, FodItmMod_TBL "RIB_FodItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.int_fulfill_order_delivery_id := int_fulfill_order_delivery_id;
self.notes := notes;
self.FodBolMod := FodBolMod;
self.FodItmMod_TBL := FodItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_notes_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_notes_TBL" AS TABLE OF varchar2(2000);
/
DROP TYPE "RIB_FodBolMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodBolMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  bill_of_lading_motive_id varchar2(120),
  requested_pickup_date date,
  carrier_role varchar2(20), -- carrier_role is enumeration field, valid values are [SENDER, RECEIVER, THIRD_PARTY] (all lower-case)
  carrier_code varchar2(25),
  carrier_service_code varchar2(25),
  carrier_name varchar2(240),
  carrier_address varchar2(2000),
  carton_type_id number(12),
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  alternate_ship_to_address varchar2(2000),
  tracking_number varchar2(128),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, alternate_ship_to_address varchar2
) return self as result
,constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodBolMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodModVo') := "ns_name_FodModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'bill_of_lading_motive_id') := bill_of_lading_motive_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'requested_pickup_date') := requested_pickup_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_role') := carrier_role;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_name') := carrier_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_address') := carrier_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'carton_type_id') := carton_type_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'alternate_ship_to_address') := alternate_ship_to_address;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
END appendNodeValues;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, alternate_ship_to_address varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.alternate_ship_to_address := alternate_ship_to_address;
RETURN;
end;
constructor function "RIB_FodBolMod_REC"
(
  rib_oid number
, bill_of_lading_motive_id varchar2
, requested_pickup_date date
, carrier_role varchar2
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_name varchar2
, carrier_address varchar2
, carton_type_id number
, package_weight number
, package_weight_uom varchar2
, alternate_ship_to_address varchar2
, tracking_number varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.bill_of_lading_motive_id := bill_of_lading_motive_id;
self.requested_pickup_date := requested_pickup_date;
self.carrier_role := carrier_role;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_name := carrier_name;
self.carrier_address := carrier_address;
self.carton_type_id := carton_type_id;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.alternate_ship_to_address := alternate_ship_to_address;
self.tracking_number := tracking_number;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_FodItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FodItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FodModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  fulfill_order_delivery_line_id number(12),
  quantity number(20,4),
  added_uin_col "RIB_added_uin_col_TBL",   -- Size of "RIB_added_uin_col_TBL" is 100
  removed_uin_col "RIB_removed_uin_col_TBL",   -- Size of "RIB_removed_uin_col_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
) return self as result
,constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
) return self as result
,constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
) return self as result
,constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
,constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 100
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FodItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FodModVo') := "ns_name_FodModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_delivery_line_id') := fulfill_order_delivery_line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  IF added_uin_col IS NOT NULL THEN
    FOR INDX IN added_uin_col.FIRST()..added_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'added_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'added_uin_col'||'.'):=added_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_uin_col IS NOT NULL THEN
    FOR INDX IN removed_uin_col.FIRST()..removed_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_uin_col'||'.'):=removed_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
RETURN;
end;
constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
RETURN;
end;
constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_FodItmMod_REC"
(
  rib_oid number
, fulfill_order_delivery_line_id number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.fulfill_order_delivery_line_id := fulfill_order_delivery_line_id;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_added_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_added_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
