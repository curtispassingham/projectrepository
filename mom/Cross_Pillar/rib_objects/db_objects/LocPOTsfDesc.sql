DROP TYPE "RIB_LocPOTsfDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDtl_REC";
/
DROP TYPE "RIB_LocPOTsfDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDesc_REC";
/
DROP TYPE "RIB_LocPOTsfDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  pack_size number(12,4),
  ordered_quantity number(12,4),
  unit_cost number(20,4),
  orig_qty number(12,4),
  loc number(10),
  loc_type varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
) return self as result
,constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
) return self as result
,constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
) return self as result
,constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
, loc number
) return self as result
,constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
, loc number
, loc_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfDesc') := "ns_name_LocPOTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'pack_size') := pack_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'ordered_quantity') := ordered_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_cost') := unit_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_qty') := orig_qty;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
END appendNodeValues;
constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.pack_size := pack_size;
self.ordered_quantity := ordered_quantity;
RETURN;
end;
constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.pack_size := pack_size;
self.ordered_quantity := ordered_quantity;
self.unit_cost := unit_cost;
RETURN;
end;
constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.pack_size := pack_size;
self.ordered_quantity := ordered_quantity;
self.unit_cost := unit_cost;
self.orig_qty := orig_qty;
RETURN;
end;
constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
, loc number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.pack_size := pack_size;
self.ordered_quantity := ordered_quantity;
self.unit_cost := unit_cost;
self.orig_qty := orig_qty;
self.loc := loc;
RETURN;
end;
constructor function "RIB_LocPOTsfDtl_REC"
(
  rib_oid number
, item varchar2
, pack_size number
, ordered_quantity number
, unit_cost number
, orig_qty number
, loc number
, loc_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.pack_size := pack_size;
self.ordered_quantity := ordered_quantity;
self.unit_cost := unit_cost;
self.orig_qty := orig_qty;
self.loc := loc;
self.loc_type := loc_type;
RETURN;
end;
END;
/
DROP TYPE "RIB_LocPOTsfDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDtl_TBL" AS TABLE OF "RIB_LocPOTsfDtl_REC";
/
DROP TYPE "RIB_LocPOTsfDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LocPOTsfDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LocPOTsfDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  loc number(10),
  loc_type varchar2(1),
  order_id number(12),
  source_type varchar2(1),
  source_id number(10),
  user_id varchar2(30),
  comments varchar2(2000),
  order_status varchar2(6),
  not_before_date date,
  not_after_date date,
  create_date date,
  LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL",   -- Size of "RIB_LocPOTsfDtl_TBL" is unbounded
  orig_ind number(1),
  origin_country_id varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"  -- Size of "RIB_LocPOTsfDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"  -- Size of "RIB_LocPOTsfDtl_TBL" is unbounded
, orig_ind number
) return self as result
,constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"  -- Size of "RIB_LocPOTsfDtl_TBL" is unbounded
, orig_ind number
, origin_country_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LocPOTsfDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LocPOTsfDesc') := "ns_name_LocPOTsfDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'loc') := loc;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_id') := order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_type') := source_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_id') := source_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'user_id') := user_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_status') := order_status;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_before_date') := not_before_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'not_after_date') := not_after_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'create_date') := create_date;
  IF LocPOTsfDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LocPOTsfDtl_TBL.';
    FOR INDX IN LocPOTsfDtl_TBL.FIRST()..LocPOTsfDtl_TBL.LAST() LOOP
      LocPOTsfDtl_TBL(indx).appendNodeValues( i_prefix||indx||'LocPOTsfDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'orig_ind') := orig_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
END appendNodeValues;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.create_date := create_date;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.create_date := create_date;
self.LocPOTsfDtl_TBL := LocPOTsfDtl_TBL;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"
, orig_ind number
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.create_date := create_date;
self.LocPOTsfDtl_TBL := LocPOTsfDtl_TBL;
self.orig_ind := orig_ind;
RETURN;
end;
constructor function "RIB_LocPOTsfDesc_REC"
(
  rib_oid number
, loc number
, loc_type varchar2
, order_id number
, source_type varchar2
, source_id number
, user_id varchar2
, comments varchar2
, order_status varchar2
, not_before_date date
, not_after_date date
, create_date date
, LocPOTsfDtl_TBL "RIB_LocPOTsfDtl_TBL"
, orig_ind number
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.loc := loc;
self.loc_type := loc_type;
self.order_id := order_id;
self.source_type := source_type;
self.source_id := source_id;
self.user_id := user_id;
self.comments := comments;
self.order_status := order_status;
self.not_before_date := not_before_date;
self.not_after_date := not_after_date;
self.create_date := create_date;
self.LocPOTsfDtl_TBL := LocPOTsfDtl_TBL;
self.orig_ind := orig_ind;
self.origin_country_id := origin_country_id;
RETURN;
end;
END;
/
