@@XItemSupCtyMfrRef.sql;
/
DROP TYPE "RIB_XISCDimRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCDimRef_REC";
/
DROP TYPE "RIB_XItemUDARef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemUDARef_REC";
/
DROP TYPE "RIB_XItemBOMRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemBOMRef_REC";
/
DROP TYPE "RIB_XISCLocRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCLocRef_REC";
/
DROP TYPE "RIB_XItemVATRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemVATRef_REC";
/
DROP TYPE "RIB_XItemSeasonRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSeasonRef_REC";
/
DROP TYPE "RIB_LangOfXItemImageRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImageRef_REC";
/
DROP TYPE "RIB_XItemImageRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemImageRef_REC";
/
DROP TYPE "RIB_XItemSupCtyRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyRef_REC";
/
DROP TYPE "RIB_LangOfXItemSupRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupRef_REC";
/
DROP TYPE "RIB_XItemSupRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupRef_REC";
/
DROP TYPE "RIB_XItemCtryRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCtryRef_REC";
/
DROP TYPE "RIB_XItemCostRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCostRef_REC";
/
DROP TYPE "RIB_LangOfXItemRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemRef_REC";
/
DROP TYPE "RIB_XItemRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemRef_REC";
/
DROP TYPE "RIB_XISCDimRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XISCDimRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dim_object varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XISCDimRef_REC"
(
  rib_oid number
, dim_object varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XISCDimRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dim_object') := dim_object;
END appendNodeValues;
constructor function "RIB_XISCDimRef_REC"
(
  rib_oid number
, dim_object varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dim_object := dim_object;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemUDARef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemUDARef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  uda_id number(5),
  display_type varchar2(2),
  uda_value varchar2(30),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemUDARef_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
) return self as result
,constructor function "RIB_XItemUDARef_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_value varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemUDARef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_id') := uda_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'display_type') := display_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'uda_value') := uda_value;
END appendNodeValues;
constructor function "RIB_XItemUDARef_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
RETURN;
end;
constructor function "RIB_XItemUDARef_REC"
(
  rib_oid number
, uda_id number
, display_type varchar2
, uda_value varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.uda_id := uda_id;
self.display_type := display_type;
self.uda_value := uda_value;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemBOMRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemBOMRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  component_item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemBOMRef_REC"
(
  rib_oid number
, component_item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemBOMRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'component_item') := component_item;
END appendNodeValues;
constructor function "RIB_XItemBOMRef_REC"
(
  rib_oid number
, component_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.component_item := component_item;
RETURN;
end;
END;
/
DROP TYPE "RIB_XISCLocRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XISCLocRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  hier_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XISCLocRef_REC"
(
  rib_oid number
, hier_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XISCLocRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_id') := hier_id;
END appendNodeValues;
constructor function "RIB_XISCLocRef_REC"
(
  rib_oid number
, hier_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.hier_id := hier_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemVATRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemVATRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  vat_type varchar2(1),
  vat_region number(6),
  vat_code varchar2(6),
  active_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemVATRef_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemVATRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_type') := vat_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_region') := vat_region;
  rib_obj_util.g_RIB_element_values(i_prefix||'vat_code') := vat_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'active_date') := active_date;
END appendNodeValues;
constructor function "RIB_XItemVATRef_REC"
(
  rib_oid number
, vat_type varchar2
, vat_region number
, vat_code varchar2
, active_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.vat_type := vat_type;
self.vat_region := vat_region;
self.vat_code := vat_code;
self.active_date := active_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemSeasonRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSeasonRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  season_id number(3),
  phase_id number(3),
  diff_id varchar2(10),
  color number(4),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
) return self as result
,constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
, diff_id varchar2
) return self as result
,constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
, diff_id varchar2
, color number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSeasonRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'season_id') := season_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'phase_id') := phase_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'diff_id') := diff_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'color') := color;
END appendNodeValues;
constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
RETURN;
end;
constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
, diff_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.diff_id := diff_id;
RETURN;
end;
constructor function "RIB_XItemSeasonRef_REC"
(
  rib_oid number
, season_id number
, phase_id number
, diff_id varchar2
, color number
) return self as result is
begin
self.rib_oid := rib_oid;
self.season_id := season_id;
self.phase_id := phase_id;
self.diff_id := diff_id;
self.color := color;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemImageRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImageRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemImageRef_REC"
(
  rib_oid number
, lang number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemImageRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
END appendNodeValues;
constructor function "RIB_LangOfXItemImageRef_REC"
(
  rib_oid number
, lang number
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemImageRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemImageRef_TBL" AS TABLE OF "RIB_LangOfXItemImageRef_REC";
/
DROP TYPE "RIB_XItemImageRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemImageRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  image_name varchar2(120),
  LangOfXItemImageRef_TBL "RIB_LangOfXItemImageRef_TBL",   -- Size of "RIB_LangOfXItemImageRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemImageRef_REC"
(
  rib_oid number
, image_name varchar2
) return self as result
,constructor function "RIB_XItemImageRef_REC"
(
  rib_oid number
, image_name varchar2
, LangOfXItemImageRef_TBL "RIB_LangOfXItemImageRef_TBL"  -- Size of "RIB_LangOfXItemImageRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemImageRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'image_name') := image_name;
  IF LangOfXItemImageRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemImageRef_TBL.';
    FOR INDX IN LangOfXItemImageRef_TBL.FIRST()..LangOfXItemImageRef_TBL.LAST() LOOP
      LangOfXItemImageRef_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemImageRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemImageRef_REC"
(
  rib_oid number
, image_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
RETURN;
end;
constructor function "RIB_XItemImageRef_REC"
(
  rib_oid number
, image_name varchar2
, LangOfXItemImageRef_TBL "RIB_LangOfXItemImageRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.image_name := image_name;
self.LangOfXItemImageRef_TBL := LangOfXItemImageRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_XISCLocRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCLocRef_TBL" AS TABLE OF "RIB_XISCLocRef_REC";
/
DROP TYPE "RIB_XItemCostRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCostRef_TBL" AS TABLE OF "RIB_XItemCostRef_REC";
/
DROP TYPE "RIB_XISCDimRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XISCDimRef_TBL" AS TABLE OF "RIB_XISCDimRef_REC";
/
DROP TYPE "RIB_XItemSupCtyRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  origin_country_id varchar2(3),
  XISCLocRef_TBL "RIB_XISCLocRef_TBL",   -- Size of "RIB_XISCLocRef_TBL" is unbounded
  XItemCostRef_TBL "RIB_XItemCostRef_TBL",   -- Size of "RIB_XItemCostRef_TBL" is unbounded
  XISCDimRef_TBL "RIB_XISCDimRef_TBL",   -- Size of "RIB_XISCDimRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
) return self as result
,constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"  -- Size of "RIB_XISCLocRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"  -- Size of "RIB_XISCLocRef_TBL" is unbounded
, XItemCostRef_TBL "RIB_XItemCostRef_TBL"  -- Size of "RIB_XItemCostRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"  -- Size of "RIB_XISCLocRef_TBL" is unbounded
, XItemCostRef_TBL "RIB_XItemCostRef_TBL"  -- Size of "RIB_XItemCostRef_TBL" is unbounded
, XISCDimRef_TBL "RIB_XISCDimRef_TBL"  -- Size of "RIB_XISCDimRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSupCtyRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'origin_country_id') := origin_country_id;
  IF XISCLocRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XISCLocRef_TBL.';
    FOR INDX IN XISCLocRef_TBL.FIRST()..XISCLocRef_TBL.LAST() LOOP
      XISCLocRef_TBL(indx).appendNodeValues( i_prefix||indx||'XISCLocRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemCostRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemCostRef_TBL.';
    FOR INDX IN XItemCostRef_TBL.FIRST()..XItemCostRef_TBL.LAST() LOOP
      XItemCostRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemCostRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XISCDimRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XISCDimRef_TBL.';
    FOR INDX IN XISCDimRef_TBL.FIRST()..XISCDimRef_TBL.LAST() LOOP
      XISCDimRef_TBL(indx).appendNodeValues( i_prefix||indx||'XISCDimRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
RETURN;
end;
constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.XISCLocRef_TBL := XISCLocRef_TBL;
RETURN;
end;
constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"
, XItemCostRef_TBL "RIB_XItemCostRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.XISCLocRef_TBL := XISCLocRef_TBL;
self.XItemCostRef_TBL := XItemCostRef_TBL;
RETURN;
end;
constructor function "RIB_XItemSupCtyRef_REC"
(
  rib_oid number
, origin_country_id varchar2
, XISCLocRef_TBL "RIB_XISCLocRef_TBL"
, XItemCostRef_TBL "RIB_XItemCostRef_TBL"
, XISCDimRef_TBL "RIB_XISCDimRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.origin_country_id := origin_country_id;
self.XISCLocRef_TBL := XISCLocRef_TBL;
self.XItemCostRef_TBL := XItemCostRef_TBL;
self.XISCDimRef_TBL := XISCDimRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemSupRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemSupRef_REC"
(
  rib_oid number
, lang number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemSupRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
END appendNodeValues;
constructor function "RIB_LangOfXItemSupRef_REC"
(
  rib_oid number
, lang number
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemSupCtyRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyRef_TBL" AS TABLE OF "RIB_XItemSupCtyRef_REC";
/
DROP TYPE "RIB_XItemSupCtyMfrRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupCtyMfrRef_TBL" AS TABLE OF "RIB_XItemSupCtyMfrRef_REC";
/
DROP TYPE "RIB_LangOfXItemSupRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemSupRef_TBL" AS TABLE OF "RIB_LangOfXItemSupRef_REC";
/
DROP TYPE "RIB_XItemSupRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemSupRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  supplier varchar2(10),
  delete_children_ind varchar2(1),
  XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL",   -- Size of "RIB_XItemSupCtyRef_TBL" is unbounded
  XItemSupCtyMfrRef_TBL "RIB_XItemSupCtyMfrRef_TBL",   -- Size of "RIB_XItemSupCtyMfrRef_TBL" is unbounded
  LangOfXItemSupRef_TBL "RIB_LangOfXItemSupRef_TBL",   -- Size of "RIB_LangOfXItemSupRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
) return self as result
,constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"  -- Size of "RIB_XItemSupCtyRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"  -- Size of "RIB_XItemSupCtyRef_TBL" is unbounded
, XItemSupCtyMfrRef_TBL "RIB_XItemSupCtyMfrRef_TBL"  -- Size of "RIB_XItemSupCtyMfrRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"  -- Size of "RIB_XItemSupCtyRef_TBL" is unbounded
, XItemSupCtyMfrRef_TBL "RIB_XItemSupCtyMfrRef_TBL"  -- Size of "RIB_XItemSupCtyMfrRef_TBL" is unbounded
, LangOfXItemSupRef_TBL "RIB_LangOfXItemSupRef_TBL"  -- Size of "RIB_LangOfXItemSupRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemSupRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'supplier') := supplier;
  rib_obj_util.g_RIB_element_values(i_prefix||'delete_children_ind') := delete_children_ind;
  IF XItemSupCtyRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSupCtyRef_TBL.';
    FOR INDX IN XItemSupCtyRef_TBL.FIRST()..XItemSupCtyRef_TBL.LAST() LOOP
      XItemSupCtyRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSupCtyRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemSupCtyMfrRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSupCtyMfrRef_TBL.';
    FOR INDX IN XItemSupCtyMfrRef_TBL.FIRST()..XItemSupCtyMfrRef_TBL.LAST() LOOP
      XItemSupCtyMfrRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSupCtyMfrRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF LangOfXItemSupRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemSupRef_TBL.';
    FOR INDX IN LangOfXItemSupRef_TBL.FIRST()..LangOfXItemSupRef_TBL.LAST() LOOP
      LangOfXItemSupRef_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemSupRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.delete_children_ind := delete_children_ind;
RETURN;
end;
constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.delete_children_ind := delete_children_ind;
self.XItemSupCtyRef_TBL := XItemSupCtyRef_TBL;
RETURN;
end;
constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"
, XItemSupCtyMfrRef_TBL "RIB_XItemSupCtyMfrRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.delete_children_ind := delete_children_ind;
self.XItemSupCtyRef_TBL := XItemSupCtyRef_TBL;
self.XItemSupCtyMfrRef_TBL := XItemSupCtyMfrRef_TBL;
RETURN;
end;
constructor function "RIB_XItemSupRef_REC"
(
  rib_oid number
, supplier varchar2
, delete_children_ind varchar2
, XItemSupCtyRef_TBL "RIB_XItemSupCtyRef_TBL"
, XItemSupCtyMfrRef_TBL "RIB_XItemSupCtyMfrRef_TBL"
, LangOfXItemSupRef_TBL "RIB_LangOfXItemSupRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.supplier := supplier;
self.delete_children_ind := delete_children_ind;
self.XItemSupCtyRef_TBL := XItemSupCtyRef_TBL;
self.XItemSupCtyMfrRef_TBL := XItemSupCtyMfrRef_TBL;
self.LangOfXItemSupRef_TBL := LangOfXItemSupRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCtryRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemCtryRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  country_id varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemCtryRef_REC"
(
  rib_oid number
, country_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemCtryRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'country_id') := country_id;
END appendNodeValues;
constructor function "RIB_XItemCtryRef_REC"
(
  rib_oid number
, country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.country_id := country_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCostRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemCostRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  delivery_country_id varchar2(3),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemCostRef_REC"
(
  rib_oid number
, delivery_country_id varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemCostRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_country_id') := delivery_country_id;
END appendNodeValues;
constructor function "RIB_XItemCostRef_REC"
(
  rib_oid number
, delivery_country_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.delivery_country_id := delivery_country_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_LangOfXItemRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LangOfXItemRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  lang number(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LangOfXItemRef_REC"
(
  rib_oid number
, lang number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LangOfXItemRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'lang') := lang;
END appendNodeValues;
constructor function "RIB_LangOfXItemRef_REC"
(
  rib_oid number
, lang number
) return self as result is
begin
self.rib_oid := rib_oid;
self.lang := lang;
RETURN;
end;
END;
/
DROP TYPE "RIB_XItemCtryRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemCtryRef_TBL" AS TABLE OF "RIB_XItemCtryRef_REC";
/
DROP TYPE "RIB_XItemSupRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSupRef_TBL" AS TABLE OF "RIB_XItemSupRef_REC";
/
DROP TYPE "RIB_XItemVATRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemVATRef_TBL" AS TABLE OF "RIB_XItemVATRef_REC";
/
DROP TYPE "RIB_XItemImageRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemImageRef_TBL" AS TABLE OF "RIB_XItemImageRef_REC";
/
DROP TYPE "RIB_XItemSeasonRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemSeasonRef_TBL" AS TABLE OF "RIB_XItemSeasonRef_REC";
/
DROP TYPE "RIB_XItemUDARef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemUDARef_TBL" AS TABLE OF "RIB_XItemUDARef_REC";
/
DROP TYPE "RIB_XItemBOMRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_XItemBOMRef_TBL" AS TABLE OF "RIB_XItemBOMRef_REC";
/
DROP TYPE "RIB_LangOfXItemRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LangOfXItemRef_TBL" AS TABLE OF "RIB_LangOfXItemRef_REC";
/
DROP TYPE "RIB_XItemRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_XItemRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_XItemRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item varchar2(25),
  hier_level varchar2(2),
  XItemCtryRef_TBL "RIB_XItemCtryRef_TBL",   -- Size of "RIB_XItemCtryRef_TBL" is unbounded
  XItemSupRef_TBL "RIB_XItemSupRef_TBL",   -- Size of "RIB_XItemSupRef_TBL" is unbounded
  XItemVATRef_TBL "RIB_XItemVATRef_TBL",   -- Size of "RIB_XItemVATRef_TBL" is unbounded
  XItemImageRef_TBL "RIB_XItemImageRef_TBL",   -- Size of "RIB_XItemImageRef_TBL" is unbounded
  XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL",   -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
  XItemUDARef_TBL "RIB_XItemUDARef_TBL",   -- Size of "RIB_XItemUDARef_TBL" is unbounded
  XItemBOMRef_TBL "RIB_XItemBOMRef_TBL",   -- Size of "RIB_XItemBOMRef_TBL" is unbounded
  system_ind varchar2(1),
  upc_supplement number(5),
  LangOfXItemRef_TBL "RIB_LangOfXItemRef_TBL",   -- Size of "RIB_LangOfXItemRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"  -- Size of "RIB_XItemUDARef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"  -- Size of "RIB_XItemUDARef_TBL" is unbounded
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"  -- Size of "RIB_XItemBOMRef_TBL" is unbounded
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"  -- Size of "RIB_XItemUDARef_TBL" is unbounded
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"  -- Size of "RIB_XItemBOMRef_TBL" is unbounded
, system_ind varchar2
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"  -- Size of "RIB_XItemUDARef_TBL" is unbounded
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"  -- Size of "RIB_XItemBOMRef_TBL" is unbounded
, system_ind varchar2
, upc_supplement number
) return self as result
,constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"  -- Size of "RIB_XItemCtryRef_TBL" is unbounded
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"  -- Size of "RIB_XItemSupRef_TBL" is unbounded
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"  -- Size of "RIB_XItemVATRef_TBL" is unbounded
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"  -- Size of "RIB_XItemImageRef_TBL" is unbounded
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"  -- Size of "RIB_XItemSeasonRef_TBL" is unbounded
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"  -- Size of "RIB_XItemUDARef_TBL" is unbounded
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"  -- Size of "RIB_XItemBOMRef_TBL" is unbounded
, system_ind varchar2
, upc_supplement number
, LangOfXItemRef_TBL "RIB_LangOfXItemRef_TBL"  -- Size of "RIB_LangOfXItemRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_XItemRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_XItemRef') := "ns_name_XItemRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'hier_level') := hier_level;
  IF XItemCtryRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemCtryRef_TBL.';
    FOR INDX IN XItemCtryRef_TBL.FIRST()..XItemCtryRef_TBL.LAST() LOOP
      XItemCtryRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemCtryRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemSupRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSupRef_TBL.';
    FOR INDX IN XItemSupRef_TBL.FIRST()..XItemSupRef_TBL.LAST() LOOP
      XItemSupRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSupRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemVATRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemVATRef_TBL.';
    FOR INDX IN XItemVATRef_TBL.FIRST()..XItemVATRef_TBL.LAST() LOOP
      XItemVATRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemVATRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemImageRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemImageRef_TBL.';
    FOR INDX IN XItemImageRef_TBL.FIRST()..XItemImageRef_TBL.LAST() LOOP
      XItemImageRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemImageRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemSeasonRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemSeasonRef_TBL.';
    FOR INDX IN XItemSeasonRef_TBL.FIRST()..XItemSeasonRef_TBL.LAST() LOOP
      XItemSeasonRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemSeasonRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemUDARef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemUDARef_TBL.';
    FOR INDX IN XItemUDARef_TBL.FIRST()..XItemUDARef_TBL.LAST() LOOP
      XItemUDARef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemUDARef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF XItemBOMRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'XItemBOMRef_TBL.';
    FOR INDX IN XItemBOMRef_TBL.FIRST()..XItemBOMRef_TBL.LAST() LOOP
      XItemBOMRef_TBL(indx).appendNodeValues( i_prefix||indx||'XItemBOMRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'system_ind') := system_ind;
  rib_obj_util.g_RIB_element_values(i_prefix||'upc_supplement') := upc_supplement;
  IF LangOfXItemRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LangOfXItemRef_TBL.';
    FOR INDX IN LangOfXItemRef_TBL.FIRST()..LangOfXItemRef_TBL.LAST() LOOP
      LangOfXItemRef_TBL(indx).appendNodeValues( i_prefix||indx||'LangOfXItemRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
self.XItemUDARef_TBL := XItemUDARef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
self.XItemUDARef_TBL := XItemUDARef_TBL;
self.XItemBOMRef_TBL := XItemBOMRef_TBL;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"
, system_ind varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
self.XItemUDARef_TBL := XItemUDARef_TBL;
self.XItemBOMRef_TBL := XItemBOMRef_TBL;
self.system_ind := system_ind;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"
, system_ind varchar2
, upc_supplement number
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
self.XItemUDARef_TBL := XItemUDARef_TBL;
self.XItemBOMRef_TBL := XItemBOMRef_TBL;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
RETURN;
end;
constructor function "RIB_XItemRef_REC"
(
  rib_oid number
, item varchar2
, hier_level varchar2
, XItemCtryRef_TBL "RIB_XItemCtryRef_TBL"
, XItemSupRef_TBL "RIB_XItemSupRef_TBL"
, XItemVATRef_TBL "RIB_XItemVATRef_TBL"
, XItemImageRef_TBL "RIB_XItemImageRef_TBL"
, XItemSeasonRef_TBL "RIB_XItemSeasonRef_TBL"
, XItemUDARef_TBL "RIB_XItemUDARef_TBL"
, XItemBOMRef_TBL "RIB_XItemBOMRef_TBL"
, system_ind varchar2
, upc_supplement number
, LangOfXItemRef_TBL "RIB_LangOfXItemRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.item := item;
self.hier_level := hier_level;
self.XItemCtryRef_TBL := XItemCtryRef_TBL;
self.XItemSupRef_TBL := XItemSupRef_TBL;
self.XItemVATRef_TBL := XItemVATRef_TBL;
self.XItemImageRef_TBL := XItemImageRef_TBL;
self.XItemSeasonRef_TBL := XItemSeasonRef_TBL;
self.XItemUDARef_TBL := XItemUDARef_TBL;
self.XItemBOMRef_TBL := XItemBOMRef_TBL;
self.system_ind := system_ind;
self.upc_supplement := upc_supplement;
self.LangOfXItemRef_TBL := LangOfXItemRef_TBL;
RETURN;
end;
END;
/
