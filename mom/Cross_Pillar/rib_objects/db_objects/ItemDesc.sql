@@ItemHdrDesc.sql;
/
@@ItemSupDesc.sql;
/
@@ItemSupCtyDesc.sql;
/
@@ISCDimDesc.sql;
/
@@ItemUDALOVDesc.sql;
/
@@ItemUDAFFDesc.sql;
/
@@ItemUDADateDesc.sql;
/
@@ItemImageDesc.sql;
/
@@ItemUPCDesc.sql;
/
@@ItemBOMDesc.sql;
/
@@ItemTcktDesc.sql;
/
@@ItemSupCtyMfrDesc.sql;
/
@@RelatedItemDesc.sql;
/
DROP TYPE "RIB_ItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemDesc_REC";
/
DROP TYPE "RIB_ItemSupDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemSupDesc_TBL" AS TABLE OF "RIB_ItemSupDesc_REC";
/
DROP TYPE "RIB_ItemSupCtyDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemSupCtyDesc_TBL" AS TABLE OF "RIB_ItemSupCtyDesc_REC";
/
DROP TYPE "RIB_ISCDimDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ISCDimDesc_TBL" AS TABLE OF "RIB_ISCDimDesc_REC";
/
DROP TYPE "RIB_ItemUDALOVDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemUDALOVDesc_TBL" AS TABLE OF "RIB_ItemUDALOVDesc_REC";
/
DROP TYPE "RIB_ItemUDAFFDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemUDAFFDesc_TBL" AS TABLE OF "RIB_ItemUDAFFDesc_REC";
/
DROP TYPE "RIB_ItemUDADateDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemUDADateDesc_TBL" AS TABLE OF "RIB_ItemUDADateDesc_REC";
/
DROP TYPE "RIB_ItemImageDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemImageDesc_TBL" AS TABLE OF "RIB_ItemImageDesc_REC";
/
DROP TYPE "RIB_ItemUPCDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemUPCDesc_TBL" AS TABLE OF "RIB_ItemUPCDesc_REC";
/
DROP TYPE "RIB_ItemBOMDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemBOMDesc_TBL" AS TABLE OF "RIB_ItemBOMDesc_REC";
/
DROP TYPE "RIB_ItemTcktDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemTcktDesc_TBL" AS TABLE OF "RIB_ItemTcktDesc_REC";
/
DROP TYPE "RIB_ItemSupCtyMfrDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_ItemSupCtyMfrDesc_TBL" AS TABLE OF "RIB_ItemSupCtyMfrDesc_REC";
/
DROP TYPE "RIB_RelatedItemDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_RelatedItemDesc_TBL" AS TABLE OF "RIB_RelatedItemDesc_REC";
/
DROP TYPE "RIB_ItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  ItemHdrDesc "RIB_ItemHdrDesc_REC",
  ItemSupDesc_TBL "RIB_ItemSupDesc_TBL",   -- Size of "RIB_ItemSupDesc_TBL" is unbounded
  ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL",   -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
  ISCDimDesc_TBL "RIB_ISCDimDesc_TBL",   -- Size of "RIB_ISCDimDesc_TBL" is unbounded
  ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL",   -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
  ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL",   -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
  ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL",   -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
  ItemImageDesc_TBL "RIB_ItemImageDesc_TBL",   -- Size of "RIB_ItemImageDesc_TBL" is unbounded
  ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL",   -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
  ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL",   -- Size of "RIB_ItemBOMDesc_TBL" is unbounded
  ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL",   -- Size of "RIB_ItemTcktDesc_TBL" is unbounded
  ItemSupCtyMfrDesc_TBL "RIB_ItemSupCtyMfrDesc_TBL",   -- Size of "RIB_ItemSupCtyMfrDesc_TBL" is unbounded
  RelatedItemDesc_TBL "RIB_RelatedItemDesc_TBL",   -- Size of "RIB_RelatedItemDesc_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"  -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"  -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"  -- Size of "RIB_ItemBOMDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"  -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"  -- Size of "RIB_ItemBOMDesc_TBL" is unbounded
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"  -- Size of "RIB_ItemTcktDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"  -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"  -- Size of "RIB_ItemBOMDesc_TBL" is unbounded
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"  -- Size of "RIB_ItemTcktDesc_TBL" is unbounded
, ItemSupCtyMfrDesc_TBL "RIB_ItemSupCtyMfrDesc_TBL"  -- Size of "RIB_ItemSupCtyMfrDesc_TBL" is unbounded
) return self as result
,constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"  -- Size of "RIB_ItemSupDesc_TBL" is unbounded
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"  -- Size of "RIB_ItemSupCtyDesc_TBL" is unbounded
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"  -- Size of "RIB_ISCDimDesc_TBL" is unbounded
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"  -- Size of "RIB_ItemUDALOVDesc_TBL" is unbounded
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"  -- Size of "RIB_ItemUDAFFDesc_TBL" is unbounded
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"  -- Size of "RIB_ItemUDADateDesc_TBL" is unbounded
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"  -- Size of "RIB_ItemImageDesc_TBL" is unbounded
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"  -- Size of "RIB_ItemUPCDesc_TBL" is unbounded
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"  -- Size of "RIB_ItemBOMDesc_TBL" is unbounded
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"  -- Size of "RIB_ItemTcktDesc_TBL" is unbounded
, ItemSupCtyMfrDesc_TBL "RIB_ItemSupCtyMfrDesc_TBL"  -- Size of "RIB_ItemSupCtyMfrDesc_TBL" is unbounded
, RelatedItemDesc_TBL "RIB_RelatedItemDesc_TBL"  -- Size of "RIB_RelatedItemDesc_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ItemDesc') := "ns_name_ItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'ItemHdrDesc.';
  ItemHdrDesc.appendNodeValues( i_prefix||'ItemHdrDesc');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
  IF ItemSupDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemSupDesc_TBL.';
    FOR INDX IN ItemSupDesc_TBL.FIRST()..ItemSupDesc_TBL.LAST() LOOP
      ItemSupDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemSupDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemSupCtyDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemSupCtyDesc_TBL.';
    FOR INDX IN ItemSupCtyDesc_TBL.FIRST()..ItemSupCtyDesc_TBL.LAST() LOOP
      ItemSupCtyDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemSupCtyDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ISCDimDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ISCDimDesc_TBL.';
    FOR INDX IN ISCDimDesc_TBL.FIRST()..ISCDimDesc_TBL.LAST() LOOP
      ISCDimDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ISCDimDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemUDALOVDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemUDALOVDesc_TBL.';
    FOR INDX IN ItemUDALOVDesc_TBL.FIRST()..ItemUDALOVDesc_TBL.LAST() LOOP
      ItemUDALOVDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemUDALOVDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemUDAFFDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemUDAFFDesc_TBL.';
    FOR INDX IN ItemUDAFFDesc_TBL.FIRST()..ItemUDAFFDesc_TBL.LAST() LOOP
      ItemUDAFFDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemUDAFFDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemUDADateDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemUDADateDesc_TBL.';
    FOR INDX IN ItemUDADateDesc_TBL.FIRST()..ItemUDADateDesc_TBL.LAST() LOOP
      ItemUDADateDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemUDADateDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemImageDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemImageDesc_TBL.';
    FOR INDX IN ItemImageDesc_TBL.FIRST()..ItemImageDesc_TBL.LAST() LOOP
      ItemImageDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemImageDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemUPCDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemUPCDesc_TBL.';
    FOR INDX IN ItemUPCDesc_TBL.FIRST()..ItemUPCDesc_TBL.LAST() LOOP
      ItemUPCDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemUPCDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemBOMDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemBOMDesc_TBL.';
    FOR INDX IN ItemBOMDesc_TBL.FIRST()..ItemBOMDesc_TBL.LAST() LOOP
      ItemBOMDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemBOMDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemTcktDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemTcktDesc_TBL.';
    FOR INDX IN ItemTcktDesc_TBL.FIRST()..ItemTcktDesc_TBL.LAST() LOOP
      ItemTcktDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemTcktDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF ItemSupCtyMfrDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'ItemSupCtyMfrDesc_TBL.';
    FOR INDX IN ItemSupCtyMfrDesc_TBL.FIRST()..ItemSupCtyMfrDesc_TBL.LAST() LOOP
      ItemSupCtyMfrDesc_TBL(indx).appendNodeValues( i_prefix||indx||'ItemSupCtyMfrDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF RelatedItemDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'RelatedItemDesc_TBL.';
    FOR INDX IN RelatedItemDesc_TBL.FIRST()..RelatedItemDesc_TBL.LAST() LOOP
      RelatedItemDesc_TBL(indx).appendNodeValues( i_prefix||indx||'RelatedItemDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
RETURN;
end;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
self.ItemUPCDesc_TBL := ItemUPCDesc_TBL;
RETURN;
end;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
self.ItemUPCDesc_TBL := ItemUPCDesc_TBL;
self.ItemBOMDesc_TBL := ItemBOMDesc_TBL;
RETURN;
end;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
self.ItemUPCDesc_TBL := ItemUPCDesc_TBL;
self.ItemBOMDesc_TBL := ItemBOMDesc_TBL;
self.ItemTcktDesc_TBL := ItemTcktDesc_TBL;
RETURN;
end;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"
, ItemSupCtyMfrDesc_TBL "RIB_ItemSupCtyMfrDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
self.ItemUPCDesc_TBL := ItemUPCDesc_TBL;
self.ItemBOMDesc_TBL := ItemBOMDesc_TBL;
self.ItemTcktDesc_TBL := ItemTcktDesc_TBL;
self.ItemSupCtyMfrDesc_TBL := ItemSupCtyMfrDesc_TBL;
RETURN;
end;
constructor function "RIB_ItemDesc_REC"
(
  rib_oid number
, ItemHdrDesc "RIB_ItemHdrDesc_REC"
, ItemSupDesc_TBL "RIB_ItemSupDesc_TBL"
, ItemSupCtyDesc_TBL "RIB_ItemSupCtyDesc_TBL"
, ISCDimDesc_TBL "RIB_ISCDimDesc_TBL"
, ItemUDALOVDesc_TBL "RIB_ItemUDALOVDesc_TBL"
, ItemUDAFFDesc_TBL "RIB_ItemUDAFFDesc_TBL"
, ItemUDADateDesc_TBL "RIB_ItemUDADateDesc_TBL"
, ItemImageDesc_TBL "RIB_ItemImageDesc_TBL"
, ItemUPCDesc_TBL "RIB_ItemUPCDesc_TBL"
, ItemBOMDesc_TBL "RIB_ItemBOMDesc_TBL"
, ItemTcktDesc_TBL "RIB_ItemTcktDesc_TBL"
, ItemSupCtyMfrDesc_TBL "RIB_ItemSupCtyMfrDesc_TBL"
, RelatedItemDesc_TBL "RIB_RelatedItemDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.ItemHdrDesc := ItemHdrDesc;
self.ItemSupDesc_TBL := ItemSupDesc_TBL;
self.ItemSupCtyDesc_TBL := ItemSupCtyDesc_TBL;
self.ISCDimDesc_TBL := ISCDimDesc_TBL;
self.ItemUDALOVDesc_TBL := ItemUDALOVDesc_TBL;
self.ItemUDAFFDesc_TBL := ItemUDAFFDesc_TBL;
self.ItemUDADateDesc_TBL := ItemUDADateDesc_TBL;
self.ItemImageDesc_TBL := ItemImageDesc_TBL;
self.ItemUPCDesc_TBL := ItemUPCDesc_TBL;
self.ItemBOMDesc_TBL := ItemBOMDesc_TBL;
self.ItemTcktDesc_TBL := ItemTcktDesc_TBL;
self.ItemSupCtyMfrDesc_TBL := ItemSupCtyMfrDesc_TBL;
self.RelatedItemDesc_TBL := RelatedItemDesc_TBL;
RETURN;
end;
END;
/
