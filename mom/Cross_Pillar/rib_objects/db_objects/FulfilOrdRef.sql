@@FulfilOrdDtlRef.sql;
/
DROP TYPE "RIB_FulfilOrdRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdRef_REC";
/
DROP TYPE "RIB_FulfilOrdDtlRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_FulfilOrdDtlRef_TBL" AS TABLE OF "RIB_FulfilOrdDtlRef_REC";
/
DROP TYPE "RIB_FulfilOrdRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_FulfilOrdRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_FulfilOrdRef" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  customer_order_no varchar2(48),
  fulfill_order_no varchar2(48),
  source_loc_type varchar2(2), -- source_loc_type is enumeration field, valid values are [SU, ST, WH] (all lower-case)
  source_loc_id number(10),
  fulfill_loc_type varchar2(1), -- fulfill_loc_type is enumeration field, valid values are [S, V] (all lower-case)
  fulfill_loc_id number(10),
  FulfilOrdDtlRef_TBL "RIB_FulfilOrdDtlRef_TBL",   -- Size of "RIB_FulfilOrdDtlRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_FulfilOrdRef_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, source_loc_type varchar2
, source_loc_id number
, fulfill_loc_type varchar2
, fulfill_loc_id number
, FulfilOrdDtlRef_TBL "RIB_FulfilOrdDtlRef_TBL"  -- Size of "RIB_FulfilOrdDtlRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_FulfilOrdRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_FulfilOrdRef') := "ns_name_FulfilOrdRef";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_no') := customer_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_no') := fulfill_order_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_loc_type') := source_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'source_loc_id') := source_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_type') := fulfill_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_loc_id') := fulfill_loc_id;
  l_new_pre :=i_prefix||'FulfilOrdDtlRef_TBL.';
  FOR INDX IN FulfilOrdDtlRef_TBL.FIRST()..FulfilOrdDtlRef_TBL.LAST() LOOP
    FulfilOrdDtlRef_TBL(indx).appendNodeValues( i_prefix||indx||'FulfilOrdDtlRef_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_FulfilOrdRef_REC"
(
  rib_oid number
, customer_order_no varchar2
, fulfill_order_no varchar2
, source_loc_type varchar2
, source_loc_id number
, fulfill_loc_type varchar2
, fulfill_loc_id number
, FulfilOrdDtlRef_TBL "RIB_FulfilOrdDtlRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.customer_order_no := customer_order_no;
self.fulfill_order_no := fulfill_order_no;
self.source_loc_type := source_loc_type;
self.source_loc_id := source_loc_id;
self.fulfill_loc_type := fulfill_loc_type;
self.fulfill_loc_id := fulfill_loc_id;
self.FulfilOrdDtlRef_TBL := FulfilOrdDtlRef_TBL;
RETURN;
end;
END;
/
