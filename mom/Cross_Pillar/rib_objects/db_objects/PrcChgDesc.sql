DROP TYPE "RIB_PrcChgRef_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgRef_REC";
/
DROP TYPE "RIB_PrcChgDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgDtl_REC";
/
DROP TYPE "RIB_PrcChg_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChg_REC";
/
DROP TYPE "RIB_PrcChgDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgDesc_REC";
/
DROP TYPE "RIB_PrcChgRef_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChgRef_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  change_id number(15),
  promo_comp_id number(10),
  promo_id number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
) return self as result
,constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
, promo_comp_id number
) return self as result
,constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
, promo_comp_id number
, promo_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChgRef_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgDesc') := "ns_name_PrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'change_id') := change_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_comp_id') := promo_comp_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'promo_id') := promo_id;
END appendNodeValues;
constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.change_id := change_id;
RETURN;
end;
constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
, promo_comp_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.change_id := change_id;
self.promo_comp_id := promo_comp_id;
RETURN;
end;
constructor function "RIB_PrcChgRef_REC"
(
  rib_oid number
, change_id number
, promo_comp_id number
, promo_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.change_id := change_id;
self.promo_comp_id := promo_comp_id;
self.promo_id := promo_id;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcChgDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChgDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  unit_retail number(20,4),
  new_uom varchar2(4),
  effective_date date,
  end_date date,
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
) return self as result
,constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
) return self as result
,constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
, effective_date date
) return self as result
,constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
, effective_date date
, end_date date
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChgDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgDesc') := "ns_name_PrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_retail') := unit_retail;
  rib_obj_util.g_RIB_element_values(i_prefix||'new_uom') := new_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'effective_date') := effective_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'end_date') := end_date;
END appendNodeValues;
constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
) return self as result is
begin
self.rib_oid := rib_oid;
self.unit_retail := unit_retail;
RETURN;
end;
constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.unit_retail := unit_retail;
self.new_uom := new_uom;
RETURN;
end;
constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
, effective_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.unit_retail := unit_retail;
self.new_uom := new_uom;
self.effective_date := effective_date;
RETURN;
end;
constructor function "RIB_PrcChgDtl_REC"
(
  rib_oid number
, unit_retail number
, new_uom varchar2
, effective_date date
, end_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.unit_retail := unit_retail;
self.new_uom := new_uom;
self.effective_date := effective_date;
self.end_date := end_date;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcChgDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgDtl_TBL" AS TABLE OF "RIB_PrcChgDtl_REC";
/
DROP TYPE "RIB_PrcChgRef_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChgRef_TBL" AS TABLE OF "RIB_PrcChgRef_REC";
/
DROP TYPE "RIB_PrcChg_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChg_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  location number(10),
  loc_type varchar2(1),
  item varchar2(25),
  change_type varchar2(6),
  PrcChgDtl_TBL "RIB_PrcChgDtl_TBL",   -- Size of "RIB_PrcChgDtl_TBL" is unbounded
  PrcChgRef_TBL "RIB_PrcChgRef_TBL",   -- Size of "RIB_PrcChgRef_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
) return self as result
,constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
, PrcChgDtl_TBL "RIB_PrcChgDtl_TBL"  -- Size of "RIB_PrcChgDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
, PrcChgDtl_TBL "RIB_PrcChgDtl_TBL"  -- Size of "RIB_PrcChgDtl_TBL" is unbounded
, PrcChgRef_TBL "RIB_PrcChgRef_TBL"  -- Size of "RIB_PrcChgRef_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChg_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgDesc') := "ns_name_PrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'location') := location;
  rib_obj_util.g_RIB_element_values(i_prefix||'loc_type') := loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'item') := item;
  rib_obj_util.g_RIB_element_values(i_prefix||'change_type') := change_type;
  IF PrcChgDtl_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrcChgDtl_TBL.';
    FOR INDX IN PrcChgDtl_TBL.FIRST()..PrcChgDtl_TBL.LAST() LOOP
      PrcChgDtl_TBL(indx).appendNodeValues( i_prefix||indx||'PrcChgDtl_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF PrcChgRef_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'PrcChgRef_TBL.';
    FOR INDX IN PrcChgRef_TBL.FIRST()..PrcChgRef_TBL.LAST() LOOP
      PrcChgRef_TBL(indx).appendNodeValues( i_prefix||indx||'PrcChgRef_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.item := item;
self.change_type := change_type;
RETURN;
end;
constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
, PrcChgDtl_TBL "RIB_PrcChgDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.item := item;
self.change_type := change_type;
self.PrcChgDtl_TBL := PrcChgDtl_TBL;
RETURN;
end;
constructor function "RIB_PrcChg_REC"
(
  rib_oid number
, location number
, loc_type varchar2
, item varchar2
, change_type varchar2
, PrcChgDtl_TBL "RIB_PrcChgDtl_TBL"
, PrcChgRef_TBL "RIB_PrcChgRef_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.location := location;
self.loc_type := loc_type;
self.item := item;
self.change_type := change_type;
self.PrcChgDtl_TBL := PrcChgDtl_TBL;
self.PrcChgRef_TBL := PrcChgRef_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcChg_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcChg_TBL" AS TABLE OF "RIB_PrcChg_REC";
/
DROP TYPE "RIB_PrcChgDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcChgDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcChgDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  PrcChg_TBL "RIB_PrcChg_TBL",   -- Size of "RIB_PrcChg_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcChgDesc_REC"
(
  rib_oid number
, PrcChg_TBL "RIB_PrcChg_TBL"  -- Size of "RIB_PrcChg_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcChgDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcChgDesc') := "ns_name_PrcChgDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  l_new_pre :=i_prefix||'PrcChg_TBL.';
  FOR INDX IN PrcChg_TBL.FIRST()..PrcChg_TBL.LAST() LOOP
    PrcChg_TBL(indx).appendNodeValues( i_prefix||indx||'PrcChg_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PrcChgDesc_REC"
(
  rib_oid number
, PrcChg_TBL "RIB_PrcChg_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.PrcChg_TBL := PrcChg_TBL;
RETURN;
end;
END;
/
