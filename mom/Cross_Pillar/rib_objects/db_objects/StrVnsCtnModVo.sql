@@StrInvExtAttDesc.sql;
/
DROP TYPE "RIB_StrVnsCtnModVo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnModVo_REC";
/
DROP TYPE "RIB_StrVnsCtnItmMod_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItmMod_REC";
/
DROP TYPE "RIB_StrVnsCtnItmMod_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItmMod_TBL" AS TABLE OF "RIB_StrVnsCtnItmMod_REC";
/
DROP TYPE "RIB_StrVnsCtnModVo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnModVo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  shipment_id number(15),
  container_id number(15),
  store_id number(10),
  container_label varchar2(128),
  package_weight number(12,4),
  package_weight_uom varchar2(4),
  tracking_number varchar2(128),
  package_type varchar2(128),
  restriction_level varchar2(10), -- restriction_level is enumeration field, valid values are [DEPT, CLASS, SUBCLASS, NONE] (all lower-case)
  StrVnsCtnItmMod_TBL "RIB_StrVnsCtnItmMod_TBL",   -- Size of "RIB_StrVnsCtnItmMod_TBL" is 5000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsCtnModVo_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
) return self as result
,constructor function "RIB_StrVnsCtnModVo_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, StrVnsCtnItmMod_TBL "RIB_StrVnsCtnItmMod_TBL"  -- Size of "RIB_StrVnsCtnItmMod_TBL" is 5000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsCtnModVo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsCtnModVo') := "ns_name_StrVnsCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'shipment_id') := shipment_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_id') := container_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'container_label') := container_label;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight') := package_weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_weight_uom') := package_weight_uom;
  rib_obj_util.g_RIB_element_values(i_prefix||'tracking_number') := tracking_number;
  rib_obj_util.g_RIB_element_values(i_prefix||'package_type') := package_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'restriction_level') := restriction_level;
  IF StrVnsCtnItmMod_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrVnsCtnItmMod_TBL.';
    FOR INDX IN StrVnsCtnItmMod_TBL.FIRST()..StrVnsCtnItmMod_TBL.LAST() LOOP
      StrVnsCtnItmMod_TBL(indx).appendNodeValues( i_prefix||indx||'StrVnsCtnItmMod_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsCtnModVo_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
RETURN;
end;
constructor function "RIB_StrVnsCtnModVo_REC"
(
  rib_oid number
, shipment_id number
, container_id number
, store_id number
, container_label varchar2
, package_weight number
, package_weight_uom varchar2
, tracking_number varchar2
, package_type varchar2
, restriction_level varchar2
, StrVnsCtnItmMod_TBL "RIB_StrVnsCtnItmMod_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.shipment_id := shipment_id;
self.container_id := container_id;
self.store_id := store_id;
self.container_label := container_label;
self.package_weight := package_weight;
self.package_weight_uom := package_weight_uom;
self.tracking_number := tracking_number;
self.package_type := package_type;
self.restriction_level := restriction_level;
self.StrVnsCtnItmMod_TBL := StrVnsCtnItmMod_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_StrInvExtAttDesc_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_StrInvExtAttDesc_TBL" AS TABLE OF "RIB_StrInvExtAttDesc_REC";
/
DROP TYPE "RIB_StrVnsCtnItmMod_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_StrVnsCtnItmMod_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_StrVnsCtnModVo" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_id number(15),
  external_id number(15),
  item_id varchar2(25),
  reason_id number(15),
  case_size number(10,2),
  quantity number(20,4),
  added_uin_col "RIB_added_uin_col_TBL",   -- Size of "RIB_added_uin_col_TBL" is 1000
  removed_uin_col "RIB_removed_uin_col_TBL",   -- Size of "RIB_removed_uin_col_TBL" is 1000
  StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL",   -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
  removed_ext_att_col "RIB_removed_ext_att_col_TBL",   -- Size of "RIB_removed_ext_att_col_TBL" is 1000
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
) return self as result
,constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 1000
) return self as result
,constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 1000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
) return self as result
,constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 1000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
) return self as result
,constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"  -- Size of "RIB_added_uin_col_TBL" is 1000
, removed_uin_col "RIB_removed_uin_col_TBL"  -- Size of "RIB_removed_uin_col_TBL" is 1000
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"  -- Size of "RIB_StrInvExtAttDesc_TBL" is 1000
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"  -- Size of "RIB_removed_ext_att_col_TBL" is 1000
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_StrVnsCtnItmMod_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_StrVnsCtnModVo') := "ns_name_StrVnsCtnModVo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_id') := line_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'external_id') := external_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_id') := reason_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'case_size') := case_size;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  IF added_uin_col IS NOT NULL THEN
    FOR INDX IN added_uin_col.FIRST()..added_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'added_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'added_uin_col'||'.'):=added_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_uin_col IS NOT NULL THEN
    FOR INDX IN removed_uin_col.FIRST()..removed_uin_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_uin_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_uin_col'||'.'):=removed_uin_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF StrInvExtAttDesc_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'StrInvExtAttDesc_TBL.';
    FOR INDX IN StrInvExtAttDesc_TBL.FIRST()..StrInvExtAttDesc_TBL.LAST() LOOP
      StrInvExtAttDesc_TBL(indx).appendNodeValues( i_prefix||indx||'StrInvExtAttDesc_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF removed_ext_att_col IS NOT NULL THEN
    FOR INDX IN removed_ext_att_col.FIRST()..removed_ext_att_col.LAST() LOOP
      l_new_pre :=i_prefix||indx||'removed_ext_att_col'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'removed_ext_att_col'||'.'):=removed_ext_att_col(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
END appendNodeValues;
constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity := quantity;
RETURN;
end;
constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
RETURN;
end;
constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
RETURN;
end;
constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
RETURN;
end;
constructor function "RIB_StrVnsCtnItmMod_REC"
(
  rib_oid number
, line_id number
, external_id number
, item_id varchar2
, reason_id number
, case_size number
, quantity number
, added_uin_col "RIB_added_uin_col_TBL"
, removed_uin_col "RIB_removed_uin_col_TBL"
, StrInvExtAttDesc_TBL "RIB_StrInvExtAttDesc_TBL"
, removed_ext_att_col "RIB_removed_ext_att_col_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_id := line_id;
self.external_id := external_id;
self.item_id := item_id;
self.reason_id := reason_id;
self.case_size := case_size;
self.quantity := quantity;
self.added_uin_col := added_uin_col;
self.removed_uin_col := removed_uin_col;
self.StrInvExtAttDesc_TBL := StrInvExtAttDesc_TBL;
self.removed_ext_att_col := removed_ext_att_col;
RETURN;
end;
END;
/
DROP TYPE "RIB_added_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_added_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_uin_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_uin_col_TBL" AS TABLE OF varchar2(128);
/
DROP TYPE "RIB_removed_ext_att_col_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_removed_ext_att_col_TBL" AS TABLE OF varchar2(128);
/
