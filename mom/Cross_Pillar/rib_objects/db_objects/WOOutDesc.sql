DROP TYPE "RIB_WOOutActivity_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutActivity_REC";
/
DROP TYPE "RIB_WOOutPackTo_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPackTo_REC";
/
DROP TYPE "RIB_WOOutPackFrom_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPackFrom_REC";
/
DROP TYPE "RIB_WOOutPacking_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPacking_REC";
/
DROP TYPE "RIB_WOOutDtl_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutDtl_REC";
/
DROP TYPE "RIB_WOOutXForm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutXForm_REC";
/
DROP TYPE "RIB_WOOutDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutDesc_REC";
/
DROP TYPE "RIB_WOOutActivity_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutActivity_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  activity_id number(10),
  activity_cost number(20,4),
  comments varchar2(2000),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
) return self as result
,constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
, activity_cost number
) return self as result
,constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
, activity_cost number
, comments varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutActivity_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'activity_id') := activity_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'activity_cost') := activity_cost;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
END appendNodeValues;
constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.activity_id := activity_id;
RETURN;
end;
constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
, activity_cost number
) return self as result is
begin
self.rib_oid := rib_oid;
self.activity_id := activity_id;
self.activity_cost := activity_cost;
RETURN;
end;
constructor function "RIB_WOOutActivity_REC"
(
  rib_oid number
, activity_id number
, activity_cost number
, comments varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.activity_id := activity_id;
self.activity_cost := activity_cost;
self.comments := comments;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutPackTo_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutPackTo_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  to_item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutPackTo_REC"
(
  rib_oid number
, to_item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutPackTo_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'to_item') := to_item;
END appendNodeValues;
constructor function "RIB_WOOutPackTo_REC"
(
  rib_oid number
, to_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.to_item := to_item;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutPackFrom_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutPackFrom_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutPackFrom_REC"
(
  rib_oid number
, from_item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutPackFrom_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_item') := from_item;
END appendNodeValues;
constructor function "RIB_WOOutPackFrom_REC"
(
  rib_oid number
, from_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_item := from_item;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutPackTo_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPackTo_TBL" AS TABLE OF "RIB_WOOutPackTo_REC";
/
DROP TYPE "RIB_WOOutPackFrom_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPackFrom_TBL" AS TABLE OF "RIB_WOOutPackFrom_REC";
/
DROP TYPE "RIB_WOOutPacking_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutPacking_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  WOOutPackTo_TBL "RIB_WOOutPackTo_TBL",   -- Size of "RIB_WOOutPackTo_TBL" is unbounded
  WOOutPackFrom_TBL "RIB_WOOutPackFrom_TBL",   -- Size of "RIB_WOOutPackFrom_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutPacking_REC"
(
  rib_oid number
, WOOutPackTo_TBL "RIB_WOOutPackTo_TBL"  -- Size of "RIB_WOOutPackTo_TBL" is unbounded
, WOOutPackFrom_TBL "RIB_WOOutPackFrom_TBL"  -- Size of "RIB_WOOutPackFrom_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutPacking_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  l_new_pre :=i_prefix||'WOOutPackTo_TBL.';
  FOR INDX IN WOOutPackTo_TBL.FIRST()..WOOutPackTo_TBL.LAST() LOOP
    WOOutPackTo_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutPackTo_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  l_new_pre :=i_prefix||'WOOutPackFrom_TBL.';
  FOR INDX IN WOOutPackFrom_TBL.FIRST()..WOOutPackFrom_TBL.LAST() LOOP
    WOOutPackFrom_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutPackFrom_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_WOOutPacking_REC"
(
  rib_oid number
, WOOutPackTo_TBL "RIB_WOOutPackTo_TBL"
, WOOutPackFrom_TBL "RIB_WOOutPackFrom_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.WOOutPackTo_TBL := WOOutPackTo_TBL;
self.WOOutPackFrom_TBL := WOOutPackFrom_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutActivity_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutActivity_TBL" AS TABLE OF "RIB_WOOutActivity_REC";
/
DROP TYPE "RIB_WOOutDtl_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutDtl_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  dest_id varchar2(10),
  item_id varchar2(25),
  wip_seq_nbr number(7),
  wip_code varchar2(9),
  personalization varchar2(300),
  instructions varchar2(300),
  order_line_nbr number(3),
  auto_complete varchar2(1),
  WOOutActivity_TBL "RIB_WOOutActivity_TBL",   -- Size of "RIB_WOOutActivity_TBL" is unbounded
  inv_status number(2),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
) return self as result
,constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
, WOOutActivity_TBL "RIB_WOOutActivity_TBL"  -- Size of "RIB_WOOutActivity_TBL" is unbounded
) return self as result
,constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
, WOOutActivity_TBL "RIB_WOOutActivity_TBL"  -- Size of "RIB_WOOutActivity_TBL" is unbounded
, inv_status number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutDtl_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'dest_id') := dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_seq_nbr') := wip_seq_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'wip_code') := wip_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'personalization') := personalization;
  rib_obj_util.g_RIB_element_values(i_prefix||'instructions') := instructions;
  rib_obj_util.g_RIB_element_values(i_prefix||'order_line_nbr') := order_line_nbr;
  rib_obj_util.g_RIB_element_values(i_prefix||'auto_complete') := auto_complete;
  IF WOOutActivity_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOOutActivity_TBL.';
    FOR INDX IN WOOutActivity_TBL.FIRST()..WOOutActivity_TBL.LAST() LOOP
      WOOutActivity_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutActivity_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_status') := inv_status;
END appendNodeValues;
constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
self.wip_seq_nbr := wip_seq_nbr;
self.wip_code := wip_code;
self.personalization := personalization;
self.instructions := instructions;
self.order_line_nbr := order_line_nbr;
self.auto_complete := auto_complete;
RETURN;
end;
constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
, WOOutActivity_TBL "RIB_WOOutActivity_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
self.wip_seq_nbr := wip_seq_nbr;
self.wip_code := wip_code;
self.personalization := personalization;
self.instructions := instructions;
self.order_line_nbr := order_line_nbr;
self.auto_complete := auto_complete;
self.WOOutActivity_TBL := WOOutActivity_TBL;
RETURN;
end;
constructor function "RIB_WOOutDtl_REC"
(
  rib_oid number
, dest_id varchar2
, item_id varchar2
, wip_seq_nbr number
, wip_code varchar2
, personalization varchar2
, instructions varchar2
, order_line_nbr number
, auto_complete varchar2
, WOOutActivity_TBL "RIB_WOOutActivity_TBL"
, inv_status number
) return self as result is
begin
self.rib_oid := rib_oid;
self.dest_id := dest_id;
self.item_id := item_id;
self.wip_seq_nbr := wip_seq_nbr;
self.wip_code := wip_code;
self.personalization := personalization;
self.instructions := instructions;
self.order_line_nbr := order_line_nbr;
self.auto_complete := auto_complete;
self.WOOutActivity_TBL := WOOutActivity_TBL;
self.inv_status := inv_status;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutXForm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutXForm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  from_item varchar2(25),
  to_item varchar2(25),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutXForm_REC"
(
  rib_oid number
, from_item varchar2
, to_item varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutXForm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'from_item') := from_item;
  rib_obj_util.g_RIB_element_values(i_prefix||'to_item') := to_item;
END appendNodeValues;
constructor function "RIB_WOOutXForm_REC"
(
  rib_oid number
, from_item varchar2
, to_item varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.from_item := from_item;
self.to_item := to_item;
RETURN;
end;
END;
/
DROP TYPE "RIB_WOOutDtl_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutDtl_TBL" AS TABLE OF "RIB_WOOutDtl_REC";
/
DROP TYPE "RIB_WOOutXForm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutXForm_TBL" AS TABLE OF "RIB_WOOutXForm_REC";
/
DROP TYPE "RIB_WOOutPacking_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_WOOutPacking_TBL" AS TABLE OF "RIB_WOOutPacking_REC";
/
DROP TYPE "RIB_WOOutDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_WOOutDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_WOOutDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  wo_id number(15),
  dc_dest_id varchar2(10),
  distro_nbr varchar2(12),
  WOOutDtl_TBL "RIB_WOOutDtl_TBL",   -- Size of "RIB_WOOutDtl_TBL" is unbounded
  distro_parent_nbr varchar2(12),
  WOOutXForm_TBL "RIB_WOOutXForm_TBL",   -- Size of "RIB_WOOutXForm_TBL" is unbounded
  WOOutPacking_TBL "RIB_WOOutPacking_TBL",   -- Size of "RIB_WOOutPacking_TBL" is unbounded
  inv_type varchar2(6),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"  -- Size of "RIB_WOOutDtl_TBL" is unbounded
) return self as result
,constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"  -- Size of "RIB_WOOutDtl_TBL" is unbounded
, distro_parent_nbr varchar2
) return self as result
,constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"  -- Size of "RIB_WOOutDtl_TBL" is unbounded
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"  -- Size of "RIB_WOOutXForm_TBL" is unbounded
) return self as result
,constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"  -- Size of "RIB_WOOutDtl_TBL" is unbounded
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"  -- Size of "RIB_WOOutXForm_TBL" is unbounded
, WOOutPacking_TBL "RIB_WOOutPacking_TBL"  -- Size of "RIB_WOOutPacking_TBL" is unbounded
) return self as result
,constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"  -- Size of "RIB_WOOutDtl_TBL" is unbounded
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"  -- Size of "RIB_WOOutXForm_TBL" is unbounded
, WOOutPacking_TBL "RIB_WOOutPacking_TBL"  -- Size of "RIB_WOOutPacking_TBL" is unbounded
, inv_type varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_WOOutDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_WOOutDesc') := "ns_name_WOOutDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'wo_id') := wo_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'dc_dest_id') := dc_dest_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_nbr') := distro_nbr;
  l_new_pre :=i_prefix||'WOOutDtl_TBL.';
  FOR INDX IN WOOutDtl_TBL.FIRST()..WOOutDtl_TBL.LAST() LOOP
    WOOutDtl_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutDtl_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'distro_parent_nbr') := distro_parent_nbr;
  IF WOOutXForm_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOOutXForm_TBL.';
    FOR INDX IN WOOutXForm_TBL.FIRST()..WOOutXForm_TBL.LAST() LOOP
      WOOutXForm_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutXForm_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  IF WOOutPacking_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'WOOutPacking_TBL.';
    FOR INDX IN WOOutPacking_TBL.FIRST()..WOOutPacking_TBL.LAST() LOOP
      WOOutPacking_TBL(indx).appendNodeValues( i_prefix||indx||'WOOutPacking_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'inv_type') := inv_type;
END appendNodeValues;
constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtl_TBL := WOOutDtl_TBL;
RETURN;
end;
constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"
, distro_parent_nbr varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtl_TBL := WOOutDtl_TBL;
self.distro_parent_nbr := distro_parent_nbr;
RETURN;
end;
constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtl_TBL := WOOutDtl_TBL;
self.distro_parent_nbr := distro_parent_nbr;
self.WOOutXForm_TBL := WOOutXForm_TBL;
RETURN;
end;
constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"
, WOOutPacking_TBL "RIB_WOOutPacking_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtl_TBL := WOOutDtl_TBL;
self.distro_parent_nbr := distro_parent_nbr;
self.WOOutXForm_TBL := WOOutXForm_TBL;
self.WOOutPacking_TBL := WOOutPacking_TBL;
RETURN;
end;
constructor function "RIB_WOOutDesc_REC"
(
  rib_oid number
, wo_id number
, dc_dest_id varchar2
, distro_nbr varchar2
, WOOutDtl_TBL "RIB_WOOutDtl_TBL"
, distro_parent_nbr varchar2
, WOOutXForm_TBL "RIB_WOOutXForm_TBL"
, WOOutPacking_TBL "RIB_WOOutPacking_TBL"
, inv_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.wo_id := wo_id;
self.dc_dest_id := dc_dest_id;
self.distro_nbr := distro_nbr;
self.WOOutDtl_TBL := WOOutDtl_TBL;
self.distro_parent_nbr := distro_parent_nbr;
self.WOOutXForm_TBL := WOOutXForm_TBL;
self.WOOutPacking_TBL := WOOutPacking_TBL;
self.inv_type := inv_type;
RETURN;
end;
END;
/
