DROP TYPE "RIB_CustOrdDelDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_CustOrdDelDesc_REC";
/
DROP TYPE "RIB_CustOrdDelDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_CustOrdDelDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_CustOrdDelDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  delivery_loc_type varchar2(1), -- delivery_loc_type is enumeration field, valid values are [S, W] (all lower-case)
  delivery_loc_id number(10),
  delivery_type varchar2(1), -- delivery_type is enumeration field, valid values are [C, S] (all lower-case)
  delivery_date date,
  delivery_time date,
  customer_id varchar2(14),
  customer_signature "RIB_customer_signature_TBL",   -- Size of "RIB_customer_signature_TBL" is unbounded
  carrier_code varchar2(4),
  carrier_service_code varchar2(6),
  carrier_tracking_no varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"  -- Size of "RIB_customer_signature_TBL" is unbounded
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"  -- Size of "RIB_customer_signature_TBL" is unbounded
, carrier_code varchar2
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"  -- Size of "RIB_customer_signature_TBL" is unbounded
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result
,constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"  -- Size of "RIB_customer_signature_TBL" is unbounded
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_tracking_no varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_CustOrdDelDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_CustOrdDelDesc') := "ns_name_CustOrdDelDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_loc_type') := delivery_loc_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_loc_id') := delivery_loc_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_type') := delivery_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_date') := delivery_date;
  rib_obj_util.g_RIB_element_values(i_prefix||'delivery_time') := delivery_time;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_id') := customer_id;
  IF customer_signature IS NOT NULL THEN
    FOR INDX IN customer_signature.FIRST()..customer_signature.LAST() LOOP
      l_new_pre :=i_prefix||indx||'customer_signature'||'.';
      RIB_obj_util.g_RIB_element_values( i_prefix||indx||'customer_signature'||'.'):=customer_signature(indx);
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_code') := carrier_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_service_code') := carrier_service_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'carrier_tracking_no') := carrier_tracking_no;
END appendNodeValues;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
self.customer_id := customer_id;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
self.customer_id := customer_id;
self.customer_signature := customer_signature;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"
, carrier_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
self.customer_id := customer_id;
self.customer_signature := customer_signature;
self.carrier_code := carrier_code;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"
, carrier_code varchar2
, carrier_service_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
self.customer_id := customer_id;
self.customer_signature := customer_signature;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
RETURN;
end;
constructor function "RIB_CustOrdDelDesc_REC"
(
  rib_oid number
, seq_no number
, delivery_loc_type varchar2
, delivery_loc_id number
, delivery_type varchar2
, delivery_date date
, delivery_time date
, customer_id varchar2
, customer_signature "RIB_customer_signature_TBL"
, carrier_code varchar2
, carrier_service_code varchar2
, carrier_tracking_no varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.delivery_loc_type := delivery_loc_type;
self.delivery_loc_id := delivery_loc_id;
self.delivery_type := delivery_type;
self.delivery_date := delivery_date;
self.delivery_time := delivery_time;
self.customer_id := customer_id;
self.customer_signature := customer_signature;
self.carrier_code := carrier_code;
self.carrier_service_code := carrier_service_code;
self.carrier_tracking_no := carrier_tracking_no;
RETURN;
end;
END;
/
DROP TYPE "RIB_customer_signature_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_customer_signature_TBL" AS TABLE OF varchar2(5000);
/
