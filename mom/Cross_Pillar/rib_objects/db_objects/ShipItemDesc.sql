DROP TYPE "RIB_ShipItemDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_ShipItemDesc_REC";
/
DROP TYPE "RIB_ShipItemDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_ShipItemDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_ShipItemDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  seq_no number(4),
  item_id varchar2(25),
  item_upc varchar2(25),
  item_quantity number(12,4),
  unit_of_measure varchar2(4),
  currency_code varchar2(3),
  price number(20,4),
  discount_price number(20,4),
  weight number(12,4),
  size_code varchar2(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
) return self as result
,constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
, weight number
) return self as result
,constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
, weight number
, size_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_ShipItemDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_ShipItemDesc') := "ns_name_ShipItemDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'seq_no') := seq_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_upc') := item_upc;
  rib_obj_util.g_RIB_element_values(i_prefix||'item_quantity') := item_quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'price') := price;
  rib_obj_util.g_RIB_element_values(i_prefix||'discount_price') := discount_price;
  rib_obj_util.g_RIB_element_values(i_prefix||'weight') := weight;
  rib_obj_util.g_RIB_element_values(i_prefix||'size_code') := size_code;
END appendNodeValues;
constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.item_id := item_id;
self.item_upc := item_upc;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.price := price;
self.discount_price := discount_price;
RETURN;
end;
constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
, weight number
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.item_id := item_id;
self.item_upc := item_upc;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.price := price;
self.discount_price := discount_price;
self.weight := weight;
RETURN;
end;
constructor function "RIB_ShipItemDesc_REC"
(
  rib_oid number
, seq_no number
, item_id varchar2
, item_upc varchar2
, item_quantity number
, unit_of_measure varchar2
, currency_code varchar2
, price number
, discount_price number
, weight number
, size_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.seq_no := seq_no;
self.item_id := item_id;
self.item_upc := item_upc;
self.item_quantity := item_quantity;
self.unit_of_measure := unit_of_measure;
self.currency_code := currency_code;
self.price := price;
self.discount_price := discount_price;
self.weight := weight;
self.size_code := size_code;
RETURN;
end;
END;
/
