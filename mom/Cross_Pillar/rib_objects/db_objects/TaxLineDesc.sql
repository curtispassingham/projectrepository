DROP TYPE "RIB_TaxLineDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TaxLineDesc_REC";
/
DROP TYPE "RIB_TaxLineDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TaxLineDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_TaxLineDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_no number(4),
  tax_authority_id varchar2(22),
  tax_group_id number(22),
  tax_type_code number(22),
  tax_holiday_flag varchar2(1), -- tax_holiday_flag is enumeration field, valid values are [Y, N] (all lower-case)
  currency_code varchar2(3),
  taxable_amount number(20,4),
  tax_amount number(20,4),
  completed_tax_amount number(20,4),
  cancelled_tax_amount number(20,4),
  returned_tax_amount number(20,4),
  inclusive_tax_flag varchar2(1), -- inclusive_tax_flag is enumeration field, valid values are [Y, N] (all lower-case)
  tax_mode varchar2(12), -- tax_mode is enumeration field, valid values are [STANDARD, NONTAXABLE, OVERRIDERATE, OVERRIDEAMT, TOGGLEON, TOGGLEOFF, EXEMPT] (all lower-case)
  tax_mod_reason_code varchar2(20),
  tax_mod_scope varchar2(3), -- tax_mod_scope is enumeration field, valid values are [ORD, ITM] (all lower-case)
  tax_rate number(8,5),
  tax_rule_name varchar2(120),
  tax_authority_name varchar2(120),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
) return self as result
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
) return self as result
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
) return self as result
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
) return self as result
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
, tax_rule_name varchar2
) return self as result
,constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
, tax_rule_name varchar2
, tax_authority_name varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TaxLineDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_TaxLineDesc') := "ns_name_TaxLineDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_no') := line_no;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_authority_id') := tax_authority_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_group_id') := tax_group_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_type_code') := tax_type_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_holiday_flag') := tax_holiday_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'taxable_amount') := taxable_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_amount') := tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'completed_tax_amount') := completed_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'cancelled_tax_amount') := cancelled_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'returned_tax_amount') := returned_tax_amount;
  rib_obj_util.g_RIB_element_values(i_prefix||'inclusive_tax_flag') := inclusive_tax_flag;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_mode') := tax_mode;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_mod_reason_code') := tax_mod_reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_mod_scope') := tax_mod_scope;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rate') := tax_rate;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_rule_name') := tax_rule_name;
  rib_obj_util.g_RIB_element_values(i_prefix||'tax_authority_name') := tax_authority_name;
END appendNodeValues;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
RETURN;
end;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
self.tax_mod_reason_code := tax_mod_reason_code;
RETURN;
end;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
self.tax_mod_reason_code := tax_mod_reason_code;
self.tax_mod_scope := tax_mod_scope;
RETURN;
end;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
self.tax_mod_reason_code := tax_mod_reason_code;
self.tax_mod_scope := tax_mod_scope;
self.tax_rate := tax_rate;
RETURN;
end;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
, tax_rule_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
self.tax_mod_reason_code := tax_mod_reason_code;
self.tax_mod_scope := tax_mod_scope;
self.tax_rate := tax_rate;
self.tax_rule_name := tax_rule_name;
RETURN;
end;
constructor function "RIB_TaxLineDesc_REC"
(
  rib_oid number
, line_no number
, tax_authority_id varchar2
, tax_group_id number
, tax_type_code number
, tax_holiday_flag varchar2
, currency_code varchar2
, taxable_amount number
, tax_amount number
, completed_tax_amount number
, cancelled_tax_amount number
, returned_tax_amount number
, inclusive_tax_flag varchar2
, tax_mode varchar2
, tax_mod_reason_code varchar2
, tax_mod_scope varchar2
, tax_rate number
, tax_rule_name varchar2
, tax_authority_name varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_no := line_no;
self.tax_authority_id := tax_authority_id;
self.tax_group_id := tax_group_id;
self.tax_type_code := tax_type_code;
self.tax_holiday_flag := tax_holiday_flag;
self.currency_code := currency_code;
self.taxable_amount := taxable_amount;
self.tax_amount := tax_amount;
self.completed_tax_amount := completed_tax_amount;
self.cancelled_tax_amount := cancelled_tax_amount;
self.returned_tax_amount := returned_tax_amount;
self.inclusive_tax_flag := inclusive_tax_flag;
self.tax_mode := tax_mode;
self.tax_mod_reason_code := tax_mod_reason_code;
self.tax_mod_scope := tax_mod_scope;
self.tax_rate := tax_rate;
self.tax_rule_name := tax_rule_name;
self.tax_authority_name := tax_authority_name;
RETURN;
end;
END;
/
