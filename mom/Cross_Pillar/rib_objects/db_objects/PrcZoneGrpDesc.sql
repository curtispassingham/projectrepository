DROP TYPE "RIB_PrcZoneGrpSt_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpSt_REC";
/
DROP TYPE "RIB_PriceZone_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PriceZone_REC";
/
DROP TYPE "RIB_PrcZoneGrpDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpDesc_REC";
/
DROP TYPE "RIB_PrcZoneGrpSt_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpSt_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store number(10),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcZoneGrpSt_REC"
(
  rib_oid number
, store number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcZoneGrpSt_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpDesc') := "ns_name_PrcZoneGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'store') := store;
END appendNodeValues;
constructor function "RIB_PrcZoneGrpSt_REC"
(
  rib_oid number
, store number
) return self as result is
begin
self.rib_oid := rib_oid;
self.store := store;
RETURN;
end;
END;
/
DROP TYPE "RIB_PrcZoneGrpSt_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpSt_TBL" AS TABLE OF "RIB_PrcZoneGrpSt_REC";
/
DROP TYPE "RIB_PriceZone_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PriceZone_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  zone_id number(10),
  currency_code varchar2(3),
  PrcZoneGrpSt_TBL "RIB_PrcZoneGrpSt_TBL",   -- Size of "RIB_PrcZoneGrpSt_TBL" is unbounded
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PriceZone_REC"
(
  rib_oid number
, zone_id number
, currency_code varchar2
, PrcZoneGrpSt_TBL "RIB_PrcZoneGrpSt_TBL"  -- Size of "RIB_PrcZoneGrpSt_TBL" is unbounded
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PriceZone_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpDesc') := "ns_name_PrcZoneGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'zone_id') := zone_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'currency_code') := currency_code;
  l_new_pre :=i_prefix||'PrcZoneGrpSt_TBL.';
  FOR INDX IN PrcZoneGrpSt_TBL.FIRST()..PrcZoneGrpSt_TBL.LAST() LOOP
    PrcZoneGrpSt_TBL(indx).appendNodeValues( i_prefix||indx||'PrcZoneGrpSt_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PriceZone_REC"
(
  rib_oid number
, zone_id number
, currency_code varchar2
, PrcZoneGrpSt_TBL "RIB_PrcZoneGrpSt_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.zone_id := zone_id;
self.currency_code := currency_code;
self.PrcZoneGrpSt_TBL := PrcZoneGrpSt_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PriceZone_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PriceZone_TBL" AS TABLE OF "RIB_PriceZone_REC";
/
DROP TYPE "RIB_PrcZoneGrpDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PrcZoneGrpDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PrcZoneGrpDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  zone_group_id number(4),
  PriceZone_TBL "RIB_PriceZone_TBL",   -- Size of "RIB_PriceZone_TBL" is unbounded
  pricing_level varchar2(1),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PrcZoneGrpDesc_REC"
(
  rib_oid number
, zone_group_id number
, PriceZone_TBL "RIB_PriceZone_TBL"  -- Size of "RIB_PriceZone_TBL" is unbounded
) return self as result
,constructor function "RIB_PrcZoneGrpDesc_REC"
(
  rib_oid number
, zone_group_id number
, PriceZone_TBL "RIB_PriceZone_TBL"  -- Size of "RIB_PriceZone_TBL" is unbounded
, pricing_level varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PrcZoneGrpDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PrcZoneGrpDesc') := "ns_name_PrcZoneGrpDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'zone_group_id') := zone_group_id;
  l_new_pre :=i_prefix||'PriceZone_TBL.';
  FOR INDX IN PriceZone_TBL.FIRST()..PriceZone_TBL.LAST() LOOP
    PriceZone_TBL(indx).appendNodeValues( i_prefix||indx||'PriceZone_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
  rib_obj_util.g_RIB_element_values(i_prefix||'pricing_level') := pricing_level;
END appendNodeValues;
constructor function "RIB_PrcZoneGrpDesc_REC"
(
  rib_oid number
, zone_group_id number
, PriceZone_TBL "RIB_PriceZone_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.zone_group_id := zone_group_id;
self.PriceZone_TBL := PriceZone_TBL;
RETURN;
end;
constructor function "RIB_PrcZoneGrpDesc_REC"
(
  rib_oid number
, zone_group_id number
, PriceZone_TBL "RIB_PriceZone_TBL"
, pricing_level varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.zone_group_id := zone_group_id;
self.PriceZone_TBL := PriceZone_TBL;
self.pricing_level := pricing_level;
RETURN;
end;
END;
/
