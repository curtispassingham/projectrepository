DROP TYPE "RIB_LoyInvcDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LoyInvcDesc_REC";
/
DROP TYPE "RIB_LineItemPoints_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemPoints_REC";
/
DROP TYPE "RIB_TotalPoints_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_TotalPoints_REC";
/
DROP TYPE "RIB_LineItemPoints_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_LineItemPoints_TBL" AS TABLE OF "RIB_LineItemPoints_REC";
/
DROP TYPE "RIB_LoyInvcDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LoyInvcDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyInvcDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  transaction_id varchar2(128),
  customer_order_id varchar2(48),
  loyalty_account_id varchar2(32),
  loyalty_program_id varchar2(32),
  loyalty_program_level_id number(2),
  LineItemPoints_TBL "RIB_LineItemPoints_TBL",   -- Size of "RIB_LineItemPoints_TBL" is 999
  TotalPoints "RIB_TotalPoints_REC",
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LoyInvcDesc_REC"
(
  rib_oid number
, transaction_id varchar2
, customer_order_id varchar2
, loyalty_account_id varchar2
, loyalty_program_id varchar2
, loyalty_program_level_id number
, LineItemPoints_TBL "RIB_LineItemPoints_TBL"  -- Size of "RIB_LineItemPoints_TBL" is 999
, TotalPoints "RIB_TotalPoints_REC"
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LoyInvcDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyInvcDesc') := "ns_name_LoyInvcDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'customer_order_id') := customer_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loyalty_account_id') := loyalty_account_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loyalty_program_id') := loyalty_program_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'loyalty_program_level_id') := loyalty_program_level_id;
  IF LineItemPoints_TBL IS NOT NULL THEN
    l_new_pre :=i_prefix||'LineItemPoints_TBL.';
    FOR INDX IN LineItemPoints_TBL.FIRST()..LineItemPoints_TBL.LAST() LOOP
      LineItemPoints_TBL(indx).appendNodeValues( i_prefix||indx||'LineItemPoints_TBL.');
      RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
    END LOOP;
  END IF;
  l_new_pre :=i_prefix||'TotalPoints.';
  TotalPoints.appendNodeValues( i_prefix||'TotalPoints');
  RIB_obj_util.g_RIB_table_names(l_new_pre) := 1;
END appendNodeValues;
constructor function "RIB_LoyInvcDesc_REC"
(
  rib_oid number
, transaction_id varchar2
, customer_order_id varchar2
, loyalty_account_id varchar2
, loyalty_program_id varchar2
, loyalty_program_level_id number
, LineItemPoints_TBL "RIB_LineItemPoints_TBL"
, TotalPoints "RIB_TotalPoints_REC"
) return self as result is
begin
self.rib_oid := rib_oid;
self.transaction_id := transaction_id;
self.customer_order_id := customer_order_id;
self.loyalty_account_id := loyalty_account_id;
self.loyalty_program_id := loyalty_program_id;
self.loyalty_program_level_id := loyalty_program_level_id;
self.LineItemPoints_TBL := LineItemPoints_TBL;
self.TotalPoints := TotalPoints;
RETURN;
end;
END;
/
DROP TYPE "RIB_LineItemPoints_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_LineItemPoints_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyInvcDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  line_index number(3),
  line_item_id varchar2(25),
  earned number(9),
  escrow number(9),
  bonus number(9),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
) return self as result
,constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
, escrow number
) return self as result
,constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
, escrow number
, bonus number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_LineItemPoints_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyInvcDesc') := "ns_name_LoyInvcDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'line_index') := line_index;
  rib_obj_util.g_RIB_element_values(i_prefix||'line_item_id') := line_item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'earned') := earned;
  rib_obj_util.g_RIB_element_values(i_prefix||'escrow') := escrow;
  rib_obj_util.g_RIB_element_values(i_prefix||'bonus') := bonus;
END appendNodeValues;
constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_index := line_index;
self.line_item_id := line_item_id;
self.earned := earned;
RETURN;
end;
constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
, escrow number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_index := line_index;
self.line_item_id := line_item_id;
self.earned := earned;
self.escrow := escrow;
RETURN;
end;
constructor function "RIB_LineItemPoints_REC"
(
  rib_oid number
, line_index number
, line_item_id varchar2
, earned number
, escrow number
, bonus number
) return self as result is
begin
self.rib_oid := rib_oid;
self.line_index := line_index;
self.line_item_id := line_item_id;
self.earned := earned;
self.escrow := escrow;
self.bonus := bonus;
RETURN;
end;
END;
/
DROP TYPE "RIB_TotalPoints_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_TotalPoints_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_LoyInvcDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  earned number(9),
  escrow number(9),
  bonus number(9),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
) return self as result
,constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
, escrow number
) return self as result
,constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_TotalPoints_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_LoyInvcDesc') := "ns_name_LoyInvcDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'earned') := earned;
  rib_obj_util.g_RIB_element_values(i_prefix||'escrow') := escrow;
  rib_obj_util.g_RIB_element_values(i_prefix||'bonus') := bonus;
END appendNodeValues;
constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
RETURN;
end;
constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
, escrow number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
RETURN;
end;
constructor function "RIB_TotalPoints_REC"
(
  rib_oid number
, earned number
, escrow number
, bonus number
) return self as result is
begin
self.rib_oid := rib_oid;
self.earned := earned;
self.escrow := escrow;
self.bonus := bonus;
RETURN;
end;
END;
/
