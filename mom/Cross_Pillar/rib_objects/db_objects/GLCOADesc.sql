DROP TYPE "RIB_GLCOADesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_GLCOADesc_REC";
/
DROP TYPE "RIB_GLCOADesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_GLCOADesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_GLCOADesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  primary_account varchar2(25),
  attribute1 varchar2(25),
  attribute2 varchar2(25),
  attribute3 varchar2(25),
  attribute4 varchar2(25),
  attribute5 varchar2(25),
  attribute6 varchar2(25),
  attribute7 varchar2(25),
  attribute8 varchar2(25),
  attribute9 varchar2(25),
  attribute10 varchar2(25),
  attribute11 varchar2(25),
  attribute12 varchar2(25),
  attribute13 varchar2(25),
  attribute14 varchar2(25),
  attribute15 varchar2(25),
  description1 varchar2(50),
  description2 varchar2(50),
  description3 varchar2(50),
  description4 varchar2(50),
  description5 varchar2(50),
  description6 varchar2(50),
  description7 varchar2(50),
  description8 varchar2(50),
  description9 varchar2(50),
  description10 varchar2(50),
  description11 varchar2(50),
  description12 varchar2(50),
  description13 varchar2(50),
  description14 varchar2(50),
  description15 varchar2(50),
  set_of_books_id number(15),
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
, description15 varchar2
) return self as result
,constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
, description15 varchar2
, set_of_books_id number
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_GLCOADesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_GLCOADesc') := "ns_name_GLCOADesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'primary_account') := primary_account;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute1') := attribute1;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute2') := attribute2;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute3') := attribute3;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute4') := attribute4;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute5') := attribute5;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute6') := attribute6;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute7') := attribute7;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute8') := attribute8;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute9') := attribute9;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute10') := attribute10;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute11') := attribute11;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute12') := attribute12;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute13') := attribute13;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute14') := attribute14;
  rib_obj_util.g_RIB_element_values(i_prefix||'attribute15') := attribute15;
  rib_obj_util.g_RIB_element_values(i_prefix||'description1') := description1;
  rib_obj_util.g_RIB_element_values(i_prefix||'description2') := description2;
  rib_obj_util.g_RIB_element_values(i_prefix||'description3') := description3;
  rib_obj_util.g_RIB_element_values(i_prefix||'description4') := description4;
  rib_obj_util.g_RIB_element_values(i_prefix||'description5') := description5;
  rib_obj_util.g_RIB_element_values(i_prefix||'description6') := description6;
  rib_obj_util.g_RIB_element_values(i_prefix||'description7') := description7;
  rib_obj_util.g_RIB_element_values(i_prefix||'description8') := description8;
  rib_obj_util.g_RIB_element_values(i_prefix||'description9') := description9;
  rib_obj_util.g_RIB_element_values(i_prefix||'description10') := description10;
  rib_obj_util.g_RIB_element_values(i_prefix||'description11') := description11;
  rib_obj_util.g_RIB_element_values(i_prefix||'description12') := description12;
  rib_obj_util.g_RIB_element_values(i_prefix||'description13') := description13;
  rib_obj_util.g_RIB_element_values(i_prefix||'description14') := description14;
  rib_obj_util.g_RIB_element_values(i_prefix||'description15') := description15;
  rib_obj_util.g_RIB_element_values(i_prefix||'set_of_books_id') := set_of_books_id;
END appendNodeValues;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
self.description12 := description12;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
self.description12 := description12;
self.description13 := description13;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
self.description12 := description12;
self.description13 := description13;
self.description14 := description14;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
, description15 varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
self.description12 := description12;
self.description13 := description13;
self.description14 := description14;
self.description15 := description15;
RETURN;
end;
constructor function "RIB_GLCOADesc_REC"
(
  rib_oid number
, primary_account varchar2
, attribute1 varchar2
, attribute2 varchar2
, attribute3 varchar2
, attribute4 varchar2
, attribute5 varchar2
, attribute6 varchar2
, attribute7 varchar2
, attribute8 varchar2
, attribute9 varchar2
, attribute10 varchar2
, attribute11 varchar2
, attribute12 varchar2
, attribute13 varchar2
, attribute14 varchar2
, attribute15 varchar2
, description1 varchar2
, description2 varchar2
, description3 varchar2
, description4 varchar2
, description5 varchar2
, description6 varchar2
, description7 varchar2
, description8 varchar2
, description9 varchar2
, description10 varchar2
, description11 varchar2
, description12 varchar2
, description13 varchar2
, description14 varchar2
, description15 varchar2
, set_of_books_id number
) return self as result is
begin
self.rib_oid := rib_oid;
self.primary_account := primary_account;
self.attribute1 := attribute1;
self.attribute2 := attribute2;
self.attribute3 := attribute3;
self.attribute4 := attribute4;
self.attribute5 := attribute5;
self.attribute6 := attribute6;
self.attribute7 := attribute7;
self.attribute8 := attribute8;
self.attribute9 := attribute9;
self.attribute10 := attribute10;
self.attribute11 := attribute11;
self.attribute12 := attribute12;
self.attribute13 := attribute13;
self.attribute14 := attribute14;
self.attribute15 := attribute15;
self.description1 := description1;
self.description2 := description2;
self.description3 := description3;
self.description4 := description4;
self.description5 := description5;
self.description6 := description6;
self.description7 := description7;
self.description8 := description8;
self.description9 := description9;
self.description10 := description10;
self.description11 := description11;
self.description12 := description12;
self.description13 := description13;
self.description14 := description14;
self.description15 := description15;
self.set_of_books_id := set_of_books_id;
RETURN;
end;
END;
/
