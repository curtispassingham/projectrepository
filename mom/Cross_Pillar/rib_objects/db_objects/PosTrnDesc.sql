DROP TYPE "RIB_PosTrnDesc_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PosTrnDesc_REC";
/
DROP TYPE "RIB_PosTrnItm_REC" FORCE;
CREATE OR REPLACE TYPE "RIB_PosTrnItm_REC";
/
DROP TYPE "RIB_PosTrnItm_TBL" FORCE;
CREATE OR REPLACE TYPE "RIB_PosTrnItm_TBL" AS TABLE OF "RIB_PosTrnItm_REC";
/
DROP TYPE "RIB_PosTrnDesc_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PosTrnDesc_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PosTrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_top" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  store_id number(10),
  transaction_id varchar2(128),
  transaction_timestamp date,
  cust_order_id varchar2(128),
  cust_order_comment varchar2(512),
  PosTrnItm_TBL "RIB_PosTrnItm_TBL",   -- Size of "RIB_PosTrnItm_TBL" is 999
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PosTrnDesc_REC"
(
  rib_oid number
, store_id number
, transaction_id varchar2
, transaction_timestamp date
, cust_order_id varchar2
, cust_order_comment varchar2
, PosTrnItm_TBL "RIB_PosTrnItm_TBL"  -- Size of "RIB_PosTrnItm_TBL" is 999
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PosTrnDesc_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PosTrnDesc') := "ns_name_PosTrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_top') := "ns_level_top";
  rib_obj_util.g_RIB_element_values(i_prefix||'store_id') := store_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_id') := transaction_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_timestamp') := transaction_timestamp;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_id') := cust_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'cust_order_comment') := cust_order_comment;
  l_new_pre :=i_prefix||'PosTrnItm_TBL.';
  FOR INDX IN PosTrnItm_TBL.FIRST()..PosTrnItm_TBL.LAST() LOOP
    PosTrnItm_TBL(indx).appendNodeValues( i_prefix||indx||'PosTrnItm_TBL.');
    RIB_obj_util.g_RIB_table_names(l_new_pre) := indx;
  END LOOP;
END appendNodeValues;
constructor function "RIB_PosTrnDesc_REC"
(
  rib_oid number
, store_id number
, transaction_id varchar2
, transaction_timestamp date
, cust_order_id varchar2
, cust_order_comment varchar2
, PosTrnItm_TBL "RIB_PosTrnItm_TBL"
) return self as result is
begin
self.rib_oid := rib_oid;
self.store_id := store_id;
self.transaction_id := transaction_id;
self.transaction_timestamp := transaction_timestamp;
self.cust_order_id := cust_order_id;
self.cust_order_comment := cust_order_comment;
self.PosTrnItm_TBL := PosTrnItm_TBL;
RETURN;
end;
END;
/
DROP TYPE "RIB_PosTrnItm_REC" FORCE ;
CREATE OR REPLACE TYPE "RIB_PosTrnItm_REC" UNDER RIB_OBJECT (
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are infrastructure variables which will be used by the RIB internally for constructing the namespace of the payload. 
   --
   -------------------------------------------------------------------------------------------------------------------------------------------- 
  "ns_version_v1" varchar2(1), -- This variable(ns_type_<version no>) is used to identify the version of a retail domain object.
  "ns_name_PosTrnDesc" varchar2(1), -- This variable(ns_name_<xyz>) is used to identify the current type name or parent type name of a retail domain object.
  "ns_type_bo" varchar2(1), -- This variable(ns_type_<bo or bm>) is used to identify the type or category of a retail domain object.
  "ns_location_base" varchar2(1), -- This variable(ns_location_<custom or base>) is used to identify the location of a retail domain object.
  "ns_level_nontop" varchar2(1), -- This variable(ns_level_<top or nontop>) is used to identify the level of a retail domain object.
   --------------------------------------------------------------------------------------------------------------------------------------------
   -- 
   --  These variables are the payload variables which are used to construct the payload. 
   --
   --------------------------------------------------------------------------------------------------------------------------------------------  
  item_id varchar2(25),
  quantity number(12,4),
  unit_of_measure varchar2(4),
  uin varchar2(128),
  reason_code number(4),
  drop_ship varchar2(5), --drop_ship is boolean field, valid values are true,false (all lower-case) 
  comments varchar2(512),
  fulfill_order_id varchar2(48),
  reservation_type varchar2(25), -- reservation_type is enumeration field, valid values are [WEB_ORDER, SPECIAL_ORDER, PICKUP_OR_DELIVERY, LAYAWAY, ON_HOLD, NO_VALUE] (all lower-case)
  transaction_code varchar2(25), -- transaction_code is enumeration field, valid values are [SALE, RETURN, VOID_SALE, VOID_RETURN, ORDER_NEW, ORDER_FULFILL, ORDER_CANCEL] (all lower-case)
  OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
,constructor function "RIB_PosTrnItm_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, unit_of_measure varchar2
, uin varchar2
, reason_code number
, drop_ship varchar2  --drop_ship is boolean field, valid values are true,false (all lower-case)
, comments varchar2
, fulfill_order_id varchar2
, reservation_type varchar2
, transaction_code varchar2
) return self as result
);
/
CREATE OR REPLACE TYPE BODY "RIB_PosTrnItm_REC" AS
OVERRIDING MEMBER PROCEDURE appendNodeValues( i_prefix IN VARCHAR2)
IS
  l_new_pre varchar2(4000);
BEGIN
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_version_v1') := "ns_version_v1";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_name_PosTrnDesc') := "ns_name_PosTrnDesc";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_type_bo') := "ns_type_bo";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_location_base') := "ns_location_base";
  rib_obj_util.g_RIB_element_values(i_prefix||'ns_level_nontop') := "ns_level_nontop";
  rib_obj_util.g_RIB_element_values(i_prefix||'item_id') := item_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'quantity') := quantity;
  rib_obj_util.g_RIB_element_values(i_prefix||'unit_of_measure') := unit_of_measure;
  rib_obj_util.g_RIB_element_values(i_prefix||'uin') := uin;
  rib_obj_util.g_RIB_element_values(i_prefix||'reason_code') := reason_code;
  rib_obj_util.g_RIB_element_values(i_prefix||'drop_ship') := drop_ship;
  rib_obj_util.g_RIB_element_values(i_prefix||'comments') := comments;
  rib_obj_util.g_RIB_element_values(i_prefix||'fulfill_order_id') := fulfill_order_id;
  rib_obj_util.g_RIB_element_values(i_prefix||'reservation_type') := reservation_type;
  rib_obj_util.g_RIB_element_values(i_prefix||'transaction_code') := transaction_code;
END appendNodeValues;
constructor function "RIB_PosTrnItm_REC"
(
  rib_oid number
, item_id varchar2
, quantity number
, unit_of_measure varchar2
, uin varchar2
, reason_code number
, drop_ship varchar2
, comments varchar2
, fulfill_order_id varchar2
, reservation_type varchar2
, transaction_code varchar2
) return self as result is
begin
self.rib_oid := rib_oid;
self.item_id := item_id;
self.quantity := quantity;
self.unit_of_measure := unit_of_measure;
self.uin := uin;
self.reason_code := reason_code;
self.drop_ship := drop_ship;
self.comments := comments;
self.fulfill_order_id := fulfill_order_id;
self.reservation_type := reservation_type;
self.transaction_code := transaction_code;
RETURN;
end;
END;
/
