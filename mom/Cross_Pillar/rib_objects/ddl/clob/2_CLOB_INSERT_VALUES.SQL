set feedback on termout on verify off heading off pagesize 0 echo off recsep off

prompt ;
prompt ********************************************************************************;
prompt * 2_CLOB_INSERT_VALUES.SQL  BEGIN                                              *;
prompt ********************************************************************************;
prompt * This script will insert values into the following tables:                    *;
prompt *    rib_options                                                               *;
prompt *    rib_settings                                                              *;
prompt *    rib_type_settings                                                         *;
prompt *                                                                              *;
prompt * WARNING: All existing data on the table will be deleted. You are responsible *;
prompt * for backing up and restoring any existing data if required.                  *;
prompt *                                                                              *;
prompt ********************************************************************************;
prompt ;
prompt ;


--------------------------------------------------------------------------------
-- Bind variables.  These will be set in the PL/SQL code chunk below.
--------------------------------------------------------------------------------
VARIABLE XML_SCHEMA_BASE_URL_1  VARCHAR2(400);
VARIABLE XML_NAMESPACE_URL_1    VARCHAR2(400);
VARIABLE XML_XSI_URL_1          VARCHAR2(400);


--------------------------------------------------------------------------------
-- Substitution variables.  These are the default values.
--------------------------------------------------------------------------------
DEFINE XML_SCHEMA_BASE_URL_DEFAULT = 'http://mspdev81:7777/rib-func-artifact/';
DEFINE XML_NAMESPACE_URL_DEFAULT   = 'http://www.oracle.com/retail/integration/base/bo';
DEFINE XML_XSI_URL_DEFAULT         = 'http://www.w3.org/2001/XMLSchema-instance';


--------------------------------------------------------------------------------
-- Interact with user and set values.
--------------------------------------------------------------------------------
prompt --------------------------------------------------------------------------------;
prompt Enter a URL value for XML_SCHEMA_BASE_URL, or press ENTER to accept the default.;
prompt ;
prompt Default: &XML_SCHEMA_BASE_URL_DEFAULT;
prompt ;
-- accept XML_SCHEMA_BASE_URL_USER CHAR prompt 'XML_SCHEMA_BASE_URL = ';
prompt ;
prompt ;
prompt --------------------------------------------------------------------------------;
prompt Enter a URL value for XML_NAMESPACE_URL, or press ENTER to accept the default.;
prompt ;
prompt Default: &XML_NAMESPACE_URL_DEFAULT;
prompt ;
-- accept XML_NAMESPACE_URL_USER CHAR prompt 'XML_NAMESPACE_URL = ';
prompt ;
prompt ;
prompt --------------------------------------------------------------------------------;
prompt Enter a URL value for XML_XSI_URL, or press ENTER to accept the default.;
prompt ;
prompt Default: &XML_XSI_URL_DEFAULT;
prompt ;
-- accept XML_XSI_URL_USER CHAR prompt 'XML_XSI_URL = ';
prompt ;
prompt ;
define XML_SCHEMA_BASE_URL_USER = &1;
define XML_NAMESPACE_URL_USER = &2;
define XML_XSI_URL_USER = &3;


--------------------------------------------------------------------------------
-- This bit of PL/SQL code is used to set the bind variables.
--------------------------------------------------------------------------------
DECLARE
BEGIN
   :XML_NAMESPACE_URL_1   := NVL('&XML_NAMESPACE_URL_USER'  ,'&XML_NAMESPACE_URL_DEFAULT');
   :XML_SCHEMA_BASE_URL_1 := NVL('&XML_SCHEMA_BASE_URL_USER'  ,'&XML_SCHEMA_BASE_URL_DEFAULT');
   :XML_XSI_URL_1         := NVL('&XML_XSI_URL_USER'  ,'&XML_XSI_URL_DEFAULT');
END;
/


--------------------------------------------------------------------------------
-- Copy the bind variables into substitution variables which are then used in
-- the table creation scripts.
--------------------------------------------------------------------------------
set termout off;

column :XML_SCHEMA_BASE_URL_1 new_value XML_SCHEMA_BASE_URL
    select :XML_SCHEMA_BASE_URL_1 from DUAL;

column :XML_NAMESPACE_URL_1 new_value XML_NAMESPACE_URL
    select :XML_NAMESPACE_URL_1 from DUAL;

column :XML_XSI_URL_1 new_value XML_XSI_URL
    select :XML_XSI_URL_1 from DUAL;

set termout on;
--------------------------------------------------------------------------------


prompt ;
prompt --------------------------------------------------------------------------------;
prompt ;
prompt XML_SCHEMA_BASE_URL = &XML_SCHEMA_BASE_URL;
prompt XML_NAMESPACE_URL   = &XML_NAMESPACE_URL;
prompt XML_XSI_URL         = &XML_XSI_URL;
prompt ;
prompt If these values are correct, press ENTER to continue. If any of these values are;
prompt incorrect, kill this script or session now then re-run this script to set the
prompt correct values before continuing with creation/insertion scripts.
prompt ;
-- pause <ENTER TO CONTINUE>;
prompt ;
prompt Continuing...;
prompt ;


prompt --------------------------------------------------------------------------------;
prompt Delete RIB_OPTIONS;
prompt --------------------------------------------------------------------------------;
delete rib_options;


prompt --------------------------------------------------------------------------------;
prompt Insert RIB_OPTIONS values;
prompt --------------------------------------------------------------------------------;
insert into rib_options ( XML_NAMESPACE_URL
                        , XML_SCHEMA_BASE_URL
                        , XML_XSI_URL)
                 values ( '&XML_NAMESPACE_URL'
                        , '&XML_SCHEMA_BASE_URL'
                        , '&XML_XSI_URL');


prompt --------------------------------------------------------------------------------;
prompt Delete RIB_TYPE_SETTINGS;
prompt --------------------------------------------------------------------------------;
delete rib_type_settings;

prompt --------------------------------------------------------------------------------;
prompt Delete RIB_SETTINGS;
prompt --------------------------------------------------------------------------------;
delete rib_settings;


prompt --------------------------------------------------------------------------------;
prompt Insert RIB_SETTINGS values;
prompt --------------------------------------------------------------------------------;

DECLARE
   --
   TYPE TEMP_TAB IS TABLE OF RIB_SETTINGS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_table          TEMP_TAB;
   L_index          NUMBER(6) := 0;
   --
   PROCEDURE NEW_REC(I_FAMILY                  IN  RIB_SETTINGS.FAMILY%TYPE,
                     I_MAX_DETAILS_TO_PUBLISH  IN  RIB_SETTINGS.MAX_DETAILS_TO_PUBLISH%TYPE DEFAULT 1000,
                     I_NUM_THREADS             IN  RIB_SETTINGS.NUM_THREADS%TYPE            DEFAULT 1,
                     I_MINUTES_TIME_LAG        IN  RIB_SETTINGS.MINUTES_TIME_LAG%TYPE       DEFAULT 0)
   IS
   BEGIN
      L_table(L_index).family                 := I_FAMILY;
      L_table(L_index).max_details_to_publish := I_MAX_DETAILS_TO_PUBLISH;
      L_table(L_index).num_threads            := I_NUM_THREADS;
      L_table(L_index).minutes_time_lag       := I_MINUTES_TIME_LAG;
      L_index                                 := L_index + 1;
   END;
   --
BEGIN

   NEW_REC('dsdreceipt');
   NEW_REC('alloc');
   NEW_REC('asnin');
   NEW_REC('asnout');
   NEW_REC('banner');
   NEW_REC('carriers');
   NEW_REC('codes');  
   NEW_REC('currate');
   NEW_REC('custorder');
   NEW_REC('custreturn');
   NEW_REC('diffgrp');
   NEW_REC('diffs');
   NEW_REC('emp');
   NEW_REC('emptime');
   NEW_REC('frtterm');
   NEW_REC('glcoa');
   NEW_REC('invadjust');
   NEW_REC('itemprice');
   NEW_REC('items');
   NEW_REC('loc');
   NEW_REC('itemloc');
   NEW_REC('order');
   NEW_REC('orderphys');
   NEW_REC('payterm');
   NEW_REC('pendreturn');
   NEW_REC('receiving');
   NEW_REC('rtv');
   NEW_REC('rtvreq');
   NEW_REC('sellitem');
   NEW_REC('shipmeth');
   NEW_REC('skuoptm');
   NEW_REC('sostatus');
   NEW_REC('spacelocs');
   NEW_REC('stockorder');
   NEW_REC('stores');
   NEW_REC('transfers');
   NEW_REC('udas');
   NEW_REC('venavl');
   NEW_REC('vendor');
   NEW_REC('wh');
   NEW_REC('whtrans');
   NEW_REC('woin');
   NEW_REC('woint');
   NEW_REC('woout');
   NEW_REC('wostatus');
   NEW_REC('orghier');
   NEW_REC('correspondence');
   NEW_REC('prczonegrp');
   NEW_REC('partner');
   NEW_REC('poschedule');
   NEW_REC('season');
   NEW_REC('seedobj');
   NEW_REC('merchhier');
   NEW_REC('rcvunitadj');
   NEW_REC('seeddata');
   NEW_REC('dlvyslt');
   NEW_REC('fulfilord');
   NEW_REC('fulfilordcfm');


   -- Insert new records
   forall i in L_table.FIRST..L_table.LAST
      insert into rib_settings values L_table(i);

END;
/

prompt --------------------------------------------------------------------------------;
prompt Insert RIB_TYPE_SETTINGS values;
prompt --------------------------------------------------------------------------------;
DECLARE
   --
   TYPE TEMP_TAB IS TABLE OF RIB_TYPE_SETTINGS%ROWTYPE INDEX BY BINARY_INTEGER;
   L_table          TEMP_TAB;
   L_index          NUMBER(6) := 0;
   --
   PROCEDURE NEW_REC(I_FAMILY          IN  RIB_TYPE_SETTINGS.FAMILY%TYPE,
                     I_TYPE            IN  RIB_TYPE_SETTINGS.TYPE%TYPE,
                     I_DO_DBMS_OUTPUT  IN  RIB_TYPE_SETTINGS.DO_DBMS_OUTPUT%TYPE  DEFAULT 'N',
                     I_DO_LOG_OUTPUT   IN  RIB_TYPE_SETTINGS.DO_LOG_OUTPUT%TYPE   DEFAULT 'N')
   IS
   BEGIN
      L_table(L_index).family         := I_FAMILY;
      L_table(L_index).type           := I_TYPE;
      L_table(L_index).do_dbms_output := I_DO_DBMS_OUTPUT;
      L_table(L_index).do_log_output  := I_DO_LOG_OUTPUT;
      L_index                         := L_index + 1;
   END;
   --
BEGIN

   NEW_REC('wh', 'whdel');
   NEW_REC('wh', 'whmod');
   NEW_REC('alloc', 'allocdtldel');
   NEW_REC('alloc', 'allochdrmod');
   NEW_REC('asnin', 'asnincre');
   NEW_REC('alloc', 'allocdtlcre');
   NEW_REC('asnin', 'asninmod');
   NEW_REC('dsdreceipt', 'dsdreceiptcre');
   NEW_REC('order', 'pocre');
   NEW_REC('receiving', 'otbmod');
   NEW_REC('receiving', 'appointdtlcre');
   NEW_REC('receiving', 'appointdtldel');
   NEW_REC('receiving', 'appointdtlmod');
   NEW_REC('receiving', 'appointhdrmod');
   NEW_REC('banner', 'BanDlvScdCre');
   NEW_REC('banner', 'BanDlvScdDel');
   NEW_REC('banner', 'BanDlvScdMod');
   NEW_REC('banner', 'BannerCre');
   NEW_REC('banner', 'BannerDel');
   NEW_REC('banner', 'BannerMod');
   NEW_REC('codes', 'CodeDtlCre');
   NEW_REC('codes', 'CodeDtlDel');
   NEW_REC('codes', 'CodeDtlMod');
   NEW_REC('codes', 'CodeHdrCre');
   NEW_REC('codes', 'CodeHdrDel');
   NEW_REC('codes', 'CodeHdrMod');
   NEW_REC('emptime', 'EmpTimeCre');
   NEW_REC('emptime', 'EmpTimeMod');
   NEW_REC('emp', 'EmployeeCre');
   NEW_REC('emp', 'EmployeeDel');
   NEW_REC('emp', 'EmployeeMod');
   NEW_REC('items', 'IBomRecipeCre');
   NEW_REC('items', 'IBomRecipeDel');
   NEW_REC('items', 'IBomRecipeMod');
   NEW_REC('items', 'ISAttrCre');
   NEW_REC('items', 'ISAttrDel');
   NEW_REC('items', 'ISAttrMod');
   NEW_REC('items', 'ISDlvBlkCre');
   NEW_REC('items', 'ISDlvBlkMod');
   NEW_REC('items', 'ISDlvDlkDel');
   NEW_REC('items', 'ISPerAttrCre');
   NEW_REC('items', 'ISPerAttrDel');
   NEW_REC('items', 'ISPerAttrMod');
   NEW_REC('items', 'ISPerFnClCre');
   NEW_REC('items', 'ISPerFnClDel');
   NEW_REC('items', 'ISPerFnClMod');
   NEW_REC('items', 'ISPerMxChrCre');
   NEW_REC('items', 'ISPerMxChrDel');
   NEW_REC('items', 'ISPerMxChrMod');
   NEW_REC('items', 'IShipRsCre');
   NEW_REC('items', 'IShipRsDtlCre');
   NEW_REC('items', 'IShipRsDtlDel');
   NEW_REC('items', 'IShipRsDtlMod');
   NEW_REC('items', 'IShipRsHdrCre');
   NEW_REC('items', 'IShipRsHdrDel');
   NEW_REC('items', 'IShipRsHdrMod');
   NEW_REC('items', 'ItemAttrCre');
   NEW_REC('items', 'ItemAttrDel');
   NEW_REC('items', 'ItemAttrMod');
   NEW_REC('items', 'ItemLocCre');
   NEW_REC('items', 'ItemLocDel');
   NEW_REC('items', 'ItemLocMod');
   NEW_REC('items', 'ItemLocsCre');
   NEW_REC('items', 'ItemLocsMod');
   NEW_REC('items', 'ItmCarrSvcCre');
   NEW_REC('items', 'ItmCarrSvcDel');
   NEW_REC('items', 'ItmCarrSvcMod');
   NEW_REC('items', 'ItmLocAttrCre');
   NEW_REC('items', 'ItmLocAttrDel');
   NEW_REC('items', 'ItmLocAttrMod');
   NEW_REC('items', 'ISCMfrCre');
   NEW_REC('items', 'ISCMfrMod');
   NEW_REC('items', 'ISCMfrDel');
   NEW_REC('items', 'RelItemHeadCre');
   NEW_REC('items', 'RelItemHeadMod');
   NEW_REC('items', 'RelItemDetCre');
   NEW_REC('items', 'RelItemDetMod');
   NEW_REC('items', 'RelItemHeadDel');
   NEW_REC('items', 'RelItemDetDel');
   NEW_REC('venavl', 'VenAvlIDsCre');
   NEW_REC('venavl', 'VenAvlIDsDel');
   NEW_REC('venavl', 'VenAvlIDsMod');
   NEW_REC('venavl', 'VenConIDsCre');
   NEW_REC('venavl', 'VenConIDsDel');
   NEW_REC('venavl', 'VenConIDsMod');
   NEW_REC('wh', 'WHAttrCre');
   NEW_REC('wh', 'WHAttrDel');
   NEW_REC('wh', 'WHAttrMod');
   NEW_REC('whtrans', 'WHTransCre');
   NEW_REC('woint', 'WOIntCre');
   NEW_REC('woint', 'WOIntDel');
   NEW_REC('woint', 'WOIntMod');
   NEW_REC('wostatus', 'WOSInvAdjCre');
   NEW_REC('wostatus', 'WOStatHdrCre');
   NEW_REC('wostatus', 'WOStatHdrMod');
   NEW_REC('wostatus', 'WOStatusCre');
   NEW_REC('wostatus', 'WOStatusDel');
   NEW_REC('alloc', 'alloccre');
   NEW_REC('alloc', 'allocdel');
   NEW_REC('alloc', 'allocdtlmod');
   NEW_REC('receiving', 'appointcre');
   NEW_REC('receiving', 'appointdel');
   NEW_REC('asnin', 'asnindel');
   NEW_REC('asnout', 'asnoutcre');
   NEW_REC('asnout', 'asnoutmod');
   NEW_REC('banner', 'bannercre');
   NEW_REC('banner', 'bannerdel');
   NEW_REC('banner', 'bannermod');
   NEW_REC('banner', 'channelcre');
   NEW_REC('banner', 'channeldel');
   NEW_REC('banner', 'channelmod');
   NEW_REC('custorder', 'cocre');
   NEW_REC('custreturn', 'coretcre');
   NEW_REC('custreturn', 'coretdtlcre');
   NEW_REC('custreturn', 'corethdrcre');
   NEW_REC('currate', 'curratecre');      
   NEW_REC('diffs', 'diffcre');
   NEW_REC('diffs', 'diffdel');
   NEW_REC('diffgrp', 'diffgrpdel');
   NEW_REC('diffgrp', 'diffgrpdtlcre');
   NEW_REC('diffgrp', 'diffgrpdtldel');
   NEW_REC('diffgrp', 'diffgrpdtlmod');
   NEW_REC('diffgrp', 'diffgrphdrcre');
   NEW_REC('diffgrp', 'diffgrphdrmod');
   NEW_REC('diffs', 'diffmod');
   NEW_REC('frtterm', 'frttermcre');
   NEW_REC('glcoa', 'glcoacre');
   NEW_REC('woin', 'inbdwocre');
   NEW_REC('woin', 'inbdwodel');
   NEW_REC('woin', 'inbdwomod');
   NEW_REC('invadjust', 'invadjustcre');
   NEW_REC('items', 'iscdimcre');
   NEW_REC('items', 'iscdimdel');
   NEW_REC('items', 'iscdimmod');
   NEW_REC('items', 'itembomcre');
   NEW_REC('items', 'itembomdel');
   NEW_REC('items', 'itembommod');
   NEW_REC('items', 'itemcre');
   NEW_REC('items', 'itemdel');
   NEW_REC('items', 'itemhdrmod');
   NEW_REC('items', 'itemimagecre');
   NEW_REC('items', 'itemimagedel');
   NEW_REC('items', 'itemimagemod');
   NEW_REC('items', 'itemsupcre');
   NEW_REC('items', 'itemsupctycre');
   NEW_REC('items', 'itemsupctydel');
   NEW_REC('items', 'itemsupctymod');
   NEW_REC('items', 'itemsupdel');
   NEW_REC('items', 'itemsupmod');
   NEW_REC('items', 'itemudadatecre');
   NEW_REC('items', 'itemudadatedel');
   NEW_REC('items', 'itemudadatemod');
   NEW_REC('items', 'itemudaffcre');
   NEW_REC('items', 'itemudaffdel');
   NEW_REC('items', 'itemudaffmod');
   NEW_REC('items', 'itemudalovcre');
   NEW_REC('items', 'itemudalovdel');
   NEW_REC('items', 'itemudalovmod');
   NEW_REC('items', 'itemupccre');
   NEW_REC('items', 'itemupcdel');
   NEW_REC('items', 'itemupcmod');
   NEW_REC('loc', 'locationcre');
   NEW_REC('loc', 'locationdel');
   NEW_REC('loc', 'locationmod');
   NEW_REC('woout', 'outbdwocre');
   NEW_REC('woout', 'outbdwodel');
   NEW_REC('woout', 'outbdwomod');
   NEW_REC('payterm', 'paytermcre');
   NEW_REC('pendreturn', 'pendretcre');
   NEW_REC('pendreturn', 'pendretdel');
   NEW_REC('pendreturn', 'pendretdtlcre');
   NEW_REC('pendreturn', 'pendretdtldel');
   NEW_REC('pendreturn', 'pendretdtlmod');
   NEW_REC('poschedule', 'poschedcre');
   NEW_REC('poschedule', 'poschedmod');
   NEW_REC('poschedule', 'poscheddel');
   NEW_REC('order', 'podel');
   NEW_REC('order', 'podtlcre');
   NEW_REC('order', 'podtldel');
   NEW_REC('order', 'podtlmod');
   NEW_REC('order', 'pohdrmod');
   NEW_REC('orderphys', 'pophyscre');
   NEW_REC('orderphys', 'pophysdel');
   NEW_REC('orderphys', 'pophysdtlcre');
   NEW_REC('orderphys', 'pophysdtldel');
   NEW_REC('orderphys', 'pophysdtlmod');
   NEW_REC('orderphys', 'pophyshdrmod');
   NEW_REC('receiving', 'receiptcre');
   NEW_REC('receiving', 'receiptmod');
   NEW_REC('receiving', 'receiptordcre');
   NEW_REC('rtv', 'rtvcre');
   NEW_REC('rtvreq', 'rtvreqcre');
   NEW_REC('rtvreq', 'rtvreqdel');
   NEW_REC('rtvreq', 'rtvreqmod');
   NEW_REC('shipmeth', 'shipmethcre');
   NEW_REC('shipmeth', 'shipmethdel');
   NEW_REC('shipmeth', 'shipmethmod');
   NEW_REC('skuoptm', 'skuoptmcre');
   NEW_REC('stockorder', 'socre');
   NEW_REC('stockorder', 'sodtlcre');
   NEW_REC('stockorder', 'sodtldel');
   NEW_REC('stockorder', 'sodtlmod');
   NEW_REC('stockorder', 'sohdrdel');
   NEW_REC('stockorder', 'sohdrmod');
   NEW_REC('sostatus', 'sostatuscre');
   NEW_REC('spacelocs', 'spacelocscre');
   NEW_REC('spacelocs', 'spacelocsdel');
   NEW_REC('spacelocs', 'spacelocsmod');
   NEW_REC('stores', 'storecre');
   NEW_REC('stores', 'storedel');
   NEW_REC('stores', 'storemod');
   NEW_REC('transfers', 'transfercre');
   NEW_REC('transfers', 'transferdel');
   NEW_REC('transfers', 'transferdtlcre');
   NEW_REC('transfers', 'transferdtldel');
   NEW_REC('transfers', 'transferdtlmod');
   NEW_REC('transfers', 'transferhdrmod');
   NEW_REC('udas', 'udahdrcre');
   NEW_REC('udas', 'udahdrdel');
   NEW_REC('udas', 'udahdrmod');
   NEW_REC('udas', 'udavalcre');
   NEW_REC('udas', 'udavaldel');
   NEW_REC('udas', 'udavalmod');
   NEW_REC('vendor', 'vendoraddrcre');
   NEW_REC('vendor', 'vendoraddrdel');
   NEW_REC('vendor', 'vendoraddrmod');
   NEW_REC('vendor', 'vendorcre');
   NEW_REC('vendor', 'vendordel');
   NEW_REC('vendor', 'vendorhdrmod');
   NEW_REC('vendor', 'vendoroucre');
   NEW_REC('vendor', 'vendoroumod');
   NEW_REC('vendor', 'vendoroudel');
   NEW_REC('wh', 'whcre');
   NEW_REC('prczonegrp', 'prczonegrpcre');
   NEW_REC('prczonegrp', 'prczonegrpmod');
   NEW_REC('prczonegrp', 'prczonegrpdel');
   NEW_REC('partner', 'partnercre');
   NEW_REC('partner', 'partnermod');
   NEW_REC('partner', 'partnerdel');
   NEW_REC('season', 'seasoncre');
   NEW_REC('season', 'seasonmod');
   NEW_REC('season', 'seasondel');
   NEW_REC('season', 'seasondtlcre');
   NEW_REC('season', 'seasondtlmod');
   NEW_REC('season', 'seasondtldel');
   NEW_REC('seedobj', 'countrycre');
   NEW_REC('seedobj', 'countrymod');
   NEW_REC('seedobj', 'countrydel');
   NEW_REC('seedobj', 'curratecre');
   NEW_REC('seedobj', 'curratemod');
   NEW_REC('merchhier', 'divisioncre');
   NEW_REC('merchhier', 'divisionmod');
   NEW_REC('merchhier', 'divisiondel');
   NEW_REC('merchhier', 'groupcre');
   NEW_REC('merchhier', 'groupmod');
   NEW_REC('merchhier', 'groupdel');
   NEW_REC('merchhier', 'deptcre');
   NEW_REC('merchhier', 'deptmod');
   NEW_REC('merchhier', 'deptdel');
   NEW_REC('merchhier', 'classcre');
   NEW_REC('merchhier', 'classmod');
   NEW_REC('merchhier', 'classdel');
   NEW_REC('merchhier', 'subclasscre');
   NEW_REC('merchhier', 'subclassmod');
   NEW_REC('merchhier', 'subclassdel');
   NEW_REC('dlvyslt','dlvysltcre');
   NEW_REC('dlvyslt','dlvysltmod');
   NEW_REC('dlvyslt','dlvysltdel');
   NEW_REC('fulfilord','fulfilordreqdel');
   NEW_REC('fulfilord','fulfilordapprdel');
   NEW_REC('fulfilord','fulfilordpocre');
   NEW_REC('fulfilord','fulfilordtsfcre');
   NEW_REC('fulfilord','fulfilordstdlvcre');
   NEW_REC('fulfilordcfm','fulfilordcfmcre');
   
   -- Insert new records
   forall i in L_table.FIRST..L_table.LAST
      insert into rib_type_settings values L_table(i);

END;
/


prompt --------------------------------------------------------------------------------;
prompt COMMIT;
prompt --------------------------------------------------------------------------------;
COMMIT;

prompt ;
prompt ********************************************************************************;
prompt * 2_CLOB_INSERT_VALUES.SQL  END                                                *;
prompt ********************************************************************************;
prompt ;
prompt ;

UNDEFINE XML_SCHEMA_BASE_URL_DEFAULT;
UNDEFINE XML_NAMESPACE_URL_DEFAULT;
UNDEFINE XML_XSI_URL_DEFAULT;
UNDEFINE XML_SCHEMA_BASE_URL_USER;
UNDEFINE XML_NAMESPACE_URL_USER;
UNDEFINE XML_XSI_URL_USER;
UNDEFINE XML_SCHEMA_BASE_URL;
UNDEFINE XML_NAMESPACE_URL;
UNDEFINE XML_XSI_URL;
