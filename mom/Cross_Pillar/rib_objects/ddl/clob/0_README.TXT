Run the following scripts in this (numbered) order.

1_CLOB_CREATE_OBJECTS.SQL
-------------------------
This script will create the following tables:
   RIB_OPTIONS
   RIB_SETTINGS
   RIB_TYPE_SETTINGS

The script requires user input to set the names of tablespace values.

NOTE: The 'DROP TABLE' section is commented out.  This script is to be used
for a new installation of these tables.  If these tables already exist in
your environment then you can comment in the 'DROP TABLE' section.

WARNING: All data will be lost if the tables are dropped. You are
responsible for backing up and restoring any existing data if required.



2_CLOB_INSERT_VALUES.SQL
------------------------
This script will insert into the following tables:
   RIB_OPTIONS
   RIB_SETTINGS
   RIB_TYPE_SETTINGS

WARNING: All existing data on the table will be deleted. You are responsible
for backing up and restoring any existing data if required.


3_CLOB_COMPILE.SQL
------------------
This script will compile the following:
   rib_options_sql.pks/pkb
   rib_sxw.pks/pkb
   rib_xmls/b.pls
