CREATE OR REPLACE package rib_xml as

--------------------------------------------------------------------------------
-- Create a new DOM document and return the root node of its tree.
-- PARAMETERS
--    root: The handle to be set with the index of the root element of
--          the tree.
--    name: The name of the root element in the tree.
--------------------------------------------------------------------------------
function newRoot(root  out  xmldom.DOMElement,
                 name  in   varchar2)
return boolean;

--------------------------------------------------------------------------------
-- Parse the document contained in the given CLOB.
-- PARAMETERS
--    message    : A CLOB containing the message.
--    messageName: The name of the message. This must match the name
--                 of the root element in the document.
--    prevalidate: TRUE if the parser should validate, FALSE otherwise
-- RETURNS
--    The handle to the root element of the document, or NO_HANDLE if
--    none is found.
--------------------------------------------------------------------------------
function readRoot(message      in  clob,
                  messageName  in  varchar2)
return xmldom.DOMElement;

--------------------------------------------------------------------------------
-- Serialize a DOM tree and return the result.
-- PARAMETERS
--    root       : The handle for the root node of the document to be serialized
--    data       : The CLOB to be filled with the text of the XML document.
--    prevalidate: If true, the document will be re-validated against its xsd
--                 and an exception will be thrown if there's an error.
--------------------------------------------------------------------------------
function writeRoot(root  in out  xmldom.DOMElement,
                   data  in out  clob)
return boolean;

--------------------------------------------------------------------------------
-- Release all resources associated with the document that owns the given
-- element. If readRoot() or newRoot() were called with the checkMRU parameter
-- set to false, this must be called in order to release any resources allocated
-- to the elements in the tree.
-- PARAMETERS
--    root: The handle for any node in the document.
--------------------------------------------------------------------------------
procedure freeRoot(root  in out  xmldom.DOMElement);

--------------------------------------------------------------------------------
-- Add a new element to the tree with a varchar value.
-- PARAMETERS
--    parentElem: The handle for the parent element.
--    name      : The name of the new element.
--    value     : The text content of the new element. If this is NULL then no
--                element will be added.
--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                     name        in  varchar2,
                     value       in  varchar2);

--------------------------------------------------------------------------------
-- Add a new element to the tree with a number value. The numeric values will
-- use an explicit number-to-varchar conversion specifying that the decimal
-- point will always be a period (.) in the string.
-- PARAMETERS
--    parentElem: The handle for the parent element.
--    name      : The name of the new element.
--    value     : The number content of the new element. If this is NULL then
--                no element will be added.
--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                     name        in  varchar2,
                     value       in  number);

--------------------------------------------------------------------------------
-- Overloaded addElement for empty tags.  When a tag is needed, and a value has
-- not yet been specified, this function will be used.
-- PARAMETERS
--    parentElem: The handle for the parent element.
--    name      : The name of the new element.
--------------------------------------------------------------------------------
function addElement(parentElem  in  xmldom.DOMElement,
                    name        in  varchar2)
return xmldom.DOMElement;

--------------------------------------------------------------------------------
-- Overloaded addElement for empty tags.  When a tag is needed, and a value has
-- not yet been specified, this function will be used.
-- PARAMETERS
--    parentElem: The handle for the parent element.
--    name      : The name of the new element.
--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                        name     in  varchar2);

--------------------------------------------------------------------------------
-- Add an element containing date information to the tree.
-- PARAMETERS
--    parentElem: The handle for the element that will be the parent of the
--                new one.
--    name      : The name of the new element.
--    value     : The date value. This is a Timestamp so that it can be called
--                from PL/SQL while maintaining the time component.
--    inclTime  : If true, The time portion of the date (HH24MISS) will be
--                included in the DOM element.
--------------------------------------------------------------------------------
procedure addDateElement(parentElem  in  xmldom.DOMElement,
                         name        in  varchar2,
                         value       in  date,
                         inclTime    in  boolean default FALSE);

--------------------------------------------------------------------------------
-- Copy the contents of the source element into the destination element. Any
-- existing nodes in the destination are left unchanged. All copies are deep.
-- PARAMETERS
--    dst: The element to be copied into.
--    src: The element whose contents will be copied into the dst element.
--------------------------------------------------------------------------------
procedure addElementContents(dst  in out  xmldom.DOMElement,
                             src  in      xmldom.DOMElement);

--------------------------------------------------------------------------------
-- Get the child element with the given name. If there are multiple children
-- with the same name, this will return the first one.
-- PARAMETERS
--    elem: The handle of the parent element.
--    name: The name of the child element.
-- RETURNS
--    The handle of the child node, or NO_HANDLE if there isn't one.
--------------------------------------------------------------------------------
function getChild(elem  in  xmldom.DOMElement,
                  name  in  varchar2)
return xmldom.DOMElement;

--------------------------------------------------------------------------------
-- Get the text contents of the child of the element with the given name. If
-- there are multiple child elements with the same name, this method will return
-- the text of the first one.
-- PARAMETERS
--    elem: Handle of the parent element.
--    name: The name of the child element.
-- RETURNS
--    The text contents of the child element.
--------------------------------------------------------------------------------
function getChildText(elem  in  xmldom.DOMElement,
                      name  in  varchar2)
return varchar2;

--------------------------------------------------------------------------------
-- Get the numeric contents of the child of the element with the given name. If
-- there are multiple child elements with the same name, this method will return
-- the text of the first one.
-- PARAMETERS
--    elem: Handle of the parent element.
--    name: The name of the child element.
-- RETURNS
--    The numeric value of the contents of the child element.
--------------------------------------------------------------------------------
function getChildNumber(elem  in  xmldom.DOMElement,
                        name  in  varchar2)
return number;

--------------------------------------------------------------------------------
-- Get the date value represented by the child with the given name. If the
-- element doesn't exist or the passed-in handle is NULL then this will raise an
-- exception.
-- PARAMETERS
--    elem: Handle of the parent element.
--    name: The name of the child element.
-- RETURNS
--    The date, or null if the conditions detailed in getDate() are true for the
--    child element.
--------------------------------------------------------------------------------
function getChildDate( elem  in  xmldom.DOMElement,
                       name  in  varchar2)
return date;

--------------------------------------------------------------------------------
-- Retrieve all children of a node with the given name. If there are no children
-- with the given name, this will return NO_HANDLE.
-- PARAMETERS
--    elem: Handle of the parent element.
--    name: Name that all the children will have.
-- RETURNS
--    A handle to a list containing the child elements.
--------------------------------------------------------------------------------
function getChildren(elem  in  xmldom.DOMElement,
                     name  in  varchar2)
return xmldom.DOMNodeList;

--------------------------------------------------------------------------------
-- Get the length of the given element list.
-- PARAMETERS
--    list: handle of the element list.
-- RETURNS
--    The length of the list, or 0 if the input handle is invalid or null.
--------------------------------------------------------------------------------
function getListLength(list in xmldom.DOMNodeList)
return number;

--------------------------------------------------------------------------------
-- Get a member of an element list. If the handle is null or the index is out of
-- bounds then an exception will be raised.
-- PARAMETERS
--    list : The handle of the element list.
--    index: The index of the element to retrieve (start from 0).
-- RETURNS
--    The handle of the element.
--------------------------------------------------------------------------------
function getListElement(list  in  xmldom.DOMNodeList,
                        indx  in  number)
return xmldom.DOMElement;

--------------------------------------------------------------------------------
end rib_xml;
/
