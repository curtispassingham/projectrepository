CREATE OR REPLACE package rib_sxw as

--------------------------------------------------------------------------------

SUCCESS  CONSTANT  VARCHAR2(1) := 'S';
FAILED   CONSTANT  VARCHAR2(1) := 'F';
--
subtype SXWHandle is integer;
NULL_HANDLE SXWHandle := -1;

--------------------------------------------------------------------------------
-- Create a handle for the root node of a new XML document with the given name.
--------------------------------------------------------------------------------
procedure newRoot(root           out  SXWHandle,
                  name           in   varchar2,
                  O_status_code  out  varchar2);

--------------------------------------------------------------------------------
-- Write the document out to the given clob.
--------------------------------------------------------------------------------
procedure writeRoot(root           in out  nocopy SXWHandle,
                    clobdata       in out  nocopy clob,
                    O_status_code  out     varchar2);

--------------------------------------------------------------------------------
-- Release any allocated resources and do general cleanup.
--------------------------------------------------------------------------------
procedure freeRoot(root  in out  SXWHandle);

--------------------------------------------------------------------------------
-- Add a new opening tag to the document with the given name, and text after
-- the tag. If there are any currently open elements in the doc at or below
-- the depth of the new element, they will be closed.
--------------------------------------------------------------------------------
procedure addElement(parent  in out  nocopy SXWHandle,
                     name    in      varchar2,
                     value   in      varchar2);

--------------------------------------------------------------------------------
-- Overloaded addElement function and package for numeric values.  The numeric
-- values will use an explicit conversion to a string.  The explicit conversion
-- will specify that the decimal point will be a period (.) in the string.
-- This modification was added to handle European number settings, which use
-- a comma (,) as the decimal point.
--------------------------------------------------------------------------------
procedure addElement(parent  in out  nocopy SXWHandle,
                     name    in      varchar2,
                     value   in      number);

--------------------------------------------------------------------------------
-- Add a date element to the document.  If there are any currently open elements
-- in the doc at or below the depth of the new element, they will be closed. If
-- inclTime is TRUE, a timestamp (hour/minute/second) will be added to the
-- content of the element in addition to the year/month/day.
--------------------------------------------------------------------------------
procedure addDateElement( parent    in out  nocopy SXWHandle,
                          name      in      varchar2,
                          value     in      date,
                          inclTime  in      boolean default FALSE);

--------------------------------------------------------------------------------
end rib_sxw;
/
