CREATE OR REPLACE package body rib_sxw as

--------------------------------------------------------------------------------
-- Types
--------------------------------------------------------------------------------
type names_t   is table of varchar2(255)   index by binary_integer;
type buffers_t is table of varchar2(32767) index by binary_integer;

--------------------------------------------------------------------------------
-- Constants
--------------------------------------------------------------------------------
XMLNS          constant varchar2(20) := 'xmlns';
XMLNS_XSI      constant varchar2(20) := 'xmlns:xsi';
XSI_SCH_LOC    constant varchar2(20) := 'xsi:schemaLocation';
FILE_EXT       constant varchar2(4)  := '.xsd';
XSD_PATH       constant varchar2(11)   := 'payload/xsd';
EMPTY_ELEMENT  constant varchar2(20) := '*EMPTY_ELEMENT*';
AMP_AMP        constant varchar2(5)  := '&'||'amp;';
AMP_GT         constant varchar2(4)  := '&'||'gt;';
AMP_LT         constant varchar2(4)  := '&'||'lt;';
AMP_APOS       constant varchar2(6)  := '&'||'apos;';
AMP_QUOT       constant varchar2(6)  := '&'||'quot;';

--------------------------------------------------------------------------------
-- Package Variables
--------------------------------------------------------------------------------
gBuf           varchar2(32767);
gNames         names_t;
gBuffers       buffers_t;
gCurLev        SXWHandle := -1;
gBufferCount   binary_integer := 0;

errMessage           rtk_errors.rtk_text%type;


/******************************************************************************\
 * Private functions and procedures                                           *
\******************************************************************************/
--------------------------------------------------------------------------------
function fixEntities(s in varchar2) return varchar2 is
   o varchar2(4000);
begin
   if ( instr(s, '&') != 0
      or instr(s,'<') != 0
      or instr(s,'>') != 0
      or instr(s,'''') != 0
      or instr(s,'"') != 0
      ) then
      o := replace(s, '&', AMP_AMP);
      o := replace(o, '<', AMP_LT);
      o := replace(o, '>', AMP_GT);
      o := replace(o, '''',AMP_APOS);
      o := replace(o, '"', AMP_QUOT);
      return o;
   else
      return s;
   end if;
end;

--------------------------------------------------------------------------------
procedure writeStart(n  in  varchar2)
is
   module  varchar2(60) := 'rib_sxw.writeStart';
begin
   gBuf := gBuf||'<'||n||'>';
exception
   when VALUE_ERROR then
      gBufferCount := gBufferCount + 1;
      gbuffers(gBufferCount) := gBuf;
      gBuf := '<'||n||'>';
      raise_application_error(-20001, sqlerrm);
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      RAISE VALUE_ERROR;
end writeStart;

--------------------------------------------------------------------------------
procedure writeText(text  in  varchar2)
is
   module  varchar2(60) := 'rib_sxw.writeText';
begin
   gBuf := gBuf || text;
exception
   when VALUE_ERROR then
      gBufferCount := gBufferCount + 1;
      gbuffers(gBufferCount) := gBuf;
      gBuf := text;
      raise_application_error(-20001, sqlerrm);
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      RAISE VALUE_ERROR;
end writeText;

--------------------------------------------------------------------------------
procedure writeEnd(lev  in  SXWHandle)
is
   module  varchar2(60) := 'rib_sxw.writeEnd';
   n varchar2(255);
begin
   n := gNames(lev);
   gBuf := gBuf||'</'||n||'>';
exception
   when VALUE_ERROR then
      gBufferCount := gBufferCount + 1;
      gbuffers(gBufferCount) := gBuf;
      gBuf := '</'||n||'>';
      raise_application_error(-20001, sqlerrm);
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      RAISE VALUE_ERROR;
end;

--------------------------------------------------------------------------------
procedure flushEnds
is
   module  varchar2(60) := 'rib_sxw.flushEnds';
begin
   while (gCurLev > -1) loop
      writeEnd(gCurLev);
      gCurLev := gCurLev - 1;
   end loop;
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end flushEnds;

--------------------------------------------------------------------------------
function getXMLNamespaceURL
return varchar2 is

   module  varchar2(60) := 'rib_sxw.getXMLNamespaceURL';
   url     varchar2(400);

begin

   -- Check if the rib options row is populated
   if RIB_OPTIONS_SQL.GET_XML_NAMESPACE_URL(errMessage,
                                            url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLNamespaceURL;

--------------------------------------------------------------------------------
function getXMLSchemaBaseURL
return varchar2 is

   module  varchar2(60) := 'rib_sxw.getXMLSchemaBaseURL';
   url     varchar2(400);

begin

   -- Check if the rib options row is populated
   if RIB_OPTIONS_SQL.GET_XML_SCHEMA_BASE_URL(errMessage,
                                              url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLSchemaBaseURL;

--------------------------------------------------------------------------------
function getXMLxsiURL
return varchar2 is

   module  varchar2(60) := 'rib_sxw.getXMLxsiURL';
   url     varchar2(400);

begin

   -- Check if the rib options row is populated
   if RIB_OPTIONS_SQL.GET_XML_XSI_URL(errMessage,
                                      url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLxsiURL;


/******************************************************************************\
* Public functions and procedures
\******************************************************************************/

--------------------------------------------------------------------------------
procedure newRoot(root           out  SXWHandle,
                  name           in   varchar2,
                  O_status_code  out  varchar2)
is
   module  varchar2(60) := 'rib_sxw.newRoot';
   nelm SXWHandle := 0;
begin
   freeRoot(root);
   --
   gBuf := '<'||name||' '||XMLNS||'="'||getXMLNamespaceURL||'/'||name||'/'||'v1'||'" '
        ||XMLNS_XSI||'="'||getXMLxsiURL||'">';
       -- ||XSI_SCH_LOC||'="'||getXMLNamespaceURL||'/'||name||' '||getXMLSchemaBaseURL||XSD_PATH||'/'||name||FILE_EXT||'">';
       -- Removed the Schema base url from being added to the xml header since it is no longer required for validating the payload. 
   --
   gNames(nelm)  := name;
   gCurLev       := nelm;
   root          := nelm;
   --
   O_status_code := SUCCESS;
exception
   when OTHERS then
      O_status_code := FAILED;
end newRoot;

--------------------------------------------------------------------------------
procedure writeRoot(root           in out  nocopy SXWHandle,
                    clobdata       in out  nocopy clob,
                    O_status_code  out     varchar2)
is
   module  varchar2(60) := 'rib_sxw.writeRoot';
   lCurrentBuf binary_integer := 1;
begin
   flushEnds;
   --
   while lCurrentBuf <= gBufferCount loop
      dbms_lob.writeAppend(clobdata, length(gBuffers(lCurrentBuf)),gBuffers(lCurrentBuf));
      lCurrentBuf := lCurrentBuf + 1;
   end loop;
   --
   dbms_lob.writeAppend(clobdata, length(gBuf), gBuf );
   --
   O_status_code := SUCCESS;
exception
   when OTHERS then
      O_status_code := FAILED;
end writeRoot;

--------------------------------------------------------------------------------
procedure freeRoot(root in out SXWHandle)
is
   module  varchar2(60) := 'rib_sxw.freeRoot';
begin
   gNames.delete;
   gBuffers.delete;
   gCurLev := -1;
   gBufferCount :=0;
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end freeRoot;

--------------------------------------------------------------------------------
procedure addElement(parent  in out nocopy SXWHandle,
                     name    in varchar2,
                     value   in varchar2)
is
   module  varchar2(60) := 'rib_sxw.addElement';
   nelm SXWHandle := parent + 1;
begin
   -- close any open tags at or below this level.
   while ( gCurLev >= nelm ) loop
      writeEnd(gCurLev);
      gCurLev := gCurLev - 1;
   end loop;
   --
   gNames(nelm) := name;
   writeStart(name);
   gCurLev := nelm;
   if value != EMPTY_ELEMENT then
      writeText(fixEntities(value));
   end if;
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
procedure addElement(parent  in out  nocopy SXWHandle,
                     name    in      varchar2,
                     value   in      number)
is
   module  varchar2(60) := 'rib_sxw.addElement';
   char_value varchar2(64) := NULL;
begin
   if value is NOT NULL then
      char_value := TO_CHAR(value,'TM', 'NLS_NUMERIC_CHARACTERS = ''. '' ');
   end if;
   addElement(parent, name, char_value);
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
procedure addEmptyElement(parent  in out  nocopy SXWHandle,
                          name    in      varchar2)
is
   module  varchar2(60) := 'rib_sxw.addEmptyElement';
begin
   addElement(parent, name, EMPTY_ELEMENT);
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addEmptyElement;

--------------------------------------------------------------------------------
procedure addDateElement(parent    in out  nocopy SXWHandle,
                         name      in      varchar2,
                         value     in      date,
                         inclTime  in      boolean default FALSE)
is
   module  varchar2(60) := 'rib_sxw.addDateElement';
   lTextElement varchar2(255);
   
begin
   if value is not NULL then
      lTextElement := to_char(value,'YYYY-MM-DD')||'T'||to_char(value,'HH24:MI:SS');       
      addElement(parent, name, lTextElement);
   end if;
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addDateElement;

--------------------------------------------------------------------------------
end rib_sxw;
/
