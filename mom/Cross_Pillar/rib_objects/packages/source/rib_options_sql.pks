CREATE OR REPLACE PACKAGE RIB_OPTIONS_SQL AS

--------------------------------------------------------------------------------
FUNCTION GET_RIB_OPTIONS_ROW(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_rib_options_row  IN OUT  RIB_OPTIONS%ROWTYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_XML_NAMESPACE_URL(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_xml_namespace_url  IN OUT  RIB_OPTIONS.XML_NAMESPACE_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_XML_SCHEMA_BASE_URL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_xml_schema_base_url  IN OUT  RIB_OPTIONS.XML_SCHEMA_BASE_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION GET_XML_XSI_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_xml_xsi_url    IN OUT  RIB_OPTIONS.XML_XSI_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SET_XML_NAMESPACE_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_url            IN      RIB_OPTIONS.XML_NAMESPACE_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SET_XML_SCHEMA_BASE_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_url            IN      RIB_OPTIONS.XML_SCHEMA_BASE_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
FUNCTION SET_XML_XSI_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_url            IN      RIB_OPTIONS.XML_XSI_URL%TYPE)
RETURN BOOLEAN;

--------------------------------------------------------------------------------
END RIB_OPTIONS_SQL;
/
