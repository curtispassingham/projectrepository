CREATE OR REPLACE package body rib_xml as

--------------------------------------------------------------------------------
-- Constants
--------------------------------------------------------------------------------
XMLNS          constant varchar2(20) := 'xmlns';
XMLNS_NS1      constant varchar2(20) := 'xmlns:ns1';
XMLNS_NS2      constant varchar2(20) := 'xmlns:ns2';
XMLNS_NS3      constant varchar2(20) := 'xmlns:ns3';
XMLNS_XSI      constant varchar2(20) := 'xmlns:xsi';
NS_1           constant varchar2(20) := 'ns1:';
NS_2           constant varchar2(20) := 'ns2:';
NS_3           constant varchar2(20) := 'ns3:';
XSI_SCH_LOC    constant varchar2(20) := 'xsi:schemaLocation';
FILE_EXT       constant varchar2(4)  := '.xsd';
EMPTY_ELEMENT  constant varchar2(20) := '*EMPTY_ELEMENT*';


--------------------------------------------------------------------------------
-- Package Variables
--------------------------------------------------------------------------------
errMessage   rtk_errors.rtk_text%type;


/******************************************************************************\
* Private functions and procedures                                             *
\******************************************************************************/

--------------------------------------------------------------------------------
function getXMLNamespaceURL
return varchar2 is

   module  varchar2(60) := 'rib_xml.getXMLNamespaceURL';
   url     varchar2(400);

begin

   if RIB_OPTIONS_SQL.GET_XML_NAMESPACE_URL(errMessage,
                                            url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLNamespaceURL;

--------------------------------------------------------------------------------
function getXMLSchemaBaseURL
return varchar2 is

   module  varchar2(60) := 'rib_xml.getXMLSchemaBaseURL';
   url     varchar2(400);

begin

   if RIB_OPTIONS_SQL.GET_XML_SCHEMA_BASE_URL(errMessage,
                                              url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLSchemaBaseURL;

--------------------------------------------------------------------------------
function getXMLxsiURL
return varchar2 is

   module  varchar2(60) := 'rib_xml.getXMLxsiURL';
   url     varchar2(400);

begin

   if RIB_OPTIONS_SQL.GET_XML_XSI_URL(errMessage,
                                      url) = FALSE then
      raise_application_error(-20002, errMessage);
   end if;

   -- Return the URL.
   return url;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getXMLxsiURL;

--------------------------------------------------------------------------------
-- This function will create a new element within the document, make a node from
-- the element, then append the new node to the parent element.  For certain
-- nodes it will append namespace values.
--------------------------------------------------------------------------------
function addElementPrivate(parentElem  in  xmldom.DOMElement,
                           name        in  varchar2,
                           value       in  varchar2)
return xmldom.DOMElement is

   module      varchar2(60) := 'rib_xml.addElementPrivate';
   parentNode  xmldom.DOMNode;
   parentName  varchar2(100);
   ownerdoc    xmldom.DOMDocument;
   newElem     xmldom.DOMElement;
   newNode     xmldom.DOMNode;
   textNode    xmldom.DOMNode;
   lName       varchar2(100);
   attr        xmldom.DOMAttr;
   namespace   varchar2(20);

begin

   if value is not null then

      -- Get the owner of the document and the name of the parent node.
      parentNode := xmldom.makeNode(parentElem);
      parentName := xmldom.getNodeName(parentNode);
      ownerdoc   := xmldom.getOwnerDocument(parentNode);

      -- If the parent node is VendorHdrDesc, or the parent node is VendorDesc
      -- and this node is VendorHdrDesc, then we want to append the namespace
      -- value to each tag.
      if parentName = 'ns1:VendorHdrDesc'
      or parentName = 'VendorDesc' and name = 'VendorHdrDesc' then
         lName := NS_1||name;
      -- If the parent node is VendorAddrDesc, or the parent node is VendorDesc
      -- and this node is VendorAddrDesc, then we want to append the namespace
      -- value to each tag.
      elsif parentName = 'ns2:VendorAddrDesc'
      or parentName = 'VendorDesc' and name = 'VendorAddrDesc' then
         lName := NS_2||name;
      -- If the parent node is VendorOUDesc, or the parent node is VendorDesc
      -- and this node is VendorOUDesc, then we want to append the namespace
      -- value to each tag.
      elsif parentName = 'ns3:VendorOUDesc'
      or parentName = 'VendorDesc' and name = 'VendorOUDesc' then
         lName := NS_3||name; 
      -- If the parent node is VendorOURef, or the parent node is VendorRef
      -- and this node is VendorOURef, then we want to append the namespace
      -- value to each tag.
      elsif parentName = 'ns1:VendorOURef'
      or parentName = 'VendorRef' and name = 'VendorOURef' then
         lName := NS_1||name;
      -- Otherwise we just want to name the tag with the original name.
      else
         lName := name;
      end if;

      -- Create a new element within this document and name it lName.
      -- Then create a node from the new element and append it to the parent node.
      newElem := xmldom.createElement(ownerdoc, lName);
      newNode := xmldom.appendChild(parentNode, xmldom.makeNode(newElem));

      -- If building a VendorDesc xml then we want to create namespace attributes
      -- for VendorHdrDesc, VendorAddrDesc, and VendorOUDesc tags.
      -- EXAMPLE: <ns1:VendorHdrDesc xmlns:ns1="http://www.oracle.com/...">
      if parentName = 'VendorDesc' and name in ('VendorHdrDesc','VendorAddrDesc','VendorOUDesc') then
         if name = 'VendorHdrDesc' then
            namespace := XMLNS_NS1;
         elsif name = 'VendorAddrDesc' then
            namespace := XMLNS_NS2;
         elsif name = 'VendorOUDesc' then
            namespace := XMLNS_NS3;
         end if;
         attr := xmldom.createAttribute(ownerdoc, namespace);
         xmldom.setValue(attr, getXMLNamespaceURL||'/'||name||'/'||'v1');
         attr := xmldom.setAttributeNode(newElem, attr);
      end if;

      -- If building a VendorRef xml then we want to create namespace attributes
      -- for VendorOURef tag.
      -- EXAMPLE: <ns1:VendorOURef xmlns:ns1="http://www.oracle.com/..">
      if parentName = 'VendorRef' and name = 'VendorOURef' then
         namespace := XMLNS_NS1;
         attr := xmldom.createAttribute(ownerdoc, namespace);
         xmldom.setValue(attr, getXMLNamespaceURL||'/'||name||'/'||'v1');
         attr := xmldom.setAttributeNode(newElem, attr);
      end if;

      -- If the value is not EMPTY_ELEMENT then add the text value to the node.
      if value != EMPTY_ELEMENT then
         textNode := xmldom.appendChild(newNode, xmldom.makeNode(xmldom.createTextNode(ownerdoc, value)));
      end if;
   end if;

   return newElem;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElementPrivate;

/******************************************************************************\
 * Public functions and procedures                                            *
\******************************************************************************/

--------------------------------------------------------------------------------
function newRoot(root  out  xmldom.DOMElement,
                 name  in   varchar2 )
return boolean is

   module     varchar2(60) := 'rib_xml.newRoot';
   docnode    xmldom.DOMNode;
   doc        xmldom.DOMDocument;
   attr       xmldom.DOMAttr;
   newnod     xmldom.DOMNode;
   namespace  varchar2(20);
   rootname   varchar2(20);
begin

   -- If this doc is a VendorHdrDesc or VendorAddrDesc then we need to use
   -- a specific value for the namespace. This is necessary for building
   -- a VendorDesc document.
   if name = 'VendorHdrDesc' or name = 'VendorOURef' then
      namespace := XMLNS_NS1;
      rootname := NS_1 || name;
   elsif name = 'VendorAddrDesc' then
      namespace := XMLNS_NS2;
      rootname := NS_2 || name;
   elsif name = 'VendorOUDesc' then
      namespace := XMLNS_NS3;
      rootname := NS_3 || name;
   else
      namespace := XMLNS;
      rootname := name;
   end if;

   -- Create a new document and make a new node in it.
   doc     := xmldom.newDOMDocument;
   docnode := xmldom.makeNode(doc);

   -- Now create a new element (root), make a node out of the element,
   -- and append it to the new docnode.
   root    := xmldom.createElement(doc, rootname);
   newnod  := xmldom.appendChild(docnode, xmldom.makeNode(root));

   -- Add attributes to the root node.
   attr := xmldom.createAttribute(doc, namespace);
   xmldom.setValue(attr, getXMLNamespaceURL||'/'||name||'/'||'v1');
   attr := xmldom.setAttributeNode(root, attr);
   --
   attr := xmldom.createAttribute(doc, XMLNS_XSI);
   xmldom.setValue(attr, getXMLxsiURL);
   attr := xmldom.setAttributeNode(root, attr);
   --
   --attr := xmldom.createAttribute(doc, XSI_SCH_LOC);
   --xmldom.setValue(attr, getXMLNamespaceURL||'/'||name||' '||getXMLSchemaBaseURL||'/'||name||FILE_EXT);
   --attr := xmldom.setAttributeNode(root, attr);
   -- Removed the Schema base url from being added to the xml header since it is no longer required for validating the payload.
   --
   return true;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      return false;
end newRoot;

--------------------------------------------------------------------------------
function readRoot(message      in  clob,
                  messageName  in  varchar2)
return xmldom.DOMElement is

   module   varchar2(60) := 'rib_xml.readRoot';
   lParser  xmlparser.Parser;
   outdoc   xmldom.DOMDocument;

begin

   lParser := xmlparser.newParser;
   xmlparser.parseClob(lParser, message);
   outdoc := xmlparser.getDocument(lParser);
   return xmldom.getDocumentElement(outdoc);

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end readRoot;

--------------------------------------------------------------------------------
function writeRoot(root  in out  xmldom.DOMElement,
                   data  in out  clob)
return boolean is

   module  varchar2(60) := 'rib_xml.writeRoot';
   nod     xmldom.DOMNode;
   doc     xmldom.DOMDocument;

begin

   -- Make a node from the root element, get the owner document, then
   -- write the document out to a CLOB.
   nod := xmldom.makeNode(root);
   doc := xmldom.getOwnerDocument(nod);
   xmldom.writeToClob(doc, data);

   return true;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      return false;
end writeRoot;

--------------------------------------------------------------------------------
procedure freeRoot(root  in out  xmldom.DOMElement)
is
   module  varchar2(60) := 'rib_xml.freeRoot';
begin
   xmldom.freeDocument(xmldom.getOwnerDocument(xmldom.makeNode(root)));
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end freeRoot;

--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                     name        in  varchar2,
                     value       in  varchar2)
is

   module  varchar2(60) := 'rib_xml.addElement(1)';
   tmp     xmldom.DOMElement;

begin

   tmp := addElementPrivate(parentElem, name, value);

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                     name        in  varchar2,
                     value       in  number)
is

   module     varchar2(60) := 'rib_xml.addElement(2)';
   tmp        xmldom.DOMElement;
   charValue  varchar2(64) := NULL;

begin

   if value is NOT NULL then
      charValue := TO_CHAR(value,'TM', 'NLS_NUMERIC_CHARACTERS = ''. '' ');
      tmp := addElementPrivate(parentElem, name, charValue);
   end if;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
function addElement(parentElem  in xmldom.DOMElement,
                    name        in varchar2)
return xmldom.DOMElement is

   module  varchar2(60) := 'rib_xml.addElement(3)';

begin

   return addElementPrivate(parentElem, name, EMPTY_ELEMENT);

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
procedure addElement(parentElem  in  xmldom.DOMElement,
                     name        in  varchar2)
is

   module  varchar2(60) := 'rib_xml.addElement(4)';
   tmp     xmldom.DOMElement;

begin

   tmp := addElementPrivate(parentElem, name, EMPTY_ELEMENT);

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElement;

--------------------------------------------------------------------------------
procedure addDateElement(parentElem  in  xmldom.DOMElement,
                         name        in  varchar2,
                         value       in  date,
                         inclTime    in  boolean default FALSE)
is

   module      varchar2(60) := 'rib_xml.addDateElement';
   parentNode  xmldom.DOMNode;
   ownerdoc    xmldom.DOMDocument;
   dateElem    xmldom.DOMElement;
   newNode     xmldom.DOMNode;

begin

   if value is not NULL then
      parentNode := xmldom.makeNode(parentElem);
      ownerdoc   := xmldom.getOwnerDocument(parentNode);
      dateElem   := xmldom.createElement(ownerdoc, name);
      newNode    := xmldom.appendChild(parentNode, xmldom.makeNode(dateElem));
      --
      addElement(dateElem, name, to_char(value,'YYYY-MM-DD')||'T'||to_char(value,'HH24:MI:SS'));
      
   end if;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addDateElement;

--------------------------------------------------------------------------------
procedure addElementContents(dst  in out  xmldom.DOMElement,
                             src  in      xmldom.DOMElement)
is

   module    varchar2(60) := 'rib_xml.addElementContents';
   dstDoc    xmldom.DOMDocument;
   dstNode   xmldom.DOMNode;
   srcNode   xmldom.DOMNode;
   srcList   xmldom.DOMNodeList;
   listNode  xmldom.DOMNode;
   newNode   xmldom.DOMNode;
   tmp       xmldom.DOMNode;

begin

   -- Make a node from the destination element and get the owning document
   dstNode := xmldom.makeNode(dst);
   dstDoc  := xmldom.getOwnerDocument(dstNode);

   -- Make a node from the source element and get all of the child nodes.
   srcNode := xmldom.makeNode(src);
   srcList := xmldom.getChildNodes(srcNode);

   -- Loop through all of the child nodes, import each node into the destination
   -- document, then append the new node to the destination node.
   for i in 0 .. (xmldom.getLength(srclist)-1) loop
      listNode := xmldom.item(srclist,i);
      newNode  := xmldom.importnode(dstDoc, listNode, TRUE);
      tmp      := xmldom.appendChild(dstNode, newNode);
   end loop;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end addElementContents;

--------------------------------------------------------------------------------
function getChild(elem  in  xmldom.DOMElement,
                  name  in  varchar2)
return xmldom.DOMElement is

   module  varchar2(60) := 'rib_xml.getChild';
   kids    xmldom.DOMNodeList;
   node    xmldom.DOMNode;
   outelm  xmldom.DOMElement;

begin

   kids := getChildren(elem, name);
   if xmldom.getLength(kids) <= 0 then
      return outelm;
   end if;
   node := xmldom.item(kids, 0);
   if xmldom.getNodeType(node) != xmldom.ELEMENT_NODE then
      return outelm;
   end if;
   outelm := xmldom.makeElement(node);

   return outelm;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getChild;

--------------------------------------------------------------------------------
function getChildText(elem  in  xmldom.DOMElement,
                      name  in  varchar2)
return varchar2 is

   module     varchar2(60) := 'rib_xml.getChildText';
   childElem  xmldom.DOMElement;
   textNode   xmldom.DOMText;
   rv         varchar2(4000) := NULL;
   nkds       xmldom.DOMNodeList;
   kid        xmldom.DOMNode;

begin

   childElem := getChild(elem, name);
   if not xmldom.isNull(childElem)  then
      xmldom.normalize(childElem);
      nkds := xmldom.getChildNodes(xmldom.makeNode(childElem));
      for i in 0 .. (xmldom.getLength(nkds)-1) loop
         kid := xmldom.item(nkds, i);
         if xmldom.getNodeType(kid) = xmldom.TEXT_NODE then
            textNode := xmldom.makeText(kid);
            exit;
         end if;
      end loop;
   end if;
   if not xmldom.isNull(textNode) then
      rv := xmldom.getData(xmldom.makeCharacterData(xmldom.makeNode(textNode)));
   end if;

   return rv;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getChildText;

--------------------------------------------------------------------------------
function getChildNumber(elem  in  xmldom.DOMElement,
                        name  in  varchar2)
return number is

   module      varchar2(60)  := 'rib_xml.getChildNumber';
   NUMBER_MASK varchar2(100) := '999999999999999999999999999999D99999999999999999999';
   childElem   xmldom.DOMElement;
   textNode    xmldom.DOMText;
   textValue   varchar2(4000);
   rv          number := NULL;

begin

   textValue := getChildText(elem, name);
   if textValue is NOT NULL then
      rv := to_number(textValue, NUMBER_MASK, 'NLS_NUMERIC_CHARACTERS = ''. '' ');
   end if;

   return rv;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getChildNumber;

--------------------------------------------------------------------------------
function getChildDate(elem  in  xmldom.DOMElement,
                      name  in  varchar2)
return date is

   module  varchar2(60) := 'rib_xml.getChildDate';
   textValue varchar2(100);
   textValueBeforeT varchar2(100);
   textValueAfterT varchar2(100);
   idxT				number := 0;
   dte date;
   errMessage varchar2(500); 
   
   
begin

   textValue := getChildText(elem, name);
   if textValue is NOT NULL then
      -- I/P date :2011-12-20T06:05:08-05:00  O/P date 2011-12-20 06:05:08-05:00
       if instr(textValue,'T') <> 0 then
       	 idxT := instr(textValue,'T');
         textValueBeforeT := SUBSTR(textValue, 0,idxT-1);
	  -- check if date starts with '-', ex: -2011-12-20T06:05:08Z this is accepted format.
         if (substr(textValueBeforeT,1,1) = '-') then
            textValueBeforeT := substr(textValueBeforeT,2, length(textValueBeforeT));
         end if;  
         textValueAfterT := SUBSTR(textValue, idxT+1,length(textValue));
         if instr(textValueAfterT,'Z') <> 0 then
         	-- 2011-12-20T06:05:08Z 
         	textValueAfterT := SUBSTR(textValueAfterT, 0 ,instr(textValueAfterT,'Z')-1);          
         elsif instr(textValueAfterT,'.') <> 0 then
         	-- 2011-12-20T06:05:08.111-05:00 
         	textValueAfterT := SUBSTR(textValueAfterT, 0 ,instr(textValueAfterT,'.')-1); 
         elsif instr(textValueAfterT,'-') <> 0 then
         	-- 2011-12-20T06:05:08-05:00 
         	textValueAfterT := SUBSTR(textValueAfterT, 0 ,instr(textValueAfterT,'-')-1);
         elsif instr(textValueAfterT,'+') <> 0 then
         	-- 2011-12-20T06:05:08+05:00
         	textValueAfterT := SUBSTR(textValueAfterT, 0 ,instr(textValueAfterT,'+')-1);
         end if;
         textValue := textValueBeforeT || ' ' || textValueAfterT;
         -- 2011-12-20 06:05:08
         dte := to_date(textValue,'YYYY-MM-DD HH24:MI:SS');
      else
         -- We need to raise exception if there is no T in the incoming date
	errMessage := 'ERROR in '||module||': Invalid date format found in '||textValue ||' format should have T as the separator between date and time components.';
         raise_application_error(-20001, errMessage);
      end if;
   end if;
   return dte;
   
exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getChildDate;
--------------------------------------------------------------------------------
function getChildren(elem  in  xmldom.DOMElement,
                     name  in  varchar2)
return xmldom.DOMNodeList is

   module  varchar2(60) := 'rib_xml.getChildren';
   kids    xmldom.DOMNodeList;

begin

   kids := xmldom.getChildrenByTagName(elem, name);
   return kids;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getChildren;

--------------------------------------------------------------------------------
function getListLength(list  in  xmldom.DOMNodeList)
return number is

   module  varchar2(60) := 'rib_xml.getListLength';

begin

   if xmldom.isNull(list) then
      return 0;
   else
      return xmldom.getLength(list);
   end if;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getListLength;

--------------------------------------------------------------------------------
function getListElement(list  in  xmldom.DOMNodeList,
                        indx  in  number)
return xmldom.DOMElement is

   module  varchar2(60) := 'rib_xml.getListElement';
   node    xmldom.DOMNode;
   rv      xmldom.DOMElement;

begin

   node := xmldom.item(list, indx);
   rv   := xmldom.makeElement(node);

   return rv;

exception
   when OTHERS then
      dbms_output.put_line('ERROR in '||module||': '||sqlerrm);
      raise_application_error(-20001, sqlerrm);
end getListElement;

--------------------------------------------------------------------------------
end rib_xml;
/
