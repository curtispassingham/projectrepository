CREATE OR REPLACE PACKAGE BODY RIB_OPTIONS_SQL AS

--------------------------------------------------------------------------------
-- Package Variables
--------------------------------------------------------------------------------
LP_rib_options_row  RIB_OPTIONS%ROWTYPE;
LP_populated        BOOLEAN := FALSE;


/******************************************************************************\
* Private functions and procedures                                             *
\******************************************************************************/

--------------------------------------------------------------------------------
FUNCTION POPULATE_RIB_OPTIONS_ROW(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.POPULATE_RIB_OPTIONS_ROW';

   CURSOR C_GET_RIB_OPTIONS IS
      select *
        from rib_options;

BEGIN

   open  c_get_rib_options;
   fetch c_get_rib_options into LP_rib_options_row;
   --
   if C_GET_RIB_OPTIONS%NOTFOUND then
      close C_GET_RIB_OPTIONS;
      LP_populated := FALSE;
      O_error_message := ('ERROR in '||module||': Unable to retrieve values from rib_options table');
      return FALSE;
   end if;
   --
   close C_GET_RIB_OPTIONS;
   LP_populated := TRUE;
   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
END POPULATE_RIB_OPTIONS_ROW;


/******************************************************************************\
* Public functions and procedures                                             *
\******************************************************************************/

--------------------------------------------------------------------------------
FUNCTION GET_RIB_OPTIONS_ROW(O_error_message    IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                             O_rib_options_row  IN OUT  RIB_OPTIONS%ROWTYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.GET_RIB_OPTIONS_ROW';

BEGIN

   -- If the rib options have already been saved in LP_rib_options_row then
   -- we don't need to go to the database again.
   if NOT LP_populated then
      if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   O_rib_options_row := LP_rib_options_row;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
END GET_RIB_OPTIONS_ROW;

--------------------------------------------------------------------------------
FUNCTION GET_XML_NAMESPACE_URL(O_error_message      IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               O_xml_namespace_url  IN OUT  RIB_OPTIONS.XML_NAMESPACE_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.GET_XML_NAMESPACE_URL';

BEGIN

   -- Check if the rib options row is populated
   if not LP_populated then
      if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   O_xml_namespace_url := LP_rib_options_row.xml_namespace_url;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
end GET_XML_NAMESPACE_URL;

--------------------------------------------------------------------------------
FUNCTION GET_XML_SCHEMA_BASE_URL(O_error_message        IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 O_xml_schema_base_url  IN OUT  RIB_OPTIONS.XML_SCHEMA_BASE_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.GET_XML_SCHEMA_BASE_URL';

BEGIN

   -- Check if the rib options row is populated
   if not LP_populated then
      if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   O_xml_schema_base_url := LP_rib_options_row.xml_schema_base_url;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
end GET_XML_SCHEMA_BASE_URL;

--------------------------------------------------------------------------------
FUNCTION GET_XML_XSI_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         O_xml_xsi_url    IN OUT  RIB_OPTIONS.XML_XSI_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.GET_XML_XSI_URL';

BEGIN

   -- Check if the rib options row is populated
   if not LP_populated then
      if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
         return FALSE;
      end if;
   end if;

   O_xml_xsi_url := LP_rib_options_row.xml_xsi_url;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
end GET_XML_XSI_URL;

--------------------------------------------------------------------------------
FUNCTION SET_XML_NAMESPACE_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                               I_url            IN      RIB_OPTIONS.XML_NAMESPACE_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.SET_XML_NAMESPACE_URL';

BEGIN

   -- Update the rib_options table
   update rib_options
      set xml_namespace_url = I_url;

   -- If a record is not found for updating then insert a new record.
   if SQL%NOTFOUND then
      insert into rib_options(xml_namespace_url)
                       values(I_url);

      -- If the insert failed then raise an error
      if SQL%NOTFOUND then
         O_error_message := ('ERROR in '||module||': Unable to insert/update table rib_options.');
         return FALSE;
      end if;
   end if;

   -- Repopulate the global variables
   if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
END SET_XML_NAMESPACE_URL;

--------------------------------------------------------------------------------
FUNCTION SET_XML_SCHEMA_BASE_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                 I_url            IN      RIB_OPTIONS.XML_SCHEMA_BASE_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.SET_XML_SCHEMA_BASE_URL';

BEGIN

   -- Update the rib_options table
   update rib_options
      set xml_schema_base_url = I_url;

   -- If a record is not found for updating then insert a new record.
   if SQL%NOTFOUND then
      insert into rib_options(xml_schema_base_url)
                       values(I_url);

      -- If the insert failed then raise an error
      if SQL%NOTFOUND then
         O_error_message := ('ERROR in '||module||': Unable to insert/update table rib_options.');
         return FALSE;
      end if;
   end if;

   -- Repopulate the global variables
   if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
END SET_XML_SCHEMA_BASE_URL;

--------------------------------------------------------------------------------
FUNCTION SET_XML_XSI_URL(O_error_message  IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                         I_url            IN      RIB_OPTIONS.XML_XSI_URL%TYPE)
RETURN BOOLEAN IS

   module  VARCHAR2(60) := 'RIB_OPTIONS_SQL.SET_XML_XSI_BASE_URL';

BEGIN

   -- Update the rib_options table
   update rib_options
      set xml_xsi_url = I_url;

   -- If a record is not found for updating then insert a new record.
   if SQL%NOTFOUND then
      insert into rib_options(xml_xsi_url)
                       values(I_url);

      -- If the insert failed then raise an error
      if SQL%NOTFOUND then
         O_error_message := ('ERROR in '||module||': Unable to insert/update table rib_options.');
         return FALSE;
      end if;
   end if;

   -- Repopulate the global variables
   if POPULATE_RIB_OPTIONS_ROW(O_error_message) = FALSE then
      return FALSE;
   end if;

   return TRUE;

EXCEPTION
   when OTHERS then
      O_error_message := ('ERROR in '||module||': '||sqlerrm);
      return FALSE;
END SET_XML_XSI_URL;

--------------------------------------------------------------------------------
END RIB_OPTIONS_SQL;
/
