SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------------------
-- Copyright (c) 2016, Oracle Retail Inc.  All rights reserved.
-- $Workfile:   populate_rpm_dept_aggregration.sql  $
-- $Revision:    $
-- $Modtime:     $
--------------------------------------------------------------------

REM ----------------------------------------------------------------
REM  Inserts records into rpm_dept_aggregation from the deps table
REM ----------------------------------------------------------------

PROMPT Inserting records into rpm_dept_aggregation for departments not yet present in the table

DECLARE

   L_RPM_IND     SYSTEM_OPTIONS.RPM_IND%TYPE;
   
   cursor C_GET_RPM_IND is
      select rpm_ind
        from system_options;
   
BEGIN
   
   open C_GET_RPM_IND;
   fetch C_GET_RPM_IND into L_RPM_IND;
   close C_GET_RPM_IND;
   
   if L_RPM_IND = 'Y' then
   
      INSERT INTO RPM_DEPT_AGGREGATION (DEPT_AGGREGATION_ID,
                                     DEPT,
                                     LOWEST_STRATEGY_LEVEL,
                                     WORKSHEET_LEVEL,
                                     HISTORICAL_SALES_LEVEL,
                                     REGULAR_SALES_IND,
                                     CLEARANCE_SALES_IND,
                                     PROMOTIONAL_SALES_IND,
                                     INCLUDE_WH_ON_HAND,
                                     INCLUDE_WH_ON_ORDER,
                                     PRICE_CHANGE_AMOUNT_CALC_TYPE,
                                     LOCK_VERSION)
       SELECT RPM_DEPT_AGGREGATION_SEQ.NEXTVAL,
              D.DEPT,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
         FROM DEPS D
        WHERE NOT EXISTS (SELECT 1
                            FROM RPM_DEPT_AGGREGATION RDA
                           WHERE RDA.DEPT = D.DEPT);
      
      COMMIT;
   end if; 
   
END;   
/