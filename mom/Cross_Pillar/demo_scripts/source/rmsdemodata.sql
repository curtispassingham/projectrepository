
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set serveroutput on;

spool off
spool rmsdemodata.log replace

--installerize
--accept base_country     char   prompt 'Enter the Base Country Id (2 characters): '
--accept prim_currency    char   prompt 'Enter the Primary currency: '
--accept table_owner      char   prompt 'What is the owner schema? '
define base_country = &1;
define prim_currency = &2;
define table_owner = &3;
define num_items = &4;
define tran_level = &5;

REM----------------------------------------------------
REM-----------------------------------------------------
REM  This script populates generic basic info.  Typically, this is used for a demo
REM  beginning point.

prompt 'Starting File: rmsstates.sql';
@@rmsstates.sql;
prompt 'Starting File: rmscurrencyrates.sql';
@@rmscurrencyrates.sql;
set define "&";
prompt 'Starting File:rmsdemodata.sql';

DECLARE
   L_error_message         RTK_ERRORS.RTK_TEXT%TYPE;
   LP_system_options_row   SYSTEM_OPTIONS%ROWTYPE;

   PROGRAM_ERROR           EXCEPTION;

BEGIN

   if SYSTEM_OPTIONS_SQL.GET_SYSTEM_OPTIONS(L_error_message,
                                            LP_system_options_row) = FALSE then
      return;
   end if;

   if not SYS_UNIT_OPTIONS_SQL_SQL.SYS_UOP_TABLES(L_error_message,
                                                  upper('&prim_currency'),
                                                  LP_system_options_row.default_tax_type,
                                                  LP_system_options_row.class_level_vat_ind,
                                                  'N',
                                                  '&table_owner',
                                                  upper('&base_country')) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.BANNER(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.CHANNEL(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.BUYERS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.MERCHANTS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.COMPHEAD(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.LOCHIER(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
      if not GENERAL_DATA_INSTALL.WF_CUSTOMER_GROUP(L_error_message) then
         raise PROGRAM_ERROR;
      end if;
      if not GENERAL_DATA_INSTALL.WF_CUSTOMER(L_error_message) then
         raise PROGRAM_ERROR;
      end if;
   ---
   if not GENERAL_DATA_INSTALL.FIF_GL_SETUP(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.ORG_UNIT(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.TSF_ENTITY(L_error_message) then
         raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.TSF_ENTITY_ORG_UNIT_SOB(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.WHS(L_error_message,
                                   LP_system_options_row.default_tax_type) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.BRAND(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   
   if not GENERAL_DATA_INSTALL.FREIGHT_TERMS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   
   if not GENERAL_DATA_INSTALL.PAYMENT_TERMS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;

   if (LP_system_options_row.supplier_sites_ind = 'Y') then
      if not GENERAL_DATA_INSTALL.SUPS_ADDR_SUP_SITE(L_error_message,
                                                     LP_system_options_row.default_tax_type) then
         raise PROGRAM_ERROR;
      end if;
   else
      if not GENERAL_DATA_INSTALL.SUPS_ADDR(L_error_message,
                                            LP_system_options_row.default_tax_type) then
         raise PROGRAM_ERROR;
      end if;
   end if;
   ---

   if not GENERAL_DATA_INSTALL.PARTNER_ORG_UNIT(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.TSF_ZONE(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.STORE_FORMAT(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.STORES(L_error_message,
                                      LP_system_options_row.default_tax_type)  then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.REFRESH_MV_LOC_SOB(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.REFRESH_MV_CURRENCY_CONV_RATES(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.UDAS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.DIFFS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.DEAL_COMP_TYPE(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.DIVISION(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.GROUPS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.DEPT(L_error_message,
                                    LP_system_options_row.default_tax_type,
                                    LP_system_options_row.class_level_vat_ind) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.CLASS_SUBCLASS(L_error_message,
                                              LP_system_options_row.default_tax_type,
                                              LP_system_options_row.class_level_vat_ind) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if LP_system_options_row.rpm_ind = 'Y' then
   
      if not GENERAL_DATA_INSTALL.RPM_MERCH_HIER(L_error_message) then
         raise PROGRAM_ERROR;
      end if;
   end if;   
   ---
   if not GENERAL_DATA_INSTALL.PO_TYPE(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.OUTLOC(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.RTK_ROLE_PRIVS(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.FREIGHT_TYPE_SIZE(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.DOC(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.NON_MERCH_CODES(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not GENERAL_DATA_INSTALL.COST_ZONE(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   if not ADDRESS_SQL.REFRESH_MV_LOC_PRIM_ADDR(L_error_message) then
      raise PROGRAM_ERROR;
   end if;
   ---
   commit;

EXCEPTION
   when OTHERS then
      dbms_output.put_line(L_error_message);
      return;
END;
/
--Creates an RMS security super group
insert into sec_group (GROUP_ID,
                       GROUP_NAME,
                       ROLE,
                       COMMENTS)
               values (1,
                       'SYSTEM SUPER USER GROUP',
                       NULL,
                       'Not to be associated with any hierarchy levels.');

-- Run after superGroup.sql
-- Associates RMS_ADMIN user with the super user group

insert into sec_user ( USER_SEQ,
                       DATABASE_USER_ID,
                       APPLICATION_USER_ID,
                       CREATE_ID,
                       CREATE_DATETIME,
                       RMS_USER_IND,
                       RESA_USER_IND,
                       REIM_USER_IND,
                       ALLOCATION_USER_IND)
                     ( select sec_user_sequence.NEXTVAL,
                              NULL,
                              'RMS_ADMIN',
                              USER,
                              SYSDATE,
                              'Y',
                              'N',
                              'N',
                              'N'
                         from  dual );
                       

insert into sec_user_group (GROUP_ID,
                            USER_SEQ,
                            CREATE_ID,
                            CREATE_DATETIME)
                           (select sg.group_id,
                                   ua.user_seq,
                                   USER,
                                   SYSDATE
                              from sec_group sg,
                                   sec_user ua
                             where sg.group_name like UPPER('%super%')
                                   and ua.application_user_id = 'RMS_ADMIN');

-- Inserting the Distinct country from addr and outloc tables to country_attrib table for item_costing
insert into country_attrib (country_id,
                            localized_ind,
                            item_cost_tax_incl_ind,
                            default_po_cost,
                            default_deal_cost,
                            default_cost_comp_cost)
                     select country_id,
                            'N',
                            'N',
                            'BC',
                            'BC',
                            'BC'
                       from
                         (select distinct country_id
                            from addr
                           where primary_addr_ind = 'Y'
                             and addr_type = '01'
                             and module in ('ST','WH','WFST','PTNR','SUPP')
                          UNION
                          select distinct outloc_country_id country_id
                            from outloc)tbl
                           where not exists (select 1
                                               from country_attrib ca
                                              where ca.country_id = tbl.country_id);

prompt 'Starting File: create_item.sql';
@@create_item.sql &num_items &tran_level;

prompt 'Starting File: populate_rpm_dept_aggregration.sql';
@@populate_rpm_dept_aggregration.sql;

prompt 'Starting File: populate_rpm_futureretail.sql';
@@populate_rpm_futureretail.sql;

commit;

prompt 'Execution of Demo Data script has completed.';
prompt 'Please check RmsDemoData.log for any errors.';

spool off
