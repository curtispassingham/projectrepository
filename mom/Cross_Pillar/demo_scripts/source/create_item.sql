
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

VARIABLE cmd VARCHAR2(1000);

EXECUTE DBMS_RANDOM.INITIALIZE(999999);

--installerize
--ACCEPT num_items PROMPT 'Number of items to create: '
define num_items = &1;

--installerize
--PROMPT
--PROMPT Please enter the transaction level you want for these items:
--PROMPT 1 - Line
--PROMPT 2 - Line Extension
--PROMPT 3 - Variant
--ACCEPT tran_level PROMPT 'Transaction level: '
define tran_level = &2;

BEGIN
	IF &tran_level = 1 THEN
		:cmd := 'start populate_item_data.sql &num_items';
	ELSIF &tran_level = 2 THEN
		:cmd := 'start populate_item_tranlevel2_data.sql &num_items 2';
	ELSIF &tran_level = 3 THEN
		:cmd := 'start populate_item_tranlevel3_data.sql &num_items 3';	
	END IF;
END;
/

spool off

SET HEADING OFF
SET FEEDBACK OFF
SET ECHO OFF
SET VERIFY OFF
SET LINESIZE 10000
spool whattorun.sql;
PRINT :cmd;
spool off;
SET HEADING ON
SET FEEDBACK ON
SET VERIFY ON
SET LINESIZE 80
spool RmsDemoData.log append
@whattorun.sql;


