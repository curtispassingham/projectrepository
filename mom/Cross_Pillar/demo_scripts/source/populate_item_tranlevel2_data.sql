SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
DECLARE

CURSOR get_subclass (p_count_row IN NUMBER) IS

     SELECT subclass, class, dept
     FROM subclass
     WHERE dept IN (SELECT dept
                    FROM deps
                    WHERE purchase_type = '0')
     AND ROWNUM <= p_count_row
     ORDER BY ROWNUM DESC;

     -- the "rownum" and "order by" here are added in order to aid selection
     -- of different subclass, class, dept combinations

CURSOR get_system_options IS

     SELECT check_digit_ind, round_lvl, round_to_inner_pct, round_to_case_pct, round_to_layer_pct, round_to_pallet_pct, rpm_ind, supplier_sites_ind,base_country_id
     FROM system_options;

CURSOR get_item_seq_nextval IS

     SELECT item_chkdig_sequence.NEXTVAL
     FROM sys.dual;

CURSOR get_chkdigit_mod(p_item_seq_nextval IN NUMBER) IS

   SELECT mod(system_options.cd_modulus -
             mod( ((mod(  TRUNC(p_item_seq_nextval / 1),       10)  * system_options.cd_weight_1) +
              (mod(  TRUNC(p_item_seq_nextval / 10),      10)  * system_options.cd_weight_2) +
              (mod(  TRUNC(p_item_seq_nextval / 100),     10)  * system_options.cd_weight_3) +
              (mod(  TRUNC(p_item_seq_nextval / 1000),    10)  * system_options.cd_weight_4) +
              (mod(  TRUNC(p_item_seq_nextval / 10000),   10)  * system_options.cd_weight_5) +
              (mod(  TRUNC(p_item_seq_nextval / 100000),  10)  * system_options.cd_weight_6) +
              (mod(  TRUNC(p_item_seq_nextval / 1000000), 10)  * system_options.cd_weight_7) +
              (mod(  TRUNC(p_item_seq_nextval / 10000000),10)  * system_options.cd_weight_8)),
             system_options.cd_modulus), 10)
      from system_options;

CURSOR get_cost_zone_group(p_count_row IN NUMBER) IS

   SELECT czg.zone_group_id, czg.description, cd.code_desc
   FROM code_detail cd,
        cost_zone_group czg
   WHERE czg.cost_level = cd.code
   AND cd.code_type = 'CZGP'
   AND ROWNUM <= p_count_row
        ORDER BY ROWNUM DESC;

        -- the "rownum" and "order by" here are added in order to aid selection
        -- of different code zone groups

CURSOR get_period IS

   SELECT *
   FROM period;

CURSOR get_supplier(p_item IN item_master.item%TYPE, p_count_row IN NUMBER, p_supplier_sites_ind IN system_options.supplier_sites_ind%TYPE) IS

   SELECT s1.supplier, s1.sup_name name, s1.currency_code
   FROM sups s1
   WHERE NVL(to_char(s1.supplier_parent),'X') = decode(p_supplier_sites_ind,'N', 'X',to_char(s1.supplier_parent))
   AND NOT EXISTS (SELECT 'x'
           FROM item_supplier is1
           WHERE is1.supplier = s1.supplier
           AND is1.item = p_item)
           AND NOT EXISTS (SELECT 'x'
                 FROM item_master im
                 WHERE im.item = p_item
                 AND im.item_level > im.tran_level
                 AND NOT EXISTS (SELECT 'x'
                       FROM item_supplier isp
                       WHERE isp.supplier = s1.supplier
                       AND isp.item = im.item_parent))
   AND ROWNUM <= p_count_row
   ORDER BY ROWNUM DESC;

       -- the "rownum" and "order by" here are added in order to aid selection
       -- of different countries

CURSOR get_country_msob(p_item IN item_master.item%TYPE, p_supplier IN sups.supplier%TYPE) IS

   SELECT c.country_id, c.country_desc
     FROM country c
    WHERE NOT EXISTS (SELECT 'x'
                        FROM packitem p
                       WHERE pack_no = p.item
                         AND EXISTS (SELECT 'x'
                                       FROM item_supp_country isc2
                                      WHERE isc2.item = p.item
                                        AND isc2.supplier = p_supplier
                                        AND isc2.origin_country_id = c.country_id))
      AND (EXISTS (SELECT 'X'
                     FROM addr
                    WHERE key_value_1 = to_char(p_supplier)
                      AND module ='SUPP'
                      AND primary_addr_ind = 'Y'
                      AND addr_type = '04'
                      AND country_id = c.country_id));

CURSOR get_primary_supp_ind(p_item IN item_master.item%TYPE, p_supplier IN sups.supplier%TYPE) IS
       SELECT primary_supp_ind
       FROM item_supplier
       WHERE item = p_item
       AND supplier = p_supplier;

CURSOR get_item_lead_time(p_supplier IN sups.supplier%TYPE) IS
       SELECT default_item_lead_time
       FROM sups
       WHERE supplier = p_supplier;

CURSOR get_store(p_item IN item_master.item%TYPE, p_count_row IN NUMBER) IS

       SELECT vs.store, vs.store_name
       FROM store vs
       WHERE NOT EXISTS (SELECT 'x'
                         FROM item_loc
                         WHERE item = p_item
                         AND loc = vs.store)
       AND ROWNUM <= p_count_row
       ORDER BY ROWNUM DESC;

       -- the "rownum" and "order by" here are added in order to aid selection
       -- of different stores

CURSOR get_item(p_item IN item_master.item%TYPE) IS

       SELECT *
       FROM item_master
       WHERE item = p_item;

CURSOR get_primary_supp_country(p_item IN item_master.item%TYPE) IS

       SELECT supplier, origin_country_id
       FROM item_supp_country
       WHERE item = p_item
       AND primary_supp_ind = 'Y'
       AND primary_country_ind = 'Y';

CURSOR get_vat(p_item IN item_master.item%TYPE,
               p_loc IN item_loc.loc%TYPE,
               p_vdate IN DATE) IS

       SELECT v.vat_code, v.vat_rate
       FROM vat_item v, store s
       WHERE v.item = p_item
       AND s.store = p_loc
       AND s.vat_region = v.vat_region
       AND v.vat_type in('R','B')
       AND v.active_date = (SELECT MAX(v2.active_date)
                    FROM vat_item v2
                    WHERE v2.vat_region = v.vat_region
                    AND v2.item = p_item
                    AND v.vat_type in ('R','B')
                    AND v2.active_date <= p_vdate);

CURSOR get_class_vat_ind(p_class IN class.class%TYPE,
                         p_dept IN deps.dept%TYPE) IS

   SELECT class_vat_ind
   FROM class
   WHERE class = p_class
   AND dept = p_dept;

CURSOR get_inv_adj_reason(p_count_row IN NUMBER) IS
   SELECT reason
     FROM inv_adj_reason
    WHERE reason != 98
      AND ROWNUM <= p_count_row
    ORDER BY ROWNUM DESC;

CURSOR get_prev_soh_qty(p_loc_type IN item_loc.loc_type%TYPE, p_loc IN item_loc.loc%TYPE, p_item IN item_master.item%TYPE) IS
          SELECT stock_on_hand
   FROM item_loc_soh
   WHERE loc_type = DECODE(p_loc_type, 'I', 'W', p_loc_type)
   AND LOC = p_loc
   AND ITEM = p_item;

CURSOR get_cogs_ind(p_reason IN inv_adj_reason.reason%TYPE) IS
   SELECT cogs_ind
        FROM inv_adj_reason
       WHERE reason = p_reason;

CURSOR get_tif_seq_nextval IS

   SELECT tif_explode_sequence.NEXTVAL
        FROM sys.dual;

CURSOR get_item_levels(p_item IN item_master.item%TYPE) IS
   SELECT item, item_parent, item_grandparent
   FROM item_master
   WHERE item = p_item;

CURSOR get_brand_name(p_brand IN item_master.brand_name%TYPE) IS
        SELECT brand_name
        FROM brand
        WHERE brand_name=p_brand;

CURSOR get_prod_classification IS
        SELECT code
      FROM code_detail
      WHERE code_type='PCLA' and rownum='1';

v_num_items      NUMBER;
v_tran_level      NUMBER;
v_dept         deps.dept%TYPE;
v_dept_name      deps.dept_name%TYPE;
v_purchase_type    deps.purchase_type%TYPE;
v_class              class.class%TYPE;
v_class_name      class.class_name%TYPE;
v_subclass         subclass.subclass%TYPE;
v_sub_name          subclass.sub_name%TYPE;
v_item_seq_nextval     NUMBER;
v_chkdigit_mod      NUMBER;
v_item         item_master.item%TYPE;
v_item_parent      item_master.item%TYPE;
v_item_grandparent   item_master.item%TYPE;
v_store_ord_mult   item_master.store_ord_mult%TYPE;
v_zone_group_id      cost_zone_group.zone_group_id%TYPE;
v_zone_group_desc   cost_zone_group.description%TYPE;
v_code_desc      code_detail.code_desc%TYPE;
v_period_rec      period%ROWTYPE;
v_supplier      sups.supplier%TYPE;
v_sup_name      sups.sup_name%TYPE;
v_currency_code      sups.currency_code%TYPE;
v_country_id      country.country_id%TYPE;
v_country_desc          country.country_desc%TYPE;
v_primary_supp_ind      item_supplier.primary_supp_ind%TYPE;
v_lead_time      sups.default_item_lead_time%TYPE;
v_check_digit_ind   system_options.check_digit_ind%TYPE;
v_round_lvl      system_options.round_lvl%TYPE;
v_round_to_inner_pct   system_options.round_to_inner_pct%TYPE;
v_round_to_case_pct   system_options.round_to_case_pct%TYPE;
v_round_to_layer_pct   system_options.round_to_layer_pct%TYPE;
v_round_to_pallet_pct   system_options.round_to_pallet_pct%TYPE;
v_rpm_ind               system_options.rpm_ind%TYPE;
v_supplier_sites_ind    system_options.supplier_sites_ind%TYPE;
v_inner_pack_size   item_supp_country.inner_pack_size%TYPE;
v_supp_pack_size   item_supp_country.supp_pack_size%TYPE;
v_ti         item_supp_country.ti%TYPE;
v_hi         item_supp_country.hi%TYPE;
v_unit_retail      item_loc.unit_retail%TYPE;
v_selling_unit_retail   item_loc.selling_unit_retail%TYPE;
v_selling_uom      item_loc.selling_uom%TYPE;
v_multi_units      item_loc.multi_units%TYPE;
v_multi_unit_retail   item_loc.multi_unit_retail%TYPE;
v_multi_selling_uom   item_loc.multi_selling_uom%TYPE;
v_curr_code       sups.currency_code%TYPE;
v_loc_type      item_loc.loc_type%TYPE;
v_loc         item_loc.loc%TYPE;
v_store_name      store.store_name%TYPE;
v_primary_supplier   item_supp_country.supplier%TYPE;
v_primary_country   item_supp_country.origin_country_id%TYPE;
v_vat_code      vat_item.vat_code%TYPE;
v_vat_rate      vat_item.vat_rate%TYPE;
v_class_vat_ind      class.class_vat_ind%TYPE;
v_unit_cost      item_supp_country.unit_cost%TYPE;
v_taxable_ind      item_loc.taxable_ind%TYPE;
v_status      item_loc.status%TYPE;
v_av_cost      item_loc_soh.av_cost%TYPE;
v_error_code      NUMBER;
v_error_text      VARCHAR2(200);
v_iteration      NUMBER;
v_num_sub_rows      NUMBER;
v_num_supp_rows      NUMBER;
v_num_country_rows   NUMBER;
v_num_store_rows   NUMBER;
v_num_czg_rows      NUMBER;
v_count_sub_row      NUMBER := 1;
v_count_supp_row   NUMBER := 1;
v_count_country_row   NUMBER := 1;
v_count_store_row   NUMBER := 1;
v_count_czg_row      NUMBER := 1;
v_rowid         VARCHAR2(25);
v_count_inv_adj_row   NUMBER := 1;
v_num_inv_adj_rows    NUMBER;
v_inv_adj_reason   inv_adj_reason.reason%TYPE;
v_prev_qty      inv_adj.prev_qty%TYPE;
v_adj_qty      inv_adj.adj_qty%TYPE;
v_pack_comp_soh      item_loc_soh.pack_comp_soh%TYPE;
v_packsku_qty      packitem_breakout.pack_item_qty%TYPE;
v_non_sellable_qty   item_loc_soh.non_sellable_qty%TYPE;
v_tran_code      tran_data.tran_code%TYPE;
v_cogs_ind      inv_adj_reason.cogs_ind%TYPE;
v_total_cost      tran_data_a.total_cost%TYPE;
v_total_retail      tran_data_a.total_retail%TYPE;
v_gl_ref_no       tran_data.gl_ref_no%TYPE;
v_pgm_name       tran_data.pgm_name%TYPE;
v_tif_seq_nextval   NUMBER;
bk_data       pm_retail_api_sql.item_pricing_table;
v_count              NUMBER;
v_error_message         rtk_errors.rtk_text%type;
v_ce_process_id         cost_event.cost_event_process_id%type;
v_itemloc_TBL           OBJ_ITEMLOC_TBL                          := OBJ_ITEMLOC_TBL();
v_user                  cost_event.user_id%type                  DEFAULT get_user;
v_fc_excp               EXCEPTION;
v_num_brand_rows        NUMBER;
v_count_brand_row       NUMBER :=1;
v_brand_name            item_master.brand_name%TYPE;
v_prod_class            item_master.product_classification%TYPE;
v_country               item_country.country_id%TYPE;
v_pm_retail_excp        EXCEPTION;
v_calc_retail_excp      EXCEPTION;

BEGIN

     DBMS_OUTPUT.PUT_LINE('Created the following items:');

     v_num_items := &num_items;
     v_tran_level := &tran_level;

     SELECT COUNT(*)
     INTO v_num_sub_rows
     FROM subclass;

     SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_sub_rows))))
     INTO v_count_sub_row
     FROM dual;

     SELECT COUNT(*)
     INTO v_num_supp_rows
     FROM sups;

     SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_supp_rows))))
     INTO v_count_supp_row
     FROM dual;

     SELECT COUNT(*)
     INTO v_num_country_rows
     FROM country;

     SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_country_rows))))
     INTO v_count_country_row
     FROM dual;

     SELECT COUNT(*)
     INTO v_num_store_rows
     FROM store;

     SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_store_rows))))
     INTO v_count_store_row
     FROM dual;

     SELECT COUNT(*)
     INTO v_num_czg_rows
     FROM store;

     SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_czg_rows))))
     INTO v_count_czg_row
     FROM dual;

    SELECT COUNT(*)
     INTO v_num_brand_rows
    FROM brand;

    SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, LENGTH(TO_CHAR(v_num_brand_rows))))
     INTO v_count_brand_row
     FROM dual;

     FOR v_iteration IN 1..v_num_items LOOP

        IF v_count_sub_row > v_num_sub_rows THEN
           v_count_sub_row := SUBSTR(TO_CHAR(v_count_sub_row), 1, TO_NUMBER(LENGTH(TO_CHAR(v_count_sub_row))) - 1);
        END IF;

        OPEN get_subclass(v_count_sub_row);
        FETCH get_subclass INTO v_subclass, v_class, v_dept;
        CLOSE get_subclass;

        OPEN get_item_seq_nextval;
        FETCH get_item_seq_nextval INTO v_item_seq_nextval;
        CLOSE get_item_seq_nextval;

        OPEN get_chkdigit_mod(v_item_seq_nextval);
        FETCH get_chkdigit_mod INTO v_chkdigit_mod;
        CLOSE get_chkdigit_mod;

        v_item := (v_item_seq_nextval * 10) + v_chkdigit_mod;

        IF v_count_czg_row > v_num_czg_rows THEN
           v_count_czg_row := SUBSTR(TO_CHAR(v_count_czg_row), 1, TO_NUMBER(LENGTH(TO_CHAR(v_count_czg_row))) - 1);
        END IF;

        OPEN get_cost_zone_group(v_count_czg_row);
        FETCH get_cost_zone_group INTO v_zone_group_id, v_zone_group_desc, v_code_desc;
        CLOSE get_cost_zone_group;

      OPEN get_brand_name(v_count_brand_row);
      FETCH get_brand_name into v_brand_name;
      CLOSE get_brand_name;

      OPEN get_prod_classification;
      FETCH get_prod_classification into v_prod_class;
      CLOSE get_prod_classification;

        v_store_ord_mult := 'E';

        INSERT INTO item_master
        (item,
         item_number_type,
         pack_ind,
         item_level,
         tran_level,
         item_aggregate_ind,
         diff_1_aggregate_ind,
         diff_2_aggregate_ind,
         diff_3_aggregate_ind,
         diff_4_aggregate_ind,
         dept,
         class,
         subclass,
         status,
         item_desc,
         short_desc,
         desc_up,
         primary_ref_item_ind,
         cost_zone_group_id,
         standard_uom,
         merchandise_ind,
         store_ord_mult,
         forecast_ind,
         catch_weight_ind,
         const_dimen_ind,
         simple_pack_ind,
         contains_inner_ind,
         sellable_ind,
         orderable_ind,
         gift_wrap_ind,
         ship_alone_ind,
         create_datetime,
         last_update_id,
         last_update_datetime,
         item_xform_ind,
         inventory_ind,
         perishable_ind,
         notional_pack_ind,
         soh_inquiry_at_pack_ind,
         brand_name,
         product_classification)
        VALUES
        (TO_CHAR(v_item),
         'ITEM',
         'N',
         1,
         v_tran_level,
         'N',
         'N',
         'N',
         'N',
         'N',
         v_dept,
         v_class,
         v_subclass,
         'W',
         'Test Item '||TO_CHAR(v_item),
         'Test Item '||TO_CHAR(v_item),
         'TEST ITEM '||TO_CHAR(v_item),
         'N',
         v_zone_group_id,
         'EA',
         'Y',
         v_store_ord_mult,
         'N',
         'N',
         'N',
         'N',
         'N',
         'Y',
         'Y',
         'N',
         'N',
         SYSDATE,
         v_user,
         SYSDATE,
         'N',
         'Y',
         'N',
         'N',
         'N',
         v_brand_Name,
         v_prod_class);

        DBMS_OUTPUT.PUT_LINE('');
        DBMS_OUTPUT.PUT_LINE('Item Level 1'||' '||TO_CHAR(v_item));

        OPEN get_period;
        FETCH get_period INTO v_period_rec;
        CLOSE get_period;

        INSERT INTO vat_item
        (item, vat_region, active_date, vat_type, vat_code, vat_rate, reverse_vat_ind, create_date, create_id,
        create_datetime, last_update_datetime, last_update_id)
        SELECT TO_CHAR(v_item), vd.vat_region, vcr1.active_date, vd.vat_type, vd.vat_code,
      vcr1.vat_rate, vd.reverse_vat_ind, TRUNC(SYSDATE), v_user, SYSDATE, SYSDATE, v_user
        FROM vat_deps vd,
           vat_code_rates vcr1
        WHERE vd.dept = v_dept
        AND vcr1.vat_code = vd.vat_code
        AND vcr1.active_date = (SELECT MAX (vcr2.active_date)
                 FROM vat_code_rates vcr2
                 WHERE vcr2.vat_code = vd.vat_code
                 AND vcr2.active_date <= TRUNC(SYSDATE));

        INSERT INTO uda_item_lov
        (item, uda_id, uda_value, create_datetime, last_update_datetime, last_update_id)
        SELECT TO_CHAR(v_item), a.uda_id, a.uda_value, SYSDATE, SYSDATE, v_user
        FROM uda_item_defaults a, uda
        WHERE a.dept = v_dept
        AND NVL(a.class, v_class) = v_class
        AND NVL(a.subclass, v_subclass) = v_subclass
        AND a.uda_value IS NOT NULL
        AND uda.uda_id = a.uda_id
        AND uda.display_type = 'LV'
        AND a.hierarchy_value = (SELECT MAX(b.hierarchy_value)
                  FROM uda_item_defaults b
                  WHERE b.dept = v_dept
                  AND NVL(b.class, v_class) = v_class
                  AND NVL(b.subclass, v_subclass) = v_subclass
                  AND b.uda_id = a.uda_id);

        INSERT INTO item_chrg_head
        (item, from_loc, to_loc, from_loc_type, to_loc_type)
        SELECT DISTINCT TO_CHAR(v_item), d.from_loc, d.to_loc, d.from_loc_type, d.to_loc_type
        FROM dept_chrg_head d
        WHERE d.dept = v_dept
        AND NOT EXISTS (SELECT 'x'
              FROM item_chrg_head
              WHERE item = v_item
              and from_loc = d.from_loc
              and to_loc = d.to_loc);

        INSERT INTO item_chrg_detail
        (item, from_loc, to_loc, comp_id, from_loc_type,
        to_loc_type, comp_rate, per_count, per_count_uom, up_chrg_group,
        comp_currency, display_order)
        SELECT DISTINCT v_item, d.from_loc, d.to_loc,
        d.comp_id, d.from_loc_type, d.to_loc_type, d.comp_rate, d.per_count,
        d.per_count_uom, d.up_chrg_group, d.comp_currency, e.display_order
        FROM dept_chrg_detail d, elc_comp e
        WHERE d.dept = v_dept
        AND d.comp_id = e.comp_id
        AND NOT EXISTS (SELECT 'x'
              FROM item_chrg_detail i
              WHERE i.item = v_item
              AND i.from_loc = d.from_loc
              AND i.to_loc = d.to_loc
              AND i.comp_id = d.comp_id);

        IF v_count_supp_row > v_num_supp_rows THEN
           v_count_supp_row := SUBSTR(TO_CHAR(v_count_supp_row), 1, TO_NUMBER(LENGTH(TO_CHAR(v_count_supp_row))) - 1);
        END IF;

        OPEN get_system_options;
        FETCH get_system_options INTO v_check_digit_ind,
                  v_round_lvl,
                  v_round_to_inner_pct,
                  v_round_to_case_pct,
                  v_round_to_layer_pct,
                  v_round_to_pallet_pct,
                  v_rpm_ind,
                  v_supplier_sites_ind,
                  v_country;
        CLOSE get_system_options;


        OPEN get_supplier(v_item, v_count_supp_row, v_supplier_sites_ind);
        FETCH get_supplier INTO v_supplier, v_sup_name, v_currency_code;
        CLOSE get_supplier;

        INSERT INTO item_supplier
        (item, supplier, primary_supp_ind, inner_name, case_name,
         pallet_name, direct_ship_ind, create_datetime,
         last_update_datetime, last_update_id)
        VALUES(v_item, v_supplier, 'Y', 'INR', 'CS', 'PAL', 'N',
         SYSDATE, SYSDATE, v_user);

        IF v_count_country_row > v_num_country_rows THEN
           v_count_country_row := SUBSTR(TO_CHAR(v_count_country_row), 1, TO_NUMBER(LENGTH(TO_CHAR(v_count_country_row))) - 1);
        END IF;

        OPEN get_country_msob(v_item, v_supplier);
        FETCH get_country_msob INTO v_country_id, v_country_desc;
        CLOSE get_country_msob;

        OPEN get_primary_supp_ind (v_item, v_supplier);
        FETCH get_primary_supp_ind INTO v_primary_supp_ind;
        CLOSE get_primary_supp_ind;

        OPEN get_item_lead_time (v_supplier);
        FETCH get_item_lead_time INTO v_lead_time;
        CLOSE get_item_lead_time;


        v_inner_pack_size := 10;
        v_supp_pack_size := 10;
        v_ti := 1;
        v_hi := 1;
        --v_unit_cost := 25;

        SELECT TO_NUMBER(SUBSTR(TO_CHAR(ABS(DBMS_RANDOM.RANDOM)), 1, 2))
   INTO v_unit_cost
   FROM DUAL;

        INSERT INTO item_supp_country
        (item, supplier, origin_country_id, primary_supp_ind, primary_country_ind, cost_uom,
         unit_cost, lead_time, default_uop, inner_pack_size, supp_pack_size, ti, hi,
         round_lvl, round_to_inner_pct, round_to_layer_pct, round_to_case_pct, round_to_pallet_pct,
         create_datetime, last_update_datetime, last_update_id)
        VALUES
        (v_item, v_supplier, v_country_id, v_primary_supp_ind, 'Y', 'EA', v_unit_cost, v_lead_time,
         'EA', v_inner_pack_size, v_supp_pack_size, v_ti, v_hi, v_round_lvl, v_round_to_inner_pct, v_round_to_layer_pct, v_round_to_case_pct,
         v_round_to_pallet_pct, SYSDATE, SYSDATE, v_user);

        INSERT INTO item_supp_manu_country
        (item, supplier, manu_country_id, primary_manu_ctry_ind)
        VALUES
        (v_item, v_supplier, v_country_id, 'Y');

        /*  This section has been replaced by calls to the package pm_retail_api_sql below

        OPEN get_retail_zone_group(v_item);
        FETCH get_retail_zone_group INTO v_retail_zone_group_id;
        CLOSE get_retail_zone_group;

        OPEN get_zone(v_retail_zone_group_id);
        FETCH get_zone INTO v_zone_id, v_base_retail_ind;

        v_unit_retail := 1.58;
        v_selling_unit_retail := 1.67;
        v_selling_uom := 'EA';
        v_multi_units := 5;
        v_multi_unit_retail := 2.35;
        v_multi_selling_uom := 'EA';

       WHILE get_zone%FOUND LOOP

        INSERT INTO item_zone_price
        (item, zone_group_id, zone_id, unit_retail, selling_unit_retail, selling_uom,
         multi_units, multi_unit_retail, multi_selling_uom, base_retail_ind,
         create_datetime, last_update_datetime, last_update_id, publish_ind)
        VALUES
        (v_item, v_retail_zone_group_id, v_zone_id, v_unit_retail, v_selling_unit_retail, v_selling_uom,
           v_multi_units, v_multi_unit_retail, v_multi_selling_uom, v_base_retail_ind, SYSDATE, SYSDATE, v_user, 'N');

            FETCH get_zone INTO v_zone_id, v_base_retail_ind;

       END LOOP;

       CLOSE get_zone;

       OPEN get_rpm_zone_group (v_dept, v_class, v_subclass);
       FETCH get_rpm_zone_group into v_sort_order, v_rpm_regular_zone_group, v_rpm_markup_calc_type, v_rpm_markup_percent, v_rpm_merch_retail_def_id;
       CLOSE get_rpm_zone_group;

       OPEN get_rpm_zone(v_rpm_regular_zone_group);
       FETCH get_rpm_zone INTO v_rpm_zone_id;

       WHILE get_rpm_zone%FOUND LOOP

          INSERT INTO rpm_item_zone_price
          (item_zone_price_id,
           item,
           zone_id,
           standard_retail,
           standard_retail_currency,
           standard_uom,
           selling_retail,
           selling_retail_currency,
           selling_uom,
           multi_units,
           multi_unit_retail,
           multi_unit_retail_currency,
           multi_selling_uom)
          SELECT
           rpm_item_zone_price_seq.NEXTVAL,
           v_item,
           v_rpm_zone_id,
           v_unit_retail,
           v_currency_code,
           v_selling_uom,
           v_selling_unit_retail,
           v_currency_code,
           v_selling_uom,
           v_multi_units,
           v_multi_unit_retail,
           v_currency_code,
           v_multi_selling_uom
          FROM item_zone_price
          WHERE item = v_item
          AND zone_group_id = v_retail_zone_group_id
          AND base_retail_ind = 'Y';

          FETCH get_rpm_zone INTO v_rpm_zone_id;

       END LOOP;

       CLOSE get_rpm_zone;

       */
       
       if v_rpm_ind = 'Y' then
          
          pm_retail_api_sql.get_item_pricing_info(bk_data, v_item, v_dept, v_class, v_subclass, 'USD', v_unit_cost);
          
          if bk_data(1).return_code = 'FALSE' then
	         v_error_message := bk_data(1).error_message;
             raise v_pm_retail_excp;
          end if;		   
	       
          FOR v_count IN 1 .. bk_data.count LOOP
          
             bk_data(v_count).selling_uom := 'EA';
          
          END LOOP;
          
          pm_retail_api_sql.set_item_pricing_info(bk_data);
          
          v_unit_retail          := bk_data(1).unit_retail;
          v_selling_unit_retail  := bk_data(1).selling_unit_retail;
          v_selling_uom          := bk_data(1).selling_uom;
          v_multi_units          := bk_data(1).multi_units;
          v_multi_unit_retail    := bk_data(1).multi_unit_retail;
          v_multi_selling_uom    := bk_data(1).multi_selling_uom;
          
       end if;   

       v_loc_type := 'S';

       IF v_count_store_row > v_num_store_rows THEN
           v_count_store_row := SUBSTR(TO_CHAR(v_count_store_row), 1, TO_NUMBER(LENGTH(TO_CHAR(v_count_store_row))) - 1);
       END IF;

       OPEN get_store(v_item, v_count_store_row);
       FETCH get_store INTO v_loc, v_store_name;
       CLOSE get_store;

       INSERT INTO item_supp_country_loc
       (item, supplier, origin_country_id, loc,
        loc_type, primary_loc_ind, unit_cost, round_lvl, round_to_inner_pct,
        round_to_case_pct, round_to_layer_pct, round_to_pallet_pct,
        supp_hier_type_1, supp_hier_lvl_1, supp_hier_type_2, supp_hier_lvl_2,
        supp_hier_type_3, supp_hier_lvl_3, pickup_lead_time, create_datetime,
        last_update_datetime, last_update_id)
        SELECT v_item, isc.supplier,
        isc.origin_country_id, v_loc, 'W', 'N', isc.unit_cost, sim.round_lvl,
        sim.round_to_inner_pct, sim.round_to_case_pct, sim.round_to_layer_pct,
        sim.round_to_pallet_pct, isc.supp_hier_type_1, isc.supp_hier_lvl_1,
        isc.supp_hier_type_2, isc.supp_hier_lvl_2, isc.supp_hier_type_3,
        isc.supp_hier_lvl_3, isc.pickup_lead_time, SYSDATE, SYSDATE, v_user
        FROM item_supp_country isc, sups s, sup_inv_mgmt sim, wh w
        WHERE isc.item = v_item
        AND isc.supplier = nvl(v_supplier, isc.supplier)
        AND isc.origin_country_id = nvl(v_country_id, isc.origin_country_id)
        AND s.supplier = isc.supplier
        AND s.inv_mgmt_lvl = 'A'
        AND w.wh = v_loc
        AND sim.supplier = isc.supplier
        AND sim.dept = v_dept
        AND sim.location = w.physical_wh
        AND NOT EXISTS (SELECT 'x'
                    FROM item_supp_country_loc isl
                    WHERE isl.item = v_item
                    AND isl.loc = v_loc
                    AND isl.supplier = isc.supplier
                    AND isl.origin_country_id = isc.origin_country_id)
        UNION ALL
        SELECT v_item, isc.supplier, isc.origin_country_id, v_loc, 'W', 'N',
        isc.unit_cost, sim.round_lvl, sim.round_to_inner_pct, sim.round_to_case_pct,
        sim.round_to_layer_pct, sim.round_to_pallet_pct, isc.supp_hier_type_1, isc.supp_hier_lvl_1,
        isc.supp_hier_type_2, isc.supp_hier_lvl_2, isc.supp_hier_type_3,
        isc.supp_hier_lvl_3, isc.pickup_lead_time, SYSDATE, SYSDATE, v_user
        FROM item_supp_country isc, sups s, sup_inv_mgmt sim, wh w
        WHERE isc.item = v_item
        AND isc.supplier = nvl(v_supplier, isc.supplier)
        AND isc.origin_country_id = nvl(v_country_id, isc.origin_country_id)
        AND s.supplier = isc.supplier
        AND s.inv_mgmt_lvl = 'L'
        AND w.wh = v_loc
        AND sim.supplier = isc.supplier
        AND sim.dept IS NULL
        AND sim.location = w.physical_wh
        AND NOT EXISTS (SELECT 'x'
                  FROM item_supp_country_loc isl
                  WHERE isl.item = v_item
                  AND isl.loc = v_loc
                  AND isl.supplier = isc.supplier
                  AND isl.origin_country_id = isc.origin_country_id)
        UNION ALL
        SELECT v_item, isc.supplier, isc.origin_country_id, v_loc, 'W', 'N',
        isc.unit_cost, isc.round_lvl, isc.round_to_inner_pct, isc.round_to_case_pct,
        isc.round_to_layer_pct, isc.round_to_pallet_pct, isc.supp_hier_type_1, isc.supp_hier_lvl_1,
        isc.supp_hier_type_2, isc.supp_hier_lvl_2, isc.supp_hier_type_3,
        isc.supp_hier_lvl_3, isc.pickup_lead_time, SYSDATE, SYSDATE, v_user
        FROM item_supp_country isc, wh w, sups s
        WHERE isc.item = v_item
        AND w.wh = v_loc
        AND isc.supplier = nvl(v_supplier, isc.supplier)
        AND isc.origin_country_id = nvl(v_country_id,isc.origin_country_id)
        AND s.supplier = isc.supplier
        AND ((v_loc_type = 'W'
        AND s.inv_mgmt_lvl = 'A'
        AND NOT EXISTS (SELECT 'x'
                    FROM sup_inv_mgmt sim2
                    WHERE sim2.supplier = isc.supplier
                    AND sim2.dept = v_dept
                    AND sim2.location = w.physical_wh))
        OR (v_loc_type = 'W'
        AND s.inv_mgmt_lvl = 'L'
        AND NOT EXISTS (SELECT 'x'
                 FROM sup_inv_mgmt sim2
                 WHERE sim2.supplier = isc.supplier
                 AND sim2.location = w.physical_wh)))
        AND NOT EXISTS (SELECT 'x'
                 FROM item_supp_country_loc isl
                 WHERE isl.item = v_item
                 AND isl.loc = v_loc
                 AND isl.supplier = isc.supplier
                 AND isl.origin_country_id = isc.origin_country_id)
        UNION ALL
        SELECT v_item, isc.supplier,
        isc.origin_country_id, v_loc, v_loc_type, 'N', isc.unit_cost, isc.round_lvl,
        isc.round_to_inner_pct, isc.round_to_case_pct, isc.round_to_layer_pct,
        isc.round_to_pallet_pct, isc.supp_hier_type_1, isc.supp_hier_lvl_1,
        isc.supp_hier_type_2, isc.supp_hier_lvl_2, isc.supp_hier_type_3,
        isc.supp_hier_lvl_3, isc.pickup_lead_time, SYSDATE, SYSDATE, v_user
        FROM item_supp_country isc, sups s
        WHERE isc.item = v_item
        AND isc.supplier = nvl(v_supplier, isc.supplier)
        AND isc.origin_country_id = nvl(v_country_id, isc.origin_country_id)
        AND s.supplier = isc.supplier
        AND (v_loc_type = 'S'
        OR s.inv_mgmt_lvl in ('S', 'D'))
        AND NOT EXISTS (SELECT 'x'
                 FROM item_supp_country_loc isl
                 WHERE isl.item = v_item
                 AND isl.loc = v_loc
                 AND isl.supplier = isc.supplier
                 AND isl.origin_country_id = isc.origin_country_id);

        INSERT INTO item_supp_country_bracket_cost
        (item, supplier, origin_country_id, location, loc_type, default_bracket_ind,
        bracket_value1, unit_cost, bracket_value2, sup_dept_seq_no)
        SELECT v_item, v_supplier, v_country_id, nvl(v_loc, iscl.loc),
        'W', iscbc.default_bracket_ind, iscbc.bracket_value1, iscbc.unit_cost,
        iscbc.bracket_value2, iscbc.sup_dept_seq_no
        FROM item_supp_country_bracket_cost iscbc,
             item_supp_country_loc iscl
        WHERE iscbc.item = v_item
        AND iscbc.supplier = v_supplier
        AND iscbc.origin_country_id = v_country_id
        AND iscbc.location IS NULL
        AND iscbc.item = iscl.item
        AND iscbc.supplier = iscl.supplier
        AND iscbc.origin_country_id = iscl.origin_country_id
        AND iscl.loc = nvl(v_loc, iscl.loc)
        AND iscl.loc_type = 'W'
        AND NOT EXISTS (SELECT 'x'
                        FROM item_supp_country_bracket_cost iscbc2
                        WHERE iscbc2.item = v_item
                        AND iscbc2.supplier = v_supplier
                        AND iscbc2.origin_country_id = v_country_id
                        AND iscbc2.location = nvl(v_loc, iscl.loc)
                        AND iscbc2.bracket_value1 = iscbc.bracket_value1 and ROWNUM = 1);

        UPDATE item_supp_country_loc isl
        SET primary_loc_ind = 'Y',
         last_update_datetime = SYSDATE,
         last_update_id = v_user
   WHERE isl.item = v_item
   AND isl.loc = v_loc
   AND isl.supplier = NVL(v_supplier, supplier)
   AND isl.origin_country_id = NVL(v_country_id, origin_country_id)
   AND NOT EXISTS (SELECT 'x'
                   FROM item_supp_country_loc isl3
                   WHERE isl3.item = isl.item
                   AND isl3.loc != isl.loc
                   AND isl3.supplier = NVL(v_supplier, supplier)
                   AND isl3.origin_country_id = NVL(v_country_id, origin_country_id)
                   AND isl3.primary_loc_ind = 'Y');

        INSERT INTO item_supp_country_bracket_cost
        (item, supplier, origin_country_id,
        location, loc_type, default_bracket_ind, bracket_value1, unit_cost,
        bracket_value2, sup_dept_seq_no)
        SELECT v_item, v_supplier, v_country_id, nvl(v_loc, iscl.loc),
        'W', sbc.default_bracket_ind, sbc.bracket_value1, iscl.unit_cost,
        sbc.bracket_value2, sbc.sup_dept_seq_no
        FROM item_supp_country_loc iscl,
             sup_bracket_cost sbc
        WHERE iscl.item = v_item and iscl.supplier = v_supplier
        AND iscl.origin_country_id = v_country_id
        AND iscl.loc = nvl(v_loc, iscl.loc)
        AND iscl.loc_type = 'W'
        AND iscl.supplier = sbc.supplier
        AND sbc.dept = v_dept
        AND NOT EXISTS (SELECT 'x'
                        FROM item_supp_country_bracket_cost iscbc2
                        WHERE iscbc2.item = v_item
                        AND iscbc2.supplier = v_supplier
                        AND iscbc2.origin_country_id = v_country_id
                        AND iscbc2.location = nvl(v_loc, iscl.loc)
                        AND iscbc2.bracket_value1 = sbc.bracket_value1
                        AND ROWNUM = 1);

        INSERT INTO item_supp_country_bracket_cost
        (item, supplier, origin_country_id,
        location, loc_type, default_bracket_ind, bracket_value1, unit_cost,
        bracket_value2, sup_dept_seq_no)
        SELECT v_item, v_supplier, v_country_id, nvl(v_loc, iscl.loc),
        'W', sbc.default_bracket_ind, sbc.bracket_value1, iscl.unit_cost,
        sbc.bracket_value2, sbc.sup_dept_seq_no
        FROM item_supp_country_loc iscl,
             sup_bracket_cost sbc
        WHERE iscl.item = v_item
        AND iscl.supplier = v_supplier
        AND iscl.origin_country_id = v_country_id
        AND iscl.loc = NVL(v_loc, iscl.loc)
        AND iscl.loc_type = 'W'
        AND iscl.supplier = sbc.supplier
        AND sbc.dept IS NULL
        AND NOT EXISTS (SELECT 'x'
                        FROM item_supp_country_bracket_cost iscbc2
                        WHERE iscbc2.item = v_item
                        AND iscbc2.supplier = v_supplier
                        AND iscbc2.origin_country_id = v_country_id
                        AND iscbc2.location = nvl(v_loc, iscl.loc)
                        AND iscbc2.bracket_value1 = sbc.bracket_value1
                        AND ROWNUM = 1);

        OPEN get_primary_supp_country(v_item);
        FETCH get_primary_supp_country INTO v_primary_supplier, v_primary_country;
        CLOSE get_primary_supp_country;

        v_taxable_ind := 'Y';
        v_status := 'A';

        if v_rpm_ind = 'N' then
        
           if RETAIL_API_SQL.CALC_RETAIL(v_error_message,
                                         v_selling_unit_retail,    
                                         v_selling_uom,       
                                         v_multi_units,       
                                         v_multi_unit_retail, 
                                         v_multi_selling_uom, 
                                         v_curr_code,     
                                         v_item,              
                                         v_dept,              
                                         v_class,             
                                         v_subclass,          
                                         v_loc,          
                                         v_loc_type) = FALSE then
              raise v_calc_retail_excp;      
            end if;
            v_unit_retail :=  v_selling_unit_retail;          
        end if;   
        
        INSERT INTO item_loc
        (item, loc, loc_type, unit_retail, selling_unit_retail, selling_uom,
         clear_ind, taxable_ind, local_item_desc, local_short_desc, ti, hi,
         store_ord_mult, status, status_update_date, primary_supp, primary_cntry,
         create_datetime, last_update_datetime, last_update_id, store_price_ind,rpm_ind)
        VALUES
         (v_item, v_loc, v_loc_type, v_unit_retail, v_selling_unit_retail,
          v_selling_uom, 'N', 'Y', 'Test Item '||TO_CHAR(v_item), 'Test Item '||TO_CHAR(v_item),
          v_ti, v_hi, v_store_ord_mult, 'A', v_period_rec.vdate, v_primary_supplier,
          v_primary_country, SYSDATE, SYSDATE, v_user, 'N','N');

         insert into item_country (item,country_id)
                           values (v_item,v_country);
   -- start routine for creating children

   /*

   OPEN get_tif_seq_nextval;
   FETCH get_tif_seq_nextval INTO v_tif_seq_nextval;
   CLOSE get_tif_seq_nextval;

   INSERT into tif_explode
   (seq_no, item, store, current_rate_ind, changed_ind)
        SELECT v_tif_seq_nextval, v_item, v_loc, 'Y', 'N'
        FROM item_master
        WHERE item = v_item
        AND item_level = tran_level;

        */

        OPEN get_item_seq_nextval;
        FETCH get_item_seq_nextval INTO v_item_seq_nextval;
        CLOSE get_item_seq_nextval;

        OPEN get_chkdigit_mod(v_item_seq_nextval);
        FETCH get_chkdigit_mod INTO v_chkdigit_mod;
        CLOSE get_chkdigit_mod;

        v_item_parent := v_item;

        v_item := (v_item_seq_nextval * 10) + v_chkdigit_mod;

        INSERT INTO item_temp
        (item, item_number_type, item_level, item_desc, existing_item_parent)
   VALUES
    (v_item, 'ITEM', 2, 'Test Item'||' '||TO_CHAR(v_item), v_item_parent);

    DBMS_OUTPUT.PUT_LINE('Item Level 2'||' '||TO_CHAR(v_item));

    INSERT INTO item_master
    (item, item_number_type, format_id, prefix,
     item_parent, item_grandparent, pack_ind, item_level, tran_level,
     item_aggregate_ind, diff_1, diff_1_aggregate_ind, diff_2,
     diff_2_aggregate_ind, diff_3, diff_3_aggregate_ind, diff_4,
     diff_4_aggregate_ind, dept, class, subclass, status, item_desc, short_desc,
     desc_up, primary_ref_item_ind, cost_zone_group_id,
     standard_uom, uom_conv_factor, package_size, package_uom, merchandise_ind,
     store_ord_mult, forecast_ind, original_retail, mfg_rec_retail,
     retail_label_type, retail_label_value, handling_temp, handling_sensitivity,
     catch_weight_ind, waste_type,
     waste_pct, default_waste_pct, const_dimen_ind, simple_pack_ind,
     contains_inner_ind, sellable_ind, orderable_ind, pack_type, order_as_type,
     comments, gift_wrap_ind, ship_alone_ind, check_uda_ind, create_datetime,
     last_update_id, last_update_datetime, item_xform_ind, inventory_ind,
     order_type, sale_type, deposit_item_type, container_item,
     deposit_in_price_per_uom, perishable_ind, notional_pack_ind,
         soh_inquiry_at_pack_ind,brand_name,product_classification)
     SELECT it.item, it.item_number_type,
     it.format_id, im.prefix, it.existing_item_parent, im.item_parent,
     im.pack_ind, it.item_level, im.tran_level, 'N', it.diff_1, 'N', it.diff_2,
     'N', it.diff_3, 'N', it.diff_4, 'N', im.dept, im.class, im.subclass, 'W',
     it.item_desc, im.short_desc, upper(it.item_desc), 'N',
     im.cost_zone_group_id, im.standard_uom,
     im.uom_conv_factor, im.package_size, im.package_uom, im.merchandise_ind,
     im.store_ord_mult, im.forecast_ind, im.original_retail, im.mfg_rec_retail,
     im.retail_label_type, im.retail_label_value, im.handling_temp,
     im.handling_sensitivity, im.catch_weight_ind,
     im.waste_type, im.waste_pct, im.default_waste_pct, im.const_dimen_ind,
     im.simple_pack_ind, im.contains_inner_ind, im.sellable_ind,
     im.orderable_ind, im.pack_type, im.order_as_type, im.comments,
     im.gift_wrap_ind, im.ship_alone_ind, im.check_uda_ind, SYSDATE, v_user, SYSDATE,
     im.item_xform_ind, im.inventory_ind, im.order_type, im.sale_type,
     im.deposit_item_type, im.container_item, im.deposit_in_price_per_uom,
     im.perishable_ind, im.notional_pack_ind, im.soh_inquiry_at_pack_ind,im.brand_name,im.product_classification
     FROM item_master im, item_temp it
     WHERE im.item = it.existing_item_parent;

     INSERT INTO item_supplier
     (item, supplier, primary_supp_ind, vpn, supp_label,
      consignment_rate, supp_diff_1, supp_diff_2, supp_diff_3, supp_diff_4,
      pallet_name, case_name, inner_name, supp_discontinue_date, direct_ship_ind,
      last_update_datetime, last_update_id, create_datetime)
     SELECT it.item, supplier, primary_supp_ind, NULL, NULL,
            consignment_rate, NULL, NULL, NULL, NULL, pallet_name, case_name,
            inner_name, supp_discontinue_date, direct_ship_ind, SYSDATE, v_user, SYSDATE
     FROM item_supplier its, item_temp it
     WHERE its.item = it.existing_item_parent;

     INSERT INTO item_loc
     (item, loc, item_parent, item_grandparent, loc_type,
     unit_retail, clear_ind, taxable_ind, local_item_desc, local_short_desc, ti,
     hi, store_ord_mult, status, status_update_date, daily_waste_pct,
     meas_of_each, meas_of_price, uom_of_price, primary_variant,
     primary_cost_pack, primary_supp, primary_cntry, selling_unit_retail,
     selling_uom, last_update_datetime, last_update_id, create_datetime,
     inbound_handling_days, store_price_ind,rpm_ind)
     SELECT it.item, i.loc, it.existing_item_parent, i.item_parent, i.loc_type,
     i.unit_retail, 'N', i.taxable_ind, it.item_desc, 'Test Item'||' '||TO_CHAR(v_item),
     i.ti, i.hi, i.store_ord_mult, i.status, v_period_rec.vdate, i.daily_waste_pct,
     i.meas_of_each, i.meas_of_price, i.uom_of_price, i.primary_variant, NULL, i.primary_supp,
     i.primary_cntry, i.selling_unit_retail, i.selling_uom, SYSDATE, v_user, SYSDATE, NULL, 'N' ,'N'
     FROM item_loc i, item_temp it, store s
     WHERE i.item = it.existing_item_parent
     AND s.store = i.loc
     UNION ALL
     SELECT it.item, i.loc, it.existing_item_parent, i.item_parent,
     i.loc_type, i.unit_retail, 'N', i.taxable_ind, it.item_desc,
     'Test Item'||' '||TO_CHAR(v_item), i.ti, i.hi, i.store_ord_mult, i.status,
     v_period_rec.vdate, i.daily_waste_pct, i.meas_of_each, i.meas_of_price, i.uom_of_price,
     i.primary_variant, NULL, i.primary_supp, i.primary_cntry,
     i.selling_unit_retail, i.selling_uom, SYSDATE, v_user, SYSDATE, NULL, 'N' ,'N'
     FROM item_loc i, item_temp it, wh
     WHERE i.item = it.existing_item_parent
     AND wh.wh = i.loc;

     /*

     OPEN get_tif_seq_nextval;
   FETCH get_tif_seq_nextval INTO v_tif_seq_nextval;
   CLOSE get_tif_seq_nextval;

   INSERT into tif_explode
   (seq_no, item, store, current_rate_ind, changed_ind)
        SELECT v_tif_seq_nextval, v_item, v_loc, 'Y', 'N'
        FROM item_master
        WHERE item = v_item
        AND item_level = tran_level;

     */

     INSERT INTO item_supp_country
     (item, supplier, origin_country_id, unit_cost,
     lead_time, supp_pack_size, inner_pack_size, round_lvl, round_to_inner_pct,
     round_to_case_pct, round_to_layer_pct, round_to_pallet_pct, min_order_qty,
     max_order_qty, packing_method, primary_supp_ind, primary_country_ind,
     default_uop, ti, hi, supp_hier_type_1, supp_hier_lvl_1, supp_hier_type_2,
     supp_hier_lvl_2, supp_hier_type_3, supp_hier_lvl_3, pickup_lead_time,
     last_update_datetime, last_update_id, create_datetime, cost_uom,
     tolerance_type, max_tolerance, min_tolerance)
     SELECT v_item, supplier, origin_country_id, unit_cost, lead_time,
     supp_pack_size, inner_pack_size, round_lvl, round_to_inner_pct,
     round_to_case_pct, round_to_layer_pct, round_to_pallet_pct,
     min_order_qty, max_order_qty, packing_method, primary_supp_ind,
     primary_country_ind, default_uop, ti, hi,
     supp_hier_type_1, supp_hier_lvl_1, supp_hier_type_2, supp_hier_lvl_2,
     supp_hier_type_3, supp_hier_lvl_3, pickup_lead_time, SYSDATE, v_user, SYSDATE,
     cost_uom, tolerance_type, max_tolerance, min_tolerance
     FROM item_supp_country
     WHERE item = v_item_parent;

        INSERT INTO item_supp_manu_country
        (item, supplier, manu_country_id, primary_manu_ctry_ind)
        SELECT v_item, supplier, manu_country_id, primary_manu_ctry_ind
        FROM item_supp_manu_country
        WHERE item = v_item_parent;

     INSERT INTO item_supp_country_loc
     (item, supplier, origin_country_id, loc,
      loc_type, primary_loc_ind, unit_cost, round_lvl, round_to_inner_pct,
      round_to_case_pct, round_to_layer_pct, round_to_pallet_pct,
      supp_hier_type_1, supp_hier_lvl_1, supp_hier_type_2, supp_hier_lvl_2,
      supp_hier_type_3, supp_hier_lvl_3, pickup_lead_time, last_update_datetime,
      last_update_id, create_datetime)
     SELECT v_item, i.supplier,
     i.origin_country_id, i.loc, i.loc_type, i.primary_loc_ind, i.unit_cost,
     i.round_lvl, i.round_to_inner_pct, i.round_to_case_pct,
     i.round_to_layer_pct, i.round_to_pallet_pct, i.supp_hier_type_1,
     i.supp_hier_lvl_1, i.supp_hier_type_2, i.supp_hier_lvl_2,
     i.supp_hier_type_3, i.supp_hier_lvl_3, i.pickup_lead_time, SYSDATE, v_user, SYSDATE
     FROM item_supp_country_loc i, store s
     WHERE item = v_item_parent
     AND s.store = i.loc
     UNION ALL
     SELECT v_item, i.supplier, i.origin_country_id, i.loc, i.loc_type,
     i.primary_loc_ind, i.unit_cost, i.round_lvl, i.round_to_inner_pct,
     i.round_to_case_pct, i.round_to_layer_pct, i.round_to_pallet_pct,
     i.supp_hier_type_1, i.supp_hier_lvl_1, i.supp_hier_type_2,
     i.supp_hier_lvl_2, i.supp_hier_type_3, i.supp_hier_lvl_3,
     i.pickup_lead_time, SYSDATE, v_user, SYSDATE
     FROM item_supp_country_loc i, wh w
     WHERE item = v_item_parent
     AND w.wh = i.loc;

     INSERT INTO item_supp_country_bracket_cost
     (item, supplier, origin_country_id, location, bracket_value1,
     loc_type, default_bracket_ind, unit_cost, bracket_value2, sup_dept_seq_no)
     SELECT v_item, supplier, origin_country_id,
     location, bracket_value1, loc_type, default_bracket_ind, unit_cost,
     bracket_value2, sup_dept_seq_no
     FROM item_supp_country_bracket_cost
     WHERE item = v_item_parent;

   INSERT INTO item_supp_uom
   (item, supplier, uom, value, last_update_datetime,
     last_update_id, create_datetime)
     SELECT v_item, supplier, uom, value, SYSDATE, v_user, SYSDATE
     FROM item_supp_uom
     WHERE item = v_item_parent;

     INSERT INTO item_supp_country_dim
     (item, supplier, origin_country, dim_object,
      presentation_method, length, width, height, lwh_uom, weight, net_weight,
      weight_uom, liquid_volume, liquid_volume_uom, stat_cube, tare_weight,
      tare_type, last_update_datetime, last_update_id, create_datetime)
     SELECT v_item, supplier, origin_country, dim_object, presentation_method, length,
     width, height, lwh_uom, weight, net_weight, weight_uom, liquid_volume,
     liquid_volume_uom, stat_cube, tare_weight, tare_type, SYSDATE, v_user, SYSDATE
     FROM item_supp_country_dim
     WHERE item = v_item_parent;

     if v_rpm_ind = 'Y' then
     
        pm_retail_api_sql.get_item_pricing_info(bk_data, v_item, v_dept, v_class, v_subclass, 'USD', v_unit_cost);

        if bk_data(1).return_code = 'FALSE' then
	       v_error_message := bk_data(1).error_message;
           raise v_pm_retail_excp;
        end if;	
	   
        FOR v_count IN 1 .. bk_data.count LOOP

           bk_data(v_count).selling_uom := 'EA';

        END LOOP;

        pm_retail_api_sql.set_item_pricing_info(bk_data);
        
        v_unit_retail          := bk_data(1).unit_retail;
        v_selling_unit_retail  := bk_data(1).selling_unit_retail;
        v_selling_uom          := bk_data(1).selling_uom;
        v_multi_units          := bk_data(1).multi_units;
        v_multi_unit_retail    := bk_data(1).multi_unit_retail;
        v_multi_selling_uom    := bk_data(1).multi_selling_uom;
        
     end if;

     INSERT INTO vat_item
     (item, vat_region, active_date, vat_type, vat_code,
     vat_rate, reverse_vat_ind, create_date, create_id, last_update_datetime, last_update_id,
     create_datetime)
     SELECT v_item, vat_region, active_date, vat_type, vat_code,
     vat_rate, vs1.reverse_vat_ind, TRUNC(SYSDATE), v_user, SYSDATE, v_user, SYSDATE
     FROM vat_item vs1
     WHERE vs1.item = v_item_parent
     AND vs1.active_date = (SELECT MAX(vs2.active_date)
                  FROM vat_item vs2
                  WHERE vs2.vat_region = vs1.vat_region
                  AND vs2.active_date <= TRUNC(SYSDATE)
                  AND vs2.item = v_item_parent);

     INSERT INTO item_ticket
     (item, ticket_type_id, po_print_type, print_on_pc_ind,
     ticket_over_pct, last_update_datetime, last_update_id, create_datetime)
     SELECT v_item, ticket_type_id, po_print_type, print_on_pc_ind, ticket_over_pct,
      SYSDATE, v_user, SYSDATE
      FROM item_ticket
      WHERE item = v_item_parent;

      INSERT INTO uda_item_lov
      (item, uda_id, uda_value, last_update_datetime,
     last_update_id, create_datetime)
     SELECT v_item, uda_id, uda_value, SYSDATE, v_user, SYSDATE
     FROM uda_item_lov
     WHERE item = v_item_parent;

     INSERT INTO uda_item_ff
     (item, uda_id, uda_text, last_update_datetime,
     last_update_id, create_datetime)
     SELECT v_item, uda_id, uda_text, SYSDATE, v_user, SYSDATE
     FROM uda_item_ff
     WHERE item = v_item_parent;

     INSERT INTO uda_item_date
     (item, uda_id, uda_date, last_update_datetime,
     last_update_id, create_datetime)
     SELECT v_item, uda_id, uda_date, SYSDATE, v_user, SYSDATE
     FROM uda_item_date
     WHERE item = v_item_parent;

     INSERT INTO item_hts
     (item, hts, import_country_id, origin_country_id,
     effect_from, effect_to, clearing_zone_id, status, last_update_datetime, last_update_id,
     create_datetime)
     SELECT v_item, hts, import_country_id, origin_country_id,
     effect_from, effect_to, clearing_zone_id, status, SYSDATE, v_user, SYSDATE
     FROM item_hts
     WHERE item = v_item_parent;

     INSERT INTO item_hts_assess
     (item, hts, import_country_id, origin_country_id,
     effect_from, effect_to, comp_id, cvb_code, comp_rate, per_count,
     per_count_uom, est_assess_value, nom_flag_1, nom_flag_2, nom_flag_3,
     nom_flag_4, nom_flag_5, display_order, last_update_datetime, last_update_id,
      create_datetime)
      SELECT v_item, hts, import_country_id, origin_country_id,
     effect_from, effect_to, comp_id, cvb_code, comp_rate, per_count,
     per_count_uom, est_assess_value, nom_flag_1, nom_flag_2, nom_flag_3,
     nom_flag_4, nom_flag_5, display_order, SYSDATE, v_user, SYSDATE
     FROM item_hts_assess
     WHERE item = v_item_parent;

     INSERT INTO cond_tariff_treatment
     (item, tariff_treatment, last_update_datetime, last_update_id, create_datetime)
     SELECT v_item, tariff_treatment, SYSDATE, v_user, SYSDATE
     FROM cond_tariff_treatment
     WHERE item = v_item_parent;

     INSERT INTO item_exp_head
     (item, supplier, item_exp_type, item_exp_seq,
     origin_country_id, zone_id, lading_port, discharge_port, zone_group_id,
     base_exp_ind, last_update_datetime, last_update_id, create_datetime)
     SELECT v_item, supplier, item_exp_type, item_exp_seq, origin_country_id, zone_id,
     lading_port, discharge_port, zone_group_id, base_exp_ind, SYSDATE, v_user, SYSDATE
     FROM item_exp_head
     WHERE item = v_item_parent;

     INSERT INTO item_exp_detail
     (item, supplier, item_exp_type, item_exp_seq,
     comp_id, cvb_code, comp_rate, comp_currency, per_count, per_count_uom,
     est_exp_value, nom_flag_1, nom_flag_2, nom_flag_3, nom_flag_4, nom_flag_5,
     display_order, last_update_datetime, last_update_id, create_datetime)
     SELECT v_item, supplier, item_exp_type, item_exp_seq, comp_id, cvb_code,
     comp_rate, comp_currency, per_count, per_count_uom, est_exp_value,
     nom_flag_1, nom_flag_2, nom_flag_3, nom_flag_4, nom_flag_5, display_order,
     SYSDATE, v_user, SYSDATE
     FROM item_exp_detail
     WHERE item = v_item_parent;

     INSERT INTO item_chrg_head
     (item, from_loc, to_loc, from_loc_type, to_loc_type)
      select v_item, from_loc, to_loc, from_loc_type, to_loc_type
      FROM item_chrg_head
      WHERE item = v_item_parent;

     INSERT INTO item_chrg_detail
     (item, from_loc, to_loc, comp_id, from_loc_type,
     to_loc_type, comp_rate, per_count, per_count_uom, up_chrg_group,
     comp_currency, display_order)
     SELECT v_item, from_loc, to_loc, comp_id,
     from_loc_type, to_loc_type, comp_rate, per_count, per_count_uom,
     up_chrg_group, comp_currency, display_order
     FROM item_chrg_detail
     WHERE item = v_item_parent;

     INSERT INTO item_seasons
     (item,
         season_id,
         phase_id,
    item_season_seq_no,
    create_datetime,
    last_update_datetime,
    last_update_id)
   SELECT    v_item,
      season_id,
      phase_id,
      rownum,
      SYSDATE,
      SYSDATE,
      v_user
   FROM item_seasons s, item_master i
   WHERE i.item = v_item_parent
   AND s.item = i.item_grandparent
   AND s.diff_id IS NULL;

      INSERT INTO item_import_attr
      (item, tooling, first_order_ind, amortize_base,
     open_balance, commodity, import_desc)
     SELECT v_item, tooling, first_order_ind,
     amortize_base, open_balance, commodity, import_desc
     FROM item_import_attr
     WHERE item = v_item_parent;

     DELETE timeline
          WHERE key_value_1 in (SELECT item
                              FROM item_master
                           WHERE (item_parent = v_item
                           OR item_grandparent = v_item_parent)
                          AND item_level <= tran_level)
        AND timeline_type = 'IT';

        INSERT INTO timeline
        (timeline_key, timeline_no, timeline_type,
         key_value_1, key_value_2, base_date,
         step_no, display_seq, original_date,
         revised_date, actual_date, reason_code,
         comment_desc, create_datetime, last_update_datetime, last_update_id)
        SELECT timeline_sequence.NEXTVAL, t.timeline_no, t.timeline_type,
               im.item, t.key_value_2, t.base_date,
               t.step_no, t.display_seq, t.original_date,
               t.revised_date, t.actual_date, t.reason_code,
               t.comment_desc, SYSDATE, SYSDATE, v_user
        FROM timeline t, item_master im
        WHERE t.key_value_1 = v_item_parent
        AND (item_parent  = t.key_value_1
        OR im.item_grandparent = t.key_value_1)
        AND im.item_level <= im.tran_level
        AND timeline_type = 'IT';

      INSERT INTO item_image (item,
                              image_name,
                              image_addr,
                              image_desc,
                              create_datetime,
                              last_update_datetime,
                              last_update_id,
                              create_id,
                              image_type,
                              primary_ind,
                              display_priority)
                       SELECT v_item,
                              image_name,
                              image_addr,
                              image_desc,
                              SYSDATE, 
                              SYSDATE,
                              v_user,
                              v_user,
                              image_type,
                              primary_ind,
                              display_priority
                         FROM item_image
                        WHERE item = v_item_parent;

        INSERT INTO item_loc_traits
        (item, loc, launch_date, qty_key_options,
         manual_price_entry, deposit_code, food_stamp_ind, wic_ind,
         proportional_tare_pct, fixed_tare_value, fixed_tare_uom,
         reward_eligible_ind, natl_brand_comp_item, return_policy, stop_sale_ind,
         elect_mtk_clubs, report_code, req_shelf_life_on_selection,
         req_shelf_life_on_receipt, ib_shelf_life, store_reorderable_ind, rack_size,
         full_pallet_item, in_store_market_basket, storage_location,
         alt_storage_location, create_datetime, last_update_id, last_update_datetime)
        SELECT v_item, i.loc, i.launch_date, i.qty_key_options, i.manual_price_entry,
        i.deposit_code, i.food_stamp_ind, i.wic_ind, i.proportional_tare_pct,
        i.fixed_tare_value, i.fixed_tare_uom, i.reward_eligible_ind,
        i.natl_brand_comp_item, i.return_policy, i.stop_sale_ind, i.elect_mtk_clubs,
        i.report_code, i.req_shelf_life_on_selection, i.req_shelf_life_on_receipt,
        i.ib_shelf_life, i.store_reorderable_ind, i.rack_size, i.full_pallet_item,
        i.in_store_market_basket, i.storage_location, i.alt_storage_location, SYSDATE,
        v_user, SYSDATE
        FROM item_loc_traits i, store s
        WHERE i.item = v_item_parent
        AND s.store = i.loc
        UNION ALL
        SELECT v_item, i.loc, i.launch_date, i.qty_key_options,
        i.manual_price_entry, i.deposit_code, i.food_stamp_ind, i.wic_ind,
        i.proportional_tare_pct, i.fixed_tare_value, i.fixed_tare_uom,
        i.reward_eligible_ind, i.natl_brand_comp_item, i.return_policy,
        i.stop_sale_ind, i.elect_mtk_clubs, i.report_code,
        i.req_shelf_life_on_selection, i.req_shelf_life_on_receipt, i.ib_shelf_life,
        i.store_reorderable_ind, i.rack_size, i.full_pallet_item,
        i.in_store_market_basket, i.storage_location, i.alt_storage_location, SYSDATE,
        v_user, SYSDATE
        FROM item_loc_traits i, wh
        WHERE i.item = v_item_parent
        AND wh.wh = i.loc;

   v_av_cost := v_unit_cost;

   INSERT INTO item_loc_soh
   (loc, loc_type, item, item_parent, item_grandparent,
     av_cost, unit_cost, stock_on_hand, soh_update_datetime,
     last_hist_export_date, in_transit_qty, pack_comp_intran, pack_comp_soh,
     tsf_reserved_qty, pack_comp_resv, tsf_expected_qty, pack_comp_exp, rtv_qty,
     non_sellable_qty, customer_resv, customer_backorder, pack_comp_cust_resv,
     pack_comp_cust_back, last_update_datetime, last_update_id, create_datetime,
     primary_supp, primary_cntry,pack_comp_non_sellable)
     SELECT DISTINCT il.loc,
     il.loc_type, v_item, il.item_parent, il.item_grandparent, v_av_cost, v_unit_cost, 0,
     NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     SYSDATE, v_user, SYSDATE, il.primary_supp,
      il.primary_cntry, 0
      FROM item_loc il, store s, wh
      WHERE il.item = v_item_parent
     AND ((s.store = il.loc
     --AND s.currency_code = v_currency_code
     AND il.loc_type = 'S')
     OR (wh.wh = il.loc
     --AND wh.currency_code = v_currency_code
     AND il.loc_type = 'W'));

     DELETE FROM item_temp;

     DELETE FROM temp_diff1;

     DELETE FROM temp_diff2;

     DELETE FROM temp_diff3;

     DELETE FROM temp_diff4;

     DELETE FROM temp_diff_duplicate;

     -- end routine for creating children

        -- start routine for approval

        OPEN get_item_levels(v_item);
        FETCH get_item_levels INTO v_item, v_item_parent, v_item_grandparent;
        CLOSE get_item_levels;

        DELETE FROM item_approval_error
   WHERE (item = v_item
   OR item = v_item_parent
   OR item = v_item_grandparent)
   AND override_ind = 'N';

        v_itemloc_TBL.EXTEND;
        if v_tran_level = 1 then
           v_itemloc_TBL(v_itemloc_TBL.COUNT) := OBJ_ITEMLOC_REC(v_item_parent, v_loc);
        elsif v_tran_level = 2 then
           v_itemloc_TBL(v_itemloc_TBL.COUNT) := OBJ_ITEMLOC_REC(v_item, v_loc);
        end if;

        if v_itemloc_TBL.COUNT > 0 then
           if FUTURE_COST_EVENT_SQL.ADD_NIL(v_error_message,
                                            v_ce_process_id,
                                            v_itemloc_TBL,
                                            v_user) = FALSE then
              raise v_fc_excp;
           end if;
        end if;

   if v_rpm_ind = 'N' then
   
      if RETAIL_API_SQL.CALC_RETAIL(v_error_message,
                                    v_selling_unit_retail,    
                                    v_selling_uom,       
                                    v_multi_units,       
                                    v_multi_unit_retail, 
                                    v_multi_selling_uom, 
                                    v_curr_code,     
                                    v_item,              
                                    v_dept,              
                                    v_class,             
                                    v_subclass,          
                                    v_loc,          
                                    v_loc_type) = FALSE then
         raise v_calc_retail_excp;      
       end if;
       v_unit_retail :=  v_selling_unit_retail;          
   end if; 
        
        if v_rpm_ind='N' then

		   UPDATE item_master
              SET status = 'A',
                  original_retail = v_unit_retail,
                  curr_selling_unit_retail = v_selling_unit_retail,
			      curr_selling_uom = 'EA',
                  last_update_datetime = SYSDATE,
                  last_update_id = v_user             
            WHERE (item = v_item
                         OR item = v_item_parent
                         OR item = v_item_grandparent);   

	    elsif v_rpm_ind='Y' then
		   
		   UPDATE item_master
              SET status = 'A',
                  original_retail = v_unit_retail,
                  last_update_datetime = SYSDATE,
                  last_update_id = v_user             
            WHERE (item = v_item
                         OR item = v_item_parent
                         OR item = v_item_grandparent);   
		 end if;

    DELETE FROM item_approval_error
   WHERE (item = v_item
   OR item = v_item_parent
   OR item = v_item_grandparent)
   AND override_ind = 'N';

   INSERT INTO repl_item_loc_updates
   (item, supplier, origin_country_id, location,
      loc_type, change_type)
      (SELECT DISTINCT item, NULL, NULL, -1, NULL, 'IM'
     FROM repl_item_loc
     WHERE item = v_item);

     INSERT INTO price_hist
     (tran_type, reason, event, item, loc, loc_type,
     unit_cost, unit_retail, selling_unit_retail, selling_uom, action_date,
     multi_units, multi_unit_retail, multi_selling_uom)
   VALUES
    (0, 0, NULL, v_item, 0, NULL, v_unit_cost, v_unit_retail,
    v_selling_unit_retail, v_selling_uom, v_period_rec.vdate, NULL, NULL, NULL);

    INSERT INTO price_hist
     (tran_type, reason, event, item, loc, loc_type,
     unit_cost, unit_retail, selling_unit_retail, selling_uom, action_date,
     multi_units, multi_unit_retail, multi_selling_uom)
   VALUES
    (0, 0, NULL, v_item_parent, 0, NULL, v_unit_cost, v_unit_retail,
    v_selling_unit_retail, v_selling_uom, v_period_rec.vdate, NULL, NULL, NULL);

    INSERT INTO price_hist
    (tran_type, reason, event, item, loc, loc_type,
     unit_cost, unit_retail, selling_unit_retail, selling_uom, action_date,
     multi_units, multi_unit_retail, multi_selling_uom)
   VALUES
    (0, 0, NULL, v_item, v_loc, v_loc_type, NULL,
    v_unit_retail, v_selling_unit_retail, v_selling_uom, v_period_rec.vdate,
    v_multi_units, v_multi_unit_retail, v_multi_selling_uom);

    INSERT INTO price_hist
    (tran_type, reason, event, item, loc, loc_type,
     unit_cost, unit_retail, selling_unit_retail, selling_uom, action_date,
     multi_units, multi_unit_retail, multi_selling_uom)
   VALUES
    (0, 0, NULL, v_item_parent, v_loc, v_loc_type, NULL,
    v_unit_retail, v_selling_unit_retail, v_selling_uom, v_period_rec.vdate,
    v_multi_units, v_multi_unit_retail, v_multi_selling_uom);

    DELETE FROM item_approval_error
   WHERE (item = v_item
   OR item = v_item_parent
   OR item = v_item_grandparent);

   v_count_sub_row := v_count_sub_row + 1;
   v_count_supp_row := v_count_supp_row + 1;
   v_count_country_row := v_count_country_row + 1;
        v_count_store_row := v_count_store_row + 1;
        v_count_czg_row := v_count_czg_row + 1;

   -- start routine for creating inventory

   IF v_count_inv_adj_row > v_num_inv_adj_rows THEN
           v_count_inv_adj_row := 1;
        END IF;

        OPEN get_inv_adj_reason(v_count_inv_adj_row);
        FETCH get_inv_adj_reason INTO v_inv_adj_reason;
        CLOSE get_inv_adj_reason;

        OPEN get_prev_soh_qty(v_loc_type, v_loc, v_item);
        FETCH get_prev_soh_qty INTO v_prev_qty;
        CLOSE get_prev_soh_qty;

        v_adj_qty := 100;

   INSERT INTO inv_adj
        (item, loc_type, location, adj_qty, reason, adj_date, prev_qty, user_id, adj_weight, adj_weight_uom)
        VALUES
        (v_item, v_loc_type, v_loc, v_adj_qty, v_inv_adj_reason, v_period_rec.vdate, v_prev_qty, v_user, NULL, NULL);

        v_pack_comp_soh := 0;
        v_packsku_qty := 0;
        v_non_sellable_qty := 0;

        UPDATE item_loc_soh
        SET stock_on_hand = stock_on_hand + v_adj_qty,
     soh_update_datetime = DECODE(v_adj_qty, 0, soh_update_datetime, SYSDATE),
     pack_comp_soh = pack_comp_soh + (v_pack_comp_soh * v_packsku_qty),
     non_sellable_qty = non_sellable_qty + v_non_sellable_qty,
     last_update_datetime = SYSDATE,
     last_update_id = v_user
   WHERE item = v_item
   AND loc = v_loc;

   v_tran_code := 22;

   OPEN get_cogs_ind(v_inv_adj_reason);
   FETCH get_cogs_ind INTO v_cogs_ind;
   CLOSE get_cogs_ind;

   IF v_cogs_ind = 'Y' THEN
           v_tran_code := 23;
        END If;

   v_total_cost := v_av_cost * v_adj_qty;
   v_total_retail := (v_unit_retail * v_adj_qty)/(1 + (v_vat_rate/100));
   v_gl_ref_no := v_inv_adj_reason;
   v_pgm_name := 'INVADJSK';

   INSERT INTO tran_data (item,
                          dept,
                          class,
                          subclass,
                          location,
                          loc_type,
                          tran_date,
                          tran_code,
                          units,
                          total_cost,
                          total_retail,
                          gl_ref_no,
                          pgm_name,
                          timestamp)
                   SELECT v_item,
                          v_dept,
                          v_class,
                          v_subclass,
                          v_loc,
                          v_loc_type,
                          v_period_rec.vdate,
                          v_tran_code,
                          v_adj_qty,
                          ROUND(v_total_cost, currencies.currency_cost_dec),
                          ROUND(v_total_retail, currencies.currency_rtl_dec),
                          v_gl_ref_no,
                          v_pgm_name,
                          SYSDATE
                     FROM currencies,
                          (SELECT store loc,
                                  currency_code
                             FROM store
                           UNION ALL
                           SELECT wh loc,
                                  currency_code
                             FROM wh) loc
                    WHERE loc.currency_code = currencies.currency_code
                      AND loc.loc           = v_loc;
   -- end routine for creating inventory

     END LOOP;

    if L10N_SQL.REFRESH_MV_L10N_ENTITY(v_error_message) = FALSE then
        raise v_fc_excp;
     end if;

     if GENERAL_DATA_INSTALL.ITEM_COST(v_error_message, 'Y') = FALSE then
        raise v_fc_excp;
     end if;

     if GENERAL_DATA_INSTALL.FUTURE_COST(v_error_message) = FALSE then
        raise v_fc_excp;
     end if;

     COMMIT;

EXCEPTION

      WHEN v_fc_excp THEN
         v_error_text := SUBSTR(v_error_message, 1, 200);
         DBMS_OUTPUT.PUT_LINE('Error text: '||v_error_text);
         ROLLBACK;

      WHEN v_pm_retail_excp THEN
         v_error_text := SUBSTR(v_error_message, 1, 200);
         DBMS_OUTPUT.PUT_LINE('Error text: '||v_error_text);
         ROLLBACK;

      WHEN v_calc_retail_excp THEN
         v_error_text := SUBSTR(v_error_message, 1, 200);
         DBMS_OUTPUT.PUT_LINE('Error text: '||v_error_text);
         ROLLBACK;

      WHEN OTHERS THEN
         v_error_code := SQLCODE;
         v_error_text := SUBSTR(SQLERRM, 1, 200);
         DBMS_OUTPUT.PUT_LINE('Error Code: '||v_error_code);
         DBMS_OUTPUT.PUT_LINE('Error text: '||v_error_text);
         ROLLBACK;

END;
/