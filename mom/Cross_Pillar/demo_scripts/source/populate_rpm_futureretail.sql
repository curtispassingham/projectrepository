DECLARE

   LP_vdate      DATE  := GET_VDATE;
   L_rpm_ind     SYSTEM_OPTIONS.RPM_IND%TYPE;
   
   cursor C_GET_RPM_IND is
      select rpm_ind
        from system_options;

BEGIN

   open C_GET_RPM_IND;
   fetch C_GET_RPM_IND into L_rpm_ind;
   close C_GET_RPM_IND;
   
   if L_rpm_ind = 'Y' then
   
      delete rpm_merch_node_zone_node_gtt;
      
      delete rpm_me_item_gtt;
      
      -- insert merchandise heirarchy combinations.  In this case, it's only tran item and parent items.
      insert into rpm_me_item_gtt(dept,
                                  class,
                                  subclass,
                                  item_parent,
                                  item,
                                  item_level)
         select distinct
                t.dept,
                t.class,
                t.subclass,
                t.item_parent,
                t.item,
                t.item_level
           from (select dept,  /*tran items with no parents*/
                        class,
                        subclass,
                        item,
                        NULL item_parent,
                        RPM_CONSTANTS.ITEM_MERCH_TYPE item_level
                   from item_master
                  where item_parent is NULL
                    and status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                    and sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    and item_level   = tran_level 
                 union all
                 select distinct  /*parent items with tran children*/
                        dept, 
                        class,
                        subclass,
                        item_parent item,
                        NULL item_parent,
                        RPM_CONSTANTS.PARENT_MERCH_TYPE item_level
                   from item_master
                  where item_parent is NOT NULL
                    and status       = RPM_CONSTANTS.APPROVED_ITEM_STATUS
                    and sellable_ind = RPM_CONSTANTS.SELLABLE_ITEM_IND_YES
                    and item_level   = tran_level) t;
      
      -- insert organizational heirarchy combinations.
      insert into rpm_merch_node_zone_node_gtt(dept,
                                               class,
                                               subclass,
                                               location,
                                               zone_node_type,
                                               zone_id)
         select distinct
                t.dept,
                t.class,
                t.subclass,
                rzl.location,
                rzl.loc_type,
                rzl.zone_id
           from (select distinct
                        dept,
                        class,
                        subclass
                   from rpm_me_item_gtt) t,
                rpm_merch_retail_def_expl mer,
                rpm_zone rz,
                rpm_zone_location rzl
          where mer.dept         = t.dept
            and mer.class        = t.class
            and mer.subclass     = t.subclass
            and rz.zone_group_id = mer.regular_zone_group
            and rzl.zone_id      = rz.zone_id;
               
      -- combine the merch and org hierarchy combinations
      merge into rpm_future_retail target
      using (select dept,
                    class,
                    subclass,
                    item,
                    zone_node_type,  
                    location,   
                    action_date, 
                    selling_retail,
                    currency_code,
                    selling_uom,
                    multi_units,
                    multi_unit_retail,
                    multi_selling_uom,
                    cur_hier_level,  
                    max_hier_level
               from (select rnk.dept,
                            rnk.class,
                            rnk.subclass,
                            rnk.item,
                            rnk.zone_node_type,  
                            rnk.location,   
                            rnk.action_date,
                            rnk.selling_retail,
                            rnk.currency_code,
                            rnk.selling_uom,
                            rnk.multi_units,
                            rnk.multi_unit_retail,
                            rnk.multi_selling_uom,
                            rnk.cur_hier_level,  
                            rnk.max_hier_level,
                            RANK() OVER (PARTITION BY rnk.item,
                                                      rnk.location
                                             ORDER BY rnk.selling_retail, 
                                                      rnk.currency_code, 
                                                      rnk.selling_uom) rank
                       from (select distinct
                                    fr.dept,
                                    fr.class,
                                    fr.subclass,
                                    fr.item,
                                    DECODE(s.zone_id,            /*if s.zone is null then set this to store or wh else zone node type.*/ 
                                           NULL, fr.zone_node_type, 
                                           RPM_CONSTANTS.ZONE_NODE_TYPE_ZONE) zone_node_type,  
                                    DECODE(s.zone_id,            /*if s.zone is null then set this to location else set to zone.*/
                                           NULL, fr.location, 
                                           s.zone_id) location,   
                                    (LP_vdate - 1) action_date,
                                    fr.unit_retail selling_retail,
                                    fr.currency_code,
                                    fr.selling_uom,
                                    fr.multi_units,
                                    fr.multi_unit_retail,
                                    fr.multi_selling_uom,
                                    case
                                       when im.item_level = RPM_CONSTANTS.PARENT_MERCH_TYPE and
                                            s.zone_id is NOT NULL then
                                          RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                       when im.item_level = RPM_CONSTANTS.PARENT_MERCH_TYPE and
                                            s.zone_id is NULL then
                                          RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                       when im.item_level = RPM_CONSTANTS.ITEM_MERCH_TYPE and
                                            s.zone_id is NOT NULL then
                                          RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                       else
                                          RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                    end as cur_hier_level,  
                                    case
                                       when im.item_level = RPM_CONSTANTS.PARENT_MERCH_TYPE and
                                            s.zone_id is NOT NULL then
                                          RPM_CONSTANTS.FR_HIER_PARENT_ZONE
                                       when im.item_level = RPM_CONSTANTS.PARENT_MERCH_TYPE and
                                            s.zone_id is NULL then
                                          RPM_CONSTANTS.FR_HIER_PARENT_LOC
                                       when im.item_level = RPM_CONSTANTS.ITEM_MERCH_TYPE and
                                            s.zone_id is NOT NULL then
                                          RPM_CONSTANTS.FR_HIER_ITEM_ZONE
                                       else
                                          RPM_CONSTANTS.FR_HIER_ITEM_LOC
                                    end as max_hier_level
                               from rpm_me_item_gtt im,
                                    (select im.dept,
                                            im.class, 
                                            im.subclass, 
                                            im.item,
                                            il.loc location,
                                            DECODE(il.loc_type, 
                                                   RPM_CONSTANTS.LOCATION_TYPE_STORE, RPM_CONSTANTS.ZONE_NODE_TYPE_STORE, 
                                                   RPM_CONSTANTS.ZONE_NODE_TYPE_WAREHOUSE) zone_node_type,
                                            loc.currency_code,
                                            il.unit_retail,
                                            il.selling_uom,
                                            il.multi_units,
                                            il.multi_unit_retail,
                                            il.multi_selling_uom
                                       from item_master im,
                                            item_loc il,
                                            (select store as location,
                                                    currency_code
                                               from store
                                             union all
                                             select wh as location,
                                                    currency_code
                                               from wh) loc
                                      where im.item = il.item
                                        and il.loc  = loc.location) fr,
                                    rpm_merch_node_zone_node_gtt s
                              where fr.dept           = im.dept
                                and fr.item           = im.item
                                and fr.dept           = s.dept (+)
                                and fr.class          = s.class (+)
                                and fr.subclass       = s.subclass (+)
                                and fr.location       = s.location (+)
                                and fr.zone_node_type = s.zone_node_type (+)) rnk)
              where rank = 1) source
      on (    target.item           = source.item
          and target.location       = source.location
          and target.zone_node_type = source.zone_node_type)
      when NOT MATCHED then
         insert(future_retail_id,
                dept,
                class,
                subclass,
                item,
                zone_node_type,
                location,
                action_date,
                selling_retail,
                selling_retail_currency,
                selling_uom,
                multi_units,
                multi_unit_retail,
                multi_unit_retail_currency,
                multi_selling_uom,
                clear_retail,
                clear_retail_currency,
                clear_uom,
                simple_promo_retail,
                simple_promo_retail_currency,
                simple_promo_uom,
                item_parent,
                diff_id,
                zone_id,
                max_hier_level,
                cur_hier_level,
                lock_version)
         values(RPM_FUTURE_RETAIL_SEQ.NEXTVAL,
                source.dept,
                source.class,
                source.subclass,
                source.item,
                source.zone_node_type,  
                source.location,   
                source.action_date, 
                source.selling_retail,
                source.currency_code,
                source.selling_uom,
                source.multi_units,
                source.multi_unit_retail,
                source.currency_code,      --multi_unit_retail_currency
                source.multi_selling_uom,
                source.selling_retail,     --clear_retail
                source.currency_code,      --clear_retail_currency
                source.selling_uom,        --clear_uom
                source.selling_retail,     --simple_promo_retail
                source.currency_code,      --simple_promo_retail_currency
                source.selling_uom,        --simple_promo_uom
                NULL,                      --item_parent
                NULL,                      --diff_1
                NULL,                      --zone_id
                source.cur_hier_level,  
                source.max_hier_level,
                0);
   end if;          

END;
/
