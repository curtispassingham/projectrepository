SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'DUTY6' CVB_CODE, 'DUTY US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'DUTYUS' CVB_CODE, '税额 US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'SELLCOMM' CVB_CODE, '销售员佣金计算基值' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'TDTYPE' CVB_CODE, '税额总计 US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'TDTYUS' CVB_CODE, '税额总计 US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'TEXP' CVB_CODE, '总费用' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'TEXPC' CVB_CODE, '国家（地区）总费用' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'TEXPZ' CVB_CODE, '区域总费用' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'VFD25US' CVB_CODE, '税额值的 25% US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'VFD50US' CVB_CODE, '税额值的 50% US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'VFD75US' CVB_CODE, '税额值的 75% US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 8 LANG, 'VFDUS' CVB_CODE, '税额值 US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO CVB_HEAD base USING
( SELECT CVB_CODE CVB_CODE, CVB_DESC CVB_DESC FROM CVB_HEAD_TL TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8)) USE_THIS
ON ( base.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE SET base.CVB_DESC = use_this.CVB_DESC;
--
DELETE FROM CVB_HEAD_TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
