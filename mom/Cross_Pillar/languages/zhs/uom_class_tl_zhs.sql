SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'AGG' UOM, 'AGG' UOM_TRANS, '含银量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'AUG' UOM, 'AUG' UOM_TRANS, '含金量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BA' UOM, 'BA' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BBL' UOM, 'BBL' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BE' UOM, 'BE' UOM_TRANS, '捆绑' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BG' UOM, 'BG' UOM_TRANS, '袋' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BI' UOM, 'BI' UOM_TRANS, '仓位' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BJ' UOM, 'BJ' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BK' UOM, 'BK' UOM_TRANS, '篮' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'BX' UOM, 'UOM' UOM_TRANS, '盒' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'C' UOM, 'C' UOM_TRANS, '摄氏度' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CA' UOM, 'CA' UOM_TRANS, '听' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CAR' UOM, 'CAR' UOM_TRANS, '克拉' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CBM' UOM, 'CBM' UOM_TRANS, '立方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CC' UOM, 'CC' UOM_TRANS, '立方厘米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CFT' UOM, 'CFT' UOM_TRANS, '立方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CG' UOM, 'CG' UOM_TRANS, '厘克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CGM' UOM, 'CGM' UOM_TRANS, '含量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CKG' UOM, 'CKG' UOM_TRANS, '含量（公斤）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CM' UOM, 'CM' UOM_TRANS, '厘米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CM2' UOM, 'CM2' UOM_TRANS, '平方厘米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CM3' UOM, 'CM3' UOM_TRANS, '立方厘米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CON' UOM, 'CON' UOM_TRANS, '集装箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CR' UOM, 'CR' UOM_TRANS, '板条箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CS' UOM, 'CS' UOM_TRANS, '货箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CT' UOM, 'CT' UOM_TRANS, '纸箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CTN' UOM, 'CTN' UOM_TRANS, '含量（吨）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CU' UOM, 'CU' UOM_TRANS, '立方' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CUR' UOM, 'CUR' UOM_TRANS, '居里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CY' UOM, 'CY' UOM_TRANS, '清除收益率' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CYG' UOM, 'CYG' UOM_TRANS, '清除收益率（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'CYK' UOM, 'CYK' UOM_TRANS, '清除收益率（千克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'D' UOM, 'D' UOM_TRANS, '旦尼尔' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'DEG' UOM, 'DEG' UOM_TRANS, '度' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'DOZ' UOM, 'AGG' UOM_TRANS, '一打' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'DPC' UOM, 'DOZ' UOM_TRANS, '十二件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'DPR' UOM, 'DPR' UOM_TRANS, '十二对' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'DS' UOM, 'ADS' UOM_TRANS, '剂量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'EA' UOM, 'EA' UOM_TRANS, '每个' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FBM' UOM, 'FBM' UOM_TRANS, '纤维米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FIB' UOM, 'FIB' UOM_TRANS, '纤维' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FT' UOM, 'FT' UOM_TRANS, '英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FT2' UOM, 'FT2' UOM_TRANS, '平方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FT3' UOM, 'FT3' UOM_TRANS, '立方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'FZ' UOM, 'FZ' UOM_TRANS, '液盎司' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'G' UOM, 'G' UOM_TRANS, '克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GBQ' UOM, 'GBQ' UOM_TRANS, '千兆贝可' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GCN' UOM, 'GCN' UOM_TRANS, '集装箱毛重' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GKG' UOM, 'GKG' UOM_TRANS, '含金量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GL' UOM, 'GL' UOM_TRANS, '加仑' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GM' UOM, 'GM' UOM_TRANS, '克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GR' UOM, 'GR' UOM_TRANS, '毛' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GRL' UOM, 'GRL' UOM_TRANS, 'HTSUPLD 自动插入' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GRS' UOM, 'GRS' UOM_TRANS, '毛' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'GVW' UOM, 'GVW' UOM_TRANS, '车辆毛重' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'HND' UOM, 'HND' UOM_TRANS, '百' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'HUN' UOM, 'HUN' UOM_TRANS, '百' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'HZ' UOM, 'HZ' UOM_TRANS, '赫兹' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'IN' UOM, 'IN' UOM_TRANS, '英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'IN2' UOM, 'IN2' UOM_TRANS, '平方英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'IN3' UOM, 'IN3' UOM_TRANS, '立方英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'IRC' UOM, 'IRC' UOM_TRANS, '公司收入代码' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'IRG' UOM, 'IRG' UOM_TRANS, '铱含量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'JR' UOM, 'JR' UOM_TRANS, '罐' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'JWL' UOM, 'JWL' UOM_TRANS, '焦耳' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'K' UOM, 'K' UOM_TRANS, '1,000' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KCAL' UOM, 'KCAL' UOM_TRANS, '千卡' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KG' UOM, 'KG' UOM_TRANS, '公斤' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KHZ' UOM, 'KHZ' UOM_TRANS, '千赫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KM' UOM, 'KM' UOM_TRANS, '公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KM2' UOM, 'KM2' UOM_TRANS, '平方公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KM3' UOM, 'KM3' UOM_TRANS, '立方公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KN' UOM, 'KN' UOM_TRANS, '千牛顿' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KPA' UOM, 'KPA' UOM_TRANS, '千帕斯卡' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KSB' UOM, 'KSB' UOM_TRANS, '千标准砖' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KTS' UOM, 'KTS' UOM_TRANS, '糖总计（公斤）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KVA' UOM, 'KVA' UOM_TRANS, '千伏安' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KVAR' UOM, 'KVAR' UOM_TRANS, '千伏安无功' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KW' UOM, 'KW' UOM_TRANS, '千瓦' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'KWH' UOM, 'KWH' UOM_TRANS, '千瓦时' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'L' UOM, 'L' UOM_TRANS, '升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'LB' UOM, 'LB' UOM_TRANS, '磅' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'LBS' UOM, 'LBS' UOM_TRANS, '磅' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'LIN' UOM, 'LIN' UOM_TRANS, '直线' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'LNM' UOM, 'LNM' UOM_TRANS, '直线米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'M' UOM, 'M' UOM_TRANS, '米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'M2' UOM, 'M2' UOM_TRANS, '平方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'M3' UOM, 'M3' UOM_TRANS, '立方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MBQ' UOM, 'MBQ' UOM_TRANS, '兆贝可' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MC' UOM, 'MC' UOM_TRANS, '毫居里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MG' UOM, 'MG' UOM_TRANS, '毫克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MHZ' UOM, 'MHZ' UOM_TRANS, '兆赫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'ML' UOM, 'ML' UOM_TRANS, '毫升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MM' UOM, 'MM' UOM_TRANS, '毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MM2' UOM, 'MM2' UOM_TRANS, '平方毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MM3' UOM, 'MM3' UOM_TRANS, '立方毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MPA' UOM, 'MPA' UOM_TRANS, '兆帕' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'MWH' UOM, 'MWH' UOM_TRANS, '兆瓦时' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'NA' UOM, 'NA' UOM_TRANS, '不可用' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'NO' UOM, 'NO' UOM_TRANS, '数量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'ODE' UOM, 'ODE' UOM_TRANS, '臭氧消耗当量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'OSG' UOM, 'OSG' UOM_TRANS, '含锇量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'OZ' UOM, 'OZ' UOM_TRANS, '盎司' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PACK' UOM, 'PACK' UOM_TRANS, '打包组合商品' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PAL' UOM, 'PAL' UOM_TRANS, '托盘' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PC' UOM, 'PC' UOM_TRANS, '件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PCS' UOM, 'PCS' UOM_TRANS, '件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PDG' UOM, 'PDG' UOM_TRANS, '含钯量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PF' UOM, 'PF' UOM_TRANS, '证明' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PFL' UOM, 'PFL' UOM_TRANS, '证明升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PK' UOM, 'PK' UOM_TRANS, '打包组合商品' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PO' UOM, 'PO' UOM_TRANS, '壶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PRS' UOM, 'PRS' UOM_TRANS, '对' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PT' UOM, 'PT' UOM_TRANS, '品脱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'PTG' UOM, 'PTG' UOM_TRANS, '含钚量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'QT' UOM, 'QT' UOM_TRANS, '夸脱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'RBA' UOM, 'RBA' UOM_TRANS, '已打包的棉花包' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'RHG' UOM, 'RHG' UOM_TRANS, '含铑量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'RPM' UOM, 'RPM' UOM_TRANS, '每分钟转数' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'RUG' UOM, 'RUG' UOM_TRANS, '含钌量（克）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'SBE' UOM, 'SBE' UOM_TRANS, '标准砖当量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'SF' UOM, 'SF' UOM_TRANS, '平方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'SME' UOM, 'SME' UOM_TRANS, '平方米当量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'SQ' UOM, 'SQ' UOM_TRANS, '平方' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'SQM' UOM, 'SQM' UOM_TRANS, '平方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'T' UOM, 'T' UOM_TRANS, '公吨' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'THM' UOM, 'THM' UOM_TRANS, '千米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'THS' UOM, 'THS' UOM_TRANS, '千单位' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'TNV' UOM, 'TNV' UOM_TRANS, '原始值（吨）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'TON' UOM, 'TON' UOM_TRANS, '吨' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'TS' UOM, 'TS' UOM_TRANS, '短缺（吨）' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'V' UOM, 'V' UOM_TRANS, '伏特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'W' UOM, 'W' UOM_TRANS, '瓦特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'WTS' UOM, 'WTS' UOM_TRANS, '瓦特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'X' UOM, 'X' UOM_TRANS, '无已收集单位' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'YD' UOM, 'YD' UOM_TRANS, '码' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'YD2' UOM, 'YD2' UOM_TRANS, '平方码' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 LANG, 'YD3' UOM, 'YD3' UOM_TRANS, '立方码' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
