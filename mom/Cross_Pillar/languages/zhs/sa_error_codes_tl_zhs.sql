SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAIL_WITHOUT_THEAD' ERROR_CODE, '部分事务处理。TTAIL 处于事务处理外。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAX_AMT_STIN' ERROR_CODE, '税金 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的税金。' REC_SOLUTION, '税额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAX_FIL_STIN' ERROR_CODE, 'TTAX 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TTAX 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAX_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TTAX 处于非法位置。' ERROR_DESC, '输入文件已损坏。TTAX 记录必须介于 THEAD 记录和 TTAIL 记录之间。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_CHECK_NO_REQ' ERROR_CODE, '支票号码为空或者数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的支票号码。' REC_SOLUTION, '支票号码无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_COUPON_NO_REQ' ERROR_CODE, '当支付手段类型商品组为优惠券时，优惠券编号不能为空。' ERROR_DESC, '请输入优惠券。' REC_SOLUTION, '缺少优惠券编号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_FIL_STIN' ERROR_CODE, 'TTEND 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TTEND 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TTEND 处于非法位置。' ERROR_DESC, '输入文件已损坏。TTEND 记录必须介于 THEAD 记录和 TTAIL 记录之间。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_OCA_STIN' ERROR_CODE, '原始货币金额 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的原始货币金额。' REC_SOLUTION, '货币金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_TAM_STIN' ERROR_CODE, '支付手段金额 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '请输入正确的支付手段金额。' REC_SOLUTION, '支付手段金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTEND_TTI_STIN' ERROR_CODE, '支付手段类型 ID 不属于支付手段类型商品组或者为空。' ERROR_DESC, '请从清单中选择支付手段类型。' REC_SOLUTION, '支付手段类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'UPC_NOT_FOUND' ERROR_CODE, 'RMS 中不存在此 UPC。' ERROR_DESC, '请从清单中选择 UPC。' REC_SOLUTION, '未找到 UPC' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'VENDOR_DATA_REQ' ERROR_CODE, '所需的供应商数据 - 以下至少一个字段中必须含有值：供应商发票编号、付款参考编号和交货证明编号。' ERROR_DESC, '至少输入以下一个值：供应商发票编号、付款参考编号和交货证明编号。' REC_SOLUTION, '缺少供应商' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'VENDOR_NO_REQ' ERROR_CODE, '商品供应商支出或费用供应商支出中，“THEAD 的供应商编号”为必需的字段。' ERROR_DESC, '请从清单中选择供应商编号。' REC_SOLUTION, '缺少供应商' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'VOID_NO_PYMT' ERROR_CODE, '作废事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_TAX' ERROR_CODE, '天结束事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DUP_TAXC' ERROR_CODE, '税务代码重复。此事务处理已发送至拒绝文件。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '税务代码重复' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FTAIL_FRC_STIN' ERROR_CODE, 'FTAIL 记录计数器 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'FTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_EXP_DATE' ERROR_CODE, '代金券的失效日期无效。' ERROR_DESC, '请输入有效的失效日期。失效日期的有效格式为 YYYMMDD。' REC_SOLUTION, '失效日期无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_NON_MERCH_CODE' ERROR_CODE, '费用供应商支出或商品供应商支出事务处理的原因代码无效。' ERROR_DESC, '请为事务处理选择有效的原因代码。' REC_SOLUTION, '非商品代码无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_TEND' ERROR_CODE, '仪表事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NON_MERCH_ITEM_NO_REQ' ERROR_CODE, '此物品记录需要非商品物品编号。' ERROR_DESC, '请选择一个合适的非商品品类。' REC_SOLUTION, '缺少非商品品类' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_TAX' ERROR_CODE, '无销售事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_CUST' ERROR_CODE, '拉出事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_TAX' ERROR_CODE, '拉出事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_IGTAX' ERROR_CODE, '泵测试事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_CUST' ERROR_CODE, '过账作废事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_CUST' ERROR_CODE, '油罐浸沾事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CASHIER_ID_REQ_BAL_LEVEL' ERROR_CODE, '由于是平衡级别，因此收银员 ID 是必需的。' ERROR_DESC, '请输入正确的收银员 ID。' REC_SOLUTION, '收银员 ID 无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CATT_FIL_STIN' ERROR_CODE, 'CATT 记录文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'CATT 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CATT_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。CATT 记录处于非法位置。' ERROR_DESC, '输入文件已损坏。CATT 记录必须在 TCUST 记录之后。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CC_NO_REQ' ERROR_CODE, '需要提供信用卡号。' ERROR_DESC, '请输入正确的信用卡号。' REC_SOLUTION, '缺少信用卡号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_CATT' ERROR_CODE, '结算事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_CUST' ERROR_CODE, '结算事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAIL_TRC_STIN' ERROR_CODE, 'TTAIL 事务处理记录计数器 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_IGTAX' ERROR_CODE, '结算事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_ITEM' ERROR_CODE, '结算事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_TAX' ERROR_CODE, '结算事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_TEND' ERROR_CODE, '结算事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_CATT' ERROR_CODE, '门店状况事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_CUST' ERROR_CODE, '门店状况事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_DISC' ERROR_CODE, '门店状况事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_IGTAX' ERROR_CODE, '门店状况事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_PYMT' ERROR_CODE, '门店状况事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_TAX' ERROR_CODE, '门店状况事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_TEND' ERROR_CODE, '门店状况事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CREDIT_PROMO_ID_STIN' ERROR_CODE, '信用促销 ID 无效。' ERROR_DESC, '请输入正确的信用促销 ID。' REC_SOLUTION, '信用促销无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CUST_ID_REQ' ERROR_CODE, '客户识别号是必需的。' ERROR_DESC, '请输入正确的客户识别号。' REC_SOLUTION, '缺少客户 ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CUST_ORD_ATTR_REQ' ERROR_CODE, '必须提供客户订单属性。' ERROR_DESC, '请通过“客户属性”屏幕输入客户订单属性。' REC_SOLUTION, '缺少客户属性' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CUST_ORD_DATE_REQDINTITEM' ERROR_CODE, '客户订单的对应 TITEM 中需要客户订单日期。' ERROR_DESC, '通过“客户属性”屏幕输入客户订单日期。预期格式为 YYYYMMDD' REC_SOLUTION, '缺少客户订单日期' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DATAUNEXPECTEDSTOREDAY' ERROR_CODE, '此门店营业日不应有数据。' ERROR_DESC, '门店或营业日无效。请检查“门店”、“公司关闭”、“公司关闭例外”和“关店”信息。' REC_SOLUTION, '门店营业日有意外数据' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_CATT' ERROR_CODE, '天结束事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_DISC' ERROR_CODE, '天结束事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_IGTAX' ERROR_CODE, '天结束事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_ITEM' ERROR_CODE, '天结束事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_PYMT' ERROR_CODE, '天结束事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_TEND' ERROR_CODE, '天结束事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DISC_REASON_REQ' ERROR_CODE, '门店折扣的折扣原因不能为空。' ERROR_DESC, '请输入折扣原因。' REC_SOLUTION, '缺少折扣原因' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DISC_REF_NO_REQ' ERROR_CODE, '促销的折扣参考编号不能为空' ERROR_DESC, '请输入折扣参考编号。' REC_SOLUTION, '缺少折扣参考' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DUP_REC' ERROR_CODE, '重复的记录。此事务处理已发送至拒绝文件。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '事务处理重复' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DUP_TAXC_COMPITEM' ERROR_CODE, '税务代码 - 组成商品编号组合重复。此事务处理已发送至拒绝文件。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '税务代码重复' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DUP_TRAN' ERROR_CODE, '重复的事务处理编号。此事务处理已发送至拒绝文件。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '事务处理重复' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'EEXCH_ITEM_REQ' ERROR_CODE, '交换事务处理至少需要两条商品记录。' ERROR_DESC, '请向此事务处理中添加一条或多条商品记录。' REC_SOLUTION, '缺少商品记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'EEXCH_NO_PYMT' ERROR_CODE, '公平交换事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '付款记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FHEAD_FIL_STIN' ERROR_CODE, 'FHEAD 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'FHEAD 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FHEAD_NOT_FIRST' ERROR_CODE, 'FHEAD 不是此文件的首条记录。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'FHEAD 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FILE_ERROR' ERROR_CODE, '在操作最后一个文件的过程中出错。' ERROR_DESC, '' REC_SOLUTION, '文件错误' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FTAIL_FIL_STIN' ERROR_CODE, 'FTAIL 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'FTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FTAIL_IN_TRAN' ERROR_CODE, '部分事务处理。事务处理中存在 FTAIL 记录。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FTAIL_NOT_LAST' ERROR_CODE, 'FTAIL 不是文件的最后一个记录。' ERROR_DESC, '输入文件没有 FTAIL 记录。' REC_SOLUTION, '缺少 FTAIL 记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FULFILL_ORDER_NO_ERR' ERROR_CODE, '履行订单编号仅对具有“完成订单”或“取消订单”商品状态的外部客户订单有效。' ERROR_DESC, '请将履行订单编号设置为空。' REC_SOLUTION, '履行订单编号无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_DISC' ERROR_CODE, '过账作废事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_DISC' ERROR_CODE, '结算事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_IGTAX' ERROR_CODE, '存入事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_PYMT' ERROR_CODE, '存入事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_TAX' ERROR_CODE, '存入事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_IGTAX' ERROR_CODE, '过账作废事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_ITEM' ERROR_CODE, '过账作废事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_PYMT' ERROR_CODE, '过账作废事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_TEND' ERROR_CODE, '过账作废事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RECORDS_REJECTED' ERROR_CODE, '加载此门店/日的过程中，记录被拒绝。' ERROR_DESC, '此输入文件已损坏，无法完全加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '拒绝的记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_NO_DISC' ERROR_CODE, '退款事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_NO_IGTAX' ERROR_CODE, '退款事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_NO_ITEM' ERROR_CODE, '退款事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_NO_PYMT' ERROR_CODE, '退款事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_NO_TAX' ERROR_CODE, '退款事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REFUND_TENDER_REQ' ERROR_CODE, '退款事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '退款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REGISTER_ID_REQ' ERROR_CODE, '需要收银机 ID，因为事务处理是在收银机级别上生成的。' ERROR_DESC, '请输入正确的收银机 ID。' REC_SOLUTION, '缺少收银机 ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'REGISTER_ID_REQ_BAL_LEVEL' ERROR_CODE, '对于该平衡级别必须输入收银机 ID。' ERROR_DESC, '请输入正确的收银机 ID。' REC_SOLUTION, '缺少收银机 ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETDISP_INV_RET' ERROR_CODE, '退货处理仅在存在与该退货关联的库存时有效。' ERROR_DESC, '将“退货处理”设置为空。' REC_SOLUTION, '退货处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETDISP_OMS' ERROR_CODE, '退货处理仅应与源自 OMS 且在 OMS 中处理的退货事务处理一起使用。' ERROR_DESC, '请将“退货处理”设置为空。' REC_SOLUTION, '退货处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETURN_DISP_REQ' ERROR_CODE, '需要退货处理。' ERROR_DESC, '请选择有效的“退货处理”。' REC_SOLUTION, '缺少退货处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETURN_ITEM_REQ' ERROR_CODE, '退货事务处理至少需要一条商品记录。' ERROR_DESC, '请向此事务处理添加商品记录。' REC_SOLUTION, '退货事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETURN_NO_PYMT' ERROR_CODE, '退货事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '退货事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETURN_TENDER_REQ' ERROR_CODE, '退货事务处理应至少有一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '退货缺少支付手段' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETWH_EXT_ORD' ERROR_CODE, '退货仓库仅对外部客户订单有效。' ERROR_DESC, '请将“退货仓库”设置为空。' REC_SOLUTION, '退货仓库无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETWH_OMS' ERROR_CODE, '退货仓库仅应该用于源自 OMS 且在 OMS 中处理的退货事务处理中。' ERROR_DESC, '请将“退货仓库”设置为空。' REC_SOLUTION, '退货仓库无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETWH_REQ' ERROR_CODE, '销售类型为外部客户订单且源自 OMS 的退货事务处理需要退货仓库。' ERROR_DESC, '请向此事务处理添加有效的仓库。' REC_SOLUTION, '退货仓库无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'SALE_ITEM_REQ' ERROR_CODE, '销售事务处理至少需要一条商品记录。' ERROR_DESC, '请向此事务处理添加商品记录。' REC_SOLUTION, '缺少商品记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'SALE_TENDER_REQ' ERROR_CODE, '销售事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '缺少支付手段记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'SET_DROP_SHIP' ERROR_CODE, '未正确导入此商品的直运指示。' ERROR_DESC, '请验证商品明细部分的直运指示。' REC_SOLUTION, '直运指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'SKU_NOT_FOUND' ERROR_CODE, '商品为空或在 RMS 中不存在。' ERROR_DESC, '请从清单中选择商品。' REC_SOLUTION, '未找到商品' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'STOREDAYNOTREADYTOBELOAD' ERROR_CODE, '此门店营业日不处于加载就绪的状态。' ERROR_DESC, '数据可能已经加载，或者点滴轮询的数据可能已经载出订单。' REC_SOLUTION, '门店营业日未加载就绪' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'STOREDAY_LOCK_FAILED' ERROR_CODE, '无法获取用于此门店营业日的写入锁。锁定记录尚未加载，或者此门店营业日已经被另一个进程锁定。' ERROR_DESC, '释放锁定后，请重试。' REC_SOLUTION, '门店营业日锁失败' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_ITEM_REQ' ERROR_CODE, '油罐浸沾事务处理至少需要一条商品记录。' ERROR_DESC, '请向此事务处理添加商品记录。' REC_SOLUTION, '缺少商品记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_CATT' ERROR_CODE, '油罐浸沾事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_DISC' ERROR_CODE, '油罐浸沾事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_PYMT' ERROR_CODE, '油罐浸沾事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_TAX' ERROR_CODE, '油罐浸沾事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TCUST_FIL_STIN' ERROR_CODE, 'TCUST 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TCUST 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TCUST_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TCUST 处于非法位置。' ERROR_DESC, '输入文件已损坏。TCUST 记录必须介于 THEAD 记录和 TTAIL 记录之间。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TERM_MARKER_NO_ERROR' ERROR_CODE, '此错误可用来检查是否已经加载所有数据。' ERROR_DESC, '加载所有数据，然后运行 saimptlogfin。' REC_SOLUTION, '无错误' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_FIL_STIN' ERROR_CODE, 'THEAD 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'THEAD 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_REASON_CODE_REQ' ERROR_CODE, '存入事务处理必需有原因代码。' ERROR_DESC, '请从清单中选择原因代码。' REC_SOLUTION, '缺少原因代码' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_TENDER_REQ' ERROR_CODE, '存入事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '缺少支付手段记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_CATT' ERROR_CODE, '支出事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_CUST' ERROR_CODE, '支出事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_IGTAX' ERROR_CODE, '支出事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_ITEM' ERROR_CODE, '支出事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_PYMT' ERROR_CODE, '支出事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_TAX' ERROR_CODE, '支出事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_REASON_CODE_REQ' ERROR_CODE, '支出事务处理必需有原因代码。' ERROR_DESC, '请从清单中选择原因代码。' REC_SOLUTION, '缺少原因代码' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_TENDER_REQ' ERROR_CODE, '支出事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '缺少支付手段记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'POSDATATOOOLD' ERROR_CODE, '销售点数据太旧，无法加载系统。' ERROR_DESC, '请检查“销售审计系统选项”屏幕中的“销售后天数”。此字段包含从今天起往前推移的天数，此天数中的销售点数据可加载系统。' REC_SOLUTION, '旧的销售点数据' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'POSFUTUREDATA' ERROR_CODE, '销售点数据针对晚于当前日期的营业日。' ERROR_DESC, '无法导入将来营业日的数据。' REC_SOLUTION, '营业日无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'POSUNEXPECTEDSTOREDAY' ERROR_CODE, '此门店营业日不应有销售点数据。此门店不应开业。' ERROR_DESC, '门店和营业日正确。但是，此门店营业日不应有销售点数据。请检查此门店的“公司关闭”、“公司关闭例外”和“关店”信息。' REC_SOLUTION, '意外的销售点数据' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PROM_COMP_REQ' ERROR_CODE, '促销的促销组成不能为空。' ERROR_DESC, '请输入促销组成。' REC_SOLUTION, '缺少促销组成' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_CATT' ERROR_CODE, '拉出事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_IGTAX' ERROR_CODE, '拉出事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_ITEM' ERROR_CODE, '拉出事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_PYMT' ERROR_CODE, '拉出事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_TENDER_REQ' ERROR_CODE, '拉出事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '缺少支付手段记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_ITEM_REQ' ERROR_CODE, '泵测试事务处理至少需要一条商品记录。' ERROR_DESC, '请向此事务处理添加商品记录。' REC_SOLUTION, '缺少商品记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_CATT' ERROR_CODE, '泵测试事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_CUST' ERROR_CODE, '泵测试事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_DISC' ERROR_CODE, '泵测试事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_TAX' ERROR_CODE, '泵测试事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_TEND' ERROR_CODE, '泵测试事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_CATT' ERROR_CODE, '过账作废事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CAN_MASK' ERROR_CODE, '支票账号屏蔽值无效。' ERROR_DESC, '请输入正确的支票账号屏蔽值。' REC_SOLUTION, '支票账号屏蔽值无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_OTN_STIN' ERROR_CODE, '原始销售点事务编号 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '将此值设置为 -1 以使数据加载。请输入目前作废的销售点原始事务编号。' REC_SOLUTION, '销售点事务处理编号无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_RAMT_STIN' ERROR_CODE, '舍入金额 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的舍入金额。' REC_SOLUTION, '舍入金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_ROAMT_STIN' ERROR_CODE, '舍入金额 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的舍入金额。' REC_SOLUTION, '舍入金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_TRN_STIN' ERROR_CODE, 'THEAD 事务处理编号 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '将此值设置为 -1 以使数据加载。输入此事务处理的销售点事务处理编号。' REC_SOLUTION, 'THEAD 编号无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_VAL_STIN' ERROR_CODE, '值 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的值。' REC_SOLUTION, '值无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_CLS_STIN' ERROR_CODE, 'SKU 分类 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的 SKU 分类。' REC_SOLUTION, '分类无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_CUST_ORD_STIN' ERROR_CODE, '客户订单行号 – 数字字段中有非数字字符' ERROR_DESC, '通过“客户属性”屏幕输入正确的客户订单行号' REC_SOLUTION, '客户订单行无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_DEP_STIN' ERROR_CODE, '部门 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的部门。' REC_SOLUTION, '部门无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_FIL_STIN' ERROR_CODE, 'TITEM 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TITEM 编号无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_INVALID_CW' ERROR_CODE, '商品称重指示 – 没有为称重商品设置称重指示。' ERROR_DESC, '在商品明细部分，验证称重指示。' REC_SOLUTION, '称重指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。THEAD 处于非法位置。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_LOC_NO_BANN' ERROR_CODE, '地点无横幅 ID。' ERROR_DESC, '门店没有关联的横幅 ID。' REC_SOLUTION, '缺少横幅' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'FULFILL_ORDER_NO_REQ' ERROR_CODE, '需要履行订单编号。' ERROR_DESC, '请输入正确的履行订单编号。' REC_SOLUTION, '缺少履行订单编号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'GARBAGE_IN_RECORD' ERROR_CODE, '记录中嵌入了不正确的内容。' ERROR_DESC, '输入文件已损坏。记录中有嵌入的字符。请编辑文件或者重新轮询门店。' REC_SOLUTION, '记录中有不正确的内容' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_COUPON_NO_REQ' ERROR_CODE, '当折扣类型为门店优惠券时，优惠券编号不能为空。' ERROR_DESC, '请输入优惠券编号。' REC_SOLUTION, '缺少优惠券编号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_FIL_STIN' ERROR_CODE, 'IDISC 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'IDISC 标识符无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_INVALID_CW' ERROR_CODE, '折扣称重指示 – 未设置称重商品的称重指示。' ERROR_DESC, '输入正确的称重指示' REC_SOLUTION, '称重指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。IDISC 记录处于非法位置。' ERROR_DESC, '此输入文件已损坏。IDISC 记录必须在 TITEM 记录之后。请编辑文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_PRC_STIN' ERROR_CODE, '无效折扣促销组成' ERROR_DESC, '输入有效折扣促销组成' REC_SOLUTION, '促销组成无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_QTY_STIN' ERROR_CODE, '折扣数量 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '请输入有效的折扣数量。' REC_SOLUTION, '折扣数量无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_SET_CW' ERROR_CODE, '折扣称重指示 - 称重指示未正确导入' ERROR_DESC, '输入正确的称重指示' REC_SOLUTION, '称重指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_UDA_STIN' ERROR_CODE, '折扣金额 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '请输入有效的折扣金额。' REC_SOLUTION, '折扣金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_UOM_QTY_STIN' ERROR_CODE, '折扣单位数量 - 数字字段中有非数字字符' ERROR_DESC, '输入正确的单位数量' REC_SOLUTION, '折扣单位无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDNT_ID_WITHOUT_IDNT_MTHD' ERROR_CODE, '没有为识别提供识别方法。' ERROR_DESC, '请从列表中输入一个有效的识别方法。' REC_SOLUTION, '缺少识别方法' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDNT_MTHD_WITHOUT_IDNT_ID' ERROR_CODE, '此识别方法没有识别。' ERROR_DESC, '请输入一个有效的识别。' REC_SOLUTION, '缺少识别' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IGTAX_FIL_STIN' ERROR_CODE, 'IGTAX 文件行标识符。数字字段中存在非数字字符或者字段为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'IGTAX 标识符无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IGTAX_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。IGTAX 记录处于非法位置' ERROR_DESC, '输入文件已损坏。IGTAX 记录必须在 TITEM-IDISC 记录之后。请编辑文件或者重新轮询门店。' REC_SOLUTION, 'IGTAX 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IGTAX_RAT_STIN' ERROR_CODE, 'IGTAX 税率 - 数字字段中存在非数字字符或者字段为空' ERROR_DESC, '请输入正确的 IGTAX 比率。' REC_SOLUTION, 'IGTAX 税率无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_BANNER_ID' ERROR_CODE, '无效横幅 ID。' ERROR_DESC, '请选择一个有效的横幅 ID。' REC_SOLUTION, '横幅无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_BIRTHDATE' ERROR_CODE, '出生日期无效。' ERROR_DESC, '出生日期的有效格式为 YYYYMMDD。' REC_SOLUTION, '出生日期无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_BUSINESS_DATE' ERROR_CODE, '营业日无效或缺少营业日。' ERROR_DESC, '营业日的有效格式为 YYYYMMDD。' REC_SOLUTION, '营业日无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CASHIER_ID' ERROR_CODE, '收银员销售点 ID 没有与之对应的员工 ID。' ERROR_DESC, '请在“员工维护”表单中输入员工 ID 和销售点 ID。' REC_SOLUTION, '收银员 ID 无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CCAS' ERROR_CODE, '信用卡授权方无效。' ERROR_DESC, '请从清单中选择信用卡授权方。' REC_SOLUTION, '授权来源无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CCEM' ERROR_CODE, '信用卡输入方式无效。' ERROR_DESC, '请从清单中选择信用卡输入方式。' REC_SOLUTION, '信用卡输入方式无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CCSC' ERROR_CODE, '信用卡特殊条件无效。' ERROR_DESC, '请从清单中选择信用卡特殊条件' REC_SOLUTION, '条件无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CCVF' ERROR_CODE, '信用卡持有人验证无效。' ERROR_DESC, '请从清单中选择信用卡持有人验证。' REC_SOLUTION, '验证无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CC_CHECKSUM' ERROR_CODE, '信用卡校验位无效。此卡号无效。' ERROR_DESC, '请输入正确的信用卡号。' REC_SOLUTION, '校验位无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CC_EXP_DATE' ERROR_CODE, '失效日期无效。' ERROR_DESC, '失效日期的有效格式为 YYYYMMDD。' REC_SOLUTION, '失效日期无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CC_MASK' ERROR_CODE, '信用卡屏蔽值无效。' ERROR_DESC, '请输入正确的信用卡屏蔽值。' REC_SOLUTION, '信用卡屏蔽值无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CC_PREFIX' ERROR_CODE, '此信用卡的数字前缀不在指定的范围内。' ERROR_DESC, '请输入正确的信用卡号。' REC_SOLUTION, '信用卡前缀无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CHECK_NO' ERROR_CODE, '支票号码无效或者数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的支票号码。' REC_SOLUTION, '支票号码无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CIDT' ERROR_CODE, '客户识别类型无效。' ERROR_DESC, '请从清单中选择客户识别类型。' REC_SOLUTION, '客户 ID 类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_CUST_ORD_DATE' ERROR_CODE, '无效客户订单日期。' ERROR_DESC, '客户订单日期的有效格式为 YYYYMMDD。' REC_SOLUTION, '客户订单日期无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_DISCOUNT_UOM' ERROR_CODE, '此商品的折扣单位无效。' ERROR_DESC, '请选择与标准单位兼容的不同单位，或者定义当前折扣单位与标准单位之间的换算。' REC_SOLUTION, '折扣单位无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_DOCUMENT' ERROR_CODE, '礼券/代金券编号是必需的。' ERROR_DESC, '请输入礼券/代金券编号。' REC_SOLUTION, '单证无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_FTAIL_POS' ERROR_CODE, 'FTAIL 记录的位置不正确。' ERROR_DESC, '输入文件已损坏。请编辑文件或者重新轮询门店。' REC_SOLUTION, 'FTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_FTAIL_TRAN_CNT' ERROR_CODE, 'FTAIL 事务处理编号计数不正确。' ERROR_DESC, '输入文件已损坏。请编辑文件或者重新轮询门店。' REC_SOLUTION, 'FTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_ITEM_NO' ERROR_CODE, '商品编号为空。' ERROR_DESC, '请从清单中选择商品编号。' REC_SOLUTION, '商品无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_ITEM_SWIPED_IND' ERROR_CODE, '商品刷卡指示无效。值已默认为“Y”。' ERROR_DESC, '请单击“商品刷卡”复选框以设定正确的值。' REC_SOLUTION, '商品刷卡指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TITEM 处于非法位置。' ERROR_DESC, '输入文件已损坏。TITEM 记录必须介于 THEAD 记录和 TTAIL 记录之间。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_MID_STIN' ERROR_CODE, '媒体 ID - 数字字段中有非数字字符。' ERROR_DESC, '通过“客户属性”屏幕输入正确的媒体 ID。' REC_SOLUTION, '媒体 ID 无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_OUR_STIN' ERROR_CODE, '原始单位零售 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的原始单位零售。' REC_SOLUTION, '原始单位零售无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_QTY_SIGN' ERROR_CODE, '商品状态“销售”、“开始订单 (ORI)”、“完成订单 (ORD)”、“开始预约订货 (LIN)”或“完成预约订货 (LCO)”应为正。' ERROR_DESC, '请修改商品数量的符号。' REC_SOLUTION, '商品数量符号无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_QTY_STIN' ERROR_CODE, '商品数量 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '请输入正确的商品数量。商品状态为销售的商品记录应该包含正数量，而商品状态为退货的商品记录应该包含负数量。如果商品状态为无效，则符号应该与无效商品的符号相反。' REC_SOLUTION, '商品数量无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_SBC_STIN' ERROR_CODE, '子分类 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的子分类。' REC_SOLUTION, '子分类无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_SET_CW' ERROR_CODE, '商品称重指示 - 没有为此商品正确导入称重指示' ERROR_DESC, '在商品明细部分，验证称重指示。' REC_SOLUTION, '称重指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_SUP_STIN' ERROR_CODE, 'UPC 补充 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的 UPC 补充。' REC_SOLUTION, 'UPC 补充无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_UOM_QTY_STIN' ERROR_CODE, '商品单位数量 – 数字字段中有非数字字符' ERROR_DESC, '输入正确的单位数量' REC_SOLUTION, '单位数量无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_UOM_QTY_ZERO' ERROR_CODE, '对于商品状态为“销售”或“退货”的称重商品记录，如果数量不是零，则单位数量不能为零，反之亦然。' ERROR_DESC, '请输入商品单位数量。' REC_SOLUTION, '单位数量无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_URT_STIN' ERROR_CODE, '单位零售 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的单位零售。' REC_SOLUTION, '单位零售无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TITEM_WH_STIN' ERROR_CODE, '退货仓库 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的退货仓库。' REC_SOLUTION, '退货仓库无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOO_MANY_FHEADS' ERROR_CODE, '发生了其他 FHEAD 记录。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'FHEAD 记录太多' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_IGTAX_AMT_GT_TOTRET' ERROR_CODE, 'IGTAX 总金额大于总计零售。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'IGTAX 金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_IGTAX_AMT_STIN' ERROR_CODE, 'IGTAX 总金额。数字字段中存在非数字字符或者字段为空。' ERROR_DESC, '请输入正确的 IGTAX 总金额。' REC_SOLUTION, 'IGTAX 金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_CATT' ERROR_CODE, '总计事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_CUST' ERROR_CODE, '总计事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_DISC' ERROR_CODE, '总计事务处理不应有折扣记录。' ERROR_DESC, '从此事务处理中删除折扣记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_ITEM' ERROR_CODE, '总计事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_PYMT' ERROR_CODE, '总计事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_TAX' ERROR_CODE, '总计事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_TEND' ERROR_CODE, '总计事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_REF_NO_1_REQ' ERROR_CODE, '总计事务处理必需在“参考编号 1”字段中含有总计 ID。' ERROR_DESC, '请在“参考编号 1”字段中输入总计 ID。' REC_SOLUTION, '缺少参考编号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_VALUE_REQ' ERROR_CODE, '总计事务处理必需有总计值。' ERROR_DESC, '请输入总计值。' REC_SOLUTION, '缺少总计值' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TPYMT_AMT_STIN' ERROR_CODE, '付款金额 - 数字字段中存在非数字字符。' ERROR_DESC, '请输入正确的付款金额' REC_SOLUTION, '付款金额无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TPYMT_FIL_STIN' ERROR_CODE, 'TPYMT 文件行标识符。数字字段中存在非数字字符或者字段为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TPYMT 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TPYMT_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TPYMT 记录处于非法位置' ERROR_DESC, '输入文件已损坏。TPYMT 记录必须在 TTEND 记录之前。请编辑文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_MISS_REC' ERROR_CODE, '事务处理丢失了子记录。' ERROR_DESC, '输入文件已损坏。请编辑文件或者重新轮询门店。' REC_SOLUTION, '缺少子记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_NOT_RECOGNIZED' ERROR_CODE, '事务处理类型未识别。' ERROR_DESC, '输入文件已损坏。请检查 TLOG 至 RTLOG 的转换过程或者重新轮询门店。' REC_SOLUTION, '事务处理类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_NO_REQ' ERROR_CODE, '事务处理编号是必需的。' ERROR_DESC, '请输入正确的事务处理编号。' REC_SOLUTION, '缺少事务处理编号' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_XTRA_CUST' ERROR_CODE, '事务处理只可以有一个客户记录。' ERROR_DESC, '请从此事务处理中删除多余的客户记录。' REC_SOLUTION, '客户记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_XTRA_REC' ERROR_CODE, '找到未知的记录类型。' ERROR_DESC, '输入文件已损坏。有效的记录类型为 FHEAD、THEAD、TCUST、CATT、TITEM、IDISC、TTEND、TTAX、TTAIL 和 FTAIL。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '未知的记录类型' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRANLVLTAX_NO_IGTAX' ERROR_CODE, '配置有事务处理级税的门店不能具有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, 'IGTAX 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAIL_FIL_STIN' ERROR_CODE, 'TTAIL 文件行标识符 - 数字字段中存在非数字字符或者该字段可能为空。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, 'TTAIL 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TTAIL_IN_ILLEGAL_POS' ERROR_CODE, '部分事务处理。TTAIL 处于非法位置。' ERROR_DESC, '输入文件已损坏。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_NO_INV_RET_IND' ERROR_CODE, '无库存退货指示无效。该值已默认为空。' ERROR_DESC, '请从“无库存退货”下拉列表中进行选择以设置正确的值。' REC_SOLUTION, '非库存退货指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_ORIG_CURR' ERROR_CODE, '原始货币代码无效' ERROR_DESC, '请输入有效的货币代码。' REC_SOLUTION, '货币代码无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_ORRC' ERROR_CODE, '价格覆盖原因无效。' ERROR_DESC, '请从清单中选择价格覆盖原因。' REC_SOLUTION, '价格覆盖原因无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_PRMT' ERROR_CODE, '促销类型无效。' ERROR_DESC, '请从清单中选择促销类型。' REC_SOLUTION, '促销类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_REAC' ERROR_CODE, '原因代码无效。' ERROR_DESC, '请从清单中选择原因代码。' REC_SOLUTION, '原因代码无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_RTLOG_ORIG_SYS' ERROR_CODE, 'RTLog 源系统无效。' ERROR_DESC, 'RTLOG 源系统无效时无法导入数据。' REC_SOLUTION, '源系统无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SACA' ERROR_CODE, '客户属性类型无效。' ERROR_DESC, '请从清单中选择客户属性类型。' REC_SOLUTION, '客户属性类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SAIT' ERROR_CODE, '商品类型无效。' ERROR_DESC, '请从清单中选择商品类型。' REC_SOLUTION, '商品类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SALESPERSON_ID' ERROR_CODE, '销售员销售点 ID 没有与之对应的员工 ID。' ERROR_DESC, '在“员工维护”表单中输入员工 ID 和销售点 ID，或者在“员工”选项卡上更正销售点 ID。' REC_SOLUTION, '销售员 ID 无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SARR' ERROR_CODE, '退货原因无效。' ERROR_DESC, '请从清单中选择退货原因。' REC_SOLUTION, '退货原因无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SASY' ERROR_CODE, '销售类型无效。' ERROR_DESC, '请从清单中选择销售类型。' REC_SOLUTION, '销售类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SASY_E' ERROR_CODE, '“销售”, “预约订货”或“作废”事务处理的“销售类型”无效。' ERROR_DESC, '请从清单中选择销售类型。' REC_SOLUTION, '销售类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SELLING_UOM' ERROR_CODE, '商品的销售单位无效。' ERROR_DESC, '请选择与标准单位兼容的不同销售单位，或者定义当前销售单位与标准单位之间的换算。' REC_SOLUTION, '销售单位无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SYSTEM_IND' ERROR_CODE, '系统指示无效。' ERROR_DESC, '请从清单中选择系统指示。' REC_SOLUTION, '系统指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TAXC' ERROR_CODE, '税种无效。' ERROR_DESC, '请从清单中选择税种。' REC_SOLUTION, '税种无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TAX_AUTHORITY' ERROR_CODE, '税务机关无效或者字段为空。' ERROR_DESC, '请输入有效的税务机关' REC_SOLUTION, '税务机关无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TAX_IND' ERROR_CODE, '税指示无效。值已默认为“Y”。' ERROR_DESC, '请单击“税指示”复选框以设定正确的值。' REC_SOLUTION, '税指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TENDER_ENDING' ERROR_CODE, '期末支付手段无效。' ERROR_DESC, '按需要调整支付手段。' REC_SOLUTION, '期末支付手段无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TENT' ERROR_CODE, '支付手段类型商品组无效。' ERROR_DESC, '请从清单中选择支付手段类型商品组。' REC_SOLUTION, '支付手段类型组无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TRAN_DATETIME' ERROR_CODE, '事务处理日期/时间无效。' ERROR_DESC, '请输入有效的事务处理日期/时间。' REC_SOLUTION, '事务处理日期/时间无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TRAT' ERROR_CODE, '事务处理类型无效。' ERROR_DESC, '请从清单中选择事务处理类型。' REC_SOLUTION, '事务处理类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TSYS' ERROR_CODE, '事务处理系统无效。此事务处理已发送至拒绝文件。' ERROR_DESC, '请输入有效的事务处理系统。' REC_SOLUTION, '处理系统无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_VATC' ERROR_CODE, '增值税代码无效。' ERROR_DESC, '请输入有效的增值税代码。' REC_SOLUTION, '增值税代码无效。' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INV_RULE_DEF' ERROR_CODE, '此规则没有成功计算此门店营业日。' ERROR_DESC, '请复核规则定义并根据需要进行更新。' REC_SOLUTION, '规则定义无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INV_TOTAL_DEF' ERROR_CODE, '此门店营业日的此总计没有成功计算。' ERROR_DESC, '请复核总计定义并根据需要进行更新。' REC_SOLUTION, '总计定义无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'ITEMLVLTAX_NO_TTAX' ERROR_CODE, '配置有商品级别税的门店不能具有事务处理级税记录。' ERROR_DESC, '从此事务处理中删除事务处理级税记录。' REC_SOLUTION, 'TTAX 记录无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'ITEM_STATUS_REQ' ERROR_CODE, '商品状态不能为空。' ERROR_DESC, '请从清单中选择商品状态。' REC_SOLUTION, '缺少商品状态' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_CATT' ERROR_CODE, '贷款事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_CUST' ERROR_CODE, '贷款事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_DISC' ERROR_CODE, '贷款事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_IGTAX' ERROR_CODE, '贷款事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_ITEM' ERROR_CODE, '贷款事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_PYMT' ERROR_CODE, '贷款事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_TENDER_REQ' ERROR_CODE, '贷款事务处理至少需要一条支付手段记录。' ERROR_DESC, '请向此事务处理添加支付手段记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_ITEM_REQ' ERROR_CODE, '仪表事务处理至少需要一条商品记录。' ERROR_DESC, '请向此事务处理添加商品记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_CATT' ERROR_CODE, '仪表事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_CUST' ERROR_CODE, '仪表事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_IGTAX' ERROR_CODE, '仪表事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_PYMT' ERROR_CODE, '仪表事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'METER_NO_TAX' ERROR_CODE, '仪表事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '仪表事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'MISSING_THEAD' ERROR_CODE, 'THEAD 记录丢失。' ERROR_DESC, '输入文件已损坏。每一事务处理都必须有 THEAD 记录和 TTAIL 记录。请编辑文件或者重新轮询门店。' REC_SOLUTION, '缺少 THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'MISSING_TRAN_BIG_GAP' ERROR_CODE, '大量事务处理丢失。' ERROR_DESC, '输入文件可能已损坏。请尝试重新轮询门店。' REC_SOLUTION, '大型事务处理缺口' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'MISSING_TTAIL' ERROR_CODE, 'TTAIL 记录丢失。' ERROR_DESC, '输入文件已损坏。每一事务处理都必须有 THEAD 记录和 TTAIL 记录。请编辑文件或者重新轮询门店。' REC_SOLUTION, '缺少 TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NO_INV_RET_IND_REQ' ERROR_CODE, '对于销售类型为“外部客户订单”且源自 OMS 的退货事务处理，需要提供无库存退货指示。' ERROR_DESC, '请从“无库存退货”下拉列表中进行选择以设置正确的值。' REC_SOLUTION, '非库存退货指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_CATT' ERROR_CODE, '无销售事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_CUST' ERROR_CODE, '无销售事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_DISC' ERROR_CODE, '无销售事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_IGTAX' ERROR_CODE, '“无销售”事务处理不应有商品级税' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_ITEM' ERROR_CODE, '无销售事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_PYMT' ERROR_CODE, '无销售事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NOSALE_NO_TEND' ERROR_CODE, '无销售事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '无效的无销售记录' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NO_RULE_FOR_SALETOTAL' ERROR_CODE, '没有为期末销售总计金额定义规则。' ERROR_DESC, '请确保在 sa_rounding_rule_detail 表中为期末销售总计金额定义了规则。' REC_SOLUTION, '缺少规则' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_CATT' ERROR_CODE, '未结事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_CUST' ERROR_CODE, '未结事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_DISC' ERROR_CODE, '未结事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_IGTAX' ERROR_CODE, '未结事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_ITEM' ERROR_CODE, '未结事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_TAX' ERROR_CODE, '未结事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_TEND' ERROR_CODE, '未结事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'ORGCUR_AMT_WITHOUT_ORGCUR' ERROR_CODE, '原始货币金额没有原始货币代码。' ERROR_DESC, '请输入有效的原始货币代码。' REC_SOLUTION, '缺少货币代码' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'ORGCUR_WITHOUT_ORGCUR_AMT' ERROR_CODE, '指定的原始货币代码没有原始货币金额。' ERROR_DESC, '请输入正确的原始货币金额。' REC_SOLUTION, '缺少货币金额' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_CATT' ERROR_CODE, '存入事务处理不应有客户属性记录。' ERROR_DESC, '从此事务处理中删除客户属性记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_CUST' ERROR_CODE, '存入事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_DISC' ERROR_CODE, '存入事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_TEND' ERROR_CODE, '油罐浸沾事务处理不应有支付手段记录。' ERROR_DESC, '从此事务处理中删除支付手段记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'THEAD_IN_TRAN' ERROR_CODE, '部分事务处理。THEAD 在事务处理中。' ERROR_DESC, '此输入文件已损坏，无法加载。请编辑该文件或者重新轮询门店。' REC_SOLUTION, '部分事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TRAN_OUT_BAL' ERROR_CODE, '事务处理失去平衡。商品和付款金额总计不等于支付手段总计。' ERROR_DESC, '请按需要调整商品或支付手段或者付款金额。' REC_SOLUTION, '事务处理失去平衡' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'CLOSE_NO_PYMT' ERROR_CODE, '结算事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '结算事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'COND_NO_ITEM' ERROR_CODE, '门店状况事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '门店状况事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'DCLOSE_NO_CUST' ERROR_CODE, '天结束事务处理不应有客户记录。' ERROR_DESC, '从此事务处理中删除客户记录。' REC_SOLUTION, '天结束事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'IDISC_DRN_STIN' ERROR_CODE, '折扣促销编号无效。' ERROR_DESC, '请输入有效的折扣促销编号。' REC_SOLUTION, '促销无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_IDMH' ERROR_CODE, '识别方法无效。' ERROR_DESC, '请从列表中输入一个有效的识别方法。' REC_SOLUTION, '识别方法无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_RETURN_DISP' ERROR_CODE, '退货处理无效。' ERROR_DESC, '请选择有效的“退货处理”。' REC_SOLUTION, '退货处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SADT' ERROR_CODE, '折扣类型无效。' ERROR_DESC, '请从清单中选择折扣类型。' REC_SOLUTION, '折扣类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SASI' ERROR_CODE, '商品状态无效。' ERROR_DESC, '请从清单中选择商品状态。' REC_SOLUTION, '商品状态无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_SUB_SACA' ERROR_CODE, '客户属性值无效。' ERROR_DESC, '请从清单中选择客户属性值。' REC_SOLUTION, '客户属性无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_TRAS' ERROR_CODE, '子事务处理类型无效。' ERROR_DESC, '请从清单中选择子事务处理类型。' REC_SOLUTION, '子事务处理类型无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_VENDOR_NO' ERROR_CODE, '供应商编号无效。' ERROR_DESC, '请使用费用供应商的合作伙伴表中的供应商，或使用商品供应商的供应商表中的供应商。' REC_SOLUTION, '供应商无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'LOAN_NO_TAX' ERROR_CODE, '贷款事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '贷款事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NON_MERCH_ITEM_NOT_FOUND' ERROR_CODE, 'RMS 中不存在此非商品品类。' ERROR_DESC, '请选择一个合适的非商品品类。' REC_SOLUTION, '此非商品品类不存在' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'NO_INV_RET_IND_RETURN' ERROR_CODE, '无库存退货指示仅对退货商品状态有效。' ERROR_DESC, '请将该指示设置为空。' REC_SOLUTION, '非库存退货指示无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'OPEN_NO_PYMT' ERROR_CODE, '未结事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '无效的未结事务处理' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDIN_NO_ITEM' ERROR_CODE, '存入事务处理不应有商品记录。' ERROR_DESC, '从此事务处理中删除商品记录。' REC_SOLUTION, '存入事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PAIDOU_NO_DISC' ERROR_CODE, '支出事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '支出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PULL_NO_DISC' ERROR_CODE, '拉出事务处理不应有折扣记录。' ERROR_DESC, '请从此事务处理中删除折扣记录。' REC_SOLUTION, '拉出事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PUMPT_NO_PYMT' ERROR_CODE, '泵测试事务处理不应有付款记录。' ERROR_DESC, '请从此事务处理中删除付款记录。' REC_SOLUTION, '泵测试事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'PVOID_NO_TAX' ERROR_CODE, '过账作废事务处理不应有税记录。' ERROR_DESC, '从此事务处理中删除税记录。' REC_SOLUTION, '过账作废事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETDISP_EXT_ORD' ERROR_CODE, '退货处理仅对外部客户订单有效。' ERROR_DESC, '请将“退货处理”设置为空。' REC_SOLUTION, '退货处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'RETWH_NOT_FOUND' ERROR_CODE, 'RMS 中不存在退货仓库。' ERROR_DESC, '请从清单中选择仓库。' REC_SOLUTION, '退货仓库无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TANKDP_NO_IGTAX' ERROR_CODE, '油罐浸沾事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '油罐浸沾事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'TOTAL_NO_IGTAX' ERROR_CODE, '总计事务处理不应有商品级税记录。' ERROR_DESC, '请从此事务处理中删除商品级税记录。' REC_SOLUTION, '总计事务处理无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_VENDOR_NO_MV' ERROR_CODE, '商品供应商编号无效。' ERROR_DESC, '请使用商品供应商的供应商表中的供应商。' REC_SOLUTION, '商品供应商无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 8 LANG, 'INVLD_VENDOR_NO_EV' ERROR_CODE, '费用供应商编号无效。' ERROR_DESC, '请使用费用供应商的合作伙伴表中的供应商。' REC_SOLUTION, '费用供应商无效' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO SA_ERROR_CODES base USING
( SELECT ERROR_CODE ERROR_CODE, ERROR_DESC ERROR_DESC, REC_SOLUTION REC_SOLUTION, SHORT_DESC SHORT_DESC FROM SA_ERROR_CODES_TL TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8)) USE_THIS
ON ( base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE SET base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
--
DELETE FROM SA_ERROR_CODES_TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
