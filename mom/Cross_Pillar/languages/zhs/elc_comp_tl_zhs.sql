SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'INSINT' COMP_ID, '国际保险' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, '200' COMP_ID, '航空货运' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, '300' COMP_ID, '陆地货运' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, '60' COMP_ID, '第三方物流' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, '55' COMP_ID, '拆箱装运费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'ICING' COMP_ID, '冷冻费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'ORDCST' COMP_ID, '订货成本' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TEXP' COMP_ID, '总费用' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TEXPC' COMP_ID, '县总费用' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TEXPZ' COMP_ID, '区域总费用' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'UNCST' COMP_ID, '单位成本' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BAPAL' COMP_ID, '每托盘的回程备抵' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BACWT' COMP_ID, '每英担的回程备抵' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BAEA' COMP_ID, '回程备抵，每' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BACS' COMP_ID, '每货箱的回程备抵' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BACFT' COMP_ID, '每立方英寸的回程备抵' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TSFFRGHT' COMP_ID, '转移运费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TSFINSUR' COMP_ID, '转移保险' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'WHFEE' COMP_ID, '仓库储存费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'WHPROC' COMP_ID, '仓库处理费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'AGCOMM' COMP_ID, '代理佣金' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'ILFRT' COMP_ID, '内陆运费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'BUYCOMM' COMP_ID, '采购员佣金' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'OCFRT' COMP_ID, '海运费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'INSUR' COMP_ID, '保险' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'SELLCOMM' COMP_ID, '销售员佣金' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'ROYALTY' COMP_ID, '特许权使用费' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'VFD25US' COMP_ID, '税额值的 25% US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'VFD50US' COMP_ID, '税额值的 50% US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'VFD75US' COMP_ID, '税额值的 75% US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'MPFUS' COMP_ID, '商品处理费 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TDTYPE' COMP_ID, '税额总计 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'HMFUS' COMP_ID, '港口维护费 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'ADUS' COMP_ID, '反倾销 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'CVDUS' COMP_ID, '反补贴 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 8 LANG, 'TDTYUS' COMP_ID, '税额总计 US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ELC_COMP base USING
( SELECT COMP_ID COMP_ID, COMP_DESC COMP_DESC FROM ELC_COMP_TL TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8)) USE_THIS
ON ( base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE SET base.COMP_DESC = use_this.COMP_DESC;
--
DELETE FROM ELC_COMP_TL where lang = 8
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 8);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
