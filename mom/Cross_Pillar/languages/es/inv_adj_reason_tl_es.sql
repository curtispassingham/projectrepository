SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 REASON, 4 LANG, '(+) debido a conversión de inventario' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 13 REASON, 4 LANG, 'Inventario no disponible' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 20 REASON, 4 LANG, '(+/-) debido a transferencia de artículo' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 30 REASON, 4 LANG, '(+/-) debido a sistema de selección de unidades' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 31 REASON, 4 LANG, 'AAT oculto' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 42 REASON, 4 LANG, '(+/-) debido a recuento cíclico' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 48 REASON, 4 LANG, '(+/-) debido a división de grupo de paquetes' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 49 REASON, 4 LANG, '(+/-) debido a consolidación de orden' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 50 REASON, 4 LANG, '(+/-) debido a colocación en varias SKU' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 55 REASON, 4 LANG, '(+/-) debido a selección de unidad de papel' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 60 REASON, 4 LANG, '+ debido a devolución cliente' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 70 REASON, 4 LANG, '+ debido a desensamblaje de kit' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 81 REASON, 4 LANG, 'Daño - Salida' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 82 REASON, 4 LANG, 'Daño - Retener' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 83 REASON, 4 LANG, 'Robo' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 84 REASON, 4 LANG, 'Uso tienda' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 85 REASON, 4 LANG, 'Reparación - Salida' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 86 REASON, 4 LANG, 'Benéfico' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 1 REASON, 4 LANG, 'Merma' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 REASON, 4 LANG, '(+/-) debido a auditoría de salida' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 3 REASON, 4 LANG, 'Reparación - Entrada' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 5 REASON, 4 LANG, 'Recuperado por fabricante' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 91 REASON, 4 LANG, 'Stock - Retención' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 92 REASON, 4 LANG, 'Administración' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 93 REASON, 4 LANG, 'Devolución cliente' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 94 REASON, 4 LANG, 'Entrada transformación producto' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 95 REASON, 4 LANG, 'Consignación' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 96 REASON, 4 LANG, 'Listo para venta' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 97 REASON, 4 LANG, 'Devoluciones' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 98 REASON, 4 LANG, 'Salida transformación producto' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 99 REASON, 4 LANG, '(+/-) debido a ajuste general' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 110 REASON, 4 LANG, 'Dañado' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 120 REASON, 4 LANG, 'Mostrar' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 130 REASON, 4 LANG, 'Reacondicionamiento' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 140 REASON, 4 LANG, 'Desfile de moda' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 141 REASON, 4 LANG, 'Ajuste inventario físico' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 142 REASON, 4 LANG, 'Retención para cliente' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 145 REASON, 4 LANG, '-debido a devolución al proveedor' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 146 REASON, 4 LANG, '-debido a robo' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 147 REASON, 4 LANG, 'Pérdida por incendio' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 148 REASON, 4 LANG, 'Pérdida por inundación' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 149 REASON, 4 LANG, 'Pérdida por transporte' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 150 REASON, 4 LANG, 'Muestreo de publicidad' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 151 REASON, 4 LANG, 'Excedente de proveedor' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 152 REASON, 4 LANG, 'Transformación' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 153 REASON, 4 LANG, 'Disposición de proveedor' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 154 REASON, 4 LANG, 'Marca stock agotado' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 155 REASON, 4 LANG, 'Roto' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 156 REASON, 4 LANG, 'Fecha de agotado' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 157 REASON, 4 LANG, 'Robado' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 158 REASON, 4 LANG, 'Consumo interno' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 159 REASON, 4 LANG, 'Inventariado fiscal' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 160 REASON, 4 LANG, 'Foto para catálogo' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 180 REASON, 4 LANG, 'Reserva de orden de cliente (entrada)' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 181 REASON, 4 LANG, 'Reserva de orden de cliente (salida)' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 182 REASON, 4 LANG, 'Entrada stock' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 183 REASON, 4 LANG, 'Agotamiento stock' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 184 REASON, 4 LANG, 'Aumento de ajuste de inventario de últimas unidades' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 185 REASON, 4 LANG, 'Disminución de ajuste de inventario de últimas unidades' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 186 REASON, 4 LANG, 'Aumento de ajuste de inventario de último importe y unidades' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 187 REASON, 4 LANG, 'Disminución de ajuste de inventario de último importe y unidades' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 190 REASON, 4 LANG, 'Destruir en sitio para devoluciones mayorista/franquicia' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 191 REASON, 4 LANG, 'Devolución de cliente sin inventario' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 9999 REASON, 4 LANG, 'Código transformación artículo' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 87 REASON, 4 LANG, 'Inventario físico de entrada' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 88 REASON, 4 LANG, 'Inventario físico de salida' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 89 REASON, 4 LANG, 'Desechar desde retención' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 90 REASON, 4 LANG, 'Desechar desde stock disponible' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
