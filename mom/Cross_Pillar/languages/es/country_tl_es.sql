SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DK' COUNTRY_ID, 'Dinamarca' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DJ' COUNTRY_ID, 'Yibuti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DM' COUNTRY_ID, 'Dominica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DO' COUNTRY_ID, 'República Dominicana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'EC' COUNTRY_ID, 'Ecuador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'EG' COUNTRY_ID, 'Egipto' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SV' COUNTRY_ID, 'El Salvador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GQ' COUNTRY_ID, 'Guinea Ecuatorial' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ER' COUNTRY_ID, 'Eritrea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'EE' COUNTRY_ID, 'Estonia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ET' COUNTRY_ID, 'Etiopía' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FK' COUNTRY_ID, 'Islas Malvinas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FO' COUNTRY_ID, 'Islas Feroe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FJ' COUNTRY_ID, 'Fiyi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FI' COUNTRY_ID, 'Finlandia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FR' COUNTRY_ID, 'Francia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GF' COUNTRY_ID, 'Guayana Francesa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PF' COUNTRY_ID, 'Polinesia Francesa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TF' COUNTRY_ID, 'Territorios Australes Franceses' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GA' COUNTRY_ID, 'Gabón' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GM' COUNTRY_ID, 'Gambia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GE' COUNTRY_ID, 'Georgia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DE' COUNTRY_ID, 'Alemania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GH' COUNTRY_ID, 'Ghana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GI' COUNTRY_ID, 'Gibraltar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GR' COUNTRY_ID, 'Grecia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GL' COUNTRY_ID, 'Groenlandia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GD' COUNTRY_ID, 'Granada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GP' COUNTRY_ID, 'Guadalupe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GU' COUNTRY_ID, 'Guam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GT' COUNTRY_ID, 'Guatemala' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GG' COUNTRY_ID, 'Guernesey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GN' COUNTRY_ID, 'Guinea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GW' COUNTRY_ID, 'Guinea-Bisáu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GY' COUNTRY_ID, 'Guyana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HT' COUNTRY_ID, 'Haití' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HM' COUNTRY_ID, 'Islas Heard y McDonald' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HN' COUNTRY_ID, 'Honduras' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HK' COUNTRY_ID, 'Hong Kong' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HU' COUNTRY_ID, 'Hungría' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IS' COUNTRY_ID, 'Islandia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IN' COUNTRY_ID, 'India' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ID' COUNTRY_ID, 'Indonesia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IR' COUNTRY_ID, 'Irán, República Islámica de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IQ' COUNTRY_ID, 'Irak' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IE' COUNTRY_ID, 'Irlanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IM' COUNTRY_ID, 'Isla de Man' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IL' COUNTRY_ID, 'Israel' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IT' COUNTRY_ID, 'Italia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'JM' COUNTRY_ID, 'Jamaica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'JP' COUNTRY_ID, 'Japón' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'JE' COUNTRY_ID, 'Jersey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'JO' COUNTRY_ID, 'Jordania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KZ' COUNTRY_ID, 'Kazajistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KE' COUNTRY_ID, 'Kenia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KI' COUNTRY_ID, 'Kiribati' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KP' COUNTRY_ID, 'Corea, República Popular Democrática de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KR' COUNTRY_ID, 'Corea, República de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KW' COUNTRY_ID, 'Kuwait' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KG' COUNTRY_ID, 'Kirguistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LA' COUNTRY_ID, 'República Democrática Popular de Laos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LV' COUNTRY_ID, 'Letonia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LB' COUNTRY_ID, 'Líbano' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LS' COUNTRY_ID, 'Lesoto' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LR' COUNTRY_ID, 'Liberia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LY' COUNTRY_ID, 'Libia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LI' COUNTRY_ID, 'Liechtenstein' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LT' COUNTRY_ID, 'Lituania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LU' COUNTRY_ID, 'Luxemburgo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MO' COUNTRY_ID, 'Macao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MK' COUNTRY_ID, 'Macedonia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MG' COUNTRY_ID, 'Madagascar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MW' COUNTRY_ID, 'Malaui' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MY' COUNTRY_ID, 'Malasia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MV' COUNTRY_ID, 'Maldivas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ML' COUNTRY_ID, 'Mali' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MT' COUNTRY_ID, 'Malta' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MH' COUNTRY_ID, 'Islas Marshall' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MQ' COUNTRY_ID, 'Martinica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MR' COUNTRY_ID, 'Mauritania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MU' COUNTRY_ID, 'Mauricio' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'YT' COUNTRY_ID, 'Mayotte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MX' COUNTRY_ID, 'México' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'FM' COUNTRY_ID, 'Micronesia, Estados Federados de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MD' COUNTRY_ID, 'Moldavia, República de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MC' COUNTRY_ID, 'Mónaco' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MN' COUNTRY_ID, 'Mongolia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ME' COUNTRY_ID, 'Montenegro' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MS' COUNTRY_ID, 'Montserrat' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MA' COUNTRY_ID, 'Marruecos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MZ' COUNTRY_ID, 'Mozambique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MM' COUNTRY_ID, 'Myanmar/Birmania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NA' COUNTRY_ID, 'Namibia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NR' COUNTRY_ID, 'Nauru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NP' COUNTRY_ID, 'Nepal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NL' COUNTRY_ID, 'Países Bajos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NC' COUNTRY_ID, 'Nueva Caledonia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NZ' COUNTRY_ID, 'Nueva Zelanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NI' COUNTRY_ID, 'Nicaragua' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NE' COUNTRY_ID, 'Níger' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NG' COUNTRY_ID, 'Nigeria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NU' COUNTRY_ID, 'Niue' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NF' COUNTRY_ID, 'Isla Norfolk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MP' COUNTRY_ID, 'Islas Marianas del Norte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NO' COUNTRY_ID, 'Noruega' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'OM' COUNTRY_ID, 'Omán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PK' COUNTRY_ID, 'Pakistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KM' COUNTRY_ID, 'Comoras' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CG' COUNTRY_ID, 'Congo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CD' COUNTRY_ID, 'Congo, República Democrática del' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CK' COUNTRY_ID, 'Islas Cook' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CR' COUNTRY_ID, 'Costa Rica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CI' COUNTRY_ID, 'Costa de Marfil' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'HR' COUNTRY_ID, 'Croacia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CU' COUNTRY_ID, 'Cuba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CW' COUNTRY_ID, 'Curazao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CY' COUNTRY_ID, 'Chipre' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CZ' COUNTRY_ID, 'República Checa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AF' COUNTRY_ID, 'Afganistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AL' COUNTRY_ID, 'Albania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'DZ' COUNTRY_ID, 'Argelia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AS' COUNTRY_ID, 'Samoa Americana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AD' COUNTRY_ID, 'Andorra' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AO' COUNTRY_ID, 'Angola' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AI' COUNTRY_ID, 'Anguila' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AQ' COUNTRY_ID, 'Antártida' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AG' COUNTRY_ID, 'Antigua y Barbuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AR' COUNTRY_ID, 'Argentina' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AM' COUNTRY_ID, 'Armenia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AW' COUNTRY_ID, 'Aruba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AU' COUNTRY_ID, 'Australia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AT' COUNTRY_ID, 'Austria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AZ' COUNTRY_ID, 'Azerbaiyán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BS' COUNTRY_ID, 'Bahamas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BH' COUNTRY_ID, 'Baréin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BD' COUNTRY_ID, 'Bangladés' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BB' COUNTRY_ID, 'Barbados' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BY' COUNTRY_ID, 'Bielorrusia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BE' COUNTRY_ID, 'Bélgica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BZ' COUNTRY_ID, 'Belice' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BJ' COUNTRY_ID, 'Benín' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BM' COUNTRY_ID, 'Bermudas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BT' COUNTRY_ID, 'Bután' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BO' COUNTRY_ID, 'Bolivia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BQ' COUNTRY_ID, 'Bonaire, San Eustaquio y Saba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BA' COUNTRY_ID, 'Bosnia y Herzegovina' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BW' COUNTRY_ID, 'Botsuana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BV' COUNTRY_ID, 'Isla Bouvet' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BR' COUNTRY_ID, 'Brasil' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'IO' COUNTRY_ID, 'Territorio Británico del Océano Índico' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BN' COUNTRY_ID, 'Estado de Brunéi Darusalam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BG' COUNTRY_ID, 'Bulgaria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BF' COUNTRY_ID, 'Burkina Faso' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BI' COUNTRY_ID, 'Burundi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KH' COUNTRY_ID, 'Camboya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CM' COUNTRY_ID, 'Camerún' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CA' COUNTRY_ID, 'Canadá' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CV' COUNTRY_ID, 'Cabo Verde' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KY' COUNTRY_ID, 'Islas Caimán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CF' COUNTRY_ID, 'República Centroafricana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TD' COUNTRY_ID, 'Chad' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CL' COUNTRY_ID, 'Chile' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CN' COUNTRY_ID, 'China' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CX' COUNTRY_ID, 'Isla Christmas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CC' COUNTRY_ID, 'Islas Cocos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CO' COUNTRY_ID, 'Colombia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PW' COUNTRY_ID, 'Palaos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PS' COUNTRY_ID, 'Territorios Palestinos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PA' COUNTRY_ID, 'Panamá' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PG' COUNTRY_ID, 'Papúa Nueva Guinea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PY' COUNTRY_ID, 'Paraguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PE' COUNTRY_ID, 'Perú' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PH' COUNTRY_ID, 'Filipinas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PN' COUNTRY_ID, 'Islas Pitcairn' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PL' COUNTRY_ID, 'Polonia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PT' COUNTRY_ID, 'Portugal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PR' COUNTRY_ID, 'Puerto Rico' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'QA' COUNTRY_ID, 'Qatar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'RE' COUNTRY_ID, 'Reunión' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'RO' COUNTRY_ID, 'Rumanía' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'RU' COUNTRY_ID, 'Federación de Rusia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'RW' COUNTRY_ID, 'Ruanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'BL' COUNTRY_ID, 'San Bartolomé' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SH' COUNTRY_ID, 'Santa Elena, Ascensión y Tristán da Cunha' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'KN' COUNTRY_ID, 'San Cristóbal y Nieves' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LC' COUNTRY_ID, 'Santa Lucía' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'MF' COUNTRY_ID, 'San Martín (zona francesa)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'PM' COUNTRY_ID, 'San Pedro y Miquelón' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VC' COUNTRY_ID, 'San Vicente y las Granadinas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'WS' COUNTRY_ID, 'Samoa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SM' COUNTRY_ID, 'San Marino' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ST' COUNTRY_ID, 'Santo Tomé y Príncipe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SA' COUNTRY_ID, 'Arabia Saudí' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SN' COUNTRY_ID, 'Senegal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'RS' COUNTRY_ID, 'Serbia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SC' COUNTRY_ID, 'Seychelles' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SL' COUNTRY_ID, 'Sierra Leona' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SG' COUNTRY_ID, 'Singapur' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SX' COUNTRY_ID, 'Sint Maarten (zona neerlandesa)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SK' COUNTRY_ID, 'Eslovaquia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SI' COUNTRY_ID, 'Eslovenia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SB' COUNTRY_ID, 'Islas Salomón' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SO' COUNTRY_ID, 'Somalia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ZA' COUNTRY_ID, 'Sudáfrica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GS' COUNTRY_ID, 'Georgia del Sur e Islas Sandwich del Sur' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SS' COUNTRY_ID, 'Sudán del Sur' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ES' COUNTRY_ID, 'España' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'LK' COUNTRY_ID, 'Sri Lanka' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SD' COUNTRY_ID, 'Sudán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SR' COUNTRY_ID, 'Surinam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SJ' COUNTRY_ID, 'Svalbard y Jan Mayen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SZ' COUNTRY_ID, 'Suazilandia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SE' COUNTRY_ID, 'Suecia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'CH' COUNTRY_ID, 'Suiza' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'SY' COUNTRY_ID, 'República Árabe Siria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TW' COUNTRY_ID, 'Taiwán (Provincia de China)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TJ' COUNTRY_ID, 'Tayikistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TZ' COUNTRY_ID, 'Tanzania, República Unida de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TH' COUNTRY_ID, 'Tailandia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TL' COUNTRY_ID, 'Timor Oriental' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TG' COUNTRY_ID, 'Togo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TK' COUNTRY_ID, 'Tokelau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TO' COUNTRY_ID, 'Tonga' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TT' COUNTRY_ID, 'Trinidad y Tobago' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TN' COUNTRY_ID, 'Túnez' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TR' COUNTRY_ID, 'Turquía' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TM' COUNTRY_ID, 'Turkmenistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TC' COUNTRY_ID, 'Islas Turcas y Caicos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'TV' COUNTRY_ID, 'Tuvalu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'UG' COUNTRY_ID, 'Uganda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'UA' COUNTRY_ID, 'Ucrania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AE' COUNTRY_ID, 'Emiratos Árabes Unidos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'GB' COUNTRY_ID, 'Reino Unido' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'US' COUNTRY_ID, 'Estados Unidos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'UM' COUNTRY_ID, 'Islas menores alejadas de los Estados Unidos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'UY' COUNTRY_ID, 'Uruguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'UZ' COUNTRY_ID, 'Uzbekistán' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VU' COUNTRY_ID, 'Vanuatu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VA' COUNTRY_ID, 'Santa Sede [Vaticano]' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VE' COUNTRY_ID, 'Venezuela' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VN' COUNTRY_ID, 'Vietnam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VG' COUNTRY_ID, 'Islas Vírgenes Británicas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'VI' COUNTRY_ID, 'Islas Vírgenes de los Estados Unidos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'WF' COUNTRY_ID, 'Islas Wallis y Futuna' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'EH' COUNTRY_ID, 'Sáhara Occidental' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'YE' COUNTRY_ID, 'Yemen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ZM' COUNTRY_ID, 'Zambia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'ZW' COUNTRY_ID, 'Zimbabue' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'AX' COUNTRY_ID, 'Islas Åland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, 'NEW' COUNTRY_ID, 'Nuevo país' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 4 LANG, '99' COUNTRY_ID, 'Varios' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COUNTRY base USING
( SELECT COUNTRY_ID COUNTRY_ID, COUNTRY_DESC COUNTRY_DESC FROM COUNTRY_TL TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4)) USE_THIS
ON ( base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.COUNTRY_DESC = use_this.COUNTRY_DESC;
--
DELETE FROM COUNTRY_TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
