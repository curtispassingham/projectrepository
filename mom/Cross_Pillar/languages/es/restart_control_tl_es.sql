SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'LIKE_STORE' PROGRAM_NAME, 'Proceso de tienda modelo' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'ang_saplgen' PROGRAM_NAME, 'Google - Extracto de registro de punto de venta' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'costcalc' PROGRAM_NAME, 'calcular descuentos de acuerdo en la tabla FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealact' PROGRAM_NAME, 'Calcula información de valores reales' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealcls' PROGRAM_NAME, 'cerrar acuerdos que hayan alcanzado close_date' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealday' PROGRAM_NAME, 'Sumas hasta día de acuerdo' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealex' PROGRAM_NAME, 'Desglosa información de acuerdo a ubicación de artículo' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealfct' PROGRAM_NAME, 'Calcula información de previsión' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealfinc' PROGRAM_NAME, 'Ingresos de acuerdo fijo en contabilidad general' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealinc' PROGRAM_NAME, 'Calcula ingresos' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dealprg' PROGRAM_NAME, 'depurar acuerdos cerrados para período especificado por sistema' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dfrtbld' PROGRAM_NAME, 'insertar registros en tabla DEAL_SKU_TEMP' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'discotbapply' PROGRAM_NAME, 'Aplicar descuento de presupuesto de compras' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'ditinsrt' PROGRAM_NAME, 'insertar registros en deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dlyprg' PROGRAM_NAME, 'depuración diaria' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'docclose' PROGRAM_NAME, 'cerrar recepciones sin asignar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'dsdupld' PROGRAM_NAME, 'cargar entrega directa en tienda' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'edidlinv' PROGRAM_NAME, 'Descarga de factura EDI' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'elccostcalc' PROGRAM_NAME, 'Calcular cambios de CDE en tabla FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'fifinvcu11i' PROGRAM_NAME, 'Carga de factura' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'fifinvcup' PROGRAM_NAME, 'Proceso de interfaz de factura' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'genpreiss' PROGRAM_NAME, 'Generar órdenes preemitidas' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'hstmthupd' PROGRAM_NAME, 'Actualizar SOH, precio de venta, avg_cost en item_loc_hist_mth' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'lclrbld' PROGRAM_NAME, 'volver a crear lote de listas de ubicaciones' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'lcup798' PROGRAM_NAME, 'disposiciones/cargos de carta de crédito' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'lcupld' PROGRAM_NAME, 'carga de carta de crédito' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'lifstkup' PROGRAM_NAME, 'conversión de carga de stock' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'mrt' PROGRAM_NAME, 'Crea transferencias a partir de TDM' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'mrtprg' PROGRAM_NAME, 'mrtprg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'mrtrtv' PROGRAM_NAME, 'Crea RTV para TDM de RTV' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'mrtupd' PROGRAM_NAME, 'mrtupd' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'nwppurge' PROGRAM_NAME, 'Depuración de principio de menor valoración' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'ociroq' PROGRAM_NAME, 'nulo' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'ordautcl' PROGRAM_NAME, 'Suprimir/cerrar OC' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'orddscnt' PROGRAM_NAME, 'descuento de orden de compra' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'prchstprg' PROGRAM_NAME, 'Depuración de historial de precios' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'precostcalc' PROGRAM_NAME, 'precalcular descuentos de acuerdo en deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'refmvl10nentity' PROGRAM_NAME, 'Refrescar vista materializada de entidad L10N' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'refmvlocprimaddr' PROGRAM_NAME, 'Refrescar vista materializada de dirección principal de ubicación' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saescheat' PROGRAM_NAME, 'Reversión de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpach' PROGRAM_NAME, 'Exportación de ReSA a ACH' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpdw' PROGRAM_NAME, 'Exportación de auditoría de ventas a RDW' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpgl' PROGRAM_NAME, 'Exportación de ReSA a contabilidad general de Oracle' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpgl107' PROGRAM_NAME, 'Exportación de ReSA a contabilidad general de Oracle' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpsim' PROGRAM_NAME, 'Exportación de auditoría de ventas a SIM' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saexpuar' PROGRAM_NAME, 'Exportación de auditoría de ventas a CCG' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'sagetref' PROGRAM_NAME, 'descargar datos de referencia de auditoría de ventas' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saimpadj' PROGRAM_NAME, 'Importación de ajustes de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saimptlog' PROGRAM_NAME, 'TLOG de importación de auditoría de ventas' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saimptlogfin' PROGRAM_NAME, 'Finalización de importación de TLOG de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saimptlogi' PROGRAM_NAME, 'TLOG de importación de auditoría de ventas' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'saimptlogtdup_upd' PROGRAM_NAME, 'Archivo TDUP de actualización de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'sapreexp' PROGRAM_NAME, 'Procesamiento previo de exportación de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'sapurge' PROGRAM_NAME, 'Depuración de datos de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'sarules' PROGRAM_NAME, 'Procesamiento de regla de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'sastdycr' PROGRAM_NAME, 'Creación de día de tienda de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'satotals' PROGRAM_NAME, 'Procesamiento total de ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'soupld' PROGRAM_NAME, 'carga de orden de tienda' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'stkschedxpld' PROGRAM_NAME, 'STOCK COUNT SCHEDULE EXPLODE' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'supsplit' PROGRAM_NAME, 'Lote de división de proveedor' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'szonrbld' PROGRAM_NAME, 'nueva creación de seguridad de zona de usuario' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'trandataprocess' PROGRAM_NAME, 'Carga de datos de transacción externa' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'vendinvc' PROGRAM_NAME, 'Facturación de proveedor para acuerdos complejos' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 4 LANG, 'vendinvf' PROGRAM_NAME, 'Facturación de proveedor para acuerdos fijos' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO RESTART_CONTROL base USING
( SELECT PROGRAM_NAME PROGRAM_NAME, PROGRAM_DESC PROGRAM_DESC FROM RESTART_CONTROL_TL TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4)) USE_THIS
ON ( base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE SET base.PROGRAM_DESC = use_this.PROGRAM_DESC;
--
DELETE FROM RESTART_CONTROL_TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
