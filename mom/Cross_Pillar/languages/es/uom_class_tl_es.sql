SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'AGG' UOM, 'AGG' UOM_TRANS, 'Gramos de contenido de plata' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'AUG' UOM, 'AGO' UOM_TRANS, 'Gramos de contenido de oro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BA' UOM, 'BA' UOM_TRANS, 'Barril' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BBL' UOM, 'BBL' UOM_TRANS, 'Barril' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BE' UOM, 'HAZ' UOM_TRANS, 'Lote' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BG' UOM, 'BOL' UOM_TRANS, 'Bolsa' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BI' UOM, 'BI' UOM_TRANS, 'Bandeja' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BJ' UOM, 'CB' UOM_TRANS, 'Cubo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BK' UOM, 'CT' UOM_TRANS, 'Cesta' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'BX' UOM, 'UOM' UOM_TRANS, 'Caja' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'C' UOM, 'C' UOM_TRANS, 'Centígrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CA' UOM, 'LAT' UOM_TRANS, 'Lata' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CAR' UOM, 'CT' UOM_TRANS, 'Quilate' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CBM' UOM, 'M3' UOM_TRANS, 'Metro cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CC' UOM, 'CM3' UOM_TRANS, 'Centímetro cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CFT' UOM, 'FT3' UOM_TRANS, 'Pies cúbicos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CG' UOM, 'CG' UOM_TRANS, 'Centigramo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CGM' UOM, 'CGM' UOM_TRANS, 'Gramo de contenido' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CKG' UOM, 'CKG' UOM_TRANS, 'Kilogramo de contenido' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CM' UOM, 'CM' UOM_TRANS, 'Centímetros' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CM2' UOM, 'CM2' UOM_TRANS, 'Centímetros cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CM3' UOM, 'CM3' UOM_TRANS, 'Centímetros cúbicos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CON' UOM, 'CON' UOM_TRANS, 'Contenedor' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CR' UOM, 'CJN' UOM_TRANS, 'Cajón' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CS' UOM, 'MLT' UOM_TRANS, 'Caja' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CT' UOM, 'CT' UOM_TRANS, 'Embalaje' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CTN' UOM, 'CTN' UOM_TRANS, 'Tonelada de contenido' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CU' UOM, 'N3' UOM_TRANS, 'Cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CUR' UOM, 'CI' UOM_TRANS, 'Curio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CY' UOM, 'RN' UOM_TRANS, 'Rendimiento limpio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CYG' UOM, 'GRN' UOM_TRANS, 'Gramo de rendimiento limpio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'CYK' UOM, 'KRN' UOM_TRANS, 'Kilogramo de rendimiento limpio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'D' UOM, 'DEN' UOM_TRANS, 'Denier' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'DEG' UOM, 'GRA' UOM_TRANS, 'Grado' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'DOZ' UOM, 'AGG' UOM_TRANS, 'Docena' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'DPC' UOM, 'DOC' UOM_TRANS, 'Piezas por docenas' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'DPR' UOM, 'DPR' UOM_TRANS, 'Par de docenas' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'DS' UOM, 'ADS' UOM_TRANS, 'Dosis' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'EA' UOM, 'UD' UOM_TRANS, 'Unidad suelta' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FBM' UOM, 'MFB' UOM_TRANS, 'Metros de fibra' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FIB' UOM, 'FIB' UOM_TRANS, 'Fibras' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FT' UOM, 'FT' UOM_TRANS, 'Pie' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FT2' UOM, 'FT2' UOM_TRANS, 'Pie cuadrado' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FT3' UOM, 'FT3' UOM_TRANS, 'Pie cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'FZ' UOM, 'OZL' UOM_TRANS, 'Onzas de fluido' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'G' UOM, 'G' UOM_TRANS, 'Gramo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GBQ' UOM, 'GBQ' UOM_TRANS, 'Gigabequerelios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GCN' UOM, 'BRCN' UOM_TRANS, 'Contenedores en bruto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GKG' UOM, 'GKG' UOM_TRANS, 'Gramos de contenido de oro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GL' UOM, 'GL' UOM_TRANS, 'Galón' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GM' UOM, 'G' UOM_TRANS, 'Gramos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GR' UOM, 'BR' UOM_TRANS, 'Bruto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GRL' UOM, 'GRL' UOM_TRANS, 'HTSUPLD Inserción automática' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GRS' UOM, 'BR' UOM_TRANS, 'Bruto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'GVW' UOM, 'PBRV' UOM_TRANS, 'Peso bruto de vehículo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'HND' UOM, 'CS' UOM_TRANS, 'Cientos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'HUN' UOM, 'C' UOM_TRANS, 'Cientos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'HZ' UOM, 'HZ' UOM_TRANS, 'Hercio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'IN' UOM, 'PULG' UOM_TRANS, 'Pulgada' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'IN2' UOM, 'PLG2' UOM_TRANS, 'Pulgada cuadrada' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'IN3' UOM, 'PLG3' UOM_TRANS, 'Pulgada cúbica' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'IRC' UOM, 'CII' UOM_TRANS, 'Código de ingresos interno' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'IRG' UOM, 'IRG' UOM_TRANS, 'Gramo de contenido de iridio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'JR' UOM, 'JR' UOM_TRANS, 'Frasco' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'JWL' UOM, 'JWL' UOM_TRANS, 'Julios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'K' UOM, 'K' UOM_TRANS, '1,000' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KCAL' UOM, 'KCAL' UOM_TRANS, 'Kilocalorías' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KG' UOM, 'KG' UOM_TRANS, 'Kilogramo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KHZ' UOM, 'KHZ' UOM_TRANS, 'Kilohercio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KM' UOM, 'KM' UOM_TRANS, 'Kilómetro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KM2' UOM, 'KM2' UOM_TRANS, 'Kilómetro cuadrado' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KM3' UOM, 'KM3' UOM_TRANS, 'Kilómetro cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KN' UOM, 'KN' UOM_TRANS, 'Kilonewton' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KPA' UOM, 'KPA' UOM_TRANS, 'Kilopascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KSB' UOM, 'KSB' UOM_TRANS, 'Kilo ladrillo estándar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KTS' UOM, 'KTS' UOM_TRANS, 'Kilogramos totales de azúcar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KVA' UOM, 'KVA' UOM_TRANS, 'Kilovoltio-amperios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KVAR' UOM, 'KVAR' UOM_TRANS, 'Kilovoltio-amperios reactivo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KW' UOM, 'KW' UOM_TRANS, 'Kilovatios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'KWH' UOM, 'KWH' UOM_TRANS, 'Kilovatios-hora' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'L' UOM, 'L' UOM_TRANS, 'Litro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'LB' UOM, 'LB' UOM_TRANS, 'Libras' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'LBS' UOM, 'LB' UOM_TRANS, 'Libras' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'LIN' UOM, 'LIN' UOM_TRANS, 'Lineal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'LNM' UOM, 'MLIN' UOM_TRANS, 'Metros lineales' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'M' UOM, 'M' UOM_TRANS, 'Metro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'M2' UOM, 'M2' UOM_TRANS, 'Metro cuadrado' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'M3' UOM, 'M3' UOM_TRANS, 'Metro cúbico' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MBQ' UOM, 'MBQ' UOM_TRANS, 'Megabequerelios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MC' UOM, 'MCM' UOM_TRANS, 'Milicurios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MG' UOM, 'MG' UOM_TRANS, 'Miligramo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MHZ' UOM, 'MHZ' UOM_TRANS, 'Megahercio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'ML' UOM, 'ML' UOM_TRANS, 'Mililitro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MM' UOM, 'MM' UOM_TRANS, 'Milímetro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MM2' UOM, 'MM2' UOM_TRANS, 'Milímetros cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MM3' UOM, 'MM3' UOM_TRANS, 'Milímetros cúbicos' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MPA' UOM, 'MPA' UOM_TRANS, 'Megapascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'MWH' UOM, 'MWH' UOM_TRANS, 'Megavatios-hora' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'NA' UOM, 'ND' UOM_TRANS, 'No disponible' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'NO' UOM, 'Nº' UOM_TRANS, 'Número' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'ODE' UOM, 'ODE' UOM_TRANS, 'Equivalente de agotamiento de la capa de ozono' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'OSG' UOM, 'OSG' UOM_TRANS, 'Gramos de contenido de osmio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'OZ' UOM, 'OZ' UOM_TRANS, 'Onza' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PACK' UOM, 'PAQ' UOM_TRANS, 'Paquete' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PAL' UOM, 'PAL' UOM_TRANS, 'Palé' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PC' UOM, 'UD' UOM_TRANS, 'Pieza' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PCS' UOM, 'UDS' UOM_TRANS, 'Piezas' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PDG' UOM, 'PDG' UOM_TRANS, 'Gramos de contenido de paladio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PF' UOM, '%VOL' UOM_TRANS, 'Graduación alcohólica' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PFL' UOM, '%VL' UOM_TRANS, 'Graduación alcohólica-litro' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PK' UOM, 'PQ' UOM_TRANS, 'Paquete' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PO' UOM, 'RCP' UOM_TRANS, 'Recipiente' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PRS' UOM, 'PAR' UOM_TRANS, 'Par' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PT' UOM, 'PT' UOM_TRANS, 'Pinta' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'PTG' UOM, 'PTG' UOM_TRANS, 'Gramos de contenido de plutonio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'QT' UOM, 'QT' UOM_TRANS, 'Cuarto de galón' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'RBA' UOM, 'RBA' UOM_TRANS, 'Fardos de algodón' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'RHG' UOM, 'RHG' UOM_TRANS, 'Gramo de contenido de rodio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'RPM' UOM, 'RPM' UOM_TRANS, 'Revoluciones por minuto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'RUG' UOM, 'RUG' UOM_TRANS, 'Gramos de contenido de rutenio' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'SBE' UOM, 'SBE' UOM_TRANS, 'Equivalente de ladrillo estándar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'SF' UOM, 'FT2' UOM_TRANS, 'Pies cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'SME' UOM, 'EM2' UOM_TRANS, 'Equivalente de metros cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'SQ' UOM, 'N2' UOM_TRANS, 'Cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'SQM' UOM, 'M2' UOM_TRANS, 'Metros cuadrados' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'T' UOM, 'T' UOM_TRANS, 'Tonelada métrica' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'THM' UOM, 'MM' UOM_TRANS, 'Mil metros' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'THS' UOM, 'UM' UOM_TRANS, 'Mil unidades' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'TNV' UOM, 'TNV' UOM_TRANS, 'Tonelada valor crudo' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'TON' UOM, 'T' UOM_TRANS, 'Toneladas' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'TS' UOM, 'TS' UOM_TRANS, 'Tonelada corta' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'V' UOM, 'V' UOM_TRANS, 'Voltios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'W' UOM, 'W' UOM_TRANS, 'Vatios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'WTS' UOM, 'W' UOM_TRANS, 'Vatios' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'X' UOM, 'X' UOM_TRANS, 'No se han recopilado unidades' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'YD' UOM, 'YD' UOM_TRANS, 'Yarda' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'YD2' UOM, 'YD2' UOM_TRANS, 'Yarda cuadrada' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 LANG, 'YD3' UOM, 'YD3' UOM_TRANS, 'Yarda cúbica' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
