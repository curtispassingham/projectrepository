SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_OTN_STIN' ERROR_CODE, 'Nº de transacción de PDV original - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este valor se definió en -1 para facilitar la carga de datos. Introduzca el número de transacción de PDV original que se está anulando.' REC_SOLUTION, 'Nº de transacción de PDV no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_RAMT_STIN' ERROR_CODE, 'Importe redondeado - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el importe redondeado correcto.' REC_SOLUTION, 'Importe redondeado no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_ROAMT_STIN' ERROR_CODE, 'Importe de redondeo - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el importe de redondeo correcto.' REC_SOLUTION, 'Importe de redondeo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_TRN_STIN' ERROR_CODE, 'Número de transacción THEAD - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este valor se ha establecido en -1 para facilitar la carga de datos. Introduzca el número de transacción de PDV para esta transacción.' REC_SOLUTION, 'Número de THEAD no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_VAL_STIN' ERROR_CODE, 'Valor - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el valor correcto.' REC_SOLUTION, 'Valor no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_CLS_STIN' ERROR_CODE, 'Clase de SKU - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca la clase de SKU correcta.' REC_SOLUTION, 'Clase no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_CUST_ORD_STIN' ERROR_CODE, 'Número de línea de orden de cliente - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el número de línea de orden de cliente correcto a través de la pantalla Atributos de cliente.' REC_SOLUTION, 'Línea de orden de cliente no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_DEP_STIN' ERROR_CODE, 'Departamento - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el departamento correcto.' REC_SOLUTION, 'Departamento no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TITEM - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Número de TITEM no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_INVALID_CW' ERROR_CODE, 'Indicador de peso variable de artículo - Indicador de peso variable no fijado para un artículo de peso.' ERROR_DESC, 'En la sección de detalle de artículo, verifique el indicador de peso variable..' REC_SOLUTION, 'Peso variable no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. TITEM en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro TITEM debe estar entre un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_MID_STIN' ERROR_CODE, 'ID medios - Carácter no numérico en campo numérico..' ERROR_DESC, 'Introduzca el ID de medios correcto a través de la pantalla Atributos de cliente..' REC_SOLUTION, 'ID de medios no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_OUR_STIN' ERROR_CODE, 'Precio de venta unitario original - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el precio de venta unitario original correcto.' REC_SOLUTION, 'Precio venta unidad original no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_QTY_SIGN' ERROR_CODE, 'Los estados de artículo Venta, Iniciar orden (ORI), Orden finalizada (ORD), Iniciar reserva (LIN) o Reserva finalizada (LCO) deben ser positivos.' ERROR_DESC, 'Modifique el signo de la cantidad de artículo.' REC_SOLUTION, 'Signo de cantidad artículo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_QTY_STIN' ERROR_CODE, 'Cantidad de artículo - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Introduzca cant artículo correcta. Registros artículo con estado artículo venta deben tener cantidad positiva, aquellos con estado artículo devolución deben tener cantidad negativa. Si estado artículo es nulo, signo debe ser opuesto artículo nulo.' REC_SOLUTION, 'Cantidad artículo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_SBC_STIN' ERROR_CODE, 'Subclase - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca la subclase correcta.' REC_SOLUTION, 'Subclase no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_SET_CW' ERROR_CODE, 'Indicador de peso variable de artículo - Indicador de peso variable no importado correctamente para este artículo.' ERROR_DESC, 'En la sección de detalle de artículo, verifique el indicador de peso variable..' REC_SOLUTION, 'Peso variable no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_SUP_STIN' ERROR_CODE, 'Suplemento de UPC - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el suplemento de UPC correcto.' REC_SOLUTION, 'Suplemento de UPC no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_UOM_QTY_STIN' ERROR_CODE, 'Cantidad UM de artículo - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca la cantidad de UM correcta.' REC_SOLUTION, 'Cantidad de UM no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_UOM_QTY_ZERO' ERROR_CODE, 'Para registros de artículos de peso variable con un estado de artículo de venta o devolución, la cantidad de UM no puede ser cero si la cantidad no es cero y viceversa.' ERROR_DESC, 'Introduzca una cantidad de UM de artículo.' REC_SOLUTION, 'Cantidad de UM no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_URT_STIN' ERROR_CODE, 'Precio de venta unitario - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el precio de venta unitario correcto.' REC_SOLUTION, 'Precio venta de unidad no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TITEM_WH_STIN' ERROR_CODE, 'Almacén de devolución - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el almacén de devolución correcto.' REC_SOLUTION, 'Almacén de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOO_MANY_FHEADS' ERROR_CODE, 'Se han encontrado otros registros FHEAD.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Demasiados registros de FHEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_IGTAX_AMT_GT_TOTRET' ERROR_CODE, 'El importe total de impuesto global es superior al precio de venta total.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Importe de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_IGTAX_AMT_STIN' ERROR_CODE, 'Total de importe de impuesto global - Carácter no numérico en campo numérico o el campo está vacío.' ERROR_DESC, 'Introduzca el importe total de impuesto global correcto.' REC_SOLUTION, 'Importe de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_CATT' ERROR_CODE, 'Las transacciones totales no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_CUST' ERROR_CODE, 'Las transacciones totales no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_DISC' ERROR_CODE, 'Las transacciones totales no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de total de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_ITEM' ERROR_CODE, 'Las transacciones totales no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_PYMT' ERROR_CODE, 'Las transacciones de total no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_TAX' ERROR_CODE, 'Las transacciones totales no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_TEND' ERROR_CODE, 'Las transacciones de total no deben tener registros de medio de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_REF_NO_1_REQ' ERROR_CODE, 'Es necesario que el total de transacciones tenga un ID total en el campo Número de referencia 1.' ERROR_DESC, 'Introduzca el ID total en el campo Número de referencia 1.' REC_SOLUTION, 'Falta el número de referencia' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_VALUE_REQ' ERROR_CODE, 'Es necesario que el total de transacciones tenga un valor total.' ERROR_DESC, 'Introduzca el valor total.' REC_SOLUTION, 'Falta el valor total' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TPYMT_AMT_STIN' ERROR_CODE, 'Importe de pago - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el importe de pago correcto' REC_SOLUTION, 'Importe de pago no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TPYMT_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TPYMT. Carácter no numérico en campo numérico o el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TPYMT no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TPYMT_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. Registro TPYMT en posición no válida' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro TPYMT debe estar antes de un registro TTEND. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_MISS_REC' ERROR_CODE, 'A la transacción le faltan subregistros.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Faltan subregistros' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_NOT_RECOGNIZED' ERROR_CODE, 'No se reconoce el tipo de transacción.' ERROR_DESC, 'El archivo de entrada está corrupto. Compruebe el proceso de conversión de TLOG a RTLOG o vuelva a sondear la tienda.' REC_SOLUTION, 'Tipo de transacción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_NO_REQ' ERROR_CODE, 'El número de transacción es necesario.' ERROR_DESC, 'Introduzca el número de transacción correcto.' REC_SOLUTION, 'Falta el número de transacción' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_XTRA_CUST' ERROR_CODE, 'Una transacción solo puede tener un registro de cliente.' ERROR_DESC, 'Suprima el registro de cliente extra de esta transacción.' REC_SOLUTION, 'Registro de cliente no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_XTRA_REC' ERROR_CODE, 'Se ha encontrado un tipo de registro desconocido.' ERROR_DESC, 'El archivo de entrada está corrupto. Los tipos de registro válidos son FHEAD, THEAD, TCUST, CATT, TITEM, IDISC, TTEND, TTAX, TTAIL y FTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Tipo de registro desconocido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRANLVLTAX_NO_IGTAX' ERROR_CODE, 'Una tienda configurada con un impuesto de nivel de transacción no debe tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Registro de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAIL_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TTAIL - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAIL_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. TTAIL en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAIL_TRC_STIN' ERROR_CODE, 'Contador de registro de transacciones TTAIL - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAIL_WITHOUT_THEAD' ERROR_CODE, 'Transacción parcial. TTAIL fuera de la transacción' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAX_AMT_STIN' ERROR_CODE, 'Importe de impuesto - Carácter no numérico en un campo numérico.' ERROR_DESC, 'Introduzca el importe de impuesto correcto.' REC_SOLUTION, 'Importe de impuesto no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAX_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TTAX - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. TTAX en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro TTAX debe estar entre un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_CHECK_NO_REQ' ERROR_CODE, 'El número de cheque está vacío o hay un carácter no numérico en el campo numérico.' ERROR_DESC, 'Introduzca el número de cheque correcto.' REC_SOLUTION, 'Número de cheque no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_COUPON_NO_REQ' ERROR_CODE, 'El número de cupón no puede estar vacío cuando el grupo de tipo de oferta es Cupón.' ERROR_DESC, 'Introduzca un cupón.' REC_SOLUTION, 'Falta el número de cupón' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TTEND - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TTEND no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. TTEND en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro TTEND debe estar entre un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_OCA_STIN' ERROR_CODE, 'Importe de moneda original - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca un importe de moneda original correcto.' REC_SOLUTION, 'Importe de moneda no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_TAM_STIN' ERROR_CODE, 'Importe de pago - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Introduzca el importe de pago correcto.' REC_SOLUTION, 'Importe de medio de pago no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TTEND_TTI_STIN' ERROR_CODE, 'El ID de tipo de medio de pago no forma parte del grupo de tipo de oferta o bien está vacío.' ERROR_DESC, 'Escoja un tipo de medio de pago de la lista.' REC_SOLUTION, 'Tipo de medio de pago no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'UPC_NOT_FOUND' ERROR_CODE, 'Este UPC no existe en RMS.' ERROR_DESC, 'Escoja un UPC de la lista.' REC_SOLUTION, 'UPC no encontrado' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'VENDOR_DATA_REQ' ERROR_CODE, 'Se necesitan los datos de un proveedor - Al menos uno de los siguientes campos debe tener un valor: Número de factura de proveedor, Número de referencia de pago o Número comprobante de entrega.' ERROR_DESC, 'Introduzca al menos uno de los siguientes valores: Número de factura de proveedor, Número de referencia de pago o Número comprobante de entrega.' REC_SOLUTION, 'Falta el proveedor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'VENDOR_NO_REQ' ERROR_CODE, 'El número de proveedor en THEAD es un campo necesario para un desembolso de proveedor por mercancía o por gasto.' ERROR_DESC, 'Elija un número de proveedor de la lista.' REC_SOLUTION, 'Falta el proveedor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'VOID_NO_PYMT' ERROR_CODE, 'Las transacciones de anulación no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_TAX' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DUP_TAXC' ERROR_CODE, 'Código de impuesto duplicado. La transacción se ha enviado al archivo de rechazos.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Código de impuesto duplicado' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FTAIL_FRC_STIN' ERROR_CODE, 'Contador de registros FTAIL - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_EXP_DATE' ERROR_CODE, 'Fecha de caducidad no válida para un comprobante.' ERROR_DESC, 'Introduzca una fecha de caducidad válida. El formato válido para la fecha de caducidad es AAAMMDD.' REC_SOLUTION, 'Fecha de caducidad no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_NON_MERCH_CODE' ERROR_CODE, 'Código de motivo no válido para una transacción de desembolso de gastos de proveedor o desembolso de mercancías de proveedor.' ERROR_DESC, 'Seleccione un código de motivo válido para la transacción.' REC_SOLUTION, 'Código de no mercancía no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_TEND' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NON_MERCH_ITEM_NO_REQ' ERROR_CODE, 'Se necesita un número de artículo que no sea de mercancía para este registro de artículo.' ERROR_DESC, 'Seleccione un artículo de no mercancía apropiado.' REC_SOLUTION, 'Falta el artículo de no mercancía' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_TAX' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_CUST' ERROR_CODE, 'Las transacciones de retiro no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_TAX' ERROR_CODE, 'Las transacciones de retiro no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_IGTAX' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_CUST' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_CUST' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CASHIER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'El ID de cajero es necesario debido al nivel del saldo.' ERROR_DESC, 'Introduzca el ID de cajero correcto.' REC_SOLUTION, 'ID de cajero no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CATT_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo CATT - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de CATT no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CATT_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. Registro CATT en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro CATT debe estar después de registro TCUST. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CC_NO_REQ' ERROR_CODE, 'El número de tarjeta de crédito es necesario.' ERROR_DESC, 'Introduzca el número de tarjeta de crédito correcto.' REC_SOLUTION, 'Falta el número de tarjeta de crédito' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_CATT' ERROR_CODE, 'La transacción de cierre no debe tener un registro de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_CUST' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_DISC' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_IGTAX' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_ITEM' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_TAX' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de impuestos.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_TEND' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_CATT' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_CUST' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener un registro de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_DISC' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener un registro de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_IGTAX' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_PYMT' ERROR_CODE, 'Las transacciones de condiciones tienda no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_TAX' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_TEND' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CREDIT_PROMO_ID_STIN' ERROR_CODE, 'El ID de promoción de crédito no es válido.' ERROR_DESC, 'Introduzca el ID de promoción de crédito correcto' REC_SOLUTION, 'Promoción de crédito no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CUST_ID_REQ' ERROR_CODE, 'Es necesario un número de identificación de cliente.' ERROR_DESC, 'Introduzca el número de identificación de cliente correcto.' REC_SOLUTION, 'Falta el ID de cliente' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CUST_ORD_ATTR_REQ' ERROR_CODE, 'Los atributos de orden de cliente son necesarios.' ERROR_DESC, 'Introduzca los atributos de orden de cliente a través de la pantalla Atributos de cliente.' REC_SOLUTION, 'Faltan atributos del cliente' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CUST_ORD_DATE_REQDINTITEM' ERROR_CODE, 'Se necesita la fecha de orden de cliente en su TITEM correspondiente.' ERROR_DESC, 'Introduzca la fecha de orden de cliente a través de la pantalla Atributos de cliente. El formato admitido es AAAAMMDD.' REC_SOLUTION, 'Falta la fecha de orden de cliente' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DATAUNEXPECTEDSTOREDAY' ERROR_CODE, 'No se esperan datos para este día de tienda.' ERROR_DESC, 'Tienda o fecha laborable no válidas. Compruebe la información de tienda, cierre de compañía, excepción de cierre de compañía y cierre de tienda.' REC_SOLUTION, 'Datos inesperados para el día de tienda' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_CATT' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_DISC' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_IGTAX' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_ITEM' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_PYMT' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_TEND' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DISC_REASON_REQ' ERROR_CODE, 'El motivo de un descuento en tienda no se puede dejar en blanco..' ERROR_DESC, 'Introduzca un motivo de descuento..' REC_SOLUTION, 'Falta el motivo de descuento' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DISC_REF_NO_REQ' ERROR_CODE, 'El número de referencia del descuento de una promoción no se puede dejar en blanco.' ERROR_DESC, 'Introduzca un número de referencia de descuento..' REC_SOLUTION, 'Falta la referencia de descuento' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DUP_REC' ERROR_CODE, 'Registro duplicado. Se ha enviado la transacción al archivo de rechazo.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción duplicada' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DUP_TAXC_COMPITEM' ERROR_CODE, 'Combinación de código de impuesto y número de artículo de componente duplicada. La transacción se ha enviado al archivo de rechazos.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Código de impuesto duplicado' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DUP_TRAN' ERROR_CODE, 'Número de transacción duplicado. Se ha enviado la transacción al archivo de rechazo.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción duplicada' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'EEXCH_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de cambio tengan al menos dos registros de artículo.' ERROR_DESC, 'Agregue uno o varios registros de artículos a esta transacción.' REC_SOLUTION, 'Faltan registros de artículos' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'EEXCH_NO_PYMT' ERROR_CODE, 'Las transacciones de cambio a la par no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Registro de pago no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FHEAD_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo FHEAD - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FHEAD no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FHEAD_NOT_FIRST' ERROR_CODE, 'FHEAD no es el primer registro del archivo.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FHEAD no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FILE_ERROR' ERROR_CODE, 'Se produjo un error durante la última operación de archivo.' ERROR_DESC, '' REC_SOLUTION, 'Error de archivo' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FTAIL_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo FTAIL - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FTAIL_IN_TRAN' ERROR_CODE, 'Transacción parcial. Hay un registro FTAIL en una transacción.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FTAIL_NOT_LAST' ERROR_CODE, 'FTAIL no es el último registro del archivo.' ERROR_DESC, 'El archivo de entrada no tiene un registro FTAIL' REC_SOLUTION, 'Falta el registro de FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FULFILL_ORDER_NO_ERR' ERROR_CODE, 'El número de orden de satisfacción solo es válido para órdenes de cliente externas cuyo artículo tenga el estado Orden finalizada o Cancelación de orden.' ERROR_DESC, 'Defina el número de orden de satisfacción en blanco.' REC_SOLUTION, 'Nº de orden de satisfacción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_DISC' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_IGTAX' ERROR_CODE, 'Las transacciones de anulación no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_ITEM' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_PYMT' ERROR_CODE, 'Las transacciones de anulación posterior no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_TEND' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de oferta.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RECORDS_REJECTED' ERROR_CODE, 'Se han rechazado registros durante la carga de esta tienda/día.' ERROR_DESC, 'Este archivo de entrada está dañado y no se puede cargar completamente. Edítelo o vuelva a elegir la tienda.' REC_SOLUTION, 'Registros rechazados' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_NO_DISC' ERROR_CODE, 'Las transacciones de reembolso no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_NO_IGTAX' ERROR_CODE, 'Las transacciones de reembolso no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_NO_ITEM' ERROR_CODE, 'Las transacciones de reembolso no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_NO_PYMT' ERROR_CODE, 'Las transacciones de reembolso no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_NO_TAX' ERROR_CODE, 'Las transacciones de reembolso no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REFUND_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones de reembolso tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Transacción de reembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REGISTER_ID_REQ' ERROR_CODE, 'El identificador de caja registradora es necesario porque las transacciones se generan en el nivel de caja registradora.' ERROR_DESC, 'Introduzca el ID de caja registradora correcto.' REC_SOLUTION, 'Falta el ID de registro' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'REGISTER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'El ID de caja registradora es necesario debido al nivel del saldo.' ERROR_DESC, 'Introduzca el ID de caja registradora correcto.' REC_SOLUTION, 'Falta el ID de registro' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETDISP_INV_RET' ERROR_CODE, 'La disposición de devolución solo es válida si hay un inventario asociado con la devolución.' ERROR_DESC, 'Defina la disposición de devolución en blanco.' REC_SOLUTION, 'Disposición de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETDISP_OMS' ERROR_CODE, 'La disposición de devolución solo debe utilizarse con una transacción de devolución que se haya originado y procesado en OMS.' ERROR_DESC, 'Defina la disposición de devolución en blanco.' REC_SOLUTION, 'Disposición de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETURN_DISP_REQ' ERROR_CODE, 'Se necesita la disposición de devolución.' ERROR_DESC, 'Elija una disposición de devolución válida.' REC_SOLUTION, 'Falta la disposición de devolución' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETURN_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de devolución tengan al menos un registro de artículo.' ERROR_DESC, 'Agregue un registro de artículo a esta transacción.' REC_SOLUTION, 'Transacción de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETURN_NO_PYMT' ERROR_CODE, 'Las transacciones de devolución no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETURN_TENDER_REQ' ERROR_CODE, 'Es obligatorio que las transacciones de devolución tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Falta el medio de pago de devolución' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETWH_EXT_ORD' ERROR_CODE, 'El almacén de devolución solo es válido para las órdenes de cliente externo.' ERROR_DESC, 'Defina el almacén de devolución en blanco.' REC_SOLUTION, 'Almacén de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETWH_OMS' ERROR_CODE, 'El almacén de devolución solo debe utilizarse en una transacción de devolución que se haya originado y procesado en OMS.' ERROR_DESC, 'Defina el almacén de devolución en blanco.' REC_SOLUTION, 'Almacén de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETWH_REQ' ERROR_CODE, 'Es necesario el almacén de devolución para una transacción de devolución cuyo tipo de ventas es Orden de cliente externo y que se ha originado de OMS.' ERROR_DESC, 'Agregue un almacén válido a esta transacción.' REC_SOLUTION, 'Almacén de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'SALE_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de venta tengan al menos un registro de artículo.' ERROR_DESC, 'Agregue un registro de artículo a esta transacción.' REC_SOLUTION, 'Faltan registros de artículos' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'SALE_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones de venta tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Faltan registros de medios de pago' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'SET_DROP_SHIP' ERROR_CODE, 'El envío directo no se ha importado correctamente para este artículo.' ERROR_DESC, 'En la sección de detalle de artículo, verifique el indicador de envío directo.' REC_SOLUTION, 'Envío directo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'SKU_NOT_FOUND' ERROR_CODE, 'El artículo está vacío o no existe en RMS.' ERROR_DESC, 'Seleccione un artículo de la lista.' REC_SOLUTION, 'Artículo no encontrado' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'STOREDAYNOTREADYTOBELOAD' ERROR_CODE, 'Este día de tienda no se encuentra en el estado de Preparado para cargar.' ERROR_DESC, 'Es posible que ya se hayan cargado los datos o que los datos seleccionados por el filtro no se hayan cargado en orden.' REC_SOLUTION, 'Día de tienda no preparado para cargar' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'STOREDAY_LOCK_FAILED' ERROR_CODE, 'No se puede obtener un bloqueo de escritura para un día de tienda. No se ha cargado un registro de bloqueo o el día de tienda está bloqueado por otro proceso.' ERROR_DESC, 'Vuelva a intentarlo después de haber liberado el bloqueo.' REC_SOLUTION, 'Fallo de bloqueo de día de tienda' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de nivel en tanque tengan al menos un registro de artículo.' ERROR_DESC, 'Agregue un registro de artículo a esta transacción.' REC_SOLUTION, 'Faltan registros de artículos' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_CATT' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_DISC' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_PYMT' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_TAX' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TCUST_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo TCUST - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de TCUST no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TCUST_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. TCUST en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro TCUST debe estar entre un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TERM_MARKER_NO_ERROR' ERROR_CODE, 'Este error se usa para comprobar que todos los datos estén cargados.' ERROR_DESC, 'Cargue todos los datos y ejecute saimptlogfin.' REC_SOLUTION, 'Sin errores' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo THEAD - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de THEAD no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. THEAD en posición no válida.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_LOC_NO_BANN' ERROR_CODE, 'La ubicación no tiene ID de banner..' ERROR_DESC, 'La tienda no tiene ID de banner asociado..' REC_SOLUTION, 'Falta el banner' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'FULFILL_ORDER_NO_REQ' ERROR_CODE, 'Se necesita el número de orden de satisfacción.' ERROR_DESC, 'Introduzca el número de orden de satisfacción correcto.' REC_SOLUTION, 'Falta número de orden de satisfacción' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'GARBAGE_IN_RECORD' ERROR_CODE, 'Se ha incorporado un elemento extraño al registro.' ERROR_DESC, 'El archivo de entrada está corrupto. Hay un carácter embebido en el registro. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Elemento extraño en el registro' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_COUPON_NO_REQ' ERROR_CODE, 'El número de cupón no puede estar vacío cuando el Tipo descuento es Cupón tienda.' ERROR_DESC, 'Introduzca un número de cupón.' REC_SOLUTION, 'Falta el número de cupón' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_FIL_STIN' ERROR_CODE, 'ID línea archivo IDISC - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Identificador de IDISC no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_INVALID_CW' ERROR_CODE, 'Indicador de descuento de peso variable - Indicador de peso variable no fijado para un artículo de peso.' ERROR_DESC, 'Introduzca el indicador de peso variable correcto.' REC_SOLUTION, 'Peso variable no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. Registro IDISC en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro IDISC debe estar después de un registro TITEM. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_PRC_STIN' ERROR_CODE, 'Componente de promoción de descuento no válido' ERROR_DESC, 'Introduzca un componente de promoción de descuento válido.' REC_SOLUTION, 'Componente de promoción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_QTY_STIN' ERROR_CODE, 'Cantidad de descuento - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Introduzca una cantidad de descuento válida.' REC_SOLUTION, 'Cantidad de descuento no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_SET_CW' ERROR_CODE, 'Indicador de descuento de peso variable - Indicador de peso variable no importado correctamente.' ERROR_DESC, 'Introduzca el indicador de peso variable correcto.' REC_SOLUTION, 'Peso variable no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_UDA_STIN' ERROR_CODE, 'Importe de descuento - Hay un carácter no numérico en un campo numérico o bien el campo está vacío.' ERROR_DESC, 'Introduzca un importe de descuento válido.' REC_SOLUTION, 'Importe de descuento no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_UOM_QTY_STIN' ERROR_CODE, 'Cantidad de UM de descuento - Carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca la cantidad de UM correcta.' REC_SOLUTION, 'UM de descuento no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDNT_ID_WITHOUT_IDNT_MTHD' ERROR_CODE, 'Existe identificación sin método de identificación.' ERROR_DESC, 'Introduzca un método de identificación válido de la lista.' REC_SOLUTION, 'Falta el método de identificación' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDNT_MTHD_WITHOUT_IDNT_ID' ERROR_CODE, 'No existe identificación para este método de identificación.' ERROR_DESC, 'Introduzca una identificación válida.' REC_SOLUTION, 'Falta la identificación' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IGTAX_FIL_STIN' ERROR_CODE, 'Identificador de línea de archivo impuesto global. Carácter no numérico en campo numérico o el campo está vacío.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Identificador de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IGTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Transacción parcial. Registro de impuesto global en posición no válida.' ERROR_DESC, 'El archivo de entrada está corrupto. Un registro IGTAX debe estar después de un registro TITEM-IDISC. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IGTAX_RAT_STIN' ERROR_CODE, 'Tipo impositivo global - Carácter no numérico en campo numérico o el campo está vacío.' ERROR_DESC, 'Introduzca el tipo impositivo global correcto.' REC_SOLUTION, 'Tipo de IGTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_BANNER_ID' ERROR_CODE, 'ID de banner no válido.' ERROR_DESC, 'Seleccione un ID de banner válido.' REC_SOLUTION, 'Banner no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_BIRTHDATE' ERROR_CODE, 'Fecha de nacimiento no válida.' ERROR_DESC, 'El formato válido para una fecha de nacimiento es AAAAMMDD.' REC_SOLUTION, 'Fecha de nacimiento no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_BUSINESS_DATE' ERROR_CODE, 'Falta la fecha laborable o no es válida.' ERROR_DESC, 'El formato válido para una fecha laborable es AAAAMMDD.' REC_SOLUTION, 'Fecha laborable no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CASHIER_ID' ERROR_CODE, 'El ID de PDV del cajero no tiene un ID de empleado que le corresponda.' ERROR_DESC, 'Introduzca el ID de empleado y el ID de PDV en la pantalla Mantenimiento de empleado.' REC_SOLUTION, 'ID de cajero no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CCAS' ERROR_CODE, 'Origen de autorización de tarjeta de crédito no válida.' ERROR_DESC, 'Elija un origen de autorización de tarjeta de crédito de la lista.' REC_SOLUTION, 'Origen de autorización no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CCEM' ERROR_CODE, 'Modo de entrada de tarjeta de crédito no válido.' ERROR_DESC, 'Elija un modo de entrada de tarjeta de crédito de la lista.' REC_SOLUTION, 'Modo entrada tarjeta crédito no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CCSC' ERROR_CODE, 'Condición especial de tarjeta crédito no válida.' ERROR_DESC, 'Elija una condición especial de tarjeta de crédito de la lista.' REC_SOLUTION, 'Condición no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CCVF' ERROR_CODE, 'Verificación de titular de tarjeta de crédito no válida.' ERROR_DESC, 'Elija una verificación de titular de tarjeta de crédito de la lista.' REC_SOLUTION, 'Verificación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CC_CHECKSUM' ERROR_CODE, 'Dígito de control de tarjeta de crédito no válido. Este número de tarjeta no es válido.' ERROR_DESC, 'Introduzca el número de tarjeta de crédito correcto.' REC_SOLUTION, 'Dígito de control no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CC_EXP_DATE' ERROR_CODE, 'Fecha de caducidad no válida.' ERROR_DESC, 'El formato válido para una fecha de caducidad es AAAAMMDD.' REC_SOLUTION, 'Fecha de caducidad no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CC_MASK' ERROR_CODE, 'Valor de máscara de tarjeta de crédito no válido.' ERROR_DESC, 'Introduzca el valor de máscara de tarjeta de crédito correcto.' REC_SOLUTION, 'Valor máscara TC no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CC_PREFIX' ERROR_CODE, 'El prefijo numérico de esta tarjeta de crédito no se encuentra dentro del rango especificado.' ERROR_DESC, 'Introduzca el número de tarjeta de crédito correcto.' REC_SOLUTION, 'Prefijo tarjeta crédito no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CHECK_NO' ERROR_CODE, 'Número de cheque no válido o carácter no numérico en campo numérico.' ERROR_DESC, 'Introduzca el número de cheque correcto.' REC_SOLUTION, 'Número de cheque no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CIDT' ERROR_CODE, 'Tipo de identificación de cliente no válido.' ERROR_DESC, 'Escoja un tipo de identificación de cliente de la lista.' REC_SOLUTION, 'Tipo de ID de cliente no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CUST_ORD_DATE' ERROR_CODE, 'Fecha de orden de cliente no válida.' ERROR_DESC, 'El formato válido para una fecha de orden de cliente es AAAAMMDD.' REC_SOLUTION, 'Fecha de orden de cliente no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_DISCOUNT_UOM' ERROR_CODE, 'La unidad de medida de descuento no es válida para el artículo.' ERROR_DESC, 'Seleccione una unidad de medida diferente que sea compatible con la unidad de medida estándar o defina la conversión entre la unidad de medida actual del descuento y la estándar.' REC_SOLUTION, 'UM de descuento no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_DOCUMENT' ERROR_CODE, 'Se necesita un cheque regalo o número de comprobante.' ERROR_DESC, 'Introduzca el cheque regalo/número de comprobante.' REC_SOLUTION, 'Documento no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_FTAIL_POS' ERROR_CODE, 'El registro FTAIL se encuentra en la posición incorrecta.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_FTAIL_TRAN_CNT' ERROR_CODE, 'El recuento de número de transacción FTAIL no es correcto.' ERROR_DESC, 'El archivo de entrada está corrupto. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Registro de FTAIL no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_ITEM_NO' ERROR_CODE, 'Número de artículo vacío.' ERROR_DESC, 'Escoja un número de artículo de la lista.' REC_SOLUTION, 'Artículo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_ITEM_SWIPED_IND' ERROR_CODE, 'Indicador de lectura del artículo no válido. El valor se ha fijado por defecto en ''Y''.' ERROR_DESC, 'Haga clic en la casilla de control Artículo leído para configurar el valor correcto.' REC_SOLUTION, 'Artículo pasado no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_NO_INV_RET_IND' ERROR_CODE, 'Indicador de devolución sin inventario no válido. El valor se ha definido en blanco por defecto.' ERROR_DESC, 'Elija en la lista desplegable Devolución sin inventario para definir el valor correcto.' REC_SOLUTION, 'Devolución no de inventario no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_ORIG_CURR' ERROR_CODE, 'El código de moneda original no es válido.' ERROR_DESC, 'Introduzca un código de moneda válido.' REC_SOLUTION, 'Código de moneda no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_ORRC' ERROR_CODE, 'Motivo de sustitución de precio no válido.' ERROR_DESC, 'Seleccione un motivo para sustituir precio de la lista.' REC_SOLUTION, 'Motivo sustitución precio no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_PRMT' ERROR_CODE, 'Tipo de promoción no válido.' ERROR_DESC, 'Seleccione un tipo de promoción de la lista.' REC_SOLUTION, 'Tipo de promoción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_REAC' ERROR_CODE, 'Código de motivo no válido.' ERROR_DESC, 'Escoja un código de motivo de la lista.' REC_SOLUTION, 'Código de motivo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_RTLOG_ORIG_SYS' ERROR_CODE, 'Sistema de origen de RTLOG no válido.' ERROR_DESC, 'No es posible importar datos con un sistema de origen RTLOG no válido.' REC_SOLUTION, 'Sistema de origen no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SACA' ERROR_CODE, 'Tipo de atributo de cliente no válido.' ERROR_DESC, 'Escoja un tipo de atributo de cliente de la lista.' REC_SOLUTION, 'Tipo de atributo de cliente no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SAIT' ERROR_CODE, 'Tipo de artículo no válido.' ERROR_DESC, 'Escoja un tipo de artículo de la lista.' REC_SOLUTION, 'Tipo de artículo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SALESPERSON_ID' ERROR_CODE, 'El ID de PDV del vendedor no tiene un ID de empleado que le corresponda.' ERROR_DESC, 'Introduzca el ID de empleado y el ID de PDV en la pantalla Mantenimiento de empleado o corrija el ID de PDV en el separador Empleados.' REC_SOLUTION, 'ID de vendedor no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SARR' ERROR_CODE, 'Motivo de devolución no válido.' ERROR_DESC, 'Elija un motivo de devolución de la lista.' REC_SOLUTION, 'Motivo de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SASY' ERROR_CODE, 'Tipo de ventas no válido.' ERROR_DESC, 'Elija un tipo de ventas de la lista.' REC_SOLUTION, 'Tipo de ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SASY_E' ERROR_CODE, 'Tipo de ventas no válido para transacciones de ventas, reservas o anulaciones.' ERROR_DESC, 'Elija un tipo de ventas de la lista.' REC_SOLUTION, 'Tipo de ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SELLING_UOM' ERROR_CODE, 'La unidad de medida de venta no es válida para el artículo.' ERROR_DESC, 'Seleccione una unidad de medida de venta diferente que sea compatible con la unidad estándar, o defina la conversión entre la unidad de medida actual y la estándar.' REC_SOLUTION, 'UM de venta no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SYSTEM_IND' ERROR_CODE, 'Indicador del sistema no válido.' ERROR_DESC, 'Escoja un indicador del sistema de la lista.' REC_SOLUTION, 'Indicador de sistema no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TAXC' ERROR_CODE, 'Tipo de impuesto no válido.' ERROR_DESC, 'Escoja un tipo de impuesto de la lista.' REC_SOLUTION, 'Tipo de impuesto no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TAX_AUTHORITY' ERROR_CODE, 'La administración fiscal no es válida o el campo está vacío.' ERROR_DESC, 'Introduzca una administración fiscal válida.' REC_SOLUTION, 'Administración fiscal no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TAX_IND' ERROR_CODE, 'Indicador de impuesto no válido. El valor se ha fijado por defecto en ''Y''.' ERROR_DESC, 'Haga clic en la casilla de control Indicador de impuesto para configurar el valor correcto.' REC_SOLUTION, 'Impuesto no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TENDER_ENDING' ERROR_CODE, 'Finalización de medio de pago no válida.' ERROR_DESC, 'Ajuste la pago según sea necesario' REC_SOLUTION, 'Finalización medio pago no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TENT' ERROR_CODE, 'Grupo de tipos de medios de pago no válido.' ERROR_DESC, 'Escoja un grupo de tipos de medios de pago de la lista.' REC_SOLUTION, 'Grupo de tipos de medios pago no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TRAN_DATETIME' ERROR_CODE, 'Fecha/hora de transacción no válida.' ERROR_DESC, 'Introduzca una fecha/hora de transacción válida.' REC_SOLUTION, 'Fecha/hora de transacción no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TRAT' ERROR_CODE, 'Tipo de transacción no válido.' ERROR_DESC, 'Escoja un tipo de transacción de la lista.' REC_SOLUTION, 'Tipo de transacción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TSYS' ERROR_CODE, 'Sistema de procesamiento de transacciones no válido. Se ha enviado la transacción al archivo de rechazo.' ERROR_DESC, 'Introduzca un sistema de procesamiento de transacciones válido.' REC_SOLUTION, 'Sistema de procesamiento no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_VATC' ERROR_CODE, 'Código de IVA no válido.' ERROR_DESC, 'Introduzca un código de IVA válido.' REC_SOLUTION, 'Código de IVA no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INV_RULE_DEF' ERROR_CODE, 'Esta regla no se calculó correctamente para el día de tienda.' ERROR_DESC, 'Revise la definición de la regla y actualícela según proceda.' REC_SOLUTION, 'Definición de regla no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INV_TOTAL_DEF' ERROR_CODE, 'Este total no se calculó correctamente para el día de tienda.' ERROR_DESC, 'Revise la definición del total y actualícela según proceda.' REC_SOLUTION, 'Definición de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'ITEMLVLTAX_NO_TTAX' ERROR_CODE, 'Una tienda configurada con el impuesto de nivel de artículo no debe tener registros de impuestos de nivel de transacción.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de transacción de esta transacción.' REC_SOLUTION, 'Registro de TTAX no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'ITEM_STATUS_REQ' ERROR_CODE, 'El estado del artículo no puede estar vacío.' ERROR_DESC, 'Escoja un estado de artículo de la lista.' REC_SOLUTION, 'Falta el estado del artículo' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_CATT' ERROR_CODE, 'Las transacciones de préstamo no deben tener un registro de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_CUST' ERROR_CODE, 'Las transacciones de préstamo no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_DISC' ERROR_CODE, 'Las transacciones de préstamos no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_IGTAX' ERROR_CODE, 'Las transacciones de préstamo no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_ITEM' ERROR_CODE, 'Las transacciones de préstamos no deben tener un registro de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_PYMT' ERROR_CODE, 'Las transacciones de préstamo no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones de préstamos tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de medidor tengan al menos un registro de artículo.' ERROR_DESC, 'Agregue un registro de artículo a esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_CATT' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_CUST' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_IGTAX' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_PYMT' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'METER_NO_TAX' ERROR_CODE, 'Las transacciones de medidor no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de medidor no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'MISSING_THEAD' ERROR_CODE, 'Falta el registro THEAD.' ERROR_DESC, 'El archivo de entrada está corrupto. Cada transacción debe tener un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Falta THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'MISSING_TRAN_BIG_GAP' ERROR_CODE, 'Falta una gran cantidad de transacciones.' ERROR_DESC, 'El archivo de entrada puede estar corrupto. Intente volver a sondear la tienda.' REC_SOLUTION, 'Gran diferencia de transacción' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'MISSING_TTAIL' ERROR_CODE, 'Falta el registro TTAIL.' ERROR_DESC, 'El archivo de entrada está corrupto. Cada transacción debe tener un registro THEAD y un TTAIL. Edite el archivo o vuelva a sondear la tienda.' REC_SOLUTION, 'Falta TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NO_INV_RET_IND_REQ' ERROR_CODE, 'Es necesario el indicador de devolución sin inventario para una transacción de devolución cuyo tipo de ventas es Orden de cliente externo y que se ha originado de OMS.' ERROR_DESC, 'Elija en la lista desplegable Devolución sin inventario para definir el valor correcto.' REC_SOLUTION, 'Devolución no de inventario no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_CATT' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_CUST' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_DISC' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_IGTAX' ERROR_CODE, 'La transacción sin venta no debe tener impuestos de nivel de artículo' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_ITEM' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_PYMT' ERROR_CODE, 'La transacción sin venta no debe tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NOSALE_NO_TEND' ERROR_CODE, 'Las transacciones sin venta no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Registro sin ventas no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NO_RULE_FOR_SALETOTAL' ERROR_CODE, 'No hay ninguna regla definida para el importe final del total de ventas.' ERROR_DESC, 'Asegúrese de que hay una regla definida en la tabla sa_rounding_rule_detail para el importe final del total de ventas.' REC_SOLUTION, 'Falta la regla' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_CATT' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_CUST' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_DISC' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_IGTAX' ERROR_CODE, 'Las transacciones de apertura no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_ITEM' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_TAX' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_TEND' ERROR_CODE, 'Las transacciones abiertas no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'ORGCUR_AMT_WITHOUT_ORGCUR' ERROR_CODE, 'El importe de moneda original existe sin el código de moneda original.' ERROR_DESC, 'Introduzca un código de moneda original válido.' REC_SOLUTION, 'Falta el código de moneda' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'ORGCUR_WITHOUT_ORGCUR_AMT' ERROR_CODE, 'No existe ningún importe de moneda original para el código de moneda original proporcionado.' ERROR_DESC, 'Introduzca un importe de moneda original correcto.' REC_SOLUTION, 'Falta el importe de moneda' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_CATT' ERROR_CODE, 'Las transacciones pagadas no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_CUST' ERROR_CODE, 'Las transacciones pagadas no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_DISC' ERROR_CODE, 'Las transacciones pagadas no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_TEND' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'THEAD_IN_TRAN' ERROR_CODE, 'Transacción parcial. THEAD en transacción.' ERROR_DESC, 'Este archivo de entrada está corrupto y no se puede cargar. Edítelo o vuelva a sondear la tienda.' REC_SOLUTION, 'Transacción parcial' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TRAN_OUT_BAL' ERROR_CODE, 'La transacción no está equilibrada. El total de los artículos y el importe de pago no es igual al total de los medios de pago.' ERROR_DESC, 'Ajuste los artículos, el medio de pago o el importe de pago según sea necesario.' REC_SOLUTION, 'Transacción descuadrada' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'CLOSE_NO_PYMT' ERROR_CODE, 'Las transacciones de cierre no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de cierre no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'COND_NO_ITEM' ERROR_CODE, 'Las transacciones de condiciones de tienda no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción condic tienda no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'DCLOSE_NO_CUST' ERROR_CODE, 'Las transacciones de cierre de día no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de cierre día no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'IDISC_DRN_STIN' ERROR_CODE, 'Número de promoción de descuento no válido.' ERROR_DESC, 'Introduzca un número de promoción de descuento válido.' REC_SOLUTION, 'Promoción no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_IDMH' ERROR_CODE, 'El método de identificación no es válido.' ERROR_DESC, 'Introduzca un método de identificación válido de la lista.' REC_SOLUTION, 'Método de identificación no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_RETURN_DISP' ERROR_CODE, 'Disposición de devolución no válida.' ERROR_DESC, 'Elija una disposición de devolución válida.' REC_SOLUTION, 'Disposición de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SADT' ERROR_CODE, 'Tipo de descuento no válido.' ERROR_DESC, 'Escoja un tipo de descuento de la lista.' REC_SOLUTION, 'Tipo de descuento no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SASI' ERROR_CODE, 'Estado de artículo no válido.' ERROR_DESC, 'Escoja un estado de artículo de la lista.' REC_SOLUTION, 'Estado de artículo no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_SUB_SACA' ERROR_CODE, 'Valor de atributo de cliente no válido.' ERROR_DESC, 'Escoja un valor de atributo de cliente de la lista.' REC_SOLUTION, 'Atributo de cliente no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_TRAS' ERROR_CODE, 'Tipo de subtransacción no válido.' ERROR_DESC, 'Escoja un tipo de subtransacción de la lista.' REC_SOLUTION, 'Tipo de subtransacción no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_VENDOR_NO' ERROR_CODE, 'Número de proveedor no válido.' ERROR_DESC, 'Utilice un proveedor de la tabla de socios para proveedores de gastos o de la tabla de proveedores para proveedores de mercancía.' REC_SOLUTION, 'Proveedor no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'LOAN_NO_TAX' ERROR_CODE, 'Las transacciones de préstamos no deben tener un registro de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de préstamo no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NON_MERCH_ITEM_NOT_FOUND' ERROR_CODE, 'Este artículo de no mercancía no existe en RMS.' ERROR_DESC, 'Seleccione un artículo de no mercancía apropiado.' REC_SOLUTION, 'El artículo de no mercancía no existe' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'NO_INV_RET_IND_RETURN' ERROR_CODE, 'El indicador de devolución sin inventario solo es válido para el estado del artículo de devolución.' ERROR_DESC, 'Defina el indicador en blanco.' REC_SOLUTION, 'Devolución no de inventario no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'OPEN_NO_PYMT' ERROR_CODE, 'Las transacciones de apertura no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de apertura no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_ITEM' ERROR_CODE, 'Las transacciones pagadas no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_DISC' ERROR_CODE, 'Las transacciones desembolsadas no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_DISC' ERROR_CODE, 'Las transacciones de retiro no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_PYMT' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de pago' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_TAX' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETDISP_EXT_ORD' ERROR_CODE, 'La disposición de devolución solo es válida para las órdenes de cliente externo.' ERROR_DESC, 'Defina la disposición de devolución en blanco.' REC_SOLUTION, 'Disposición de devolución no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'RETWH_NOT_FOUND' ERROR_CODE, 'El almacén de devolución no existe en RMS.' ERROR_DESC, 'Elija un almacén de la lista.' REC_SOLUTION, 'Almacén de devolución no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TANKDP_NO_IGTAX' ERROR_CODE, 'Las transacciones de nivel en tanque no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción nivel en tanque no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'TOTAL_NO_IGTAX' ERROR_CODE, 'Las transacciones de total no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de total no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_VENDOR_NO_MV' ERROR_CODE, 'Número de proveedor de mercancía no válido.' ERROR_DESC, 'Utilice un proveedor de la tabla de proveedores para proveedores de mercancía.' REC_SOLUTION, 'Proveedor de mercancía no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_VENDOR_NO_EV' ERROR_CODE, 'Número de proveedor de gastos no válido.' ERROR_DESC, 'Utilice un proveedor de la tabla de socios para proveedores de gastos.' REC_SOLUTION, 'Proveedor de gastos no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_IGTAX' ERROR_CODE, 'Las transacciones de ingresos no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_PYMT' ERROR_CODE, 'Las transacciones de ingresos no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_NO_TAX' ERROR_CODE, 'Las transacciones pagadas no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de ingreso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_REASON_CODE_REQ' ERROR_CODE, 'Es necesario que las transacciones pagadas tengan un código de motivo.' ERROR_DESC, 'Escoja un código de motivo de la lista.' REC_SOLUTION, 'Falta el código de motivo' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDIN_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones pagadas tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Faltan registros de medios de pago' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_CATT' ERROR_CODE, 'Las transacciones desembolsadas no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_CUST' ERROR_CODE, 'Las transacciones desembolsadas no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_IGTAX' ERROR_CODE, 'Las transacciones de desembolsos no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_ITEM' ERROR_CODE, 'Las transacciones desembolsadas no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_PYMT' ERROR_CODE, 'Las transacciones de desembolsos no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_NO_TAX' ERROR_CODE, 'Las transacciones desembolsadas no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción de desembolso no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_REASON_CODE_REQ' ERROR_CODE, 'Es necesario que las transacciones desembolsadas tengan un código de motivo.' ERROR_DESC, 'Escoja un código de motivo de la lista.' REC_SOLUTION, 'Falta el código de motivo' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PAIDOU_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones desembolsadas tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Faltan registros de medios de pago' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'POSDATATOOOLD' ERROR_CODE, 'Los datos de PDV son demasiado antiguos para cargarlos en el sistema.' ERROR_DESC, 'Compruebe los días posteriores a la venta en la pantalla Opciones sistema auditoría de ventas. Este campo contiene la cantidad de días hasta la fecha de hoy en que los datos de PDV se pueden cargar en el sistema.' REC_SOLUTION, 'Antiguos datos de PDV' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'POSFUTUREDATA' ERROR_CODE, 'Los datos de PDV son para una fecha laborable posterior a la fecha actual.' ERROR_DESC, 'No es posible importar datos para una fecha laborable en el futuro.' REC_SOLUTION, 'Fecha laborable no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'POSUNEXPECTEDSTOREDAY' ERROR_CODE, 'No se esperan datos de PDV para este día de tienda. No se espera que esta tienda esté abierta.' ERROR_DESC, 'La tienda y la fecha laborable son correctas. Sin embargo, no se esperan datos de PDV para este día de tienda. Compruebe la información de cierre de compañía, excepción de cierre de compañía y cierre de tienda correspondiente a esta tienda.' REC_SOLUTION, 'Datos de PDV inesperados' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PROM_COMP_REQ' ERROR_CODE, 'El componente de una promoción no se puede dejar en blanco..' ERROR_DESC, 'Introduzca un componente de promoción..' REC_SOLUTION, 'Falta el componente de promoción' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_CATT' ERROR_CODE, 'Las transacciones de retiro no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_IGTAX' ERROR_CODE, 'Las transacciones de recuperación no deben tener registros de impuestos de nivel de artículo.' ERROR_DESC, 'Suprima el registro de impuestos de nivel de artículo de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_ITEM' ERROR_CODE, 'Las transacciones de retiro no deben tener registros de artículo.' ERROR_DESC, 'Suprima el registro de artículo de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_NO_PYMT' ERROR_CODE, 'Las transacciones de recuperación no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción de recuperación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PULL_TENDER_REQ' ERROR_CODE, 'Es necesario que las transacciones de retiro tengan al menos un registro de pago.' ERROR_DESC, 'Agregue un registro de pago a esta transacción.' REC_SOLUTION, 'Faltan registros de medios de pago' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_ITEM_REQ' ERROR_CODE, 'Es necesario que las transacciones de prueba de bomba tengan como mínimo un registro de artículo.' ERROR_DESC, 'Agregue un registro de artículo a esta transacción.' REC_SOLUTION, 'Faltan registros de artículos' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_CATT' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_CUST' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de cliente.' ERROR_DESC, 'Suprima el registro de cliente de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_DISC' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de descuento.' ERROR_DESC, 'Suprima el registro de descuento de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_TAX' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de impuesto.' ERROR_DESC, 'Suprima el registro de impuesto de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PUMPT_NO_TEND' ERROR_CODE, 'Las transacciones de prueba de bomba no deben tener registros de pago.' ERROR_DESC, 'Suprima el registro de pago de esta transacción.' REC_SOLUTION, 'Transacción prueba de bomba no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'PVOID_NO_CATT' ERROR_CODE, 'Las transacciones posteriores a la anulación no deben tener registros de atributo de cliente.' ERROR_DESC, 'Suprima el registro de atributo de cliente de esta transacción.' REC_SOLUTION, 'Transacción de anulación no válida' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 4 LANG, 'INVLD_CAN_MASK' ERROR_CODE, 'Valor de máscara de número de cuenta corriente no válido.' ERROR_DESC, 'Introduzca el valor de máscara de número de cuenta corriente correcto.' REC_SOLUTION, 'Valor másc nº cta corriente no válido' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO SA_ERROR_CODES base USING
( SELECT ERROR_CODE ERROR_CODE, ERROR_DESC ERROR_DESC, REC_SOLUTION REC_SOLUTION, SHORT_DESC SHORT_DESC FROM SA_ERROR_CODES_TL TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4)) USE_THIS
ON ( base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE SET base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
--
DELETE FROM SA_ERROR_CODES_TL where lang = 4
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 4);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
