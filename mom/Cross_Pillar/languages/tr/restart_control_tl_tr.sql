SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'LIKE_STORE' PROGRAM_NAME, 'Benzer Mağaza işlemi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'ang_saplgen' PROGRAM_NAME, 'Google - POSLog Çıkarma' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'costcalc' PROGRAM_NAME, 'anlaşma indirimlerini FUTURE_COST tablosunda hesapla' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealact' PROGRAM_NAME, 'Fiili değer bilgilerini hesaplar.' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealcls' PROGRAM_NAME, 'close_date tarihine ulaşmış anlaşmaları kapat' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealday' PROGRAM_NAME, 'Anlaşma gününe kadar toplar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealex' PROGRAM_NAME, 'Anlaşma bilgilerini ürün lokasyonuna ayrıştırır' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealfct' PROGRAM_NAME, 'Tahmin bilgilerini hesaplar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealfinc' PROGRAM_NAME, 'Kebir Defterine Sabitlenmiş Anlaşma Geliri' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealinc' PROGRAM_NAME, 'Geliri hesaplar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dealprg' PROGRAM_NAME, 'anlaşmaların temizlenmesi sistem tarafından belirlenen bir dönem boyunca kapatıldı' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dfrtbld' PROGRAM_NAME, 'DEAL_SKU_TEMP tablosuna kayıt ekle' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'discotbapply' PROGRAM_NAME, 'İndirim SAA uygulama' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'ditinsrt' PROGRAM_NAME, 'deal_sku_temp tablosuna kayıt ekle' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dlyprg' PROGRAM_NAME, 'günlük temizleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'docclose' PROGRAM_NAME, 'atanmamış makbuzları kapat' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'dsdupld' PROGRAM_NAME, 'doğrudan mağaza teslim yüklemesi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'edidlinv' PROGRAM_NAME, 'EDI Fatura İndirme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'elccostcalc' PROGRAM_NAME, 'FUTURE_COST tablosunda ÖVM değişikliklerini hesapla' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'fifinvcu11i' PROGRAM_NAME, 'Fatura Karşıya Yüklemesi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'fifinvcup' PROGRAM_NAME, 'Fatura Arayüzü işlemi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'genpreiss' PROGRAM_NAME, 'Hazır siparişler oluştur' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'hstmthupd' PROGRAM_NAME, 'item_loc_hist_mth içinde eldeki stok, satış fiyatı, avg_cost güncelle' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'lclrbld' PROGRAM_NAME, 'lokasyon listesi işlem grubu yeniden oluşturma' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'lcup798' PROGRAM_NAME, 'akreditif kapatmaları/ücretleri' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'lcupld' PROGRAM_NAME, 'akreditif karşıya yükleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'lifstkup' PROGRAM_NAME, 'stok karşıya yükleme dönüştürmesi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'mrt' PROGRAM_NAME, 'TİT''lerden transferleri oluşturur' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'mrtprg' PROGRAM_NAME, 'mrtprg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'mrtrtv' PROGRAM_NAME, 'Sİ TİT''leri için Sİ oluşturur' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'mrtupd' PROGRAM_NAME, 'mrtupd' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'nwppurge' PROGRAM_NAME, 'En Düşük Değerleme İlke Temizliği' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'ociroq' PROGRAM_NAME, 'boş' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'ordautcl' PROGRAM_NAME, 'SAS''leri sil / kapat' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'orddscnt' PROGRAM_NAME, 'satınalma siparişi indirimi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'prchstprg' PROGRAM_NAME, 'Fiyat Tarihçesi Temizleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'precostcalc' PROGRAM_NAME, 'deal_sku_temp içinde anlaşma indirimlerini önceden hesapla' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'refmvl10nentity' PROGRAM_NAME, 'L10N varlık materyalleştirilmiş görünümünü yenile' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'refmvlocprimaddr' PROGRAM_NAME, 'Lokasyon birincil adresi materyalleştirilmiş görünümünü yeniler' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saescheat' PROGRAM_NAME, 'ReSA Devri' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpach' PROGRAM_NAME, 'OTM''ye ReSa aktarımı' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpdw' PROGRAM_NAME, 'RDW''ye Satış Denetlemesi Aktarma' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpgl' PROGRAM_NAME, 'Oracle KD''ye ReSA aktarımı' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpgl107' PROGRAM_NAME, 'Oracle KD''ye ReSA aktarımı' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpsim' PROGRAM_NAME, 'Satış Denetimi Dışa Aktarma - SIM' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saexpuar' PROGRAM_NAME, 'Satış Denetimi Dışa Aktarma - UAR' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'sagetref' PROGRAM_NAME, 'Satış Denetimi referans verilerini indir' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saimpadj' PROGRAM_NAME, 'ReSA Düzeltmelerin içe aktarılması' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saimptlog' PROGRAM_NAME, 'Satış Denetimi İçe Aktarma - TLOG' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saimptlogfin' PROGRAM_NAME, 'ReSA TLOG İçe Aktarma sonu' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saimptlogi' PROGRAM_NAME, 'Satış Denetimi İçe Aktarma - TLOG' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'saimptlogtdup_upd' PROGRAM_NAME, 'ReSA TDUP dosyası güncelleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'sapreexp' PROGRAM_NAME, 'ReSA dışa aktarma ön işlemi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'sapurge' PROGRAM_NAME, 'ReSA Veri Temizleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'sarules' PROGRAM_NAME, 'ReSA Kural İşleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'sastdycr' PROGRAM_NAME, 'ReSA Mağaza Günü Oluşturma' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'satotals' PROGRAM_NAME, 'ReSA Toplu İşleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'soupld' PROGRAM_NAME, 'mağaza siparişi karşıya yüklemesi' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'stkschedxpld' PROGRAM_NAME, 'STOCK COUNT SCHEDULE EXPLODE' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'supsplit' PROGRAM_NAME, 'Tedarikçi Bölünmüş İşlem Grubu' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'szonrbld' PROGRAM_NAME, 'kullanıcı bölge güvenliğini yeniden oluşturma' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'trandataprocess' PROGRAM_NAME, 'Harici İşlem veri yükleme' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'vendinvc' PROGRAM_NAME, 'Karmaşık Anlaşmalar için Satıcı Faturalaması' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 9 LANG, 'vendinvf' PROGRAM_NAME, 'Sabit Anlaşmalar için Satıcı Faturalaması' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO RESTART_CONTROL base USING
( SELECT PROGRAM_NAME PROGRAM_NAME, PROGRAM_DESC PROGRAM_DESC FROM RESTART_CONTROL_TL TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9)) USE_THIS
ON ( base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE SET base.PROGRAM_DESC = use_this.PROGRAM_DESC;
--
DELETE FROM RESTART_CONTROL_TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
