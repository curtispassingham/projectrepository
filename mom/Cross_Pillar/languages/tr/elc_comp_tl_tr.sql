SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'INSINT' COMP_ID, 'Uluslararası Sigorta' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, '200' COMP_ID, 'Uçak Navlunu' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, '300' COMP_ID, 'Şehirler Arası Navlun' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, '60' COMP_ID, '3. Taraf Lojistik' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, '55' COMP_ID, 'Dekonsolidasyon Ücretleri' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'ICING' COMP_ID, 'Dondurma Ücretleri' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'ORDCST' COMP_ID, 'Sipariş Maliyeti' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TEXP' COMP_ID, 'Toplam Gider' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TEXPC' COMP_ID, 'Toplam Gider İlçesi' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TEXPZ' COMP_ID, 'Toplam Gider Bölgesi' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'UNCST' COMP_ID, 'Birim Maliyet' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BAPAL' COMP_ID, 'Palet başına Çift Yönlü Taşıma Ödeneği' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BACWT' COMP_ID, '100 lb Ağırlık başına Çift Yönlü Taşıma Ödeneği' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BAEA' COMP_ID, 'Her Biri başına Çift Yönlü Taşıma Ödeneği' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TSFINSUR' COMP_ID, 'Aktarma Sigortası' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'WHFEE' COMP_ID, 'Depoda Saklama Ücreti' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'WHPROC' COMP_ID, 'Depo İşleme Ücreti' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'AGCOMM' COMP_ID, 'Temsilci Komisyonu' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'ILFRT' COMP_ID, 'Şehir İçi Navlun' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BUYCOMM' COMP_ID, 'Satın Almacı Komisyonu' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'OCFRT' COMP_ID, 'Okyanus Ötesi Navlun' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'INSUR' COMP_ID, 'Sigorta' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'SELLCOMM' COMP_ID, 'Satıcı Komisyonu' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'ROYALTY' COMP_ID, 'Telif Ücreti' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'VFD25US' COMP_ID, 'ABD Harç için Değerin %25''i' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'VFD50US' COMP_ID, 'ABD Harç için Değerin %50''si' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'VFD75US' COMP_ID, 'ABD Harç için Değerin %75''i' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'MPFUS' COMP_ID, 'Mal İşleme Ücreti US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TDTYPE' COMP_ID, 'Toplam Harç ABD' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'HMFUS' COMP_ID, 'Liman Güncelleme Ücreti US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'ADUS' COMP_ID, 'Damping Karşıtı US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'CVDUS' COMP_ID, 'Dengeleme US' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TDTYUS' COMP_ID, 'Toplam Harç ABD' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BACS' COMP_ID, 'Kutu başına Çift Yönlü Taşıma Ödeneği' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'BACFT' COMP_ID, 'Feet Küp başına Çift Yönlü Taşıma Ödeneği' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 9 LANG, 'TSFFRGHT' COMP_ID, 'Aktarmalı Navlun' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ELC_COMP base USING
( SELECT COMP_ID COMP_ID, COMP_DESC COMP_DESC FROM ELC_COMP_TL TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9)) USE_THIS
ON ( base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE SET base.COMP_DESC = use_this.COMP_DESC;
--
DELETE FROM ELC_COMP_TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
