SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 1 CODE, 'Net Satış' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 2 CODE, 'KDV Hariç Net Satışlar' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 3 CODE, 'Envanter Dışı Ürün Satışları/İadeleri' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 4 CODE, 'İadeler' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 5 CODE, 'KDV Hariç Envanter dışı Satışlar' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 6 CODE, 'Anlaşma geliri (Satışlar)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 7 CODE, 'Anlaşma geliri (Satın Almalar)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 8 CODE, 'Sabit Gelir Tahakkuku' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 10 CODE, 'Ağırlık Sapması' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 11 CODE, 'Marj' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 12 CODE, 'Marj İptali' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 13 CODE, 'Kalıcı İndirim' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 14 CODE, 'İndirim İptali' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 15 CODE, 'Promosyon İndirimi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 16 CODE, 'Sezon Sonu İndirimi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 17 CODE, 'Şirketlerarası Satış Marjı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 18 CODE, 'Şirketlerarası İndirim' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 20 CODE, 'Satın Almalar' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 21 CODE, 'Yalnız Oracle KD arayüzünde kullanılan fatura' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 22 CODE, 'Stok Düzeltmesi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 23 CODE, 'Stok Düzeltmesi - Satılan Mal' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 24 CODE, 'Satıcıya İade' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 25 CODE, 'Kullanılamayan Envanter Transferi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 26 CODE, 'Navlun' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 27 CODE, 'Kalite Kontrol - Satıcıya İade' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 28 CODE, 'Kar Ek Masrafı - Alım Lokasyonu' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 29 CODE, 'Gider Ek Masrafı - Alım Lokasyonu' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 30 CODE, 'Gelen Transferler' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 31 CODE, 'Defter Transferleri Gelen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 32 CODE, 'Giden Transferler' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 33 CODE, 'Defter Transferleri Giden' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 34 CODE, 'Yeniden Sınıflandırmalar Gelen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 36 CODE, 'Yeniden Sınıflandırmalar Giden' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 37 CODE, 'Şirketlerarası Gelen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 38 CODE, 'Şirketlerarası Giden' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 39 CODE, 'Şirketlerarası Marj' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 41 CODE, 'Veri Deposu Stok Defteri Düzeltmesi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 44 CODE, 'Veri Deposu Gelen Transfer Alındısı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 50 CODE, 'Açık Stok' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 51 CODE, 'Küçülme' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 52 CODE, 'Kapalı Stok' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 53 CODE, 'Brüt Marj' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 54 CODE, 'Bugüne Kadarki Satışa Uygun Mallar' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 55 CODE, 'Şirketlerarası Stok Hesaplaması Satış Tutarı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 56 CODE, 'Şirketlerarası Stok Hesaplaması Küçülme Tutarı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 57 CODE, 'Stok Hesaplaması Aylık Kümüle Satış Tutarı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 58 CODE, 'Stok Hesaplaması Aylık Kümüle Küçülme Tutarı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 59 CODE, 'Stok Hesaplaması Defter Stok Maliyeti' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 60 CODE, 'Personel İskontosu' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 61 CODE, 'Stok Hesaplaması Actstk Maliyet/Satış Fiyatı' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 62 CODE, 'Navlun Talebi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 63 CODE, 'İE Aktivitesi - Envanter Güncelle' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 64 CODE, 'İE Aktivitesi - Mali Tablolara Yansıtılan Maliyet' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 65 CODE, 'Yeniden Stoklama Ücreti' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 70 CODE, 'Maliyet Sapması' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 71 CODE, 'Maliyet Sapması - Satış Fiyatı Muhasebe' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 72 CODE, 'Maliyet Sapması - Maliyet Muhasebesi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 73 CODE, 'Maliyet Sapması - Al. Maliyet Dzlt  FiFO' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 74 CODE, 'Hedef Lokasyon için Kurtarılabilir Vergi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 75 CODE, 'Kaynak Lokasyon için Kurtarılabilir Vergi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 80 CODE, 'İşyeri/Diğer Satış Maliyetleri' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 81 CODE, 'Nakit İndirimi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 82 CODE, 'Franchise Satış' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 83 CODE, 'Franchise İadeler' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 84 CODE, 'Franchise Satış Marjları' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 85 CODE, 'Franchise İndirimleri' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 86 CODE, 'Franchise Yeniden Stoklama ücreti' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 87 CODE, 'Maliyet içinde KDV' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 9 LANG, 88 CODE, 'Çıkış Satış Fiyatında KDV' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES base USING
( SELECT CODE CODE, DECODE DECODE FROM TRAN_DATA_CODES_TL TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9)) USE_THIS
ON ( base.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE SET base.DECODE = use_this.DECODE;
--
DELETE FROM TRAN_DATA_CODES_TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
