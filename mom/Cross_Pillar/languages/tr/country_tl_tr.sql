SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AF' COUNTRY_ID, 'Afganistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AL' COUNTRY_ID, 'Arnavutluk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DZ' COUNTRY_ID, 'Cezayir' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AS' COUNTRY_ID, 'Amerikan Samoası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AD' COUNTRY_ID, 'Andorra' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AO' COUNTRY_ID, 'Angola' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AI' COUNTRY_ID, 'Anguilla' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AQ' COUNTRY_ID, 'Antarktika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AG' COUNTRY_ID, 'Antigua ve Barbuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AR' COUNTRY_ID, 'Arjantin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AM' COUNTRY_ID, 'Ermenistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AW' COUNTRY_ID, 'Aruba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AU' COUNTRY_ID, 'Avustralya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AT' COUNTRY_ID, 'Avusturya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AZ' COUNTRY_ID, 'Azerbaycan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BS' COUNTRY_ID, 'Bahamalar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BH' COUNTRY_ID, 'Bahreyn' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BD' COUNTRY_ID, 'Bangladeş' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BB' COUNTRY_ID, 'Barbados' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BY' COUNTRY_ID, 'Belarus' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BE' COUNTRY_ID, 'Belçika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BZ' COUNTRY_ID, 'Belize' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BJ' COUNTRY_ID, 'Benin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BM' COUNTRY_ID, 'Bermuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IO' COUNTRY_ID, 'İngiliz Hint Okyanusu Toprakları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BN' COUNTRY_ID, 'Brunei Darüsselam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BG' COUNTRY_ID, 'Bulgaristan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BF' COUNTRY_ID, 'Burkina Faso' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BI' COUNTRY_ID, 'Burundi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KH' COUNTRY_ID, 'Kamboçya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CM' COUNTRY_ID, 'Kamerun' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CA' COUNTRY_ID, 'Kanada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CV' COUNTRY_ID, 'Cabo Verde' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KY' COUNTRY_ID, 'Cayman Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CF' COUNTRY_ID, 'Orta Afrika Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TD' COUNTRY_ID, 'Çad' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CL' COUNTRY_ID, 'Şili' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CN' COUNTRY_ID, 'Çin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CX' COUNTRY_ID, 'Christmas Adası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CC' COUNTRY_ID, 'Cocos (Keeling) Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CO' COUNTRY_ID, 'Kolombiya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KM' COUNTRY_ID, 'Komorlar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CG' COUNTRY_ID, 'Kongo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CD' COUNTRY_ID, 'Demokratik Kongo Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CK' COUNTRY_ID, 'Cook Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CR' COUNTRY_ID, 'Kosta Rika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CI' COUNTRY_ID, 'Fildişi Sahili' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HR' COUNTRY_ID, 'Hırvatistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CU' COUNTRY_ID, 'Küba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CW' COUNTRY_ID, 'Kurasao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CY' COUNTRY_ID, 'Kıbrıs Rum Kesimi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CZ' COUNTRY_ID, 'Çek Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DK' COUNTRY_ID, 'Danimarka' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DJ' COUNTRY_ID, 'Cibuti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DM' COUNTRY_ID, 'Dominika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DO' COUNTRY_ID, 'Dominik Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'EC' COUNTRY_ID, 'Ekvador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'EG' COUNTRY_ID, 'Mısır' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SV' COUNTRY_ID, 'El Salvador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GQ' COUNTRY_ID, 'Ekvator Ginesi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ER' COUNTRY_ID, 'Eritre' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'EE' COUNTRY_ID, 'Estonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ET' COUNTRY_ID, 'Etiyopya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FK' COUNTRY_ID, 'Falkland Adaları, Malvinas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FO' COUNTRY_ID, 'Faroe Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FJ' COUNTRY_ID, 'Fiji' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FI' COUNTRY_ID, 'Finlandiya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FR' COUNTRY_ID, 'Fransa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GF' COUNTRY_ID, 'Fransız Guyanası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PF' COUNTRY_ID, 'Fransız Polinezyası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TF' COUNTRY_ID, 'Fransız Güney Toprakları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GA' COUNTRY_ID, 'Gabon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GM' COUNTRY_ID, 'Gambiya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GE' COUNTRY_ID, 'Gürcistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'DE' COUNTRY_ID, 'Almanya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GH' COUNTRY_ID, 'Gana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GI' COUNTRY_ID, 'Cebelitarık' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GR' COUNTRY_ID, 'Yunanistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GL' COUNTRY_ID, 'Grönland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GD' COUNTRY_ID, 'Grenada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GP' COUNTRY_ID, 'Guadelupe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GU' COUNTRY_ID, 'Guam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GT' COUNTRY_ID, 'Guatemala' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GG' COUNTRY_ID, 'Guernsey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GN' COUNTRY_ID, 'Gine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GW' COUNTRY_ID, 'Gine Bissau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GY' COUNTRY_ID, 'Guyana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HT' COUNTRY_ID, 'Haiti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HM' COUNTRY_ID, 'Heard Adası ve McDonald Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HN' COUNTRY_ID, 'Honduras' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HK' COUNTRY_ID, 'Hong Kong' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'HU' COUNTRY_ID, 'Macaristan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IS' COUNTRY_ID, 'İzlanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IN' COUNTRY_ID, 'Hindistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ID' COUNTRY_ID, 'Endonezya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IR' COUNTRY_ID, 'İran İslam Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IQ' COUNTRY_ID, 'Irak' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IE' COUNTRY_ID, 'İrlanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IM' COUNTRY_ID, 'Isle of Man' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IL' COUNTRY_ID, 'İsrail' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'IT' COUNTRY_ID, 'İtalya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'JM' COUNTRY_ID, 'Jamaika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'JP' COUNTRY_ID, 'Japonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'JE' COUNTRY_ID, 'Jersey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'JO' COUNTRY_ID, 'Ürdün' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KZ' COUNTRY_ID, 'Kazakistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KE' COUNTRY_ID, 'Kenya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KI' COUNTRY_ID, 'Kiribati' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KP' COUNTRY_ID, 'Kore Demokratik Halk Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KR' COUNTRY_ID, 'Kore Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KW' COUNTRY_ID, 'Kuveyt' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KG' COUNTRY_ID, 'Kırgızistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LA' COUNTRY_ID, 'Lao Halkları Demokratik Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LV' COUNTRY_ID, 'Letonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LB' COUNTRY_ID, 'Lübnan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LS' COUNTRY_ID, 'Lesotho' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LR' COUNTRY_ID, 'Liberya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LY' COUNTRY_ID, 'Libya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LI' COUNTRY_ID, 'Liechtenstein' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LT' COUNTRY_ID, 'Litvanya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LU' COUNTRY_ID, 'Lüksemburg' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MO' COUNTRY_ID, 'Makao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MK' COUNTRY_ID, 'Makedonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MG' COUNTRY_ID, 'Madagaskar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MW' COUNTRY_ID, 'Malavi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MY' COUNTRY_ID, 'Malezya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MV' COUNTRY_ID, 'Maldivler' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ML' COUNTRY_ID, 'Mali' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MT' COUNTRY_ID, 'Malta' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MH' COUNTRY_ID, 'Marshall Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MQ' COUNTRY_ID, 'Martinik' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MR' COUNTRY_ID, 'Moritanya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MU' COUNTRY_ID, 'Mauritius' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'YT' COUNTRY_ID, 'Mayotte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MX' COUNTRY_ID, 'Meksika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'FM' COUNTRY_ID, 'Mikronezya Federal Devletleri' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MD' COUNTRY_ID, 'Moldova Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MC' COUNTRY_ID, 'Monako' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MN' COUNTRY_ID, 'Moğolistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ME' COUNTRY_ID, 'Karadağ' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MS' COUNTRY_ID, 'Montserrat' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MA' COUNTRY_ID, 'Fas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MZ' COUNTRY_ID, 'Mozambik' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BT' COUNTRY_ID, 'Butan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BO' COUNTRY_ID, 'Bolivya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BQ' COUNTRY_ID, 'Bonaire, Sint Eustatius ve Saba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BA' COUNTRY_ID, 'Bosna Hersek' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BW' COUNTRY_ID, 'Botsvana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BV' COUNTRY_ID, 'Bouvet Adası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BR' COUNTRY_ID, 'Brezilya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MM' COUNTRY_ID, 'Myanmar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NA' COUNTRY_ID, 'Namibya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NR' COUNTRY_ID, 'Nauru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NP' COUNTRY_ID, 'Nepal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NL' COUNTRY_ID, 'Hollanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NC' COUNTRY_ID, 'Yeni Kaledonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NZ' COUNTRY_ID, 'Yeni Zelanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NI' COUNTRY_ID, 'Nikaragua' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NE' COUNTRY_ID, 'Nijer' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NG' COUNTRY_ID, 'Nijerya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NU' COUNTRY_ID, 'Niue' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NF' COUNTRY_ID, 'Norfolk Adası' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MP' COUNTRY_ID, 'Kuzey Mariana Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NO' COUNTRY_ID, 'Norveç' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'OM' COUNTRY_ID, 'Umman' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PK' COUNTRY_ID, 'Pakistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PW' COUNTRY_ID, 'Palau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PS' COUNTRY_ID, 'Filistin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PA' COUNTRY_ID, 'Panama' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PG' COUNTRY_ID, 'Papua Yeni Gine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PY' COUNTRY_ID, 'Paraguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PE' COUNTRY_ID, 'Peru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PH' COUNTRY_ID, 'Filipinler' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PN' COUNTRY_ID, 'Pitcairn' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PL' COUNTRY_ID, 'Polonya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PT' COUNTRY_ID, 'Portekiz' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PR' COUNTRY_ID, 'Porto Riko' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'QA' COUNTRY_ID, 'Katar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'RE' COUNTRY_ID, 'Réunion' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'RO' COUNTRY_ID, 'Romanya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'RU' COUNTRY_ID, 'Rusya Federasyonu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'RW' COUNTRY_ID, 'Ruanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'BL' COUNTRY_ID, 'Saint Barthélemy' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SH' COUNTRY_ID, 'Saint Helena, Ascension ve Tristan da Cunha' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'KN' COUNTRY_ID, 'Saint Kitts ve Nevis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LC' COUNTRY_ID, 'Saint Lucia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'MF' COUNTRY_ID, 'Saint Martin (Fransız bölgesi)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'PM' COUNTRY_ID, 'Saint Pierre ve Miquelon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VC' COUNTRY_ID, 'Saint Vincent ve Grenadines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'WS' COUNTRY_ID, 'Samoa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SM' COUNTRY_ID, 'San Marino' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ST' COUNTRY_ID, 'Sao Tome ve Principe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SA' COUNTRY_ID, 'Suudi Arabistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SN' COUNTRY_ID, 'Senegal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'RS' COUNTRY_ID, 'Sırbistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SC' COUNTRY_ID, 'Seyşeller' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SL' COUNTRY_ID, 'Sierra Leone' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SG' COUNTRY_ID, 'Singapur' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SX' COUNTRY_ID, 'Sint Maarten (Hollanda bölgesi)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SK' COUNTRY_ID, 'Slovakya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SI' COUNTRY_ID, 'Slovenya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SB' COUNTRY_ID, 'Solomon Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SO' COUNTRY_ID, 'Somali' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ZA' COUNTRY_ID, 'Güney Afrika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GS' COUNTRY_ID, 'South Georgia ve South Sandwich Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SS' COUNTRY_ID, 'Güney Sudan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ES' COUNTRY_ID, 'İspanya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'LK' COUNTRY_ID, 'Sri Lanka' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SD' COUNTRY_ID, 'Sudan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SR' COUNTRY_ID, 'Surinam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SJ' COUNTRY_ID, 'Svalbard ve Jan Mayen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SZ' COUNTRY_ID, 'Svaziland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SE' COUNTRY_ID, 'İsveç' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'CH' COUNTRY_ID, 'İsviçre' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'SY' COUNTRY_ID, 'Suriye Arap Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TW' COUNTRY_ID, 'Tayvan (Çin İdari Bölgesi)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TJ' COUNTRY_ID, 'Tacikistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TZ' COUNTRY_ID, 'Tanzanya Birleşik Cumhuriyeti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TH' COUNTRY_ID, 'Tayland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TL' COUNTRY_ID, 'Timor-Leste' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TG' COUNTRY_ID, 'Togo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TK' COUNTRY_ID, 'Tokelau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TO' COUNTRY_ID, 'Tonga' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TT' COUNTRY_ID, 'Trinidad ve Tobago' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TN' COUNTRY_ID, 'Tunus' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TV' COUNTRY_ID, 'Tuvalu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'UG' COUNTRY_ID, 'Uganda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'UA' COUNTRY_ID, 'Ukrayna' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AE' COUNTRY_ID, 'Birleşik Arap Emirlikleri' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'GB' COUNTRY_ID, 'Birleşik Krallık' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'US' COUNTRY_ID, 'Amerika Birleşik Devletleri' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'UM' COUNTRY_ID, 'ABD Küçük Harici Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'UY' COUNTRY_ID, 'Uruguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'UZ' COUNTRY_ID, 'Özbekistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VU' COUNTRY_ID, 'Vanuatu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VA' COUNTRY_ID, 'Papalık [Vatikan Kent Devleti]' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VE' COUNTRY_ID, 'Venezuela' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VN' COUNTRY_ID, 'Vietnam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VG' COUNTRY_ID, 'İngiliz Virgin Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'VI' COUNTRY_ID, 'ABD Virgin Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'WF' COUNTRY_ID, 'Wallis ve Futuna' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'EH' COUNTRY_ID, 'Batı Sahra' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'YE' COUNTRY_ID, 'Yemen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ZM' COUNTRY_ID, 'Zambia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'ZW' COUNTRY_ID, 'Zimbabve' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'AX' COUNTRY_ID, 'Åland Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'NEW' COUNTRY_ID, 'Yeni Ülke' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, '99' COUNTRY_ID, 'Birden Çok' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TR' COUNTRY_ID, 'Türkiye' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TM' COUNTRY_ID, 'Türkmenistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 9 LANG, 'TC' COUNTRY_ID, 'Turks ve Caicos Adaları' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COUNTRY base USING
( SELECT COUNTRY_ID COUNTRY_ID, COUNTRY_DESC COUNTRY_DESC FROM COUNTRY_TL TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9)) USE_THIS
ON ( base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.COUNTRY_DESC = use_this.COUNTRY_DESC;
--
DELETE FROM COUNTRY_TL where lang = 9
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 9);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
