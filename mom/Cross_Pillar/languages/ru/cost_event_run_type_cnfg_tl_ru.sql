SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'CC' EVENT_TYPE, 'Изменение стоимости' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'CL' EVENT_TYPE, 'Изм. в об.форм.затр. франч.маг.' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'CZ' EVENT_TYPE, 'Перемещ. объекта зон себ-ти' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'D' EVENT_TYPE, 'Соглашение' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'DP' EVENT_TYPE, 'Прохождение соглашения' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'ELC' EVENT_TYPE, 'Компонент расчетной стоимости импорта' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'ICZ' EVENT_TYPE, 'Изменения зон себ-ти тов.' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'MH' EVENT_TYPE, 'Товарная иерархия' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'NIL' EVENT_TYPE, 'Объект нового товара' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'OH' EVENT_TYPE, 'Иерархия организации' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'PP' EVENT_TYPE, 'Себ-ть основн. набора' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'R' EVENT_TYPE, 'Изменение классификации' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'RTC' EVENT_TYPE, 'Измен. розн.ц.' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'SC' EVENT_TYPE, 'Изменения отношений с объектом страны поставщ. товара' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'SH' EVENT_TYPE, 'Иерархия поставщика' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'T' EVENT_TYPE, 'WF Шаблон себ-ти' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 7 LANG, 'TR' EVENT_TYPE, 'WF Отношение шаблона себ-ти' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG base USING
( SELECT EVENT_TYPE EVENT_TYPE, EVENT_DESC EVENT_DESC FROM COST_EVENT_RUN_TYPE_CNFG_TL TL where lang = 7
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 7)) USE_THIS
ON ( base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE SET base.EVENT_DESC = use_this.EVENT_DESC;
--
DELETE FROM COST_EVENT_RUN_TYPE_CNFG_TL where lang = 7
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 7);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
