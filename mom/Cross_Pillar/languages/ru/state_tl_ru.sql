SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'AK' STATE, 'US' COUNTRY_ID, 'Аляска' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'AL' STATE, 'US' COUNTRY_ID, 'Алабама' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'AR' STATE, 'US' COUNTRY_ID, 'Арканзас' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'AS' STATE, 'US' COUNTRY_ID, 'Американское Самоа' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'AZ' STATE, 'US' COUNTRY_ID, 'Аризона' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'CA' STATE, 'US' COUNTRY_ID, 'Калифорния' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'CO' STATE, 'US' COUNTRY_ID, 'Колорадо' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'CT' STATE, 'US' COUNTRY_ID, 'Коннектикут' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'DC' STATE, 'US' COUNTRY_ID, 'Округ Колумбия' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'DE' STATE, 'US' COUNTRY_ID, 'Делавэр' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'FL' STATE, 'US' COUNTRY_ID, 'Флорида' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'GA' STATE, 'US' COUNTRY_ID, 'Грузия' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'GU' STATE, 'US' COUNTRY_ID, 'Гуам' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'HI' STATE, 'US' COUNTRY_ID, 'Гавайи' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'IA' STATE, 'US' COUNTRY_ID, 'Айова' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'ID' STATE, 'US' COUNTRY_ID, 'Айдахо' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'IL' STATE, 'US' COUNTRY_ID, 'Иллинойс' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'IN' STATE, 'US' COUNTRY_ID, 'Индиана' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'KS' STATE, 'US' COUNTRY_ID, 'Канзас' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'KY' STATE, 'US' COUNTRY_ID, 'Кентукки' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'LA' STATE, 'US' COUNTRY_ID, 'Луизиана' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MA' STATE, 'US' COUNTRY_ID, 'Массачусетс' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MD' STATE, 'US' COUNTRY_ID, 'Мэриленд' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'ME' STATE, 'US' COUNTRY_ID, 'Мэн' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MI' STATE, 'US' COUNTRY_ID, 'Мичиган' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MN' STATE, 'US' COUNTRY_ID, 'Миннесота' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MO' STATE, 'US' COUNTRY_ID, 'Миссури' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MP' STATE, 'US' COUNTRY_ID, 'Северные Марианские острова' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MS' STATE, 'US' COUNTRY_ID, 'Миссисипи' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'MT' STATE, 'US' COUNTRY_ID, 'Монтана' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NC' STATE, 'US' COUNTRY_ID, 'Северная Каролина' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'ND' STATE, 'US' COUNTRY_ID, 'Северная Дакота' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NE' STATE, 'US' COUNTRY_ID, 'Небраска' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NH' STATE, 'US' COUNTRY_ID, 'Нью-Гэмпшир' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NJ' STATE, 'US' COUNTRY_ID, 'Нью-Джерси' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NM' STATE, 'US' COUNTRY_ID, 'Нью-Мексико' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NV' STATE, 'US' COUNTRY_ID, 'Невада' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'NY' STATE, 'US' COUNTRY_ID, 'Нью-Йорк' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'OH' STATE, 'US' COUNTRY_ID, 'Огайо' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'OK' STATE, 'US' COUNTRY_ID, 'Оклахома' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'ON' STATE, 'CA' COUNTRY_ID, 'Онтарио' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'OR' STATE, 'US' COUNTRY_ID, 'Орегон' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'PA' STATE, 'US' COUNTRY_ID, 'Пенсильвания' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'PR' STATE, 'US' COUNTRY_ID, 'Пуэрто-Рико' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'RI' STATE, 'US' COUNTRY_ID, 'Род-Айленд' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'SC' STATE, 'US' COUNTRY_ID, 'Южная Каролина' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'SD' STATE, 'US' COUNTRY_ID, 'Южная Дакота' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'TN' STATE, 'US' COUNTRY_ID, 'Теннесси' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'TX' STATE, 'US' COUNTRY_ID, 'Техас' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'UM' STATE, 'US' COUNTRY_ID, 'Удаленные острова США' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'UT' STATE, 'US' COUNTRY_ID, 'Юта' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'VA' STATE, 'US' COUNTRY_ID, 'Виргиния' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'VI' STATE, 'US' COUNTRY_ID, 'Виргинские о-ва, США' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'VT' STATE, 'US' COUNTRY_ID, 'Вермонт' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'WA' STATE, 'US' COUNTRY_ID, 'Вашингтон' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'WI' STATE, 'US' COUNTRY_ID, 'Висконсин' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 7 LANG, 'WV' STATE, 'US' COUNTRY_ID, 'Западная Виргиния' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO STATE base USING
( SELECT STATE STATE, COUNTRY_ID COUNTRY_ID, DESCRIPTION DESCRIPTION FROM STATE_TL TL where lang = 7
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 7)) USE_THIS
ON ( base.STATE = use_this.STATE and base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM STATE_TL where lang = 7
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 7);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
