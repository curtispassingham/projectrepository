SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 1 CODE, 'Vendite nette' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 2 CODE, 'Vendite nette IVA esclusa' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 3 CODE, 'Vendite/resi articoli non magazzino' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 4 CODE, 'Resi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 5 CODE, 'Vendite non magazzino IVA esclusa' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 6 CODE, 'Ricavi offerte (vendite)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 7 CODE, 'Ricavi offerte (acquisti)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 8 CODE, 'Ricavi fissi maturati' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 10 CODE, 'Varianza peso' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 11 CODE, 'Ricarico' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 12 CODE, 'Annullamento ricarico' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 13 CODE, 'Ribasso permanente' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 14 CODE, 'Annullamento ribasso' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 15 CODE, 'Ribasso promozionale' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 16 CODE, 'Ribasso svendita' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 17 CODE, 'Ricarico interaziendale' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 18 CODE, 'Ribasso interaziendale' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 20 CODE, 'Acquisti' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 21 CODE, 'Fattura - Utilizzato solo per interf. Oracle GL' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 22 CODE, 'Rettifica scorte' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 23 CODE, 'Rettifica scorte - COGS' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 24 CODE, 'Reso a vendor' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 25 CODE, 'Trasferimento magazzino non disponibile' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 26 CODE, 'Trasporto' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 27 CODE, 'CQ RTV' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 28 CODE, 'Costo aggiuntivo profitti - Ubicazione ricevimento' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 29 CODE, 'Costo aggiuntivo spese - Ubicazione ricevimento' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 30 CODE, 'Trasferimenti in entrata' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 31 CODE, 'Trasferimenti diretti in entrata' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 32 CODE, 'Trasferimenti in uscita' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 33 CODE, 'Trasferimenti diretti in uscita' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 34 CODE, 'Riclassificazioni in entrata' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 36 CODE, 'Riclassificazioni in uscita' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 37 CODE, 'Interaziendale in entrata' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 38 CODE, 'Interaziendale in uscita' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 39 CODE, 'Margine interaziendale' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 41 CODE, 'Rettifica stock ledger data warehouse' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 44 CODE, 'Ricezione trasferimento in entrata data warehouse' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 50 CODE, 'Scorte apertura' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 51 CODE, 'Perdite di scorte' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 52 CODE, 'Scorte chiusura' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 53 CODE, 'Margine lordo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 54 CODE, 'MDVP HTD' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 55 CODE, 'Imp. vendite inventario inter.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 56 CODE, 'Imp. perdite inventario inter.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 57 CODE, 'Importo vendite MTD inventario' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 58 CODE, 'Importo perd. MTD inventario' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 59 CODE, 'Costo SCRTSF inventario' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 60 CODE, 'Sconto dipendente' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 61 CODE, 'Costo/retail SCRATT inventario' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 62 CODE, 'Richiesta trasporto' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 63 CODE, 'Attività ord. lav. - Aggiorna magazzino' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 64 CODE, 'Attività ord. lav.-Contab. in Financials' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 65 CODE, 'Spesa reintegrazione' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 70 CODE, 'Varianza costo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 71 CODE, 'Varianza costo - Contabilità retail' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 72 CODE, 'Varianza costo - Contabilità costi' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 73 CODE, 'Varianza costo - Rett. costo ric. FIFO' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 74 CODE, 'Imposta recuperabile per ubicazione di destinazione' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 75 CODE, 'Imposta recuperabile per ubicazione di origine' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 80 CODE, 'Serv. val. agg./altro costo vendite' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 81 CODE, 'Sconto cassa' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 82 CODE, 'Vendite franchising' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 83 CODE, 'Resi franchising' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 84 CODE, 'Ricarichi franchising' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 85 CODE, 'Ribassi franchising' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 86 CODE, 'Spesa reintegrazione franchising' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 87 CODE, 'Costo IVA in entr.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 22 LANG, 88 CODE, 'Retail IVA in usc.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES base USING
( SELECT CODE CODE, DECODE DECODE FROM TRAN_DATA_CODES_TL TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22)) USE_THIS
ON ( base.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE SET base.DECODE = use_this.DECODE;
--
DELETE FROM TRAN_DATA_CODES_TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
