SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'HMFUS' COMP_ID, 'Diritti portuali USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'ADUS' COMP_ID, 'Anti-dumping USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'CVDUS' COMP_ID, 'Compensativo USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TDTYUS' COMP_ID, 'Dazio totale USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'INSINT' COMP_ID, 'Assicurazione internazionale' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, '200' COMP_ID, 'Trasporto aereo' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, '300' COMP_ID, 'Trasporto via terra' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, '60' COMP_ID, 'Logistica di terzi' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, '55' COMP_ID, 'Spese deconsolidamento' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'ICING' COMP_ID, 'Spese surgelamento' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'ORDCST' COMP_ID, 'Costo ordine' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TEXP' COMP_ID, 'Spese totali' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TEXPC' COMP_ID, 'Spese totali provincia' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TEXPZ' COMP_ID, 'Spese totali zona' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'UNCST' COMP_ID, 'Costo unitario' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BAPAL' COMP_ID, 'Riduzione per ritiro merce per pallet' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BACWT' COMP_ID, 'Riduzione per ritiro merce per cento pesi' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BAEA' COMP_ID, 'Riduzione per ritiro merce per unità' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BACS' COMP_ID, 'Riduzione per ritiro merce per scatola' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BACFT' COMP_ID, 'Riduzione per ritiro merce per piede cubo' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TSFFRGHT' COMP_ID, 'Trasporto trasferimento' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TSFINSUR' COMP_ID, 'Assicurazione trasferimento' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'WHFEE' COMP_ID, 'Spese per immagazzinaggio' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'WHPROC' COMP_ID, 'Spese per elaborazione magazzino' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'AGCOMM' COMP_ID, 'Commissioni agente' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'ILFRT' COMP_ID, 'Trasporto nazionale' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'BUYCOMM' COMP_ID, 'Commissioni buyer' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'OCFRT' COMP_ID, 'Trasporto per traversata oceanica' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'INSUR' COMP_ID, 'Assicurazione' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'SELLCOMM' COMP_ID, 'Commissioni venditore' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'ROYALTY' COMP_ID, 'Diritti' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'VFD25US' COMP_ID, '25% del valore per dazio USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'VFD50US' COMP_ID, '50% del valore per dazio USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'VFD75US' COMP_ID, '75% del valore per dazio USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'MPFUS' COMP_ID, 'Spesa elaborazione merce USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 22 LANG, 'TDTYPE' COMP_ID, 'Dazio totale USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ELC_COMP base USING
( SELECT COMP_ID COMP_ID, COMP_DESC COMP_DESC FROM ELC_COMP_TL TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22)) USE_THIS
ON ( base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE SET base.COMP_DESC = use_this.COMP_DESC;
--
DELETE FROM ELC_COMP_TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
