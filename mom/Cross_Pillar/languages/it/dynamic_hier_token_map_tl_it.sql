SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COM@' TOKEN, 22 LANG, 'Paese di produzione' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COMN@' TOKEN, 22 LANG, 'Paesi di produzione' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH2@' TOKEN, 22 LANG, 'Divisione' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH3@' TOKEN, 22 LANG, 'Gruppo' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH4@' TOKEN, 22 LANG, 'Reparto' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH5@' TOKEN, 22 LANG, 'Classe' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH6@' TOKEN, 22 LANG, 'Sottoclasse' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP2@' TOKEN, 22 LANG, 'Divisioni' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP3@' TOKEN, 22 LANG, 'Gruppi' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP4@' TOKEN, 22 LANG, 'Reparti' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP5@' TOKEN, 22 LANG, 'Classi' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP6@' TOKEN, 22 LANG, 'Sottoclassi' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH1@' TOKEN, 22 LANG, 'Società' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH2@' TOKEN, 22 LANG, 'Catena' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH3@' TOKEN, 22 LANG, 'Area' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH4@' TOKEN, 22 LANG, 'Regione' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH5@' TOKEN, 22 LANG, 'Distretto' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP1@' TOKEN, 22 LANG, 'Società' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP2@' TOKEN, 22 LANG, 'Catene' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP3@' TOKEN, 22 LANG, 'Aree' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP4@' TOKEN, 22 LANG, 'Regioni' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP5@' TOKEN, 22 LANG, 'Distretti' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH1@' TOKEN, 22 LANG, 'Produttore' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH2@' TOKEN, 22 LANG, 'Distributore' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH3@' TOKEN, 22 LANG, 'Grossista' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH4@' TOKEN, 22 LANG, 'Franchising' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP1@' TOKEN, 22 LANG, 'Produttori' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP2@' TOKEN, 22 LANG, 'Distributori' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP3@' TOKEN, 22 LANG, 'Grossisti' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP4@' TOKEN, 22 LANG, 'Affiliato' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO DYNAMIC_HIER_TOKEN_MAP base USING
( SELECT TOKEN TOKEN, RMS_NAME RMS_NAME, CLIENT_NAME CLIENT_NAME FROM DYNAMIC_HIER_TOKEN_MAP_TL TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22)) USE_THIS
ON ( base.TOKEN = use_this.TOKEN)
WHEN MATCHED THEN UPDATE SET base.RMS_NAME = use_this.RMS_NAME, base.CLIENT_NAME = use_this.CLIENT_NAME;
--
DELETE FROM DYNAMIC_HIER_TOKEN_MAP_TL where lang = 22
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 22);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
