SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COM@' TOKEN, 12 LANG, 'País de Fabricação' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COMN@' TOKEN, 12 LANG, 'Países de Fabricação' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH2@' TOKEN, 12 LANG, 'Divisão' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH3@' TOKEN, 12 LANG, 'Grupo' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH4@' TOKEN, 12 LANG, 'Departamento' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH5@' TOKEN, 12 LANG, 'Classe' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH6@' TOKEN, 12 LANG, 'Subclasse' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP2@' TOKEN, 12 LANG, 'Divisões' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP3@' TOKEN, 12 LANG, 'Grupos' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP4@' TOKEN, 12 LANG, 'Departamentos' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP5@' TOKEN, 12 LANG, 'Classes' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP6@' TOKEN, 12 LANG, 'Subclasses' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH1@' TOKEN, 12 LANG, 'Empresa' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH2@' TOKEN, 12 LANG, 'Cadeia' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH3@' TOKEN, 12 LANG, 'Área' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH4@' TOKEN, 12 LANG, 'Região' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH5@' TOKEN, 12 LANG, 'Distrito' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP1@' TOKEN, 12 LANG, 'Empresas' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP2@' TOKEN, 12 LANG, 'Cadeias' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP3@' TOKEN, 12 LANG, 'Áreas' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP4@' TOKEN, 12 LANG, 'Regiões' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP5@' TOKEN, 12 LANG, 'Distritos' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH1@' TOKEN, 12 LANG, 'Fabricante' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH2@' TOKEN, 12 LANG, 'Distribuidor' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH3@' TOKEN, 12 LANG, 'Atacadista' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH4@' TOKEN, 12 LANG, 'Franquia' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP1@' TOKEN, 12 LANG, 'Fabricantes' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP2@' TOKEN, 12 LANG, 'Distribuidores' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP3@' TOKEN, 12 LANG, 'Atacadistas' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP4@' TOKEN, 12 LANG, 'Franqueado' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO DYNAMIC_HIER_TOKEN_MAP base USING
( SELECT TOKEN TOKEN, RMS_NAME RMS_NAME, CLIENT_NAME CLIENT_NAME FROM DYNAMIC_HIER_TOKEN_MAP_TL TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12)) USE_THIS
ON ( base.TOKEN = use_this.TOKEN)
WHEN MATCHED THEN UPDATE SET base.RMS_NAME = use_this.RMS_NAME, base.CLIENT_NAME = use_this.CLIENT_NAME;
--
DELETE FROM DYNAMIC_HIER_TOKEN_MAP_TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
