SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'CC' EVENT_TYPE, 'Alteração de Custo' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'CL' EVENT_TYPE, 'Alteração no Local de Custo da Loja de franquia' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'CZ' EVENT_TYPE, 'Mudança de Local de Zona de Custo' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'D' EVENT_TYPE, 'Acordo' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'DP' EVENT_TYPE, 'Transferência de Acordo' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'ELC' EVENT_TYPE, 'Componente de Custo Estimado de Desembarque' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'ICZ' EVENT_TYPE, 'Alterações na Zona de Custo de Item' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'MH' EVENT_TYPE, 'Hierarquia da Mercadoria' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'NIL' EVENT_TYPE, 'Novo Local do Item' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'OH' EVENT_TYPE, 'Hierarquia da Organização' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'PP' EVENT_TYPE, 'Custo Principal Pack' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'R' EVENT_TYPE, 'Reclassificação' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'RTC' EVENT_TYPE, 'Alteração de Varejo' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'SC' EVENT_TYPE, 'Alterações no Relacionamento de Local do País Fornecedor do Item' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'SH' EVENT_TYPE, 'Hierarquia do Fornecedor' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'T' EVENT_TYPE, 'Modelo de Custo WF' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 12 LANG, 'TR' EVENT_TYPE, 'Relacionamento de Modelo de Custo WF' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG base USING
( SELECT EVENT_TYPE EVENT_TYPE, EVENT_DESC EVENT_DESC FROM COST_EVENT_RUN_TYPE_CNFG_TL TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12)) USE_THIS
ON ( base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE SET base.EVENT_DESC = use_this.EVENT_DESC;
--
DELETE FROM COST_EVENT_RUN_TYPE_CNFG_TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
