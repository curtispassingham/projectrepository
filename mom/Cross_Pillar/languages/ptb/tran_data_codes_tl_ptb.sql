SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 1 CODE, 'Vendas Líquidas' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 2 CODE, 'Vendas Líquidas IVA - exc. IVA' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 3 CODE, 'Vendas/Devoluções Itens Fora do Inventário' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 4 CODE, 'Devoluções' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 5 CODE, 'Vendas Exc. IVA Fora do Inventário' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 6 CODE, 'Receita do acordo (Vendas)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 7 CODE, 'Receita dos acordos (Compras)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 8 CODE, 'Acúmulo de Rendas Fixas' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 10 CODE, 'Variante Peso' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 11 CODE, 'Acréscimo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 12 CODE, 'Cancelamento de Acréscimo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 13 CODE, 'Desconto Permanente' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 14 CODE, 'Cancelamento de Desconto' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 15 CODE, 'Desconto Promocional' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 16 CODE, 'Desconto de Liquidação' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 17 CODE, 'Acréscimo Intercompanhia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 18 CODE, 'Desconto Intercompanhia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 20 CODE, 'Compras' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 21 CODE, 'Fatura - usado apenas na interface Oracle GL' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 22 CODE, 'Ajuste de Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 23 CODE, 'Ajuste de Estoque (COGS)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 24 CODE, 'Retorno para Fornecedor' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 25 CODE, 'Transferência de Inventário Não Disponível' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 26 CODE, 'Frete' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 27 CODE, 'QC RTV' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 28 CODE, 'Sobretaxa de Lucros - Local de Recebimento' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 29 CODE, 'Sobretaxa de Despesas - Local de Recebimento' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 30 CODE, 'Transferências de Entrada' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 31 CODE, 'Transferências Contábeis Internas' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 32 CODE, 'Transferência de Saída' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 33 CODE, 'Transferências Contábeis Externas' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 34 CODE, 'Entrada de Reclassificações' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 36 CODE, 'Saída de Reclassificações' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 37 CODE, 'Entrada Intercompanhia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 38 CODE, 'Saída Intercompanhia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 39 CODE, 'Margem Intercompanhia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 41 CODE, 'Ajuste de Razão de Estoque do Depósito de Dados.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 44 CODE, 'Recebimento de Transferência de Entrada do Depósito de Dados' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 50 CODE, 'Abrir Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 51 CODE, 'Diminuição' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 52 CODE, 'Fechar Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 53 CODE, 'Margem Bruta' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 54 CODE, 'HTD GAFS' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 55 CODE, 'Valor Vendas Contagens Periódicas Diversas de Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 56 CODE, 'Valor Diminuição Contagens Periódicas Diversas de Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 57 CODE, 'Valor Vendas Mtd Cont. Periódica Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 58 CODE, 'Valor Diminuição Mtd Cont. Periódica Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 59 CODE, 'Custo Bookstock Cont. Periódica Estq' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 60 CODE, 'Desconto de Funcionário' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 61 CODE, 'Custo/Preço de Venda Acstk Cont. Periódica Estoque' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 62 CODE, 'Reclamação de Frete' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 63 CODE, 'Atividade OS - Atualizar Inventário' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 64 CODE, 'Atividade da OS - Contabilizado no Financials' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 65 CODE, 'Taxa de Reabastecimento' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 70 CODE, 'Variação de Custo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 71 CODE, 'Variação de Custo - Contabilidade de Preços de Venda' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 72 CODE, 'Variação de Custo - Contabilidade de Custos' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 73 CODE, 'Variação de Custo - Ajuste Custo Rec.  FiFO' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 74 CODE, 'Imposto Recuperável no Local Destino' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 75 CODE, 'Imposto Recuperável no Local Origem' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 80 CODE, 'Sala de Serviço/Outro Custo de Vendas' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 81 CODE, 'Desconto em Dinheiro' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 82 CODE, 'Vendas de Franquia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 83 CODE, 'Devoluções de Franquia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 84 CODE, 'Acréscimos de Franquia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 85 CODE, 'Descontos de Franquia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 86 CODE, 'Taxa de Reabastecimento de Franquia' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 87 CODE, 'Custo do IVA Interno' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 12 LANG, 88 CODE, 'Varejo do IVA Externo' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES base USING
( SELECT CODE CODE, DECODE DECODE FROM TRAN_DATA_CODES_TL TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12)) USE_THIS
ON ( base.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE SET base.DECODE = use_this.DECODE;
--
DELETE FROM TRAN_DATA_CODES_TL where lang = 12
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 12);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
