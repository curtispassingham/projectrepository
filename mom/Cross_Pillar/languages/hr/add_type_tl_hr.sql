SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '01' ADDRESS_TYPE, 15 LANG, 'Tvrtka' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '02' ADDRESS_TYPE, 15 LANG, 'Pošta' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '03' ADDRESS_TYPE, 15 LANG, 'Povrati' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '04' ADDRESS_TYPE, 15 LANG, 'Narudžba' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '05' ADDRESS_TYPE, 15 LANG, 'Račun' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '06' ADDRESS_TYPE, 15 LANG, 'Doznaka' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '07' ADDRESS_TYPE, 15 LANG, 'Franšiza' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
