SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DEAL_COMP_TYPE_TL TL USING
(SELECT  DEAL_COMP_TYPE,  LANG,  DEAL_COMP_TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 'OTHER' DEAL_COMP_TYPE, 15 LANG, 'Druga komponenta' DEAL_COMP_TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM DEAL_COMP_TYPE base where dl.DEAL_COMP_TYPE = base.DEAL_COMP_TYPE)) USE_THIS
ON ( tl.DEAL_COMP_TYPE = use_this.DEAL_COMP_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.DEAL_COMP_TYPE_DESC = use_this.DEAL_COMP_TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (DEAL_COMP_TYPE, LANG, DEAL_COMP_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.DEAL_COMP_TYPE, use_this.LANG, use_this.DEAL_COMP_TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DEAL_COMP_TYPE_TL TL USING
(SELECT  DEAL_COMP_TYPE,  LANG,  DEAL_COMP_TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 'VFP' DEAL_COMP_TYPE, 15 LANG, 'Promocija na račun dobavljača' DEAL_COMP_TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM DEAL_COMP_TYPE base where dl.DEAL_COMP_TYPE = base.DEAL_COMP_TYPE)) USE_THIS
ON ( tl.DEAL_COMP_TYPE = use_this.DEAL_COMP_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.DEAL_COMP_TYPE_DESC = use_this.DEAL_COMP_TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (DEAL_COMP_TYPE, LANG, DEAL_COMP_TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.DEAL_COMP_TYPE, use_this.LANG, use_this.DEAL_COMP_TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
