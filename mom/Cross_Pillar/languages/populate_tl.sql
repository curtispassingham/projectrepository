SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set define "^";
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ADDR_TL
merge into ADDR_TL tl using
(select data_integration_lang PRIMARY_LANG, ADDR_KEY, ADD_1, ADD_2, ADD_3, CITY, CONTACT_NAME, COUNTY from ADDR, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ADDR_KEY = use_this.ADDR_KEY)
WHEN MATCHED then update set tl.ADD_1 = use_this.ADD_1, tl.ADD_2 = use_this.ADD_2, tl.ADD_3 = use_this.ADD_3, tl.CITY = use_this.CITY, tl.CONTACT_NAME = use_this.CONTACT_NAME, tl.COUNTY =
use_this.COUNTY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ADDR_KEY, ADD_1, ADD_2, ADD_3, CITY, CONTACT_NAME, COUNTY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ADDR_KEY, use_this.ADD_1, use_this.ADD_2, use_this.ADD_3, use_this.CITY, use_this.CONTACT_NAME, use_this.COUNTY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table AREA_TL
merge into AREA_TL tl using
(select data_integration_lang PRIMARY_LANG, AREA, AREA_NAME from AREA, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.AREA = use_this.AREA)
WHEN MATCHED then update set tl.AREA_NAME = use_this.AREA_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, AREA, AREA_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.AREA, use_this.AREA_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table BANNER_TL
merge into BANNER_TL tl using
(select data_integration_lang PRIMARY_LANG, BANNER_ID, BANNER_NAME from BANNER, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.BANNER_ID = use_this.BANNER_ID)
WHEN MATCHED then update set tl.BANNER_NAME = use_this.BANNER_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, BANNER_ID, BANNER_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.BANNER_ID, use_this.BANNER_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table BRAND_TL
merge into BRAND_TL tl using
(select data_integration_lang PRIMARY_LANG, BRAND_NAME, BRAND_DESCRIPTION from BRAND, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.BRAND_NAME = use_this.BRAND_NAME)
WHEN MATCHED then update set tl.BRAND_DESCRIPTION = use_this.BRAND_DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, BRAND_NAME, BRAND_DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.BRAND_NAME, use_this.BRAND_DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CHAIN_TL
merge into CHAIN_TL tl using
(select data_integration_lang PRIMARY_LANG, CHAIN, CHAIN_NAME from CHAIN, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CHAIN = use_this.CHAIN)
WHEN MATCHED then update set tl.CHAIN_NAME = use_this.CHAIN_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CHAIN, CHAIN_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CHAIN, use_this.CHAIN_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CHANNELS_TL
merge into CHANNELS_TL tl using
(select data_integration_lang PRIMARY_LANG, CHANNEL_ID, CHANNEL_NAME from CHANNELS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CHANNEL_ID = use_this.CHANNEL_ID)
WHEN MATCHED then update set tl.CHANNEL_NAME = use_this.CHANNEL_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CHANNEL_ID, CHANNEL_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CHANNEL_ID, use_this.CHANNEL_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CLASS_TL
merge into CLASS_TL tl using
(select data_integration_lang PRIMARY_LANG, DEPT, CLASS, CLASS_NAME from CLASS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DEPT = use_this.DEPT and tl.CLASS = use_this.CLASS)
WHEN MATCHED then update set tl.CLASS_NAME = use_this.CLASS_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DEPT, CLASS, CLASS_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DEPT, use_this.CLASS, use_this.CLASS_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CODE_DETAIL_TL
merge into CODE_DETAIL_TL tl using
(select data_integration_lang PRIMARY_LANG, CODE_TYPE, CODE, CODE_DESC from CODE_DETAIL, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED then update set tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COMPANY_CLOSED_TL
merge into COMPANY_CLOSED_TL tl using
(select data_integration_lang PRIMARY_LANG, CLOSE_DATE, CLOSE_DESC from COMPANY_CLOSED, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CLOSE_DATE = use_this.CLOSE_DATE)
WHEN MATCHED then update set tl.CLOSE_DESC = use_this.CLOSE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CLOSE_DATE, CLOSE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CLOSE_DATE, use_this.CLOSE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COMPHEAD_TL
merge into COMPHEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, COMPANY, CO_NAME, CO_ADD1, CO_ADD2, CO_ADD3, CO_CITY, CO_NAME_SECONDARY from COMPHEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.COMPANY = use_this.COMPANY)
WHEN MATCHED then update set tl.CO_NAME = use_this.CO_NAME, tl.CO_ADD1 = use_this.CO_ADD1, tl.CO_ADD2 = use_this.CO_ADD2, tl.CO_ADD3 = use_this.CO_ADD3, tl.CO_CITY = use_this.CO_CITY,
tl.CO_NAME_SECONDARY = use_this.CO_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, COMPANY, CO_NAME, CO_ADD1, CO_ADD2, CO_ADD3, CO_CITY, CO_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.COMPANY, use_this.CO_NAME, use_this.CO_ADD1, use_this.CO_ADD2, use_this.CO_ADD3, use_this.CO_CITY, use_this.CO_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_EVENT_RUN_TYPE_CNFG_TL
merge into COST_EVENT_RUN_TYPE_CNFG_TL tl using
(select data_integration_lang PRIMARY_LANG, EVENT_TYPE, EVENT_DESC from COST_EVENT_RUN_TYPE_CONFIG, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED then update set tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_ZONE_TL
merge into COST_ZONE_TL tl using
(select data_integration_lang PRIMARY_LANG, ZONE_GROUP_ID, ZONE_ID, DESCRIPTION from COST_ZONE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ZONE_GROUP_ID = use_this.ZONE_GROUP_ID and tl.ZONE_ID = use_this.ZONE_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ZONE_GROUP_ID, ZONE_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ZONE_GROUP_ID, use_this.ZONE_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_ZONE_GROUP_TL
merge into COST_ZONE_GROUP_TL tl using
(select data_integration_lang PRIMARY_LANG, ZONE_GROUP_ID, DESCRIPTION from COST_ZONE_GROUP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ZONE_GROUP_ID = use_this.ZONE_GROUP_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ZONE_GROUP_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ZONE_GROUP_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COUNTRY_TL
merge into COUNTRY_TL tl using
(select data_integration_lang PRIMARY_LANG, COUNTRY_ID, COUNTRY_DESC from COUNTRY, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED then update set tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CURRENCIES_TL
merge into CURRENCIES_TL tl using
(select data_integration_lang PRIMARY_LANG, CURRENCY_CODE, CURRENCY_DESC from CURRENCIES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CURRENCY_CODE = use_this.CURRENCY_CODE)
WHEN MATCHED then update set tl.CURRENCY_DESC = use_this.CURRENCY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CURRENCY_CODE, CURRENCY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CURRENCY_CODE, use_this.CURRENCY_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CUSTOMER_SEGMENTS_TL
merge into CUSTOMER_SEGMENTS_TL tl using
(select data_integration_lang PRIMARY_LANG, CUSTOMER_SEGMENT_ID, CUSTOMER_SEGMENT_DESC from CUSTOMER_SEGMENTS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CUSTOMER_SEGMENT_ID = use_this.CUSTOMER_SEGMENT_ID)
WHEN MATCHED then update set tl.CUSTOMER_SEGMENT_DESC = use_this.CUSTOMER_SEGMENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CUSTOMER_SEGMENT_ID, CUSTOMER_SEGMENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CUSTOMER_SEGMENT_ID, use_this.CUSTOMER_SEGMENT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CUSTOMER_SEGMENT_TYPES_TL
merge into CUSTOMER_SEGMENT_TYPES_TL tl using
(select data_integration_lang PRIMARY_LANG, CUSTOMER_SEGMENT_TYPE, CUSTOMER_SEGMENT_TYPE_DESC from CUSTOMER_SEGMENT_TYPES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CUSTOMER_SEGMENT_TYPE = use_this.CUSTOMER_SEGMENT_TYPE)
WHEN MATCHED then update set tl.CUSTOMER_SEGMENT_TYPE_DESC = use_this.CUSTOMER_SEGMENT_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CUSTOMER_SEGMENT_TYPE, CUSTOMER_SEGMENT_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CUSTOMER_SEGMENT_TYPE, use_this.CUSTOMER_SEGMENT_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CVB_HEAD_TL
merge into CVB_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, CVB_CODE, CVB_DESC from CVB_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED then update set tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DEAL_ATTRIB_DEF_TL
merge into DEAL_ATTRIB_DEF_TL tl using
(select data_integration_lang PRIMARY_LANG, DEAL_ATTRIB_TYPE_ID, DEAL_ATTRIB_DESC from DEAL_ATTRIB_DEF, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DEAL_ATTRIB_TYPE_ID = use_this.DEAL_ATTRIB_TYPE_ID)
WHEN MATCHED then update set tl.DEAL_ATTRIB_DESC = use_this.DEAL_ATTRIB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DEAL_ATTRIB_TYPE_ID, DEAL_ATTRIB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DEAL_ATTRIB_TYPE_ID, use_this.DEAL_ATTRIB_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DELIVERY_SLOT_TL
merge into DELIVERY_SLOT_TL tl using
(select data_integration_lang PRIMARY_LANG, DELIVERY_SLOT_ID, DELIVERY_SLOT_DESC from DELIVERY_SLOT, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DELIVERY_SLOT_ID = use_this.DELIVERY_SLOT_ID)
WHEN MATCHED then update set tl.DELIVERY_SLOT_DESC = use_this.DELIVERY_SLOT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DELIVERY_SLOT_ID, DELIVERY_SLOT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DELIVERY_SLOT_ID, use_this.DELIVERY_SLOT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DEPS_TL
merge into DEPS_TL tl using
(select data_integration_lang PRIMARY_LANG, DEPT, DEPT_NAME from DEPS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DEPT = use_this.DEPT)
WHEN MATCHED then update set tl.DEPT_NAME = use_this.DEPT_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DEPT, DEPT_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DEPT, use_this.DEPT_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_GROUP_HEAD_TL
merge into DIFF_GROUP_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, DIFF_GROUP_ID, DIFF_GROUP_DESC from DIFF_GROUP_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIFF_GROUP_ID = use_this.DIFF_GROUP_ID)
WHEN MATCHED then update set tl.DIFF_GROUP_DESC = use_this.DIFF_GROUP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIFF_GROUP_ID, DIFF_GROUP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIFF_GROUP_ID, use_this.DIFF_GROUP_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_IDS_TL
merge into DIFF_IDS_TL tl using
(select data_integration_lang PRIMARY_LANG, DIFF_ID, DIFF_DESC from DIFF_IDS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIFF_ID = use_this.DIFF_ID)
WHEN MATCHED then update set tl.DIFF_DESC = use_this.DIFF_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIFF_ID, DIFF_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIFF_ID, use_this.DIFF_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_RANGE_HEAD_TL
merge into DIFF_RANGE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, DIFF_RANGE, DIFF_RANGE_DESC from DIFF_RANGE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIFF_RANGE = use_this.DIFF_RANGE)
WHEN MATCHED then update set tl.DIFF_RANGE_DESC = use_this.DIFF_RANGE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIFF_RANGE, DIFF_RANGE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIFF_RANGE, use_this.DIFF_RANGE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_RATIO_HEAD_TL
merge into DIFF_RATIO_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, DIFF_RATIO_ID, DESCRIPTION from DIFF_RATIO_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIFF_RATIO_ID = use_this.DIFF_RATIO_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIFF_RATIO_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIFF_RATIO_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_TYPE_TL
merge into DIFF_TYPE_TL tl using
(select data_integration_lang PRIMARY_LANG, DIFF_TYPE, DIFF_TYPE_DESC from DIFF_TYPE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIFF_TYPE = use_this.DIFF_TYPE)
WHEN MATCHED then update set tl.DIFF_TYPE_DESC = use_this.DIFF_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIFF_TYPE, DIFF_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIFF_TYPE, use_this.DIFF_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DISTRICT_TL
merge into DISTRICT_TL tl using
(select data_integration_lang PRIMARY_LANG, DISTRICT, DISTRICT_NAME from DISTRICT, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DISTRICT = use_this.DISTRICT)
WHEN MATCHED then update set tl.DISTRICT_NAME = use_this.DISTRICT_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DISTRICT, DISTRICT_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DISTRICT, use_this.DISTRICT_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIVISION_TL
merge into DIVISION_TL tl using
(select data_integration_lang PRIMARY_LANG, DIVISION, DIV_NAME from DIVISION, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DIVISION = use_this.DIVISION)
WHEN MATCHED then update set tl.DIV_NAME = use_this.DIV_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DIVISION, DIV_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DIVISION, use_this.DIV_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DOC_TL
merge into DOC_TL tl using
(select data_integration_lang PRIMARY_LANG, DOC_ID, DOC_DESC from DOC, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DOC_ID = use_this.DOC_ID)
WHEN MATCHED then update set tl.DOC_DESC = use_this.DOC_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DOC_ID, DOC_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DOC_ID, use_this.DOC_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DYNAMIC_HIER_TOKEN_MAP_TL
merge into DYNAMIC_HIER_TOKEN_MAP_TL tl using
(select data_integration_lang PRIMARY_LANG, TOKEN, RMS_NAME, CLIENT_NAME from DYNAMIC_HIER_TOKEN_MAP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TOKEN = use_this.TOKEN)
WHEN MATCHED then update set tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED then insert (LANG, TOKEN, RMS_NAME, CLIENT_NAME)
values (use_this.PRIMARY_LANG, use_this.TOKEN, use_this.RMS_NAME, use_this.CLIENT_NAME);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ELC_COMP_TL
merge into ELC_COMP_TL tl using
(select data_integration_lang PRIMARY_LANG, COMP_ID, COMP_DESC from ELC_COMP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED then update set tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ENTRY_STATUS_TL
merge into ENTRY_STATUS_TL tl using
(select data_integration_lang PRIMARY_LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC from ENTRY_STATUS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ENTRY_TYPE_TL
merge into ENTRY_TYPE_TL tl using
(select data_integration_lang PRIMARY_LANG, ENTRY_TYPE, IMPORT_COUNTRY_ID, ENTRY_TYPE_DESC from ENTRY_TYPE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ENTRY_TYPE = use_this.ENTRY_TYPE and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set tl.ENTRY_TYPE_DESC = use_this.ENTRY_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ENTRY_TYPE, IMPORT_COUNTRY_ID, ENTRY_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ENTRY_TYPE, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table GROUPS_TL
merge into GROUPS_TL tl using
(select data_integration_lang PRIMARY_LANG, GROUP_NO, GROUP_NAME from GROUPS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.GROUP_NO = use_this.GROUP_NO)
WHEN MATCHED then update set tl.GROUP_NAME = use_this.GROUP_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, GROUP_NO, GROUP_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.GROUP_NO, use_this.GROUP_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HALF_TL
merge into HALF_TL tl using
(select data_integration_lang PRIMARY_LANG, HALF_NO, HALF_NAME from HALF, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.HALF_NO = use_this.HALF_NO)
WHEN MATCHED then update set tl.HALF_NAME = use_this.HALF_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, HALF_NO, HALF_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.HALF_NO, use_this.HALF_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS_TL
merge into HTS_TL tl using
(select data_integration_lang PRIMARY_LANG, HTS, IMPORT_COUNTRY_ID, EFFECT_FROM, EFFECT_TO, HTS_DESC from HTS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.HTS = use_this.HTS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID and tl.EFFECT_FROM = use_this.EFFECT_FROM and tl.EFFECT_TO = use_this.EFFECT_TO)
WHEN MATCHED then update set tl.HTS_DESC = use_this.HTS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, HTS, IMPORT_COUNTRY_ID, EFFECT_FROM, EFFECT_TO, HTS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.HTS, use_this.IMPORT_COUNTRY_ID, use_this.EFFECT_FROM, use_this.EFFECT_TO, use_this.HTS_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS_CHAPTER_TL
merge into HTS_CHAPTER_TL tl using
(select data_integration_lang PRIMARY_LANG, CHAPTER, IMPORT_COUNTRY_ID, CHAPTER_DESC from HTS_CHAPTER, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CHAPTER = use_this.CHAPTER and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set tl.CHAPTER_DESC = use_this.CHAPTER_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CHAPTER, IMPORT_COUNTRY_ID, CHAPTER_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CHAPTER, use_this.IMPORT_COUNTRY_ID, use_this.CHAPTER_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS_CHAPTER_RESTRAINTS_TL
merge into HTS_CHAPTER_RESTRAINTS_TL tl using
(select data_integration_lang PRIMARY_LANG, CHAPTER, IMPORT_COUNTRY_ID, ORIGIN_COUNTRY_ID, RESTRAINT_TYPE, RESTRAINT_DESC from HTS_CHAPTER_RESTRAINTS, (select data_integration_lang from
SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CHAPTER = use_this.CHAPTER and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID and tl.ORIGIN_COUNTRY_ID = use_this.ORIGIN_COUNTRY_ID and tl.RESTRAINT_TYPE
= use_this.RESTRAINT_TYPE)
WHEN MATCHED then update set tl.RESTRAINT_DESC = use_this.RESTRAINT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CHAPTER, IMPORT_COUNTRY_ID, ORIGIN_COUNTRY_ID, RESTRAINT_TYPE, RESTRAINT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CHAPTER, use_this.IMPORT_COUNTRY_ID, use_this.ORIGIN_COUNTRY_ID, use_this.RESTRAINT_TYPE, use_this.RESTRAINT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_IMAGE_TL
merge into ITEM_IMAGE_TL tl using
(select data_integration_lang PRIMARY_LANG, ITEM, IMAGE_NAME, IMAGE_DESC from ITEM_IMAGE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ITEM = use_this.ITEM and tl.IMAGE_NAME = use_this.IMAGE_NAME)
WHEN MATCHED then update set tl.IMAGE_DESC = use_this.IMAGE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ITEM, IMAGE_NAME, IMAGE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ITEM, use_this.IMAGE_NAME, use_this.IMAGE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_MASTER_TL
merge into ITEM_MASTER_TL tl using
(select data_integration_lang PRIMARY_LANG, ITEM, ITEM_DESC, ITEM_DESC_SECONDARY, SHORT_DESC from ITEM_MASTER, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ITEM = use_this.ITEM)
WHEN MATCHED then update set tl.ITEM_DESC = use_this.ITEM_DESC, tl.ITEM_DESC_SECONDARY = use_this.ITEM_DESC_SECONDARY, tl.SHORT_DESC = use_this.SHORT_DESC, tl.LAST_UPDATE_DATETIME = sysdate,
tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ITEM, ITEM_DESC, ITEM_DESC_SECONDARY, SHORT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ITEM, use_this.ITEM_DESC, use_this.ITEM_DESC_SECONDARY, use_this.SHORT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_SUPPLIER_TL
merge into ITEM_SUPPLIER_TL tl using
(select data_integration_lang PRIMARY_LANG, ITEM, SUPPLIER, SUPP_LABEL, SUPP_DIFF_1, SUPP_DIFF_2, SUPP_DIFF_3, SUPP_DIFF_4 from ITEM_SUPPLIER, (select data_integration_lang from
SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ITEM = use_this.ITEM and tl.SUPPLIER = use_this.SUPPLIER)
WHEN MATCHED then update set tl.SUPP_LABEL = use_this.SUPP_LABEL, tl.SUPP_DIFF_1 = use_this.SUPP_DIFF_1, tl.SUPP_DIFF_2 = use_this.SUPP_DIFF_2, tl.SUPP_DIFF_3 = use_this.SUPP_DIFF_3, tl.SUPP_DIFF_4 =
use_this.SUPP_DIFF_4, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ITEM, SUPPLIER, SUPP_LABEL, SUPP_DIFF_1, SUPP_DIFF_2, SUPP_DIFF_3, SUPP_DIFF_4, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ITEM, use_this.SUPPLIER, use_this.SUPP_LABEL, use_this.SUPP_DIFF_1, use_this.SUPP_DIFF_2, use_this.SUPP_DIFF_3, use_this.SUPP_DIFF_4, sysdate, user, sysdate,
user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_XFORM_HEAD_TL
merge into ITEM_XFORM_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, ITEM_XFORM_HEAD_ID, ITEM_XFORM_DESC from ITEM_XFORM_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ITEM_XFORM_HEAD_ID = use_this.ITEM_XFORM_HEAD_ID)
WHEN MATCHED then update set tl.ITEM_XFORM_DESC = use_this.ITEM_XFORM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ITEM_XFORM_HEAD_ID, ITEM_XFORM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ITEM_XFORM_HEAD_ID, use_this.ITEM_XFORM_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LANG_TL
merge into LANG_TL tl using
(select data_integration_lang PRIMARY_LANG, LANG, DESCRIPTION from LANG, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.LANG_LANG = use_this.LANG)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOCATION_CLOSED_TL
merge into LOCATION_CLOSED_TL tl using
(select data_integration_lang PRIMARY_LANG, LOCATION, CLOSE_DATE, REASON from LOCATION_CLOSED, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.LOCATION = use_this.LOCATION and tl.CLOSE_DATE = use_this.CLOSE_DATE)
WHEN MATCHED then update set tl.REASON = use_this.REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, LOCATION, CLOSE_DATE, REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.LOCATION, use_this.CLOSE_DATE, use_this.REASON, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOC_LIST_HEAD_TL
merge into LOC_LIST_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, LOC_LIST, LOC_LIST_DESC from LOC_LIST_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.LOC_LIST = use_this.LOC_LIST)
WHEN MATCHED then update set tl.LOC_LIST_DESC = use_this.LOC_LIST_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, LOC_LIST, LOC_LIST_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.LOC_LIST, use_this.LOC_LIST_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOC_TRAITS_TL
merge into LOC_TRAITS_TL tl using
(select data_integration_lang PRIMARY_LANG, LOC_TRAIT, DESCRIPTION from LOC_TRAITS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.LOC_TRAIT = use_this.LOC_TRAIT)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, LOC_TRAIT, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.LOC_TRAIT, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table MC_REJECTION_REASONS_TL
merge into MC_REJECTION_REASONS_TL tl using
(select data_integration_lang PRIMARY_LANG, REASON_KEY, REJECTION_REASON from MC_REJECTION_REASONS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED then update set tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table OGA_TL
merge into OGA_TL tl using
(select data_integration_lang PRIMARY_LANG, OGA_CODE, OGA_DESC from OGA, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.OGA_CODE = use_this.OGA_CODE)
WHEN MATCHED then update set tl.OGA_DESC = use_this.OGA_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, OGA_CODE, OGA_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.OGA_CODE, use_this.OGA_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ORG_UNIT_TL
merge into ORG_UNIT_TL tl using
(select data_integration_lang PRIMARY_LANG, ORG_UNIT_ID, DESCRIPTION from ORG_UNIT, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ORG_UNIT_ID = use_this.ORG_UNIT_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ORG_UNIT_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ORG_UNIT_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table OUTLOC_TL
merge into OUTLOC_TL tl using
(select data_integration_lang PRIMARY_LANG, OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_ADD1, OUTLOC_ADD2, OUTLOC_CITY, CONTACT_NAME, OUTLOC_NAME_SECONDARY from OUTLOC, (select data_integration_lang
from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.OUTLOC_TYPE = use_this.OUTLOC_TYPE and tl.OUTLOC_ID = use_this.OUTLOC_ID)
WHEN MATCHED then update set tl.OUTLOC_DESC = use_this.OUTLOC_DESC, tl.OUTLOC_ADD1 = use_this.OUTLOC_ADD1, tl.OUTLOC_ADD2 = use_this.OUTLOC_ADD2, tl.OUTLOC_CITY = use_this.OUTLOC_CITY, tl.CONTACT_NAME
= use_this.CONTACT_NAME, tl.OUTLOC_NAME_SECONDARY = use_this.OUTLOC_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_ADD1, OUTLOC_ADD2, OUTLOC_CITY, CONTACT_NAME, OUTLOC_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME,
LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.OUTLOC_TYPE, use_this.OUTLOC_ID, use_this.OUTLOC_DESC, use_this.OUTLOC_ADD1, use_this.OUTLOC_ADD2, use_this.OUTLOC_CITY, use_this.CONTACT_NAME,
use_this.OUTLOC_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PACK_TMPL_HEAD_TL
merge into PACK_TMPL_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, PACK_TMPL_ID, PACK_TMPL_DESC from PACK_TMPL_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.PACK_TMPL_ID = use_this.PACK_TMPL_ID)
WHEN MATCHED then update set tl.PACK_TMPL_DESC = use_this.PACK_TMPL_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, PACK_TMPL_ID, PACK_TMPL_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.PACK_TMPL_ID, use_this.PACK_TMPL_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PARTNER_TL
merge into PARTNER_TL tl using
(select data_integration_lang PRIMARY_LANG, PARTNER_TYPE, PARTNER_ID, PARTNER_DESC, PARTNER_NAME_SECONDARY from PARTNER, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.PARTNER_TYPE = use_this.PARTNER_TYPE and tl.PARTNER_ID = use_this.PARTNER_ID)
WHEN MATCHED then update set tl.PARTNER_DESC = use_this.PARTNER_DESC, tl.PARTNER_NAME_SECONDARY = use_this.PARTNER_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, PARTNER_TYPE, PARTNER_ID, PARTNER_DESC, PARTNER_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.PARTNER_TYPE, use_this.PARTNER_ID, use_this.PARTNER_DESC, use_this.PARTNER_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PEND_MERCH_HIER_TL
merge into PEND_MERCH_HIER_TL tl using
(select data_integration_lang PRIMARY_LANG, HIER_TYPE, MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_GRANDPARENT_ID, MERCH_HIER_NAME from PEND_MERCH_HIER, (select data_integration_lang from
SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.HIER_TYPE = use_this.HIER_TYPE and tl.MERCH_HIER_ID = use_this.MERCH_HIER_ID and tl.MERCH_HIER_PARENT_ID = use_this.MERCH_HIER_PARENT_ID and
tl.MERCH_HIER_GRANDPARENT_ID = use_this.MERCH_HIER_GRANDPARENT_ID)
WHEN MATCHED then update set tl.MERCH_HIER_NAME = use_this.MERCH_HIER_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, HIER_TYPE, MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_GRANDPARENT_ID, MERCH_HIER_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.HIER_TYPE, use_this.MERCH_HIER_ID, use_this.MERCH_HIER_PARENT_ID, use_this.MERCH_HIER_GRANDPARENT_ID, use_this.MERCH_HIER_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PHASES_TL
merge into PHASES_TL tl using
(select data_integration_lang PRIMARY_LANG, SEASON_ID, PHASE_ID, PHASE_DESC from PHASES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SEASON_ID = use_this.SEASON_ID and tl.PHASE_ID = use_this.PHASE_ID)
WHEN MATCHED then update set tl.PHASE_DESC = use_this.PHASE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SEASON_ID, PHASE_ID, PHASE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SEASON_ID, use_this.PHASE_ID, use_this.PHASE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table POS_COUPON_HEAD_TL
merge into POS_COUPON_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, COUPON_ID, COUPON_DESC from POS_COUPON_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.COUPON_ID = use_this.COUPON_ID)
WHEN MATCHED then update set tl.COUPON_DESC = use_this.COUPON_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, COUPON_ID, COUPON_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.COUPON_ID, use_this.COUPON_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table POS_TENDER_TYPE_HEAD_TL
merge into POS_TENDER_TYPE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, TENDER_TYPE_ID, TENDER_TYPE_DESC from POS_TENDER_TYPE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TENDER_TYPE_ID = use_this.TENDER_TYPE_ID)
WHEN MATCHED then update set tl.TENDER_TYPE_DESC = use_this.TENDER_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TENDER_TYPE_ID, TENDER_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TENDER_TYPE_ID, use_this.TENDER_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PO_TYPE_TL
merge into PO_TYPE_TL tl using
(select data_integration_lang PRIMARY_LANG, PO_TYPE, PO_TYPE_DESC from PO_TYPE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.PO_TYPE = use_this.PO_TYPE)
WHEN MATCHED then update set tl.PO_TYPE_DESC = use_this.PO_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, PO_TYPE, PO_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.PO_TYPE, use_this.PO_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PRIORITY_GROUP_TL
merge into PRIORITY_GROUP_TL tl using
(select data_integration_lang PRIMARY_LANG, PRIORITY_GROUP_ID, PRIORITY_GROUP_DESC from PRIORITY_GROUP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.PRIORITY_GROUP_ID = use_this.PRIORITY_GROUP_ID)
WHEN MATCHED then update set tl.PRIORITY_GROUP_DESC = use_this.PRIORITY_GROUP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, PRIORITY_GROUP_ID, PRIORITY_GROUP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.PRIORITY_GROUP_ID, use_this.PRIORITY_GROUP_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table QUOTA_CATEGORY_TL
merge into QUOTA_CATEGORY_TL tl using
(select data_integration_lang PRIMARY_LANG, QUOTA_CAT, IMPORT_COUNTRY_ID, CATEGORY_DESC from QUOTA_CATEGORY, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.QUOTA_CAT = use_this.QUOTA_CAT and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set tl.CATEGORY_DESC = use_this.CATEGORY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, QUOTA_CAT, IMPORT_COUNTRY_ID, CATEGORY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.QUOTA_CAT, use_this.IMPORT_COUNTRY_ID, use_this.CATEGORY_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RECLASS_HEAD_TL
merge into RECLASS_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, RECLASS_NO, RECLASS_DESC from RECLASS_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.RECLASS_NO = use_this.RECLASS_NO)
WHEN MATCHED then update set tl.RECLASS_DESC = use_this.RECLASS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, RECLASS_NO, RECLASS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.RECLASS_NO, use_this.RECLASS_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table REGION_TL
merge into REGION_TL tl using
(select data_integration_lang PRIMARY_LANG, REGION, REGION_NAME from REGION, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.REGION = use_this.REGION)
WHEN MATCHED then update set tl.REGION_NAME = use_this.REGION_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, REGION, REGION_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.REGION, use_this.REGION_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RELATED_ITEM_HEAD_TL
merge into RELATED_ITEM_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, RELATIONSHIP_ID, RELATIONSHIP_NAME from RELATED_ITEM_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.RELATIONSHIP_ID = use_this.RELATIONSHIP_ID)
WHEN MATCHED then update set tl.RELATIONSHIP_NAME = use_this.RELATIONSHIP_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, RELATIONSHIP_ID, RELATIONSHIP_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.RELATIONSHIP_ID, use_this.RELATIONSHIP_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RESTART_CONTROL_TL
merge into RESTART_CONTROL_TL tl using
(select data_integration_lang PRIMARY_LANG, PROGRAM_NAME, PROGRAM_DESC from RESTART_CONTROL, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED then update set tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RTK_ERRORS_TL
merge into RTK_ERRORS_TL tl using
(select data_integration_lang PRIMARY_LANG, RTK_KEY, RTK_TEXT from RTK_ERRORS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.RTK_KEY = use_this.RTK_KEY)
WHEN MATCHED then update set tl.RTK_TEXT = use_this.RTK_TEXT, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, RTK_KEY, RTK_TEXT, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.RTK_KEY, use_this.RTK_TEXT, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_CONSTANTS_TL
merge into SA_CONSTANTS_TL tl using
(select data_integration_lang PRIMARY_LANG, CONSTANT_ID, CONSTANT_NAME from SA_CONSTANTS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CONSTANT_ID = use_this.CONSTANT_ID)
WHEN MATCHED then update set tl.CONSTANT_NAME = use_this.CONSTANT_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CONSTANT_ID, CONSTANT_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CONSTANT_ID, use_this.CONSTANT_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_ERROR_CODES_TL
merge into SA_ERROR_CODES_TL tl using
(select data_integration_lang PRIMARY_LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, SHORT_DESC from SA_ERROR_CODES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED then update set tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.SHORT_DESC = use_this.SHORT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID =
user
WHEN NOT MATCHED then insert (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, SHORT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, use_this.SHORT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_ROUNDING_RULE_HEAD_TL
merge into SA_ROUNDING_RULE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, ROUNDING_RULE_ID, ROUNDING_RULE_NAME from SA_ROUNDING_RULE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ROUNDING_RULE_ID = use_this.ROUNDING_RULE_ID)
WHEN MATCHED then update set tl.ROUNDING_RULE_NAME = use_this.ROUNDING_RULE_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ROUNDING_RULE_ID, ROUNDING_RULE_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ROUNDING_RULE_ID, use_this.ROUNDING_RULE_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_RULE_HEAD_TL
merge into SA_RULE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, RULE_ID, RULE_REV_NO, RULE_NAME from SA_RULE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.RULE_ID = use_this.RULE_ID and tl.RULE_REV_NO = use_this.RULE_REV_NO)
WHEN MATCHED then update set tl.RULE_NAME = use_this.RULE_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, RULE_ID, RULE_REV_NO, RULE_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.RULE_ID, use_this.RULE_REV_NO, use_this.RULE_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_TOTAL_HEAD_TL
merge into SA_TOTAL_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, TOTAL_ID, TOTAL_REV_NO, TOTAL_DESC from SA_TOTAL_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TOTAL_ID = use_this.TOTAL_ID and tl.TOTAL_REV_NO = use_this.TOTAL_REV_NO)
WHEN MATCHED then update set tl.TOTAL_DESC = use_this.TOTAL_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TOTAL_ID, TOTAL_REV_NO, TOTAL_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TOTAL_ID, use_this.TOTAL_REV_NO, use_this.TOTAL_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SCAC_TL
merge into SCAC_TL tl using
(select data_integration_lang PRIMARY_LANG, SCAC_CODE, SCAC_CODE_DESC from SCAC, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SCAC_CODE = use_this.SCAC_CODE)
WHEN MATCHED then update set tl.SCAC_CODE_DESC = use_this.SCAC_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SCAC_CODE, SCAC_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SCAC_CODE, use_this.SCAC_CODE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SEASONS_TL
merge into SEASONS_TL tl using
(select data_integration_lang PRIMARY_LANG, SEASON_ID, SEASON_DESC from SEASONS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SEASON_ID = use_this.SEASON_ID)
WHEN MATCHED then update set tl.SEASON_DESC = use_this.SEASON_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SEASON_ID, SEASON_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SEASON_ID, use_this.SEASON_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SEC_GROUP_TL
merge into SEC_GROUP_TL tl using
(select data_integration_lang PRIMARY_LANG, GROUP_ID, GROUP_NAME from SEC_GROUP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.GROUP_ID = use_this.GROUP_ID)
WHEN MATCHED then update set tl.GROUP_NAME = use_this.GROUP_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, GROUP_ID, GROUP_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.GROUP_ID, use_this.GROUP_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SKULIST_HEAD_TL
merge into SKULIST_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, SKULIST, SKULIST_DESC from SKULIST_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SKULIST = use_this.SKULIST)
WHEN MATCHED then update set tl.SKULIST_DESC = use_this.SKULIST_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SKULIST, SKULIST_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SKULIST, use_this.SKULIST_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STATE_TL
merge into STATE_TL tl using
(select data_integration_lang PRIMARY_LANG, STATE, COUNTRY_ID, DESCRIPTION from STATE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_TL
merge into STORE_TL tl using
(select data_integration_lang PRIMARY_LANG, STORE, STORE_NAME, STORE_NAME_SECONDARY from STORE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.STORE = use_this.STORE)
WHEN MATCHED then update set tl.STORE_NAME = use_this.STORE_NAME, tl.STORE_NAME_SECONDARY = use_this.STORE_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, STORE, STORE_NAME, STORE_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.STORE, use_this.STORE_NAME, use_this.STORE_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_ADD_TL
merge into STORE_ADD_TL tl using
(select data_integration_lang PRIMARY_LANG, STORE, STORE_NAME, STORE_NAME_SECONDARY from STORE_ADD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.STORE = use_this.STORE)
WHEN MATCHED then update set tl.STORE_NAME = use_this.STORE_NAME, tl.STORE_NAME_SECONDARY = use_this.STORE_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, STORE, STORE_NAME, STORE_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.STORE, use_this.STORE_NAME, use_this.STORE_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_FORMAT_TL
merge into STORE_FORMAT_TL tl using
(select data_integration_lang PRIMARY_LANG, STORE_FORMAT, FORMAT_NAME from STORE_FORMAT, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.STORE_FORMAT = use_this.STORE_FORMAT)
WHEN MATCHED then update set tl.FORMAT_NAME = use_this.FORMAT_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, STORE_FORMAT, FORMAT_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.STORE_FORMAT, use_this.FORMAT_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_GRADE_GROUP_TL
merge into STORE_GRADE_GROUP_TL tl using
(select data_integration_lang PRIMARY_LANG, STORE_GRADE_GROUP_ID, STORE_GRADE_GROUP_DESC from STORE_GRADE_GROUP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.STORE_GRADE_GROUP_ID = use_this.STORE_GRADE_GROUP_ID)
WHEN MATCHED then update set tl.STORE_GRADE_GROUP_DESC = use_this.STORE_GRADE_GROUP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, STORE_GRADE_GROUP_ID, STORE_GRADE_GROUP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.STORE_GRADE_GROUP_ID, use_this.STORE_GRADE_GROUP_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUBCLASS_TL
merge into SUBCLASS_TL tl using
(select data_integration_lang PRIMARY_LANG, DEPT, CLASS, SUBCLASS, SUB_NAME from SUBCLASS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.DEPT = use_this.DEPT and tl.CLASS = use_this.CLASS and tl.SUBCLASS = use_this.SUBCLASS)
WHEN MATCHED then update set tl.SUB_NAME = use_this.SUB_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, DEPT, CLASS, SUBCLASS, SUB_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.DEPT, use_this.CLASS, use_this.SUBCLASS, use_this.SUB_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUPS_TL
merge into SUPS_TL tl using
(select data_integration_lang PRIMARY_LANG, SUPPLIER, SUP_NAME, SUP_NAME_SECONDARY, CONTACT_NAME from SUPS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SUPPLIER = use_this.SUPPLIER)
WHEN MATCHED then update set tl.SUP_NAME = use_this.SUP_NAME, tl.SUP_NAME_SECONDARY = use_this.SUP_NAME_SECONDARY, tl.CONTACT_NAME = use_this.CONTACT_NAME, tl.LAST_UPDATE_DATETIME = sysdate,
tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SUPPLIER, SUP_NAME, SUP_NAME_SECONDARY, CONTACT_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SUPPLIER, use_this.SUP_NAME, use_this.SUP_NAME_SECONDARY, use_this.CONTACT_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUPS_PACK_TMPL_DESC_TL
merge into SUPS_PACK_TMPL_DESC_TL tl using
(select data_integration_lang PRIMARY_LANG, SUPPLIER, PACK_TMPL_ID, SUPP_PACK_DESC from SUPS_PACK_TMPL_DESC, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SUPPLIER = use_this.SUPPLIER and tl.PACK_TMPL_ID = use_this.PACK_TMPL_ID)
WHEN MATCHED then update set tl.SUPP_PACK_DESC = use_this.SUPP_PACK_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SUPPLIER, PACK_TMPL_ID, SUPP_PACK_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SUPPLIER, use_this.PACK_TMPL_ID, use_this.SUPP_PACK_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUP_TRAITS_TL
merge into SUP_TRAITS_TL tl using
(select data_integration_lang PRIMARY_LANG, SUP_TRAIT, DESCRIPTION from SUP_TRAITS, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.SUP_TRAIT = use_this.SUP_TRAIT)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, SUP_TRAIT, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.SUP_TRAIT, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TARIFF_TREATMENT_TL
merge into TARIFF_TREATMENT_TL tl using
(select data_integration_lang PRIMARY_LANG, TARIFF_TREATMENT, TARIFF_TREATMENT_DESC from TARIFF_TREATMENT, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TARIFF_TREATMENT = use_this.TARIFF_TREATMENT)
WHEN MATCHED then update set tl.TARIFF_TREATMENT_DESC = use_this.TARIFF_TREATMENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TARIFF_TREATMENT, TARIFF_TREATMENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TARIFF_TREATMENT, use_this.TARIFF_TREATMENT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TICKET_TYPE_HEAD_TL
merge into TICKET_TYPE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, TICKET_TYPE_ID, TICKET_TYPE_DESC from TICKET_TYPE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TICKET_TYPE_ID = use_this.TICKET_TYPE_ID)
WHEN MATCHED then update set tl.TICKET_TYPE_DESC = use_this.TICKET_TYPE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TICKET_TYPE_ID, TICKET_TYPE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TICKET_TYPE_ID, use_this.TICKET_TYPE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TIMELINE_HEAD_TL
merge into TIMELINE_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, TIMELINE_NO, TIMELINE_DESC from TIMELINE_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TIMELINE_NO = use_this.TIMELINE_NO)
WHEN MATCHED then update set tl.TIMELINE_DESC = use_this.TIMELINE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TIMELINE_NO, TIMELINE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TIMELINE_NO, use_this.TIMELINE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TIMELINE_STEP_COMP_TL
merge into TIMELINE_STEP_COMP_TL tl using
(select data_integration_lang PRIMARY_LANG, TIMELINE_TYPE, STEP_NO, STEP_DESC from TIMELINE_STEP_COMP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TIMELINE_TYPE = use_this.TIMELINE_TYPE and tl.STEP_NO = use_this.STEP_NO)
WHEN MATCHED then update set tl.STEP_DESC = use_this.STEP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TIMELINE_TYPE, STEP_NO, STEP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TIMELINE_TYPE, use_this.STEP_NO, use_this.STEP_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TRAN_DATA_CODES_TL
merge into TRAN_DATA_CODES_TL tl using
(select data_integration_lang PRIMARY_LANG, CODE, DECODE from TRAN_DATA_CODES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.CODE = use_this.CODE)
WHEN MATCHED then update set tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TRAN_DATA_CODES_REF_TL
merge into TRAN_DATA_CODES_REF_TL tl using
(select data_integration_lang PRIMARY_LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC from TRAN_DATA_CODES_REF, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS))
use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED then update set tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate,
tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TSFZONE_TL
merge into TSFZONE_TL tl using
(select data_integration_lang PRIMARY_LANG, TRANSFER_ZONE, DESCRIPTION from TSFZONE, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TRANSFER_ZONE = use_this.TRANSFER_ZONE)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TRANSFER_ZONE, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TRANSFER_ZONE, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TSF_ENTITY_TL
merge into TSF_ENTITY_TL tl using
(select data_integration_lang PRIMARY_LANG, TSF_ENTITY_ID, TSF_ENTITY_DESC from TSF_ENTITY, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TSF_ENTITY_ID = use_this.TSF_ENTITY_ID)
WHEN MATCHED then update set tl.TSF_ENTITY_DESC = use_this.TSF_ENTITY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TSF_ENTITY_ID, TSF_ENTITY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TSF_ENTITY_ID, use_this.TSF_ENTITY_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table UDA_TL
merge into UDA_TL tl using
(select data_integration_lang PRIMARY_LANG, UDA_ID, UDA_DESC from UDA, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.UDA_ID = use_this.UDA_ID)
WHEN MATCHED then update set tl.UDA_DESC = use_this.UDA_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, UDA_ID, UDA_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.UDA_ID, use_this.UDA_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table UDA_ITEM_FF_TL
merge into UDA_ITEM_FF_TL tl using
(select data_integration_lang PRIMARY_LANG, ITEM, UDA_ID, UDA_TEXT, UDA_TEXT_DESC from UDA_ITEM_FF, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ITEM = use_this.ITEM and tl.UDA_ID = use_this.UDA_ID and tl.UDA_TEXT = use_this.UDA_TEXT)
WHEN MATCHED then update set tl.UDA_TEXT_DESC = use_this.UDA_TEXT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ITEM, UDA_ID, UDA_TEXT, UDA_TEXT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ITEM, use_this.UDA_ID, use_this.UDA_TEXT, use_this.UDA_TEXT_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table UDA_VALUES_TL
merge into UDA_VALUES_TL tl using
(select data_integration_lang PRIMARY_LANG, UDA_ID, UDA_VALUE, UDA_VALUE_DESC from UDA_VALUES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.UDA_ID = use_this.UDA_ID and tl.UDA_VALUE = use_this.UDA_VALUE)
WHEN MATCHED then update set tl.UDA_VALUE_DESC = use_this.UDA_VALUE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, UDA_ID, UDA_VALUE, UDA_VALUE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.UDA_ID, use_this.UDA_VALUE, use_this.UDA_VALUE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table VAT_CODES_TL
merge into VAT_CODES_TL tl using
(select data_integration_lang PRIMARY_LANG, VAT_CODE, VAT_CODE_DESC from VAT_CODES, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED then update set tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table VAT_REGION_TL
merge into VAT_REGION_TL tl using
(select data_integration_lang PRIMARY_LANG, VAT_REGION, VAT_REGION_NAME from VAT_REGION, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.VAT_REGION = use_this.VAT_REGION)
WHEN MATCHED then update set tl.VAT_REGION_NAME = use_this.VAT_REGION_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, VAT_REGION, VAT_REGION_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.VAT_REGION, use_this.VAT_REGION_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_COST_BUILDUP_TMPL_DTL_TL
merge into WF_COST_BUILDUP_TMPL_DTL_TL tl using
(select data_integration_lang PRIMARY_LANG, TEMPL_ID, COST_COMP_ID, DESCRIPTION from WF_COST_BUILDUP_TMPL_DETAIL, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TEMPL_ID = use_this.TEMPL_ID and tl.COST_COMP_ID = use_this.COST_COMP_ID)
WHEN MATCHED then update set tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TEMPL_ID, COST_COMP_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TEMPL_ID, use_this.COST_COMP_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_COST_BUILDUP_TMPL_HD_TL
merge into WF_COST_BUILDUP_TMPL_HD_TL tl using
(select data_integration_lang PRIMARY_LANG, TEMPL_ID, TEMPL_DESC from WF_COST_BUILDUP_TMPL_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.TEMPL_ID = use_this.TEMPL_ID)
WHEN MATCHED then update set tl.TEMPL_DESC = use_this.TEMPL_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, TEMPL_ID, TEMPL_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.TEMPL_ID, use_this.TEMPL_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_CUSTOMER_TL
merge into WF_CUSTOMER_TL tl using
(select data_integration_lang PRIMARY_LANG, WF_CUSTOMER_ID, WF_CUSTOMER_NAME from WF_CUSTOMER, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.WF_CUSTOMER_ID = use_this.WF_CUSTOMER_ID)
WHEN MATCHED then update set tl.WF_CUSTOMER_NAME = use_this.WF_CUSTOMER_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, WF_CUSTOMER_ID, WF_CUSTOMER_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.WF_CUSTOMER_ID, use_this.WF_CUSTOMER_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_CUSTOMER_GROUP_TL
merge into WF_CUSTOMER_GROUP_TL tl using
(select data_integration_lang PRIMARY_LANG, WF_CUSTOMER_GROUP_ID, WF_CUSTOMER_GROUP_NAME from WF_CUSTOMER_GROUP, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.WF_CUSTOMER_GROUP_ID = use_this.WF_CUSTOMER_GROUP_ID)
WHEN MATCHED then update set tl.WF_CUSTOMER_GROUP_NAME = use_this.WF_CUSTOMER_GROUP_NAME, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, WF_CUSTOMER_GROUP_ID, WF_CUSTOMER_GROUP_NAME, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.WF_CUSTOMER_GROUP_ID, use_this.WF_CUSTOMER_GROUP_NAME, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WH_TL
merge into WH_TL tl using
(select data_integration_lang PRIMARY_LANG, WH, WH_NAME, WH_NAME_SECONDARY from WH, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.WH = use_this.WH)
WHEN MATCHED then update set tl.WH_NAME = use_this.WH_NAME, tl.WH_NAME_SECONDARY = use_this.WH_NAME_SECONDARY, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, WH, WH_NAME, WH_NAME_SECONDARY, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.WH, use_this.WH_NAME, use_this.WH_NAME_SECONDARY, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WO_ACTIVITY_TL
merge into WO_ACTIVITY_TL tl using
(select data_integration_lang PRIMARY_LANG, ACTIVITY_ID, ACTIVITY_DESC from WO_ACTIVITY, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.ACTIVITY_ID = use_this.ACTIVITY_ID)
WHEN MATCHED then update set tl.ACTIVITY_DESC = use_this.ACTIVITY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, ACTIVITY_ID, ACTIVITY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.ACTIVITY_ID, use_this.ACTIVITY_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WO_TMPL_HEAD_TL
merge into WO_TMPL_HEAD_TL tl using
(select data_integration_lang PRIMARY_LANG, WO_TMPL_ID, WO_TMPL_DESC from WO_TMPL_HEAD, (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (tl.LANG = use_this.PRIMARY_LANG and tl.WO_TMPL_ID = use_this.WO_TMPL_ID)
WHEN MATCHED then update set tl.WO_TMPL_DESC = use_this.WO_TMPL_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED then insert (LANG, WO_TMPL_ID, WO_TMPL_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
values (use_this.PRIMARY_LANG, use_this.WO_TMPL_ID, use_this.WO_TMPL_DESC, sysdate, user, sysdate, user);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
set define "&";
