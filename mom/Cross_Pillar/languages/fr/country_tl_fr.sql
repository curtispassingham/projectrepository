SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AF' COUNTRY_ID, 'Afghanistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AL' COUNTRY_ID, 'Albanie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DZ' COUNTRY_ID, 'Algérie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AS' COUNTRY_ID, 'Samoa américaines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AD' COUNTRY_ID, 'Andorre' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AO' COUNTRY_ID, 'Angola' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AI' COUNTRY_ID, 'Anguilla' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AQ' COUNTRY_ID, 'Antarctique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AG' COUNTRY_ID, 'Antigua-et-Barbuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AR' COUNTRY_ID, 'Argentine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AM' COUNTRY_ID, 'Arménie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AW' COUNTRY_ID, 'Aruba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AU' COUNTRY_ID, 'Australie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AT' COUNTRY_ID, 'Autriche' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AZ' COUNTRY_ID, 'Azerbaïdjan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BS' COUNTRY_ID, 'Bahamas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BH' COUNTRY_ID, 'Bahreïn' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BD' COUNTRY_ID, 'Bangladesh' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BB' COUNTRY_ID, 'Barbade' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BY' COUNTRY_ID, 'Biélorussie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BE' COUNTRY_ID, 'Belgique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BZ' COUNTRY_ID, 'Belize' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BJ' COUNTRY_ID, 'Bénin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BM' COUNTRY_ID, 'Bermudes' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BT' COUNTRY_ID, 'Bhoutan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BO' COUNTRY_ID, 'Bolivie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BQ' COUNTRY_ID, 'Bonaire, Saint-Eustache et Saba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BA' COUNTRY_ID, 'Bosnie-Herzégovine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BW' COUNTRY_ID, 'Botswana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BV' COUNTRY_ID, 'Ile Bouvet' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BR' COUNTRY_ID, 'Brésil' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IO' COUNTRY_ID, 'Territoire britannique de l''océan Indien' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BN' COUNTRY_ID, 'Brunei Darussalam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BG' COUNTRY_ID, 'Bulgarie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BF' COUNTRY_ID, 'Burkina Faso' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BI' COUNTRY_ID, 'Burundi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KH' COUNTRY_ID, 'Cambodge' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CM' COUNTRY_ID, 'Cameroun' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CA' COUNTRY_ID, 'Canada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CV' COUNTRY_ID, 'Cap-Vert' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KY' COUNTRY_ID, 'Iles Caïmans' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CF' COUNTRY_ID, 'République centrafricaine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TD' COUNTRY_ID, 'Tchad' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CL' COUNTRY_ID, 'Chili' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CN' COUNTRY_ID, 'Chine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CX' COUNTRY_ID, 'Ile Christmas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CC' COUNTRY_ID, 'Iles Cocos (Keeling)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CO' COUNTRY_ID, 'Colombie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KM' COUNTRY_ID, 'Comores' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CG' COUNTRY_ID, 'Congo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CD' COUNTRY_ID, 'Congo, République Démocratique du' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CK' COUNTRY_ID, 'Iles Cook' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CR' COUNTRY_ID, 'Costa Rica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CI' COUNTRY_ID, 'Côte d''Ivoire' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HR' COUNTRY_ID, 'Croatie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CU' COUNTRY_ID, 'Cuba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CW' COUNTRY_ID, 'Curaçao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CY' COUNTRY_ID, 'Chypre' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CZ' COUNTRY_ID, 'République Tchèque' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DK' COUNTRY_ID, 'Danemark' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DJ' COUNTRY_ID, 'Djibouti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DM' COUNTRY_ID, 'Dominique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DO' COUNTRY_ID, 'République Dominicaine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'EC' COUNTRY_ID, 'Equateur' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'EG' COUNTRY_ID, 'Egypte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SV' COUNTRY_ID, 'Le Salvador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GQ' COUNTRY_ID, 'Guinée équatoriale' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ER' COUNTRY_ID, 'Erythrée' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'EE' COUNTRY_ID, 'Estonie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ET' COUNTRY_ID, 'Ethiopie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FK' COUNTRY_ID, 'Iles Falkland, Malouines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FO' COUNTRY_ID, 'Iles Féroé' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FJ' COUNTRY_ID, 'Fidji' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FI' COUNTRY_ID, 'Finlande' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FR' COUNTRY_ID, 'France' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GF' COUNTRY_ID, 'Guyane Française' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PF' COUNTRY_ID, 'Polynésie-Française' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TF' COUNTRY_ID, 'Terres australes françaises' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GA' COUNTRY_ID, 'Gabon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GM' COUNTRY_ID, 'Gambie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GE' COUNTRY_ID, 'Géorgie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'DE' COUNTRY_ID, 'Allemagne' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GH' COUNTRY_ID, 'Ghana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GI' COUNTRY_ID, 'Gibraltar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GR' COUNTRY_ID, 'Grèce' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GL' COUNTRY_ID, 'Groenland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GD' COUNTRY_ID, 'Grenade' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GP' COUNTRY_ID, 'Guadeloupe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GU' COUNTRY_ID, 'Guam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GT' COUNTRY_ID, 'Guatemala' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GG' COUNTRY_ID, 'Guernesey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GN' COUNTRY_ID, 'Guinée' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GW' COUNTRY_ID, 'Guinée-Bissau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GY' COUNTRY_ID, 'Guyane' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HT' COUNTRY_ID, 'Haïti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HM' COUNTRY_ID, 'Iles Heard-et-MacDonald' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HN' COUNTRY_ID, 'Honduras' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HK' COUNTRY_ID, 'Hong-Kong' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'HU' COUNTRY_ID, 'Hongrie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IS' COUNTRY_ID, 'Islande' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IN' COUNTRY_ID, 'Inde' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ID' COUNTRY_ID, 'Indonésie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IR' COUNTRY_ID, 'Iran, République Islamique d''' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IQ' COUNTRY_ID, 'Irak' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IE' COUNTRY_ID, 'Irlande' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IM' COUNTRY_ID, 'Ile de Man' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IL' COUNTRY_ID, 'Israël' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'IT' COUNTRY_ID, 'Italie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'JM' COUNTRY_ID, 'Jamaïque' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'JP' COUNTRY_ID, 'Japon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'JE' COUNTRY_ID, 'Jersey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'JO' COUNTRY_ID, 'Jordanie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KZ' COUNTRY_ID, 'Kazakhstan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KE' COUNTRY_ID, 'Kenya' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KI' COUNTRY_ID, 'Kiribati' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KP' COUNTRY_ID, 'Corée, Rép. démocratique populaire de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KR' COUNTRY_ID, 'Corée, République de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KW' COUNTRY_ID, 'Koweït' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KG' COUNTRY_ID, 'Kirghizstan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LA' COUNTRY_ID, 'République démocratique populaire lao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LV' COUNTRY_ID, 'Lettonie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LB' COUNTRY_ID, 'Liban' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LS' COUNTRY_ID, 'Lesotho' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LR' COUNTRY_ID, 'Libéria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LY' COUNTRY_ID, 'Libye' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LI' COUNTRY_ID, 'Liechtenstein' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LT' COUNTRY_ID, 'Lituanie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LU' COUNTRY_ID, 'Luxembourg' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MO' COUNTRY_ID, 'Macao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MK' COUNTRY_ID, 'Macédoine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MG' COUNTRY_ID, 'Madagascar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MW' COUNTRY_ID, 'Malawi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MY' COUNTRY_ID, 'Malaisie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MV' COUNTRY_ID, 'Maldives' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ML' COUNTRY_ID, 'Mali' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MT' COUNTRY_ID, 'Malte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MH' COUNTRY_ID, 'Iles Marshall' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MQ' COUNTRY_ID, 'Martinique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MR' COUNTRY_ID, 'Mauritanie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MU' COUNTRY_ID, 'Maurice' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'YT' COUNTRY_ID, 'Mayotte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MX' COUNTRY_ID, 'Mexique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'FM' COUNTRY_ID, 'Micronésie, Etats fédérés de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MD' COUNTRY_ID, 'Moldavie, République de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MC' COUNTRY_ID, 'Monaco' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MN' COUNTRY_ID, 'Mongolie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ME' COUNTRY_ID, 'Monténégro' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MS' COUNTRY_ID, 'Montserrat' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MA' COUNTRY_ID, 'Maroc' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MZ' COUNTRY_ID, 'Mozambique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MM' COUNTRY_ID, 'Myanmar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NA' COUNTRY_ID, 'Namibie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NR' COUNTRY_ID, 'Nauru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NP' COUNTRY_ID, 'Népal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NL' COUNTRY_ID, 'Pays-Bas' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NC' COUNTRY_ID, 'Nouvelle-Calédonie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NZ' COUNTRY_ID, 'Nouvelle-Zélande' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NI' COUNTRY_ID, 'Nicaragua' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NE' COUNTRY_ID, 'Niger' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NG' COUNTRY_ID, 'Nigeria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NU' COUNTRY_ID, 'Niue' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NF' COUNTRY_ID, 'Ile Norfolk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MP' COUNTRY_ID, 'Iles Mariannes-du-Nord' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NO' COUNTRY_ID, 'Norvège' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'OM' COUNTRY_ID, 'Oman' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PK' COUNTRY_ID, 'Pakistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PW' COUNTRY_ID, 'Palau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PS' COUNTRY_ID, 'Palestine, Etat de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PA' COUNTRY_ID, 'Panama' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PG' COUNTRY_ID, 'Papouasie Nouvelle-Guinée' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PY' COUNTRY_ID, 'Paraguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PE' COUNTRY_ID, 'Pérou' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PH' COUNTRY_ID, 'Philippines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PN' COUNTRY_ID, 'Pitcairn (Iles)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PL' COUNTRY_ID, 'Pologne' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PT' COUNTRY_ID, 'Portugal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PR' COUNTRY_ID, 'Porto Rico' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'QA' COUNTRY_ID, 'Qatar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'RE' COUNTRY_ID, 'Réunion' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'RO' COUNTRY_ID, 'Roumanie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'RU' COUNTRY_ID, 'Fédération de Russie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'RW' COUNTRY_ID, 'Rwanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'BL' COUNTRY_ID, 'Saint Barthélemy' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SH' COUNTRY_ID, 'Sainte-Hélène, Ascension et Tristan da Cunha' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'KN' COUNTRY_ID, 'Saint-Christophe-et-Nevis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LC' COUNTRY_ID, 'Sainte-Lucie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'MF' COUNTRY_ID, 'Saint-Martin (partie française)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'PM' COUNTRY_ID, 'Saint-Pierre-et-Miquelon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VC' COUNTRY_ID, 'Saint-Vincent-et-les-Grenadines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'WS' COUNTRY_ID, 'Samoa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SM' COUNTRY_ID, 'Saint-Marin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ST' COUNTRY_ID, 'São Tomé-et-Principe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SA' COUNTRY_ID, 'Arabie Saoudite' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SN' COUNTRY_ID, 'Sénégal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'RS' COUNTRY_ID, 'Serbie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SC' COUNTRY_ID, 'Seychelles' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SL' COUNTRY_ID, 'Sierra Leone' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SG' COUNTRY_ID, 'Singapour' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SX' COUNTRY_ID, 'Saint-Martin (partie néerlandaise)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SK' COUNTRY_ID, 'Slovaquie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SI' COUNTRY_ID, 'Slovénie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SB' COUNTRY_ID, 'Iles Salomon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SO' COUNTRY_ID, 'Somalie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ZA' COUNTRY_ID, 'Afrique du Sud' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GS' COUNTRY_ID, 'Géorgie du Sud et les Iles Sandwich du Sud' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SS' COUNTRY_ID, 'Soudan du Sud' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ES' COUNTRY_ID, 'Espagne' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'LK' COUNTRY_ID, 'Sri Lanka' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SD' COUNTRY_ID, 'Soudan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SR' COUNTRY_ID, 'Surinam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SJ' COUNTRY_ID, 'Svalbard et Jan Mayen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SZ' COUNTRY_ID, 'Swaziland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SE' COUNTRY_ID, 'Suède' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'CH' COUNTRY_ID, 'Suisse' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'SY' COUNTRY_ID, 'République arabe syrienne' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TW' COUNTRY_ID, 'Taïwan (Province de Chine)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TJ' COUNTRY_ID, 'Tadjikistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TZ' COUNTRY_ID, 'Tanzanie, République-Unie de' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TH' COUNTRY_ID, 'Thaïlande' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TL' COUNTRY_ID, 'Timor-Leste' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TG' COUNTRY_ID, 'Togo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TK' COUNTRY_ID, 'Tokélaou' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TO' COUNTRY_ID, 'Tonga' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TT' COUNTRY_ID, 'Trinité-et-Tobago' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TN' COUNTRY_ID, 'Tunisie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TR' COUNTRY_ID, 'Turquie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TM' COUNTRY_ID, 'Turkménistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TC' COUNTRY_ID, 'Iles Turks et Caicos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'TV' COUNTRY_ID, 'Tuvalu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'UG' COUNTRY_ID, 'Ouganda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'UA' COUNTRY_ID, 'Ukraine' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AE' COUNTRY_ID, 'Emirats Arabes Unis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'GB' COUNTRY_ID, 'Royaume-Uni' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'US' COUNTRY_ID, 'Etats-Unis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'UM' COUNTRY_ID, 'Iles Mineures éloignées des Etats-Unis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'UY' COUNTRY_ID, 'Uruguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'UZ' COUNTRY_ID, 'Ouzbékistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VU' COUNTRY_ID, 'Vanuatu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VA' COUNTRY_ID, 'Saint-Siège [Etat de la Cité du Vatican]' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VE' COUNTRY_ID, 'Vénézuela' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VN' COUNTRY_ID, 'Viêt Nam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VG' COUNTRY_ID, 'Iles Vierges, Royaume-Uni' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'VI' COUNTRY_ID, 'Iles Vierges, Etats-Unis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'WF' COUNTRY_ID, 'Wallis-et-Futuna' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'EH' COUNTRY_ID, 'Sahara Occidental' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'YE' COUNTRY_ID, 'Yémen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ZM' COUNTRY_ID, 'Zambie' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'ZW' COUNTRY_ID, 'Zimbabwe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'AX' COUNTRY_ID, 'Iles d''Åland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, 'NEW' COUNTRY_ID, 'Nouveau pays' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 3 LANG, '99' COUNTRY_ID, 'Multiple' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COUNTRY base USING
( SELECT COUNTRY_ID COUNTRY_ID, COUNTRY_DESC COUNTRY_DESC FROM COUNTRY_TL TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3)) USE_THIS
ON ( base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.COUNTRY_DESC = use_this.COUNTRY_DESC;
--
DELETE FROM COUNTRY_TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
