SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'CANNOT_CHANGE_REPL_WH' REASON_KEY, 'Impossible de modifier le statut de cet entrepôt car il est utilisé comme entrepôt d''origine pour le réapprovisionnement de cette UDS.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'CONSIGNMENT' REASON_KEY, 'Impossible de reclasser un article entre rayon de consignation %s1 et rayon de non-consignation. Ancien rayon : %s1 Nouveau rayon : %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'COST_LOC_PENDING' REASON_KEY, 'Le changement de site de calcul du PA ne peut pas être traité pour l''article %s1 et le site %s2 car un changement de site de calcul du PA est déjà en attente pour eux.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'FORECAST_REPL_METHOD' REASON_KEY, 'Les sites de cet article utilisent une méthode de prévision de réapprovisionnement. Par conséquent, vous ne pouvez pas changer l''article en non prévisible.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_ACTIVATE_DATE' REASON_KEY, 'La nouvelle date de début, %s1, est postérieure à la date de désactivation actuelle %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_DEACTIVATE_DATE' REASON_KEY, 'La nouvelle date de désactivation, %s1, est antérieure à la date d''activation actuelle %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_MAX_STOCK' REASON_KEY, 'Le nouveau stock maximum de %s1 unités est inférieur au stock minimum actuel de %s2 unités.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_MAX_SUPPLY_DAYS' REASON_KEY, 'La nouvelle durée de couverture maximum de %s1 jours est inférieure à la durée de couverture minimum de %s2 jours.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_MIN_STOCK' REASON_KEY, 'Le nouveau stock minimum de %s1 unités est supérieur au stock maximum actuel de %s2 unités.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_MIN_SUPPLY_DAYS' REASON_KEY, 'La nouvelle durée de couverture minimum de %s1 jours est supérieure à la durée de couverture maximum de %s2 jours.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_MULT_RUNS_PER_DAY' REASON_KEY, 'L''indicateur Plusieurs exécutions par jour peut avoir la valeur Y pour Yes (O pour Oui) uniquement lorsque la méthode de réapprovisionnement est Commandes magasin, que le cycle d''analyse est quotidien et que la catégorie de stock est Stocké en entrepôt ou Lien entrepôt.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_REPL_ITEM_TYPE' REASON_KEY, 'Vous ne pouvez actualiser que les attributs de réapprovisionnement des UDS première nécessité et mode, et des styles mode.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_REPL_ORDER_CTRL' REASON_KEY, 'La catégorie de stock ne peut pas être mise à jour pour devenir %s1 lorsque le site en cours de réapprovisionnement est un magasin et que le contrôle de commande en cours est Feuille de travail acheteur.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_SOURCING_WH' REASON_KEY, 'L''article n''existe pas dans le nouvel entrepôt d''origine.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_STORE_CLOSE_DATE' REASON_KEY, 'La nouvelle date de fermeture du magasin, %s1, est antérieure à la date d''ouverture actuelle, %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_STORE_OPEN_DATE' REASON_KEY, 'La nouvelle date d''ouverture du magasin, %s1, est postérieure à la date de fermeture actuelle, %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'INVALID_TSF_ZERO_SOH_IND' REASON_KEY, 'L''indicateur de transfert zéro stock disponible peut avoir la valeur Y pour Yes (O pour Oui) uniquement lorsque la méthode de réapprovisionnement est Commandes magasin et que la catégorie de stock est Stocké en entrepôt.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_IN_GROUP_ON_ORDER' REASON_KEY, 'Un article de ce groupe d''articles est en commande active. La sous-famille/famille/rayon des articles de ce groupe ne peut donc pas être changé(e)' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_IN_USE' REASON_KEY, 'Cet article fait partie d''une commande active, d''un transfert ou d''un changement de PV, son statut ne peut donc pas être modifié.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_LOC_NOT_ACTIVATE' REASON_KEY, 'L''article %s1 n''est pas actif sur le site %s2. Il ne peut donc pas être activé.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_NOT_FORECASTABLE' REASON_KEY, 'L''article n''est pas prévisible. Par conséquent, les méthodes de réapprovisionnement Dynamique et Durée de couverture ne peuvent pas être utilisées.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_NOT_RANGE_COST_LOC' REASON_KEY, 'Le site de calcul du PA %s1 n''est pas classé avec l''article %s2 ; seul le magasin ou un entrepôt classé avec l''article peut être utilisé comme site de calcul du PA.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_NOT_TOLERANCEABLE' REASON_KEY, 'Impossible d''actualiser la tolérance si la catégorie de stock est Entrepôt.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_ON_APP_ORD' REASON_KEY, 'Les articles spécifiés ou leurs enfants/petits-enfants à reclasser figurent sur la ou les commande(s) approuvée(s).' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_ON_ORDER_ZONE' REASON_KEY, 'Vous ne pouvez pas modifier la zone de coût car certains articles figurent sur au moins une commande.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_STATUS_DELETE' REASON_KEY, 'Le statut de l''article en cours pour %s1 %s2 est Supprimé, par conséquent vous ne pouvez modifier ce statut.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_SUPPLIER' REASON_KEY, 'Le fournisseur %s1 n''est pas associé à l''article.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ITEM_SUPPLIER_COUNTRY' REASON_KEY, 'Le fournisseur %s1 / pays d''origine %s2 n''est pas associé à l''article.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'LAST_PHASE' REASON_KEY, 'Suppression impossible. Une seule saison est affectée à cet article.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'MAIN_EXISTS_AS_SUB' REASON_KEY, 'L''article principal %s1 ne peut pas être affecté sur le site %s2 car il existe déjà en tant qu''article de substitution sur ce site.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'MAIN_ITEM_FORECASTS' REASON_KEY, 'L''article est un article principal du traitement d''un article de substitution et ses prévisions sont en cours d''utilisation. Il ne peut pas être modifié pour devenir non-prévisible.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'MAIN_ITEM_LOC_MISMATCH' REASON_KEY, 'L''article principal %s1 n''existe pas sur le site %s2. Seules les combinaisons Article-Site existantes peuvent être utilisées au sein du dialogue Articles de substitution.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NOT_LIST_SKU_STYLE_S' REASON_KEY, 'Vous ne pouvez mettre à jour que les attributs des UDS première nécessité, des UDS mode et des styles mode.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NOT_LIST_SKU_STYLE_W' REASON_KEY, 'Vous ne pouvez mettre à jour que les attributs des UDS première nécessité, des UDS mode et des styles mode.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NOT_ON_REPL_NO_DEACTIVATE' REASON_KEY, 'Le site d''article n''est pas en réapprovisionnement, il est donc impossible de le désactiver.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NOT_ON_REPL_NO_UPDATE' REASON_KEY, 'Le site d''article n''est pas en réapprovisionnement, vous ne pouvez donc pas mettre à jour les attributs.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NOT_SKU_STYLE' REASON_KEY, 'Les attributs d''indicateur d''article n''existent qu''aux niveaux UDS première nécessité et style Mode.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_ASSOC_UDA_VALUE' REASON_KEY, 'La nouvelle hiérarchie de l''article %s1/%s2/%s3 a des attributs définis par l''utilisateur par défaut mais pas de valeur ADU associée par défaut.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_BUYER_WKSHT' REASON_KEY, 'Le contrôle de commande ne peut pas être mis à jour pour devenir Feuille de travail acheteur lorsque la catégorie de stock en cours est %s1 et que le site en cours de réapprovisionnement est un magasin.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_DOMAIN_EXISTS' REASON_KEY, 'La hiérarchie de marchandises de l''article n''est associée à aucun domaine. Par conséquent, l''article ne peut pas avoir le statut Prévisible.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_REQUIRED_UDA' REASON_KEY, 'L''article %s1 doit être associé à l''ADU %s2. Le fait de déplacer cet enregistrement va entraîner la violation de cette condition.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_UPDATE_CHILD_COST_ZONE' REASON_KEY, 'Le groupe de zones de coût de l''article ne peut pas être mis à jour parce qu''il s''agit d''un article enfant et que son parent doit avoir la même zone de coût. Cependant, si le parent de l''article fait partie de la mise à jour, l''article peut avoir été mis à jour.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_UPD_EXIST_VAT' REASON_KEY, 'L''UDS %s1 possède déjà un enregistrement UDS TVA pour cette région de TVA et cette date de début avec un type de TVA "C" ou "R". Elle ne peut pas être mise à jour avec la valeur "B".' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'NO_UPD_VAT_TYPE' REASON_KEY, 'L''UDS %s1 possède un enregistrement UDS TVA avec un type de TVA "B" pour cette région de TVA et cette date de début. Elle ne peut pas être mise à jour avec la valeur "C" ou "R".' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ON_CLEAR_NO_ACTIVATE' REASON_KEY, 'Le site d''article est en solde, il est donc impossible de l''activer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ON_CONSIGN_NO_ACTIVATE' REASON_KEY, 'Le site de l''article est en consignation, vous ne pouvez donc pas l''activer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'ON_REPL_NO_ACTIVATE' REASON_KEY, 'Le site d''article est déjà en réapprovisionnement, vous ne pouvez donc pas l''activer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'PACK_FORECAST' REASON_KEY, 'L''article est un article de pack, qui ne peut pas faire l''objet de prévisions.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'PACK_ITEM_LOC_MISMATCH' REASON_KEY, 'Le pack de réapprovisionnement principal %s1 n''existe pas sur le site %s2. Seules les combinaisons Article-Site existantes peuvent être utilisées au sein du dialogue Articles de substitution.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'PACK_LOC_NO_EXIST' REASON_KEY, 'Le pack de réapprovisionnement principal %s1 n''existe pas sur le site %s2 avec le statut actif.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'PACK_STATUS' REASON_KEY, 'Le pack de réapprovisionnement principal, %s1, n''est pas valide car il n''a pas le même statut que son article composant %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'RECLASS_EXIST' REASON_KEY, 'L''article figure dans une autre reclassification en attente.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'RECLASS_NO_DOMAIN' REASON_KEY, 'Cet article est prévisible. La nouvelle hiérarchie des marchandises du rayon %s1, de famille %s2 et de sous-famille %s3, n''est associée à aucun domaine. L''article ne peut donc pas être reclassé.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'REJECT_DEPT_SIZE' REASON_KEY, 'Impossible de reclasser le style car au moins l''une des tailles demandées est absente du nouveau rayon %s1.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'REJECT_ITEM_LEVEL' REASON_KEY, 'Seuls les articles de niveau un peuvent être reclassés. L''article %s1 est un article de niveau %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'REJECT_ITEM_STATUS' REASON_KEY, 'L''article figure sur une commande active, par conséquent vous ne pouvez pas modifier le rayon.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SIMPLE_PACK' REASON_KEY, 'Impossible de reclasser le pack simple %s1. Il ne peut être reclassé qu''avec son article composant.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SINGLE_UDA_EXISTS' REASON_KEY, 'L''article %s1 est déjà associé à l''ADU %s2, qui ne permet qu''une seule association par article.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SKU_IN_ACTIVE_PACK' REASON_KEY, 'Cet article fait partie d''un pack actif. Son statut ne peut pas être modifié.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SKU_ORD_EXIST' REASON_KEY, 'Cet article possède des commandes actives. Son statut ne peut pas être modifié.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SKU_TSF_EXIST' REASON_KEY, 'Cet article possède un transfert actif. Son statut ne peut pas être modifié.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SOURCE_WH_NO_EXIST' REASON_KEY, 'L''article n''est pas associé à l''entrepôt %s1, ou n''y est pas actif ; cet entrepôt ne peut donc pas être utilisé en tant qu''entrepôt d''origine.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'STOCK_COUNT_PEND' REASON_KEY, 'Cet article est actuellement en cours de comptage. Il ne peut pas être reclassé pour l''instant.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'STYLE_SKU_ON_ORDER' REASON_KEY, 'La zone de coût ne peut pas être modifiée car l''article est un style et qu''au moins une de ses UDS figure sur une ou plusieurs commandes.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUB_EXISTS_AS_MAIN' REASON_KEY, 'L''article de substitution %s1 ne peut pas être affecté sur le site %s2 car il existe déjà en tant qu''article principal sur ce site.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUB_ITEM_FORECASTS' REASON_KEY, 'L''article est un article de substitution et ses prévisions sont en cours d''utilisation. Il ne peut pas être modifié pour devenir non-prévisible.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUB_ITEM_LOC_MISMATCH' REASON_KEY, 'L''article de substitution %s1 n''existe pas sur le site %s2. Seules les combinaisons Article-Site existantes peuvent être utilisées au sein du dialogue Articles de substitution.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUB_ITEM_NO_ACTIVATE' REASON_KEY, 'L''article est un article de substitution, il est donc impossible de l''activer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUPP_NO_EXIST_PACK' REASON_KEY, 'Le pack %s1 n''est associé ni au fournisseur %s2 ni au pays d''origine %s3.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'SUP_LOC_NOT_SAME_ORG_UNIT' REASON_KEY, 'Impossible d''associer le fournisseur %s1 sur le site %s2 car ils appartiennent à des unités organisationnelles distinctes.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'UDA_REQD' REASON_KEY, 'La nouvelle hiérarchie de l''article comporte des ADU obligatoires qui n''ont pas encore été affectés à l''article.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 3 LANG, 'XDOCK_WH_ITEM_NOT_ACTIVE' REASON_KEY, 'UDS %s1 non active pour l''entrepôt cross dock défini %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO MC_REJECTION_REASONS base USING
( SELECT REASON_KEY REASON_KEY, REJECTION_REASON REJECTION_REASON FROM MC_REJECTION_REASONS_TL TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3)) USE_THIS
ON ( base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE SET base.REJECTION_REASON = use_this.REJECTION_REASON;
--
DELETE FROM MC_REJECTION_REASONS_TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
