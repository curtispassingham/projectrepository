SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '1' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'US sans erreur' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '2' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Avec avertissements de recensement' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '3' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Rejeté' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '4' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Annulé' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '5' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Accepté, Non liquidé' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '6' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Liquidé' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ENTRY_STATUS_TL TL USING
(SELECT  LANG,	ENTRY_STATUS,  IMPORT_COUNTRY_ID,  ENTRY_STATUS_DESC
FROM  (SELECT 3 LANG, '7' ENTRY_STATUS, 'US' IMPORT_COUNTRY_ID, 'Liquidé de nouveau' ENTRY_STATUS_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ENTRY_STATUS base where dl.ENTRY_STATUS = base.ENTRY_STATUS and dl.IMPORT_COUNTRY_ID = base.IMPORT_COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ENTRY_STATUS = use_this.ENTRY_STATUS and tl.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.ENTRY_STATUS, use_this.IMPORT_COUNTRY_ID, use_this.ENTRY_STATUS_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ENTRY_STATUS base USING
( SELECT ENTRY_STATUS ENTRY_STATUS, IMPORT_COUNTRY_ID IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC ENTRY_STATUS_DESC FROM ENTRY_STATUS_TL TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3)) USE_THIS
ON ( base.ENTRY_STATUS = use_this.ENTRY_STATUS and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC;
--
DELETE FROM ENTRY_STATUS_TL where lang = 3
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 3);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
