SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set define "^";
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ADDR_TL
delete from ADDR_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from AREA_TL
delete from AREA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from BANNER_TL
delete from BANNER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from BRAND_TL
delete from BRAND_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CHAIN_TL
delete from CHAIN_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CHANNELS_TL
delete from CHANNELS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CLASS_TL
delete from CLASS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CODE_DETAIL_TL
delete from CODE_DETAIL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COMPANY_CLOSED_TL
delete from COMPANY_CLOSED_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COMPHEAD_TL
delete from COMPHEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COST_EVENT_RUN_TYPE_CONFIG_TL
delete from COST_EVENT_RUN_TYPE_CNFG_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COST_ZONE_TL
delete from COST_ZONE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COST_ZONE_GROUP_TL
delete from COST_ZONE_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from COUNTRY_TL
delete from COUNTRY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CURRENCIES_TL
delete from CURRENCIES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CUSTOMER_SEGMENTS_TL
delete from CUSTOMER_SEGMENTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CUSTOMER_SEGMENT_TYPES_TL
delete from CUSTOMER_SEGMENT_TYPES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from CVB_HEAD_TL
delete from CVB_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DEAL_ATTRIB_DEF_TL
delete from DEAL_ATTRIB_DEF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DELIVERY_SLOT_TL
delete from DELIVERY_SLOT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DEPS_TL
delete from DEPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIFF_GROUP_HEAD_TL
delete from DIFF_GROUP_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIFF_IDS_TL
delete from DIFF_IDS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIFF_RANGE_HEAD_TL
delete from DIFF_RANGE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIFF_RATIO_HEAD_TL
delete from DIFF_RATIO_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIFF_TYPE_TL
delete from DIFF_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DISTRICT_TL
delete from DISTRICT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DIVISION_TL
delete from DIVISION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DOC_TL
delete from DOC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from DYNAMIC_HIER_TOKEN_MAP_TL
delete from DYNAMIC_HIER_TOKEN_MAP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ELC_COMP_TL
delete from ELC_COMP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ENTRY_STATUS_TL
delete from ENTRY_STATUS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ENTRY_TYPE_TL
delete from ENTRY_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from GROUPS_TL
delete from GROUPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from HALF_TL
delete from HALF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from HTS_TL
delete from HTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from HTS_CHAPTER_TL
delete from HTS_CHAPTER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from HTS_CHAPTER_RESTRAINTS_TL
delete from HTS_CHAPTER_RESTRAINTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ITEM_IMAGE_TL
delete from ITEM_IMAGE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ITEM_MASTER_TL
delete from ITEM_MASTER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ITEM_SUPPLIER_TL
delete from ITEM_SUPPLIER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ITEM_XFORM_HEAD_TL
delete from ITEM_XFORM_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from LANG_TL
delete from LANG_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from LOCATION_CLOSED_TL
delete from LOCATION_CLOSED_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from LOC_LIST_HEAD_TL
delete from LOC_LIST_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from LOC_TRAITS_TL
delete from LOC_TRAITS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from MC_REJECTION_REASONS_TL
delete from MC_REJECTION_REASONS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from OGA_TL
delete from OGA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from ORG_UNIT_TL
delete from ORG_UNIT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from OUTLOC_TL
delete from OUTLOC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PACK_TMPL_HEAD_TL
delete from PACK_TMPL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PARTNER_TL
delete from PARTNER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PEND_MERCH_HIER_TL
delete from PEND_MERCH_HIER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PHASES_TL
delete from PHASES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from POS_COUPON_HEAD_TL
delete from POS_COUPON_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from POS_TENDER_TYPE_HEAD_TL
delete from POS_TENDER_TYPE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PO_TYPE_TL
delete from PO_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from PRIORITY_GROUP_TL
delete from PRIORITY_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from QUOTA_CATEGORY_TL
delete from QUOTA_CATEGORY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from RECLASS_HEAD_TL
delete from RECLASS_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from REGION_TL
delete from REGION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from RELATED_ITEM_HEAD_TL
delete from RELATED_ITEM_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from RESTART_CONTROL_TL
delete from RESTART_CONTROL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from RTK_ERRORS_TL
delete from RTK_ERRORS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SA_CONSTANTS_TL
delete from SA_CONSTANTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SA_ERROR_CODES_TL
delete from SA_ERROR_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SA_ROUNDING_RULE_HEAD_TL
delete from SA_ROUNDING_RULE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SA_RULE_HEAD_TL
delete from SA_RULE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SA_TOTAL_HEAD_TL
delete from SA_TOTAL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SCAC_TL
delete from SCAC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SEASONS_TL
delete from SEASONS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SEC_GROUP_TL
delete from SEC_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SKULIST_HEAD_TL
delete from SKULIST_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from STATE_TL
delete from STATE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from STORE_TL
delete from STORE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from STORE_ADD_TL
delete from STORE_ADD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from STORE_FORMAT_TL
delete from STORE_FORMAT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from STORE_GRADE_GROUP_TL
delete from STORE_GRADE_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SUBCLASS_TL
delete from SUBCLASS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SUPS_TL
delete from SUPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SUPS_PACK_TMPL_DESC_TL
delete from SUPS_PACK_TMPL_DESC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from SUP_TRAITS_TL
delete from SUP_TRAITS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TARIFF_TREATMENT_TL
delete from TARIFF_TREATMENT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TICKET_TYPE_HEAD_TL
delete from TICKET_TYPE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TIMELINE_HEAD_TL
delete from TIMELINE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TIMELINE_STEP_COMP_TL
delete from TIMELINE_STEP_COMP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TRAN_DATA_CODES_TL
delete from TRAN_DATA_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TRAN_DATA_CODES_REF_TL
delete from TRAN_DATA_CODES_REF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TSFZONE_TL
delete from TSFZONE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from TSF_ENTITY_TL
delete from TSF_ENTITY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from UDA_TL
delete from UDA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from UDA_ITEM_FF_TL
delete from UDA_ITEM_FF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from UDA_VALUES_TL
delete from UDA_VALUES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from VAT_CODES_TL
delete from VAT_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from VAT_REGION_TL
delete from VAT_REGION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WF_COST_BUILDUP_TMPL_DETAIL_TL
delete from WF_COST_BUILDUP_TMPL_DTL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WF_COST_BUILDUP_TMPL_HEAD_TL
delete from WF_COST_BUILDUP_TMPL_HD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WF_CUSTOMER_TL
delete from WF_CUSTOMER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WF_CUSTOMER_GROUP_TL
delete from WF_CUSTOMER_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WH_TL
delete from WH_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WO_ACTIVITY_TL
delete from WO_ACTIVITY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Deleting langauge entry in primary language from WO_TMPL_HEAD_TL
delete from WO_TMPL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS);
commit;
--------------------------------------------------------------------------------------------------------------------------------------
set define "&";
