SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 1 CODE, 'Nettó értékesítés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 2 CODE, 'Nettó értékesítés áfa nélkül' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 3 CODE, 'Nem készletcikkek értékesítései/visszárui' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 4 CODE, 'Visszáruk' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 5 CODE, 'Nem készletcikkek értékesítései áfa nélkül' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 6 CODE, 'Megállapodások bevétele (értékesítés)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 7 CODE, 'Megállapodás bevétele (beszerzés)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 8 CODE, 'Fix bevétel elhatárolása' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 10 CODE, 'Súlyeltérés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 11 CODE, 'Felár' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 12 CODE, 'Felár visszavonása' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 13 CODE, 'Végleges árengedmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 14 CODE, 'Árengedmény visszavonása' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 15 CODE, 'Promóciós árengedmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 16 CODE, 'Kiárusítási árengedmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 17 CODE, 'Vállalatközi felár' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 18 CODE, 'Vállalatközi árengedmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 20 CODE, 'Beszerzések' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 21 CODE, 'Számla - csak az Oracle GL-felülettel használatos' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 22 CODE, 'Készlethelyesbítés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 23 CODE, 'Készlethelyesbítés -ELÁBÉ' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 24 CODE, 'Visszáru' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 25 CODE, 'Rendelkezésre nem álló készlet transzfere' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 26 CODE, 'Rakomány' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 27 CODE, 'Minőség-ellenőrzés - Visszáru' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 28 CODE, 'Nyereségfelár- bevételező hely' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 29 CODE, 'Kiadásfelár- bevételező hely' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 30 CODE, 'Bejövő transzferek' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 31 CODE, 'Bejövő könyvi transzferek' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 32 CODE, 'Kimenő transzferek' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 33 CODE, 'Kimenő könyvi transzferek' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 34 CODE, 'Bejövő átsorolások' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 36 CODE, 'Kimenő átsorolások' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 37 CODE, 'Vállalatközi bejövő' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 38 CODE, 'Vállalatközi kimenő' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 39 CODE, 'Vállalatközi árrés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 41 CODE, 'Adattárház - készletanalitika helyesbítése' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 44 CODE, 'Adattárház - bejövő transzfer bevételezése' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 50 CODE, 'Készletnyitás' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 51 CODE, 'Veszteség' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 52 CODE, 'Készletzárás' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 53 CODE, 'Bruttó árrés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 54 CODE, 'FoFÉ - Értékesíthető áruk' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 55 CODE, 'Leltárak közötti értékesítés össz.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 56 CODE, 'Leltárak közötti veszteség össz.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 57 CODE, 'Leltár - HTN - Értékesítési összeg' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 58 CODE, 'Leltár - HTN - Veszteség összege' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 59 CODE, 'Leltár - könyv szerinti készlet - költség' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 60 CODE, 'Alkalmazotti kedvezmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 61 CODE, 'Leltár - tényleges készlet - költség/kiskereskedelmi ár' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 62 CODE, 'Fuvarozási reklamáció' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 63 CODE, 'MR-tevékenység - készletfrissítés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 64 CODE, 'MR-tevékenység - feladás pénzügybe' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 65 CODE, 'Újrakészletezési díj' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 70 CODE, 'Költségeltérés' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 71 CODE, 'Költségeltérés - kiskereskedelmiár-elszámolás' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 72 CODE, 'Költségeltérés - költségelszámolás' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 73 CODE, 'Költségeltérés - bevételező költségének helyesbítése- FiFO' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 74 CODE, 'Visszaigényelhető adó a rendeltetési helynél' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 75 CODE, 'Visszaigényelhető adó a küldő helynél' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 80 CODE, 'Munkaráfordítás/egyéb értékesítési költség' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 81 CODE, 'Készpénzfizetési kedvezmény' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 82 CODE, 'Franchise értékesítések' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 83 CODE, 'Franchise visszáruk' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 84 CODE, 'Franchise felárak' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 85 CODE, 'Franchise árengedmények' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 86 CODE, 'Franchise újrakészletezési díj' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 87 CODE, 'Bejövő áfa - költség' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 10 LANG, 88 CODE, 'Kimenő áfa - kiskereskedelmi ár' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES base USING
( SELECT CODE CODE, DECODE DECODE FROM TRAN_DATA_CODES_TL TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10)) USE_THIS
ON ( base.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE SET base.DECODE = use_this.DECODE;
--
DELETE FROM TRAN_DATA_CODES_TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
