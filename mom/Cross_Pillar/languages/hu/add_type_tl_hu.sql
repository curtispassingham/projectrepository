SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '01' ADDRESS_TYPE, 10 LANG, 'Üzleti' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '02' ADDRESS_TYPE, 10 LANG, 'Postai' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '03' ADDRESS_TYPE, 10 LANG, 'Visszáruk' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '04' ADDRESS_TYPE, 10 LANG, 'Rendelés' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '05' ADDRESS_TYPE, 10 LANG, 'Számla' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '06' ADDRESS_TYPE, 10 LANG, 'Átutalás' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ADD_TYPE_TL TL USING
(SELECT  ADDRESS_TYPE,	LANG,  TYPE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT '07' ADDRESS_TYPE, 10 LANG, 'Franchise' TYPE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM ADD_TYPE base where dl.ADDRESS_TYPE = base.ADDRESS_TYPE)) USE_THIS
ON ( tl.ADDRESS_TYPE = use_this.ADDRESS_TYPE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.TYPE_DESC = use_this.TYPE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (ADDRESS_TYPE, LANG, TYPE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.ADDRESS_TYPE, use_this.LANG, use_this.TYPE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
