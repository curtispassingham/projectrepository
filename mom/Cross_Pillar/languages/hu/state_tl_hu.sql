SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'AK' STATE, 'US' COUNTRY_ID, 'Alaszka' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'AL' STATE, 'US' COUNTRY_ID, 'Alabama' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'AR' STATE, 'US' COUNTRY_ID, 'Arkansas' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'AS' STATE, 'US' COUNTRY_ID, 'Amerikai Szamoa' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'AZ' STATE, 'US' COUNTRY_ID, 'Arizona' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'CA' STATE, 'US' COUNTRY_ID, 'Kalifornia' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'CO' STATE, 'US' COUNTRY_ID, 'Colorado' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'CT' STATE, 'US' COUNTRY_ID, 'Connecticut' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'DC' STATE, 'US' COUNTRY_ID, 'Kolumbia Kerület' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'DE' STATE, 'US' COUNTRY_ID, 'Delaware' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'FL' STATE, 'US' COUNTRY_ID, 'Florida' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'GA' STATE, 'US' COUNTRY_ID, 'Grúzia' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'GU' STATE, 'US' COUNTRY_ID, 'Guam' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'HI' STATE, 'US' COUNTRY_ID, 'Hawaii' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'IA' STATE, 'US' COUNTRY_ID, 'Iowa' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'ID' STATE, 'US' COUNTRY_ID, 'Idaho' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'IL' STATE, 'US' COUNTRY_ID, 'Illinois' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'IN' STATE, 'US' COUNTRY_ID, 'Indiana' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'KS' STATE, 'US' COUNTRY_ID, 'Kansas' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'KY' STATE, 'US' COUNTRY_ID, 'Kentucky' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'LA' STATE, 'US' COUNTRY_ID, 'Louisiana' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MA' STATE, 'US' COUNTRY_ID, 'Massachusetts' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MD' STATE, 'US' COUNTRY_ID, 'Maryland' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'ME' STATE, 'US' COUNTRY_ID, 'Maine' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MI' STATE, 'US' COUNTRY_ID, 'Michigan' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MN' STATE, 'US' COUNTRY_ID, 'Minnesota' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MO' STATE, 'US' COUNTRY_ID, 'Missouri' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MP' STATE, 'US' COUNTRY_ID, 'Északi-Mariana-szigetek' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MS' STATE, 'US' COUNTRY_ID, 'Mississippi' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'MT' STATE, 'US' COUNTRY_ID, 'Montana' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NC' STATE, 'US' COUNTRY_ID, 'Észak-Karolina' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'ND' STATE, 'US' COUNTRY_ID, 'Észak-Dakota' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NE' STATE, 'US' COUNTRY_ID, 'Nebraska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NH' STATE, 'US' COUNTRY_ID, 'New Hampshire' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NJ' STATE, 'US' COUNTRY_ID, 'New Jersey' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NM' STATE, 'US' COUNTRY_ID, 'Új-Mexikó' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NV' STATE, 'US' COUNTRY_ID, 'Nevada' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'NY' STATE, 'US' COUNTRY_ID, 'New York' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'OH' STATE, 'US' COUNTRY_ID, 'Ohio' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'OK' STATE, 'US' COUNTRY_ID, 'Oklahoma' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'ON' STATE, 'CA' COUNTRY_ID, 'Ontario' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'OR' STATE, 'US' COUNTRY_ID, 'Oregon' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'PA' STATE, 'US' COUNTRY_ID, 'Pennsylvania' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'PR' STATE, 'US' COUNTRY_ID, 'Puerto Rico' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'RI' STATE, 'US' COUNTRY_ID, 'Rhode Island' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'SC' STATE, 'US' COUNTRY_ID, 'Dél-Karolina' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'SD' STATE, 'US' COUNTRY_ID, 'Dél-Dakota' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'TN' STATE, 'US' COUNTRY_ID, 'Tennessee' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'TX' STATE, 'US' COUNTRY_ID, 'Texas' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'UM' STATE, 'US' COUNTRY_ID, 'Egyesült Államok külső szigetei' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'UT' STATE, 'US' COUNTRY_ID, 'Utah' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'VA' STATE, 'US' COUNTRY_ID, 'Virginia' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'VI' STATE, 'US' COUNTRY_ID, 'Virgin-szigetek, USA' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'VT' STATE, 'US' COUNTRY_ID, 'Vermont' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'WA' STATE, 'US' COUNTRY_ID, 'Washington' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'WI' STATE, 'US' COUNTRY_ID, 'Wisconsin' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 10 LANG, 'WV' STATE, 'US' COUNTRY_ID, 'Nyugat-Virginia' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO STATE base USING
( SELECT STATE STATE, COUNTRY_ID COUNTRY_ID, DESCRIPTION DESCRIPTION FROM STATE_TL TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10)) USE_THIS
ON ( base.STATE = use_this.STATE and base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM STATE_TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
