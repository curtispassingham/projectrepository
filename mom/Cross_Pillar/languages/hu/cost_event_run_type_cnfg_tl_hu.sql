SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'CC' EVENT_TYPE, 'Költségváltozás' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'CL' EVENT_TYPE, 'Franchise áruház költségképzési helyének változása' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'CZ' EVENT_TYPE, 'Költségzónahely áthelyezése' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'D' EVENT_TYPE, 'Megállapodás' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'DP' EVENT_TYPE, 'Megállapodás átadása' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'ELC' EVENT_TYPE, 'Becsült behozatali költség' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'ICZ' EVENT_TYPE, 'Cikk - költségzóna-változások' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'MH' EVENT_TYPE, 'Áruhierarchia' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'NIL' EVENT_TYPE, 'Új cikkhely' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'OH' EVENT_TYPE, 'Szervezeti hierarchia' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'PP' EVENT_TYPE, 'Elsődleges csomag költsége' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'R' EVENT_TYPE, 'Átsorolás' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'RTC' EVENT_TYPE, 'Kiskereskedelmiár-változás' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'SC' EVENT_TYPE, 'Cikk, szállító, ország, hely - kapcsolatok változásai' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'SH' EVENT_TYPE, 'Szállítóhierarchia' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'T' EVENT_TYPE, 'Nagykereskedelem/franchise - költségsablon' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 10 LANG, 'TR' EVENT_TYPE, 'Nagykereskedelem/franchise - költségsablon kapcsolata' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG base USING
( SELECT EVENT_TYPE EVENT_TYPE, EVENT_DESC EVENT_DESC FROM COST_EVENT_RUN_TYPE_CNFG_TL TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10)) USE_THIS
ON ( base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE SET base.EVENT_DESC = use_this.EVENT_DESC;
--
DELETE FROM COST_EVENT_RUN_TYPE_CNFG_TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
