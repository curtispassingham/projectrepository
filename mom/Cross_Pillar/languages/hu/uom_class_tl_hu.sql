SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'AGG' UOM, 'AGG' UOM_TRANS, 'Ezüsttartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'AUG' UOM, 'AUG' UOM_TRANS, 'Aranytartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BA' UOM, 'BA' UOM_TRANS, 'Hordó' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BBL' UOM, 'BBL' UOM_TRANS, 'Hordó' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BE' UOM, 'BE' UOM_TRANS, 'Köteg' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BG' UOM, 'BG' UOM_TRANS, 'Zsák' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BI' UOM, 'BI' UOM_TRANS, 'Edény' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BJ' UOM, 'BJ' UOM_TRANS, 'Vödör' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BK' UOM, 'BK' UOM_TRANS, 'Kosár' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'BX' UOM, 'BX' UOM_TRANS, 'Doboz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'C' UOM, 'C' UOM_TRANS, 'Celsius' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CA' UOM, 'CA' UOM_TRANS, 'Konzervdoboz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CAR' UOM, 'CAR' UOM_TRANS, 'Karát' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CBM' UOM, 'CBM' UOM_TRANS, 'Köbméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CC' UOM, 'CC' UOM_TRANS, 'Köbcentiméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CFT' UOM, 'CFT' UOM_TRANS, 'Köbláb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CG' UOM, 'CG' UOM_TRANS, 'Centigramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CGM' UOM, 'CGM' UOM_TRANS, 'Tartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CKG' UOM, 'CKG' UOM_TRANS, 'Tartalom (kg)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CM' UOM, 'CM' UOM_TRANS, 'Centiméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CM2' UOM, 'CM2' UOM_TRANS, 'Négyzetcentiméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CM3' UOM, 'CM3' UOM_TRANS, 'Köbcentiméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CON' UOM, 'CON' UOM_TRANS, 'Konténer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CR' UOM, 'CR' UOM_TRANS, 'Rekesz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CS' UOM, 'CS' UOM_TRANS, 'Láda' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CT' UOM, 'CT' UOM_TRANS, 'Karton' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CTN' UOM, 'CTN' UOM_TRANS, 'Tartalom (t)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CU' UOM, 'CU' UOM_TRANS, 'Térfogat' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CUR' UOM, 'CUR' UOM_TRANS, 'Curie' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CY' UOM, 'CY' UOM_TRANS, 'Tiszta anyagtartalom' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CYG' UOM, 'CYG' UOM_TRANS, 'Tiszta anyagtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'CYK' UOM, 'CYK' UOM_TRANS, 'Tiszta anyagtartalom (kg)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'D' UOM, 'D' UOM_TRANS, 'Elutasító' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'DEG' UOM, 'DEG' UOM_TRANS, 'Fok' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'DOZ' UOM, 'DOZ' UOM_TRANS, 'Tucat' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'DPC' UOM, 'DPC' UOM_TRANS, 'Tucatnyi darabok' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'DPR' UOM, 'DPR' UOM_TRANS, 'Tucat pár' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'DS' UOM, 'DS' UOM_TRANS, 'Adag' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'EA' UOM, 'EA' UOM_TRANS, 'Egység' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FBM' UOM, 'FBM' UOM_TRANS, 'Szál (m)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FIB' UOM, 'FIB' UOM_TRANS, 'Szál' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FT' UOM, 'FT' UOM_TRANS, 'Láb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FT2' UOM, 'FT2' UOM_TRANS, 'Négyzetláb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FT3' UOM, 'FT3' UOM_TRANS, 'Köbláb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'FZ' UOM, 'FZ' UOM_TRANS, 'Folyadék uncia' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'G' UOM, 'G' UOM_TRANS, 'Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GBQ' UOM, 'GBQ' UOM_TRANS, 'Gigabecquerel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GCN' UOM, 'GCN' UOM_TRANS, 'Konténerek (bruttó)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GKG' UOM, 'GKG' UOM_TRANS, 'Aranytartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GL' UOM, 'GL' UOM_TRANS, 'Gallon' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GM' UOM, 'GM' UOM_TRANS, 'Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GR' UOM, 'GR' UOM_TRANS, 'Bruttó' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GRL' UOM, 'GRL' UOM_TRANS, 'HTSUPLD - automatikus beszúrás' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GRS' UOM, 'GRS' UOM_TRANS, 'Bruttó' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'GVW' UOM, 'GVW' UOM_TRANS, 'Jármű bruttó tömege' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'HND' UOM, 'HND' UOM_TRANS, 'Száz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'HUN' UOM, 'HUN' UOM_TRANS, 'Száz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'HZ' UOM, 'HZ' UOM_TRANS, 'Hertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'IN' UOM, 'IN' UOM_TRANS, 'Hüvelyk' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'IN2' UOM, 'IN2' UOM_TRANS, 'Négyzethüvelyk' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'IN3' UOM, 'IN3' UOM_TRANS, 'Köbhüvelyk' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'IRC' UOM, 'IRC' UOM_TRANS, 'Belső árbevételkód' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'IRG' UOM, 'IRG' UOM_TRANS, 'Irídiumtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'JR' UOM, 'JR' UOM_TRANS, 'Üveg' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'JWL' UOM, 'JWL' UOM_TRANS, 'Joule' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'K' UOM, 'K' UOM_TRANS, '1,000' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KCAL' UOM, 'KCAL' UOM_TRANS, 'Kilokalória' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KG' UOM, 'KG' UOM_TRANS, 'Kilogramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KHZ' UOM, 'KHZ' UOM_TRANS, 'Kilohertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KM' UOM, 'KM' UOM_TRANS, 'Kilométer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KM2' UOM, 'KM2' UOM_TRANS, 'Négyzetkilométer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KM3' UOM, 'KM3' UOM_TRANS, 'Köbkilométer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KN' UOM, 'KN' UOM_TRANS, 'Kilonewton' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KPA' UOM, 'KPA' UOM_TRANS, 'Kilopascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KSB' UOM, 'KSB' UOM_TRANS, 'Kilogramm normáltégla' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KTS' UOM, 'KTS' UOM_TRANS, 'Kilogramm összesen (cukor)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KVA' UOM, 'KVA' UOM_TRANS, 'Kilovoltamper' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KVAR' UOM, 'KVAR' UOM_TRANS, 'Kilovoltamper reaktív' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KW' UOM, 'KW' UOM_TRANS, 'Kilowatt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'KWH' UOM, 'KWH' UOM_TRANS, 'Kilowattóra' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'L' UOM, 'L' UOM_TRANS, 'Liter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'LB' UOM, 'LB' UOM_TRANS, 'Font' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'LBS' UOM, 'LBS' UOM_TRANS, 'Font' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'LIN' UOM, 'LIN' UOM_TRANS, 'Folyó' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'LNM' UOM, 'LNM' UOM_TRANS, 'Folyóméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'M' UOM, 'M' UOM_TRANS, 'Méter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'M2' UOM, 'M2' UOM_TRANS, 'Négyzetméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'M3' UOM, 'M3' UOM_TRANS, 'Köbméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MBQ' UOM, 'MBQ' UOM_TRANS, 'Megabecquerel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MC' UOM, 'MC' UOM_TRANS, 'Millicurie' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MG' UOM, 'MG' UOM_TRANS, 'Milligramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MHZ' UOM, 'MHZ' UOM_TRANS, 'Megahertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'ML' UOM, 'ML' UOM_TRANS, 'Milliliter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MM' UOM, 'MM' UOM_TRANS, 'Milliméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MM2' UOM, 'MM2' UOM_TRANS, 'Négyzetmilliméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MM3' UOM, 'MM3' UOM_TRANS, 'Köbmilliméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MPA' UOM, 'MPA' UOM_TRANS, 'Megapascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'MWH' UOM, 'MWH' UOM_TRANS, 'Megawattóra' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'NA' UOM, 'NA' UOM_TRANS, 'Nem áll rendelkezésre' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'NO' UOM, 'NO' UOM_TRANS, 'Szám' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'ODE' UOM, 'ODE' UOM_TRANS, 'Egyenértékű ózonlebontó potenciál' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'OSG' UOM, 'OSG' UOM_TRANS, 'Ozmiumtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'OZ' UOM, 'OZ' UOM_TRANS, 'Uncia' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PACK' UOM, 'PACK' UOM_TRANS, 'Csomag' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PAL' UOM, 'PAL' UOM_TRANS, 'Raklap' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PC' UOM, 'PC' UOM_TRANS, 'Darab' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PCS' UOM, 'PCS' UOM_TRANS, 'Darab' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PDG' UOM, 'PDG' UOM_TRANS, 'Palládiumtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PF' UOM, 'PF' UOM_TRANS, 'Szesztartalom' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PFL' UOM, 'PFL' UOM_TRANS, 'Szesztartalom (liter)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PK' UOM, 'PK' UOM_TRANS, 'Csomag' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PO' UOM, 'PO' UOM_TRANS, 'Fazék' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PRS' UOM, 'PRS' UOM_TRANS, 'Pár' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PT' UOM, 'PT' UOM_TRANS, 'Pint' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'PTG' UOM, 'PTG' UOM_TRANS, 'Plutóniumtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'QT' UOM, 'QT' UOM_TRANS, 'Két pint' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'RBA' UOM, 'RBA' UOM_TRANS, 'Hengerbála' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'RHG' UOM, 'RHG' UOM_TRANS, 'Ródiumtartalom (gramm)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'RPM' UOM, 'RPM' UOM_TRANS, 'Fordulat percenként' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'RUG' UOM, 'RUG' UOM_TRANS, 'Ruténiumtartalom (g)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'SBE' UOM, 'SBE' UOM_TRANS, 'Normáltégla-egyenérték' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'SF' UOM, 'SF' UOM_TRANS, 'Négyzetláb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'SME' UOM, 'SME' UOM_TRANS, 'Négyzetméter-egyenérték' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'SQ' UOM, 'SQ' UOM_TRANS, 'Terület' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'SQM' UOM, 'SQM' UOM_TRANS, 'Négyzetméter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'T' UOM, 'T' UOM_TRANS, 'Metrikus tonna' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'THM' UOM, 'THM' UOM_TRANS, 'Ezer méter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'THS' UOM, 'THS' UOM_TRANS, 'Ezer egység' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'TNV' UOM, 'TNV' UOM_TRANS, 'Tonna (nyers érték)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'TON' UOM, 'TON' UOM_TRANS, 'Tonna' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'TS' UOM, 'TS' UOM_TRANS, 'Rövid tonna' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'V' UOM, 'V' UOM_TRANS, 'Volt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'W' UOM, 'W' UOM_TRANS, 'Watt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'WTS' UOM, 'WTS' UOM_TRANS, 'Watt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'X' UOM, 'X' UOM_TRANS, 'Mennyiség nem szükséges' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'YD' UOM, 'YD' UOM_TRANS, 'Yard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'YD2' UOM, 'YD2' UOM_TRANS, 'Négyzetyard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 LANG, 'YD3' UOM, 'YD3' UOM_TRANS, 'Köbyard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
