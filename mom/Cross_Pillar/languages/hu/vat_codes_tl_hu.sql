SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 10 LANG, 'C' VAT_CODE, 'Összetett' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 10 LANG, 'E' VAT_CODE, 'Mentes' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 10 LANG, 'S' VAT_CODE, 'Normál' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 10 LANG, 'Z' VAT_CODE, 'Nulla' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO VAT_CODES base USING
( SELECT VAT_CODE VAT_CODE, VAT_CODE_DESC VAT_CODE_DESC FROM VAT_CODES_TL TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10)) USE_THIS
ON ( base.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE SET base.VAT_CODE_DESC = use_this.VAT_CODE_DESC;
--
DELETE FROM VAT_CODES_TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
