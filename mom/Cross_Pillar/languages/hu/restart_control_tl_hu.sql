SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'LIKE_STORE' PROGRAM_NAME, 'Hasonló áruház folyamat' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'ang_saplgen' PROGRAM_NAME, 'Google - POSLog kinyerése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'costcalc' PROGRAM_NAME, 'megállapodási kedvezmények kiszámítása a FUTURE_COST táblába' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealact' PROGRAM_NAME, 'Tényadatok kiszámítása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealcls' PROGRAM_NAME, 'a close_date dátumot elérő megállapodások lezárása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealday' PROGRAM_NAME, 'Összegzés a megállapodás napjáig' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealex' PROGRAM_NAME, 'Megállapodási információ felbontása cikkhelyekre' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealfct' PROGRAM_NAME, 'Előre jelzett adatok kiszámítása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealfinc' PROGRAM_NAME, 'Fix megállapodási bevétel a főkönyvbe' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealinc' PROGRAM_NAME, 'A bevétel kiszámítása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dealprg' PROGRAM_NAME, 'a rendszer által megadott időtartam óta lezárt megállapodások törlése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dfrtbld' PROGRAM_NAME, 'rekordok beszúrása a DEAL_SKU_TEMP táblába' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'discotbapply' PROGRAM_NAME, 'Kedv. BKK érvényes' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'ditinsrt' PROGRAM_NAME, 'rekordok beszúrása a deal_sku_temp táblába' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dlyprg' PROGRAM_NAME, 'napi ürítés' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'docclose' PROGRAM_NAME, 'nem társított bevételek lezárása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'dsdupld' PROGRAM_NAME, 'közvetlen áruházi szállítás feltöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'edidlinv' PROGRAM_NAME, 'EDI-számlaletöltés' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'elccostcalc' PROGRAM_NAME, 'Becsült behozatali költség változásainak számítása a FUTURE_COST táblában' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'fifinvcu11i' PROGRAM_NAME, 'Számla feltöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'fifinvcup' PROGRAM_NAME, 'Számlázási felület feldolgozása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'genpreiss' PROGRAM_NAME, 'Előre kiadott rendelések generálása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'hstmthupd' PROGRAM_NAME, 'A következő értékek frissítése: soh, retail, avg_cost az item_loc_hist_mth fájlban' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'lclrbld' PROGRAM_NAME, 'helylista kötegelt újraépítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'lcup798' PROGRAM_NAME, 'hitellevél - lehívások/terhelések' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'lcupld' PROGRAM_NAME, 'hitellevél feltöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'lifstkup' PROGRAM_NAME, 'készlet - átalakítás feltöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'mrt' PROGRAM_NAME, 'Transzferek létrehozása tömeges visszaküldésekből' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'mrtprg' PROGRAM_NAME, 'mrtprg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'mrtrtv' PROGRAM_NAME, 'Visszáru-létrehozás GyTV-khez' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'mrtupd' PROGRAM_NAME, 'mrtupd' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'nwppurge' PROGRAM_NAME, 'Legkisebb értékelés elve - kiürítés' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'ociroq' PROGRAM_NAME, 'NULL' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'ordautcl' PROGRAM_NAME, 'BR-ek törlése/lezárása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'orddscnt' PROGRAM_NAME, 'beszerzési rendelés kedvezménye' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'prchstprg' PROGRAM_NAME, 'Ártörténet kiürítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'precostcalc' PROGRAM_NAME, 'megállapodási kedvezmények előszámítása a deal_sku_temp táblázatba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'refmvl10nentity' PROGRAM_NAME, 'L10N alany statikus nézetének frissítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'refmvlocprimaddr' PROGRAM_NAME, 'Hely elsődleges címe statikus nézet frissítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saescheat' PROGRAM_NAME, 'ReSA - tulajdonjog visszaszállása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpach' PROGRAM_NAME, 'ReSA - exportálás ACH-ba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpdw' PROGRAM_NAME, 'Értékesítés-ellenőrzés - exportálás RDW-be' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpgl' PROGRAM_NAME, 'ReSA-export Oracle GL modulba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpgl107' PROGRAM_NAME, 'ReSA-export Oracle GL modulba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpsim' PROGRAM_NAME, 'Értékesítés-ellenőrzés - exportálás SIM rendszerbe' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saexpuar' PROGRAM_NAME, 'Értékesítés-ellenőrzés - exportálás UAR-ba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'sagetref' PROGRAM_NAME, 'értékesítés-ellenőrzési referenciaadatok letöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saimpadj' PROGRAM_NAME, 'ReSA - helyesbítések importálása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saimptlog' PROGRAM_NAME, 'Értékesítés-ellenőrzés - importálás TLOG-ba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saimptlogfin' PROGRAM_NAME, 'ReSA - TLOG-import befejezése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saimptlogi' PROGRAM_NAME, 'Értékesítés-ellenőrzés - importálás TLOG-ba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'saimptlogtdup_upd' PROGRAM_NAME, 'ReSA-frissítés TDUP-fájlba' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'sapreexp' PROGRAM_NAME, 'ReSA - exportálás előzetes feldolgozása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'sapurge' PROGRAM_NAME, 'ReSA - adatok kiürítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'sarules' PROGRAM_NAME, 'ReSA - szabályfeldolgozás' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'sastdycr' PROGRAM_NAME, 'ReSA - áruházi nap létrehozása' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'satotals' PROGRAM_NAME, 'ReSA - összesítésfeldolgozás' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'soupld' PROGRAM_NAME, 'áruházi megrendelés feltöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'stkschedxpld' PROGRAM_NAME, 'STOCK COUNT SCHEDULE EXPLODE' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'supsplit' PROGRAM_NAME, 'Szállítói felbontási köteg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'szonrbld' PROGRAM_NAME, 'felhasználói zóna biztonságának újraépítése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'trandataprocess' PROGRAM_NAME, 'Külső tranzakcióadatok betöltése' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'vendinvc' PROGRAM_NAME, 'Beszállítói számlázás komplex megállapodásokhoz' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 10 LANG, 'vendinvf' PROGRAM_NAME, 'Beszállítói számlázás fix megállapodásokhoz' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO RESTART_CONTROL base USING
( SELECT PROGRAM_NAME PROGRAM_NAME, PROGRAM_DESC PROGRAM_DESC FROM RESTART_CONTROL_TL TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10)) USE_THIS
ON ( base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE SET base.PROGRAM_DESC = use_this.PROGRAM_DESC;
--
DELETE FROM RESTART_CONTROL_TL where lang = 10
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 10);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
