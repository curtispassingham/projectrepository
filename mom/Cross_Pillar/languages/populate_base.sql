SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set define "^";
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ADDR
merge into ADDR base using
(select ADDR_KEY, ADD_1, ADD_2, ADD_3, CITY, CONTACT_NAME, COUNTY from ADDR_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ADDR_KEY = use_this.ADDR_KEY)
WHEN MATCHED then update set base.ADD_1 = use_this.ADD_1, base.ADD_2 = use_this.ADD_2, base.ADD_3 = use_this.ADD_3, base.CITY = use_this.CITY, base.CONTACT_NAME = use_this.CONTACT_NAME, base.COUNTY =
use_this.COUNTY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table AREA
merge into AREA base using
(select AREA, AREA_NAME from AREA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.AREA = use_this.AREA)
WHEN MATCHED then update set base.AREA_NAME = use_this.AREA_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table BANNER
merge into BANNER base using
(select BANNER_ID, BANNER_NAME from BANNER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.BANNER_ID = use_this.BANNER_ID)
WHEN MATCHED then update set base.BANNER_NAME = use_this.BANNER_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table BRAND
merge into BRAND base using
(select BRAND_NAME, BRAND_DESCRIPTION from BRAND_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.BRAND_NAME = use_this.BRAND_NAME)
WHEN MATCHED then update set base.BRAND_DESCRIPTION = use_this.BRAND_DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CHAIN
merge into CHAIN base using
(select CHAIN, CHAIN_NAME from CHAIN_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CHAIN = use_this.CHAIN)
WHEN MATCHED then update set base.CHAIN_NAME = use_this.CHAIN_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CHANNELS
merge into CHANNELS base using
(select CHANNEL_ID, CHANNEL_NAME from CHANNELS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CHANNEL_ID = use_this.CHANNEL_ID)
WHEN MATCHED then update set base.CHANNEL_NAME = use_this.CHANNEL_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CLASS
merge into CLASS base using
(select DEPT, CLASS, CLASS_NAME from CLASS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DEPT = use_this.DEPT and base.CLASS = use_this.CLASS)
WHEN MATCHED then update set base.CLASS_NAME = use_this.CLASS_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CODE_DETAIL
merge into CODE_DETAIL base using
(select CODE_TYPE, CODE, CODE_DESC from CODE_DETAIL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CODE_TYPE = use_this.CODE_TYPE and base.CODE = use_this.CODE)
WHEN MATCHED then update set base.CODE_DESC = use_this.CODE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COMPANY_CLOSED
merge into COMPANY_CLOSED base using
(select CLOSE_DATE, CLOSE_DESC from COMPANY_CLOSED_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CLOSE_DATE = use_this.CLOSE_DATE)
WHEN MATCHED then update set base.CLOSE_DESC = use_this.CLOSE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COMPHEAD
merge into COMPHEAD base using
(select COMPANY, CO_NAME, CO_ADD1, CO_ADD2, CO_ADD3, CO_CITY, CO_NAME_SECONDARY from COMPHEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.COMPANY = use_this.COMPANY)
WHEN MATCHED then update set base.CO_NAME = use_this.CO_NAME, base.CO_ADD1 = use_this.CO_ADD1, base.CO_ADD2 = use_this.CO_ADD2, base.CO_ADD3 = use_this.CO_ADD3, base.CO_CITY = use_this.CO_CITY,
base.CO_NAME_SECONDARY = use_this.CO_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_EVENT_RUN_TYPE_CONFIG
merge into COST_EVENT_RUN_TYPE_CONFIG base using
(select EVENT_TYPE, EVENT_DESC from COST_EVENT_RUN_TYPE_CNFG_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED then update set base.EVENT_DESC = use_this.EVENT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_ZONE
merge into COST_ZONE base using
(select ZONE_GROUP_ID, ZONE_ID, DESCRIPTION from COST_ZONE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ZONE_GROUP_ID = use_this.ZONE_GROUP_ID and base.ZONE_ID = use_this.ZONE_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COST_ZONE_GROUP
merge into COST_ZONE_GROUP base using
(select ZONE_GROUP_ID, DESCRIPTION from COST_ZONE_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ZONE_GROUP_ID = use_this.ZONE_GROUP_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table COUNTRY
merge into COUNTRY base using
(select COUNTRY_ID, COUNTRY_DESC from COUNTRY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED then update set base.COUNTRY_DESC = use_this.COUNTRY_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CURRENCIES
merge into CURRENCIES base using
(select CURRENCY_CODE, CURRENCY_DESC from CURRENCIES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CURRENCY_CODE = use_this.CURRENCY_CODE)
WHEN MATCHED then update set base.CURRENCY_DESC = use_this.CURRENCY_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CUSTOMER_SEGMENTS
merge into CUSTOMER_SEGMENTS base using
(select CUSTOMER_SEGMENT_ID, CUSTOMER_SEGMENT_DESC from CUSTOMER_SEGMENTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CUSTOMER_SEGMENT_ID = use_this.CUSTOMER_SEGMENT_ID)
WHEN MATCHED then update set base.CUSTOMER_SEGMENT_DESC = use_this.CUSTOMER_SEGMENT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CUSTOMER_SEGMENT_TYPES
merge into CUSTOMER_SEGMENT_TYPES base using
(select CUSTOMER_SEGMENT_TYPE, CUSTOMER_SEGMENT_TYPE_DESC from CUSTOMER_SEGMENT_TYPES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CUSTOMER_SEGMENT_TYPE = use_this.CUSTOMER_SEGMENT_TYPE)
WHEN MATCHED then update set base.CUSTOMER_SEGMENT_TYPE_DESC = use_this.CUSTOMER_SEGMENT_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table CVB_HEAD
merge into CVB_HEAD base using
(select CVB_CODE, CVB_DESC from CVB_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED then update set base.CVB_DESC = use_this.CVB_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DEAL_ATTRIB_DEF
merge into DEAL_ATTRIB_DEF base using
(select DEAL_ATTRIB_TYPE_ID, DEAL_ATTRIB_DESC from DEAL_ATTRIB_DEF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DEAL_ATTRIB_TYPE_ID = use_this.DEAL_ATTRIB_TYPE_ID)
WHEN MATCHED then update set base.DEAL_ATTRIB_DESC = use_this.DEAL_ATTRIB_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DELIVERY_SLOT
merge into DELIVERY_SLOT base using
(select DELIVERY_SLOT_ID, DELIVERY_SLOT_DESC from DELIVERY_SLOT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DELIVERY_SLOT_ID = use_this.DELIVERY_SLOT_ID)
WHEN MATCHED then update set base.DELIVERY_SLOT_DESC = use_this.DELIVERY_SLOT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DEPS
merge into DEPS base using
(select DEPT, DEPT_NAME from DEPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DEPT = use_this.DEPT)
WHEN MATCHED then update set base.DEPT_NAME = use_this.DEPT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_GROUP_HEAD
merge into DIFF_GROUP_HEAD base using
(select DIFF_GROUP_ID, DIFF_GROUP_DESC from DIFF_GROUP_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIFF_GROUP_ID = use_this.DIFF_GROUP_ID)
WHEN MATCHED then update set base.DIFF_GROUP_DESC = use_this.DIFF_GROUP_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_IDS
merge into DIFF_IDS base using
(select DIFF_ID, DIFF_DESC from DIFF_IDS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIFF_ID = use_this.DIFF_ID)
WHEN MATCHED then update set base.DIFF_DESC = use_this.DIFF_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_RANGE_HEAD
merge into DIFF_RANGE_HEAD base using
(select DIFF_RANGE, DIFF_RANGE_DESC from DIFF_RANGE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIFF_RANGE = use_this.DIFF_RANGE)
WHEN MATCHED then update set base.DIFF_RANGE_DESC = use_this.DIFF_RANGE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_RATIO_HEAD
merge into DIFF_RATIO_HEAD base using
(select DIFF_RATIO_ID, DESCRIPTION from DIFF_RATIO_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIFF_RATIO_ID = use_this.DIFF_RATIO_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIFF_TYPE
merge into DIFF_TYPE base using
(select DIFF_TYPE, DIFF_TYPE_DESC from DIFF_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIFF_TYPE = use_this.DIFF_TYPE)
WHEN MATCHED then update set base.DIFF_TYPE_DESC = use_this.DIFF_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DISTRICT
merge into DISTRICT base using
(select DISTRICT, DISTRICT_NAME from DISTRICT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DISTRICT = use_this.DISTRICT)
WHEN MATCHED then update set base.DISTRICT_NAME = use_this.DISTRICT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DIVISION
merge into DIVISION base using
(select DIVISION, DIV_NAME from DIVISION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DIVISION = use_this.DIVISION)
WHEN MATCHED then update set base.DIV_NAME = use_this.DIV_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DOC
merge into DOC base using
(select DOC_ID, DOC_DESC from DOC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DOC_ID = use_this.DOC_ID)
WHEN MATCHED then update set base.DOC_DESC = use_this.DOC_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table DYNAMIC_HIER_TOKEN_MAP
merge into DYNAMIC_HIER_TOKEN_MAP base using
(select TOKEN, RMS_NAME, CLIENT_NAME from DYNAMIC_HIER_TOKEN_MAP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TOKEN = use_this.TOKEN)
WHEN MATCHED then update set base.RMS_NAME = use_this.RMS_NAME, base.CLIENT_NAME = use_this.CLIENT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ELC_COMP
merge into ELC_COMP base using
(select COMP_ID, COMP_DESC from ELC_COMP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED then update set base.COMP_DESC = use_this.COMP_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ENTRY_STATUS
merge into ENTRY_STATUS base using
(select ENTRY_STATUS, IMPORT_COUNTRY_ID, ENTRY_STATUS_DESC from ENTRY_STATUS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ENTRY_STATUS = use_this.ENTRY_STATUS and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set base.ENTRY_STATUS_DESC = use_this.ENTRY_STATUS_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ENTRY_TYPE
merge into ENTRY_TYPE base using
(select ENTRY_TYPE, IMPORT_COUNTRY_ID, ENTRY_TYPE_DESC from ENTRY_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ENTRY_TYPE = use_this.ENTRY_TYPE and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set base.ENTRY_TYPE_DESC = use_this.ENTRY_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table GROUPS
merge into GROUPS base using
(select GROUP_NO, GROUP_NAME from GROUPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.GROUP_NO = use_this.GROUP_NO)
WHEN MATCHED then update set base.GROUP_NAME = use_this.GROUP_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HALF
merge into HALF base using
(select HALF_NO, HALF_NAME from HALF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.HALF_NO = use_this.HALF_NO)
WHEN MATCHED then update set base.HALF_NAME = use_this.HALF_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS
merge into HTS base using
(select HTS, IMPORT_COUNTRY_ID, EFFECT_FROM, EFFECT_TO, HTS_DESC from HTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.HTS = use_this.HTS and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID and base.EFFECT_FROM = use_this.EFFECT_FROM and base.EFFECT_TO = use_this.EFFECT_TO)
WHEN MATCHED then update set base.HTS_DESC = use_this.HTS_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS_CHAPTER
merge into HTS_CHAPTER base using
(select CHAPTER, IMPORT_COUNTRY_ID, CHAPTER_DESC from HTS_CHAPTER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CHAPTER = use_this.CHAPTER and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set base.CHAPTER_DESC = use_this.CHAPTER_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table HTS_CHAPTER_RESTRAINTS
merge into HTS_CHAPTER_RESTRAINTS base using
(select CHAPTER, IMPORT_COUNTRY_ID, ORIGIN_COUNTRY_ID, RESTRAINT_TYPE, RESTRAINT_DESC from HTS_CHAPTER_RESTRAINTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CHAPTER = use_this.CHAPTER and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID and base.ORIGIN_COUNTRY_ID = use_this.ORIGIN_COUNTRY_ID and base.RESTRAINT_TYPE = use_this.RESTRAINT_TYPE)
WHEN MATCHED then update set base.RESTRAINT_DESC = use_this.RESTRAINT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_IMAGE
merge into ITEM_IMAGE base using
(select ITEM, IMAGE_NAME, IMAGE_DESC from ITEM_IMAGE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ITEM = use_this.ITEM and base.IMAGE_NAME = use_this.IMAGE_NAME)
WHEN MATCHED then update set base.IMAGE_DESC = use_this.IMAGE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_MASTER
merge into ITEM_MASTER base using
(select ITEM, ITEM_DESC, ITEM_DESC_SECONDARY, SHORT_DESC from ITEM_MASTER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ITEM = use_this.ITEM)
WHEN MATCHED then update set base.ITEM_DESC = use_this.ITEM_DESC, base.ITEM_DESC_SECONDARY = use_this.ITEM_DESC_SECONDARY, base.SHORT_DESC = use_this.SHORT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_SUPPLIER
merge into ITEM_SUPPLIER base using
(select ITEM, SUPPLIER, SUPP_LABEL, SUPP_DIFF_1, SUPP_DIFF_2, SUPP_DIFF_3, SUPP_DIFF_4 from ITEM_SUPPLIER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ITEM = use_this.ITEM and base.SUPPLIER = use_this.SUPPLIER)
WHEN MATCHED then update set base.SUPP_LABEL = use_this.SUPP_LABEL, base.SUPP_DIFF_1 = use_this.SUPP_DIFF_1, base.SUPP_DIFF_2 = use_this.SUPP_DIFF_2, base.SUPP_DIFF_3 = use_this.SUPP_DIFF_3,
base.SUPP_DIFF_4 = use_this.SUPP_DIFF_4;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ITEM_XFORM_HEAD
merge into ITEM_XFORM_HEAD base using
(select ITEM_XFORM_HEAD_ID, ITEM_XFORM_DESC from ITEM_XFORM_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ITEM_XFORM_HEAD_ID = use_this.ITEM_XFORM_HEAD_ID)
WHEN MATCHED then update set base.ITEM_XFORM_DESC = use_this.ITEM_XFORM_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LANG
merge into LANG base using
(select LANG_LANG, DESCRIPTION from LANG_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.LANG = use_this.LANG_LANG)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOCATION_CLOSED
merge into LOCATION_CLOSED base using
(select LOCATION, CLOSE_DATE, REASON from LOCATION_CLOSED_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.LOCATION = use_this.LOCATION and base.CLOSE_DATE = use_this.CLOSE_DATE)
WHEN MATCHED then update set base.REASON = use_this.REASON;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOC_LIST_HEAD
merge into LOC_LIST_HEAD base using
(select LOC_LIST, LOC_LIST_DESC from LOC_LIST_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.LOC_LIST = use_this.LOC_LIST)
WHEN MATCHED then update set base.LOC_LIST_DESC = use_this.LOC_LIST_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table LOC_TRAITS
merge into LOC_TRAITS base using
(select LOC_TRAIT, DESCRIPTION from LOC_TRAITS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.LOC_TRAIT = use_this.LOC_TRAIT)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table MC_REJECTION_REASONS
merge into MC_REJECTION_REASONS base using
(select REASON_KEY, REJECTION_REASON from MC_REJECTION_REASONS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED then update set base.REJECTION_REASON = use_this.REJECTION_REASON;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table OGA
merge into OGA base using
(select OGA_CODE, OGA_DESC from OGA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.OGA_CODE = use_this.OGA_CODE)
WHEN MATCHED then update set base.OGA_DESC = use_this.OGA_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table ORG_UNIT
merge into ORG_UNIT base using
(select ORG_UNIT_ID, DESCRIPTION from ORG_UNIT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ORG_UNIT_ID = use_this.ORG_UNIT_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table OUTLOC
merge into OUTLOC base using
(select OUTLOC_TYPE, OUTLOC_ID, OUTLOC_DESC, OUTLOC_ADD1, OUTLOC_ADD2, OUTLOC_CITY, CONTACT_NAME, OUTLOC_NAME_SECONDARY from OUTLOC_TL where lang = (select data_integration_lang from
SYSTEM_CONFIG_OPTIONS)) use_this
on (base.OUTLOC_TYPE = use_this.OUTLOC_TYPE and base.OUTLOC_ID = use_this.OUTLOC_ID)
WHEN MATCHED then update set base.OUTLOC_DESC = use_this.OUTLOC_DESC, base.OUTLOC_ADD1 = use_this.OUTLOC_ADD1, base.OUTLOC_ADD2 = use_this.OUTLOC_ADD2, base.OUTLOC_CITY = use_this.OUTLOC_CITY,
base.CONTACT_NAME = use_this.CONTACT_NAME, base.OUTLOC_NAME_SECONDARY = use_this.OUTLOC_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PACK_TMPL_HEAD
merge into PACK_TMPL_HEAD base using
(select PACK_TMPL_ID, PACK_TMPL_DESC from PACK_TMPL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.PACK_TMPL_ID = use_this.PACK_TMPL_ID)
WHEN MATCHED then update set base.PACK_TMPL_DESC = use_this.PACK_TMPL_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PARTNER
merge into PARTNER base using
(select PARTNER_TYPE, PARTNER_ID, PARTNER_DESC, PARTNER_NAME_SECONDARY from PARTNER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.PARTNER_TYPE = use_this.PARTNER_TYPE and base.PARTNER_ID = use_this.PARTNER_ID)
WHEN MATCHED then update set base.PARTNER_DESC = use_this.PARTNER_DESC, base.PARTNER_NAME_SECONDARY = use_this.PARTNER_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PEND_MERCH_HIER
merge into PEND_MERCH_HIER base using
(select HIER_TYPE, MERCH_HIER_ID, MERCH_HIER_PARENT_ID, MERCH_HIER_GRANDPARENT_ID, MERCH_HIER_NAME from PEND_MERCH_HIER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS))
use_this
on (base.HIER_TYPE = use_this.HIER_TYPE and base.MERCH_HIER_ID = use_this.MERCH_HIER_ID and base.MERCH_HIER_PARENT_ID = use_this.MERCH_HIER_PARENT_ID and base.MERCH_HIER_GRANDPARENT_ID =
use_this.MERCH_HIER_GRANDPARENT_ID)
WHEN MATCHED then update set base.MERCH_HIER_NAME = use_this.MERCH_HIER_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PHASES
merge into PHASES base using
(select SEASON_ID, PHASE_ID, PHASE_DESC from PHASES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SEASON_ID = use_this.SEASON_ID and base.PHASE_ID = use_this.PHASE_ID)
WHEN MATCHED then update set base.PHASE_DESC = use_this.PHASE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table POS_COUPON_HEAD
merge into POS_COUPON_HEAD base using
(select COUPON_ID, COUPON_DESC from POS_COUPON_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.COUPON_ID = use_this.COUPON_ID)
WHEN MATCHED then update set base.COUPON_DESC = use_this.COUPON_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table POS_TENDER_TYPE_HEAD
merge into POS_TENDER_TYPE_HEAD base using
(select TENDER_TYPE_ID, TENDER_TYPE_DESC from POS_TENDER_TYPE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TENDER_TYPE_ID = use_this.TENDER_TYPE_ID)
WHEN MATCHED then update set base.TENDER_TYPE_DESC = use_this.TENDER_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PO_TYPE
merge into PO_TYPE base using
(select PO_TYPE, PO_TYPE_DESC from PO_TYPE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.PO_TYPE = use_this.PO_TYPE)
WHEN MATCHED then update set base.PO_TYPE_DESC = use_this.PO_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table PRIORITY_GROUP
merge into PRIORITY_GROUP base using
(select PRIORITY_GROUP_ID, PRIORITY_GROUP_DESC from PRIORITY_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.PRIORITY_GROUP_ID = use_this.PRIORITY_GROUP_ID)
WHEN MATCHED then update set base.PRIORITY_GROUP_DESC = use_this.PRIORITY_GROUP_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table QUOTA_CATEGORY
merge into QUOTA_CATEGORY base using
(select QUOTA_CAT, IMPORT_COUNTRY_ID, CATEGORY_DESC from QUOTA_CATEGORY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.QUOTA_CAT = use_this.QUOTA_CAT and base.IMPORT_COUNTRY_ID = use_this.IMPORT_COUNTRY_ID)
WHEN MATCHED then update set base.CATEGORY_DESC = use_this.CATEGORY_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RECLASS_HEAD
merge into RECLASS_HEAD base using
(select RECLASS_NO, RECLASS_DESC from RECLASS_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.RECLASS_NO = use_this.RECLASS_NO)
WHEN MATCHED then update set base.RECLASS_DESC = use_this.RECLASS_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table REGION
merge into REGION base using
(select REGION, REGION_NAME from REGION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.REGION = use_this.REGION)
WHEN MATCHED then update set base.REGION_NAME = use_this.REGION_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RELATED_ITEM_HEAD
merge into RELATED_ITEM_HEAD base using
(select RELATIONSHIP_ID, RELATIONSHIP_NAME from RELATED_ITEM_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.RELATIONSHIP_ID = use_this.RELATIONSHIP_ID)
WHEN MATCHED then update set base.RELATIONSHIP_NAME = use_this.RELATIONSHIP_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RESTART_CONTROL
merge into RESTART_CONTROL base using
(select PROGRAM_NAME, PROGRAM_DESC from RESTART_CONTROL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED then update set base.PROGRAM_DESC = use_this.PROGRAM_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table RTK_ERRORS
merge into RTK_ERRORS base using
(select RTK_KEY, RTK_TEXT from RTK_ERRORS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.RTK_KEY = use_this.RTK_KEY)
WHEN MATCHED then update set base.RTK_TEXT = use_this.RTK_TEXT;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_CONSTANTS
merge into SA_CONSTANTS base using
(select CONSTANT_ID, CONSTANT_NAME from SA_CONSTANTS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CONSTANT_ID = use_this.CONSTANT_ID)
WHEN MATCHED then update set base.CONSTANT_NAME = use_this.CONSTANT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_ERROR_CODES
merge into SA_ERROR_CODES base using
(select ERROR_CODE, ERROR_DESC, REC_SOLUTION, SHORT_DESC from SA_ERROR_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED then update set base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_ROUNDING_RULE_HEAD
merge into SA_ROUNDING_RULE_HEAD base using
(select ROUNDING_RULE_ID, ROUNDING_RULE_NAME from SA_ROUNDING_RULE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ROUNDING_RULE_ID = use_this.ROUNDING_RULE_ID)
WHEN MATCHED then update set base.ROUNDING_RULE_NAME = use_this.ROUNDING_RULE_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_RULE_HEAD
merge into SA_RULE_HEAD base using
(select RULE_ID, RULE_REV_NO, RULE_NAME from SA_RULE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.RULE_ID = use_this.RULE_ID and base.RULE_REV_NO = use_this.RULE_REV_NO)
WHEN MATCHED then update set base.RULE_NAME = use_this.RULE_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SA_TOTAL_HEAD
merge into SA_TOTAL_HEAD base using
(select TOTAL_ID, TOTAL_REV_NO, TOTAL_DESC from SA_TOTAL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TOTAL_ID = use_this.TOTAL_ID and base.TOTAL_REV_NO = use_this.TOTAL_REV_NO)
WHEN MATCHED then update set base.TOTAL_DESC = use_this.TOTAL_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SCAC
merge into SCAC base using
(select SCAC_CODE, SCAC_CODE_DESC from SCAC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SCAC_CODE = use_this.SCAC_CODE)
WHEN MATCHED then update set base.SCAC_CODE_DESC = use_this.SCAC_CODE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SEASONS
merge into SEASONS base using
(select SEASON_ID, SEASON_DESC from SEASONS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SEASON_ID = use_this.SEASON_ID)
WHEN MATCHED then update set base.SEASON_DESC = use_this.SEASON_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SEC_GROUP
merge into SEC_GROUP base using
(select GROUP_ID, GROUP_NAME from SEC_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.GROUP_ID = use_this.GROUP_ID)
WHEN MATCHED then update set base.GROUP_NAME = use_this.GROUP_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SKULIST_HEAD
merge into SKULIST_HEAD base using
(select SKULIST, SKULIST_DESC from SKULIST_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SKULIST = use_this.SKULIST)
WHEN MATCHED then update set base.SKULIST_DESC = use_this.SKULIST_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STATE
merge into STATE base using
(select STATE, COUNTRY_ID, DESCRIPTION from STATE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.STATE = use_this.STATE and base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE
merge into STORE base using
(select STORE, STORE_NAME, STORE_NAME_SECONDARY from STORE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.STORE = use_this.STORE)
WHEN MATCHED then update set base.STORE_NAME = use_this.STORE_NAME, base.STORE_NAME_SECONDARY = use_this.STORE_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_ADD
merge into STORE_ADD base using
(select STORE, STORE_NAME, STORE_NAME_SECONDARY from STORE_ADD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.STORE = use_this.STORE)
WHEN MATCHED then update set base.STORE_NAME = use_this.STORE_NAME, base.STORE_NAME_SECONDARY = use_this.STORE_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_FORMAT
merge into STORE_FORMAT base using
(select STORE_FORMAT, FORMAT_NAME from STORE_FORMAT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.STORE_FORMAT = use_this.STORE_FORMAT)
WHEN MATCHED then update set base.FORMAT_NAME = use_this.FORMAT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table STORE_GRADE_GROUP
merge into STORE_GRADE_GROUP base using
(select STORE_GRADE_GROUP_ID, STORE_GRADE_GROUP_DESC from STORE_GRADE_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.STORE_GRADE_GROUP_ID = use_this.STORE_GRADE_GROUP_ID)
WHEN MATCHED then update set base.STORE_GRADE_GROUP_DESC = use_this.STORE_GRADE_GROUP_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUBCLASS
merge into SUBCLASS base using
(select DEPT, CLASS, SUBCLASS, SUB_NAME from SUBCLASS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.DEPT = use_this.DEPT and base.CLASS = use_this.CLASS and base.SUBCLASS = use_this.SUBCLASS)
WHEN MATCHED then update set base.SUB_NAME = use_this.SUB_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUPS
merge into SUPS base using
(select SUPPLIER, SUP_NAME, SUP_NAME_SECONDARY, CONTACT_NAME from SUPS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SUPPLIER = use_this.SUPPLIER)
WHEN MATCHED then update set base.SUP_NAME = use_this.SUP_NAME, base.SUP_NAME_SECONDARY = use_this.SUP_NAME_SECONDARY, base.CONTACT_NAME = use_this.CONTACT_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUPS_PACK_TMPL_DESC
merge into SUPS_PACK_TMPL_DESC base using
(select SUPPLIER, PACK_TMPL_ID, SUPP_PACK_DESC from SUPS_PACK_TMPL_DESC_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SUPPLIER = use_this.SUPPLIER and base.PACK_TMPL_ID = use_this.PACK_TMPL_ID)
WHEN MATCHED then update set base.SUPP_PACK_DESC = use_this.SUPP_PACK_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table SUP_TRAITS
merge into SUP_TRAITS base using
(select SUP_TRAIT, DESCRIPTION from SUP_TRAITS_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.SUP_TRAIT = use_this.SUP_TRAIT)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TARIFF_TREATMENT
merge into TARIFF_TREATMENT base using
(select TARIFF_TREATMENT, TARIFF_TREATMENT_DESC from TARIFF_TREATMENT_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TARIFF_TREATMENT = use_this.TARIFF_TREATMENT)
WHEN MATCHED then update set base.TARIFF_TREATMENT_DESC = use_this.TARIFF_TREATMENT_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TICKET_TYPE_HEAD
merge into TICKET_TYPE_HEAD base using
(select TICKET_TYPE_ID, TICKET_TYPE_DESC from TICKET_TYPE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TICKET_TYPE_ID = use_this.TICKET_TYPE_ID)
WHEN MATCHED then update set base.TICKET_TYPE_DESC = use_this.TICKET_TYPE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TIMELINE_HEAD
merge into TIMELINE_HEAD base using
(select TIMELINE_NO, TIMELINE_DESC from TIMELINE_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TIMELINE_NO = use_this.TIMELINE_NO)
WHEN MATCHED then update set base.TIMELINE_DESC = use_this.TIMELINE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TIMELINE_STEP_COMP
merge into TIMELINE_STEP_COMP base using
(select TIMELINE_TYPE, STEP_NO, STEP_DESC from TIMELINE_STEP_COMP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TIMELINE_TYPE = use_this.TIMELINE_TYPE and base.STEP_NO = use_this.STEP_NO)
WHEN MATCHED then update set base.STEP_DESC = use_this.STEP_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TRAN_DATA_CODES
merge into TRAN_DATA_CODES base using
(select CODE, DECODE from TRAN_DATA_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.CODE = use_this.CODE)
WHEN MATCHED then update set base.DECODE = use_this.DECODE;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TRAN_DATA_CODES_REF
merge into TRAN_DATA_CODES_REF base using
(select TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC from TRAN_DATA_CODES_REF_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TRAN_CODE = use_this.TRAN_CODE and base.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED then update set base.REF_NO_1_DESC = use_this.REF_NO_1_DESC, base.REF_NO_2_DESC = use_this.REF_NO_2_DESC, base.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TSFZONE
merge into TSFZONE base using
(select TRANSFER_ZONE, DESCRIPTION from TSFZONE_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TRANSFER_ZONE = use_this.TRANSFER_ZONE)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table TSF_ENTITY
merge into TSF_ENTITY base using
(select TSF_ENTITY_ID, TSF_ENTITY_DESC from TSF_ENTITY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TSF_ENTITY_ID = use_this.TSF_ENTITY_ID)
WHEN MATCHED then update set base.TSF_ENTITY_DESC = use_this.TSF_ENTITY_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table UDA
merge into UDA base using
(select UDA_ID, UDA_DESC from UDA_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.UDA_ID = use_this.UDA_ID)
WHEN MATCHED then update set base.UDA_DESC = use_this.UDA_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table UDA_VALUES
merge into UDA_VALUES base using
(select UDA_ID, UDA_VALUE, UDA_VALUE_DESC from UDA_VALUES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.UDA_ID = use_this.UDA_ID and base.UDA_VALUE = use_this.UDA_VALUE)
WHEN MATCHED then update set base.UDA_VALUE_DESC = use_this.UDA_VALUE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table VAT_CODES
merge into VAT_CODES base using
(select VAT_CODE, VAT_CODE_DESC from VAT_CODES_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED then update set base.VAT_CODE_DESC = use_this.VAT_CODE_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table VAT_REGION
merge into VAT_REGION base using
(select VAT_REGION, VAT_REGION_NAME from VAT_REGION_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.VAT_REGION = use_this.VAT_REGION)
WHEN MATCHED then update set base.VAT_REGION_NAME = use_this.VAT_REGION_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_COST_BUILDUP_TMPL_DETAIL
merge into WF_COST_BUILDUP_TMPL_DETAIL base using
(select TEMPL_ID, COST_COMP_ID, DESCRIPTION from WF_COST_BUILDUP_TMPL_DTL_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TEMPL_ID = use_this.TEMPL_ID and base.COST_COMP_ID = use_this.COST_COMP_ID)
WHEN MATCHED then update set base.DESCRIPTION = use_this.DESCRIPTION;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_COST_BUILDUP_TMPL_HEAD
merge into WF_COST_BUILDUP_TMPL_HEAD base using
(select TEMPL_ID, TEMPL_DESC from WF_COST_BUILDUP_TMPL_HD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.TEMPL_ID = use_this.TEMPL_ID)
WHEN MATCHED then update set base.TEMPL_DESC = use_this.TEMPL_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_CUSTOMER
merge into WF_CUSTOMER base using
(select WF_CUSTOMER_ID, WF_CUSTOMER_NAME from WF_CUSTOMER_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.WF_CUSTOMER_ID = use_this.WF_CUSTOMER_ID)
WHEN MATCHED then update set base.WF_CUSTOMER_NAME = use_this.WF_CUSTOMER_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WF_CUSTOMER_GROUP
merge into WF_CUSTOMER_GROUP base using
(select WF_CUSTOMER_GROUP_ID, WF_CUSTOMER_GROUP_NAME from WF_CUSTOMER_GROUP_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.WF_CUSTOMER_GROUP_ID = use_this.WF_CUSTOMER_GROUP_ID)
WHEN MATCHED then update set base.WF_CUSTOMER_GROUP_NAME = use_this.WF_CUSTOMER_GROUP_NAME;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WH
merge into WH base using
(select WH, WH_NAME, WH_NAME_SECONDARY from WH_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.WH = use_this.WH)
WHEN MATCHED then update set base.WH_NAME = use_this.WH_NAME, base.WH_NAME_SECONDARY = use_this.WH_NAME_SECONDARY;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WO_ACTIVITY
merge into WO_ACTIVITY base using
(select ACTIVITY_ID, ACTIVITY_DESC from WO_ACTIVITY_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.ACTIVITY_ID = use_this.ACTIVITY_ID)
WHEN MATCHED then update set base.ACTIVITY_DESC = use_this.ACTIVITY_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
PROMPT Updating Table WO_TMPL_HEAD
merge into WO_TMPL_HEAD base using
(select WO_TMPL_ID, WO_TMPL_DESC from WO_TMPL_HEAD_TL where lang = (select data_integration_lang from SYSTEM_CONFIG_OPTIONS)) use_this
on (base.WO_TMPL_ID = use_this.WO_TMPL_ID)
WHEN MATCHED then update set base.WO_TMPL_DESC = use_this.WO_TMPL_DESC;
commit;
--------------------------------------------------------------------------------------------------------------------------------------
set define "&";