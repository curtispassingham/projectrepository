SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO NON_MERCH_CODE_HEAD_TL TL USING
(SELECT  NON_MERCH_CODE,  LANG,  NON_MERCH_CODE_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 'M' NON_MERCH_CODE, 31 LANG, 'Övrigt' NON_MERCH_CODE_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM NON_MERCH_CODE_HEAD base where dl.NON_MERCH_CODE = base.NON_MERCH_CODE)) USE_THIS
ON ( tl.NON_MERCH_CODE = use_this.NON_MERCH_CODE and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.NON_MERCH_CODE_DESC = use_this.NON_MERCH_CODE_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (NON_MERCH_CODE, LANG, NON_MERCH_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.NON_MERCH_CODE, use_this.LANG, use_this.NON_MERCH_CODE_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
