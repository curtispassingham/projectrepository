SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 1 LANG_LANG, 'Engelska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 2 LANG_LANG, 'Tyska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 3 LANG_LANG, 'Franska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 4 LANG_LANG, 'Spanska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 5 LANG_LANG, 'Japanska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 6 LANG_LANG, 'Koreanska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 7 LANG_LANG, 'Ryska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 8 LANG_LANG, 'Förenklad kinesiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 9 LANG_LANG, 'Turkiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 10 LANG_LANG, 'Ungerska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 11 LANG_LANG, 'Traditionell kinesiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 12 LANG_LANG, 'Portugisiska (Brasilien)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 13 LANG_LANG, 'Arabiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 14 LANG_LANG, 'Kanadensisk franska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 15 LANG_LANG, 'Kroatiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 16 LANG_LANG, 'Tjeckiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 17 LANG_LANG, 'Danska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 18 LANG_LANG, 'Nederländska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 19 LANG_LANG, 'Finska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 20 LANG_LANG, 'Grekiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 21 LANG_LANG, 'Hebreiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 22 LANG_LANG, 'Italienska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 23 LANG_LANG, 'Latinamerikansk spanska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 24 LANG_LANG, 'Litauiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 25 LANG_LANG, 'Norska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 26 LANG_LANG, 'Polska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 27 LANG_LANG, 'Portugisiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 28 LANG_LANG, 'Rumänska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 29 LANG_LANG, 'Slovakiska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 30 LANG_LANG, 'Slovenska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 31 LANG_LANG, 'Svenska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 32 LANG_LANG, 'Thailändska' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 31 LANG, 33 LANG_LANG, 'Tagalog' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO LANG base USING
( SELECT LANG_LANG LANG, DESCRIPTION DESCRIPTION FROM LANG_TL TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM LANG_TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
