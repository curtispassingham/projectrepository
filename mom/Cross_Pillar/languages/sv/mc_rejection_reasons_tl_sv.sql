SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'CANNOT_CHANGE_REPL_WH' REASON_KEY, 'Lagerstatusen kan inte ändras eftersom lagret används som källager för påfyllnad för denna lagerenhet.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'CONSIGNMENT' REASON_KEY, 'Kan inte omklassificera en artikel mellan konsignationsavdelning %s1 och icke-konsignationsavdelning. Gammal avdelning: %s1 Ny avdelning: %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'COST_LOC_PENDING' REASON_KEY, 'Ändringen av kostnadsplatsen kan inte bearbetas för artikel %s1 och plats %s2, eftersom det finns en avvaktande ändring av kostnadsplatsen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'FORECAST_REPL_METHOD' REASON_KEY, 'Platserna för den här artikeln använder en prognosbaserad påfyllnadsmetod, och artikeln kan därför inte ändras till ej prognostiserad.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_ACTIVATE_DATE' REASON_KEY, 'Det nya aktiveringsdatumet %s1 infaller senare än det aktuella inaktiveringsdatumet %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_DEACTIVATE_DATE' REASON_KEY, 'Det nya inaktiveringsdatumet %s1 infaller tidigare än det aktuella aktiveringsdatumet %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_MAX_STOCK' REASON_KEY, 'Det nya maxlagret på %s1 enheter är mindre än det aktuella minimilagret på %s2 enheter.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_MAX_SUPPLY_DAYS' REASON_KEY, 'Det nya högsta antalet dagar för täcktid på %s1 är lägre än det aktuella lägsta antalet dagar för täcktid på %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_MIN_STOCK' REASON_KEY, 'Det nya minimilagret på %s1 enheter är större än det aktuella maxlagret på %s2 enheter.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_MIN_SUPPLY_DAYS' REASON_KEY, 'Det nya lägsta antalet dagar för täcktid på %s1 är högre än det aktuella högsta antalet dagar för täcktid på %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_MULT_RUNS_PER_DAY' REASON_KEY, 'Indikatorn för flera körningar per dag kan enbart vara J när påfyllnadsmetoden är Butiksorder, granskningscykeln är Daglig och lagerkategorin är Lagerhållen eller Lagerlänk.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_REPL_ITEM_TYPE' REASON_KEY, 'Påfyllnadsattributen kan endast uppdateras för lagerenhet för stapelvaror, mode och modegenrer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_REPL_ORDER_CTRL' REASON_KEY, 'Kan inte uppdatera lagerkategorin till %s1 när platsen som ska fyllas på är en butik och den aktuella orderkontrollen är inköpararbetsblad.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_SOURCING_WH' REASON_KEY, 'Artikeln finns inte i det nya källagret.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_STORE_CLOSE_DATE' REASON_KEY, 'Butikens nya stängningsdatum %s1 infaller tidigare än det aktuella öppningsdatumet %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_STORE_OPEN_DATE' REASON_KEY, 'Butikens nya öppningsdatum %s1 infaller senare än det aktuella stängningsdatumet %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'INVALID_TSF_ZERO_SOH_IND' REASON_KEY, 'Indikatorn för noll överföring av lagersaldo kan enbart vara J när påfyllnadsmetoden är Butiksorder och lagerkategorin är Lagerhållen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_IN_GROUP_ON_ORDER' REASON_KEY, 'En artikel från den här artikelgruppen finns på aktiv order. Därför går det inte att ändra avd./klass/delklass för några artiklar i gruppen' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_IN_USE' REASON_KEY, 'Artikeln är en del av en aktiv order, överföring eller prisändring. Statusen för den kan därför inte ändras.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_LOC_NOT_ACTIVATE' REASON_KEY, 'Artikel %s1 är inte aktiv på plats %s2, och den kan därför inte aktiveras.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_NOT_FORECASTABLE' REASON_KEY, 'Artikeln kan inte prognostiseras. Därför går det inte att använda metoderna för tidsbaserad och dynamisk påfyllnad.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_NOT_RANGE_COST_LOC' REASON_KEY, 'Kostnadsplatsen %s1 finns inte i sortimentet för artikel %s2, enbart butik eller lager som har sortimentet för artikeln kan användas som kostnadsplats.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_NOT_TOLERANCEABLE' REASON_KEY, 'Kan inte uppdatera toleransen om lagerkategorin är lager.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_ON_APP_ORD' REASON_KEY, 'De angivna artiklarna eller deras underordnade/dubbelt underordnade som ska omklassificeras finns i godkända order.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_ON_ORDER_ZONE' REASON_KEY, 'Kunde inte ändra kostnadszonen eftersom artikeln finns på en eller flera order.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_STATUS_DELETE' REASON_KEY, 'Den aktuella artikelstatusen för %s1 %s2 är borttagen. Därför går det inte att ändra statusen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_SUPPLIER' REASON_KEY, 'Leverantör %s1 har inte associerats med artikeln.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ITEM_SUPPLIER_COUNTRY' REASON_KEY, 'Leverantör %s1/ursprungsland %s2 har inte associerats med artikeln.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'LAST_PHASE' REASON_KEY, 'Kan inte ta bort. Artikeln har endast en säsong tilldelad.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'MAIN_EXISTS_AS_SUB' REASON_KEY, 'Huvudartikel %s1 får inte tilldelas till platsen %s2, på grund av att den redan är en ersättningsartikel på platsen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'MAIN_ITEM_FORECASTS' REASON_KEY, 'Artikeln är en huvudartikel i ersättningsartikel. Bearbetning och prognoser används. Det går inte att ändra artikeln till ej prognostiserad.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'MAIN_ITEM_LOC_MISMATCH' REASON_KEY, 'Huvudartikel %s1 finns inte på plats %s2. Endast befintliga artikel-/platskombinationer får användas i dialogen Ersättningsartiklar.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NOT_LIST_SKU_STYLE_S' REASON_KEY, 'Attributen kan endast uppdateras för lagerenheter för stapelvaror och mode samt modegenrer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NOT_LIST_SKU_STYLE_W' REASON_KEY, 'Attributen kan endast uppdateras för lagerenheter för stapelvaror och mode samt modegenrer.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NOT_ON_REPL_NO_DEACTIVATE' REASON_KEY, 'Artikelplatsen är inte under påfyllnad och kan därför inte avaktiveras.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NOT_ON_REPL_NO_UPDATE' REASON_KEY, 'Artikelplatsen är inte under påfyllnad och det går därför inte att uppdatera attributen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NOT_SKU_STYLE' REASON_KEY, 'Attribut för artikelindikator finns bara på nivåerna stapellagerenhet och modegenre.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_ASSOC_UDA_VALUE' REASON_KEY, 'Artikelns nya hierarki %s1/%s2/%s3 har användardefinierade standardattribut utan ett associerat värde för anv.def. standardattr.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_BUYER_WKSHT' REASON_KEY, 'Kan inte uppdatera orderkontrollen till inköpararbetsblad när den aktuella lagerkategorin är %s1 och platsen för påfyllnad är en butik.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_DOMAIN_EXISTS' REASON_KEY, 'Artikelns varuhierarki har inte associerats med en domän, och det går därför inte att ge artikeln värdet Kan prognostiseras.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_REQUIRED_UDA' REASON_KEY, 'Artikel %s1 måste associeras med anv.def. attr. %s2. Att flytta posten innebär en överträdelse av detta villkor.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_UPDATE_CHILD_COST_ZONE' REASON_KEY, 'Kan inte uppdatera kostnadszongruppen för artikeln eftersom artikeln är underordnad och överordnade artiklar måste ha samma kostnadszon. Om de överordnade artiklarna är en del av uppdateringen kan artikeln ändå ha uppdaterats.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_UPD_EXIST_VAT' REASON_KEY, 'Lagerenhet %s1 har redan en momslagerenhetspost för denna momsregion och aktivt datum med momstypen C eller R. Det går inte att uppdatera till B.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'NO_UPD_VAT_TYPE' REASON_KEY, 'Lagerenhet %s1 har en momslagerenhetspost med momstypen B för den här momsregionen samt aktivt datum. Det går inte att uppdatera till C eller R.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ON_CLEAR_NO_ACTIVATE' REASON_KEY, 'Artikelplatsen är i utförsäljning, och det går därför inte att aktivera den.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ON_CONSIGN_NO_ACTIVATE' REASON_KEY, 'Artikelplatsen är i konsignation, och det går därför inte att aktivera den.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'ON_REPL_NO_ACTIVATE' REASON_KEY, 'Artikelplatsen är redan under påfyllnad, och det går därför inte att aktivera den.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'PACK_FORECAST' REASON_KEY, 'Artikeln är en packartikel som inte går att prognostisera.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'PACK_ITEM_LOC_MISMATCH' REASON_KEY, 'Primärt påfyllnadspack %s1 finns inte på plats %s2. Endast befintliga artikel-/platskombinationer får användas i dialogen Ersättningsartiklar.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'PACK_LOC_NO_EXIST' REASON_KEY, 'Primärt påfyllnadspack %s1 finns inte på plats %s2 i aktiv status.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'PACK_STATUS' REASON_KEY, 'Primärt påfyllnadspack %s1 är inte giltigt eftersom det inte har samma status som sin komponentartikel %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'RECLASS_EXIST' REASON_KEY, 'Artikeln finns i en annan avvaktande omklassificering.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'RECLASS_NO_DOMAIN' REASON_KEY, 'Artikeln kan prognostiseras. Den nya varuhierarkin för avdelning %s1, klass %s2 och delklass %s3 har inte associerats med en domän och det går därför inte att omklassificera artikeln.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'REJECT_DEPT_SIZE' REASON_KEY, 'Formatet kan inte omklassificeras eftersom minst en av de nödvändiga storlekarna fattas i den nya avdelningen %s1.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'REJECT_ITEM_LEVEL' REASON_KEY, 'Det går endast att omklassificera artiklar på nivå 1. Artikel %s1 är en nivå-%s2-artikel.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'REJECT_ITEM_STATUS' REASON_KEY, 'Artikelns finns på en aktiv order och det går därför inte att ändra avdelning.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SIMPLE_PACK' REASON_KEY, 'Kan inte omklassificera enkelt pack %s1. Den kan endast omklassificeras tillsammans med sin komponentartikel.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SINGLE_UDA_EXISTS' REASON_KEY, 'Artikel %s1 har redan associerats med anv.def. attr. %s2, och denna anv.def. attr. tillåter endast en associering per artikel.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SKU_IN_ACTIVE_PACK' REASON_KEY, 'Den här artikeln ingår i ett aktivt pack. Kan inte ändra dess status.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SKU_ORD_EXIST' REASON_KEY, 'Artikeln har aktiva order. Det går inte att ändra statusen för den' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SKU_TSF_EXIST' REASON_KEY, 'Artikeln har aktiv överföring. Det går inte att ändra statusen för den' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SOURCE_WH_NO_EXIST' REASON_KEY, 'Antingen har artikeln inte associerats med lager %s1 eller så är den inte aktiv där. Artikeln kan därför inte användas som källager.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'STOCK_COUNT_PEND' REASON_KEY, 'Artikeln finns för närvarande inte i lagerinventeringen och kan därför inte omklassificeras just nu.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'STYLE_SKU_ON_ORDER' REASON_KEY, 'Kunde inte ändra kostnadszonen eftersom artikeln är en genre och minst en av lagerenheterna finns på en eller flera order.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUB_EXISTS_AS_MAIN' REASON_KEY, 'Ersättningsartikel %s1 får inte tilldelas till platsen %s2, på grund av att den redan är en huvudartikel på platsen.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUB_ITEM_FORECASTS' REASON_KEY, 'Artikeln är en ersättningsartikel och dess prognoser används. Artikeln går inte att ändra till Ej prognostiserbar.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUB_ITEM_LOC_MISMATCH' REASON_KEY, 'Ersättningsartikel %s1 finns inte på plats %s2. Endast befintliga artikel-/platskombinationer får användas i dialogen Ersättningsartiklar.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUB_ITEM_NO_ACTIVATE' REASON_KEY, 'Artikeln är en ersättningsartikel och kan därför inte aktiveras.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUPP_NO_EXIST_PACK' REASON_KEY, 'Pack %s1 har inte associerats med leverantör %s2 och ursprungsland %s3.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'SUP_LOC_NOT_SAME_ORG_UNIT' REASON_KEY, 'Kan inte associera leverantör %s1 med plats %s2 eftersom de tillhör olika organisationsenheter.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'UDA_REQD' REASON_KEY, 'Den nya artikelhierarkin har obligatoriska anv.def. attribut som ännu inte har tilldelats till artikeln.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 31 LANG, 'XDOCK_WH_ITEM_NOT_ACTIVE' REASON_KEY, 'Lagerenhet %s1 är inte aktiv i det angivna samdistributionslagret %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO MC_REJECTION_REASONS base USING
( SELECT REASON_KEY REASON_KEY, REJECTION_REASON REJECTION_REASON FROM MC_REJECTION_REASONS_TL TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31)) USE_THIS
ON ( base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE SET base.REJECTION_REASON = use_this.REJECTION_REASON;
--
DELETE FROM MC_REJECTION_REASONS_TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
