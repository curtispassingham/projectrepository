SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'LIKE_STORE' PROGRAM_NAME, 'Bearbeta likn. butik' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'ang_saplgen' PROGRAM_NAME, 'Google - extrahering av POSLog' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'costcalc' PROGRAM_NAME, 'beräkna avtalsrabatter i tabellen FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealact' PROGRAM_NAME, 'Beräknar information om faktiska' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealcls' PROGRAM_NAME, 'avsluta avtal som har nått close_date' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealday' PROGRAM_NAME, 'Summeras till avtalsdag' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealex' PROGRAM_NAME, 'Bryter ned avtalsinformation per artikelplats' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealfct' PROGRAM_NAME, 'Beräknar prognosinformation' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealfinc' PROGRAM_NAME, 'Intäkt från fast avtal till huvudbok' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealinc' PROGRAM_NAME, 'Beräknar intäkt' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dealprg' PROGRAM_NAME, 'rensa avslutade avtal för en period angiven av systemet' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dfrtbld' PROGRAM_NAME, 'infoga poster i tabellen DEAL_SKU_TEMP' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'discotbapply' PROGRAM_NAME, 'Rabatt på budg. inköp tillämpas' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'ditinsrt' PROGRAM_NAME, 'infoga poster i deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dlyprg' PROGRAM_NAME, 'daglig rensning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'docclose' PROGRAM_NAME, 'stäng ej bokade mottagningar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'dsdupld' PROGRAM_NAME, 'ladda upp direktleverans för butik' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'edidlinv' PROGRAM_NAME, 'Ladda ned faktura via EDI' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'elccostcalc' PROGRAM_NAME, 'beräkna ändr. av hemtagningskostn. i tabellen FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'fifinvcu11i' PROGRAM_NAME, 'Ladda upp faktura' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'fifinvcup' PROGRAM_NAME, 'Faktureringsprocess' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'genpreiss' PROGRAM_NAME, 'Generera förhandsutfärdade order' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'hstmthupd' PROGRAM_NAME, 'Uppdatera lgrsaldo, detaljh.pris, avg_cost i item_loc_hist_mth' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'lclrbld' PROGRAM_NAME, 'bygg om batch med platslista' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'lcup798' PROGRAM_NAME, 'neddragningar/avgifter för remburs' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'lcupld' PROGRAM_NAME, 'uppladdning av remburs' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'lifstkup' PROGRAM_NAME, 'konvertering av lageruppladdning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'mrt' PROGRAM_NAME, 'Skapa överföringar från massret.övf' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'mrtprg' PROGRAM_NAME, 'mrtprg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'mrtrtv' PROGRAM_NAME, 'Skapar retur till lev. för massret.övf' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'mrtupd' PROGRAM_NAME, 'mrtupd' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'nwppurge' PROGRAM_NAME, 'Rensning enligt principen för lägst värdering' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'ociroq' PROGRAM_NAME, 'null' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'ordautcl' PROGRAM_NAME, 'Ta bort/stäng order' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'orddscnt' PROGRAM_NAME, 'inköpsorderrabatt' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'prchstprg' PROGRAM_NAME, 'Rensa prishistorik' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'precostcalc' PROGRAM_NAME, 'förberäkna avtalsrabatter i deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'refmvl10nentity' PROGRAM_NAME, 'Uppdatera materialiserad vy för L10N-enhet' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'refmvlocprimaddr' PROGRAM_NAME, 'Uppdatera materialiserad vy för primär platsadress' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saescheat' PROGRAM_NAME, 'ReSA rekv. av ägodelar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpach' PROGRAM_NAME, 'ReSA-export till ACH' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpdw' PROGRAM_NAME, 'Försäljningsgranskningsexport till RDW' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpgl' PROGRAM_NAME, 'ReSA-export till Oracle GL' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpgl107' PROGRAM_NAME, 'ReSA-export till Oracle GL' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpsim' PROGRAM_NAME, 'Försäljningsgranskningsexport till SIM' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saexpuar' PROGRAM_NAME, 'Försäljningsgranskningsexport till UAR' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'sagetref' PROGRAM_NAME, 'ladda ned referensdata för försäljningsgranskning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saimpadj' PROGRAM_NAME, 'ReSA-import av justeringar' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saimptlog' PROGRAM_NAME, 'TLOG-import av försäljningsgranskning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saimptlogfin' PROGRAM_NAME, 'ReSA-slutförande av TLOG-import' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saimptlogi' PROGRAM_NAME, 'TLOG-import av försäljningsgranskning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'saimptlogtdup_upd' PROGRAM_NAME, 'ReSA-uppdatering TDUP-fil' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'sapreexp' PROGRAM_NAME, 'förbearbetning av ReSA-export' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'sapurge' PROGRAM_NAME, 'ReSA-datarensning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'sarules' PROGRAM_NAME, 'ReSA-regelbearbetning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'sastdycr' PROGRAM_NAME, 'ReSA, skapande av butiksdatum' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'satotals' PROGRAM_NAME, 'ReSA-summabearbetning' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'soupld' PROGRAM_NAME, 'uppladdning av butiksorder' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'stkschedxpld' PROGRAM_NAME, 'STOCK COUNT SCHEDULE EXPLODE' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'supsplit' PROGRAM_NAME, 'Leverantörsdelad batch' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'szonrbld' PROGRAM_NAME, 'bygg om användarzonens säkerhet' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'trandataprocess' PROGRAM_NAME, 'Extern laddning av trans.data' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'vendinvc' PROGRAM_NAME, 'Lev.fakturering för komplexa avtal' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 31 LANG, 'vendinvf' PROGRAM_NAME, 'Lev.fakturering för fasta avtal' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO RESTART_CONTROL base USING
( SELECT PROGRAM_NAME PROGRAM_NAME, PROGRAM_DESC PROGRAM_DESC FROM RESTART_CONTROL_TL TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31)) USE_THIS
ON ( base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE SET base.PROGRAM_DESC = use_this.PROGRAM_DESC;
--
DELETE FROM RESTART_CONTROL_TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
