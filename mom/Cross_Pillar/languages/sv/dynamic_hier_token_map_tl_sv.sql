SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COM@' TOKEN, 31 LANG, 'Tillverkningsland' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COMN@' TOKEN, 31 LANG, 'Tillverkningsländer' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH2@' TOKEN, 31 LANG, 'Division' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH3@' TOKEN, 31 LANG, 'Grupp' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH4@' TOKEN, 31 LANG, 'Avdelning' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH5@' TOKEN, 31 LANG, 'Klass' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH6@' TOKEN, 31 LANG, 'Underklass' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP2@' TOKEN, 31 LANG, 'Divisioner' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP3@' TOKEN, 31 LANG, 'Grupper' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP4@' TOKEN, 31 LANG, 'Avdelningar' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP5@' TOKEN, 31 LANG, 'Klasser' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP6@' TOKEN, 31 LANG, 'Underklasser' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH1@' TOKEN, 31 LANG, 'Företag' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH2@' TOKEN, 31 LANG, 'Kedja' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH3@' TOKEN, 31 LANG, 'Område' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH4@' TOKEN, 31 LANG, 'Region' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH5@' TOKEN, 31 LANG, 'Distrikt' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP1@' TOKEN, 31 LANG, 'Företag' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP2@' TOKEN, 31 LANG, 'Kedjor' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP3@' TOKEN, 31 LANG, 'Områden' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP4@' TOKEN, 31 LANG, 'Regioner' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP5@' TOKEN, 31 LANG, 'Distrikt' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH1@' TOKEN, 31 LANG, 'Tillverkare' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH2@' TOKEN, 31 LANG, 'Distributör' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH3@' TOKEN, 31 LANG, 'Partihandlare' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH4@' TOKEN, 31 LANG, 'Franchise' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP1@' TOKEN, 31 LANG, 'Tillverkare' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP2@' TOKEN, 31 LANG, 'Distributörer' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP3@' TOKEN, 31 LANG, 'Partihandlare' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP4@' TOKEN, 31 LANG, 'Franchisetagare' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO DYNAMIC_HIER_TOKEN_MAP base USING
( SELECT TOKEN TOKEN, RMS_NAME RMS_NAME, CLIENT_NAME CLIENT_NAME FROM DYNAMIC_HIER_TOKEN_MAP_TL TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31)) USE_THIS
ON ( base.TOKEN = use_this.TOKEN)
WHEN MATCHED THEN UPDATE SET base.RMS_NAME = use_this.RMS_NAME, base.CLIENT_NAME = use_this.CLIENT_NAME;
--
DELETE FROM DYNAMIC_HIER_TOKEN_MAP_TL where lang = 31
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 31);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
