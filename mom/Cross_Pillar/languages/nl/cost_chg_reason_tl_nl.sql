SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 1 REASON, 18 LANG, 'Nieuwe staffel' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 REASON, 18 LANG, 'Nieuwe staffelstructuur' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 3 REASON, 18 LANG, 'Standaardstaffelwijziging' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 4 REASON, 18 LANG, 'Primaire leverancier gewijzigd' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 5 REASON, 18 LANG, 'Primair land gewijzigd' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 6 REASON, 18 LANG, 'Primaire locatie gewijzigd' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 7 REASON, 18 LANG, 'Wijziging leverancier/land artikellocatie' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 8 REASON, 18 LANG, 'RCA-kostenwijziging' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 9 REASON, 18 LANG, 'Belastingwet gewijzigd' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_CHG_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 REASON, 18 LANG, 'Externe kostenwijziging' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM COST_CHG_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
