SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'CC' EVENT_TYPE, 'Kostenwijziging' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'CL' EVENT_TYPE, 'Wijziging kostenlocatie franchisewinkel' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'CZ' EVENT_TYPE, 'Kostenzonelocatie verplaatsen' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'D' EVENT_TYPE, 'Overeenkomst' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'DP' EVENT_TYPE, 'Overeenkomstdoorvoer' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'ELC' EVENT_TYPE, 'Geschatte-integrale-kostprijsonderdeel' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'ICZ' EVENT_TYPE, 'Wijzigingen kostenzone artikel' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'MH' EVENT_TYPE, 'Handelswaarhiërarchie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'NIL' EVENT_TYPE, 'Nieuwe artikellocatie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'OH' EVENT_TYPE, 'Organisatiehiërarchie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'PP' EVENT_TYPE, 'Primaire setkosten' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'R' EVENT_TYPE, 'Herclassificatie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'RTC' EVENT_TYPE, 'Wijziging VK-prijs' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'SC' EVENT_TYPE, 'Relatiewijzigingen artikel, leverancier, land, locatie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'SH' EVENT_TYPE, 'Leverancierhiërarchie' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'T' EVENT_TYPE, 'Kostensjabloon grooth./franch.' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 18 LANG, 'TR' EVENT_TYPE, 'Relatie kostensjabloon grooth./franch.' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG base USING
( SELECT EVENT_TYPE EVENT_TYPE, EVENT_DESC EVENT_DESC FROM COST_EVENT_RUN_TYPE_CNFG_TL TL where lang = 18
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 18)) USE_THIS
ON ( base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE SET base.EVENT_DESC = use_this.EVENT_DESC;
--
DELETE FROM COST_EVENT_RUN_TYPE_CNFG_TL where lang = 18
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 18);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
