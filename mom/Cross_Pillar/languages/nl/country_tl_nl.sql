SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AF' COUNTRY_ID, 'Afghanistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AL' COUNTRY_ID, 'Albanië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DZ' COUNTRY_ID, 'Algerije' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AS' COUNTRY_ID, 'Amerikaans-Samoa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AD' COUNTRY_ID, 'Andorra' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AO' COUNTRY_ID, 'Angola' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AI' COUNTRY_ID, 'Anguilla' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AQ' COUNTRY_ID, 'Antarctica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AG' COUNTRY_ID, 'Antigua en Barbuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AR' COUNTRY_ID, 'Argentinië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AM' COUNTRY_ID, 'Armenië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AW' COUNTRY_ID, 'Aruba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AU' COUNTRY_ID, 'Australië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AT' COUNTRY_ID, 'Oostenrijk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AZ' COUNTRY_ID, 'Azerbeidzjan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BS' COUNTRY_ID, 'Bahama''s' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FM' COUNTRY_ID, 'Micronesië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MD' COUNTRY_ID, 'Moldavië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MC' COUNTRY_ID, 'Monaco' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MN' COUNTRY_ID, 'Mongolië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ME' COUNTRY_ID, 'Montenegro' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MS' COUNTRY_ID, 'Montserrat' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MA' COUNTRY_ID, 'Marokko' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MZ' COUNTRY_ID, 'Mozambique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MM' COUNTRY_ID, 'Myanmar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NA' COUNTRY_ID, 'Namibië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NR' COUNTRY_ID, 'Nauru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NP' COUNTRY_ID, 'Nepal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NL' COUNTRY_ID, 'Nederland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NC' COUNTRY_ID, 'Nieuw-Caledonië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NZ' COUNTRY_ID, 'Nieuw-Zeeland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NI' COUNTRY_ID, 'Nicaragua' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NE' COUNTRY_ID, 'Niger' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NG' COUNTRY_ID, 'Nigeria' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NU' COUNTRY_ID, 'Niue' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NF' COUNTRY_ID, 'Norfolkeiland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MP' COUNTRY_ID, 'Noordelijke Marianen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NO' COUNTRY_ID, 'Noorwegen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'OM' COUNTRY_ID, 'Oman' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PK' COUNTRY_ID, 'Pakistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PW' COUNTRY_ID, 'Palau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PS' COUNTRY_ID, 'Palestina' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PA' COUNTRY_ID, 'Panama' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PG' COUNTRY_ID, 'Papoea-Nieuw-Guinea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PY' COUNTRY_ID, 'Paraguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PE' COUNTRY_ID, 'Peru' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PH' COUNTRY_ID, 'Filipijnen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PN' COUNTRY_ID, 'Pitcairneilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PL' COUNTRY_ID, 'Polen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PT' COUNTRY_ID, 'Portugal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PR' COUNTRY_ID, 'Puerto Rico' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'QA' COUNTRY_ID, 'Qatar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'RE' COUNTRY_ID, 'Réunion' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'RO' COUNTRY_ID, 'Roemenië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'RU' COUNTRY_ID, 'Rusland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'RW' COUNTRY_ID, 'Rwanda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BL' COUNTRY_ID, 'Sint-Bartholomeus' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SH' COUNTRY_ID, 'Sint-Helena, Ascension en Tristan da Cunha' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KN' COUNTRY_ID, 'Saint Kitts en Nevis' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LC' COUNTRY_ID, 'Saint Lucia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MF' COUNTRY_ID, 'Saint-Martin (Frans deel)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PM' COUNTRY_ID, 'Saint-Pierre en Miquelon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VC' COUNTRY_ID, 'Saint Vincent en de Grenadines' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'WS' COUNTRY_ID, 'Samoa' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SM' COUNTRY_ID, 'San Marino' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ST' COUNTRY_ID, 'Sao Tomé en Principe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SA' COUNTRY_ID, 'Saoedi-Arabië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SN' COUNTRY_ID, 'Senegal' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'RS' COUNTRY_ID, 'Servië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SC' COUNTRY_ID, 'Seychellen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SL' COUNTRY_ID, 'Sierra Leone' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SG' COUNTRY_ID, 'Singapore' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SX' COUNTRY_ID, 'Sint Maarten (Nederlands deel)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SK' COUNTRY_ID, 'Slowakije' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SI' COUNTRY_ID, 'Slovenië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SB' COUNTRY_ID, 'Salomonseilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SO' COUNTRY_ID, 'Somalië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ZA' COUNTRY_ID, 'Zuid-Afrika' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GS' COUNTRY_ID, 'Zuid-Georgia en de Zuidelijke Sandwicheilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SS' COUNTRY_ID, 'Zuid-Soedan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ES' COUNTRY_ID, 'Spanje' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LK' COUNTRY_ID, 'Sri Lanka' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SD' COUNTRY_ID, 'Soedan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SR' COUNTRY_ID, 'Suriname' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SJ' COUNTRY_ID, 'Spitsbergen en Jan Mayen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SZ' COUNTRY_ID, 'Swaziland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SE' COUNTRY_ID, 'Zweden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CH' COUNTRY_ID, 'Zwitserland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SY' COUNTRY_ID, 'Arabische Republiek Syrië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TW' COUNTRY_ID, 'Taiwan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TJ' COUNTRY_ID, 'Tadzjikistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TZ' COUNTRY_ID, 'Tanzania' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TH' COUNTRY_ID, 'Thailand' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TL' COUNTRY_ID, 'Oost-Timor' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TG' COUNTRY_ID, 'Togo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TK' COUNTRY_ID, 'Tokelau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TO' COUNTRY_ID, 'Tonga' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TT' COUNTRY_ID, 'Trinidad en Tobago' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TN' COUNTRY_ID, 'Tunesië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TR' COUNTRY_ID, 'Turkije' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TM' COUNTRY_ID, 'Turkmenistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TC' COUNTRY_ID, 'Turks- en Caicoseilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TV' COUNTRY_ID, 'Tuvalu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'UG' COUNTRY_ID, 'Oeganda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'UA' COUNTRY_ID, 'Oekraïne' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AE' COUNTRY_ID, 'Verenigde Arabische Emiraten' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GB' COUNTRY_ID, 'Verenigd Koninkrijk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'US' COUNTRY_ID, 'Verenigde Staten' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'UM' COUNTRY_ID, 'Kleine afgelegen eilanden van de Verenigde Staten' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'UY' COUNTRY_ID, 'Uruguay' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'UZ' COUNTRY_ID, 'Oezbekistan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VU' COUNTRY_ID, 'Vanuatu' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VA' COUNTRY_ID, 'Vaticaanstad' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VE' COUNTRY_ID, 'Venezuela' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VN' COUNTRY_ID, 'Vietnam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VG' COUNTRY_ID, 'Britse Maagdeneilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'VI' COUNTRY_ID, 'Amerikaanse Maagdeneilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'WF' COUNTRY_ID, 'Wallis en Futuna' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'EH' COUNTRY_ID, 'Westelijke Sahara' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'YE' COUNTRY_ID, 'Jemen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ZM' COUNTRY_ID, 'Zambia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ZW' COUNTRY_ID, 'Zimbabwe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'AX' COUNTRY_ID, 'Åland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'NEW' COUNTRY_ID, 'Nieuw land' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, '99' COUNTRY_ID, 'Meerdere' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MT' COUNTRY_ID, 'Malta' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MH' COUNTRY_ID, 'Marshalleilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MQ' COUNTRY_ID, 'Martinique' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MR' COUNTRY_ID, 'Mauritanië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MU' COUNTRY_ID, 'Mauritius' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'YT' COUNTRY_ID, 'Mayotte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MX' COUNTRY_ID, 'Mexico' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BH' COUNTRY_ID, 'Bahrein' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BD' COUNTRY_ID, 'Bangladesh' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BB' COUNTRY_ID, 'Barbados' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BY' COUNTRY_ID, 'Wit-Rusland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BE' COUNTRY_ID, 'België' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BZ' COUNTRY_ID, 'Belize' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BJ' COUNTRY_ID, 'Benin' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BM' COUNTRY_ID, 'Bermuda' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BT' COUNTRY_ID, 'Bhutan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BO' COUNTRY_ID, 'Bolivia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BQ' COUNTRY_ID, 'Bonaire, Sint Eustatius en Saba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BA' COUNTRY_ID, 'Bosnië en Herzegovina' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BW' COUNTRY_ID, 'Botswana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BV' COUNTRY_ID, 'Bouveteiland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BR' COUNTRY_ID, 'Brazilië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IO' COUNTRY_ID, 'Britse Gebieden in de Indische Oceaan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BN' COUNTRY_ID, 'Brunei' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BG' COUNTRY_ID, 'Bulgarije' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BF' COUNTRY_ID, 'Burkina Faso' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'BI' COUNTRY_ID, 'Burundi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KH' COUNTRY_ID, 'Cambodja' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CM' COUNTRY_ID, 'Kameroen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CA' COUNTRY_ID, 'Canada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CV' COUNTRY_ID, 'Kaapverdië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KY' COUNTRY_ID, 'Caymaneilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CF' COUNTRY_ID, 'Centraal-Afrikaanse Republiek' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TD' COUNTRY_ID, 'Tsjaad' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CL' COUNTRY_ID, 'Chili' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CN' COUNTRY_ID, 'China' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CX' COUNTRY_ID, 'Christmaseiland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CC' COUNTRY_ID, 'Cocoseilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CO' COUNTRY_ID, 'Colombia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KM' COUNTRY_ID, 'Comoren' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CG' COUNTRY_ID, 'Congo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CD' COUNTRY_ID, 'Democratische Republiek Congo' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CK' COUNTRY_ID, 'Cookeilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CR' COUNTRY_ID, 'Costa Rica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CI' COUNTRY_ID, 'Ivoorkust' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HR' COUNTRY_ID, 'Kroatië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CU' COUNTRY_ID, 'Cuba' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CW' COUNTRY_ID, 'Curaçao' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CY' COUNTRY_ID, 'Cyprus' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'CZ' COUNTRY_ID, 'Tsjechische Republiek' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DK' COUNTRY_ID, 'Denemarken' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DJ' COUNTRY_ID, 'Djibouti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DM' COUNTRY_ID, 'Dominica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DO' COUNTRY_ID, 'Dominicaanse Republiek' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'EC' COUNTRY_ID, 'Ecuador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'EG' COUNTRY_ID, 'Egypte' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'SV' COUNTRY_ID, 'El Salvador' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GQ' COUNTRY_ID, 'Equatoriaal-Guinea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ER' COUNTRY_ID, 'Eritrea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'EE' COUNTRY_ID, 'Estland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ET' COUNTRY_ID, 'Ethiopië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FK' COUNTRY_ID, 'Falklandeilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FO' COUNTRY_ID, 'Faeröer' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FJ' COUNTRY_ID, 'Fiji' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FI' COUNTRY_ID, 'Finland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'FR' COUNTRY_ID, 'Frankrijk' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GF' COUNTRY_ID, 'Frans-Guyana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'PF' COUNTRY_ID, 'Frans-Polynesië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'TF' COUNTRY_ID, 'Franse Gebieden in de zuidelijke Indische Oceaan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GA' COUNTRY_ID, 'Gabon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GM' COUNTRY_ID, 'Gambia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GE' COUNTRY_ID, 'Georgië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'DE' COUNTRY_ID, 'Duitsland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GH' COUNTRY_ID, 'Ghana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GI' COUNTRY_ID, 'Gibraltar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GR' COUNTRY_ID, 'Griekenland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GL' COUNTRY_ID, 'Groenland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GD' COUNTRY_ID, 'Grenada' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GP' COUNTRY_ID, 'Guadeloupe' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GU' COUNTRY_ID, 'Guam' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GT' COUNTRY_ID, 'Guatemala' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GG' COUNTRY_ID, 'Guernsey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GN' COUNTRY_ID, 'Guinee' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GW' COUNTRY_ID, 'Guinee-Bissau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'GY' COUNTRY_ID, 'Guyana' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HT' COUNTRY_ID, 'Haïti' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HM' COUNTRY_ID, 'Heard- en McDonaldeilanden' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HN' COUNTRY_ID, 'Honduras' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HK' COUNTRY_ID, 'Hongkong' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'HU' COUNTRY_ID, 'Hongarije' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IS' COUNTRY_ID, 'IJsland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IN' COUNTRY_ID, 'India' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ID' COUNTRY_ID, 'Indonesië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IR' COUNTRY_ID, 'Iran' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IQ' COUNTRY_ID, 'Irak' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IE' COUNTRY_ID, 'Ierland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IM' COUNTRY_ID, 'Isle of Man' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IL' COUNTRY_ID, 'Israël' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'IT' COUNTRY_ID, 'Italië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'JM' COUNTRY_ID, 'Jamaica' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'JP' COUNTRY_ID, 'Japan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'JE' COUNTRY_ID, 'Jersey' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'JO' COUNTRY_ID, 'Jordanië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KZ' COUNTRY_ID, 'Kazachstan' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KE' COUNTRY_ID, 'Kenia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KI' COUNTRY_ID, 'Kiribati' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KP' COUNTRY_ID, 'Noord-Korea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KR' COUNTRY_ID, 'Zuid-Korea' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KW' COUNTRY_ID, 'Koeweit' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'KG' COUNTRY_ID, 'Kirgizië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LA' COUNTRY_ID, 'Laos' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LV' COUNTRY_ID, 'Letland' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LB' COUNTRY_ID, 'Libanon' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LS' COUNTRY_ID, 'Lesotho' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LR' COUNTRY_ID, 'Liberia' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LY' COUNTRY_ID, 'Libië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LI' COUNTRY_ID, 'Liechtenstein' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LT' COUNTRY_ID, 'Litouwen' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'LU' COUNTRY_ID, 'Luxemburg' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MO' COUNTRY_ID, 'Macau' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MK' COUNTRY_ID, 'Macedonië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MG' COUNTRY_ID, 'Madagaskar' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MW' COUNTRY_ID, 'Malawi' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MY' COUNTRY_ID, 'Maleisië' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'MV' COUNTRY_ID, 'Maldiven' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 18 LANG, 'ML' COUNTRY_ID, 'Mali' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COUNTRY base USING
( SELECT COUNTRY_ID COUNTRY_ID, COUNTRY_DESC COUNTRY_DESC FROM COUNTRY_TL TL where lang = 18
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 18)) USE_THIS
ON ( base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.COUNTRY_DESC = use_this.COUNTRY_DESC;
--
DELETE FROM COUNTRY_TL where lang = 18
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 18);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
