SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'DUTY6' CVB_CODE, 'DUTY US' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'DUTYUS' CVB_CODE, 'DUTYUS' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'SELLCOMM' CVB_CODE, '販売者手数料CVB' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'TDTYPE' CVB_CODE, '関税合計(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'TDTYUS' CVB_CODE, '関税合計(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'TEXP' CVB_CODE, '合計経費' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'TEXPC' CVB_CODE, '国の合計経費' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'TEXPZ' CVB_CODE, '合計経費ゾーン' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'VFD25US' CVB_CODE, '関税価額の25%(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'VFD50US' CVB_CODE, '関税価額の50%(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'VFD75US' CVB_CODE, '関税価額の75%(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CVB_HEAD_TL TL USING
(SELECT  LANG,	CVB_CODE,  CVB_DESC
FROM  (SELECT 5 LANG, 'VFDUS' CVB_CODE, '関税価額(US)' CVB_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CVB_HEAD base where dl.CVB_CODE = base.CVB_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE  SET tl.CVB_DESC = use_this.CVB_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CVB_CODE, CVB_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CVB_CODE, use_this.CVB_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO CVB_HEAD base USING
( SELECT CVB_CODE CVB_CODE, CVB_DESC CVB_DESC FROM CVB_HEAD_TL TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5)) USE_THIS
ON ( base.CVB_CODE = use_this.CVB_CODE)
WHEN MATCHED THEN UPDATE SET base.CVB_DESC = use_this.CVB_DESC;
--
DELETE FROM CVB_HEAD_TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
