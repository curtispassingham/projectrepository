SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 1 LANG_LANG, '英語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 2 LANG_LANG, 'ドイツ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 3 LANG_LANG, 'フランス語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 4 LANG_LANG, 'スペイン語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 5 LANG_LANG, '日本語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 6 LANG_LANG, '韓国語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 7 LANG_LANG, 'ロシア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 8 LANG_LANG, '簡体字中国語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 9 LANG_LANG, 'トルコ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 10 LANG_LANG, 'ハンガリー語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 11 LANG_LANG, '繁体字中国語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 12 LANG_LANG, 'ポルトガル語(ブラジル)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 13 LANG_LANG, 'アラビア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 14 LANG_LANG, 'フランス語(カナダ)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 15 LANG_LANG, 'クロアチア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 16 LANG_LANG, 'チェコ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 17 LANG_LANG, 'デンマーク語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 18 LANG_LANG, 'オランダ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 19 LANG_LANG, 'フィンランド語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 20 LANG_LANG, 'ギリシャ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 21 LANG_LANG, 'ヘブライ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 22 LANG_LANG, 'イタリア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 23 LANG_LANG, 'スペイン語(南米)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 24 LANG_LANG, 'リトアニア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 25 LANG_LANG, 'ノルウェー語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 26 LANG_LANG, 'ポーランド語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 27 LANG_LANG, 'ポルトガル語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 28 LANG_LANG, 'ルーマニア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 29 LANG_LANG, 'スロバキア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 30 LANG_LANG, 'スロベニア語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 31 LANG_LANG, 'スウェーデン語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 32 LANG_LANG, 'タイ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 5 LANG, 33 LANG_LANG, 'タガログ語' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO LANG base USING
( SELECT LANG_LANG LANG, DESCRIPTION DESCRIPTION FROM LANG_TL TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM LANG_TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
