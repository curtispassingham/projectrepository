SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_DISC' ERROR_CODE, '店舗条件トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_IGTAX' ERROR_CODE, '店舗条件トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_PYMT' ERROR_CODE, '店舗条件トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_TAX' ERROR_CODE, '店舗条件トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_TEND' ERROR_CODE, '店舗条件トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CREDIT_PROMO_ID_STIN' ERROR_CODE, 'クレジット販促IDが無効です。' ERROR_DESC, '正しいクレジット販促IDを入力してください' REC_SOLUTION, 'クレジット販促が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CUST_ID_REQ' ERROR_CODE, '顧客ID番号が必須です。' ERROR_DESC, '正しい顧客ID番号を入力してください。' REC_SOLUTION, '顧客IDがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CUST_ORD_ATTR_REQ' ERROR_CODE, '顧客オーダー属性が必須です。' ERROR_DESC, '顧客属性画面から顧客オーダー属性を入力してください。' REC_SOLUTION, '顧客属性がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CUST_ORD_DATE_REQDINTITEM' ERROR_CODE, '対応するTITEMに顧客オーダー日付が必須です。' ERROR_DESC, '顧客属性画面から顧客オーダー日を入力してください。推奨フォーマットは、YYYYMMDDです' REC_SOLUTION, '顧客オーダー日がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DATAUNEXPECTEDSTOREDAY' ERROR_CODE, 'データが、この店舗日に有効ではありません。' ERROR_DESC, '無効な店舗または営業日です。店舗、会社期末、会社期末の例外、および店舗期末の情報を確認してください。' REC_SOLUTION, '店舗日に予期しないデータ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_CATT' ERROR_CODE, '日次決算トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_DISC' ERROR_CODE, '日次決算トランザクションには値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_IGTAX' ERROR_CODE, '日次決算トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_ITEM' ERROR_CODE, '日次決算トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_PYMT' ERROR_CODE, '日次決算トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_TEND' ERROR_CODE, '日次決算トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DISC_REASON_REQ' ERROR_CODE, '店舗値引の値引事由を空白にすることはできません' ERROR_DESC, '値引事由を入力してください' REC_SOLUTION, '値引事由がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DISC_REF_NO_REQ' ERROR_CODE, '販促の値引参照番号を空白にすることはできません' ERROR_DESC, '値引参照番号を入力してください' REC_SOLUTION, '値引参照がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DUP_REC' ERROR_CODE, 'レコードが重複しています。トランザクションが拒否ファイルに送信されました。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'トランザクションが重複' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DUP_TAXC_COMPITEM' ERROR_CODE, '税コードとコンポーネント・アイテム番号の組み合わせが重複しています。トランザクションは拒否ファイルに送信されました。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '税コードが重複しています' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DUP_TRAN' ERROR_CODE, 'トランザクション番号が重複しています。トランザクションが拒否ファイルに送信されました。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'トランザクションが重複' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'EEXCH_ITEM_REQ' ERROR_CODE, '為替トランザクションには、2つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションに1つ以上のアイテム・レコードを追加してください。' REC_SOLUTION, 'アイテム・レコードなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'EEXCH_NO_PYMT' ERROR_CODE, '等価交換トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '支払レコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FHEAD_FIL_STIN' ERROR_CODE, 'FHEADファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFHEADレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FHEAD_NOT_FIRST' ERROR_CODE, 'FHEADレコードが、ファイル内の最初のレコードではありません。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFHEADレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FTAIL_FIL_STIN' ERROR_CODE, 'FTAILファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFTAILレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FTAIL_IN_TRAN' ERROR_CODE, '不完全なトランザクションです。トランザクションにFTAILレコードがあります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FTAIL_NOT_LAST' ERROR_CODE, 'FHEADレコードが、ファイル内の最後のレコードではありません。' ERROR_DESC, '入力ファイルにFTAILレコードがありません。' REC_SOLUTION, 'FTAILレコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FULFILL_ORDER_NO_ERR' ERROR_CODE, 'オーダー・フルフィルメント番号は、ステータスがアイテム・オーダー完了またはオーダー取消ステータスの外部顧客オーダーに対してのみ有効です。' ERROR_DESC, 'オーダー・フルフィルメント番号を空白に設定してください。' REC_SOLUTION, 'オーダーフルフィル番号無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_DISC' ERROR_CODE, '無効トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_IGTAX' ERROR_CODE, '無効トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_ITEM' ERROR_CODE, '無効トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_PYMT' ERROR_CODE, '事後無効化トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_TEND' ERROR_CODE, '無効トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RECORDS_REJECTED' ERROR_CODE, 'この店舗/日の読込み中、レコードが拒否されました。' ERROR_DESC, 'この入力ファイルに問題があるため、完全に読み込むことができません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '拒否されたレコード' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_NO_DISC' ERROR_CODE, '払戻しトランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_NO_IGTAX' ERROR_CODE, '払戻しトランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_NO_ITEM' ERROR_CODE, '払戻しトランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_NO_PYMT' ERROR_CODE, '払戻しトランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_NO_TAX' ERROR_CODE, '払戻しトランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REFUND_TENDER_REQ' ERROR_CODE, '払戻しトランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '無効な払戻しトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REGISTER_ID_REQ' ERROR_CODE, 'トランザクションがレジスタ・レベルで作成されたため、レジスタIDが必須です。' ERROR_DESC, '正しいレジスタIDを入力してください。' REC_SOLUTION, 'レジスタIDがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'REGISTER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'バランス・レベルのため、レジスタIDが必須です。' ERROR_DESC, '正しいレジスタIDを入力してください。' REC_SOLUTION, 'レジスタIDがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETDISP_INV_RET' ERROR_CODE, '返品処分は、返品に関連付けられた在庫がある場合のみ有効です。' ERROR_DESC, '返品処分を空白に設定してください。' REC_SOLUTION, '返品処分が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETDISP_OMS' ERROR_CODE, '返品処分は、OMSから作成されOMSで処理された返品トランザクションでのみ使用できます。' ERROR_DESC, '返品処分を空白に設定してください。' REC_SOLUTION, '返品処分が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETURN_DISP_REQ' ERROR_CODE, '返品処分が必須です。' ERROR_DESC, '有効な返品処分を選択してください。' REC_SOLUTION, '返品処分がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETURN_ITEM_REQ' ERROR_CODE, '返品トランザクションには、1つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションにアイテム・レコードを追加してください。' REC_SOLUTION, '無効な返品トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETURN_NO_PYMT' ERROR_CODE, '返品トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な返品トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETURN_TENDER_REQ' ERROR_CODE, '返品トランザクションには、1つ以上の決済レコードを含める必要があります。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '返品に決済がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FILE_ERROR' ERROR_CODE, '最後のファイル操作でエラーが発生しました。' ERROR_DESC, '' REC_SOLUTION, 'ファイル・エラー' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETWH_EXT_ORD' ERROR_CODE, '返品倉庫は外部顧客オーダーに対してのみ有効です。' ERROR_DESC, '返品倉庫を空白に設定してください。' REC_SOLUTION, '無効な返品倉庫です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETWH_OMS' ERROR_CODE, '返品倉庫は、OMSから作成されOMSで処理された返品トランザクションでのみ使用できます。' ERROR_DESC, '返品倉庫を空白に設定してください。' REC_SOLUTION, '無効な返品倉庫です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETWH_REQ' ERROR_CODE, '売上タイプが外部顧客オーダーで、OMSから作成された返品トランザクションには返品倉庫が必要です。' ERROR_DESC, '有効な倉庫をこのトランザクションに追加してください。' REC_SOLUTION, '無効な返品倉庫です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'SALE_ITEM_REQ' ERROR_CODE, '販売トランザクションには、1つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションにアイテム・レコードを追加してください。' REC_SOLUTION, 'アイテム・レコードなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'SALE_TENDER_REQ' ERROR_CODE, '販売トランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '決済レコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'SET_DROP_SHIP' ERROR_CODE, 'ドロップ出荷インジケータが、このアイテムに対して正常にインポートされませんでした。' ERROR_DESC, 'アイテムの詳細セクションで、ドロップ出荷インジケータを確認してください。' REC_SOLUTION, '無効なドロップ出荷インジケ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'SKU_NOT_FOUND' ERROR_CODE, 'アイテムが空か、またはRMSに存在しません。' ERROR_DESC, 'リストからアイテムを選択してください。' REC_SOLUTION, 'アイテムが見つかりません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'STOREDAYNOTREADYTOBELOAD' ERROR_CODE, 'この店舗日は、「読込み可能」のステータスではありません。' ERROR_DESC, 'データが既に読み込まれているか、ポーリングされた一部のデータが誤った順番で読み込まれた可能性があります。' REC_SOLUTION, '店舗日の読込み準備未完了' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'STOREDAY_LOCK_FAILED' ERROR_CODE, '店舗日の書込みロックを取得できません。ロック中のレコードが読み込まれていないか、店舗日が別のプロセスでロックされます。' ERROR_DESC, 'ロックが解除されてから、もう一度実行してください。' REC_SOLUTION, '店舗日ロックに失敗しました' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_ITEM_REQ' ERROR_CODE, 'タンク・ディップ・トランザクションには、1つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションにアイテム・レコードを追加してください。' REC_SOLUTION, 'アイテム・レコードなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_CATT' ERROR_CODE, 'タンク・ディップ・トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_DISC' ERROR_CODE, 'タンク・ディップ・トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_PYMT' ERROR_CODE, 'タンク・ディップ・トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_TAX' ERROR_CODE, 'タンク・ディップ・トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TCUST_FIL_STIN' ERROR_CODE, 'TCUSTファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なTCUSTレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TCUST_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TCUSTが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。TCUSTレコードは、THEADとTTAILレコード間にある必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TERM_MARKER_NO_ERROR' ERROR_CODE, 'このエラーは、すべてのデータが読み込まれたか確認するために使用されます。' ERROR_DESC, 'すべてのデータを読み込み、saimptlogfinを実行してください。' REC_SOLUTION, 'エラーはありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_FIL_STIN' ERROR_CODE, 'THEADファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なTHEADレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。THEADが不正なポジションにあります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_LOC_NO_BANN' ERROR_CODE, 'ロケーションにバナーIDがありませんでした。' ERROR_DESC, 'この店舗に関連するバナーIDはありません。' REC_SOLUTION, 'バナーがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FULFILL_ORDER_NO_REQ' ERROR_CODE, 'オーダー・フルフィルメント番号が必須です。' ERROR_DESC, '正しいオーダー・フルフィルメント番号を入力してください。' REC_SOLUTION, 'オーダーフルフィル番号なし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'GARBAGE_IN_RECORD' ERROR_CODE, 'レコードに不要データが埋め込まれています。' ERROR_DESC, '入力ファイルに問題があります。レコード内に埋込み文字があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'レコードに不要データあり' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_COUPON_NO_REQ' ERROR_CODE, '値引タイプが店舗クーポンの場合、クーポン番号を空白にすることはできません。' ERROR_DESC, 'クーポン番号を入力してください。' REC_SOLUTION, 'クーポン番号がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_FIL_STIN' ERROR_CODE, 'IDISCファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'IDISC識別子が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_INVALID_CW' ERROR_CODE, '値引要計量インジケータ - 要計量インジケータが重量アイテムに設定されていません。' ERROR_DESC, '正しい要計量インジケータを入力してください。' REC_SOLUTION, '要計量インジケータが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。IDISCレコードが不正なポジションにあります。' ERROR_DESC, 'この入力ファイルに問題があります。IDISCレコードは、TITEMレコードの後である必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_PRC_STIN' ERROR_CODE, '無効な値引販促コンポーネントです。' ERROR_DESC, '有効な値引販促コンポーネントを入力してください。' REC_SOLUTION, '販促コンポーネントが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_QTY_STIN' ERROR_CODE, '値引数量 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, '有効な値引数量を入力してください。' REC_SOLUTION, '値引数量が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_SET_CW' ERROR_CODE, '値引要計量インジケータ - 要計量インジケータが正常にインポートされませんでした。' ERROR_DESC, '正しい要計量インジケータを入力してください。' REC_SOLUTION, '要計量インジケータが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_UDA_STIN' ERROR_CODE, '値引額 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, '有効な値引額を入力してください。' REC_SOLUTION, '値引金額が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_UOM_QTY_STIN' ERROR_CODE, '値引UOM数量 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいUOM数量を入力してください。' REC_SOLUTION, '値引UOMが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDNT_ID_WITHOUT_IDNT_MTHD' ERROR_CODE, '識別子が存在しますが、識別方式が設定されていません。' ERROR_DESC, '有効な識別方式をリストから入力してください。' REC_SOLUTION, '識別方式がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDNT_MTHD_WITHOUT_IDNT_ID' ERROR_CODE, 'この識別方式の識別子がありません。' ERROR_DESC, '有効な識別子を入力してください。' REC_SOLUTION, '識別子がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IGTAX_FIL_STIN' ERROR_CODE, 'アイテムのグローバル税のファイル・ライン識別子。数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'IGTAX識別子が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IGTAX_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。アイテムのグローバル税レコードが不正なポジションにあります' ERROR_DESC, '入力ファイルに問題があります。IGTAXレコードは、TITEM-IDISCレコードの後にくる必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'IGTAXレコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IGTAX_RAT_STIN' ERROR_CODE, 'アイテムのグローバル税レート - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です' ERROR_DESC, '正しいアイテムのグローバル税レートを入力してください。' REC_SOLUTION, 'IGTAXレートが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_BANNER_ID' ERROR_CODE, 'バナーIDが無効です。' ERROR_DESC, '有効なバナーIDを選択してください。' REC_SOLUTION, 'バナーが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_BIRTHDATE' ERROR_CODE, '誕生日が無効です。' ERROR_DESC, '誕生日の有効なフォーマットは、YYYYMMDDです。' REC_SOLUTION, '誕生日が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_BUSINESS_DATE' ERROR_CODE, '営業日が無効または欠落しています。' ERROR_DESC, '営業日の有効なフォーマットは、YYYYMMDDです。' REC_SOLUTION, '無効な営業日です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CASHIER_ID' ERROR_CODE, 'キャッシャPOS IDに、対応する従業員IDが含まれません。' ERROR_DESC, '従業員管理フォームに従業員IDおよびPOS IDを入力してください。' REC_SOLUTION, '無効なキャッシャIDです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CCAS' ERROR_CODE, '無効なクレジット・カード認証元です。' ERROR_DESC, 'リストからクレジット・カード認証元を選択してください。' REC_SOLUTION, '無効な認証元です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CCEM' ERROR_CODE, '無効なクレジット・カード入力モードです。' ERROR_DESC, 'リストからクレジット・カード入力モードを選択してください。' REC_SOLUTION, 'クレジットカド入力モ-ド無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CCSC' ERROR_CODE, '無効なクレジット・カード特別条件です。' ERROR_DESC, 'リストからクレジット・カード特別条件を選択してください。' REC_SOLUTION, '無効な条件です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CCVF' ERROR_CODE, '無効なクレジット・カード所有者照合です。' ERROR_DESC, 'リストからクレジット・カード所有者照合を選択してください。' REC_SOLUTION, '無効な照合です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CC_CHECKSUM' ERROR_CODE, '無効なクレジット・カード・チェック・デジットです。このカード番号は無効です。' ERROR_DESC, '正しいクレジット・カード番号を入力してください。' REC_SOLUTION, '無効なチェック・デジット' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CC_EXP_DATE' ERROR_CODE, '無効な有効期限です。' ERROR_DESC, '有効期限の有効なフォーマットは、YYYYMMDDです。' REC_SOLUTION, '無効な有効期限です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CC_MASK' ERROR_CODE, '無効なクレジット・カード・マスク値です。' ERROR_DESC, '正しいクレジット・カード・マスク値を入力してください。' REC_SOLUTION, '無効クレジットカ-ドマスク値' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CC_PREFIX' ERROR_CODE, 'このクレジット・カードのプリフィクスは、指定範囲内にありません。' ERROR_DESC, '正しいクレジット・カード番号を入力してください。' REC_SOLUTION, '無効なクレカ・プリフィクス' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CHECK_NO' ERROR_CODE, '数値フィールドの小切手番号が無効か、または非数値文字を含んでいます。' ERROR_DESC, '正しい小切手番号を入力してください。' REC_SOLUTION, '無効な小切手番号です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CIDT' ERROR_CODE, '無効な顧客IDタイプです。' ERROR_DESC, 'リストから顧客IDタイプを選択してください。' REC_SOLUTION, '無効な顧客IDタイプ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_IGTAX' ERROR_CODE, '「支払済」トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_PYMT' ERROR_CODE, '「支払済」トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_TAX' ERROR_CODE, '「支払済」トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_REASON_CODE_REQ' ERROR_CODE, '「支払済」トランザクションには、「事由コード」が必須です。' ERROR_DESC, 'リストから事由コードを選択してください。' REC_SOLUTION, '事由コードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_TENDER_REQ' ERROR_CODE, '「支払済」トランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '決済レコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_CATT' ERROR_CODE, '「支払い登録」トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_CUST' ERROR_CODE, '「支払い登録」トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_IGTAX' ERROR_CODE, '「支払い登録」トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_ITEM' ERROR_CODE, '「支払い登録」トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_PYMT' ERROR_CODE, '「支払い登録」トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_TAX' ERROR_CODE, '「支払い登録」トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_REASON_CODE_REQ' ERROR_CODE, '「支払い登録」トランザクションには、「事由コード」が必須です。' ERROR_DESC, 'リストから事由コードを選択してください。' REC_SOLUTION, '事由コードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_TENDER_REQ' ERROR_CODE, '「支払い登録」トランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '決済レコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'POSDATATOOOLD' ERROR_CODE, 'POSデータが大きすぎるため、システムに読み込めません。' ERROR_DESC, '売上監査システムのオプション画面で「売上後の日数」フィールドを確認します。システムへPOSデータを読み込むために、このフィールドには本日から過去に向かった日数を入力します。' REC_SOLUTION, '古いPOSデータ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'POSFUTUREDATA' ERROR_CODE, 'POSデータは、現在の日付より後の営業日に有効です。' ERROR_DESC, '将来の営業日のデータをインポートすることはできません。' REC_SOLUTION, '無効な営業日です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'POSUNEXPECTEDSTOREDAY' ERROR_CODE, 'POSデータが、この店舗日に有効ではありません。この店舗の開店は予定されていません。' ERROR_DESC, '店舗と営業日に問題はありませんが、この店舗日のPOSデータは想定されていません。この店舗の会社期末、会社期末の例外、店舗期末情報を確認してください。' REC_SOLUTION, '予期しないPOSデータ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PROM_COMP_REQ' ERROR_CODE, '販促の販促コンポーネントを空白にすることはできません' ERROR_DESC, '販促コンポーネントを入力してください' REC_SOLUTION, '販促コンポーネントなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_CATT' ERROR_CODE, '引合いトランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な引合いトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_IGTAX' ERROR_CODE, '引合い登録トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な引合い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_ITEM' ERROR_CODE, '引合いトランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な引合いトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_PYMT' ERROR_CODE, '引合い登録トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な引合い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_TENDER_REQ' ERROR_CODE, '引合いトランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '決済レコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_ITEM_REQ' ERROR_CODE, 'ポンプ・テスト・トランザクションには、1つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションにアイテム・レコードを追加してください。' REC_SOLUTION, 'アイテム・レコードなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_CATT' ERROR_CODE, 'ポンプ・テスト・トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_CUST' ERROR_CODE, 'ポンプ・テスト・トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_DISC' ERROR_CODE, 'ポンプ・テスト・トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_TAX' ERROR_CODE, 'ポンプ・テスト・トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_TEND' ERROR_CODE, 'ポンプ・テスト・トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_CATT' ERROR_CODE, '無効トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CAN_MASK' ERROR_CODE, '無効な当座預金口座番号マスク値です。' ERROR_DESC, '正しい当座預金口座番号マスク値を入力してください。' REC_SOLUTION, '無効当座預金口座No.マスク値' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_OTN_STIN' ERROR_CODE, '当初POSトランザクション番号 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この値は、データの読込みを容易にするため、-1に設定されました。無効化済の当初POSトランザクション番号を入力してください。' REC_SOLUTION, 'POSトランザクション番号無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_RAMT_STIN' ERROR_CODE, '丸め金額 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい丸め額を入力してください。' REC_SOLUTION, '丸め金額が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_ROAMT_STIN' ERROR_CODE, '切捨て金額 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい切捨て額を入力してください。' REC_SOLUTION, '切捨て金額が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_TRN_STIN' ERROR_CODE, 'THEADトランザクション番号 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この値は、データの読込みを容易にするため、-1に設定されました。このトランザクションのPOSトランザクション番号を入力してください。' REC_SOLUTION, 'THEAD番号が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_VAL_STIN' ERROR_CODE, '値 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい値を入力してください。' REC_SOLUTION, '値が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_CLS_STIN' ERROR_CODE, 'SKUクラス - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいSKUクラスを入力してください。' REC_SOLUTION, 'クラスが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_CUST_ORD_STIN' ERROR_CODE, '顧客オーダー・ライン番号 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '顧客属性画面から正しい顧客オーダー・ライン番号を入力してください。' REC_SOLUTION, '顧客オーダー・ラインが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_DEP_STIN' ERROR_CODE, 'デパートメント - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいデパートメントを入力してください。' REC_SOLUTION, 'デパートメントが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_FIL_STIN' ERROR_CODE, 'TITEMファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'TITEM番号が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_INVALID_CW' ERROR_CODE, 'アイテム要計量インジケータ - 要計量インジケータが重量アイテムに設定されていません。' ERROR_DESC, 'アイテムの詳細セクションで、要計量インジケータを確認してください。' REC_SOLUTION, '要計量インジケータが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TITEMが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。TITEMレコードは、THEADとTTAILレコード間にある必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_MID_STIN' ERROR_CODE, 'メディアID - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '顧客属性画面から正しいメディアIDを入力してください。' REC_SOLUTION, 'メディアIDが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_OUR_STIN' ERROR_CODE, '当初売単価 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい当初売単価を入力してください。' REC_SOLUTION, '当初売単価が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_QTY_SIGN' ERROR_CODE, 'アイテム・ステータスが販売、オーダー開始(ORI)、オーダー完了(ORD)、取置開始(LIN)、取置完了(LCO)の場合はプラスに設定する必要があります。' ERROR_DESC, 'アイテム数量サインを修正して下さい。' REC_SOLUTION, 'アイテム数量符号が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_QTY_STIN' ERROR_CODE, 'アイテム数量 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, '正しいアイテム数量の入力。アイテムレコードステータスが「販売」の場合プラス数量、「返品」の場合マイナス数量を設定し、「無効」の場合は符号は無効済アイテムとは反対にします。' REC_SOLUTION, 'アイテム数量が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_SBC_STIN' ERROR_CODE, 'サブクラス - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいサブクラスを入力してください。' REC_SOLUTION, 'サブクラスが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_SET_CW' ERROR_CODE, 'アイテム要計量インジケータ - 要計量インジケータが、このアイテムに対して正常にインポートされませんでした。' ERROR_DESC, 'アイテムの詳細セクションで、要計量インジケータを確認してください。' REC_SOLUTION, '要計量インジケータが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_SUP_STIN' ERROR_CODE, 'UPC補足 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいUPC補足情報を入力してください。' REC_SOLUTION, '無効なUPC補足情報' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_UOM_QTY_STIN' ERROR_CODE, 'アイテムUOM数量 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しいUOM数量を入力してください。' REC_SOLUTION, 'UOM数量が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_UOM_QTY_ZERO' ERROR_CODE, 'アイテム・ステータスが「販売」または「返品」である「要計量」アイテム・レコードに対して、数量が0でない場合はUOM数量を0に設定できません。その逆も同様です。' ERROR_DESC, 'アイテムUOM数量を入力してください。' REC_SOLUTION, 'UOM数量が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_URT_STIN' ERROR_CODE, '売単価 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい売単価を入力してください。' REC_SOLUTION, '売単価が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TITEM_WH_STIN' ERROR_CODE, '返品倉庫 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい返品倉庫を入力してください。' REC_SOLUTION, '無効な返品倉庫です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOO_MANY_FHEADS' ERROR_CODE, '余計なFHEADレコードが見つかりました。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'FHEADレコードが多すぎます' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_IGTAX_AMT_GT_TOTRET' ERROR_CODE, 'アイテムのグローバル税総額が売価合計より大きくなっています。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なIGTAX金額です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_IGTAX_AMT_STIN' ERROR_CODE, 'アイテムのグローバル税の総額。数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, '正しいアイテムのグローバル税の総額を入力してください。' REC_SOLUTION, '無効なIGTAX金額です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_CATT' ERROR_CODE, '合計トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_CUST' ERROR_CODE, '合計トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_DISC' ERROR_CODE, '合計トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから合計レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_ITEM' ERROR_CODE, '合計トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_PYMT' ERROR_CODE, '合計トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_TAX' ERROR_CODE, '合計トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_TEND' ERROR_CODE, '合計トランザクションには、決済レコードを含めないでください。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_REF_NO_1_REQ' ERROR_CODE, '合計トランザクションでは、「参照番号1」フィールドに合計IDが必須です。' ERROR_DESC, '「参照番号1」フィールドに合計IDを入力してください。' REC_SOLUTION, '参照番号がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_VALUE_REQ' ERROR_CODE, '合計トランザクションには、合計値が必須です。' ERROR_DESC, '合計値を入力してください。' REC_SOLUTION, '合計値がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TPYMT_AMT_STIN' ERROR_CODE, '支払額 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい支払額を入力してください' REC_SOLUTION, '無効な支払額です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TPYMT_FIL_STIN' ERROR_CODE, 'アイテムの支払いファイル・ライン識別子の数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なTPYMTレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TPYMT_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TPYMTレコードが不正なポジションにあります' ERROR_DESC, '入力ファイルに問題があります。TPYMTレコードは、TTENDレコードの前である必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_MISS_REC' ERROR_CODE, 'トランザクションにサブレコードがありません。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'サブレコードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_NOT_RECOGNIZED' ERROR_CODE, 'トランザクション・タイプが認識されません。' ERROR_DESC, '入力ファイルに問題があります。TLOGからRTLOGへの変換プロセスを確認するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効トランザクションタイプ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_NO_REQ' ERROR_CODE, 'トランザクション番号が必須です。' ERROR_DESC, '正しいトランザクション番号を入力してください。' REC_SOLUTION, 'トランザクション番号なし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_XTRA_CUST' ERROR_CODE, 'トランザクションに含めることができる顧客レコードは1つだけです。' ERROR_DESC, 'このトランザクションから余計な顧客レコードを削除してください。' REC_SOLUTION, '顧客レコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_XTRA_REC' ERROR_CODE, '不明なレコードがあります。' ERROR_DESC, '入力ファイルに問題があります。有効なレコード・タイプはFHEAD、THEAD、TCUST、CATT、TITEM、IDISC、TTEND、TTAX、TTAIL、FTAILです。ファイルの編集店舗の再ポーリングが必要です。' REC_SOLUTION, '不明なレコード・タイプです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRANLVLTAX_NO_IGTAX' ERROR_CODE, 'トランザクション・レベルの税で構成された店舗には、アイテム・レベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, 'IGTAXレコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAIL_FIL_STIN' ERROR_CODE, 'TTAILファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'TTAILレコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAIL_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TTAILが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAIL_TRC_STIN' ERROR_CODE, 'TTAILトランザクション・レコード・カウンタ - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'TTAILレコードが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAIL_WITHOUT_THEAD' ERROR_CODE, '不完全なトランザクションです。TTAILがトランザクション外にあります。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAX_AMT_STIN' ERROR_CODE, '税額 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい税額を入力してください。' REC_SOLUTION, '税額が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAX_FIL_STIN' ERROR_CODE, 'TTAXファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なTTAXレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTAX_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TTAXが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。TTAXレコードは、THEADとTTAILレコード間にある必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_CHECK_NO_REQ' ERROR_CODE, '小切手番号の数値フィールドが空白か、または非数値文字が含まれています。' ERROR_DESC, '正しい小切手番号を入力してください。' REC_SOLUTION, '無効な小切手番号です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_COUPON_NO_REQ' ERROR_CODE, '決済方法グループがクーポンの場合、クーポン番号を空白にすることはできません。' ERROR_DESC, 'クーポンを入力してください。' REC_SOLUTION, 'クーポン番号がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_FIL_STIN' ERROR_CODE, 'TTENDファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なTTENDレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。TTENDが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。TTENDレコードは、THEADとTTAILレコード間にある必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_OCA_STIN' ERROR_CODE, '当初通貨の価額 - 数値フィールドに非数値文字が含まれています。' ERROR_DESC, '正しい当初通貨の価額を入力してください。' REC_SOLUTION, '無効な通貨の価額です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_TAM_STIN' ERROR_CODE, '決済額 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白の可能性があります。' ERROR_DESC, '正しい決済額を入力してください。' REC_SOLUTION, '無効な決済額です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TTEND_TTI_STIN' ERROR_CODE, '決済方法IDが、決済方法グループに含まれていないか、または空白です。' ERROR_DESC, 'リストから決済方法を選択してください。' REC_SOLUTION, '無効な決済方法です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'UPC_NOT_FOUND' ERROR_CODE, 'このUPCはRMSにありません。' ERROR_DESC, 'リストからUPCを選択してください。' REC_SOLUTION, 'UPCが見つかりません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'VENDOR_DATA_REQ' ERROR_CODE, 'ベンダーのデータが必須です - 次のうち少なくとも1つのフィールドに値が入力されている必要があります:「ベンダー請求番号」、「支払照会番号」、「配送番号証明」' ERROR_DESC, '次の値の少なくとも1つを入力して下さい: ベンダー請求書番号、支払参照番号、配送証明番号。' REC_SOLUTION, 'ベンダーがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'VENDOR_NO_REQ' ERROR_CODE, 'THEADのベンダー番号は、商品ベンダー支払い登録または経費元ベンダー支払い登録に必須です。' ERROR_DESC, 'リストからベンダー番号を選択してください。' REC_SOLUTION, 'ベンダーがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'VOID_NO_PYMT' ERROR_CODE, '無効トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_TAX' ERROR_CODE, '日次決算トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DUP_TAXC' ERROR_CODE, '税コードが重複しています。トランザクションは拒否ファイルに送信されました。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '税コードが重複しています' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'FTAIL_FRC_STIN' ERROR_CODE, 'FTAILレコード・カウンタ - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFTAILレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_EXP_DATE' ERROR_CODE, '無効な証票有効期限です。' ERROR_DESC, '有効な有効期限を入力してください。有効期限の有効なフォーマットは、YYYYMMDDです。' REC_SOLUTION, '無効な有効期限です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_NON_MERCH_CODE' ERROR_CODE, '経費元ベンダー支払い登録または商品ベンダー支払い登録トランザクションの事由コードが無効です。' ERROR_DESC, 'トランザクションに有効な事由コードを選択してください。' REC_SOLUTION, '無効な非商品コードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_TEND' ERROR_CODE, 'メーター・トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NON_MERCH_ITEM_NO_REQ' ERROR_CODE, 'このアイテム・レコードには、非商品アイテム番号が必須です。' ERROR_DESC, '適切な非商品アイテムを選択してください。' REC_SOLUTION, '非商品アイテムがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_TAX' ERROR_CODE, '非販売トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_CUST' ERROR_CODE, '引合いトランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な引合いトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_TAX' ERROR_CODE, '引合いトランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な引合いトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_IGTAX' ERROR_CODE, 'ポンプ・テスト・トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_CUST' ERROR_CODE, '無効トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_CUST' ERROR_CODE, 'タンク・ディップ・トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CASHIER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'バランス・レベルのため、キャッシャIDが必須です。' ERROR_DESC, '正しいキャッシャIDを入力して下さい。' REC_SOLUTION, '無効なキャッシャIDです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CATT_FIL_STIN' ERROR_CODE, 'CATTレコードのファイル・ライン識別子 - 数値フィールドに非数値文字が含まれているか、またはフィールドが空白です。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なCATTレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CATT_IN_ILLEGAL_POS' ERROR_CODE, '不完全なトランザクションです。CATTレコードが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。CATTレコードは、TCUSTレコードの後にくる必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CC_NO_REQ' ERROR_CODE, 'クレジット・カード番号が必須です。' ERROR_DESC, '正しいクレジット・カード番号を入力してください。' REC_SOLUTION, 'クレジット・カード番号なし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_CATT' ERROR_CODE, '決算トランザクションには、顧客属性レコードを含めないでください。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_CUST' ERROR_CODE, '決算トランザクションには、顧客レコードを含めないでください。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_DISC' ERROR_CODE, '決算トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_IGTAX' ERROR_CODE, '決算トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_ITEM' ERROR_CODE, '決算トランザクションには、アイテム・レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_TAX' ERROR_CODE, '決算トランザクションには、税レコードを含めないでください。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_TEND' ERROR_CODE, '決算トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_CATT' ERROR_CODE, '店舗条件トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_CUST' ERROR_CODE, '店舗条件トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_CUST_ORD_DATE' ERROR_CODE, '無効な顧客オーダー日です。' ERROR_DESC, '顧客オーダー日の有効なフォーマットは、YYYYMMDDです。' REC_SOLUTION, '無効な顧客オーダー日です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_DISCOUNT_UOM' ERROR_CODE, 'アイテムの値引UOMが無効です。' ERROR_DESC, '標準UOMと互換性がある別のUOMを選択するか、現在の値引UOMと標準UOMとの間の換算を定義してください。' REC_SOLUTION, '値引UOMが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_DOCUMENT' ERROR_CODE, '商品券/証票番号が必須です。' ERROR_DESC, '商品券/証票番号を入力してください。' REC_SOLUTION, '無効な文書です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_FTAIL_POS' ERROR_CODE, 'FTAILレコードが不正なポジションにあります。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFTAILレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_FTAIL_TRAN_CNT' ERROR_CODE, 'FTAILトランザクション番号カウントが不正です。' ERROR_DESC, '入力ファイルに問題があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '無効なFTAILレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_ITEM_NO' ERROR_CODE, 'アイテム番号が空白です。' ERROR_DESC, 'リストからアイテム番号を選択してください。' REC_SOLUTION, 'アイテムが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_ITEM_SWIPED_IND' ERROR_CODE, 'アイテム読取りインジケータが無効です。値は"Y"にデフォルト設定されました。' ERROR_DESC, '「読取り済アイテム」チェック・ボックスをオンにして、正しい値を設定してください。' REC_SOLUTION, '無効アイテム読取りインジケ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_NO_INV_RET_IND' ERROR_CODE, '在庫なしの返品インジケータが無効です。値は空白にデフォルト設定されました。' ERROR_DESC, '正しい値を設定するには、「在庫なしの返品」ドロップダウン・リストから選択してください。' REC_SOLUTION, '無効な非在庫返品インジケ-タ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_ORIG_CURR' ERROR_CODE, '当初通貨コードが無効です' ERROR_DESC, '有効な通貨コードを入力してください。' REC_SOLUTION, '無効な通貨コードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_ORRC' ERROR_CODE, '無効な価格上書き事由です。' ERROR_DESC, 'リストから価格上書き事由を選択してください。' REC_SOLUTION, '無効な価格上書き事由です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_PRMT' ERROR_CODE, '販促タイプが無効です。' ERROR_DESC, 'リストから販促タイプを選択してください。' REC_SOLUTION, '販促タイプが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_REAC' ERROR_CODE, '無効な事由コードです。' ERROR_DESC, 'リストから事由コードを選択してください。' REC_SOLUTION, '無効な事由コードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_RTLOG_ORIG_SYS' ERROR_CODE, 'RTLOG作成システムが無効です。' ERROR_DESC, '無効なRTLOG作成システムでデータをインポートすることはできません。' REC_SOLUTION, '作成システムが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SACA' ERROR_CODE, '無効な顧客属性タイプです。' ERROR_DESC, 'リストから顧客属性タイプを選択してください。' REC_SOLUTION, '無効な顧客属性タイプです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SAIT' ERROR_CODE, '無効なアイテム・タイプです。' ERROR_DESC, 'リストからアイテム・タイプを選択してください。' REC_SOLUTION, '無効なアイテム・タイプです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SALESPERSON_ID' ERROR_CODE, '販売員POS IDに、対応する従業員IDが含まれていません。' ERROR_DESC, '従業員管理フォームに従業員IDおよびPOS IDを入力するか、「従業員」タブでPOS IDを訂正してください。' REC_SOLUTION, '無効な販売員IDです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SARR' ERROR_CODE, '無効な返品事由です。' ERROR_DESC, 'リストから返品事由を選択してください。' REC_SOLUTION, '無効な返品事由です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SASY' ERROR_CODE, '売上タイプが無効です。' ERROR_DESC, 'リストから売上タイプを選択してください。' REC_SOLUTION, '売上タイプが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SASY_E' ERROR_CODE, '「売上」、「取置」または「無効化」トランザクションの売上タイプが無効です。' ERROR_DESC, 'リストから売上タイプを選択してください。' REC_SOLUTION, '売上タイプが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SELLING_UOM' ERROR_CODE, 'アイテムの販売UOMが無効です。' ERROR_DESC, '標準UOMと適合する異なる販売UOMを選択するか、現在の販売UOMと標準UOMとの間の換算を定義してください。' REC_SOLUTION, '無効な販売UOMです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SYSTEM_IND' ERROR_CODE, '無効なシステム・インジケータです。' ERROR_DESC, 'リストからシステム・インジケータを選択してください。' REC_SOLUTION, '無効なシステムインジケータ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TAXC' ERROR_CODE, '税タイプが無効です。' ERROR_DESC, 'リストから税タイプを選択してください。' REC_SOLUTION, '税タイプが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TAX_AUTHORITY' ERROR_CODE, '税務当局が無効か、またはフィールドが空白です。' ERROR_DESC, '有効な税務当局を入力してください' REC_SOLUTION, '無効な税務当局です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TAX_IND' ERROR_CODE, '税インジケータが無効です。値は''Y''にデフォルト設定されました。' ERROR_DESC, '「税インジケータ」チェック・ボックスをオンにして、正しい値を設定してください。' REC_SOLUTION, '無効な税インジケータです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TENDER_ENDING' ERROR_CODE, '無効な決済終了です。' ERROR_DESC, '必要に応じて決済を調整してください。' REC_SOLUTION, '無効な決済終了です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TENT' ERROR_CODE, '無効な決済方法グループです。' ERROR_DESC, 'リストから決済方法グループを選択してください。' REC_SOLUTION, '無効な決済方法グループです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TRAN_DATETIME' ERROR_CODE, '無効なトランザクション日/時間です。' ERROR_DESC, '有効なトランザクション日/時間を入力してください。' REC_SOLUTION, '無効トランザクション日/時間' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TRAT' ERROR_CODE, '無効なトランザクション・タイプです。' ERROR_DESC, 'リストからトランザクション・タイプを選択してください。' REC_SOLUTION, '無効トランザクションタイプ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TSYS' ERROR_CODE, 'トランザクション処理システムが無効です。トランザクションが拒否ファイルに送信されました。' ERROR_DESC, '有効なトランザクション処理システムを入力してください。' REC_SOLUTION, '無効な処理システムです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_VATC' ERROR_CODE, '無効なVATコードです。' ERROR_DESC, '有効なVATコードを入力してください。' REC_SOLUTION, '無効なVATコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INV_RULE_DEF' ERROR_CODE, 'この規則の店舗日に対する計算が失敗しました。' ERROR_DESC, '規則定義を確認して、必要に応じて更新してください。' REC_SOLUTION, '無効な規則定義です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INV_TOTAL_DEF' ERROR_CODE, 'この合計の店舗日に対する計算が失敗しました。' ERROR_DESC, '合計定義を確認して、必要に応じて更新してください。' REC_SOLUTION, '合計定義が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'ITEMLVLTAX_NO_TTAX' ERROR_CODE, 'アイテム・レベルの税で構成された店舗にトランザクション・レベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからトランザクション・レベルの税レコードを削除してください。' REC_SOLUTION, '無効なTTAXレコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'ITEM_STATUS_REQ' ERROR_CODE, 'アイテム・ステータスを空白にすることはできません。' ERROR_DESC, 'リストからアイテム・ステータスを選択してください。' REC_SOLUTION, 'アイテム・ステータスなし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_CATT' ERROR_CODE, '釣銭準備金トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_CUST' ERROR_CODE, '釣銭準備金トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_DISC' ERROR_CODE, '釣銭準備金トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_IGTAX' ERROR_CODE, '釣銭準備金トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_ITEM' ERROR_CODE, '釣銭準備金トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_PYMT' ERROR_CODE, '釣銭準備金トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_TENDER_REQ' ERROR_CODE, '釣銭準備金トランザクションには、1つ以上の決済レコードが必須です。' ERROR_DESC, 'このトランザクションに決済レコードを追加してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_ITEM_REQ' ERROR_CODE, 'メーター・トランザクションには、1つ以上のアイテム・レコードが必須です。' ERROR_DESC, 'このトランザクションにアイテム・レコードを追加してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_CATT' ERROR_CODE, 'メーター・トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_CUST' ERROR_CODE, 'メーター・トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_IGTAX' ERROR_CODE, 'メーター・トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_PYMT' ERROR_CODE, 'メーター・トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'METER_NO_TAX' ERROR_CODE, 'メーター・トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効なメーター・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'MISSING_THEAD' ERROR_CODE, 'THEADレコードがありません。' ERROR_DESC, '入力ファイルに問題があります。THEADとTTAILレコードは、すべてのレコードに含まれている必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'THEADがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'MISSING_TRAN_BIG_GAP' ERROR_CODE, '多数のトランザクションが不明です。' ERROR_DESC, '入力ファイルに問題がある可能性があります。もう一度店舗をポーリングしてください。' REC_SOLUTION, '大きなトランザク・ギャップ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'MISSING_TTAIL' ERROR_CODE, 'TTAILレコードがありません。' ERROR_DESC, '入力ファイルに問題があります。THEADとTTAILレコードは、すべてのレコードに含まれている必要があります。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, 'TTAILがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NO_INV_RET_IND_REQ' ERROR_CODE, '売上タイプが外部顧客オーダーでOMSから作成された返品トランザクションには、在庫なしの返品インジケータが必要です。' ERROR_DESC, '正しい値を設定するには、「在庫なしの返品」ドロップダウン・リストから選択してください。' REC_SOLUTION, '無効な非在庫返品インジケ-タ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_CATT' ERROR_CODE, '非販売トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_CUST' ERROR_CODE, '非販売トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_DISC' ERROR_CODE, '非販売トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_IGTAX' ERROR_CODE, '非販売トランザクションには、アイテムレベルの税を含めないでください' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_ITEM' ERROR_CODE, '非販売トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_PYMT' ERROR_CODE, '非販売トランザクションに支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NOSALE_NO_TEND' ERROR_CODE, '非販売トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効な非販売レコードです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NO_RULE_FOR_SALETOTAL' ERROR_CODE, '総売上の終了金額のために定義された規則がありません。' ERROR_DESC, '総売上の終了金額のために定義された規則がsa_rounding_rule_detail表に定義されていることを確認してください。' REC_SOLUTION, '規則がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_CATT' ERROR_CODE, '未決済トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_CUST' ERROR_CODE, '未決済トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_DISC' ERROR_CODE, '未決済トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_IGTAX' ERROR_CODE, 'オープン・トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_ITEM' ERROR_CODE, '未決済トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_TAX' ERROR_CODE, '未決済トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_TEND' ERROR_CODE, '未決済トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'ORGCUR_AMT_WITHOUT_ORGCUR' ERROR_CODE, '当初通貨コードが設定されていない状態で、当初通貨での価額が存在します。' ERROR_DESC, '有効な当初通貨コードを入力してください。' REC_SOLUTION, '通貨コードがありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'ORGCUR_WITHOUT_ORGCUR_AMT' ERROR_CODE, '指定された当初通貨コードに対応する当初通貨の価額が存在しません。' ERROR_DESC, '正しい当初通貨の価額を入力してください。' REC_SOLUTION, '通貨の価額がありません' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_CATT' ERROR_CODE, '「支払済」トランザクションには、顧客属性レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客属性レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_CUST' ERROR_CODE, '「支払済」トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_DISC' ERROR_CODE, '「支払済」トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_TEND' ERROR_CODE, 'タンク・ディップ・トランザクションには、決済レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから決済レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'THEAD_IN_TRAN' ERROR_CODE, '不完全なトランザクションです。トランザクションにTHEADがあります。' ERROR_DESC, 'この入力ファイルに問題があるため、読み込めません。ファイルを編集するか、もう一度店舗をポーリングしてください。' REC_SOLUTION, '不完全なトランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TRAN_OUT_BAL' ERROR_CODE, 'トランザクションの決算が合いません。アイテムおよび支払額の合計が決済の合計と異なります。' ERROR_DESC, '必要に応じて、アイテムまたは決済または支払額を調整してください。' REC_SOLUTION, '残高不一致トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'CLOSE_NO_PYMT' ERROR_CODE, '決算トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効な決算トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'COND_NO_ITEM' ERROR_CODE, '店舗条件トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な店舗条件トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'DCLOSE_NO_CUST' ERROR_CODE, '日次決算トランザクションには、顧客レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから顧客レコードを削除してください。' REC_SOLUTION, '無効な日次決算トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'IDISC_DRN_STIN' ERROR_CODE, '無効な値引販促番号です。' ERROR_DESC, '有効な値引販促番号を入力してください。' REC_SOLUTION, '販促が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_IDMH' ERROR_CODE, '識別方式が無効です。' ERROR_DESC, '有効な識別方式をリストから入力してください。' REC_SOLUTION, '識別方式が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_RETURN_DISP' ERROR_CODE, '返品処分が無効です。' ERROR_DESC, '有効な返品処分を選択してください。' REC_SOLUTION, '返品処分が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SADT' ERROR_CODE, '無効な値引タイプです。' ERROR_DESC, 'リストから値引タイプを選択してください。' REC_SOLUTION, '無効な値引タイプです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SASI' ERROR_CODE, '無効なアイテム・ステータスです。' ERROR_DESC, 'リストからアイテム・ステータスを選択してください。' REC_SOLUTION, '無効なアイテム・ステータス' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_SUB_SACA' ERROR_CODE, '無効な顧客属性値です。' ERROR_DESC, 'リストから顧客属性値を選択してください。' REC_SOLUTION, '無効な顧客属性です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_TRAS' ERROR_CODE, '無効なサブ・トランザクション・タイプです。' ERROR_DESC, 'リストからサブ・トランザクション・タイプを選択してください。' REC_SOLUTION, '無効なサブトランザクタイプ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_VENDOR_NO' ERROR_CODE, '無効なベンダー番号です。' ERROR_DESC, '経費元ベンダーのパートナ表、または商品ベンダーのサプライヤ表からベンダーを指定してください。' REC_SOLUTION, '無効なベンダー' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'LOAN_NO_TAX' ERROR_CODE, '釣銭準備金トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な釣銭準備金トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NON_MERCH_ITEM_NOT_FOUND' ERROR_CODE, 'この非商品アイテムはRMSに存在しません。' ERROR_DESC, '適切な非商品アイテムを選択してください。' REC_SOLUTION, '非商品アイテム存在なし' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'NO_INV_RET_IND_RETURN' ERROR_CODE, '在庫なしの返品インジケータは、返品アイテム・ステータスに対してのみ有効です。' ERROR_DESC, 'インジケータを空白に設定してください。' REC_SOLUTION, '無効な非在庫返品インジケ-タ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'OPEN_NO_PYMT' ERROR_CODE, 'オープン・トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効なオープン・トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDIN_NO_ITEM' ERROR_CODE, '「支払済」トランザクションには、アイテム・レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションからアイテム・レコードを削除してください。' REC_SOLUTION, '無効な支払済トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PAIDOU_NO_DISC' ERROR_CODE, '「支払い登録」トランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な支払い登録トランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PULL_NO_DISC' ERROR_CODE, '引合いトランザクションには、値引レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから値引レコードを削除してください。' REC_SOLUTION, '無効な引合いトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PUMPT_NO_PYMT' ERROR_CODE, 'ポンプ・テスト・トランザクションには、支払レコードを含めないでください。' ERROR_DESC, 'このトランザクションから支払レコードを削除してください。' REC_SOLUTION, '無効ポンプテストトランザク' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'PVOID_NO_TAX' ERROR_CODE, '無効トランザクションには、税レコードを含めてはなりません。' ERROR_DESC, 'このトランザクションから税レコードを削除してください。' REC_SOLUTION, '無効な無効トランザクション' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETDISP_EXT_ORD' ERROR_CODE, '返品処分は外部顧客オーダーに対してのみ有効です。' ERROR_DESC, '返品処分を空白に設定してください。' REC_SOLUTION, '返品処分が無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'RETWH_NOT_FOUND' ERROR_CODE, '返品倉庫はRMSに存在しません。' ERROR_DESC, 'リストから倉庫を選択してください。' REC_SOLUTION, '無効な返品倉庫です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TANKDP_NO_IGTAX' ERROR_CODE, 'タンク・ディップ・トランザクションには、アイテム・レベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '無効タンクディップトランザ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'TOTAL_NO_IGTAX' ERROR_CODE, '合計トランザクションには、アイテムレベルの税レコードを含めないでください。' ERROR_DESC, 'このトランザクションからアイテムレベルの税レコードを削除してください。' REC_SOLUTION, '合計トランザクションが無効' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_VENDOR_NO_MV' ERROR_CODE, '無効な商品ベンダー番号です。' ERROR_DESC, '商品ベンダーのサプライヤ表のベンダーを使用してください。' REC_SOLUTION, '無効な商品ベンダーです' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 5 LANG, 'INVLD_VENDOR_NO_EV' ERROR_CODE, '無効な経費ベンダー番号です。' ERROR_DESC, '経費ベンダーのパートナ表のベンダーを使用してください。' REC_SOLUTION, '経費ベンダーが無効です' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO SA_ERROR_CODES base USING
( SELECT ERROR_CODE ERROR_CODE, ERROR_DESC ERROR_DESC, REC_SOLUTION REC_SOLUTION, SHORT_DESC SHORT_DESC FROM SA_ERROR_CODES_TL TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5)) USE_THIS
ON ( base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE SET base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
--
DELETE FROM SA_ERROR_CODES_TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
