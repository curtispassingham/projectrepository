SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'AK' STATE, 'US' COUNTRY_ID, 'アラスカ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'AL' STATE, 'US' COUNTRY_ID, 'アラバマ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'AR' STATE, 'US' COUNTRY_ID, 'アーカンソー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'AS' STATE, 'US' COUNTRY_ID, 'アメリカ領サモア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'AZ' STATE, 'US' COUNTRY_ID, 'アリゾナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'CA' STATE, 'US' COUNTRY_ID, 'カリフォルニア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'CO' STATE, 'US' COUNTRY_ID, 'コロラド' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'CT' STATE, 'US' COUNTRY_ID, 'コネティカット' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'DC' STATE, 'US' COUNTRY_ID, 'コロンビア特別区' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'DE' STATE, 'US' COUNTRY_ID, 'デラウェア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'FL' STATE, 'US' COUNTRY_ID, 'フロリダ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'GA' STATE, 'US' COUNTRY_ID, 'ジョージア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'GU' STATE, 'US' COUNTRY_ID, 'グアム' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'HI' STATE, 'US' COUNTRY_ID, 'ハワイ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'IA' STATE, 'US' COUNTRY_ID, 'アイオワ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'ID' STATE, 'US' COUNTRY_ID, 'アイダホ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'IL' STATE, 'US' COUNTRY_ID, 'イリノイ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'IN' STATE, 'US' COUNTRY_ID, 'インディアナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'KS' STATE, 'US' COUNTRY_ID, 'カンザス' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'KY' STATE, 'US' COUNTRY_ID, 'ケンタッキー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'LA' STATE, 'US' COUNTRY_ID, 'ルイジアナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MA' STATE, 'US' COUNTRY_ID, 'マサチューセッツ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MD' STATE, 'US' COUNTRY_ID, 'メリーランド' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'ME' STATE, 'US' COUNTRY_ID, 'メイン' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MI' STATE, 'US' COUNTRY_ID, 'ミシガン' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MN' STATE, 'US' COUNTRY_ID, 'ミネソタ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MO' STATE, 'US' COUNTRY_ID, 'ミズーリ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MP' STATE, 'US' COUNTRY_ID, '北マリアナ諸島' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MS' STATE, 'US' COUNTRY_ID, 'ミシシッピー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'MT' STATE, 'US' COUNTRY_ID, 'モンタナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NC' STATE, 'US' COUNTRY_ID, 'ノースカロライナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'ND' STATE, 'US' COUNTRY_ID, 'ノースダコタ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NE' STATE, 'US' COUNTRY_ID, 'ネブラスカ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NH' STATE, 'US' COUNTRY_ID, 'ニューハンプシャー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NJ' STATE, 'US' COUNTRY_ID, 'ニュージャージー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NM' STATE, 'US' COUNTRY_ID, 'ニューメキシコ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NV' STATE, 'US' COUNTRY_ID, 'ネバダ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'NY' STATE, 'US' COUNTRY_ID, 'ニューヨーク' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'OH' STATE, 'US' COUNTRY_ID, 'オハイオ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'OK' STATE, 'US' COUNTRY_ID, 'オクラホマ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'ON' STATE, 'CA' COUNTRY_ID, 'オンタリオ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'OR' STATE, 'US' COUNTRY_ID, 'オレゴン' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'PA' STATE, 'US' COUNTRY_ID, 'ペンシルバニア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'PR' STATE, 'US' COUNTRY_ID, 'プエルトリコ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'RI' STATE, 'US' COUNTRY_ID, 'ロードアイランド' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'SC' STATE, 'US' COUNTRY_ID, 'サウスカロライナ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'SD' STATE, 'US' COUNTRY_ID, 'サウスダコタ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'TN' STATE, 'US' COUNTRY_ID, 'テネシー' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'TX' STATE, 'US' COUNTRY_ID, 'テキサス' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'UM' STATE, 'US' COUNTRY_ID, '合衆国小周辺諸島' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'UT' STATE, 'US' COUNTRY_ID, 'ユタ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'VA' STATE, 'US' COUNTRY_ID, 'バージニア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'VI' STATE, 'US' COUNTRY_ID, 'バージン諸島、U.S.' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'VT' STATE, 'US' COUNTRY_ID, 'バーモント' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'WA' STATE, 'US' COUNTRY_ID, 'ワシントン' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'WI' STATE, 'US' COUNTRY_ID, 'ウィスコンシン' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 5 LANG, 'WV' STATE, 'US' COUNTRY_ID, 'ウェストバージニア' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO STATE base USING
( SELECT STATE STATE, COUNTRY_ID COUNTRY_ID, DESCRIPTION DESCRIPTION FROM STATE_TL TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5)) USE_THIS
ON ( base.STATE = use_this.STATE and base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM STATE_TL where lang = 5
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 5);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
