SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'AGG' UOM, 'AGG' UOM_TRANS, 'Lang2 Silbergehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'AUG' UOM, 'AUG' UOM_TRANS, 'Lang2 Goldgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BA' UOM, 'BA' UOM_TRANS, 'Lang2 Fass' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BBL' UOM, 'BBL' UOM_TRANS, 'Lang2 Fass' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BE' UOM, 'BE' UOM_TRANS, 'Lang2 Bündel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BG' UOM, 'BG' UOM_TRANS, 'Lang2 Beutel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BI' UOM, 'BI' UOM_TRANS, 'Lang2 Kasten' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BJ' UOM, 'BJ' UOM_TRANS, 'Lang2 Eimer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BK' UOM, 'BK' UOM_TRANS, 'Lang2 Korb' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'BX' UOM, 'UOM' UOM_TRANS, 'Lang2 Kiste' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'C' UOM, 'C' UOM_TRANS, 'Lang2 Celsius' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CA' UOM, 'CA' UOM_TRANS, 'Lang2 Dose' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CAR' UOM, 'KT' UOM_TRANS, 'Lang2 Karat' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CBM' UOM, 'CBM' UOM_TRANS, 'Lang2 Kubikmeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CC' UOM, 'CC' UOM_TRANS, 'Lang2 Kubikzentimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CFT' UOM, 'CFT' UOM_TRANS, 'Lang2 Kubikfuß' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CG' UOM, 'CG' UOM_TRANS, 'Lang2 Zentigramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CGM' UOM, 'CGM' UOM_TRANS, 'Lang2 Inhalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CKG' UOM, 'CKG' UOM_TRANS, 'Lang2 Inhalt - Kilogramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CM' UOM, 'CM' UOM_TRANS, 'Lang2 Zentimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CM2' UOM, 'CM2' UOM_TRANS, 'Lang2 Quadratzentimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CM3' UOM, 'CM3' UOM_TRANS, 'Lang2 Kubikzentimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CON' UOM, 'CON' UOM_TRANS, 'Lang2 Container' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CR' UOM, 'CR' UOM_TRANS, 'Lang2 Kiste' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CS' UOM, 'CS' UOM_TRANS, 'Lang2 Umverpackung' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CT' UOM, 'CT' UOM_TRANS, 'Lang2 Karton' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CTN' UOM, 'CTN' UOM_TRANS, 'Lang2 Inhalt - Tonnen' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CU' UOM, 'CU' UOM_TRANS, 'Lang2 Kubik' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CUR' UOM, 'CUR' UOM_TRANS, 'Lang2 Curie' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CY' UOM, 'CY' UOM_TRANS, 'Lang2 Clean Yield (CY)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CYG' UOM, 'CYG' UOM_TRANS, 'Lang2 Clean Yield (CY) Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'CYK' UOM, 'CYK' UOM_TRANS, 'Lang2 Clean Yield (CY) Kilogramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'D' UOM, 'D' UOM_TRANS, 'Lang2 Denier' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'DEG' UOM, 'GRAD' UOM_TRANS, 'Lang2 Grad' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'DOZ' UOM, 'AGG' UOM_TRANS, 'Lang2 Dutzend' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'DPC' UOM, 'DOZ' UOM_TRANS, 'Lang2 Dutzend Teile' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'DPR' UOM, 'DPR' UOM_TRANS, 'Lang2 Dutzend Paar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'DS' UOM, 'ADS' UOM_TRANS, 'Lang2 Dosis' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'EA' UOM, 'JE' UOM_TRANS, 'Lang2 Stück' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FBM' UOM, 'FBM' UOM_TRANS, 'Lang2 Faser - Meter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FIB' UOM, 'FIB' UOM_TRANS, 'Lang2 Fasern' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FT' UOM, 'FT' UOM_TRANS, 'Lang2 Fuß' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FT2' UOM, 'FT2' UOM_TRANS, 'Lang2 Quadratfuß' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FT3' UOM, 'FT3' UOM_TRANS, 'Lang2 Kubikfuß' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'FZ' UOM, 'FZ' UOM_TRANS, 'Lang2 Flüssigunzen' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'G' UOM, 'G' UOM_TRANS, 'Lang2 Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GBQ' UOM, 'GBQ' UOM_TRANS, 'Lang2 Gigabecquerel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GCN' UOM, 'GCN' UOM_TRANS, 'Lang2 Bruttocontainer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GKG' UOM, 'GKG' UOM_TRANS, 'Lang2 Goldgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GL' UOM, 'GL' UOM_TRANS, 'Lang2 Gallone' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GM' UOM, 'GM' UOM_TRANS, 'Lang2 Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GR' UOM, 'BR' UOM_TRANS, 'Lang2 Brutto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GRL' UOM, 'GRL' UOM_TRANS, 'Lang2 HTSUPLD Autom. einfügen' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GRS' UOM, 'BR' UOM_TRANS, 'Lang2 Brutto' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'GVW' UOM, 'GVW' UOM_TRANS, 'Lang2 Bruttogewicht Fahrzeug' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'HND' UOM, 'HND' UOM_TRANS, 'Lang2 Hundert' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'HUN' UOM, 'HUN' UOM_TRANS, 'Lang2 Hundert' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'HZ' UOM, 'HZ' UOM_TRANS, 'Lang2 Hertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'IN' UOM, 'IN' UOM_TRANS, 'Lang2 Zoll' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'IN2' UOM, 'IN2' UOM_TRANS, 'Lang2 Quadratzoll' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'IN3' UOM, 'IN3' UOM_TRANS, 'Lang2 Kubikzoll' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'IRC' UOM, 'IRC' UOM_TRANS, 'Lang2 Interner Umsatzcode' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'IRG' UOM, 'IRG' UOM_TRANS, 'Lang2 Iridiumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'JR' UOM, 'JR' UOM_TRANS, 'Lang2 Krug' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'JWL' UOM, 'JWL' UOM_TRANS, 'Lang2 Joule' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'K' UOM, 'K' UOM_TRANS, 'Lang2 1,000' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KCAL' UOM, 'KCAL' UOM_TRANS, 'Lang2 Kilokalorien' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KG' UOM, 'KG' UOM_TRANS, 'Lang2 Kilogramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KHZ' UOM, 'KHZ' UOM_TRANS, 'Lang2 Kilohertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KM' UOM, 'KM' UOM_TRANS, 'Lang2 Kilometer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KM2' UOM, 'KM2' UOM_TRANS, 'Lang2 Quadratkilometer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KM3' UOM, 'KM3' UOM_TRANS, 'Lang2 Kubikkilometer' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KN' UOM, 'KN' UOM_TRANS, 'Lang2 Kilonewton' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KPA' UOM, 'KPA' UOM_TRANS, 'Lang2 Kilopascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KSB' UOM, 'KSB' UOM_TRANS, 'Lang2 Kilo Standard Brick' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KTS' UOM, 'KTS' UOM_TRANS, 'Lang2 Gesamtzucker/Kilogramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KVA' UOM, 'KVA' UOM_TRANS, 'Lang2 Kilovoltampere' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KVAR' UOM, 'KVAR' UOM_TRANS, 'Lang2 Kilovoltampere - reaktiv' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KW' UOM, 'KW' UOM_TRANS, 'Lang2 Kilowatt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'KWH' UOM, 'KWH' UOM_TRANS, 'Lang2 Kilowattstunden' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'L' UOM, 'L' UOM_TRANS, 'Lang2 Liter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'LB' UOM, 'LB' UOM_TRANS, 'Lang2 Pfund' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'LBS' UOM, 'LBS' UOM_TRANS, 'Lang2 Pfund' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'LIN' UOM, 'LIN' UOM_TRANS, 'Lang2 Linear' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'LNM' UOM, 'LNM' UOM_TRANS, 'Lang2 Lineare Meter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'M' UOM, 'M' UOM_TRANS, 'Lang2 Meter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'M2' UOM, 'M2' UOM_TRANS, 'Lang2 Quadratmeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'M3' UOM, 'M3' UOM_TRANS, 'Lang2 Kubikmeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MBQ' UOM, 'MBQ' UOM_TRANS, 'Lang2 Megabecquerel' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MC' UOM, 'MC' UOM_TRANS, 'Lang2 Millicurie' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MG' UOM, 'MG' UOM_TRANS, 'Lang2 Milligramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MHZ' UOM, 'MHZ' UOM_TRANS, 'Lang2 Megahertz' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'ML' UOM, 'ML' UOM_TRANS, 'Lang2 Milliliter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MM' UOM, 'MM' UOM_TRANS, 'Lang2 Millimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MM2' UOM, 'MM2' UOM_TRANS, 'Lang2 Quadratmillimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MM3' UOM, 'MM3' UOM_TRANS, 'Lang2 Kubikmillimeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MPA' UOM, 'MPA' UOM_TRANS, 'Lang2 Megapascal' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'MWH' UOM, 'MWH' UOM_TRANS, 'Lang2 Megawattstunden' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'NA' UOM, 'NV' UOM_TRANS, 'Lang2 Nicht verfügbar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'NO' UOM, 'NR' UOM_TRANS, 'Lang2 Zahl' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'ODE' UOM, 'ODE' UOM_TRANS, 'Lang2 Ozone Depletion Equivalent (ODE)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'OSG' UOM, 'OSG' UOM_TRANS, 'Lang2 Osmiumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'OZ' UOM, 'OZ' UOM_TRANS, 'Lang2 Unze' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PACK' UOM, 'SET' UOM_TRANS, 'Lang2 Gebinde' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PAL' UOM, 'PAL' UOM_TRANS, 'Lang2 Palette' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PC' UOM, 'ST' UOM_TRANS, 'Lang2 Teil' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PCS' UOM, 'ST' UOM_TRANS, 'Lang2 Teile' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PDG' UOM, 'PDG' UOM_TRANS, 'Lang2 Palladiumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PF' UOM, 'PF' UOM_TRANS, 'Lang2 Proof' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PFL' UOM, 'PFL' UOM_TRANS, 'Lang2 Proof - Liter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PK' UOM, 'SET' UOM_TRANS, 'Lang2 Gebinde' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PO' UOM, 'PO' UOM_TRANS, 'Lang2 Topf' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PRS' UOM, 'PR' UOM_TRANS, 'Lang2 Paar' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PT' UOM, 'PT' UOM_TRANS, 'Lang2 Pint' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'PTG' UOM, 'PTG' UOM_TRANS, 'Lang2 Plutoniumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'QT' UOM, 'QT' UOM_TRANS, 'Lang2 Quart' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'RBA' UOM, 'RBA' UOM_TRANS, 'Lang2 Running Bales (RB)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'RHG' UOM, 'RHG' UOM_TRANS, 'Lang2 Rhodiumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'RPM' UOM, 'RPM' UOM_TRANS, 'Lang2 Umdrehungen pro Minute' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'RUG' UOM, 'RUG' UOM_TRANS, 'Lang2 Rutheniumgehalt - Gramm' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'SBE' UOM, 'SBE' UOM_TRANS, 'Lang2 Standard Brick Equivalent (SBE)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'SF' UOM, 'FT2' UOM_TRANS, 'Lang2 Quadratfuß' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'SME' UOM, 'SME' UOM_TRANS, 'Lang2 Square Meters Equivalent (SME)' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'SQ' UOM, 'SQ' UOM_TRANS, 'Lang2 Quadrat' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'SQM' UOM, 'M2' UOM_TRANS, 'Lang2 Quadratmeter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'T' UOM, 'F' UOM_TRANS, 'Lang2 Metrische Tonne' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'THM' UOM, 'THM' UOM_TRANS, 'Lang2 Tausend Meter' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'THS' UOM, 'THS' UOM_TRANS, 'Lang2 Tausend Einheiten' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'TNV' UOM, 'TNV' UOM_TRANS, 'Lang2 Tonne Rohwert' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'TON' UOM, 'TON' UOM_TRANS, 'Lang2 Tonnen' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'TS' UOM, 'TS' UOM_TRANS, 'Lang2 Amerikanische Tonne' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'V' UOM, 'V' UOM_TRANS, 'Lang2 Volt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'W' UOM, 'W' UOM_TRANS, 'Lang2 Watt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'WTS' UOM, 'WTS' UOM_TRANS, 'Lang2 Watt' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'X' UOM, 'X' UOM_TRANS, 'Lang2 Keine Einheiten erfasst' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'YD' UOM, 'YD' UOM_TRANS, 'Lang2 Yard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'YD2' UOM, 'YD2' UOM_TRANS, 'Lang2 Quadrat-Yard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 LANG, 'YD3' UOM, 'YD3' UOM_TRANS, 'Lang2 Kubik-Yard' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
