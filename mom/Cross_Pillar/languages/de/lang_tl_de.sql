SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 1 LANG_LANG, 'Englisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 2 LANG_LANG, 'Deutsch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 3 LANG_LANG, 'Französisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 4 LANG_LANG, 'Spanisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 5 LANG_LANG, 'Japanisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 6 LANG_LANG, 'Koreanisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 7 LANG_LANG, 'Russisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 8 LANG_LANG, 'Chinesisch (vereinfacht)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 9 LANG_LANG, 'Türkisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 10 LANG_LANG, 'Ungarisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 11 LANG_LANG, 'Chinesisch (traditionell)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 12 LANG_LANG, 'Portugiesisch (Brasilien)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 13 LANG_LANG, 'Arabisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 14 LANG_LANG, 'Französisch (Kanada)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 15 LANG_LANG, 'Kroatisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 16 LANG_LANG, 'Tschechisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 17 LANG_LANG, 'Dänisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 18 LANG_LANG, 'Niederländisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 19 LANG_LANG, 'Finnisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 20 LANG_LANG, 'Griechisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 21 LANG_LANG, 'Hebräisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 22 LANG_LANG, 'Italienisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 23 LANG_LANG, 'Spanisch (Lateinamerika)' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 24 LANG_LANG, 'Litauisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 25 LANG_LANG, 'Norwegisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 26 LANG_LANG, 'Polnisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 27 LANG_LANG, 'Portugiesisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 28 LANG_LANG, 'Rumänisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 29 LANG_LANG, 'Slowakisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 30 LANG_LANG, 'Slowenisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 31 LANG_LANG, 'Schwedisch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 32 LANG_LANG, 'Thai' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 2 LANG, 33 LANG_LANG, 'Tagalog' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO LANG base USING
( SELECT LANG_LANG LANG, DESCRIPTION DESCRIPTION FROM LANG_TL TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM LANG_TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
