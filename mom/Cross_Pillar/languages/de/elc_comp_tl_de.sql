SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'INSINT' COMP_ID, 'Internationale Versicherung' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, '200' COMP_ID, 'Luftfracht' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, '300' COMP_ID, 'Überlandfracht' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, '60' COMP_ID, 'Drittanbieterlogistik' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, '55' COMP_ID, 'Konsolidierungsgebühren' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'ICING' COMP_ID, 'Vereisungskosten' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'ORDCST' COMP_ID, 'EK Bestellung' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TEXP' COMP_ID, 'Gesamtkosten' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TEXPC' COMP_ID, 'Gesamtkosten - Kreis' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TEXPZ' COMP_ID, 'Gesamtkosten - Zone' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'UNCST' COMP_ID, 'EK/Einheit' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BAPAL' COMP_ID, 'Rücktransportrabatt pro Palette' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BACWT' COMP_ID, 'Rücktransportrabatt pro Hundred-Weight' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BAEA' COMP_ID, 'Rücktransportrabatt pro Stück' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BACS' COMP_ID, 'Rücktransportrabatt pro Umverpackung' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BACFT' COMP_ID, 'Rücktransportrabatt pro Kubikfuß' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TSFFRGHT' COMP_ID, 'Transfer Fracht' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TSFINSUR' COMP_ID, 'Transfer Versicherung' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'WHFEE' COMP_ID, 'Lagerungsgebühr Lager' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'WHPROC' COMP_ID, 'Bearbeitungsgebühr Lager' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'AGCOMM' COMP_ID, 'Agentenprovision' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'ILFRT' COMP_ID, 'Inlandsfracht' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'BUYCOMM' COMP_ID, 'Käuferprovision' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'OCFRT' COMP_ID, 'Ozeanfracht' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'INSUR' COMP_ID, 'Versicherung' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'SELLCOMM' COMP_ID, 'Verkäuferprovision' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'ROYALTY' COMP_ID, 'Gebühr' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'VFD25US' COMP_ID, '25% des Werts für US-Zoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'VFD50US' COMP_ID, '50% des Werts für US-Zoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'VFD75US' COMP_ID, '75% des Werts für US-Zoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'MPFUS' COMP_ID, 'US-Warenverarbeitungsgebühr' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TDTYPE' COMP_ID, 'Summe US-Zoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'HMFUS' COMP_ID, 'US-Hafenwartungsgebühr' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'ADUS' COMP_ID, 'US-Anti-Dumping' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'CVDUS' COMP_ID, 'US-Ausgleichszoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 2 LANG, 'TDTYUS' COMP_ID, 'Summe US-Zoll' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ELC_COMP base USING
( SELECT COMP_ID COMP_ID, COMP_DESC COMP_DESC FROM ELC_COMP_TL TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2)) USE_THIS
ON ( base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE SET base.COMP_DESC = use_this.COMP_DESC;
--
DELETE FROM ELC_COMP_TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
