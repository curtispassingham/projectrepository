SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 1 CODE, 'Nettoabsatz' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 2 CODE, 'Nettoabsatz exkl. USt' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 3 CODE, 'Absatz/Retouren Nicht-Bestandsartikel' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 4 CODE, 'Retouren' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 5 CODE, 'Absatz exkl. USt Nicht-Bestand' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 6 CODE, 'EK-Konditionen Einkommen (Absatz)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 7 CODE, 'EK-Kond. Einkommen (Einkauf)' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 8 CODE, 'Feste Einkommensrückstellung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 10 CODE, 'Gewichtsabweichung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 11 CODE, 'Preisaufschlag' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 12 CODE, 'Preisaufschlag Storno' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 13 CODE, 'Permanenter Nachlass' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 14 CODE, 'Nachlassstorno' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 15 CODE, 'Promotionnachlass' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 16 CODE, 'Räumungsnachlass' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 17 CODE, 'Zwischenbetrieblicher Preisaufschlag' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 18 CODE, 'Zwischenbetrieblicher Nachlass' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 20 CODE, 'Bestellungen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 21 CODE, 'Rechnung - nur für Oracle HB-Schnittstelle' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 22 CODE, 'Bestandskorrektur' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 23 CODE, 'Bestandskorrektur - HKU' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 24 CODE, 'Lieferantenretoure' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 25 CODE, 'Transfer von nicht verfügbarem Bestand' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 26 CODE, 'Fracht' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 27 CODE, 'QK LR' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 28 CODE, 'Gewinnaufschlag - EingStandort' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 29 CODE, 'Kostenaufschlag - EingStandort' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 30 CODE, 'Eingehende Transfers' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 31 CODE, 'Eingehende Umbuchungen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 32 CODE, 'Ausgehende Transfers' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 33 CODE, 'Ausgehende Umbuchungen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 34 CODE, 'Eingehende Reklassifizierungen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 36 CODE, 'Ausgehende Reklassifizierungen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 37 CODE, 'Zwischenbetrieblicher Eing.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 38 CODE, 'Zwischenbetrieblicher Ausg.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 39 CODE, 'Zwischenbetriebliche Spanne' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 41 CODE, 'Data Warehouse Bestandsbuchkorrektur' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 44 CODE, 'Data Warehouse Eingehender Transfereingang' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 50 CODE, 'Anfangsbestand' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 51 CODE, 'Schwund' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 52 CODE, 'Endbestand' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 53 CODE, 'Bruttospanne' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 54 CODE, 'HBH WaB' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 55 CODE, 'Absatzbetr. int. Best.aufn.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 56 CODE, 'Schwundbetr. int. Best.aufn.' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 57 CODE, 'Absatzbetr. Bstd.aufn. MbD' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 58 CODE, 'Schw.betr. Bstd.aufn. MbD' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 59 CODE, 'Bestandsaufnahme Buchbestand EK' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 60 CODE, 'Mitarbeiterrabatt' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 61 CODE, 'Bestandsaufnahme tats. Bestand EK/VK' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 62 CODE, 'Frachtbeanstandung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 63 CODE, 'FA-Aktivität - Bestand aktualisieren' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 64 CODE, 'FA-Aktivität - Buchen' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 65 CODE, 'Wiederauffüllungsgebühr' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 70 CODE, 'EK-Abweichung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 71 CODE, 'EK-Abweichung - VK-Abrechnung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 72 CODE, 'EK-Abweichung - EK-Abrechnung' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 73 CODE, 'EK-Abweichung - Empf. EK-Korr. FiFO' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 74 CODE, 'Erstattungsfähige Steuer an Zielstandort' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 75 CODE, 'Erstattungsfähige Steuer an Quellstandort' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 80 CODE, 'Umarbeit/Sonstige Absatzkosten' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 81 CODE, 'Skonto' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 82 CODE, 'Franchiseabsatz' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 83 CODE, 'Franchiseretouren' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 84 CODE, 'Franchisepreisaufschläge' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 85 CODE, 'Franchisenachlässe' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 86 CODE, 'Franchise-Wiederauffüllungsgebühr' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 87 CODE, 'USt. Eingang EK' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_TL TL USING
(SELECT  LANG,	CODE,  DECODE
FROM  (SELECT 2 LANG, 88 CODE, 'USt. Ausgang VK' DECODE FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES base where dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.DECODE = use_this.DECODE, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE, DECODE, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE, use_this.DECODE, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES base USING
( SELECT CODE CODE, DECODE DECODE FROM TRAN_DATA_CODES_TL TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2)) USE_THIS
ON ( base.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE SET base.DECODE = use_this.DECODE;
--
DELETE FROM TRAN_DATA_CODES_TL where lang = 2
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 2);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
