SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 29 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 30 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 31 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 32 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 33 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 34 TRAN_CODE, 'RECLASS_SQL.ITEM_PROCESS' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 36 TRAN_CODE, 'RECLASS_SQL.ITEM_PROCESS' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 37 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, 'From Location' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 38 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, 'To Location' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 41 TRAN_CODE, 'stkvar' PGM_NAME, '' REF_NO_1_DESC, 'Cycle (stock) count' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 44 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 50 TRAN_CODE, 'Freight Claim' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 60 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 63 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, 'Activity ID' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 64 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, 'Activity ID' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 65 TRAN_CODE, 'RTV_SQL.INVENTORY' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 65 TRAN_CODE, 'RTV_SQL.WRITE_RESTOCKING_FEE' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 65 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, 'To Location' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 65 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 70 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 70 TRAN_CODE, 'RECEIVE_SQL.ITEM' PGM_NAME, 'Order No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 70 TRAN_CODE, 'UPDATE_BASE_COST' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 70 TRAN_CODE, 'RECCTADJ.FMB' PGM_NAME, 'Order No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 70 TRAN_CODE, 'avcstadj.fmb' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 71 TRAN_CODE, 'INVC_SQL.WRITE_INVC_TOL_TRAN_DATA' PGM_NAME, 'Invoice No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 71 TRAN_CODE, 'STKLEDGR_SQL.POST_COST_VARIANCE' PGM_NAME, 'RTV Order No / Transfer No / Alloc No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 71 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 72 TRAN_CODE, 'STKLEDGR_SQL.POST_COST_VARIANCE' PGM_NAME, 'RTV Order No / Transfer No / Alloc No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 72 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 73 TRAN_CODE, 'recctadj.fmb' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 82 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 83 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 84 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 85 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 86 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'ORDER_RCV_SQL.STOCKLEDGER_INFO' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'ORDER_RCV_SQL.BACK_OUT_ALC' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'RTV_SQL.WRITE_TRAN_DATA' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'REC_COST_ADJ_SQL.ITEM' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'REC_UNIT_ADJ_SQL.CHECK_RECORDS' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'WF_BOL_SQL.SEND_TSF' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 82 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 82 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 84 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 84 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 85 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 85 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 87 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 83 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 83 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 83 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 84 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 85 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 86 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 88 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No/RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 24 TRAN_CODE, 'WF_RETURN_SQL.APPROVE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 24 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.DETAIL_PROCESSING' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 24 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.FORCED_OVERAGE' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 1 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 2 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 3 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 4 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 5 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 6 TRAN_CODE, 'DEALINC.PC' PGM_NAME, 'Deal ID' REF_NO_1_DESC, 'Stock Ledger Indicator' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 7 TRAN_CODE, 'DEALINC.PC' PGM_NAME, 'Deal ID' REF_NO_1_DESC, 'Stock Ledger Indicator' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 8 TRAN_CODE, 'DEALFINC.PC' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 10 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 11 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 11 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 12 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 13 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 13 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 14 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 15 TRAN_CODE, 'salesprocess' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 16 TRAN_CODE, 'RMSSUB_PRICECHANGE_UPDATE.PERSIST' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 17 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 18 TRAN_CODE, 'STKLEDGR_SQL.WRITE_FINANCIALS' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'RECEIVE_SQL.ITEM' PGM_NAME, 'Order No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'salesprocess' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'RECCTADJ.FMB' PGM_NAME, 'Order No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'ALC_SQL.UPDATE_STKLEDGR' PGM_NAME, 'Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 20 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'WF Order No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'INV_SQL.ADJ_TRAN_DATA' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'STKVAR.PC' PGM_NAME, 'Stock Count No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'INVADJ_SQL.ADJ_STOCK' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'INVADJSK' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'prodtsfm.fmb' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'invaupld.pc' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'INVADJST' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'wasteadj.pc' PGM_NAME, 'Reason Code' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'TRANSFER_IN_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'RMA No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'wasteadj.pc' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'RMA No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 24 TRAN_CODE, 'RTV_SQL.INVENTORY' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 24 TRAN_CODE, 'BOL_SQL.PROCESS_TSF' PGM_NAME, 'Distro Number' REF_NO_1_DESC, 'RMA No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'invaupld.pc' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'INVADJSK' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'INV_SQL.ADJ_TRAN_DATA' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'INVADJ_SQL.CHANGE_STATUS' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'TRANSFER_IN_SQL.EXECUTE' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'RTV_SQL.INVENTORY' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 25 TRAN_CODE, 'INVADJST' PGM_NAME, 'Inventory Status' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 27 TRAN_CODE, 'rtvupld.pc' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 27 TRAN_CODE, 'RTV.FMB' PGM_NAME, 'RTV Order No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 28 TRAN_CODE, 'TRANSFER_OUT_SQL.EXECUTE' PGM_NAME, 'Transfer No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'INVADJ_SQL.BUILD_PROCESS_INVADJ' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'TRANSFER_SQL.TRAN_DATA_WRITES' PGM_NAME, 'Transfer No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'ORDER_RCV_SQL.STOCKLEDGER_INFO' PGM_NAME, 'Order No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'RTV_SQL.INVENTORY' PGM_NAME, 'RTV No' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.TRANDATA_OVERAGE' PGM_NAME, 'Distro No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'STOCK_ORDER_RCV_SQL.PROC_STK_CNT_TD_WRITE' PGM_NAME, 'Distro No' REF_NO_1_DESC, 'Stock Count No' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'SOERBOL.FMB' PGM_NAME, 'Distro No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'STOCK_ORDER_RECONCILE_SQL.TO_LOC_SHORTAGE_NL_BL' PGM_NAME, 'Distro No' REF_NO_1_DESC, 'Shipment No' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 22 TRAN_CODE, 'UPDATE_SNAPSHOT_SQL.PROC_STK_CNT_TD_WRITE' PGM_NAME, 'Stock Count No' REF_NO_1_DESC, '' REF_NO_2_DESC, '' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'INVADJSK' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'INVADJST' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'INVADJ_SQL.ADJ_STOCK' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO TRAN_DATA_CODES_REF_TL TL USING
(SELECT  LANG,	TRAN_CODE,  PGM_NAME,  REF_NO_1_DESC,  REF_NO_2_DESC,  GL_REF_NO_DESC
FROM  (SELECT 1 LANG, 23 TRAN_CODE, 'INVADJ_SQL.BUILD_PROCESS_INVADJ' PGM_NAME, '' REF_NO_1_DESC, '' REF_NO_2_DESC, 'Reason Code' GL_REF_NO_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM TRAN_DATA_CODES_REF base where dl.TRAN_CODE = base.TRAN_CODE and dl.PGM_NAME = base.PGM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.TRAN_CODE = use_this.TRAN_CODE and tl.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.REF_NO_1_DESC = use_this.REF_NO_1_DESC, tl.REF_NO_2_DESC = use_this.REF_NO_2_DESC, tl.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, TRAN_CODE, PGM_NAME, REF_NO_1_DESC, REF_NO_2_DESC, GL_REF_NO_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.TRAN_CODE, use_this.PGM_NAME, use_this.REF_NO_1_DESC, use_this.REF_NO_2_DESC, use_this.GL_REF_NO_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO TRAN_DATA_CODES_REF base USING
( SELECT TRAN_CODE TRAN_CODE, PGM_NAME PGM_NAME, REF_NO_1_DESC REF_NO_1_DESC, REF_NO_2_DESC REF_NO_2_DESC, GL_REF_NO_DESC GL_REF_NO_DESC FROM TRAN_DATA_CODES_REF_TL TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.TRAN_CODE = use_this.TRAN_CODE and base.PGM_NAME = use_this.PGM_NAME)
WHEN MATCHED THEN UPDATE SET base.REF_NO_1_DESC = use_this.REF_NO_1_DESC, base.REF_NO_2_DESC = use_this.REF_NO_2_DESC, base.GL_REF_NO_DESC = use_this.GL_REF_NO_DESC;
--
DELETE FROM TRAN_DATA_CODES_REF_TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
