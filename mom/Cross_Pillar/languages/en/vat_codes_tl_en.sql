SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'C' VAT_CODE, 'Composite' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'E' VAT_CODE, 'Exempt' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'EXEMPT' VAT_CODE, 'Exempt' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'S' VAT_CODE, 'Standard' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'TOTTAX' VAT_CODE, 'Xstore tax' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT001' VAT_CODE, 'VAT001' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT002' VAT_CODE, 'VAT002' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT003' VAT_CODE, 'VAT003' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT004' VAT_CODE, 'VAT004' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT005' VAT_CODE, 'VAT005' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT006' VAT_CODE, 'VAT006' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT007' VAT_CODE, 'VAT007' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT008' VAT_CODE, 'VAT008' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT009' VAT_CODE, 'VAT009' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT010' VAT_CODE, 'VAT010' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT011' VAT_CODE, 'VAT011' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT012' VAT_CODE, 'VAT012' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT013' VAT_CODE, 'VAT013' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT014' VAT_CODE, 'VAT014' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT015' VAT_CODE, 'VAT015' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT016' VAT_CODE, 'VAT016' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT017' VAT_CODE, 'VAT017' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT018' VAT_CODE, 'VAT018' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT019' VAT_CODE, 'VAT019' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT020' VAT_CODE, 'VAT020' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT021' VAT_CODE, 'VAT021' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT022' VAT_CODE, 'VAT022' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT023' VAT_CODE, 'VAT023' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT024' VAT_CODE, 'VAT024' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT025' VAT_CODE, 'VAT025' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT026' VAT_CODE, 'VAT026' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT027' VAT_CODE, 'VAT027' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT028' VAT_CODE, 'VAT028' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT029' VAT_CODE, 'VAT029' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT030' VAT_CODE, 'VAT030' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT031' VAT_CODE, 'VAT031' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT032' VAT_CODE, 'VAT032' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT033' VAT_CODE, 'VAT033' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT034' VAT_CODE, 'VAT034' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT035' VAT_CODE, 'VAT035' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT036' VAT_CODE, 'VAT036' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT037' VAT_CODE, 'VAT037' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT038' VAT_CODE, 'VAT038' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT039' VAT_CODE, 'VAT039' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT040' VAT_CODE, 'VAT040' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT041' VAT_CODE, 'VAT041' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT042' VAT_CODE, 'VAT042' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT043' VAT_CODE, 'VAT043' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT044' VAT_CODE, 'VAT044' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT045' VAT_CODE, 'VAT045' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT046' VAT_CODE, 'VAT046' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT047' VAT_CODE, 'VAT047' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT048' VAT_CODE, 'VAT048' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT049' VAT_CODE, 'VAT049' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT050' VAT_CODE, 'VAT050' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT051' VAT_CODE, 'VAT051' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT052' VAT_CODE, 'VAT052' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT053' VAT_CODE, 'VAT053' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT054' VAT_CODE, 'VAT054' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT055' VAT_CODE, 'VAT055' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT056' VAT_CODE, 'VAT056' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT057' VAT_CODE, 'VAT057' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT058' VAT_CODE, 'VAT058' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT059' VAT_CODE, 'VAT059' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT060' VAT_CODE, 'VAT060' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT061' VAT_CODE, 'VAT061' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT062' VAT_CODE, 'VAT062' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT063' VAT_CODE, 'VAT063' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT064' VAT_CODE, 'VAT064' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT065' VAT_CODE, 'VAT065' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT066' VAT_CODE, 'VAT066' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT067' VAT_CODE, 'VAT067' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT068' VAT_CODE, 'VAT068' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT069' VAT_CODE, 'VAT069' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT070' VAT_CODE, 'VAT070' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT071' VAT_CODE, 'VAT071' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT072' VAT_CODE, 'VAT072' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT073' VAT_CODE, 'VAT073' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT074' VAT_CODE, 'VAT074' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT075' VAT_CODE, 'VAT075' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT076' VAT_CODE, 'VAT076' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT077' VAT_CODE, 'VAT077' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT078' VAT_CODE, 'VAT078' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT079' VAT_CODE, 'VAT079' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT080' VAT_CODE, 'VAT080' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT081' VAT_CODE, 'VAT081' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT082' VAT_CODE, 'VAT082' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT083' VAT_CODE, 'VAT083' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT084' VAT_CODE, 'VAT084' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT085' VAT_CODE, 'VAT085' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT086' VAT_CODE, 'VAT086' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT087' VAT_CODE, 'VAT087' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT088' VAT_CODE, 'VAT088' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT089' VAT_CODE, 'VAT089' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT090' VAT_CODE, 'VAT090' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT091' VAT_CODE, 'VAT091' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT092' VAT_CODE, 'VAT092' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT093' VAT_CODE, 'VAT093' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT094' VAT_CODE, 'VAT094' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT095' VAT_CODE, 'VAT095' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT096' VAT_CODE, 'VAT096' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT097' VAT_CODE, 'VAT097' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT098' VAT_CODE, 'VAT098' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT099' VAT_CODE, 'VAT099' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'VAT100' VAT_CODE, 'VAT100' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO VAT_CODES_TL TL USING
(SELECT  LANG,	VAT_CODE,  VAT_CODE_DESC
FROM  (SELECT 1 LANG, 'Z' VAT_CODE, 'Zero Vat' VAT_CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM VAT_CODES base where dl.VAT_CODE = base.VAT_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE  SET tl.VAT_CODE_DESC = use_this.VAT_CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, VAT_CODE, VAT_CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.VAT_CODE, use_this.VAT_CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO VAT_CODES base USING
( SELECT VAT_CODE VAT_CODE, VAT_CODE_DESC VAT_CODE_DESC FROM VAT_CODES_TL TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.VAT_CODE = use_this.VAT_CODE)
WHEN MATCHED THEN UPDATE SET base.VAT_CODE_DESC = use_this.VAT_CODE_DESC;
--
DELETE FROM VAT_CODES_TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
