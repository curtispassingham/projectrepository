SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_IGTAX' ERROR_CODE, 'Pull Out transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid pull out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_TAX' ERROR_CODE, 'Pull Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid pull transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_TENDER_REQ' ERROR_CODE, 'Pull Transactions are required to have at least one Tender record.' ERROR_DESC, 'Add a Tender record to this transaction.' REC_SOLUTION, 'Missing tender records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_ITEM_REQ' ERROR_CODE, 'Pump Test transactions are required to have at least one Item record.' ERROR_DESC, 'Add an Item Record to this transaction.' REC_SOLUTION, 'Missing item records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_CUST' ERROR_CODE, 'Pump Test transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_DISC' ERROR_CODE, 'Pump Test transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_IGTAX' ERROR_CODE, 'Pump Test transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_PYMT' ERROR_CODE, 'Pump Test transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_TAX' ERROR_CODE, 'Pump Test transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_CATT' ERROR_CODE, 'Post Void transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_CUST' ERROR_CODE, 'Post Void Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_DISC' ERROR_CODE, 'Post Void transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_IGTAX' ERROR_CODE, 'Post Void transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_ITEM' ERROR_CODE, 'Post Void Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_TAX' ERROR_CODE, 'Post Void Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_TEND' ERROR_CODE, 'Post Void Transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RECORDS_REJECTED' ERROR_CODE, 'Records have been rejected during the loading of this store/day.' ERROR_DESC, 'This input file is corrupt and can not be fully loaded. Either edit the file or repoll the store.' REC_SOLUTION, 'Rejected records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_NO_DISC' ERROR_CODE, 'Refund transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_NO_IGTAX' ERROR_CODE, 'Refund transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_NO_ITEM' ERROR_CODE, 'Refund transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_NO_PYMT' ERROR_CODE, 'Refund transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_TENDER_REQ' ERROR_CODE, 'Refund Transactions are required to have at least one tender record.' ERROR_DESC, 'Add a Tender Record to this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REGISTER_ID_REQ' ERROR_CODE, 'The Register ID is required because transactions are generated at the register level.' ERROR_DESC, 'Enter the correct Register ID.' REC_SOLUTION, 'Missing register ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REGISTER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'The Register ID is required because of the balancing level.' ERROR_DESC, 'Enter the correct Register ID.' REC_SOLUTION, 'Missing register ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETDISP_EXT_ORD' ERROR_CODE, 'Return Disposition is only valid for external customer orders.' ERROR_DESC, 'Set the Return Disposition to blank.' REC_SOLUTION, 'Invalid return disposition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETDISP_INV_RET' ERROR_CODE, 'Return Disposition is only valid if there is inventory associated with the return.' ERROR_DESC, 'Set Return Disposition to blank.' REC_SOLUTION, 'Invalid return disposition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETDISP_OMS' ERROR_CODE, 'Return Disposition should only be used with a return transaction that originated from and was processed in OMS.' ERROR_DESC, 'Set the Return Disposition to blank.' REC_SOLUTION, 'Invalid return disposition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETURN_DISP_REQ' ERROR_CODE, 'Return Disposition is required.' ERROR_DESC, 'Choose a valid Return Disposition.' REC_SOLUTION, 'Return disposition missing' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETURN_ITEM_REQ' ERROR_CODE, 'Return Transactions are required to have at least one Item record.' ERROR_DESC, 'Add an Item record to this transaction.' REC_SOLUTION, 'Invalid return transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETURN_NO_PYMT' ERROR_CODE, 'Return transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid return transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_DISC' ERROR_CODE, 'Pull transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid pull transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETURN_TENDER_REQ' ERROR_CODE, 'Return Transactions should have at least one Tender record.' ERROR_DESC, 'Add a Tender Record to this transaction.' REC_SOLUTION, 'Return missing tender' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETWH_NOT_FOUND' ERROR_CODE, 'Return Warehouse does not exist in RMS.' ERROR_DESC, 'Choose a Warehouse from the list.' REC_SOLUTION, 'Invalid return warehouse' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETWH_OMS' ERROR_CODE, 'Return Warehouse should only be used in a return transaction which originated from and was processed in OMS.' ERROR_DESC, 'Set the Return Warehouse to blank.' REC_SOLUTION, 'Invalid return warehouse' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETWH_REQ' ERROR_CODE, 'Return Warehouse is required for a return transaction whose sales type is External Customer Order and which originated from OMS.' ERROR_DESC, 'Add a valid warehouse to this transaction.' REC_SOLUTION, 'Invalid return warehouse' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'SALE_ITEM_REQ' ERROR_CODE, 'Sale Transactions are required to have at least one Item record.' ERROR_DESC, 'Add an Item Record to this transaction.' REC_SOLUTION, 'Missing item records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'SALE_TENDER_REQ' ERROR_CODE, 'Sale Transactions are required to have at least one Tender record.' ERROR_DESC, 'Add a Tender Record to this transaction.' REC_SOLUTION, 'Missing tender records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'SET_DROP_SHIP' ERROR_CODE, 'The drop shipment indicator was not correctly imported for this item.' ERROR_DESC, 'In the item detail section, verify the drop shipment indicator.' REC_SOLUTION, 'Invalid drop ship indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'SKU_NOT_FOUND' ERROR_CODE, 'The item is either empty or does not exist in RMS.' ERROR_DESC, 'Choose a item from the list.' REC_SOLUTION, 'Item not found' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'STOREDAY_LOCK_FAILED' ERROR_CODE, 'Cannot get a write lock for a store day. Either a locking record has not been loaded or the store day is locked by another process.' ERROR_DESC, 'Try again after the lock has been released.' REC_SOLUTION, 'Store Day lock failed' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_ITEM_REQ' ERROR_CODE, 'Tank Dip transactions are required to have at least one Item record.' ERROR_DESC, 'Add an Item Record to this transaction.' REC_SOLUTION, 'Missing item records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_CATT' ERROR_CODE, 'Tank Dip transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_CUST' ERROR_CODE, 'Tank Dip Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_DISC' ERROR_CODE, 'Tank Dip Transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_IGTAX' ERROR_CODE, 'Tank Dip transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_TEND' ERROR_CODE, 'Tank Dip Transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TCUST_FIL_STIN' ERROR_CODE, 'TCUST File Line Identifier - Non-numeric character in numeric field or the field my be empty.' ERROR_DESC, 'This input file is corrupt and cannot be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TCUST record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TCUST_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TCUST in illegal position.' ERROR_DESC, 'Input file is corrupt. A TCUST record must be between a THEAD and a TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TERM_MARKER_NO_ERROR' ERROR_CODE, 'This error is used to check that all data is loaded.' ERROR_DESC, 'Load all data and run saimptlogfin.' REC_SOLUTION, 'No error' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. THEAD in illegal position.' ERROR_DESC, 'This input file is corrupt and cannot be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_IN_TRAN' ERROR_CODE, 'Partial Transaction. THEAD in transaction.' ERROR_DESC, 'This input file is corrupt and cannot be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_LOC_NO_BANN' ERROR_CODE, 'Location had no Banner ID.' ERROR_DESC, 'The store has no associated banner ID.' REC_SOLUTION, 'Missing banner' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CASHIER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'The Cashier ID is required because of the balancing level.' ERROR_DESC, 'Enter the correct Cashier ID.' REC_SOLUTION, 'Invalid cashier ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CATT_FIL_STIN' ERROR_CODE, 'CATT record File Line Identifier - Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid CATT record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CATT_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. CATT record in illegal position.' ERROR_DESC, 'Input file is corrupt. A CATT record must be after a TCUST record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CC_NO_REQ' ERROR_CODE, 'Credit card number is required.' ERROR_DESC, 'Enter the correct credit card number.' REC_SOLUTION, 'Missing credit card number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_CATT' ERROR_CODE, 'Close transaction should not have a Customer Attribute record.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_CUST' ERROR_CODE, 'Close transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_DISC' ERROR_CODE, 'Close transaction should not have a Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_IGTAX' ERROR_CODE, 'Close transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_ITEM' ERROR_CODE, 'Close transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_TAX' ERROR_CODE, 'Close transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_TEND' ERROR_CODE, 'Close transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_CATT' ERROR_CODE, 'Store Conditions transaction should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_CUST' ERROR_CODE, 'Store Conditions transactions should not have a Customer record.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_DISC' ERROR_CODE, 'Store Conditions transaction should not have a Discount record.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_IGTAX' ERROR_CODE, 'Store Condition transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_PYMT' ERROR_CODE, 'Store Condition transactions should not have payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_TAX' ERROR_CODE, 'Store Conditions transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_TEND' ERROR_CODE, 'Store Conditions transactions should not have tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CREDIT_PROMO_ID_STIN' ERROR_CODE, 'Invalid Credit Promotion Id.' ERROR_DESC, 'Enter the correct the Credit Promotion Id' REC_SOLUTION, 'Invalid credit promotion' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CUST_ID_REQ' ERROR_CODE, 'A Customer Identification Number required.' ERROR_DESC, 'Enter the correct Customer Identification Number.' REC_SOLUTION, 'Missing customer ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CUST_ORD_DATE_REQDINTITEM' ERROR_CODE, 'Customer Order Date required in its corresponding TITEM.' ERROR_DESC, 'Enter the Customer Order Date via the Customer Attributes screen. The format expected is YYYYMMDD' REC_SOLUTION, 'Missing customer order date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DATAUNEXPECTEDSTOREDAY' ERROR_CODE, 'Data is not expected for this store day.' ERROR_DESC, 'Invalid store or business date. Check the store, company close, company close exception and store close information.' REC_SOLUTION, 'Unexpected data for store day' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_CATT' ERROR_CODE, 'Day Close transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_CUST' ERROR_CODE, 'Day Close transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_DISC' ERROR_CODE, 'Day Close transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_ITEM' ERROR_CODE, 'Day Close transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_PYMT' ERROR_CODE, 'Day Close transactions should not have payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_TEND' ERROR_CODE, 'Day Close transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DISC_REASON_REQ' ERROR_CODE, 'Discount reason cannot be blank for a Store discount.' ERROR_DESC, 'Enter a Discount reason.' REC_SOLUTION, 'Missing discount reason' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DISC_REF_NO_REQ' ERROR_CODE, 'Discount reference number cannot be blank for a Promotion' ERROR_DESC, 'Enter a Discount reference number.' REC_SOLUTION, 'Missing discount reference' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DUP_REC' ERROR_CODE, 'Duplicate Record. The transaction has been sent to the reject file.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Duplicate transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DUP_TAXC_COMPITEM' ERROR_CODE, 'Duplicate Tax Code - Component Item number combination. The transaction has been sent to the reject file.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Duplicate tax code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DUP_TRAN' ERROR_CODE, 'Duplicate Transaction Number. The transaction has been sent to the reject file.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Duplicate transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'EEXCH_ITEM_REQ' ERROR_CODE, 'Exchange transactions are required to have at least two Item records.' ERROR_DESC, 'Add one or more Item records to this transaction.' REC_SOLUTION, 'Missing item records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'EEXCH_NO_PYMT' ERROR_CODE, 'Even Exchange transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid payment record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FHEAD_FIL_STIN' ERROR_CODE, 'FHEAD File Line Identifier- Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FHEAD record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FHEAD_NOT_FIRST' ERROR_CODE, 'FHEAD is not the first record in the file.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FHEAD record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FILE_ERROR' ERROR_CODE, 'An error occurred during the last file operation.' ERROR_DESC, '' REC_SOLUTION, 'File error' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FTAIL_FIL_STIN' ERROR_CODE, 'FTAIL File Line Identifier - Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FTAIL_IN_TRAN' ERROR_CODE, 'Partial Transaction. There is a FTAIL record in a transaction.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FTAIL_NOT_LAST' ERROR_CODE, 'FTAIL is not the last record in the file.' ERROR_DESC, 'Input file does not have an FTAIL record.  Insert an FTAIL record into input file.' REC_SOLUTION, 'Missing FTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FULFILL_ORDER_NO_REQ' ERROR_CODE, 'The fulfillment order number is required.' ERROR_DESC, 'Enter the correct fulfillment order number.' REC_SOLUTION, 'Missing fulfillment order number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'GARBAGE_IN_RECORD' ERROR_CODE, 'Embedded garbage in record.' ERROR_DESC, 'Input file is corrupt. There is an embedded character in the record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Garbage in record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_COUPON_NO_REQ' ERROR_CODE, 'Coupon Number cannot be empty when Discount Type is Store Coupon.' ERROR_DESC, 'Enter a Coupon Number.' REC_SOLUTION, 'Missing coupon number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_DRN_STIN' ERROR_CODE, 'Invalid Discount Promotion Number.' ERROR_DESC, 'Enter a valid Discount Promotion Number.' REC_SOLUTION, 'Invalid promotion' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_FIL_STIN' ERROR_CODE, 'IDISC File Line Identifier Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and cannot be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid IDISC identifier' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_INVALID_CW' ERROR_CODE, 'Discount Catch Weight Indicator - The Catch Weight Indicator is not set for a weight item.' ERROR_DESC, 'Enter the correct Catch Weight Indicator' REC_SOLUTION, 'Invalid catch weight indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CUST_ORD_ATTR_REQ' ERROR_CODE, 'Customer Order attributes are required.' ERROR_DESC, 'Enter the Customer Order Attributes via the Customer Attributes screen.' REC_SOLUTION, 'Missing customer attributes' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. IDISC record in illegal position.' ERROR_DESC, 'This input file is corrupt. An IDISC record must be after a TITEM record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_PRC_STIN' ERROR_CODE, 'Invalid Discount Promotion Component' ERROR_DESC, 'Enter a valid discount promotion component' REC_SOLUTION, 'Invalid promotion component' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_QTY_STIN' ERROR_CODE, 'Discount Quantity - Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'Enter a valid Discount Quantity.' REC_SOLUTION, 'Invalid discount quantity' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_SET_CW' ERROR_CODE, 'Discount Catch Weight Indicator - The Catch Weight Indicator was not correctly imported' ERROR_DESC, 'Enter the correct Catch Weight Indicator' REC_SOLUTION, 'Invalid catch weight indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_UDA_STIN' ERROR_CODE, 'Discount Amount - Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'Enter a valid Discount Amount.' REC_SOLUTION, 'Invalid discount amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDISC_UOM_QTY_STIN' ERROR_CODE, 'Discount UOM Quantity - Non-numeric character in numeric field' ERROR_DESC, 'Enter the correct UOM Quantity' REC_SOLUTION, 'Invalid discount UOM' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDNT_ID_WITHOUT_IDNT_MTHD' ERROR_CODE, 'Identification is present with no identification method.' ERROR_DESC, 'Enter a valid identification method from the list.' REC_SOLUTION, 'Missing identification method' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IGTAX_FIL_STIN' ERROR_CODE, 'IGTAX File Line Identifier. Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid IGTAX identifier' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IGTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. IGTAX record in illegal position' ERROR_DESC, 'Input file is corrupt. An IGTAX record must be after a TITEM-IDISC record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid IGTAX record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IGTAX_RAT_STIN' ERROR_CODE, 'IGTAX Rate - Non-numeric character in numeric field or filed is empty' ERROR_DESC, 'Enter the correct IGTAX rate.' REC_SOLUTION, 'Invalid IGTAX rate' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_BANNER_ID' ERROR_CODE, 'Invalid banner ID.' ERROR_DESC, 'Choose a valid Banner ID.' REC_SOLUTION, 'Invalid banner' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_BIRTHDATE' ERROR_CODE, 'Invalid birth date.' ERROR_DESC, 'The valid format for a birth date is YYYYMMDD.' REC_SOLUTION, 'Invalid birth date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_BUSINESS_DATE' ERROR_CODE, 'Invalid or missing business date.' ERROR_DESC, 'The valid format for a business date is YYYYMMDD.' REC_SOLUTION, 'Invalid business date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CASHIER_ID' ERROR_CODE, 'The Cashier POS ID does not have a corresponding Employee ID.' ERROR_DESC, 'Enter the Employee ID and POS ID in the Employee Maintenance Form.' REC_SOLUTION, 'Invalid cashier ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CCAS' ERROR_CODE, 'Invalid Credit Card Authorization Source.' ERROR_DESC, 'Choose a Credit Card Authorization Source from the list.' REC_SOLUTION, 'Invalid authorization source' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CCEM' ERROR_CODE, 'Invalid Credit Card Entry Mode.' ERROR_DESC, 'Choose a Credit Card Entry Mode from the list.' REC_SOLUTION, 'Invalid credit card entry mode' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CCSC' ERROR_CODE, 'Invalid Credit Card Special Condition.' ERROR_DESC, 'Choose a Credit Card Special Condition from the list.' REC_SOLUTION, 'Invalid condition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CCVF' ERROR_CODE, 'Invalid Credit Card Holder Verification.' ERROR_DESC, 'Choose a Credit Card Holder Verification from the list.' REC_SOLUTION, 'Invalid verification' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CC_CHECKSUM' ERROR_CODE, 'Invalid credit card check digit. This card number is invalid.' ERROR_DESC, 'Enter the correct credit card number.' REC_SOLUTION, 'Invalid check digit' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CC_EXP_DATE' ERROR_CODE, 'Invalid expiration date.' ERROR_DESC, 'The valid format for expiration date is YYYYMMDD.' REC_SOLUTION, 'Invalid expiration date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CC_MASK' ERROR_CODE, 'Invalid credit card mask value.' ERROR_DESC, 'Enter the correct credit card mask value.' REC_SOLUTION, 'Invalid credit card mask value' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CC_PREFIX' ERROR_CODE, 'The numeric prefix of this Credit Card is not within the specified range.' ERROR_DESC, 'Enter the correct Credit Card number.' REC_SOLUTION, 'Invalid credit card prefix' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CHECK_NO' ERROR_CODE, 'Invalid Check number or non-numeric character in numeric-field.' ERROR_DESC, 'Enter the correct check number.' REC_SOLUTION, 'Invalid check number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CIDT' ERROR_CODE, 'Invalid Customer Identification Type.' ERROR_DESC, 'Choose a Customer Identification Type from the list.' REC_SOLUTION, 'Invalid customer ID type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CUST_ORD_DATE' ERROR_CODE, 'Invalid Customer Order Date.' ERROR_DESC, 'The valid format for customer order date is YYYYMMDD.' REC_SOLUTION, 'Invalid customer order date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_DISCOUNT_UOM' ERROR_CODE, 'The discount unit of measure is not valid for the item.' ERROR_DESC, 'Select a different unit of measure that is compatible with the standard unit of measure, or define the conversion between the current discount unit of measure and the standard unit of measure.' REC_SOLUTION, 'Invalid discount UOM' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_DOCUMENT' ERROR_CODE, 'A Gift Certificate/Voucher Number is required.' ERROR_DESC, 'Enter the Gift Certificate/Voucher Number.' REC_SOLUTION, 'Invalid document' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_EXP_DATE' ERROR_CODE, 'Invalid Expiration Date for a Voucher.' ERROR_DESC, 'Enter a valid Expiration Date. The valid format for expiration date is YYYMMDD.' REC_SOLUTION, 'Invalid expiration date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_FTAIL_POS' ERROR_CODE, 'FTAIL record is in the incorrect position.' ERROR_DESC, 'The Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_FTAIL_TRAN_CNT' ERROR_CODE, 'FTAIL Transaction Number Count is incorrect.' ERROR_DESC, 'The Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_ITEM_NO' ERROR_CODE, 'Item Number is empty.' ERROR_DESC, 'Choose an Item Number from the list.' REC_SOLUTION, 'Invalid item' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_ITEM_SWIPED_IND' ERROR_CODE, 'Invalid Item Swiped Indicator. The value has been defaulted to ''Y''.' ERROR_DESC, 'Click the Item Swiped check box to set the correct value.' REC_SOLUTION, 'Invalid item swiped indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_NO_INV_RET_IND' ERROR_CODE, 'Invalid Return Without Inventory Indicator. The value has been defaulted to blank.' ERROR_DESC, 'Choose from the Return Without Inventory drop down list to set the correct value.' REC_SOLUTION, 'Invalid non-inventory return indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_NON_MERCH_CODE' ERROR_CODE, 'Invalid reason code for an Expense Vendor Paid Out or Merchandise Vendor Paid Out transaction.' ERROR_DESC, 'Select a valid reason code for the transaction.	Valid values are found on must come from the non_merch_code_head table.' REC_SOLUTION, 'Invalid non-merch code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_ORRC' ERROR_CODE, 'Invalid price Override Reason.' ERROR_DESC, 'Choose a Price Override Reason from the list.' REC_SOLUTION, 'Invalid price override reason' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_REAC' ERROR_CODE, 'Invalid Reason Code.' ERROR_DESC, 'Choose a Reason Code from the list.' REC_SOLUTION, 'Invalid reason code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_RTLOG_ORIG_SYS' ERROR_CODE, 'Invalid RTLOG Originating System.' ERROR_DESC, 'It is not possible to import data with an invalid RTLOG Originating System.' REC_SOLUTION, 'Invalid originating system' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SADT' ERROR_CODE, 'Invalid Discount Type.' ERROR_DESC, 'Choose a Discount Type from the list.' REC_SOLUTION, 'Invalid discount type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'IDNT_MTHD_WITHOUT_IDNT_ID' ERROR_CODE, 'No identification for this identification method.' ERROR_DESC, 'Enter a valid identification.' REC_SOLUTION, 'Missing identification' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CCARD_GREATER_100' ERROR_CODE, 'Credit card tenders should have transaction value greater than 100' ERROR_DESC, 'Verify the tender amt is greater than 100 for CCARD tender type.' REC_SOLUTION, 'Credit card tenders should have transact' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_OTN_STIN' ERROR_CODE, 'Original POS Tran No - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This value was set to -1 to facilitate data loading. Enter the original POS Tran No that is being voided.' REC_SOLUTION, 'Invalid POS transaction number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_RAMT_STIN' ERROR_CODE, 'Rounded Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Rounded Amount.' REC_SOLUTION, 'Invalid rounded amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_ROAMT_STIN' ERROR_CODE, 'Rounded Off Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Rounded off Amount.' REC_SOLUTION, 'Invalid rounded off amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_TRN_STIN' ERROR_CODE, 'THEAD Transaction Number - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This value was set to -1 to facilitate data loading. Enter the POS Tran No for this transaction.' REC_SOLUTION, 'Invalid THEAD number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_CLS_STIN' ERROR_CODE, 'SKU Class - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct SKU Class.' REC_SOLUTION, 'Invalid class' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_CUST_ORD_STIN' ERROR_CODE, 'Customer Order Line Number - Non-numeric character in numeric field' ERROR_DESC, 'Enter the correct Customer Order Line Number via the Customer Attributes screen' REC_SOLUTION, 'Invalid customer order line' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_FIL_STIN' ERROR_CODE, 'TITEM File Line Identifier - Non-numeric character in numeric field or the field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TITEM number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_INVALID_CW' ERROR_CODE, 'Item Catch Weight Indicator - The Catch Weight Indicator is not set for a weight item.' ERROR_DESC, 'In the item detail section, verify the catch weight indicator.' REC_SOLUTION, 'Invalid catch weight indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TITEM in illegal position.' ERROR_DESC, 'Input file is corrupt. A TITEM record must be between a THEAD and a TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_MID_STIN' ERROR_CODE, 'Media ID - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Media ID via the Customer Attributes screen.' REC_SOLUTION, 'Invalid media ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_OUR_STIN' ERROR_CODE, 'Original Unit Retail - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Original Until Retail.' REC_SOLUTION, 'Invalid original unit retail' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_QTY_SIGN' ERROR_CODE, 'Item statuses Sale, Order Initiate (ORI), Order Complete (ORD), Layaway Initiate (LIN), or Layaway Complete (LCO) should be positive.  Item status Return should be negative. Item status Void should have a sign opposite that of the voided item' ERROR_DESC, 'Modify the sign of the Item Quantity.' REC_SOLUTION, 'Invalid item quantity sign' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_QTY_STIN' ERROR_CODE, 'Item Quantity - Non-numeric character in numeric field or the field is empty.' ERROR_DESC, 'Enter the correct Item Quantity. Item records with an Item Status of Sale should have a positive Quantity, those with Item Status of Return should have a negative Quantity. If the Item Status is Void, the sign should be opposite that of the voided item.' REC_SOLUTION, 'Invalid item quantity' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_SBC_STIN' ERROR_CODE, 'Subclass - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Subclass.' REC_SOLUTION, 'Invalid subclass' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_SET_CW' ERROR_CODE, 'Item Catch Weight Indicator - The Catch Weight Indicator was not correctly imported for this item' ERROR_DESC, 'In the item detail section, verify the catch weight indicator.' REC_SOLUTION, 'Invalid catch weight indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_SUP_STIN' ERROR_CODE, 'UPC Supplement - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct UPC Supplement.' REC_SOLUTION, 'Invalid UPC supplement' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_UOM_QTY_STIN' ERROR_CODE, 'Item UOM Quantity - Non-numeric character in numeric field' ERROR_DESC, 'Enter the correct UOM Quantity' REC_SOLUTION, 'Invalid UOM quantity' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_UOM_QTY_ZERO' ERROR_CODE, 'For catch weight items records with an Item Status of Sale or Return, UOM quantity cannot be zero if the quantity is not zero and vice-versa.' ERROR_DESC, 'Enter an item UOM quantity.' REC_SOLUTION, 'Invalid UOM quantity' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_URT_STIN' ERROR_CODE, 'Unit Retail - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Unit Retail.' REC_SOLUTION, 'Invalid unit retail' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_WH_STIN' ERROR_CODE, 'Return Warehouse - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Return Warehouse.' REC_SOLUTION, 'Invalid return warehouse' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOO_MANY_FHEADS' ERROR_CODE, 'Additional FHEAD record encountered.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Too many FHEAD records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_IGTAX_AMT_GT_TOTRET' ERROR_CODE, 'The Total IGTAX amount greater than the Total Retail.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid IGTAX amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_IGTAX_AMT_STIN' ERROR_CODE, 'Total IGTAX AMT. Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'Enter correct Total IGTAX Amount.' REC_SOLUTION, 'Invalid IGTAX amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_CATT' ERROR_CODE, 'Total transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_DISC' ERROR_CODE, 'Total transactions should not have a Discount records.' ERROR_DESC, 'Delete the Total record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_IGTAX' ERROR_CODE, 'Total transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_ITEM' ERROR_CODE, 'Total Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_PYMT' ERROR_CODE, 'Total transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_TAX' ERROR_CODE, 'Total Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_TEND' ERROR_CODE, 'Total transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_REF_NO_1_REQ' ERROR_CODE, 'Total Transactions are required to have the Total ID in the Reference Number 1 field.' ERROR_DESC, 'Enter the Total ID in the Reference Number 1 field.' REC_SOLUTION, 'Missing reference number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_VALUE_REQ' ERROR_CODE, 'Total Transactions are required to have a Total Value.' ERROR_DESC, 'Enter the Total Value.' REC_SOLUTION, 'Missing total value' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TPYMT_AMT_STIN' ERROR_CODE, 'Payment Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter correct payment amount' REC_SOLUTION, 'Invalid payment amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TPYMT_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TPYMT record in illegal position' ERROR_DESC, 'Input file is corrupt. A TPYMT record must be before a TTEND record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_MISS_REC' ERROR_CODE, 'Transaction is missing sub-records.' ERROR_DESC, 'The input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Missing sub-records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_NOT_RECOGNIZED' ERROR_CODE, 'Transaction Type is not recognized.' ERROR_DESC, 'Input file is corrupt. Check the TLOG to RTLOG conversion process or re-poll the store.' REC_SOLUTION, 'Invalid transaction type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_NO_REQ' ERROR_CODE, 'The Transaction Number is required.' ERROR_DESC, 'Enter the correct Transaction Number.' REC_SOLUTION, 'Missing transaction number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_VAL_STIN' ERROR_CODE, 'Value - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct value.' REC_SOLUTION, 'Invalid value' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_OUT_BAL' ERROR_CODE, 'Transaction is out of balance. The total of the items and payment amount does not equal the total of the tenders.' ERROR_DESC, 'Adjust the Items or Tender or Payment Amount as required.' REC_SOLUTION, 'Transaction out of balance' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_XTRA_CUST' ERROR_CODE, 'A transaction may only have one Customer record.' ERROR_DESC, 'Delete the extra Customer record from this transaction.' REC_SOLUTION, 'Invalid customer record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_TAX' ERROR_CODE, 'Day Close transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRAN_XTRA_REC' ERROR_CODE, 'Unknown record type found.' ERROR_DESC, 'Input file is corrupt. The valid record types are FHEAD, THEAD, TCUST, CATT, TITEM, IDISC, TTEND, TTAX, TTAIL and FTAIL. Either edit the file or re-poll the store.' REC_SOLUTION, 'Unknown record type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TRANLVLTAX_NO_IGTAX' ERROR_CODE, 'A store configured with a Transaction-level Tax should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid IGTAX record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAIL_FIL_STIN' ERROR_CODE, 'TTAIL File Line Identifier - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAIL_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TTAIL in illegal position.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAIL_TRC_STIN' ERROR_CODE, 'TTAIL Transaction Record Counter - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAIL_WITHOUT_THEAD' ERROR_CODE, 'Partial Transaction. TTAIL outside of transaction' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAX_AMT_STIN' ERROR_CODE, 'Tax Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Tax Amount.' REC_SOLUTION, 'Invalid tax amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAX_FIL_STIN' ERROR_CODE, 'TTAX File Line Identifier - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TTAX record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TTAX in illegal position.' ERROR_DESC, 'Input file is corrupt. A TTAX record must be between a THEAD and a TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_CHECK_NO_REQ' ERROR_CODE, 'Check number is empty or non-numeric character in numeric-field.' ERROR_DESC, 'Enter the correct check number.' REC_SOLUTION, 'Invalid check number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_COUPON_NO_REQ' ERROR_CODE, 'Coupon Number cannot be empty when Tender Type Group is Coupon.' ERROR_DESC, 'Enter a coupon.' REC_SOLUTION, 'Missing coupon number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_FIL_STIN' ERROR_CODE, 'TTEND File Line Identifier - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TTEND record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_IN_ILLEGAL_POS' ERROR_CODE, 'Partial Transaction. TTEND in illegal position.' ERROR_DESC, 'Input file is corrupt. A TTEND record must be between a THEAD and a TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_OCA_STIN' ERROR_CODE, 'Original Currency Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter correct original currency amount.' REC_SOLUTION, 'Invalid currency amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_TAM_STIN' ERROR_CODE, 'Tender Amount - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'Enter the correct Tender Amount.' REC_SOLUTION, 'Invalid tender amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TTEND_TTI_STIN' ERROR_CODE, 'Tender Type ID is not part of the Tender Type Group or it is empty.' ERROR_DESC, 'Choose a Tender Type from the list.' REC_SOLUTION, 'Invalid tender type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'UPC_NOT_FOUND' ERROR_CODE, 'This UPC does not exist in the RMS.' ERROR_DESC, 'Choose a UPC from the list.' REC_SOLUTION, 'UPC not found' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'VENDOR_DATA_REQ' ERROR_CODE, 'Data for a Vendor required - At least one of the following fields must have a value: Vendor Invoice Number, Payment Reference Number and Proof of Delivery Number.' ERROR_DESC, 'Enter at least one of the following values: Vendor Invoice Number, Payment Reference Number and Proof of Delivery Number.' REC_SOLUTION, 'Missing vendor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'VENDOR_NO_REQ' ERROR_CODE, 'Vendor Number on THEAD is a required field for a Merchandise Vendor Paid Out or Expense Vendor Paid Out.' ERROR_DESC, 'Choose a Vendor Number from list.' REC_SOLUTION, 'Missing vendor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'VOID_NO_PYMT' ERROR_CODE, 'Void transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DUP_TAXC' ERROR_CODE, 'Duplicate Tax Code. The transaction has been sent to the reject file.' ERROR_DESC, 'Input file is corrupt. Either edit the file or re-poll the store.' REC_SOLUTION, 'Duplicate tax code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FTAIL_FRC_STIN' ERROR_CODE, 'FTAIL Record Counter - Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid FTAIL record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_IDMH' ERROR_CODE, 'Invalid identification method.' ERROR_DESC, 'Enter a valid identification method from the list.' REC_SOLUTION, 'Invalid identification method' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_ORIG_CURR' ERROR_CODE, 'Invalid original currency code' ERROR_DESC, 'Enter a valid currency code.' REC_SOLUTION, 'Invalid currency code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_RETURN_DISP' ERROR_CODE, 'Invalid Return Disposition.' ERROR_DESC, 'Choose a valid return disposition.' REC_SOLUTION, 'Invalid return disposition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_TEND' ERROR_CODE, 'Meter Transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NON_MERCH_ITEM_NO_REQ' ERROR_CODE, 'A non-merchandise item number is required for this item record.' ERROR_DESC, 'Choose an appropriate non-merchandise item.' REC_SOLUTION, 'Missing non-merch item' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_TAX' ERROR_CODE, 'No sale Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'POSFUTUREDATA' ERROR_CODE, 'POS data is for a business date greater than the current date.' ERROR_DESC, 'It is not possible to import data for a business date in the future.' REC_SOLUTION, 'Invalid business date' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_ITEM' ERROR_CODE, 'Pull Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid pull transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_CATT' ERROR_CODE, 'Pump Test transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'STOREDAYNOTREADYTOBELOAD' ERROR_CODE, 'This store day is not in the status of Ready to Load.' ERROR_DESC, 'The data may already be loaded or trickle polled data may have been loaded out of order.' REC_SOLUTION, 'Store Day not ready to load' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_PYMT' ERROR_CODE, 'Tank Dip transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'THEAD_FIL_STIN' ERROR_CODE, 'THEAD File Line Identifier - Non-numeric character in numeric field or the field may be empty.' ERROR_DESC, 'This input file is corrupt and cannot be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid THEAD record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TPYMT_FIL_STIN' ERROR_CODE, 'TPYMT File Line Identifier Non-numeric character in numeric field or field is empty.' ERROR_DESC, 'This input file is corrupt and can not be loaded. Either edit the file or re-poll the store.' REC_SOLUTION, 'Invalid TPYMT record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PVOID_NO_PYMT' ERROR_CODE, 'Post Void transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid post void transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TEST_ERR1' ERROR_CODE, 'Check number is empty or non-numeric character in numeric-field.' ERROR_DESC, 'Enter the correct check number.' REC_SOLUTION, 'Check number is empty or non-numeric cha' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TEST_ERR2' ERROR_CODE, 'Coupon Number cannot be empty when Tender Type Group is Coupon.' ERROR_DESC, 'Enter a coupon.' REC_SOLUTION, 'Coupon Number cannot be empty when Tende' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_VENDOR_NO_MV' ERROR_CODE, 'Invalid Merchandise Vendor Number.' ERROR_DESC, 'Use a vendor from the supplier table for Merchandise Vendors.' REC_SOLUTION, 'Invalid merchandise vendor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TEST_ERR4' ERROR_CODE, 'Partial Transaction. TTEND in illegal position.' ERROR_DESC, 'Input file is corrupt. A TTEND record must be between a THEAD and a TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Partial Transaction. TTEND in illegal po' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TEST_ERR5' ERROR_CODE, 'Original Currency Amount - Non-numeric character in numeric field.' ERROR_DESC, 'Enter correct original currency amount.' REC_SOLUTION, 'Original Currency Amount - Non-numeric c' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_VENDOR_NO_EV' ERROR_CODE, 'Invalid Expense Vendor Number.' ERROR_DESC, 'Use a vendor from the partner table for Expense Vendors.' REC_SOLUTION, 'Invalid expense vendor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_CAN_MASK' ERROR_CODE, 'Invalid checking account number mask value.' ERROR_DESC, 'Enter the correct checking account number mask value.' REC_SOLUTION, 'Invalid checking account no. mask value' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'T100' ERROR_CODE, 'No sale Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'No sale Transactions should not have Tax' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'REFUND_NO_TAX' ERROR_CODE, 'Refund Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid refund transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TANKDP_NO_TAX' ERROR_CODE, 'Tank Dip Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid tank dip transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'CLOSE_NO_PYMT' ERROR_CODE, 'Close transactions should not have payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'COND_NO_ITEM' ERROR_CODE, 'Store Conditions transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid store conditions transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'DCLOSE_NO_IGTAX' ERROR_CODE, 'Day Close transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid day close transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_IGTAX' ERROR_CODE, 'Loan transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_PYMT' ERROR_CODE, 'Meter transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_TEND' ERROR_CODE, 'No sale Transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_PYMT' ERROR_CODE, 'Open transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_ITEM' ERROR_CODE, 'Paid In Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_ITEM' ERROR_CODE, 'Paid Out Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_PYMT' ERROR_CODE, 'Pull Out transactions should not have payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid pull out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TOTAL_NO_CUST' ERROR_CODE, 'Total Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid total transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'RETWH_EXT_ORD' ERROR_CODE, 'Return Warehouse is only valid for external customer orders.' ERROR_DESC, 'Set the Return Warehouse to blank.' REC_SOLUTION, 'Invalid return warehouse' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'FULFILL_ORDER_NO_ERR' ERROR_CODE, 'The fulfillment order number is only valid for external customer orders with Order Complete or Order Cancel item status.' ERROR_DESC, 'Set the fulfillment order number to blank.' REC_SOLUTION, 'Invalid fulfillment order number' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NO_INV_RET_IND_RETURN' ERROR_CODE, 'Return Without Inventory Indicator is only valid for return Item Status.' ERROR_DESC, 'Set the indicator to blank.' REC_SOLUTION, 'Invalid non-inventory return indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PUMPT_NO_TEND' ERROR_CODE, 'Pump Test transactions should not have tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid pump test transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_PRMT' ERROR_CODE, 'Invalid promotion type.' ERROR_DESC, 'Choose a Promotion Type from the list.' REC_SOLUTION, 'Invalid promotion type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SACA' ERROR_CODE, 'Invalid Customer Attribute Type.' ERROR_DESC, 'Choose a Customer Attribute Type from the list.' REC_SOLUTION, 'Invalid customer attribute type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SASY' ERROR_CODE, 'Invalid Sales Type.' ERROR_DESC, 'Choose a Sales Type from the list.' REC_SOLUTION, 'Invalid sales type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TAXC' ERROR_CODE, 'Invalid Tax Type.' ERROR_DESC, 'Choose a Tax Type from the list.' REC_SOLUTION, 'Invalid tax type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TRAS' ERROR_CODE, 'Invalid Sub-Transaction Type.' ERROR_DESC, 'Choose a Sub-Transaction Type from the list.' REC_SOLUTION, 'Invalid sub-transaction type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_VENDOR_NO' ERROR_CODE, 'Invalid Vendor Number.' ERROR_DESC, 'Use a vendor from the partner table for Expense Vendors or from the supplier table for Merchandise Vendors.' REC_SOLUTION, 'Invalid vendor' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'TITEM_DEP_STIN' ERROR_CODE, 'Department - Non-numeric character in numeric field.' ERROR_DESC, 'Enter the correct Department.' REC_SOLUTION, 'Invalid department' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SAIT' ERROR_CODE, 'Invalid Item Type.' ERROR_DESC, 'Choose an Item Type from the list.' REC_SOLUTION, 'Invalid item type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SALESPERSON_ID' ERROR_CODE, 'The Salesperson POS ID does not have a corresponding Employee ID.' ERROR_DESC, 'Enter the Employee ID and POS ID in the Employee Maintenance Form or correct the POS ID on the Employees Tab.' REC_SOLUTION, 'Invalid salesperson ID' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SARR' ERROR_CODE, 'Invalid Return Reason.' ERROR_DESC, 'Choose a Return Reason from the list.' REC_SOLUTION, 'Invalid return reason' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SASI' ERROR_CODE, 'Invalid Item Status.' ERROR_DESC, 'Choose an Item Status from the list.' REC_SOLUTION, 'Invalid item status' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SASY_E' ERROR_CODE, 'Invalid Sales Type for Sale, Layaway, or Void transactions.' ERROR_DESC, 'Choose a sales type from the list.' REC_SOLUTION, 'Invalid sales type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SELLING_UOM' ERROR_CODE, 'The selling unit of measure is not valid for the item.' ERROR_DESC, 'Select a different selling unit of measure that is compatible with the standard unit of measure, or define the conversion between the current selling unit of measure and the standard unit of measure.' REC_SOLUTION, 'Invalid selling UOM' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SUB_SACA' ERROR_CODE, 'Invalid Customer Attribute Value.' ERROR_DESC, 'Choose a Customer Attribute Value from the list.' REC_SOLUTION, 'Invalid customer attribute' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_SYSTEM_IND' ERROR_CODE, 'Invalid System Indicator.' ERROR_DESC, 'Choose a System Indicator from the list.' REC_SOLUTION, 'Invalid system indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TAX_AUTHORITY' ERROR_CODE, 'Invalid Tax Authority or field is empty.' ERROR_DESC, 'Enter a valid Tax Authority' REC_SOLUTION, 'Invalid tax authority' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TAX_IND' ERROR_CODE, 'Invalid Tax Indicator. The value has been defaulted to ''Y''.' ERROR_DESC, 'Click the Tax Indicator check box to set the correct value.' REC_SOLUTION, 'Invalid tax indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TENDER_ENDING' ERROR_CODE, 'Invalid Tender ending.' ERROR_DESC, 'Adjust the Tender as required' REC_SOLUTION, 'Invalid tender ending' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TENT' ERROR_CODE, 'Invalid Tender Type Group.' ERROR_DESC, 'Choose a Tender Type Group from the list.' REC_SOLUTION, 'Invalid tender type group' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TRAN_DATETIME' ERROR_CODE, 'Invalid Transaction Date/Time.' ERROR_DESC, 'Enter a valid Transaction Date/Time.' REC_SOLUTION, 'Invalid transaction date/time' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TRAT' ERROR_CODE, 'Invalid Transaction Type.' ERROR_DESC, 'Choose a Transaction Type from the list.' REC_SOLUTION, 'Invalid transaction type' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_TSYS' ERROR_CODE, 'Invalid Transaction Processing System. The transaction has been sent to the reject file.' ERROR_DESC, 'Enter a valid transaction processing system.' REC_SOLUTION, 'Invalid processing system' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INVLD_VATC' ERROR_CODE, 'Invalid VAT Code.' ERROR_DESC, 'Enter a valid VAT Code.' REC_SOLUTION, 'Invalid VAT code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INV_RULE_DEF' ERROR_CODE, 'This rule did not calculate successfully for the store day.' ERROR_DESC, 'Review the rule definition and update as needed.' REC_SOLUTION, 'Invalid rule definition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'INV_TOTAL_DEF' ERROR_CODE, 'This total did not calculate successfully for the store day.' ERROR_DESC, 'Review the total definition and update as needed.' REC_SOLUTION, 'Invalid total definition' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'ITEMLVLTAX_NO_TTAX' ERROR_CODE, 'A store configured with Item-level Tax should not have Transaction-level tax records.' ERROR_DESC, 'Delete the transaction-level tax record from this transaction.' REC_SOLUTION, 'Invalid TTAX record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'ITEM_STATUS_REQ' ERROR_CODE, 'Item Status cannot be empty.' ERROR_DESC, 'Choose an Item Status from the list.' REC_SOLUTION, 'Missing item status' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_CATT' ERROR_CODE, 'Loan transactions should not have a Customer Attribute record.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_CUST' ERROR_CODE, 'Loan transactions should not have customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_DISC' ERROR_CODE, 'Loan transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_ITEM' ERROR_CODE, 'Loan Transactions should not have a Item record.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_PYMT' ERROR_CODE, 'Loan transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_NO_TAX' ERROR_CODE, 'Loan transactions should not have a Tax record.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'LOAN_TENDER_REQ' ERROR_CODE, 'Loan Transactions are required to have at least one Tender record.' ERROR_DESC, 'Add a Tender Record to this transaction.' REC_SOLUTION, 'Invalid loan transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_ITEM_REQ' ERROR_CODE, 'Meter Transactions are required to have at least one Item record.' ERROR_DESC, 'Add an Item Record to this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_CATT' ERROR_CODE, 'Meter transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_CUST' ERROR_CODE, 'Meter Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_IGTAX' ERROR_CODE, 'Meter transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'METER_NO_TAX' ERROR_CODE, 'Meter Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid meter transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'MISSING_THEAD' ERROR_CODE, 'Missing THEAD record.' ERROR_DESC, 'Input file is corrupt. Every transaction must have a THEAD and TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Missing THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'MISSING_TRAN_BIG_GAP' ERROR_CODE, 'A large number of transactions are missing.' ERROR_DESC, 'The input file may be corrupt. Try re-polling the store.' REC_SOLUTION, 'Large transaction gap' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'MISSING_TTAIL' ERROR_CODE, 'Missing TTAIL record.' ERROR_DESC, 'Input file is corrupt. Every transaction must have a THEAD and TTAIL record. Either edit the file or re-poll the store.' REC_SOLUTION, 'Missing TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NO_INV_RET_IND_REQ' ERROR_CODE, 'Return Without Inventory indicator is required for a return transaction whose sales type is External Customer Order and which originated from OMS.' ERROR_DESC, 'Choose from the Return Without Inventory drop down list to set the correct value.' REC_SOLUTION, 'Invalid non-inventory return indicator' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NON_MERCH_ITEM_NOT_FOUND' ERROR_CODE, 'This non-merchandise item does not exist in RMS.' ERROR_DESC, 'Choose an appropriate non-merchandise item.' REC_SOLUTION, 'Non-merch item does not exist' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_CATT' ERROR_CODE, 'No Sale transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_DISC' ERROR_CODE, 'No sale transaction should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_IGTAX' ERROR_CODE, 'No sale transaction should not have item-level tax  records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_ITEM' ERROR_CODE, 'No sale Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_PYMT' ERROR_CODE, 'No sale transaction should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NO_RULE_FOR_SALETOTAL' ERROR_CODE, 'There is no rule defined for the Sale Total ending amount.' ERROR_DESC, 'Make sure that there is rule defined in the sa_rounding_rule_detail table for the Sale Total ending amount' REC_SOLUTION, 'Missing rule' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_CATT' ERROR_CODE, 'Open transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_CUST' ERROR_CODE, 'Open Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_DISC' ERROR_CODE, 'Open transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_IGTAX' ERROR_CODE, 'Open transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_ITEM' ERROR_CODE, 'Open Transactions should not have Item records.' ERROR_DESC, 'Delete the Item record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_TAX' ERROR_CODE, 'Open Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'OPEN_NO_TEND' ERROR_CODE, 'Open Transactions should not have Tender records.' ERROR_DESC, 'Delete the Tender record from this transaction.' REC_SOLUTION, 'Invalid open transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'ORGCUR_AMT_WITHOUT_ORGCUR' ERROR_CODE, 'Original currency amount is present without original currency code.' ERROR_DESC, 'Enter valid original currency code.' REC_SOLUTION, 'Missing currency code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'ORGCUR_WITHOUT_ORGCUR_AMT' ERROR_CODE, 'No original currency amount for given original currency code.' ERROR_DESC, 'Enter correct original currency amount.' REC_SOLUTION, 'Missing currency amount' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_CATT' ERROR_CODE, 'Paid In transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_CUST' ERROR_CODE, 'Paid In Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_DISC' ERROR_CODE, 'Paid In transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_IGTAX' ERROR_CODE, 'Paid In transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_PYMT' ERROR_CODE, 'Paid In transactions should not have Payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_NO_TAX' ERROR_CODE, 'Paid In Transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid paid in transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_REASON_CODE_REQ' ERROR_CODE, 'Paid In Transactions are required to have a Reason Code.' ERROR_DESC, 'Choose a Reason Code from the list.' REC_SOLUTION, 'Missing reason code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDIN_TENDER_REQ' ERROR_CODE, 'Paid In Transactions are required to have at least one Tender record.' ERROR_DESC, 'Add a Tender record to this transaction.' REC_SOLUTION, 'Missing tender records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_CATT' ERROR_CODE, 'Paid Out transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_CUST' ERROR_CODE, 'Paid Out Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_DISC' ERROR_CODE, 'Paid Out transactions should not have Discount records.' ERROR_DESC, 'Delete the Discount record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_IGTAX' ERROR_CODE, 'Paid Out transactions should not have item-level tax records.' ERROR_DESC, 'Delete the item-level tax record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_PYMT' ERROR_CODE, 'Paid Out transactions should not have payment records.' ERROR_DESC, 'Delete payment record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_NO_TAX' ERROR_CODE, 'Paid Out transactions should not have Tax records.' ERROR_DESC, 'Delete the Tax record from this transaction.' REC_SOLUTION, 'Invalid paid out transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_REASON_CODE_REQ' ERROR_CODE, 'Paid Out Transactions are required to have a Reason Code.' ERROR_DESC, 'Choose a Reason Code from the list.' REC_SOLUTION, 'Missing reason code' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PAIDOU_TENDER_REQ' ERROR_CODE, 'Paid Out Transactions are required to have at least one tender record.' ERROR_DESC, 'Add a Tender record to this transaction.' REC_SOLUTION, 'Missing tender records' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'POSDATATOOOLD' ERROR_CODE, 'POS data is too old to be loaded into the system.' ERROR_DESC, 'Check the Days Post Sale on the Sales Audit System Options screen. This field contains the number of days back from today that POS data can be loaded into the system.' REC_SOLUTION, 'Old POS data' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'POSUNEXPECTEDSTOREDAY' ERROR_CODE, 'POS data is not expected for this store day. This store is not expected to be open.' ERROR_DESC, 'The store and business date are correct. However, POS data for this store day are not expected. Check the Company Close, Company Close Exception and Store Close information for this store.' REC_SOLUTION, 'Unexpected POS data' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PROM_COMP_REQ' ERROR_CODE, 'Promotion component cannot be blank for a Promotion.' ERROR_DESC, 'Enter a Promotion component.' REC_SOLUTION, 'Missing promotion component' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'NOSALE_NO_CUST' ERROR_CODE, 'No sale Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid no sale record' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_CATT' ERROR_CODE, 'Pull transactions should not have Customer Attribute records.' ERROR_DESC, 'Delete the Customer Attribute record from this transaction.' REC_SOLUTION, 'Invalid pull transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 1 LANG, 'PULL_NO_CUST' ERROR_CODE, 'Pull Transactions should not have Customer records.' ERROR_DESC, 'Delete the Customer record from this transaction.' REC_SOLUTION, 'Invalid pull transaction' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO SA_ERROR_CODES base USING
( SELECT ERROR_CODE ERROR_CODE, ERROR_DESC ERROR_DESC, REC_SOLUTION REC_SOLUTION, SHORT_DESC SHORT_DESC FROM SA_ERROR_CODES_TL TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE SET base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
--
DELETE FROM SA_ERROR_CODES_TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
