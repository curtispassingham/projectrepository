SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH1@' TOKEN, 1 LANG, 'Company' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP1@' TOKEN, 1 LANG, 'Companies' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH2@' TOKEN, 1 LANG, 'Chain' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP2@' TOKEN, 1 LANG, 'Chains' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH3@' TOKEN, 1 LANG, 'Area' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP3@' TOKEN, 1 LANG, 'Areas' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH4@' TOKEN, 1 LANG, 'Region' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP4@' TOKEN, 1 LANG, 'Regions' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OH5@' TOKEN, 1 LANG, 'District' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@OHP5@' TOKEN, 1 LANG, 'Districts' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH2@' TOKEN, 1 LANG, 'Division' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP2@' TOKEN, 1 LANG, 'Divisions' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH3@' TOKEN, 1 LANG, 'Group' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP3@' TOKEN, 1 LANG, 'Groups' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH4@' TOKEN, 1 LANG, 'Department' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP4@' TOKEN, 1 LANG, 'Departments' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH5@' TOKEN, 1 LANG, 'Class' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP5@' TOKEN, 1 LANG, 'Classes' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MH6@' TOKEN, 1 LANG, 'Subclass' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@MHP6@' TOKEN, 1 LANG, 'Subclasses' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH1@' TOKEN, 1 LANG, 'Manufacturer' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP1@' TOKEN, 1 LANG, 'Manufacturers' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH2@' TOKEN, 1 LANG, 'Distributor' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP2@' TOKEN, 1 LANG, 'Distributors' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH3@' TOKEN, 1 LANG, 'Wholesaler' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP3@' TOKEN, 1 LANG, 'Wholesalers' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUH4@' TOKEN, 1 LANG, 'Franchise' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@SUHP4@' TOKEN, 1 LANG, 'Franchisee' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COM@' TOKEN, 1 LANG, 'Country Of Manufacture' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO DYNAMIC_HIER_TOKEN_MAP_TL TL USING
(SELECT  TOKEN,  LANG,	RMS_NAME,  CLIENT_NAME
FROM  (SELECT '@COMN@' TOKEN, 1 LANG, 'Countries Of Manufacture' RMS_NAME, '' CLIENT_NAME FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM DYNAMIC_HIER_TOKEN_MAP base where dl.TOKEN = base.TOKEN)) USE_THIS
ON ( tl.TOKEN = use_this.TOKEN and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.RMS_NAME = use_this.RMS_NAME, tl.CLIENT_NAME = use_this.CLIENT_NAME
WHEN NOT MATCHED THEN INSERT (TOKEN, LANG, RMS_NAME, CLIENT_NAME)
VALUES (use_this.TOKEN, use_this.LANG, use_this.RMS_NAME, use_this.CLIENT_NAME);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO DYNAMIC_HIER_TOKEN_MAP base USING
( SELECT TOKEN TOKEN, RMS_NAME RMS_NAME, CLIENT_NAME CLIENT_NAME FROM DYNAMIC_HIER_TOKEN_MAP_TL TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.TOKEN = use_this.TOKEN)
WHEN MATCHED THEN UPDATE SET base.RMS_NAME = use_this.RMS_NAME, base.CLIENT_NAME = use_this.CLIENT_NAME;
--
DELETE FROM DYNAMIC_HIER_TOKEN_MAP_TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
