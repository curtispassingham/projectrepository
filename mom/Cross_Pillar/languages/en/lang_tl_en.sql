SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 1 LANG_LANG, 'English' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 2 LANG_LANG, 'German' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 3 LANG_LANG, 'French' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 4 LANG_LANG, 'Spanish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 5 LANG_LANG, 'Japanese' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 6 LANG_LANG, 'Korean' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 7 LANG_LANG, 'Russian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 8 LANG_LANG, 'Chinese-Simplified' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 9 LANG_LANG, 'Turkish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 10 LANG_LANG, 'Hungarian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 11 LANG_LANG, 'Chinese-Traditional' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 12 LANG_LANG, 'Portuguese-Brazil' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 13 LANG_LANG, 'Arabic' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 14 LANG_LANG, 'Canadian French' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 15 LANG_LANG, 'Croatian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 16 LANG_LANG, 'Czech' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 17 LANG_LANG, 'Danish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 18 LANG_LANG, 'Dutch' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 19 LANG_LANG, 'Finnish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 20 LANG_LANG, 'Greek' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 21 LANG_LANG, 'Hebrew' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 22 LANG_LANG, 'Italian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 23 LANG_LANG, 'Latin American Spanish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 24 LANG_LANG, 'Lithuanian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 25 LANG_LANG, 'Norwegian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 26 LANG_LANG, 'Polish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 27 LANG_LANG, 'Portuguese' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 28 LANG_LANG, 'Romanian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 29 LANG_LANG, 'Slovak' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 30 LANG_LANG, 'Slovenian' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 31 LANG_LANG, 'Swedish' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 32 LANG_LANG, 'Thai' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 1 LANG, 33 LANG_LANG, 'Tagalog' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)
AND NOT EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE DL.LANG = SCO.DATA_INTEGRATION_LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 1 LANG, 'English' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 2 LANG, 'German' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 3 LANG, 'French' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 4 LANG, 'Spanish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 5 LANG, 'Japanese' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 6 LANG, 'Korean' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 7 LANG, 'Russian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 8 LANG, 'Chinese-Simplified' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 9 LANG, 'Turkish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 10 LANG, 'Hungarian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 11 LANG, 'Chinese-Traditional' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 12 LANG, 'Portuguese-Brazil' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 13 LANG, 'Arabic' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 14 LANG, 'Canadian French' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 15 LANG, 'Croatian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 16 LANG, 'Czech' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 17 LANG, 'Danish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 18 LANG, 'Dutch' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 19 LANG, 'Finnish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 20 LANG, 'Greek' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 21 LANG, 'Hebrew' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 22 LANG, 'Italian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 23 LANG, 'Latin American Spanish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 24 LANG, 'Lithuanian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 25 LANG, 'Norwegian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 26 LANG, 'Polish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 27 LANG, 'Portuguese' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 28 LANG, 'Romanian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 29 LANG, 'Slovak' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 30 LANG, 'Slovenian' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 31 LANG, 'Swedish' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 32 LANG, 'Thai' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG base USING
(SELECT 33 LANG, 'Tagalog' DESCRIPTION FROM DUAL DL
WHERE EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
