SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'CC' EVENT_TYPE, 'Cost Change' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'CL' EVENT_TYPE, 'Change in Costing Location of Franchise Store' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'CZ' EVENT_TYPE, 'Cost Zone Location Move' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'D' EVENT_TYPE, 'Deal' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'DP' EVENT_TYPE, 'Deal Pass Through' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'ELC' EVENT_TYPE, 'Estimated Landed Cost Component' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'ICZ' EVENT_TYPE, 'Item Cost Zone Changes' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'MH' EVENT_TYPE, 'Merchandise Hierarchy' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'NIL' EVENT_TYPE, 'New Item Location' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'OH' EVENT_TYPE, 'Organization Hierarchy' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'PP' EVENT_TYPE, 'Primary Pack Cost' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'R' EVENT_TYPE, 'Reclassification' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'RTC' EVENT_TYPE, 'Retail Change' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'SC' EVENT_TYPE, 'Item Supplier Country Location Relationship Changes' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'SH' EVENT_TYPE, 'Supplier Hierarchy' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'T' EVENT_TYPE, 'WF Cost Template' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COST_EVENT_RUN_TYPE_CNFG_TL TL USING
(SELECT  LANG,	EVENT_TYPE,  EVENT_DESC
FROM  (SELECT 1 LANG, 'TR' EVENT_TYPE, 'WF Cost Template Relationship' EVENT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COST_EVENT_RUN_TYPE_CONFIG base where dl.EVENT_TYPE = base.EVENT_TYPE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE  SET tl.EVENT_DESC = use_this.EVENT_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, EVENT_TYPE, EVENT_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.EVENT_TYPE, use_this.EVENT_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COST_EVENT_RUN_TYPE_CONFIG base USING
( SELECT EVENT_TYPE EVENT_TYPE, EVENT_DESC EVENT_DESC FROM COST_EVENT_RUN_TYPE_CNFG_TL TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1)) USE_THIS
ON ( base.EVENT_TYPE = use_this.EVENT_TYPE)
WHEN MATCHED THEN UPDATE SET base.EVENT_DESC = use_this.EVENT_DESC;
--
DELETE FROM COST_EVENT_RUN_TYPE_CNFG_TL where lang = 1
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 1);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
