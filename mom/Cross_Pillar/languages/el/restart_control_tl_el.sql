SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'LIKE_STORE' PROGRAM_NAME, 'Διαδικασία όμοιου καταστήματος' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'ang_saplgen' PROGRAM_NAME, 'Google - POSLog Εξαγωγή' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'costcalc' PROGRAM_NAME, 'υπολογισμός εκπτώσεων συμφωνίας στον πίνακα FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealact' PROGRAM_NAME, 'Υπολογισμός πραγματικών πληροφοριών' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealcls' PROGRAM_NAME, 'κλείσιμο συμφωνιών που έχουν φτάσει την ημερομηνία close_date' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealday' PROGRAM_NAME, 'Σύνοψη μέχρι την ημέρα συμφωνίας' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealex' PROGRAM_NAME, 'Ανάπτυξη πληροφοριών συμφωνίας στη θέση του είδους' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealfct' PROGRAM_NAME, 'Υπολογισμός πληροφοριών πρόβλεψης' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealfinc' PROGRAM_NAME, 'Έσοδα από πάγιες συμφωνίες στο γενικό λογ. βιβλίο' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealinc' PROGRAM_NAME, 'Υπολογισμός εσόδων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dealprg' PROGRAM_NAME, 'εκκαθ. συμφ. που έκλεισαν για περίοδο που καθόρισε το σύστ.' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dfrtbld' PROGRAM_NAME, 'εισαγωγή εγγραφών στον πίνακα DEAL_SKU_TEMP' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'discotbapply' PROGRAM_NAME, 'Εφαρμογή έκπτωσης OTB' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'ditinsrt' PROGRAM_NAME, 'εισαγωγή εγγραφών στο deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dlyprg' PROGRAM_NAME, 'καθημερινή εκκαθάριση' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'docclose' PROGRAM_NAME, 'κλείσιμο παραλαβών χωρίς ανάθεση' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'dsdupld' PROGRAM_NAME, 'αποστολή παράδοσης απευθείας στο κατάστημα' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'edidlinv' PROGRAM_NAME, 'Λήψη τιμολογίου EDI' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'elccostcalc' PROGRAM_NAME, 'Υπολογισμός αλλαγών Υπ.Κόστ.Φορτ. στον πίνακα FUTURE_COST' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'fifinvcu11i' PROGRAM_NAME, 'Αποστολή τιμολογίου' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'fifinvcup' PROGRAM_NAME, 'Διαδικασία διασύνδεσης τιμολογίου' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'genpreiss' PROGRAM_NAME, 'Δημιουργία παραγγελιών προέκδοσης' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'hstmthupd' PROGRAM_NAME, 'Ενημέρωση στοιχείων soh, retail, avg_cost στο item_loc_hist_mth' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'lclrbld' PROGRAM_NAME, 'αναδόμηση δέσμης λίστας τοποθεσιών' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'lcup798' PROGRAM_NAME, 'αναλήψεις πίστωσης/χρεώσεις εγγυητικής επιστολής' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'lcupld' PROGRAM_NAME, 'αποστολή εγγυητικής επιστολής' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'lifstkup' PROGRAM_NAME, 'μετατροπή αποστολής αποθέματος' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'mrt' PROGRAM_NAME, 'Δημιουργία μεταφορών από Μετ.μαζ.επιστρ.' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'mrtprg' PROGRAM_NAME, 'mrtprg' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'mrtrtv' PROGRAM_NAME, 'Δημιουργία ΕΣΑΠ για Μετ.μαζ.επιστρ. ΕΣΑΠ' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'mrtupd' PROGRAM_NAME, 'mrtupd' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'nwppurge' PROGRAM_NAME, 'Εκκαθάριση αρχής κατώτερης αποτίμησης' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'ociroq' PROGRAM_NAME, 'null' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'ordautcl' PROGRAM_NAME, 'Διαγραφή/κλείσιμο ΕΑ' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'orddscnt' PROGRAM_NAME, 'έκπτωση εντολών αγοράς' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'prchstprg' PROGRAM_NAME, 'Οριστική διαγραφή ιστορικού τιμών' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'precostcalc' PROGRAM_NAME, 'προκατ. υπολογισμός εκπτώσεων συμφωνίας στο deal_sku_temp' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'refmvl10nentity' PROGRAM_NAME, 'Ανανέωση της υλοποιημένης όψης οντότητας L10N' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'refmvlocprimaddr' PROGRAM_NAME, 'Ανανέωση υλοποιημένης προβολής κύριας διεύθυνσης τοποθεσίας' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saescheat' PROGRAM_NAME, 'Μεταβίβαση ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpach' PROGRAM_NAME, 'Εξαγωγή ReSA σε ACH' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpdw' PROGRAM_NAME, 'Έλεγχος πωλήσεων-Εξαγωγή σε RDW' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpgl' PROGRAM_NAME, 'Εξαγωγή ReSA σε Oracle GL' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpgl107' PROGRAM_NAME, 'Εξαγωγή ReSA σε Oracle GL' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpsim' PROGRAM_NAME, 'SIM εξαγωγής ελέγχου πωλήσεων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saexpuar' PROGRAM_NAME, 'UAR εξαγωγής ελέγχου πωλήσεων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'sagetref' PROGRAM_NAME, 'λήψη δεδομένων αναφορών ελέγχου πωλήσεων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saimpadj' PROGRAM_NAME, 'Εισαγωγή ReSA αναπροσαρμογών' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saimptlog' PROGRAM_NAME, 'TLOG εισαγωγής ελέγχου πωλήσεων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saimptlogfin' PROGRAM_NAME, 'Ολοκλήρωση ReSA εισαγωγής TLOG' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saimptlogi' PROGRAM_NAME, 'TLOG εισαγωγής ελέγχου πωλήσεων' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'saimptlogtdup_upd' PROGRAM_NAME, 'ReSA - Ενημέρωση αρχείου TDUP' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'sapreexp' PROGRAM_NAME, 'Προ-επεξεργασία εξαγωγής ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'sapurge' PROGRAM_NAME, 'Οριστική διαγραφή δεδομένων ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'sarules' PROGRAM_NAME, 'Επεξεργασία κανόνων ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'sastdycr' PROGRAM_NAME, 'Δημιουργία ημέρας καταστήματος ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'satotals' PROGRAM_NAME, 'Συνολική επεξεργασία ReSA' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'soupld' PROGRAM_NAME, 'αποστολή παραγγελίας καταστήματος' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'stkschedxpld' PROGRAM_NAME, 'STOCK COUNT SCHEDULE EXPLODE' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'supsplit' PROGRAM_NAME, 'Διαχωρισμός παρτίδας από προμηθευτή' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'szonrbld' PROGRAM_NAME, 'αναδόμηση ασφάλειας ζώνης χρηστών' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'trandataprocess' PROGRAM_NAME, 'Φόρτωση δεδομένων εξωτερικής συναλλαγής' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'vendinvc' PROGRAM_NAME, 'Τιμολόγηση αντιπροσώπου πωλήσεων για σύνθετες συμφωνίες' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO RESTART_CONTROL_TL TL USING
(SELECT  LANG,	PROGRAM_NAME,  PROGRAM_DESC
FROM  (SELECT 20 LANG, 'vendinvf' PROGRAM_NAME, 'Τιμολόγηση αντιπροσώπου πωλήσεων για σταθερές συμφωνίες' PROGRAM_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM RESTART_CONTROL base where dl.PROGRAM_NAME = base.PROGRAM_NAME)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE  SET tl.PROGRAM_DESC = use_this.PROGRAM_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, PROGRAM_NAME, PROGRAM_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.PROGRAM_NAME, use_this.PROGRAM_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO RESTART_CONTROL base USING
( SELECT PROGRAM_NAME PROGRAM_NAME, PROGRAM_DESC PROGRAM_DESC FROM RESTART_CONTROL_TL TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20)) USE_THIS
ON ( base.PROGRAM_NAME = use_this.PROGRAM_NAME)
WHEN MATCHED THEN UPDATE SET base.PROGRAM_DESC = use_this.PROGRAM_DESC;
--
DELETE FROM RESTART_CONTROL_TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
