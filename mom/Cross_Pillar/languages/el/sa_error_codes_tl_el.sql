SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAIL_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου TTAIL - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAIL_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. TTAIL σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAIL_TRC_STIN' ERROR_CODE, 'Μετρητής εγγραφών συναλλαγών TTAIL - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAIL_WITHOUT_THEAD' ERROR_CODE, 'Μερική συναλλαγή. TTAIL εκτός συναλλαγής' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAX_AMT_STIN' ERROR_CODE, 'Ποσό φόρου - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό ποσό φόρου.' REC_SOLUTION, 'Μη έγκυρο ποσό φόρου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAX_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου TTAX - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. TTAX σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγρ. TTAX πρέπει να βρίσκεται μεταξύ εγγρ. THEAD και TTAIL. Επεξ. το αρχείο ή καταγ. ξανά το κατάστ.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_CHECK_NO_REQ' ERROR_CODE, 'Κενός αριθμός επιταγής ή μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό επιταγής.' REC_SOLUTION, 'Μη έγκ. αρ. επιταγής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_COUPON_NO_REQ' ERROR_CODE, 'Ο αριθμός κουπονιού δεν μπορεί να είναι κενός όταν η ομάδα τύπων προσφορών είναι κουπόνι.' ERROR_DESC, 'Εισαγάγετε ένα κουπόνι.' REC_SOLUTION, 'Λείπει αρ. κουπονιού' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου TTEND - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TTEND' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. TTEND σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγρ. TTEND πρέπει να βρίσκεται μεταξύ εγγρ. THEAD και TTAIL. Επεξ. το αρχείο ή καταγ. ξανά το κατάστ.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_OCA_STIN' ERROR_CODE, 'Αρχικό ποσό νομίσματος - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το σωστό αρχικό ποσό νομίσματος.' REC_SOLUTION, 'Μη έγκ. ποσό νομίσμ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_TAM_STIN' ERROR_CODE, 'Ποσό προσφοράς - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Εισαγάγετε το ορθό ποσό προσφοράς.' REC_SOLUTION, 'Μη έγκ. ποσό προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TTEND_TTI_STIN' ERROR_CODE, 'Το ID τύπου προσφοράς δεν αποτελεί μέρος της ομάδας τύπων προσφορών ή είναι κενό.' ERROR_DESC, 'Επιλέξτε έναν τύπο προσφοράς από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'UPC_NOT_FOUND' ERROR_CODE, 'Αυτό το UPC δεν υπάρχει στο RMS.' ERROR_DESC, 'Επιλέξτε ένα UPC από τη λίστα.' REC_SOLUTION, 'Το UPC δεν βρέθηκε.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'VENDOR_DATA_REQ' ERROR_CODE, 'Απαιτ. δεδ. αντ. πωλ. - Τουλ. 1 από τα κατ. πεδία πρέπει να έχει τιμή: Αρ. τιμολ. αντ. πωλ., αρ. αναφ. πληρ., αρ. απόδ. παράδ.' ERROR_DESC, 'Εισαγάγετε τουλάχιστον μία από τις παρακάτω τιμές: Αρ. τιμολ. αντιπρ. πωλήσεων, αρ. αναφ. πληρωμής, αρ. απόδειξης παράδοσης.' REC_SOLUTION, 'Λείπει αντ. πωλήσεων' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'VENDOR_NO_REQ' ERROR_CODE, 'Ο αρ. αντιπροσώπου πωλήσεων στο THEAD είναι υποχρεωτικό πεδίο για εξερχ. πληρωμή αντιπροσώπου πωλήσεων εμπορεύματος ή δαπανών.' ERROR_DESC, 'Επιλέξτε έναν αριθμό αντιπροσώπου πωλήσεων από τη λίστα.' REC_SOLUTION, 'Λείπει αντ. πωλήσεων' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'VOID_NO_PYMT' ERROR_CODE, 'Οι ακυρωμένες συναλλαγές δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ακυρωμ. συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_TAX' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DUP_TAXC' ERROR_CODE, 'Διπλός κωδικός φόρου. Η συναλλαγή απεστάλη στο αρχείο απορρίψεων.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Διπλότυπος κωδ. φόρου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FTAIL_FRC_STIN' ERROR_CODE, 'Μετρητής εγγραφών FTAIL - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_EXP_DATE' ERROR_CODE, 'Μη έγκυρη ημ/νία λήξης για ένα δελτίο.' ERROR_DESC, 'Εισαγάγετε μια έγκυρη ημερομηνία λήξης. Η έγκυρη μορφή για ημερομηνία λήξης είναι YYYMMDD.' REC_SOLUTION, 'Μη έγκ. ημ/νία λήξης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_NON_MERCH_CODE' ERROR_CODE, 'Μη έγκυρος κωδικός αιτιολογίας για συναλλαγή εξερχ. πληρωμής προμηθευτή για δαπάνη ή εξερχ. πληρωμής προμηθευτή εμπορεύματος.' ERROR_DESC, 'Επιλέξτε έναν έγκυρο κωδικό αιτιολογίας για τη συναλλαγή.' REC_SOLUTION, 'Μη έγ.κωδ.μη σχ.με ε.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_TEND' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NON_MERCH_ITEM_NO_REQ' ERROR_CODE, 'Για αυτήν την εγγραφή αρχείου απαιτείται αριθμός είδους μη σχετιζόμενου με εμπόρευμα.' ERROR_DESC, 'Επιλέξτε ένα κατάλληλο είδος μη σχετιζόμενο με εμπορεύματα.' REC_SOLUTION, 'Λείπ.είδ.μη σχ.με εμ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_TAX' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_CUST' ERROR_CODE, 'Οι συναλλαγές ανάληψης δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.ανάληψης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_TAX' ERROR_CODE, 'Οι συναλλαγές ανάληψης δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.ανάληψης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_CUST' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_CUST' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CASHIER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'Απαιτείται το ID ταμία λόγω του επιπέδου ισοσκελισμού.' ERROR_DESC, 'Εισαγάγετε το ορθό ID ταμία.' REC_SOLUTION, 'Μη έγκ. αναγν. ταμία' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CATT_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου για εγγραφή CATT - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή CATT' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CATT_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. Εγγραφή CATT σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγραφή CATT πρέπει να ακολουθεί μια εγγραφή TCUST. Επεξεργ. το αρχείο ή καταγ. ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CC_NO_REQ' ERROR_CODE, 'Απαιτείται αριθμός πιστωτικής κάρτας.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό πιστωτικής κάρτας.' REC_SOLUTION, 'Λείπει αρ. πιστ.κάρτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_CATT' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφή χαρακτηριστικού πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_CUST' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_DISC' ERROR_CODE, 'Η κλειστή συναλλαγή δεν πρέπει να περιέχει εγγραφή έκπτωσης.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_IGTAX' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_ITEM' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_TAX' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_TEND' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_CATT' ERROR_CODE, 'Η συναλλαγή συνθηκών καταστήματος δεν πρέπει να περιέχει εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_CUST' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_DISC' ERROR_CODE, 'Η συναλλαγή συνθηκών καταστήματος δεν πρέπει να περιέχει εγγραφή έκπτωσης.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_TAX' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_TEND' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CREDIT_PROMO_ID_STIN' ERROR_CODE, 'Μη έγκυρη ταυτότητα προώθησης πίστωσης.' ERROR_DESC, 'Εισαγάγετε την ορθή ταυτότητα προώθησης πίστωσης' REC_SOLUTION, 'Μη έγκ. προώθ. πίστ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CUST_ID_REQ' ERROR_CODE, 'Απαιτείται αριθμός ταυτοποίησης πελάτη.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό ταυτοποίησης πελάτη.' REC_SOLUTION, 'Λείπει αναγν. πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CUST_ORD_ATTR_REQ' ERROR_CODE, 'Απαιτούνται τα χαρακτηριστικά παραγγελίας πελάτη.' ERROR_DESC, 'Εισαγάγετε τα χαρακτηριστικά παραγγελίας παραγγελίας πελάτη μέσω της οθόνης χαρακτηριστικών πελάτη.' REC_SOLUTION, 'Λείπουν χαρακ. πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CUST_ORD_DATE_REQDINTITEM' ERROR_CODE, 'Απαιτείται ημερομηνία παραγγελίας πελάτη στο αντίστοιχο TITEM.' ERROR_DESC, 'Εισαγ. ημ/νία παρ. πελ. μέσω της οθόνης Ιδιότητες πελάτη. Αναμ. μορφή ΕΕΕΕΜΜΗΗ' REC_SOLUTION, 'Λείπει ημ. παρ. πελ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DATAUNEXPECTEDSTOREDAY' ERROR_CODE, 'Δεν αναμένονται δεδομένα για αυτήν την ημέρα καταστήματος.' ERROR_DESC, 'Το κατάστημα ή η εργ. ημ/νία δεν είναι έγκυρα. Ελέγξτε κατάστημα, κλείσ. εταιρ., εξαίρ. κλεισίμ. εταιρ. και κλείσ. καταστήματος.' REC_SOLUTION, 'Μη αναμ. δεδ.ημ. κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_CATT' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_DISC' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_TEND' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DISC_REASON_REQ' ERROR_CODE, 'Η αιτιολογία έκπτωσης δεν μπορεί να είναι κενή για μια έκπτωση σε κατάστημα.' ERROR_DESC, 'Εισαγάγετε μια αιτιολογία έκπτωσης.' REC_SOLUTION, 'Λείπει αιτία έκπτωσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DISC_REF_NO_REQ' ERROR_CODE, 'Ο αριθμός αναφοράς έκπτωσης δεν μπορεί να είναι κενός για μια προώθηση' ERROR_DESC, 'Εισαγάγετε έναν αριθμό αναφοράς έκπτωσης.' REC_SOLUTION, 'Λείπει αναφ. έκπτωσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DUP_REC' ERROR_CODE, 'Διπλότυπη εγγραφή. Η συναλλαγή απεστάλη στο αρχείο απορρίψεων.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Διπλότυπη συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DUP_TAXC_COMPITEM' ERROR_CODE, 'Διπλός συνδυασμός κωδικού φόρου - αριθμού είδους στοιχείου. Η συναλλαγή απεστάλη στο αρχείο απορρίψεων.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Διπλότυπος κωδ. φόρου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DUP_TRAN' ERROR_CODE, 'Διπλός αριθμός συναλλαγής. Η συναλλαγή απεστάλη στο αρχείο απορρίψεων.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Διπλότυπη συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'EEXCH_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές σε συνάλλαγμα απαιτείται να περιέχουν τουλάχιστον δύο εγγραφές στοιχείων.' ERROR_DESC, 'Προσθέστε μία ή περισσότερες εγγραφές σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'EEXCH_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές ισοσκελισμένης ανταλλαγής δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγγραφή πληρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FHEAD_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου FHEAD - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FHEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FHEAD_NOT_FIRST' ERROR_CODE, 'Η εγγραφή FHEAD δεν είναι η πρώτη εγγραφή στο αρχείο.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FHEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FILE_ERROR' ERROR_CODE, 'Παρουσιάστηκε σφάλμα κατά τη διάρκεια της τελευταίας λειτουργίας αρχείου.' ERROR_DESC, '' REC_SOLUTION, 'Σφάλμα αρχείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FTAIL_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου FTAIL - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FTAIL_IN_TRAN' ERROR_CODE, 'Μερική συναλλαγή. Υπάρχει εγγραφή CATT σε μια συναλλαγή.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FTAIL_NOT_LAST' ERROR_CODE, 'Η εγγραφή FTAIL δεν είναι η τελευταία εγγραφή στο αρχείο.' ERROR_DESC, 'Το αρχείο εισόδου δεν έχει εγγραφή FTAIL.' REC_SOLUTION, 'Λείπει εγγραφή FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FULFILL_ORDER_NO_ERR' ERROR_CODE, 'Ο αριθ. διεκπερ. παραγγελίας είναι έγκυρος μόνο για εξωτ. παραγγελίες πελατών με κατάσταση ολοκλ. παραγγελίας ή ακύρωσης παραγγελίας.' ERROR_DESC, 'Αφήστε κενό το πεδίο αριθμού διεκπεραίωσης παραγγελίας.' REC_SOLUTION, 'Μη έγκ. αρ.διεκ. παρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_DISC' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές ματαίωσης δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές ματαίωσης δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_TEND' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RECORDS_REJECTED' ERROR_CODE, 'Έγινε απόρριψη εγγραφών κατά τη φόρτωση αυτού του καταστήματος/αυτής της ημέρας.' ERROR_DESC, 'Το αρχείο εισαγ. είναι κατεστραμμένο και δεν μπορεί να φορτωθεί πλήρως. Επεξεργαστείτε το αρχείο ή κάντε νέα καταγραφή καταστ.' REC_SOLUTION, 'Απορριφθ. εγγραφές' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_NO_DISC' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_NO_TAX' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REFUND_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές επιστροφής χρημάτων απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.επισ.χρημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REGISTER_ID_REQ' ERROR_CODE, 'Απαιτείται το ID ταμείου, επειδή δημιουργούνται συναλλαγές στο επίπεδο ταμείου.' ERROR_DESC, 'Εισαγάγετε το ορθό ID ταμείου.' REC_SOLUTION, 'Λείπει αναγν. ταμείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'REGISTER_ID_REQ_BAL_LEVEL' ERROR_CODE, 'Απαιτείται ID ταμείου λόγω του επιπέδου ισοσκελισμού.' ERROR_DESC, 'Εισαγάγετε το ορθό ID ταμείου.' REC_SOLUTION, 'Λείπει αναγν. ταμείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETDISP_INV_RET' ERROR_CODE, 'Η διάθεση επιστροφής είναι έγκυρη μόνο όταν υπάρχει απόθεμα συσχετισμένο με την επιστροφή.' ERROR_DESC, 'Αφήστε κενό το πεδίο διάθεσης επιστροφής.' REC_SOLUTION, 'Μη έγκ. διάθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETDISP_OMS' ERROR_CODE, 'Η διάθεση επιστροφής θα πρέπει να χρησιμοποιείται μόνο με μια συναλλαγή επιστροφής που προήλθε από το OMS και διεκπεραιώθηκε σε αυτό.' ERROR_DESC, 'Αφήστε κενό το πεδίο διάθεσης επιστροφής.' REC_SOLUTION, 'Μη έγκ. διάθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETURN_DISP_REQ' ERROR_CODE, 'Απαιτείται η διάθεση επιστροφής.' ERROR_DESC, 'Επιλέξτε μια έγκυρη διάθεση επιστροφής.' REC_SOLUTION, 'Λείπει διάθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETURN_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές επιστροφής απαιτείται να περιέχουν τουλάχιστον μία εγγραφή είδους..' ERROR_DESC, 'Προσθέστε μια εγγραφή είδους σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETURN_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές επιστροφής δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETURN_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές επιστροφής πρέπει να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπει προσφ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETWH_EXT_ORD' ERROR_CODE, 'Η αποθήκη επιστροφής είναι έγκυρη μόνο για εξωτερικές παραγγελίες πελατών.' ERROR_DESC, 'Αφήστε κενό το πεδίο αποθήκης επιστροφής.' REC_SOLUTION, 'Μη έγκ. αποθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETWH_OMS' ERROR_CODE, 'Η αποθήκη επιστροφής θα πρέπει να χρησιμοποιείται μόνο με μια συναλλαγή επιστροφής που προήλθε από το OMS και διεκπεραιώθηκε σε αυτό.' ERROR_DESC, 'Αφήστε κενό το πεδίο αποθήκης επιστροφής.' REC_SOLUTION, 'Μη έγκ. αποθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETWH_REQ' ERROR_CODE, 'Η αποθήκη επιστροφής απαιτείται για συναλλαγή επιστροφής, με τύπο πώλησης "Εξωτερική παραγγελία πελάτη" και η οποία έχει προέλθει από το OMS.' ERROR_DESC, 'Προσθέστε μια έγκυρη αποθήκη σε αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. αποθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'SALE_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές πωλήσεων απαιτείται να περιέχουν τουλάχιστον μία εγγραφή είδους..' ERROR_DESC, 'Προσθέστε μια εγγραφή είδους σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'SALE_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές πωλήσεων απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'SET_DROP_SHIP' ERROR_CODE, 'Η ένδειξη αποστολής μέσω τρίτων δεν εισάχθηκε σωστά για αυτό το είδος.' ERROR_DESC, 'Στην ενότητα στοιχείων είδους, επαληθεύστε την ένδειξη αποστολής μέσω τρίτων.' REC_SOLUTION, 'Μη έγκ. ένδ.απ.παράδ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'SKU_NOT_FOUND' ERROR_CODE, 'Το είδος είναι κενό ή δεν υπάρχει στο RMS.' ERROR_DESC, 'Επιλέξτε ένα είδος από τη λίστα.' REC_SOLUTION, 'Δεν βρέθηκε το είδος' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'STOREDAYNOTREADYTOBELOAD' ERROR_CODE, 'Αυτή η ημέρα καταστήματος δεν βρίσκεται σε κατάσταση ετοιμότητας για φόρτωση.' ERROR_DESC, 'Τα δεδομένα μπορεί να έχουν ήδη φορτωθεί ή μπορεί να έχουν φορτωθεί καταγεγραμμένα δεδομένα περιορισμένης ροής χωρίς σειρά.' REC_SOLUTION, 'Ημ.κατ.ανέτοιμη φόρτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'STOREDAY_LOCK_FAILED' ERROR_CODE, 'Αδυναμία κλειδώματος εγγραφής για ημέρα καταστήματος. Δεν έχει φορτωθεί εγγραφή κλειδώμ. ή ημ. καταστ. κλειδωμένη από άλλη διεργασία.' ERROR_DESC, 'Δοκιμάστε ξανά μετά την άρση του κλειδώματος.' REC_SOLUTION, 'Απέτυχε κλείδ.ημ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής απαιτείται να περιέχουν τουλάχιστον μία εγγραφή είδους..' ERROR_DESC, 'Προσθέστε μια εγγραφή είδους σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_CATT' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_DISC' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές ένδειξης στάθμης δεξαμενής μέσω βύθισης δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_TAX' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TCUST_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου TCUST - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TCUST' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TCUST_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. TCUST σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγρ. TCUST πρέπει να βρίσκεται μεταξύ εγγρ. THEAD και TTAIL. Επεξ. το αρχείο ή καταγ. ξανά το κατάστ.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TERM_MARKER_NO_ERROR' ERROR_CODE, 'Αυτό το σφάλμα χρησιμοποιείται προκειμένου να ελέγχεται ότι έχουν φορτωθεί όλα τα δεδομένα.' ERROR_DESC, 'Φορτώστε όλα τα δεδομένα και εκτελέστε το saimptlogfin.' REC_SOLUTION, 'Κανένα σφάλμα' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου THEAD - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. THEAD σε μη νόμιμη θέση.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_LOC_NO_BANN' ERROR_CODE, 'Η τοποθεσία δεν έχει ID εμβλήματος.' ERROR_DESC, 'Το κατάστημα δεν έχει σχετικό ID εμβλήματος.' REC_SOLUTION, 'Λείπει έμβλημα' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'FULFILL_ORDER_NO_REQ' ERROR_CODE, 'Ο αριθμός διεκπεραίωσης παραγγελίας απαιτείται.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό διεκπεραίωσης παραγγελίας.' REC_SOLUTION, 'Λείπει αρ. διεκ. παρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'GARBAGE_IN_RECORD' ERROR_CODE, 'Στο αρχείο υπάρχουν ενσωματωμένα απορριφθέντα στοιχεία.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Υπάρχει ενσωματωμένος χαρακτήρας στην εγγραφή. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Άχρηστα στην εγγραφή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_COUPON_NO_REQ' ERROR_CODE, 'Ο αριθμός κουπονιού δεν μπορεί να είναι κενός όταν ο τύπος έκπτωσης είναι κουπόνι καταστήματος.' ERROR_DESC, 'Εισαγάγετε αριθμό κουπονιού.' REC_SOLUTION, 'Λείπει αρ. κουπονιού' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου IDISC - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκυρο αναγ. IDISC' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_INVALID_CW' ERROR_CODE, 'Ένδ. παραδοτέου βάρους σε σχέση με έκπτωση - Δεν έχει οριστεί η ένδειξη παραδοτέου βάρους για είδος υπολογιζόμενο βάσει βάρους.' ERROR_DESC, 'Εισαγάγετε την ορθή ένδειξη παραδοτέου βάρους.' REC_SOLUTION, 'Μη έγ.έν.βάρ.κατ.εκτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. Εγγραφή IDISC σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγραφή IDISC πρέπει να ακολουθεί μια εγγραφή TITEM. Επεξ. το αρχείο ή καταγ. ξανά το κατάστ.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_PRC_STIN' ERROR_CODE, 'Μη έγκυρο περιεχ. προώθησης έκπτωσης' ERROR_DESC, 'Εισαγάγετε ένα έγκυρο περιεχ. προώθησης έκπτωσης' REC_SOLUTION, 'Μη έγκ. στοιχ. προώθ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_QTY_STIN' ERROR_CODE, 'Ποσότητα έκπτωσης - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Εισαγάγετε μια έγκυρη ποσότητα έκπτωσης.' REC_SOLUTION, 'Μη έγκ. ποσότ. έκπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_SET_CW' ERROR_CODE, 'Ένδειξη παραδοτέου βάρους σε σχέση με έκπτωση - Η ένδειξη παραδοτέου βάρους δεν εισάχθηκε σωστά.' ERROR_DESC, 'Εισαγάγετε την ορθή ένδειξη παραδοτέου βάρους.' REC_SOLUTION, 'Μη έγ.έν.βάρ.κατ.εκτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_UDA_STIN' ERROR_CODE, 'Ποσό έκπτωσης - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Εισαγάγετε ένα έγκυρο ποσό έκπτωσης.' REC_SOLUTION, 'Μη έγκ. ποσό έκπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_UOM_QTY_STIN' ERROR_CODE, 'Ποσότητα Μον.μέτρ. έκπτωσης - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο' ERROR_DESC, 'Εισαγάγετε την ορθή ποσότητα Μον.μέτρ..' REC_SOLUTION, 'Μη έγκυρη ΜΜ έκπτωσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDNT_ID_WITHOUT_IDNT_MTHD' ERROR_CODE, 'Υπάρχει η δυνατότητα αναγνώρισης χωρίς μέθοδο αναγνώρισης.' ERROR_DESC, 'Εισαγάγατε μια έγκυρη μέθοδο ταυτοποίησης από τη λίστα.' REC_SOLUTION, 'Μη έγκ. μέθ. αναγνώρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDNT_MTHD_WITHOUT_IDNT_ID' ERROR_CODE, 'Δεν υπάρχει αναγνώριση για αυτήν τη μέθοδο αναγνώρισης.' ERROR_DESC, 'Εισαγάγετε μια έγκυρη αναγνώριση.' REC_SOLUTION, 'Λείπει αναγνώριση' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IGTAX_FIL_STIN' ERROR_CODE, 'Αναγν γρ αρχ IGTAX. Μη αρ χαρακτ σε αρ πεδ ή κενό πεδ.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκυρο αναγ. IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IGTAX_IN_ILLEGAL_POS' ERROR_CODE, 'Μερ συν. Εγγρ IGTAX σε μη νόμ θέση' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Μια εγγρ. IGTAX πρέπει να ακολουθεί εγγραφή TITEM-IDISC. Επεξεργ. το αρχείο ή καταγ. ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IGTAX_RAT_STIN' ERROR_CODE, 'Τιμή IGTAX - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό' ERROR_DESC, 'Εισαγάγετε την ορθή τιμή IGTAX.' REC_SOLUTION, 'Μη έγκ. συντελ. IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_BANNER_ID' ERROR_CODE, 'Μη έγκυρο ID εμβλήματος.' ERROR_DESC, 'Επιλέξετε ένα έγκυρο αναγνωριστικό εμβλήματος.' REC_SOLUTION, 'Μη έγκυρο έμβλημα' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_BIRTHDATE' ERROR_CODE, 'Μη έγκυρη ημερομηνία γέννησης.' ERROR_DESC, 'Η έγκυρη μορφή για ημερομηνία γέννησης είναι YYYYMMDD.' REC_SOLUTION, 'Μη έγκ. ημ. γέννησης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_BUSINESS_DATE' ERROR_CODE, 'Η εργάσιμη ημερομηνία δεν είναι έγκυρη ή λείπει.' ERROR_DESC, 'Η έγκυρη μορφή για εργάσιμη ημερομηνία είναι YYYYMMDD.' REC_SOLUTION, 'Μη έγκυρη εργ. ημ/νία' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CASHIER_ID' ERROR_CODE, 'Το ID POS ταμία δεν έχει αντίστοιχο ID υπαλλήλου.' ERROR_DESC, 'Εισαγάγετε το ID υπαλλήλου και το ID POS στο έντυπο επεξεργασίας υπαλλήλου.' REC_SOLUTION, 'Μη έγκ. αναγν. ταμία' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CCAS' ERROR_CODE, 'Μη έγκυρη πηγή εξουσιοδότησης πιστωτικής κάρτας.' ERROR_DESC, 'Επιλέξτε μια πηγή εξουσιοδότησης πιστωτικής κάρτας από τη λίστα.' REC_SOLUTION, 'Μη έγκ. προέλ. εξουσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CCEM' ERROR_CODE, 'Μη έγκυρος τρόπος εισαγωγής πιστωτικής κάρτας.' ERROR_DESC, 'Επιλέξτε έναν τρόπο εισαγωγής πιστωτικής κάρτας από τη λίστα.' REC_SOLUTION, 'Μη έγ.τρ.εισ.πισ.κάρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CCSC' ERROR_CODE, 'Μη έγκυρη ειδική προϋπόθεση για πιστωτικές κάρτες.' ERROR_DESC, 'Επιλέξτε μια ειδική προϋπόθεση για πιστωτικές κάρτες από τη λίστα.' REC_SOLUTION, 'Μη έγκυρη συνθήκη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CCVF' ERROR_CODE, 'Μη έγκυρη επαλήθευση κατόχου πιστωτικής κάρτας.' ERROR_DESC, 'Επιλέξτε μια επαλήθευση κατόχου πιστωτικής κάρτας από τη λίστα.' REC_SOLUTION, 'Μη έγκ. επαλήθευση' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CC_CHECKSUM' ERROR_CODE, 'Μη έγκυρο ψηφίο ελέγχου πιστωτικής κάρτας. Αυτός ο αριθμός κάρτας δεν είναι έγκυρος' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό πιστωτικής κάρτας.' REC_SOLUTION, 'Μη έγκ. ψηφ. ελέγχου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CC_EXP_DATE' ERROR_CODE, 'Μη έγκυρη ημερομηνία λήξης.' ERROR_DESC, 'Η έγκυρη μορφή για ημερομηνία λήξης είναι YYYYMMDD.' REC_SOLUTION, 'Μη έγκ. ημ/νία λήξης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CC_MASK' ERROR_CODE, 'Μη έγκυρη τιμή μάσκας πιστωτικής κάρτας.' ERROR_DESC, 'Εισαγάγετε την ορθή τιμή μάσκας πιστωτικής κάρτας.' REC_SOLUTION, 'Μη έγκ.τιμ.μασ.π.κάρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CC_PREFIX' ERROR_CODE, 'Το αριθμητικό πρόθημα αυτής της πιστωτικής κάρτας δεν βρίσκεται εντός του προσδιορισμένου εύρους.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό πιστωτικής κάρτας.' REC_SOLUTION, 'Μη έγκ.πρόθ.πιστ.κάρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CHECK_NO' ERROR_CODE, 'Μη έγκυρος αριθμός ελέγχου ή μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό επιταγής.' REC_SOLUTION, 'Μη έγκ. αρ. επιταγής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CIDT' ERROR_CODE, 'Μη έγκυρος τύπος ταυτοποίησης πελάτη.' ERROR_DESC, 'Επιλέξτε έναν τύπο ταυτοποίησης πελάτη από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπ.αν.πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CUST_ORD_DATE' ERROR_CODE, 'Μη έγκυρη ημ/νία παραγγελίας πελάτη.' ERROR_DESC, 'Η έγκυρη μορφή για ημερομηνία παραγγελίας πελάτη είναι YYYYMMDD.' REC_SOLUTION, 'Μη έγκ. ημ.παρ.πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_DISCOUNT_UOM' ERROR_CODE, 'Η μονάδα μέτρησης έκπτωσης δεν είναι έγκυρη για αυτό το είδος..' ERROR_DESC, 'Επιλέξτε άλλη μον. μέτρησης συμβατή με την τυπ. μον. μέτρησης ή ορίστε μετατροπή μεταξύ τρέχ. μον. μέτρ. έκπτωσης και τυπ. μον. μέτρ.' REC_SOLUTION, 'Μη έγκυρη ΜΜ έκπτωσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_DOCUMENT' ERROR_CODE, 'Απαιτείται αριθμός δωροεπιταγής/δελτίου.' ERROR_DESC, 'Εισαγάγετε τον αριθμό δωροεπιταγής/κουπονιού.' REC_SOLUTION, 'Μη έγκυρο έγγραφο' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_FTAIL_POS' ERROR_CODE, 'Η εγγραφή FTAIL είναι σε λάθος θέση.' ERROR_DESC, 'Το αρχείο εισόδου είναι κατεστραμμένο. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_FTAIL_TRAN_CNT' ERROR_CODE, 'Ο αριθμός συναλλαγής FTAIL είναι λανθασμένος.' ERROR_DESC, 'Το αρχείο εισόδου είναι κατεστραμμένο. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή FTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_ITEM_NO' ERROR_CODE, 'Ο αριθμός είδους είναι κενός.' ERROR_DESC, 'Επιλέξτε έναν αριθμό είδους από τη λίστα.' REC_SOLUTION, 'Μη έγκυρο είδος' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_ITEM_SWIPED_IND' ERROR_CODE, 'Μη αποδεκτή ένδειξη διέλευσης είδους. Η τιμή ορίστηκε από προεπιλογή σε "Υ".' ERROR_DESC, 'Κάντε κλικ στο πλαίσιο ελέγχου Διέλευση είδους για να ορίσετε την ορθή τιμή.' REC_SOLUTION, 'Μη έγκ.ένδ. διέλ.είδ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_NO_INV_RET_IND' ERROR_CODE, 'Μη έγκυρος δείκτης επιστροφής χωρίς απόθεμα. Η προεπιλογή είναι κενό πεδίο.' ERROR_DESC, 'Επιλέξτε από την αναπτυσσόμενη λίστα "Επιστροφή χωρίς απόθεμα" για να ορίσετε τη σωστή τιμή.' REC_SOLUTION, 'Μη έγκ.δεί.επ.χωρ.απ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_ORIG_CURR' ERROR_CODE, 'Μη έγκυρος αρχικός κωδικός νομίσματος' ERROR_DESC, 'Εισαγάγετε έναν έγκυρο κωδικό νομίσματος.' REC_SOLUTION, 'Μη έγκ. κωδ. νομίσμ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_ORRC' ERROR_CODE, 'Μη έγκυρη αιτιολογία υπέρβασης τιμής' ERROR_DESC, 'Επιλέξτε μια αιτιολογία παράκαμψης τιμής από τη λίστα.' REC_SOLUTION, 'Μη έγκ.αιτ.παράκ.τιμ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_PRMT' ERROR_CODE, 'Μη έγκυρος τύπος προώθησης.' ERROR_DESC, 'Επιλέξτε έναν τύπο προώθησης από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος προώθ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_REAC' ERROR_CODE, 'Μη έγκυρος κωδικός αιτιολογίας.' ERROR_DESC, 'Επιλέξτε έναν κωδικό αιτιολογίας από τη λίστα.' REC_SOLUTION, 'Μη έγκ. κωδ. αιτίας' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_RTLOG_ORIG_SYS' ERROR_CODE, 'Μη έγκυρο σύστημα προέλευσης RTLOG' ERROR_DESC, 'Δεν είναι δυνατή η εισαγωγή δεδομένων με μη έγκυρο σύστημα προέλευσης RTLOG.' REC_SOLUTION, 'Μη έγκ.σύστ.προέλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SACA' ERROR_CODE, 'Μη έγκυρος τύπος ιδιότητας πελάτη.' ERROR_DESC, 'Επιλέξτε έναν τύπο ιδιότητας πελάτη από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπ.χαρ. πελ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SAIT' ERROR_CODE, 'Μη έγκυρος τύπος είδους.' ERROR_DESC, 'Επιλέξτε έναν τύπο είδους από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SALESPERSON_ID' ERROR_CODE, 'Το ID POS πωλητή δεν έχει αντίστοιχο ID υπαλλήλου.' ERROR_DESC, 'Εισαγάγετε το ID υπαλλήλου και το ID POS στο έντυπο επεξεργασίας υπαλλήλου ή διορθώστε το ID POS στην καρτέλα υπαλλήλων.' REC_SOLUTION, 'Μη έγκ. αναγν. πωλητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SARR' ERROR_CODE, 'Μη έγκυρη αιτιολογία επιστροφής.' ERROR_DESC, 'Επιλέξτε έναν κωδικό επιστροφής από τη λίστα.' REC_SOLUTION, 'Μη έγκ. αιτία επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SASY' ERROR_CODE, 'Μη έγκυρος τύπος πωλήσεων.' ERROR_DESC, 'Επιλέξτε έναν τύπο πωλήσεων από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος πωλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SASY_E' ERROR_CODE, 'Μη έγκυρος τύπος πωλήσεων για πώληση, προκαταβολή έναντι αγοράς ή ματαίωση συναλλαγών.' ERROR_DESC, 'Επιλέξτε έναν τύπο πωλήσεων από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος πωλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SELLING_UOM' ERROR_CODE, 'Η μονάδα μέτρησης πώλησης δεν είναι έγκυρη για αυτό το είδος..' ERROR_DESC, 'Επιλέξτε άλλη μον. μέτρ. έκπτ. συμβατή με τυπ. μον. μέτρ. ή ορίστε μετατροπή μεταξύ τρέχ. μον. μέτρ. πώλ. και τυπ. μον. μέτρ.' REC_SOLUTION, 'Μη έγκ. ΜΜ πώλησης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SYSTEM_IND' ERROR_CODE, 'Μη έγκυρη ένδειξη συστήματος.' ERROR_DESC, 'Επιλέξτε μια ένδειξη συστήματος από τη λίστα.' REC_SOLUTION, 'Μη έγκ. ένδ. συστήμ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TAXC' ERROR_CODE, 'Μη έγκυρος τύπος φόρου.' ERROR_DESC, 'Επιλέξτε έναν τύπο φόρου από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος φόρου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TAX_AUTHORITY' ERROR_CODE, 'Μη έγκυρη φορολογική αρχή ή το πεδίο είναι κενό.' ERROR_DESC, 'Εισαγάγετε μια έγκυρη φορολογική αρχή' REC_SOLUTION, 'Μη έγκ. φορολ. αρχή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TAX_IND' ERROR_CODE, 'Μη αποδεκτή ένδειξη φόρου. Η τιμή ορίστηκε από προεπιλογή σε "Υ".' ERROR_DESC, 'Κάντε κλικ στο πλαίσιο ελέγχου Ένδειξη φόρου για να ορίσετε την ορθή τιμή.' REC_SOLUTION, 'Μη έγκ. ένδειξη φόρου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TENDER_ENDING' ERROR_CODE, 'Μη έγκ. λήξη προσφορ.' ERROR_DESC, 'Προσαρμόστε την προσφορά κατά τον απαιτούμενο τρόπο' REC_SOLUTION, 'Μη έγκ. λήξη προσφορ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TENT' ERROR_CODE, 'Μη έγκυρη ομάδα τύπων προσφορών.' ERROR_DESC, 'Επιλέξτε μια ομάδα τύπων προσφορών από τη λίστα.' REC_SOLUTION, 'Μη έγκ. ομ. τύπ. πρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TRAN_DATETIME' ERROR_CODE, 'Μη έγκυρη ημ/νία/ώρα συναλλαγής.' ERROR_DESC, 'Εισαγάγετε μια έγκυρη ημ/νία/ώρα συναλλαγής.' REC_SOLUTION, 'Μη έγκ. ημ/ώρα συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TRAT' ERROR_CODE, 'Μη έγκυρος τύπος συναλλαγής.' ERROR_DESC, 'Επιλέξτε έναν τύπο συναλλαγής από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TSYS' ERROR_CODE, 'Μη έγκυρο σύστημα επεξεργασίας συναλλαγών. Η συναλλαγή απεστάλη στο αρχείο απορρίψεων.' ERROR_DESC, 'Εισαγάγετε ένα έγκυρο σύστημα επεξεργασίας συναλλαγών.' REC_SOLUTION, 'Μη έγκ. σύστημα επεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_VATC' ERROR_CODE, 'Μη έγκυρος κωδικός ΦΠΑ.' ERROR_DESC, 'Εισαγάγετε έναν έγκυρο κωδικό ΦΠΑ.' REC_SOLUTION, 'Μη έγκ. κωδικός ΦΠΑ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INV_RULE_DEF' ERROR_CODE, 'Δεν ήταν επιτυχής ο υπολογισμός αυτού του κανόνα για την ημέρα καταστήματος.' ERROR_DESC, 'Αναθεωρήστε τον ορισμό του κανόνα και ενημερώστε ανάλογα με τις ανάγκες σας.' REC_SOLUTION, 'Μη έγκ. ορισμ. κανόνα' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INV_TOTAL_DEF' ERROR_CODE, 'Δεν ήταν επιτυχής ο υπολογισμός αυτού του συνόλου για την ημέρα καταστήματος.' ERROR_DESC, 'Αναθεωρήστε τον ορισμό του συνόλου και ενημερώστε ανάλογα με τις ανάγκες σας.' REC_SOLUTION, 'Μη έγκ. ορισ. συνόλου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'ITEMLVLTAX_NO_TTAX' ERROR_CODE, 'Ένα κατάστημα με φορολόγηση σε επίπεδο είδους δεν πρέπει να έχει εγγραφές φόρου σε επίπεδο συναλλαγής.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο συναλλαγής από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγγραφή TTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'ITEM_STATUS_REQ' ERROR_CODE, 'Η κατάσταση είδους δεν μπορεί να είναι κενή.' ERROR_DESC, 'Επιλέξτε μια κατάσταση είδους από τη λίστα.' REC_SOLUTION, 'Λείπει κατάστ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_CATT' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφή ιδιότητας πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_CUST' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_DISC' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφή είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές δανείων απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή απαιτείται να περιέχουν τουλάχιστον μία εγγραφή είδους..' ERROR_DESC, 'Προσθέστε μια εγγραφή είδους σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_CATT' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_CUST' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'METER_NO_TAX' ERROR_CODE, 'Οι συναλλαγές βάσει μετρητή δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. μετρητή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'MISSING_THEAD' ERROR_CODE, 'Λείπει εγγραφή THEAD.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Κάθε συναλ. πρέπει να έχει μια εγγραφή THEAD και TTAIL. Επεξεργ. το αρχείο ή καταγ. ξανά το κατάστημα.' REC_SOLUTION, 'Λείπει το THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'MISSING_TRAN_BIG_GAP' ERROR_CODE, 'Λείπει μεγάλος αριθμός συναλλαγών.' ERROR_DESC, 'Το αρχείο εισόδου ενδέχεται να έχει καταστραφεί. Δοκιμάστε να καταγράψετε ξανά το κατάστημα.' REC_SOLUTION, 'Μεγ. κενό συναλλαγής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'MISSING_TTAIL' ERROR_CODE, 'Λείπει εγγραφή TTAIL .' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Κάθε συναλ. πρέπει να έχει μια εγγραφή THEAD και TTAIL. Επεξεργ. το αρχείο ή καταγ. ξανά το κατάστημα.' REC_SOLUTION, 'Λείπει το TTAIL' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NO_INV_RET_IND_REQ' ERROR_CODE, 'Ο δείκτης επιστρ. χωρίς απόθ. απαιτ. για συν. επιστρ. με τύπος πώλησης "Εξωτερική παραγγελία πελάτη" και η οποία έχει προέλθει από το OMS.' ERROR_DESC, 'Επιλέξτε από την αναπτυσσόμενη λίστα "Επιστροφή χωρίς απόθεμα" για να ορίσετε τη σωστή τιμή.' REC_SOLUTION, 'Μη έγκ.δεί.επ.χωρ.απ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_CATT' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_CUST' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_DISC' ERROR_CODE, 'Η συναλλαγή χωρίς καμία πώληση δεν πρέπει να περιέχει εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν φόρο σε επίπεδο είδους' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_TAX' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_REASON_CODE_REQ' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών απαιτείται να περιέχουν έναν κωδικό αιτιολογίας..' ERROR_DESC, 'Επιλέξτε έναν κωδικό αιτιολογίας από τη λίστα.' REC_SOLUTION, 'Λείπει κωδικός αιτίας' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_CATT' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_CUST' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_TAX' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_REASON_CODE_REQ' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών απαιτείται να περιέχουν έναν κωδικό αιτιολογίας..' ERROR_DESC, 'Επιλέξτε έναν κωδικό αιτιολογίας από τη λίστα.' REC_SOLUTION, 'Λείπει κωδικός αιτίας' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NOSALE_NO_TEND' ERROR_CODE, 'Οι συναλλαγές χωρίς καμία πώληση δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγ. χωρ. πώλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NO_RULE_FOR_SALETOTAL' ERROR_CODE, 'Δεν έχει οριστεί κανόνας για το τελικό ποσό του συνόλου πωλήσεων.' ERROR_DESC, 'Βεβαιωθείτε ότι έχει προσδιοριστεί κανόνας στον πίνακα sa_rounding_rule_detail για το τελικό ποσό του συνόλου πωλήσεων' REC_SOLUTION, 'Λείπει ο κανόνας' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_CATT' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_CUST' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_DISC' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_IGTAX' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_ITEM' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_TAX' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_TEND' ERROR_CODE, 'Οι ανοιχτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'ORGCUR_AMT_WITHOUT_ORGCUR' ERROR_CODE, 'Αρχικό ποσό νομίσματος χωρίς αρχικό κωδικό νομίσματος.' ERROR_DESC, 'Εισαγάγετε έναν έγκυρο αρχικό κωδικό νομίσματος.' REC_SOLUTION, 'Λείπει κωδ. νομίσματ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'ORGCUR_WITHOUT_ORGCUR_AMT' ERROR_CODE, 'Δεν υπάρχει αρχικό ποσό νομίσματος για αυτόν τον αρχικό κωδικό νομίσματος.' ERROR_DESC, 'Εισαγάγετε το σωστό αρχικό ποσό νομίσματος.' REC_SOLUTION, 'Λείπει ποσό νομίσματ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_CATT' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_CUST' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_DISC' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_TEND' ERROR_CODE, 'Οι συναλλαγές κλίσης δεξαμενής δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_IN_TRAN' ERROR_CODE, 'Μερική συναλλαγή. THEAD σε συναλλαγή.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_OUT_BAL' ERROR_CODE, 'Η συναλλαγή παρουσιάζει αναντιστοιχίες. Το σύνολο των ειδών και το ποσό πληρωμής δεν ισοδυναμούν με το σύνολο των προσφορών.' ERROR_DESC, 'Προσαρμόστε τα είδη ή την προσφορά ή το ποσό πληρωμής όπως απαιτείται.' REC_SOLUTION, 'Αναντιστοιχίες συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'CLOSE_NO_PYMT' ERROR_CODE, 'Οι κλειστές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. κλειστή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'COND_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές συνθηκών καταστήματος δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.συνθ.κατ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'DCLOSE_NO_CUST' ERROR_CODE, 'Οι συναλλαγές κλεισίματος ημέρας δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.κλεισ.ημ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'POSDATATOOOLD' ERROR_CODE, 'Τα δεδομένα POS είναι πολύ παλιά για να φορτωθούν στο σύστημα.' ERROR_DESC, 'Ελέγξ. τις ημ. μετά την πώλ. στις επιλ. του συστ. ελέγχ. πωλ. Το πεδίο δείχνει για πόσες ημ. πριν μπορ. να φορτ. δεδομ. POS στο σύστ.' REC_SOLUTION, 'Παλιά δεδομένα POS' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'POSFUTUREDATA' ERROR_CODE, 'Τα δεδομένα POS αφορούν μια εργάσιμη ημέρα μεταγενέστερη από την τρέχουσα ημ/νία.' ERROR_DESC, 'Δεν είναι δυνατή η εισαγωγή δεδομένων για εργάσιμη ημέρα στο μέλλον.' REC_SOLUTION, 'Μη έγκυρη εργ. ημ/νία' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'POSUNEXPECTEDSTOREDAY' ERROR_CODE, 'Δεν αναμένονται δεδομένα POS για αυτήν την ημέρα καταστήματος. Αυτό το κατάστημα δεν αναμένεται να είναι ανοικτό.' ERROR_DESC, 'Κατάστ. και εργ. ημ. ορθά. Όμως, δεν αναμ. δεδομ.POS για ημ.καταστ. Ελέγξτε κλεισ. ετ., εξαίρ. κλεισ. ετ. και κλεισ. κατ. για κατάστ.' REC_SOLUTION, 'Μη αναμ. δεδομένα POS' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PROM_COMP_REQ' ERROR_CODE, 'Το περιεχ. προώθησης δεν μπορεί να είναι κενό για μια προώθηση.' ERROR_DESC, 'Εισαγάγετε ένα περιεχ. προώθησης.' REC_SOLUTION, 'Λείπει στοιχ. προώθ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_CATT' ERROR_CODE, 'Οι συναλλαγές ανάληψης δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.ανάληψης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές απόσυρσης δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.απόσυρσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές ανάληψης δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.ανάληψης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές απόσυρσης δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.απόσυρσης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_TENDER_REQ' ERROR_CODE, 'Οι συναλλαγές ανάληψης απαιτείται να περιέχουν τουλάχιστον μία εγγραφή προσφοράς.' ERROR_DESC, 'Προσθέστε μια εγγραφή προσφοράς σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. προσφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_ITEM_REQ' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας απαιτείται να περιέχουν τουλάχιστον μία εγγραφή είδους..' ERROR_DESC, 'Προσθέστε μια εγγραφή είδους σε αυτή τη συναλλαγή.' REC_SOLUTION, 'Λείπουν εγγρ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_CATT' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_CUST' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_DISC' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_TAX' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_TEND' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_CATT' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελάτη.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_CAN_MASK' ERROR_CODE, 'Μη έγκυρη τιμή μάσκας αριθμού λογαριασμού όψεως.' ERROR_DESC, 'Εισαγάγετε τη σωστή τιμή μάσκας αριθμού λογαριασμού όψεως.' REC_SOLUTION, 'Μη έγκ.μσκ αρ.λογ.όψ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_OTN_STIN' ERROR_CODE, 'Αρχικός αρ. συν. POS - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Αυτή η τιμή ορίστηκε σε -1 για να διευκολύνεται η φόρτωση δεδομένων. Εισαγάγετε τον αρχικό αριθμό συναλλαγής POS που ακυρώνεται.' REC_SOLUTION, 'Μη έγκ. αρ. συν. POS' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_RAMT_STIN' ERROR_CODE, 'Στρογγυλοποιημένο ποσό - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό στρογγυλοποιημένο ποσό.' REC_SOLUTION, 'Μη έγκ. στρογ. ποσό' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'IDISC_DRN_STIN' ERROR_CODE, 'Μη έγκυρος αριθμός προώθησης έκπτωσης' ERROR_DESC, 'Εισαγάγετε έναν έγκυρο αριθμό προώθησης έκπτωσης.' REC_SOLUTION, 'Μη έγκυρη προώθηση' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_IDMH' ERROR_CODE, 'Μη έγκυρη μέθοδος ταυτοποίησης.' ERROR_DESC, 'Εισαγάγατε μια έγκυρη μέθοδο ταυτοποίησης από τη λίστα.' REC_SOLUTION, 'Μη έγκ. μέθ. αναγν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_RETURN_DISP' ERROR_CODE, 'Μη έγκυρη διάθεση επιστροφής.' ERROR_DESC, 'Επιλέξτε μια έγκυρη διάθεση επιστροφής.' REC_SOLUTION, 'Μη έγκ. διάθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SADT' ERROR_CODE, 'Μη έγκυρος τύπος έκπτωσης.' ERROR_DESC, 'Επιλέξτε έναν τύπο έκπτωσης από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπος έκπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SASI' ERROR_CODE, 'Μη έγκυρη κατάσταση είδους.' ERROR_DESC, 'Επιλέξτε μια κατάσταση είδους από τη λίστα.' REC_SOLUTION, 'Μη έγκ. κατ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_SUB_SACA' ERROR_CODE, 'Μη έγκυρη τιμή ιδιότητας πελάτη.' ERROR_DESC, 'Επιλέξτε μια τιμή ιδιότητας πελάτη από τη λίστα.' REC_SOLUTION, 'Μη έγκ. χαρακ. πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_TRAS' ERROR_CODE, 'Μη έγκυρος τύπος δευτερεύουσας συναλλαγής.' ERROR_DESC, 'Επιλέξτε έναν τύπο δευτερεύουσας συναλλαγής από τη λίστα.' REC_SOLUTION, 'Μη έγκ. τύπ.δευτ.συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_VENDOR_NO' ERROR_CODE, 'Μη έγκυρος αριθμός αντιπροσώπου πώλησης.' ERROR_DESC, 'Χρησιμοποιήστε έναν αντιπρ. πωλ. από τον πίνακα εταίρων για αντιπρ. πωλ. δαπ. ή από τον πίνακα προμ. για αντιπρ. πωλ. εμπορ.' REC_SOLUTION, 'Μη έγκ. αντ. πωλήσεων' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'LOAN_NO_TAX' ERROR_CODE, 'Οι συναλλαγές δανείων δεν πρέπει να περιέχουν εγγραφή φόρου.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. δανείου' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NON_MERCH_ITEM_NOT_FOUND' ERROR_CODE, 'Αυτό το είδος μη σχετιζόμενο με εμπορεύματα δεν υπάρχει στο RMS.' ERROR_DESC, 'Επιλέξτε ένα κατάλληλο είδος μη σχετιζόμενο με εμπορεύματα.' REC_SOLUTION, 'Δ/Υ είδ.μη σχ.με εμπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'NO_INV_RET_IND_RETURN' ERROR_CODE, 'Ο δείκτης επιστροφής χωρίς απόθεμα είναι έγκυρος μόνο για κατάσταση είδους επιστροφής.' ERROR_DESC, 'Αφήστε κενό το πεδίο του δείκτη.' REC_SOLUTION, 'Μη έγκ.δεί.επ.χωρ.απ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'OPEN_NO_PYMT' ERROR_CODE, 'Οι ανοικτές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. ανοικτή συν.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDIN_NO_ITEM' ERROR_CODE, 'Οι συναλλαγές εισερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν. εισ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PAIDOU_NO_DISC' ERROR_CODE, 'Οι συναλλαγές εξερχόμενων πληρωμών δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.εξερ. πλη.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PULL_NO_DISC' ERROR_CODE, 'Οι συναλλαγές ανάληψης δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή έκπτωσης από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν.ανάληψης' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PUMPT_NO_PYMT' ERROR_CODE, 'Οι συναλλαγές δοκιμής αντλίας δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.δοκ.αντλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'PVOID_NO_TAX' ERROR_CODE, 'Οι συναλλαγές μετά από ακύρωση δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συν. ματαίωσ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETDISP_EXT_ORD' ERROR_CODE, 'Η διάθεση επιστροφής είναι έγκυρη μόνο για εξωτερικές παραγγελίες πελατών.' ERROR_DESC, 'Αφήστε κενό το πεδίο διάθεσης επιστροφής.' REC_SOLUTION, 'Μη έγκ. διάθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'RETWH_NOT_FOUND' ERROR_CODE, 'Η αποθήκη επιστροφής δεν υπάρχει στο RMS.' ERROR_DESC, 'Επιλέξτε μια αποθήκη από τη λίστα.' REC_SOLUTION, 'Μη έγκ. αποθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TANKDP_NO_IGTAX' ERROR_CODE, 'Οι συναλλαγές ένδειξης στάθμης δεξαμενής μέσω βύθισης δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ.συν.βύθ.δεξ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_IGTAX' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_VENDOR_NO_MV' ERROR_CODE, 'Μη έγκυρος αριθμός αντιπροσώπου πωλήσεων εμπορεύματος.' ERROR_DESC, 'Χρησιμοποιήστε έναν αντιπρόσωπο πωλήσεων από τον πίνακα προμηθευτών για αντιπροσώπους πωλήσεων εμπορεύματος.' REC_SOLUTION, 'Μη έγκ. αντ.πώλ. εμπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'INVLD_VENDOR_NO_EV' ERROR_CODE, 'Μη έγκυρος αριθμός αντιπροσώπου πωλήσεων για δαπάνες.' ERROR_DESC, 'Χρησιμοποιήστε έναν αντιπρόσωπο πωλήσεων από τον πίνακα συνεργατών για αντιπροσώπους πωλήσεων για δαπάνες.' REC_SOLUTION, 'Μη έγκ. αντ.πώλ. δαπ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_ROAMT_STIN' ERROR_CODE, 'Στρογγυλοποιημένο ποσό - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό στρογγυλοποιημένο ποσό.' REC_SOLUTION, 'Μη έγκ. ποσό στρογγ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_TRN_STIN' ERROR_CODE, 'Αριθμός συναλλαγής THEAD - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο ενδέχεται να είναι κενό.' ERROR_DESC, 'Η τιμή ορίστηκε σε -1 για να διευκολυνθεί η φόρτωση δεδομένων. Εισαγάγετε τον αρχικό αρ. συν. POS για αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. αριθμ. THEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'THEAD_VAL_STIN' ERROR_CODE, 'Τιμή - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε την ορθή τιμή.' REC_SOLUTION, 'Μη έγκυρη τιμή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_CLS_STIN' ERROR_CODE, 'Κατηγορία SKU - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε την ορθή κατηγορία SKU.' REC_SOLUTION, 'Μη έγκυρη κατηγορία' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_CUST_ORD_STIN' ERROR_CODE, 'Αριθμός γραμμής παραγγελίας πελάτη - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό γραμμής παραγγελίας πελάτη μέσω της οθόνης Ιδιότητες πελάτη.' REC_SOLUTION, 'Μη έγκ. γρ. παρ. πελ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_DEP_STIN' ERROR_CODE, 'Τμήμα - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό τμήμα.' REC_SOLUTION, 'Μη έγκυρο τμήμα' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_FIL_STIN' ERROR_CODE, 'Αναγνωριστικό γραμμής αρχείου TITEM - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. αριθμ. TITEM' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_INVALID_CW' ERROR_CODE, 'Ένδειξη παραδοτέου βάρους είδους - Δεν έχει οριστεί η ένδειξη παραδοτέου βάρους για είδος υπολογιζόμενο βάσει βάρους.' ERROR_DESC, 'Στην ενότητα στοιχείων είδους, επαληθεύστε την ένδειξη παραδοτέου βάρους.' REC_SOLUTION, 'Μη έγ.έν.βάρ.κατ.εκτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. TITEM σε μη νόμιμη θέση.' ERROR_DESC, 'Κατεστραμ. αρχείο εισόδου. Μια εγγρ. TITEM πρέπει να βρίσκεται μεταξύ εγγρ. THEAD και TTAIL. Επεξ. το αρχείο ή καταγ. ξανά το κατάστ.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_MID_STIN' ERROR_CODE, 'ID μέσων - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο..' ERROR_DESC, 'Εισαγάγετε το ορθό ID μέσων μέσω της οθόνης Ιδιότητες πελάτη..' REC_SOLUTION, 'Μη έγκ. αν. πολυμέσων' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_OUR_STIN' ERROR_CODE, 'Αρχική λιανική τιμή μονάδας - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε την ορθή αρχική λιανική τιμή μονάδας.' REC_SOLUTION, 'Μη έγκ. αρχ.λιαν.μον.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_QTY_SIGN' ERROR_CODE, 'Κατ.είδ. Πώλ., Εκκ.παρ., Ολ.παρ., Εκκ. προκ.έν.αγ. ή Ολ. προκ.έν.αγ. θετικές.' ERROR_DESC, 'Τροποποιήστε το σήμα της ποσότητας είδους.' REC_SOLUTION, 'Μη έγκ. σύμ.πστ. είδ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_QTY_STIN' ERROR_CODE, 'Ποσότητα είδους - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Εισ ορθή ποσ είδ. Εγ ειδ με κατ πώλ πρέπει έχουν θετ ποσ, εγ με κατ επισ αρν. Αν κατ άκυρη, σήμα πρέπει αντίθ από σήμα ακ είδ.' REC_SOLUTION, 'Μη έγκ. ποσότ. είδους' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_SBC_STIN' ERROR_CODE, 'Δευτερεύουσα κατηγορία - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε την ορθή δευτερεύουσα κατηγορία.' REC_SOLUTION, 'Μη έγκ. δευτ. κατηγ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_SET_CW' ERROR_CODE, 'Ένδειξη παραδοτέου βάρους είδους - Η ένδειξη παραδοτέου βάρους δεν εισάχθηκε σωστά για αυτό το είδος.' ERROR_DESC, 'Στην ενότητα στοιχείων είδους, επαληθεύστε την ένδειξη παραδοτέου βάρους.' REC_SOLUTION, 'Μη έγ.έν.βάρ.κατ.εκτ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_SUP_STIN' ERROR_CODE, 'Συμπλήρωμα UPC - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό συμπλήρωμα UPC.' REC_SOLUTION, 'Μη έγκυρο συμπλ. UPC' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_UOM_QTY_STIN' ERROR_CODE, 'Ποσότητα Μον.μέτρ. είδους - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο' ERROR_DESC, 'Εισαγάγετε την ορθή ποσότητα Μον.μέτρ..' REC_SOLUTION, 'Μη έγκ. ποσότ. ΜΜ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_UOM_QTY_ZERO' ERROR_CODE, 'Για εγ. ειδ. βάρ.κατ.εκτ. με κατ. Πώληση ή Επιστροφή, η ποσ. μον.μετρ. δεν μπορ. να είναι μηδέν αν η ποσ. δεν είναι μηδέν και αντίστρ.' ERROR_DESC, 'Εισαγάγετε μια ποσότητα μονάδων μέτρησης είδους.' REC_SOLUTION, 'Μη έγκ. ποσότ. ΜΜ' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_URT_STIN' ERROR_CODE, 'Λιανική τιμή μονάδας - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε την ορθή λιανική τιμή μονάδας.' REC_SOLUTION, 'Μη έγκ. λιανική μον.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TITEM_WH_STIN' ERROR_CODE, 'Αποθήκη επιστροφής - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε τη σωστή αποθήκη επιστροφής.' REC_SOLUTION, 'Μη έγκ. αποθ. επιστρ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOO_MANY_FHEADS' ERROR_CODE, 'Εντοπίστηκε πρόσθετη εγγραφή FHEAD.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Πάρα πολλές εγ. FHEAD' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_IGTAX_AMT_GT_TOTRET' ERROR_CODE, 'Το Συνολικό ποσό IGTAX είναι μεγαλύτερο από το Σύνολο λιανικής.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκυρο ποσό IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_IGTAX_AMT_STIN' ERROR_CODE, 'Συνολ. ποσό IGTAX - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο ή το πεδίο είναι κενό.' ERROR_DESC, 'Εισαγάγετε το ορθό συνολ. ποσό IGTAX.' REC_SOLUTION, 'Μη έγκυρο ποσό IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_CATT' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές ιδιοτήτων πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή ιδιότητας πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_CUST' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πελατών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_DISC' ERROR_CODE, 'Οι συνολικές ανάληψης δεν πρέπει να περιέχουν εγγραφές εκπτώσεων.' ERROR_DESC, 'Διαγράψτε την εγγραφή συνόλου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_ITEM' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές ειδών.' ERROR_DESC, 'Διαγράψτε την εγγραφή είδους από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_PYMT' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές πληρωμών.' ERROR_DESC, 'Διαγράψτε την εγγραφή πληρωμής από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_TAX' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές φόρων.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_NO_TEND' ERROR_CODE, 'Οι συνολικές συναλλαγές δεν πρέπει να περιέχουν εγγραφές προσφορών.' ERROR_DESC, 'Διαγράψτε την εγγραφή προσφοράς από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. συνολ. συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_REF_NO_1_REQ' ERROR_CODE, 'Οι συνολικές συναλλαγές απαιτείται να περιέχουν το ID συνόλου στο πεδίο Αριθμός αναφοράς 1.' ERROR_DESC, 'Εισαγάγετε το ID συνόλου στο πεδίο Αριθμός αναφοράς 1.' REC_SOLUTION, 'Λείπει αρ. αναφοράς' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TOTAL_VALUE_REQ' ERROR_CODE, 'Οι συνολικές συναλλαγές απαιτείται να περιέχουν συνολική τιμή..' ERROR_DESC, 'Εισαγάγετε τη συνολική τιμή.' REC_SOLUTION, 'Λείπει συνολική τιμή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TPYMT_AMT_STIN' ERROR_CODE, 'Ποσό πληρωμής - Μη αριθμητικός χαρακτήρας σε αριθμητικό πεδίο.' ERROR_DESC, 'Εισαγάγετε το ορθό ποσό πληρωμής' REC_SOLUTION, 'Μη έγκ. ποσό πληρωμής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TPYMT_FIL_STIN' ERROR_CODE, 'Αναγν γρ αρχ TPYMT-Μη αριθμ χαρ σε αριθμ πεδ ή κενό πεδ.' ERROR_DESC, 'Αυτό το αρχείο εισόδου είναι κατεστραμμένο και δεν μπορεί να φορτωθεί. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. εγγραφή TPYMT' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TPYMT_IN_ILLEGAL_POS' ERROR_CODE, 'Μερική συναλλαγή. Η εγγραφή TPYMT βρίσκεται σε παράνομη θέση' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Η εγγραφή TPYMT πρέπει να προηγείται μιας εγγραφής TTEND. Επεξεργ. το αρχείο ή καταγ. ξανά το κατάστημα.' REC_SOLUTION, 'Μερική συναλλαγή' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_MISS_REC' ERROR_CODE, 'Από τη συναλλαγή λείπουν δευτερεύουσες εγγραφές.' ERROR_DESC, 'Το αρχείο εισόδου είναι κατεστραμμένο. Επεξεργαστείτε το αρχείο ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Λείπουν δευτ. εγγραφ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_NOT_RECOGNIZED' ERROR_CODE, 'Ο τύπος συναλλαγής δεν αναγνωρίζεται.' ERROR_DESC, 'Κατεστραμμένο αρχείο εισόδου. Ελέγξτε τη διαδικασία μετατροπής TLOG σε RTLOG ή καταγράψτε ξανά το κατάστημα.' REC_SOLUTION, 'Μη έγκ. τύπος συναλ.' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_NO_REQ' ERROR_CODE, 'Απαιτείται ο αριθμός συναλλαγής.' ERROR_DESC, 'Εισαγάγετε τον ορθό αριθμό συναλλαγής.' REC_SOLUTION, 'Λείπει αρ. συναλλαγής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_XTRA_CUST' ERROR_CODE, 'Μια συναλλαγή μπορεί να έχει μία μόνο εγγραφή πελάτη.' ERROR_DESC, 'Διαγράψτε την επιπλέον εγγραφή πελάτη από αυτή τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγγρ. πελάτη' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRAN_XTRA_REC' ERROR_CODE, 'Βρέθηκε άγνωστος τύπος εγγραφής.' ERROR_DESC, 'Κατεσ. αρχ. εισ. Έγκ.τύπ. εγγρ.: FHEAD, THEAD, TCUST, CATT, TITEM, IDISC, TTEND, TTAX, TTAIL, FTAIL. Επεξ. το αρχ. ή κατ. ξανά το κατ.' REC_SOLUTION, 'Άγν. τύπος εγγραφής' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO SA_ERROR_CODES_TL TL USING
(SELECT  LANG,	ERROR_CODE,  ERROR_DESC,  REC_SOLUTION,  SHORT_DESC
FROM  (SELECT 20 LANG, 'TRANLVLTAX_NO_IGTAX' ERROR_CODE, 'Ένα κατάστημα με φορολόγηση σε επίπεδο συναλλαγής δεν πρέπει να έχει εγγραφές φόρου σε επίπεδο είδους.' ERROR_DESC, 'Διαγράψτε την εγγραφή φόρου σε επίπεδο είδους από αυτήν τη συναλλαγή.' REC_SOLUTION, 'Μη έγκ. εγγραφή IGTAX' SHORT_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM SA_ERROR_CODES base where dl.ERROR_CODE = base.ERROR_CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE  SET tl.ERROR_DESC = use_this.ERROR_DESC, tl.REC_SOLUTION = use_this.REC_SOLUTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user, tl.SHORT_DESC = use_this.SHORT_DESC
WHEN NOT MATCHED THEN INSERT (LANG, ERROR_CODE, ERROR_DESC, REC_SOLUTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID, SHORT_DESC)
VALUES (use_this.LANG, use_this.ERROR_CODE, use_this.ERROR_DESC, use_this.REC_SOLUTION, sysdate, user, sysdate, user, use_this.SHORT_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO SA_ERROR_CODES base USING
( SELECT ERROR_CODE ERROR_CODE, ERROR_DESC ERROR_DESC, REC_SOLUTION REC_SOLUTION, SHORT_DESC SHORT_DESC FROM SA_ERROR_CODES_TL TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20)) USE_THIS
ON ( base.ERROR_CODE = use_this.ERROR_CODE)
WHEN MATCHED THEN UPDATE SET base.ERROR_DESC = use_this.ERROR_DESC, base.REC_SOLUTION = use_this.REC_SOLUTION, base.SHORT_DESC = use_this.SHORT_DESC;
--
DELETE FROM SA_ERROR_CODES_TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
