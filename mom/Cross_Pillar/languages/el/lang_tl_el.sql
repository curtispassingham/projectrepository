SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 1 LANG_LANG, 'Αγγλικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 2 LANG_LANG, 'Γερμανικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 3 LANG_LANG, 'Γαλλικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 4 LANG_LANG, 'Ισπανικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 5 LANG_LANG, 'Ιαπωνικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 6 LANG_LANG, 'Κορεατικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 7 LANG_LANG, 'Ρωσικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 8 LANG_LANG, 'Κινεζικά - Απλοποιημένα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 9 LANG_LANG, 'Τουρκικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 10 LANG_LANG, 'Ουγγρικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 11 LANG_LANG, 'Κινεζικά - Παραδοσιακά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 12 LANG_LANG, 'Πορτογαλικά - Βραζιλίας' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 13 LANG_LANG, 'Αραβικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 14 LANG_LANG, 'Γαλλικά Καναδά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 15 LANG_LANG, 'Κροατικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 16 LANG_LANG, 'Τσεχικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 17 LANG_LANG, 'Δανικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 18 LANG_LANG, 'Ολλανδικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 19 LANG_LANG, 'Φινλανδικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 20 LANG_LANG, 'Ελληνικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 21 LANG_LANG, 'Εβραϊκά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 22 LANG_LANG, 'Ιταλικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 23 LANG_LANG, 'Ισπανικά Λατινικής Αμερικής' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 24 LANG_LANG, 'Λιθουανικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 25 LANG_LANG, 'Νορβηγικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 26 LANG_LANG, 'Πολωνικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 27 LANG_LANG, 'Πορτογαλικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 28 LANG_LANG, 'Ρουμανικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 29 LANG_LANG, 'Σλοβακικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 30 LANG_LANG, 'Σλοβενικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 31 LANG_LANG, 'Σουηδικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 32 LANG_LANG, 'Ταϊλανδικά' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO LANG_TL TL USING
(SELECT  LANG,	LANG_LANG,  DESCRIPTION
FROM  (SELECT 20 LANG, 33 LANG_LANG, 'Ταγκαλόγκ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM LANG base where dl.LANG_LANG = base.LANG)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.LANG_LANG = use_this.LANG_LANG)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, LANG_LANG, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.LANG_LANG, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO LANG base USING
( SELECT LANG_LANG LANG, DESCRIPTION DESCRIPTION FROM LANG_TL TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20)) USE_THIS
ON ( base.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM LANG_TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
