SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'AK' STATE, 'US' COUNTRY_ID, 'Αλάσκα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'AL' STATE, 'US' COUNTRY_ID, 'Αλαμπάμα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'AR' STATE, 'US' COUNTRY_ID, 'Αρκάνσας' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'AS' STATE, 'US' COUNTRY_ID, 'Αμερικανικές Σαμόα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'AZ' STATE, 'US' COUNTRY_ID, 'Αριζόνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'CA' STATE, 'US' COUNTRY_ID, 'Καλιφόρνια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'CO' STATE, 'US' COUNTRY_ID, 'Κολοράντο' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'CT' STATE, 'US' COUNTRY_ID, 'Κονέκτικατ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'DC' STATE, 'US' COUNTRY_ID, 'Περιφέρεια Κολούμπια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'DE' STATE, 'US' COUNTRY_ID, 'Ντέλαγουερ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'FL' STATE, 'US' COUNTRY_ID, 'Φλόριντα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'GA' STATE, 'US' COUNTRY_ID, 'Τζόρτζια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'GU' STATE, 'US' COUNTRY_ID, 'Γκουάμ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'HI' STATE, 'US' COUNTRY_ID, 'Χαβάη' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'IA' STATE, 'US' COUNTRY_ID, 'Αϊόβα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'ID' STATE, 'US' COUNTRY_ID, 'Αϊντάχο' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'IL' STATE, 'US' COUNTRY_ID, 'Ιλλινόις' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'IN' STATE, 'US' COUNTRY_ID, 'Ιντιάνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'KS' STATE, 'US' COUNTRY_ID, 'Κάνσας' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'KY' STATE, 'US' COUNTRY_ID, 'Κεντάκι' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'LA' STATE, 'US' COUNTRY_ID, 'Λουιζιάνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MA' STATE, 'US' COUNTRY_ID, 'Μασαχουσέτη' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MD' STATE, 'US' COUNTRY_ID, 'Μέριλαντ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'ME' STATE, 'US' COUNTRY_ID, 'Μέιν' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MI' STATE, 'US' COUNTRY_ID, 'Μίσιγκαν' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MN' STATE, 'US' COUNTRY_ID, 'Μινεσότα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MO' STATE, 'US' COUNTRY_ID, 'Μιζούρι' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MP' STATE, 'US' COUNTRY_ID, 'Νήσοι Βόρειες Μαριάνες' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MS' STATE, 'US' COUNTRY_ID, 'Μισισίπι' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'MT' STATE, 'US' COUNTRY_ID, 'Μοντάνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NC' STATE, 'US' COUNTRY_ID, 'Βόρεια Καρολίνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'ND' STATE, 'US' COUNTRY_ID, 'Βόρεια Ντακότα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NE' STATE, 'US' COUNTRY_ID, 'Νεμπράσκα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NH' STATE, 'US' COUNTRY_ID, 'Νιου Χάμσαϊρ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NJ' STATE, 'US' COUNTRY_ID, 'Νιου Τζέρσι' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NM' STATE, 'US' COUNTRY_ID, 'Νέο Μεξικό' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NV' STATE, 'US' COUNTRY_ID, 'Νεβάδα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'NY' STATE, 'US' COUNTRY_ID, 'Νέα Υόρκη' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'OH' STATE, 'US' COUNTRY_ID, 'Οχάιο' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'OK' STATE, 'US' COUNTRY_ID, 'Οκλαχόμα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'ON' STATE, 'CA' COUNTRY_ID, 'Οντάριο' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'OR' STATE, 'US' COUNTRY_ID, 'Όρεγκον' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'PA' STATE, 'US' COUNTRY_ID, 'Πενσυλβάνια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'PR' STATE, 'US' COUNTRY_ID, 'Πουέρτο Ρίκο' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'RI' STATE, 'US' COUNTRY_ID, 'Ροντ Άιλαντ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'SC' STATE, 'US' COUNTRY_ID, 'Νότια Καρολίνα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'SD' STATE, 'US' COUNTRY_ID, 'Νότια Ντακότα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'TN' STATE, 'US' COUNTRY_ID, 'Τενεσί' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'TX' STATE, 'US' COUNTRY_ID, 'Τέξας' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'UM' STATE, 'US' COUNTRY_ID, 'Μικρές Απομονωμένες Νήσοι Ηνωμένων Πολιτειών' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'UT' STATE, 'US' COUNTRY_ID, 'Γιούτα' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'VA' STATE, 'US' COUNTRY_ID, 'Βιρτζίνια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'VI' STATE, 'US' COUNTRY_ID, 'Παρθένοι Νήσοι, Η.Π.Α.' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'VT' STATE, 'US' COUNTRY_ID, 'Βερμόντ' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'WA' STATE, 'US' COUNTRY_ID, 'Ουάσινγκτον' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'WI' STATE, 'US' COUNTRY_ID, 'Γουισκόνσιν' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO STATE_TL TL USING
(SELECT  LANG,	STATE,	COUNTRY_ID,  DESCRIPTION
FROM  (SELECT 20 LANG, 'WV' STATE, 'US' COUNTRY_ID, 'Δυτική Βιρτζίνια' DESCRIPTION FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM STATE base where dl.STATE = base.STATE and dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.STATE = use_this.STATE and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.DESCRIPTION = use_this.DESCRIPTION, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, STATE, COUNTRY_ID, DESCRIPTION, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.STATE, use_this.COUNTRY_ID, use_this.DESCRIPTION, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO STATE base USING
( SELECT STATE STATE, COUNTRY_ID COUNTRY_ID, DESCRIPTION DESCRIPTION FROM STATE_TL TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20)) USE_THIS
ON ( base.STATE = use_this.STATE and base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.DESCRIPTION = use_this.DESCRIPTION;
--
DELETE FROM STATE_TL where lang = 20
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 20);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
