SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 187 REASON, 20 LANG, 'Καθυστερημένη μείωση προσαρμογής αποθέματος μονάδας και ποσού' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 190 REASON, 20 LANG, 'Καταστροφή επιτόπου για επιστροφές χονδρ.' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 191 REASON, 20 LANG, 'Επιστροφή πελάτη χωρίς απόθεμα' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 9999 REASON, 20 LANG, 'Κωδικός μετατροπής είδους' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 1 REASON, 20 LANG, 'Συρρίκνωση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 2 REASON, 20 LANG, '(+/-) λόγω ελέγχου εξερχόμενων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 3 REASON, 20 LANG, 'Επισκευή - εσωτερικά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 5 REASON, 20 LANG, 'Ανακλήθηκε από τον κατασκευαστή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 10 REASON, 20 LANG, '(+) λόγω μετατροπής αποθέματος' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 13 REASON, 20 LANG, 'Μη διαθέσιμη απογραφή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 20 REASON, 20 LANG, '(+/-) λόγω μεταφοράς είδους' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 30 REASON, 20 LANG, '(+/-) λόγω συστήματος συλλογής μονάδων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 31 REASON, 20 LANG, 'PTS Κρυφά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 42 REASON, 20 LANG, '(+/-) λόγω απογραφής κύκλου' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 48 REASON, 20 LANG, '(+/-) λόγω διαχωρισμού συστοιχίας πακέτων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 49 REASON, 20 LANG, '(+/-) λόγω ενοποίησης παραγγελιών' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 50 REASON, 20 LANG, '(+/-) λόγω τοποθέτησης πολ.μον.διατ.απ.' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 55 REASON, 20 LANG, '(+/-) λόγω συλλογής μονάδων βάσει εντύπου' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 60 REASON, 20 LANG, '+ λόγω επιστροφής πελάτη' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 70 REASON, 20 LANG, '+ λόγω διαχωρισμού κιτ' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 81 REASON, 20 LANG, 'Ζημιά - εξωτερικά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 82 REASON, 20 LANG, 'Ζημιά - Αναστολή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 83 REASON, 20 LANG, 'Κλοπή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 84 REASON, 20 LANG, 'Χρήση καταστήματος' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 85 REASON, 20 LANG, 'Επισκευή - εξωτερικά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 86 REASON, 20 LANG, 'Δωρεά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 87 REASON, 20 LANG, 'Είσοδος απογραφής' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 88 REASON, 20 LANG, 'Έξοδος απογραφής' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 89 REASON, 20 LANG, 'Διάθεση από τα κρατημένα' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 90 REASON, 20 LANG, 'Διάθεση από Αμ. Διαθ. Αποθ.' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 91 REASON, 20 LANG, 'Απόθεμα - Κρατημένο' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 92 REASON, 20 LANG, 'Διαχείριση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 93 REASON, 20 LANG, 'Επιστροφή πελάτη' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 94 REASON, 20 LANG, 'Είσοδος μετατροπής προϊόντων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 95 REASON, 20 LANG, 'Παράδοση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 96 REASON, 20 LANG, 'Έτοιμο προς πώληση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 97 REASON, 20 LANG, 'Επιστροφές' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 98 REASON, 20 LANG, 'Έξοδος μετατροπής προϊόντων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 99 REASON, 20 LANG, '(+/-) λόγω γενικής προσαρμογής' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 110 REASON, 20 LANG, 'Με ζημιά' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 120 REASON, 20 LANG, 'Εμφάνιση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 130 REASON, 20 LANG, 'Μετατροπή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 140 REASON, 20 LANG, 'Επίδειξη μόδας' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 141 REASON, 20 LANG, 'Προσαρμογή απογραφής αποθέματος' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 142 REASON, 20 LANG, 'Κράτηση για πελάτη' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 145 REASON, 20 LANG, '- λόγω επιστροφής στον αντιπρόσωπο πωλήσεων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 146 REASON, 20 LANG, '- λόγω κλοπής' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 147 REASON, 20 LANG, 'Απώλεια λόγω πυρκαγιάς' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 148 REASON, 20 LANG, 'Απώλεια λόγω πλημμύρας' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 149 REASON, 20 LANG, 'Απώλεια λόγω μεταφοράς' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 150 REASON, 20 LANG, 'Διαφημιστικά δείγματα' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 151 REASON, 20 LANG, 'Πλεόνασμα αντιπροσώπου πωλήσεων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 152 REASON, 20 LANG, 'Μετατροπή' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 153 REASON, 20 LANG, 'Διάθεση αντιπροσώπου πωλήσεων' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 154 REASON, 20 LANG, 'Σήμανση εξάντλησης αποθέματος' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 155 REASON, 20 LANG, 'Σπασμένο' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 156 REASON, 20 LANG, 'Παλαιό' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 157 REASON, 20 LANG, 'Κλεμμένο' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 158 REASON, 20 LANG, 'Εσωτερική κατανάλωση' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 159 REASON, 20 LANG, 'Παρακολούθηση οικονομικού αποθέματος' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 160 REASON, 20 LANG, 'Φωτογραφία για κατάλογο' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 180 REASON, 20 LANG, 'Κράτηση παραγγελίας πελάτη (Εισερχόμενη)' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 181 REASON, 20 LANG, 'Κράτηση παραγγελίας πελάτη (Εξερχόμενη)' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 182 REASON, 20 LANG, 'Εισερχόμενο απόθεμα' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 183 REASON, 20 LANG, 'Εξερχόμενο απόθεμα' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 184 REASON, 20 LANG, 'Καθυστερημένη αύξηση προσαρμογής αποθέματος μονάδας' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 185 REASON, 20 LANG, 'Καθυστερημένη μείωση προσαρμογής αποθέματος μονάδας' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO INV_ADJ_REASON_TL TL USING
(SELECT  REASON,  LANG,  REASON_DESC, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 186 REASON, 20 LANG, 'Καθυστερημένη αύξηση προσαρμογής αποθέματος μονάδας και ποσού' REASON_DESC FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM INV_ADJ_REASON base where dl.REASON = base.REASON)) USE_THIS
ON ( tl.REASON = use_this.REASON and tl.LANG = use_this.LANG)
WHEN MATCHED THEN UPDATE  SET tl.REASON_DESC = use_this.REASON_DESC, tl.LAST_UPDATE_ID = user, tl.LAST_UPDATE_DATETIME = sysdate
WHEN NOT MATCHED THEN INSERT (REASON, LANG, REASON_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (use_this.REASON, use_this.LANG, use_this.REASON_DESC, use_this.ORIG_LANG_IND, 'N', user, sysdate, user, sysdate);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
