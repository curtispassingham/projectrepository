SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK

REM This script changes the data integration language. The entries in the base table is copied to the 
REM translation table to retain the value. The language strings in the new primary language is then copied
REM from the translation table to the base table. Post this, the record in new data integration language
REM is deleted from the translation table. 
REM This script is provided for fresh install. Depending on the volume of the impacted tables, this script
REM can also be used after perforamance analysis to change data integration language for an existing installation. 

prompt Copying the current primary language entries to corresponding _TL tables. 
@populate_tl.sql

prompt Updating the data integration language in system options.
update SYSTEM_CONFIG_OPTIONS set data_integration_lang = 15;
commit;

prompt Copying the new primary language entries from TL table to the corresponding base tables. 
@populate_base.sql

prompt Deleting the entries from TL table the entries in primary language. 
@delete_tl.sql


