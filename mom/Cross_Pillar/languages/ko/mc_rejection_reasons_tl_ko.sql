SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'CANNOT_CHANGE_REPL_WH' REASON_KEY, '이 물류창고는 이 SKU 재고보충의 공급지 물류창고로 사용 중이므로 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'CONSIGNMENT' REASON_KEY, '위탁 매장 %s1과(와) 비위탁 매장 간에 품목을 재분류할 수 없습니다. 이전 매장: %s1 신규 매장: %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'COST_LOC_PENDING' REASON_KEY, '동일한 항목에 대해 보류 중인 원가관리 로케이션 변경이 있으므로, 품목 %s1 및 로케이션 %s2에 대해 원가관리 로케이션 변경을 처리할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'FORECAST_REPL_METHOD' REASON_KEY, '이 품목의 로케이션이 예측 재고보충 방법을 사용 중이므로 품목을 예측불가능으로 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_ACTIVATE_DATE' REASON_KEY, '새 활성화 일자 %s1이(가) 현재 비활성 일자 %s2보다 이후입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_DEACTIVATE_DATE' REASON_KEY, '새 비활성 일자 %s1이(가) 현재 활성화 일자 %s2보다 이전입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_MAX_STOCK' REASON_KEY, '%s1 단위의 신규 최대 재고가 %s2 단위의 현재 최소 재고보다 작습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_MAX_SUPPLY_DAYS' REASON_KEY, '%s1의 신규 최대 공급일수가 %s2의 현재 최소 공급일수보다 작습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_MIN_STOCK' REASON_KEY, '%s1 단위의 신규 최소 재고가 %s2 단위의 현재 최대 재고보다 큽니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_MIN_SUPPLY_DAYS' REASON_KEY, '%s1의 신규 최소 공급일수가 %s2의 현재 최대 공급일수보다 큽니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_MULT_RUNS_PER_DAY' REASON_KEY, '재고보충 방법이 점포 주문이고, 검토 주기가 매일이고, 재고 범주가 물류창고 재고 또는 물류창고 링크인 경우에만 하루당 복수 실행 표시자가 ''Y''일 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_REPL_ITEM_TYPE' REASON_KEY, '일반상품 및 패션 SKU, 패션 스타일의 재고보충 속성만 업데이트할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_REPL_ORDER_CTRL' REASON_KEY, '재고보충할 로케이션이 점포이고 현재 주문 컨트롤이 구매자 워크시트이면 재고 범주를 %s1(으)로 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_SOURCING_WH' REASON_KEY, '품목이 신규 공급 물류창고에 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_STORE_CLOSE_DATE' REASON_KEY, '신규 폐점 일자 %s1이(가) 현재 개점 일자 %s2보다 이전입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_STORE_OPEN_DATE' REASON_KEY, '신규 개점 일자 %s1이(가) 현재 폐점 일자 %s2보다 이후입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'INVALID_TSF_ZERO_SOH_IND' REASON_KEY, '재고보충 방법이 점포 주문이고, 재고 범주가 물류창고 재고인 경우에만 대출입 보유 재고 제로화 표시자가 ''Y''일 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_IN_GROUP_ON_ORDER' REASON_KEY, '이 품목 그룹의 품목이 활성 주문에 있으므로 이 그룹의 품목에 대한 매장/등급/하위등급을 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_IN_USE' REASON_KEY, '이 품목은 활성 주문, 대출입 또는 가격 변경의 일부이므로 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_LOC_NOT_ACTIVATE' REASON_KEY, '품목 %s1은(는) 로케이션 %s2에서 활성 상태가 아닙니다. 따라서 활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_NOT_FORECASTABLE' REASON_KEY, '품목을 예측할 수 없으므로 공급 시간 및 동적 재고보충 방법을 사용할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_NOT_RANGE_COST_LOC' REASON_KEY, '원가관리 로케이션 %s1이(가) 품목 %s2에 정렬되지 않았으며, 이 품목에 정렬되지 않는 점포 또는 물류창고만 원가관리 로케이션으로 사용할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_NOT_TOLERANCEABLE' REASON_KEY, '재고 범주가 물류창고이면 허용오차를 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_ON_APP_ORD' REASON_KEY, '재분류되도록 지정한 품목 또는 해당 하위/하하위 품목이 승인된 주문에 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_ON_ORDER_ZONE' REASON_KEY, '품목이 하나 이상의 주문에 있으므로 원가 존을 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_STATUS_DELETE' REASON_KEY, '%s1 %s2에 대한 현재 품목 상태가 삭제됨이므로 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_SUPPLIER' REASON_KEY, '공급자 %s1이(가) 품목에 연계되어 있지 않습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ITEM_SUPPLIER_COUNTRY' REASON_KEY, '공급자 %s1/원산지 국가 %s2이(가) 품목에 연계되어 있지 않습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'LAST_PHASE' REASON_KEY, '삭제할 수 없습니다. 이 품목과 관련하여 하나의 시즌만 지정됩니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'MAIN_EXISTS_AS_SUB' REASON_KEY, '주력 품목 %s1이(가) 로케이션 %s2에 대체 품목으로 이미 있으므로 해당 로케이션에 지정할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'MAIN_ITEM_FORECASTS' REASON_KEY, '품목이 대체 품목의 주력 품목이고 처리 중이며 해당 예측이 사용 중입니다. 예측불가능으로 전환할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'MAIN_ITEM_LOC_MISMATCH' REASON_KEY, '주력 품목 %s1이(가) 로케이션 %s2에 없습니다. 대체 품목 대화 상자에서는 기존의 품목-로케이션 조합만 사용할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NOT_LIST_SKU_STYLE_S' REASON_KEY, '일반상품 SKU, 패션 SKU 또는 패션 스타일의 속성만 업데이트할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NOT_LIST_SKU_STYLE_W' REASON_KEY, '일반상품 SKU, 패션 SKU 또는 패션 스타일의 속성만 업데이트할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NOT_ON_REPL_NO_DEACTIVATE' REASON_KEY, '품목 로케이션이 재고보충 상태가 아니므로 비활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NOT_ON_REPL_NO_UPDATE' REASON_KEY, '품목 로케이션이 재고보충 상태가 아니므로 속성을 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NOT_SKU_STYLE' REASON_KEY, '품목 표시자 속성은 일반상품 SKU 및 패션 스타일 레벨에만 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_ASSOC_UDA_VALUE' REASON_KEY, '품목의 새 분류체계 %s1/%s2/%s3에는 연계된 기본 UDA(사용자 정의 속성) 값이 없는 기본 UDA가 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_BUYER_WKSHT' REASON_KEY, '현재 재고 범주가 %s1이고 재고보충할 로케이션이 점포이면 주문 컨트롤을 구매자 워크시트로 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_DOMAIN_EXISTS' REASON_KEY, '품목의 상품 분류체계가 도메인에 연계되어 있지 않으므로 품목을 예측가능으로 설정할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_REQUIRED_UDA' REASON_KEY, '품목 %s1은(는) UDA %s2에 연계되어야 합니다. 이 레코드를 제거하면 이 조건을 위반하게 됩니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_UPDATE_CHILD_COST_ZONE' REASON_KEY, '품목이 하위이므로 품목 원가 존 그룹을 업데이트할 수 없으며, 상위 품목에 동일한 원가 존이 있어야 합니다. 상위 품목이 업데이트의 일부인 경우 품목도 업데이트할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_UPD_EXIST_VAT' REASON_KEY, 'SKU %s1에는 해당 부가가치세 지역 및 활성 일자에 부가가치세 유형이 ''C'' 또는 ''R''인 부가가치세 SKU 레코드가 이미 있습니다. ''B''로 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'NO_UPD_VAT_TYPE' REASON_KEY, 'SKU %s1에는 해당 부가가치세 지역 및 활성 일자에 부가가치세 유형이 ''B''인 부가가치세 SKU 레코드가 있습니다. ''C'' 또는 ''R''로 업데이트할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ON_CLEAR_NO_ACTIVATE' REASON_KEY, '품목 로케이션이 재고정리 상태이므로 활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ON_CONSIGN_NO_ACTIVATE' REASON_KEY, '품목 로케이션이 위탁 상태이므로 활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'ON_REPL_NO_ACTIVATE' REASON_KEY, '품목 로케이션이 이미 재고보충 상태이므로 활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'PACK_FORECAST' REASON_KEY, '품목이 예측할 수 없는 팩 품목입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'PACK_ITEM_LOC_MISMATCH' REASON_KEY, '기본 재고보충 팩 %s1이(가) 로케이션 %s2에 없습니다. 대체 품목 대화 상자에서는 기존의 품목-로케이션 조합만 사용할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'PACK_LOC_NO_EXIST' REASON_KEY, '기본 재고보충 팩 %s1이(가) 활성 상태인 로케이션 %s2에 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'PACK_STATUS' REASON_KEY, '기본 재고보충 팩 %s1은(는) 구성요소 품목 %s2과(와) 동일한 상태가 아니므로 유효하지 않습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'RECLASS_EXIST' REASON_KEY, '품목이 보류 중인 재분류에 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'RECLASS_NO_DOMAIN' REASON_KEY, '이 품목은 예측가능입니다. 매장 %s1, 등급 %s2, 하위등급 %s3의 신규 상품 분류체계가 도메인에 연계되지 않아 품목을 재분류할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'REJECT_DEPT_SIZE' REASON_KEY, '신규 매장 %s1에 필요한 하나 이상의 크기가 누락되었으므로 스타일을 재분류할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'REJECT_ITEM_LEVEL' REASON_KEY, '레벨 1 품목만 재분류할 수 있습니다. 품목 %s1은(는) 레벨 %s2 품목입니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'REJECT_ITEM_STATUS' REASON_KEY, '품목이 활성 주문에 있으므로 매장을 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SIMPLE_PACK' REASON_KEY, '동종 팩 %s1을(를) 재분류할 수 없습니다. 해당 구성요소 품목과 함께 해야만 재분류할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SINGLE_UDA_EXISTS' REASON_KEY, '품목 %s1이(가) UDA %s2에 이미 연계되어 있으므로 이 UDA에는 품목당 하나의 연계만 허용됩니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SKU_IN_ACTIVE_PACK' REASON_KEY, '이 품목은 활성 팩의 일부입니다. 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SKU_ORD_EXIST' REASON_KEY, '활성 주문이 있는 품목입니다. 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SKU_TSF_EXIST' REASON_KEY, '활성 대출입이 있는 품목입니다. 상태를 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SOURCE_WH_NO_EXIST' REASON_KEY, '품목이 물류창고 %s1에 연계되어 있지 않거나 활성 상태가 아니므로 공급 물류창고로 사용할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'STOCK_COUNT_PEND' REASON_KEY, '이 품목은 현재 재고 조사 중입니다. 지금은 재분류할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'STYLE_SKU_ON_ORDER' REASON_KEY, '품목이 스타일이고 하나 이상의 SKU가 하나 이상의 주문에 있으므로 원가 존을 변경할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUB_EXISTS_AS_MAIN' REASON_KEY, '대체 품목 %s1이(가) 주력 품목으로 로케이션 %s2에 이미 있으므로 해당 로케이션에 지정할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUB_ITEM_FORECASTS' REASON_KEY, '품목이 대체 품목이고 해당 예측은 사용 중입니다. 예측불가능으로 전환할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUB_ITEM_LOC_MISMATCH' REASON_KEY, '대체 품목 %s1이(가) 로케이션 %s2에 없습니다. 대체 품목 대화 상자에서는 기존의 품목-로케이션 조합만 사용할 수 있습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUB_ITEM_NO_ACTIVATE' REASON_KEY, '이 품목은 대체 품목이므로 활성화할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUPP_NO_EXIST_PACK' REASON_KEY, '팩 %s1이(가) 공급자 %s2 및 원산지 국가 %s3에 연계되어 있지 않습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'SUP_LOC_NOT_SAME_ORG_UNIT' REASON_KEY, '다른 조직 단위에 속하므로 공급자 %s1과(와) 로케이션 %s2을(를) 연계할 수 없습니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'UDA_REQD' REASON_KEY, '품목의 새 분류체계에는 아직 품목에 지정되지 않은 UDA 분류체계가 필요합니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 6 LANG, 'XDOCK_WH_ITEM_NOT_ACTIVE' REASON_KEY, 'SKU %s1은(는) 지정된 크로스도킹 물류창고 %s2에서 활성 상태가 아닙니다.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO MC_REJECTION_REASONS base USING
( SELECT REASON_KEY REASON_KEY, REJECTION_REASON REJECTION_REASON FROM MC_REJECTION_REASONS_TL TL where lang = 6
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 6)) USE_THIS
ON ( base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE SET base.REJECTION_REASON = use_this.REJECTION_REASON;
--
DELETE FROM MC_REJECTION_REASONS_TL where lang = 6
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 6);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
