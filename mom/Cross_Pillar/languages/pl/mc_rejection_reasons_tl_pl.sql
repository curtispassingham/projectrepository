SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'CANNOT_CHANGE_REPL_WH' REASON_KEY, 'Nie można zmienić statusu tego magazynu, ponieważ jest on używany jako magazyn źródłowy uzupełnień tej JMG.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'CONSIGNMENT' REASON_KEY, 'Nie można zmienić klasyfikacji pozycji między działem przesyłki %s1 a działem bez przesyłki. Stary dział: %s1 Nowy dział: %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'COST_LOC_PENDING' REASON_KEY, 'Nie można przetworzyć zmiany lokalizacji kalkulacji kosztów dla pozycji %s1 i lokalizacji %s2, ponieważ istnieje już oczekująca zmiana lokalizacji kalkulacji kosztów dla tych elementów.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'FORECAST_REPL_METHOD' REASON_KEY, 'Lokalizacje dla tej pozycji używają metody prognozy uzupełnień, dlatego nie można zmienić pozycji na nieprzewidywalną.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_ACTIVATE_DATE' REASON_KEY, 'Nowa data początku uzup., %s1, jest późniejsza niż bieżąca data końca uzup., %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_DEACTIVATE_DATE' REASON_KEY, 'Nowa data końca uzup., %s1, jest wcześniejsza niż bieżąca data początku uzup., %s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_MAX_STOCK' REASON_KEY, 'Nowy maks. poziom zapasów wynosz. %s1 jednostek jest niższy niż bieżący min. poziom zapasów wynosz. %s2 jedn.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_MAX_SUPPLY_DAYS' REASON_KEY, 'Nowa maksymalna liczba dni szybkiej dostawy (%s1) jest mniejsza niż bieżąca minimalna liczba dni szybkiej dostawy (%s2).' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_MIN_STOCK' REASON_KEY, 'Nowy min. poziom zapasów wynoszący %s1 jedn. jest wyższy niż bieżący maks. poziom zapasów wynosz. %s2 jedn.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_MIN_SUPPLY_DAYS' REASON_KEY, 'Nowa minimalna liczba dni szybkiej dostawy (%s1) jest większa niż bieżąca maksymalna liczba dni szybkiej dostawy (%s2).' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_MULT_RUNS_PER_DAY' REASON_KEY, 'Wskaźnik wielu sesji w ciągu dnia może mieć wartość ''T'' tylko w przypadku, gdy metoda uzupełniania to "Zamówienia sklepu", obowiązuje dzienny cykl sprawdzania, a ustawienie "Kategoria zapasów" ma wartość "Zapasy z magazynu" lub "Łącze magazynu".' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_REPL_ITEM_TYPE' REASON_KEY, 'Atrybuty uzupełnienia można aktualizować tylko dla stałych i sezonowych pozycji JMG oraz stylów pozycji sezonowych.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_REPL_ORDER_CTRL' REASON_KEY, 'Kategoria zapasów nie może zostać zaktualizowana do %s1, gdy lokalizacją uzupełnienia jest sklep, a bieżące zamówienie jest kontrolowane przez arkusz roboczy kupca.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_SOURCING_WH' REASON_KEY, 'Pozycja nie istnieje w nowym magazynie źródłowym.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_STORE_CLOSE_DATE' REASON_KEY, 'Nowa data zamknięcia sklepu, %s1, jest wcześniejsza niż bieżąca data otwarcia sklepu, %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_STORE_OPEN_DATE' REASON_KEY, 'Nowa data otwarcia sklepu, %s1, jest późniejsza niż bieżąca data zamknięcia sklepu, %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'INVALID_TSF_ZERO_SOH_IND' REASON_KEY, 'Wskaźnik transferu przy zerowych zapasach na stanie może mieć wartość ''T'' tylko w przypadku, gdy metoda uzupełniania to "Zamówienia sklepu", a w polu "Kategoria zapasów" wprowadzono wartość "Zapasy z magazynu".' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_IN_GROUP_ON_ORDER' REASON_KEY, 'Pozycja z tej grupy znajduje się na aktywnym zamówieniu, zatem nie można zmienić działu, klasy lub subklasy jakichkolwiek pozycji w grupie' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_IN_USE' REASON_KEY, 'Ta pozycja jest częścią aktywnego zamówienia, transferu lub zmiany ceny, zatem nie można zmienić jej statusu.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_LOC_NOT_ACTIVATE' REASON_KEY, 'Pozycja %s1 jest nieaktywna w lokalizacji %s2, zatem nie można jej aktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_NOT_FORECASTABLE' REASON_KEY, 'Ta pozycja jest nieprzewidywalna, zatem metody szybkiej dostawy i dynamicznego uzupełnienia nie mogą być używane.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_NOT_RANGE_COST_LOC' REASON_KEY, 'Lokalizacja kalkulacji kosztów %s1 nie należy do zakresu pozycji %s2. Tylko sklep lub magazyn należący do zakresu tej pozycji może być używany jako lokalizacja kalkulacji kosztów.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_NOT_TOLERANCEABLE' REASON_KEY, 'Nie można zaktualiz. tolerancji, jeśli kategorią zapasów jest magazyn.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_ON_APP_ORD' REASON_KEY, 'Określone pozycje lub ich pozycje podrzędne objęte zmianą klasyfikacji znajdują się w zatwierdzonych zamówieniach.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_ON_ORDER_ZONE' REASON_KEY, 'Nie można zmienić strefy kosztu, ponieważ pozycja należy do co najmniej jednego zamówienia.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_STATUS_DELETE' REASON_KEY, 'Bieżący status pozycji dla %s1 %s2 to "Skasowana", zatem nie można zmienić jej statusu.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_SUPPLIER' REASON_KEY, 'Dostawca %s1 nie jest powiązany z pozycją.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ITEM_SUPPLIER_COUNTRY' REASON_KEY, 'Dostawca %s1 lub kraj pochodzenia %s2 nie jest powiązany z pozycją.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'LAST_PHASE' REASON_KEY, 'Nie można skasować. Ta pozycja ma przypisany tylko jeden sezon.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'MAIN_EXISTS_AS_SUB' REASON_KEY, 'Nie można przypisać pozycji głównej %s1 do lokalizacji %s2, ponieważ pozycja ta jest już pozycją zastępczą dla tej lokalizacji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'MAIN_ITEM_FORECASTS' REASON_KEY, 'Jest to pozycja główna w odniesieniu do pozycji zastępczej, a jej przetwarzanie i prognozy są w użyciu. Nie można zmienić pozycji na nieprzewidywalną.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'MAIN_ITEM_LOC_MISMATCH' REASON_KEY, 'Pozycja główna %s1 nie występuje w lokalizacji %s2. W oknie dialogowym "Pozycje zastępcze" mogą być używane tylko istniejące kombinacje pozycja-lokalizacja.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NOT_LIST_SKU_STYLE_S' REASON_KEY, 'Atrybuty można aktualizować tylko dla stałych i sezonowych pozycji JMG lub stylów pozycji sezonowych.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NOT_LIST_SKU_STYLE_W' REASON_KEY, 'Atrybuty można aktualizować tylko dla stałych i sezonowych pozycji JMG lub stylów pozycji sezonowych.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NOT_ON_REPL_NO_DEACTIVATE' REASON_KEY, 'Lokalizacja pozycji nie znajduje się w uzupełnieniu, zatem nie można jej dezaktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NOT_ON_REPL_NO_UPDATE' REASON_KEY, 'Lokalizacja pozycji nie znajduje się w uzupełnieniu, zatem nie można zaktualizować atrybutów.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NOT_SKU_STYLE' REASON_KEY, 'Atrybuty wskaźnika pozycji istnieją tylko na poziomach stałej pozycji JMG i stylu pozycji sezonowych.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_ASSOC_UDA_VALUE' REASON_KEY, 'Nowa hierarchia pozycji (%s1/%s2/%s3) ma domyślne atrybuty zdefiniowane przez użytkownika bez powiązanej domyślnej wartości AZPU.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_BUYER_WKSHT' REASON_KEY, 'Kontrola zamówienia nie może zostać zaktualizowana do arkusza roboczego kupca, gdy bieżąca kategoria zapasów to %s1, a lokalizacją uzupełnienia jest sklep.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_DOMAIN_EXISTS' REASON_KEY, 'Hierarchia pozycji nie jest powiązana z domeną, zatem nie można ustawić pozycji jako przewidywalnej.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_REQUIRED_UDA' REASON_KEY, 'Pozycja %s1 musi być powiązana z AZPU %s2. Przeniesienie tego rekordu spowoduje naruszenie warunku.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_UPDATE_CHILD_COST_ZONE' REASON_KEY, 'Nie można zaktualizować grupy strefy kosztów pozycji, ponieważ pozycja jest elementem podrzędnym, a obiekt nadrzędny elementu musi mieć tę samą strefę kosztu. Jeśli element nadrz. pozycji jest objęty aktualizacją, pozycja nadal może zostać zaktualizowana.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_UPD_EXIST_VAT' REASON_KEY, 'JMG %s1 ma już rekord JMG VAT dla tego regionu VAT i datę obowiązywania z podatkiem VAT typu C lub R. Nie można zaktualizować rekordu do typu B.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'NO_UPD_VAT_TYPE' REASON_KEY, 'JMG %s1 ma rekord JMG VAT z typem podatku VAT B dla tego regionu VAT i daty obowiązywania. Nie można zaktualizować rekordu do typu C ani R.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ON_CLEAR_NO_ACTIVATE' REASON_KEY, 'Lokalizacja pozycji jest objęta wyprzedażą, zatem nie można jej aktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ON_CONSIGN_NO_ACTIVATE' REASON_KEY, 'Lokalizacja pozycji jest objęta przesyłką, zatem nie można jej aktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'ON_REPL_NO_ACTIVATE' REASON_KEY, 'Lokalizacja pozycji jest już objęta uzupełnieniem, zatem nie można jej aktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'PACK_FORECAST' REASON_KEY, 'Pozycja jest pozycją paczki, której nie można prognozować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'PACK_ITEM_LOC_MISMATCH' REASON_KEY, 'Podstawowa paczka uzupełnienia %s1 nie występuje w lokalizacji %s2. W oknie dialogowym "Pozycje zastępcze" mogą być używane tylko istniejące kombinacje pozycja-lokalizacja.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'PACK_LOC_NO_EXIST' REASON_KEY, 'Podstawowa paczka uzupełnienia %s1 nie ma statusu aktywnego i nie istnieje w lokalizacji %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'PACK_STATUS' REASON_KEY, 'Podstawowa paczka uzupełnienia %s1 jest nieprawidłowa, ponieważ jej status jest inny niż status składnika pozycji %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'RECLASS_EXIST' REASON_KEY, 'Pozycja istnieje w innej oczekującej zmianie klasyfikacji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'RECLASS_NO_DOMAIN' REASON_KEY, 'Ta pozycja jest przewidywalna. Nowa hierarchia towarów - dział %s1, klasa %s2 i subklasa %s3 - nie jest powiązana z domeną, zatem nie można zmienić klasyfikacji pozycji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'REJECT_DEPT_SIZE' REASON_KEY, 'Nie można zmienić klasyfikacji stylu, ponieważ w nowym dziale %s1 brakuje co najmniej jednego wymaganego rozmiaru.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'REJECT_ITEM_LEVEL' REASON_KEY, 'Klasyfikację można zmienić tylko w przypadku pozycji poziomu pierwszego. Pozycja %s1 należy do poziomu %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'REJECT_ITEM_STATUS' REASON_KEY, 'Pozycja znajduje się na aktywnym zamówieniu, zatem nie można zmienić jej działu.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SIMPLE_PACK' REASON_KEY, 'Nie można zmienić klasyf. prostej paczki %s1. Klasyfikację można zmienić tylko wraz ze składnikiem pozycji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SINGLE_UDA_EXISTS' REASON_KEY, 'Pozycja %s1 jest już powiązana z AZPU %s2, w przypadku którego może występować tylko jedno powiązanie na pozycję.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SKU_IN_ACTIVE_PACK' REASON_KEY, 'Ta pozycja jest częścią aktywnej paczki. Nie można zmienić jej statusu.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SKU_ORD_EXIST' REASON_KEY, 'Ta pozycja ma aktywne zamówienia. Nie można zmienić jej statusu' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SKU_TSF_EXIST' REASON_KEY, 'Ta pozycja ma aktywny transfer. Nie można zmienić jej statusu' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SOURCE_WH_NO_EXIST' REASON_KEY, 'Pozycja nie jest powiązana z magazynem %s1 lub nie jest aktywna w tym magazynie, zatem nie może być on używany jako źródłowy.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'STOCK_COUNT_PEND' REASON_KEY, 'Ta pozycja jest obecnie objęta inwentaryzacją.   W tej chwili nie można zmienić jej klasyfikacji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'STYLE_SKU_ON_ORDER' REASON_KEY, 'Nie można zmienić strefy kosztu, ponieważ pozycja należy do stylu, a co najmniej jedna JMG istnieje w jednym lub większej liczbie zamówień.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUB_EXISTS_AS_MAIN' REASON_KEY, 'Pozycji zastępczej %s1 nie można przypisać do lokalizacji %s2, ponieważ jest ona już w niej pozycją główną.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUB_ITEM_FORECASTS' REASON_KEY, 'Pozycja jest pozycją zastępczą, a jej prognozy są w użyciu. Nie można zmienić pozycji na nieprzewidywalną.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUB_ITEM_LOC_MISMATCH' REASON_KEY, 'Pozycja zastępcza %s1 nie występuje w lokalizacji %s2. W oknie dialogowym "Pozycje zastępcze" mogą być używane tylko istniejące kombinacje pozycja-lokalizacja.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUB_ITEM_NO_ACTIVATE' REASON_KEY, 'Pozycja jest pozycją zastępczą, zatem nie można jej aktywować.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUPP_NO_EXIST_PACK' REASON_KEY, 'Paczka %s1 nie jest powiązana z dostawcą %s2 i krajem pochodzenia %s3.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'SUP_LOC_NOT_SAME_ORG_UNIT' REASON_KEY, 'Nie można powiązać dostawcy %s1 i lokalizacji %s2, ponieważ należą do różnych jednostek organizacyjnych.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'UDA_REQD' REASON_KEY, 'Nowa hierarchia tej pozycji wymaga AZPU, które jeszcze nie zostały przypisane do pozycji.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 26 LANG, 'XDOCK_WH_ITEM_NOT_ACTIVE' REASON_KEY, 'JMG %s1 nie jest aktywna w określonym magazynie kompletacji %s2.' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO MC_REJECTION_REASONS base USING
( SELECT REASON_KEY REASON_KEY, REJECTION_REASON REJECTION_REASON FROM MC_REJECTION_REASONS_TL TL where lang = 26
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 26)) USE_THIS
ON ( base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE SET base.REJECTION_REASON = use_this.REJECTION_REASON;
--
DELETE FROM MC_REJECTION_REASONS_TL where lang = 26
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 26);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
