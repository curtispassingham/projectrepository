SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'INSINT' COMP_ID, 'Ubezpieczenie międzynarodowe' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, '200' COMP_ID, 'Fracht lotniczy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, '300' COMP_ID, 'Fracht lądowy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, '60' COMP_ID, 'Logistyka innej firmy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, '55' COMP_ID, 'Opłaty za dekonsolidację' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'ICING' COMP_ID, 'Opłaty dodatkowe' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'ORDCST' COMP_ID, 'Koszt zamówienia' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TEXP' COMP_ID, 'Wydatek łączny' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TEXPC' COMP_ID, 'Wydatek łączny na powiat' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TEXPZ' COMP_ID, 'Wydatek łączny na strefę' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'UNCST' COMP_ID, 'Koszt jednostkowy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BAPAL' COMP_ID, 'Bonifikata zależ. od lok. na paletę' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BACWT' COMP_ID, 'Bonifikata zależna od lokalizacji na cetnar' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BAEA' COMP_ID, 'Bonifikata zal. od lokaliz. na szt.' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BACS' COMP_ID, 'Bonifikata zal. od lok. na op. zbiorcze' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BACFT' COMP_ID, 'Bonifikata zależna od lok. na stopę sześc.' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TSFFRGHT' COMP_ID, 'Fracht transferowy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TSFINSUR' COMP_ID, 'Ubezpieczenie transferu' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'WHFEE' COMP_ID, 'Opłata za magazynowanie' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'WHPROC' COMP_ID, 'Opłata za przetwarz. magazynowe' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'AGCOMM' COMP_ID, 'Prowizja agenta' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'ILFRT' COMP_ID, 'Fracht śródlądowy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'BUYCOMM' COMP_ID, 'Prowizja kupca' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'OCFRT' COMP_ID, 'Fracht oceaniczny' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'INSUR' COMP_ID, 'Ubezpiecz.' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'SELLCOMM' COMP_ID, 'Prowizja sprzedawcy' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'ROYALTY' COMP_ID, 'Opłata licencyjna' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'VFD25US' COMP_ID, 'Cło USA - 25% wartości' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'VFD50US' COMP_ID, 'Cło USA - 50% wartości' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'VFD75US' COMP_ID, 'Cło USA - 75% wartości' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'MPFUS' COMP_ID, 'Opłata za przetworzenie towaru USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TDTYPE' COMP_ID, 'Łączne cło USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'HMFUS' COMP_ID, 'Opłata za obsługę portową USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'ADUS' COMP_ID, 'Cło antydumping. USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'CVDUS' COMP_ID, 'Cło wyrównawcze USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO ELC_COMP_TL TL USING
(SELECT  LANG,	COMP_ID,  COMP_DESC
FROM  (SELECT 26 LANG, 'TDTYUS' COMP_ID, 'Łączne cło USA' COMP_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM ELC_COMP base where dl.COMP_ID = base.COMP_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE  SET tl.COMP_DESC = use_this.COMP_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COMP_ID, COMP_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COMP_ID, use_this.COMP_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO ELC_COMP base USING
( SELECT COMP_ID COMP_ID, COMP_DESC COMP_DESC FROM ELC_COMP_TL TL where lang = 26
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 26)) USE_THIS
ON ( base.COMP_ID = use_this.COMP_ID)
WHEN MATCHED THEN UPDATE SET base.COMP_DESC = use_this.COMP_DESC;
--
DELETE FROM ELC_COMP_TL where lang = 26
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 26);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
