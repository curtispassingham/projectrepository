SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'AGG' UOM, 'AGG' UOM_TRANS, '銀含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'AUG' UOM, 'AUG' UOM_TRANS, '黃金含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BA' UOM, '桶' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BBL' UOM, '桶' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BE' UOM, '束' UOM_TRANS, '捆' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BG' UOM, '袋' UOM_TRANS, '袋' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BI' UOM, '箱' UOM_TRANS, '大箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BJ' UOM, '桶' UOM_TRANS, '桶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BK' UOM, '籃' UOM_TRANS, '籃' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'BX' UOM, 'UOM' UOM_TRANS, '盒' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'C' UOM, 'C' UOM_TRANS, '攝氏' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CA' UOM, '罐' UOM_TRANS, '罐' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CAR' UOM, 'CAR' UOM_TRANS, '克拉' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CBM' UOM, 'CBM' UOM_TRANS, '立方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CC' UOM, 'CC' UOM_TRANS, '立方公分' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CFT' UOM, 'CFT' UOM_TRANS, '立方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CG' UOM, 'CG' UOM_TRANS, '毫克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CGM' UOM, 'CGM' UOM_TRANS, '含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CKG' UOM, 'CKG' UOM_TRANS, '含量千克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CM' UOM, 'CM' UOM_TRANS, '公分' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CM2' UOM, 'CM2' UOM_TRANS, '平方公分' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CM3' UOM, 'CM3' UOM_TRANS, '立方公分' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CON' UOM, 'CON' UOM_TRANS, '容器' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CR' UOM, 'CR' UOM_TRANS, '條板箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CS' UOM, '盒' UOM_TRANS, '貨箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CT' UOM, 'CT' UOM_TRANS, '紙箱' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CTN' UOM, 'CTN' UOM_TRANS, '含量噸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CU' UOM, 'CU' UOM_TRANS, '立方' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CUR' UOM, 'CUR' UOM_TRANS, '居里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CY' UOM, 'CY' UOM_TRANS, '清除良品率' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CYG' UOM, 'CYG' UOM_TRANS, '清除良品率克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'CYK' UOM, 'CYK' UOM_TRANS, '清除良品率千克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'D' UOM, 'D' UOM_TRANS, '丹尼數' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'DEG' UOM, '度' UOM_TRANS, '度' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'DOZ' UOM, 'AGG' UOM_TRANS, '打' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'DPC' UOM, '打' UOM_TRANS, '十二件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'DPR' UOM, 'DPR' UOM_TRANS, '十二對' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'DS' UOM, 'ADS' UOM_TRANS, '劑量' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'EA' UOM, 'EA' UOM_TRANS, '每個' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FBM' UOM, 'FBM' UOM_TRANS, '纖維米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FIB' UOM, 'FIB' UOM_TRANS, '纖維' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FT' UOM, 'FT' UOM_TRANS, '英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FT2' UOM, 'FT2' UOM_TRANS, '平方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FT3' UOM, 'FT3' UOM_TRANS, '立方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'FZ' UOM, 'FZ' UOM_TRANS, '液體盎司' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'G' UOM, '克' UOM_TRANS, '克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GBQ' UOM, 'GBQ' UOM_TRANS, '千兆貝可' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GCN' UOM, 'GCN' UOM_TRANS, '容器毛重' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GKG' UOM, 'GKG' UOM_TRANS, '黃金含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GL' UOM, 'GL' UOM_TRANS, '加侖' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GM' UOM, '克' UOM_TRANS, '克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GR' UOM, '總' UOM_TRANS, '毛' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GRL' UOM, 'GRL' UOM_TRANS, 'HTSUPLD 自動插入' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GRS' UOM, '總' UOM_TRANS, '毛' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'GVW' UOM, 'GVW' UOM_TRANS, '車輛毛重' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'HND' UOM, '百' UOM_TRANS, '百' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'HUN' UOM, '百' UOM_TRANS, '百' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'HZ' UOM, 'HZ' UOM_TRANS, '赫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'IN' UOM, 'IN' UOM_TRANS, '英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'IN2' UOM, 'IN2' UOM_TRANS, '平方英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'IN3' UOM, 'IN3' UOM_TRANS, '立方英寸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'IRC' UOM, 'IRC' UOM_TRANS, '內部收益 COD' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'IRG' UOM, 'IRG' UOM_TRANS, '銥含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'JR' UOM, '瓶' UOM_TRANS, '瓶' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'JWL' UOM, 'JWL' UOM_TRANS, '焦耳' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'K' UOM, 'K' UOM_TRANS, '1,000' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KCAL' UOM, 'KCAL' UOM_TRANS, '千卡' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KG' UOM, 'KG' UOM_TRANS, '千克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KHZ' UOM, 'KHZ' UOM_TRANS, '千赫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KM' UOM, 'KM' UOM_TRANS, '公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KM2' UOM, 'KM2' UOM_TRANS, '平方公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KM3' UOM, 'KM3' UOM_TRANS, '立方公里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KN' UOM, 'KN' UOM_TRANS, '千牛頓' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KPA' UOM, 'KPA' UOM_TRANS, '千帕' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KSB' UOM, 'KSB' UOM_TRANS, '千標準磚' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KTS' UOM, 'KTS' UOM_TRANS, '糖總計重量/千克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KVA' UOM, 'KVA' UOM_TRANS, '千伏特-安培' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KVAR' UOM, 'KVAR' UOM_TRANS, '反應性千伏特-安培' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KW' UOM, 'KW' UOM_TRANS, '千瓦' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'KWH' UOM, 'KWH' UOM_TRANS, '千瓦小時' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'L' UOM, 'L' UOM_TRANS, '升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'LB' UOM, '磅' UOM_TRANS, '磅' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'LBS' UOM, '磅' UOM_TRANS, '磅' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'LIN' UOM, 'LIN' UOM_TRANS, '直線' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'LNM' UOM, 'LNM' UOM_TRANS, '直線米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'M' UOM, '米' UOM_TRANS, '米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'M2' UOM, 'M2' UOM_TRANS, '平方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'M3' UOM, 'M3' UOM_TRANS, '立方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MBQ' UOM, 'MBQ' UOM_TRANS, '百萬貝可' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MC' UOM, 'MC' UOM_TRANS, '毫居里' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MG' UOM, 'MG' UOM_TRANS, '毫克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MHZ' UOM, 'MHZ' UOM_TRANS, '兆赫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'ML' UOM, 'ML' UOM_TRANS, '毫升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MM' UOM, 'MM' UOM_TRANS, '毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MM2' UOM, 'MM2' UOM_TRANS, '平方毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MM3' UOM, 'MM3' UOM_TRANS, '立方毫米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MPA' UOM, 'MPA' UOM_TRANS, '百萬帕斯卡' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'MWH' UOM, 'MWH' UOM_TRANS, '百萬瓦小時' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'NA' UOM, 'NA' UOM_TRANS, '不可用' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'NO' UOM, 'NO' UOM_TRANS, '編號' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'ODE' UOM, 'ODE' UOM_TRANS, '臭氧消耗約當數' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'OSG' UOM, 'OSG' UOM_TRANS, '鋨含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'OZ' UOM, 'OZ' UOM_TRANS, '盎司' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PACK' UOM, '盒' UOM_TRANS, '包裝' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PAL' UOM, 'PAL' UOM_TRANS, '貨板' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PC' UOM, '件' UOM_TRANS, '件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PCS' UOM, '件' UOM_TRANS, '件' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PDG' UOM, 'PDG' UOM_TRANS, '鈀含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PF' UOM, 'PF' UOM_TRANS, '酒度' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PFL' UOM, 'PFL' UOM_TRANS, '酒度升' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PK' UOM, '盒' UOM_TRANS, '包裝' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PO' UOM, '罐' UOM_TRANS, '壺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PRS' UOM, '雙' UOM_TRANS, '對' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PT' UOM, 'PT' UOM_TRANS, '品脫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'PTG' UOM, 'PTG' UOM_TRANS, '鈽含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'QT' UOM, 'QT' UOM_TRANS, '夸脫' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'RBA' UOM, 'RBA' UOM_TRANS, '出口包' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'RHG' UOM, 'RHG' UOM_TRANS, '銠含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'RPM' UOM, 'RPM' UOM_TRANS, '每分鐘旋轉' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'RUG' UOM, 'RUG' UOM_TRANS, '釕含量克' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'SBE' UOM, 'SBE' UOM_TRANS, '標準磚約當數' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'SF' UOM, 'SF' UOM_TRANS, '平方英尺' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'SME' UOM, 'SME' UOM_TRANS, '平方米約當數' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'SQ' UOM, 'SQ' UOM_TRANS, '平方' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'SQM' UOM, 'SQM' UOM_TRANS, '平方米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'T' UOM, 'T' UOM_TRANS, '公噸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'THM' UOM, 'THM' UOM_TRANS, '千米' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'THS' UOM, 'THS' UOM_TRANS, '千單位' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'TNV' UOM, 'TNV' UOM_TRANS, '噸原始數值' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'TON' UOM, '噸' UOM_TRANS, '噸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'TS' UOM, 'TS' UOM_TRANS, '短噸' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'V' UOM, 'V' UOM_TRANS, '伏特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'W' UOM, 'W' UOM_TRANS, '瓦特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'WTS' UOM, '瓦' UOM_TRANS, '瓦特' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'X' UOM, 'X' UOM_TRANS, '無收集單位' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'YD' UOM, '碼' UOM_TRANS, '碼' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'YD2' UOM, 'YD2' UOM_TRANS, '平方碼' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO UOM_CLASS_TL TL USING
(SELECT  LANG,	UOM,  UOM_TRANS,  UOM_DESC_TRANS, DECODE(DL.LANG,SCO.DATA_INTEGRATION_LANG,'Y','N') ORIG_LANG_IND
FROM  (SELECT 11 LANG, 'YD3' UOM, 'YD3' UOM_TRANS, '立方碼' UOM_DESC_TRANS FROM DUAL) DL, SYSTEM_CONFIG_OPTIONS SCO
WHERE EXISTS  (SELECT 1 FROM UOM_CLASS base where dl.UOM = base.UOM)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.UOM = use_this.UOM)
WHEN MATCHED THEN UPDATE  SET tl.UOM_TRANS = use_this.UOM_TRANS, tl.UOM_DESC_TRANS = use_this.UOM_DESC_TRANS, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, UOM, UOM_TRANS, UOM_DESC_TRANS, ORIG_LANG_IND, REVIEWED_IND, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.UOM, use_this.UOM_TRANS, use_this.UOM_DESC_TRANS, use_this.ORIG_LANG_IND, 'N', sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
