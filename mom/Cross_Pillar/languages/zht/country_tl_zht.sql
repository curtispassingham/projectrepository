SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AF' COUNTRY_ID, '阿富汗' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AL' COUNTRY_ID, '阿爾巴尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DZ' COUNTRY_ID, '阿爾及利亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AS' COUNTRY_ID, '美屬薩摩亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AD' COUNTRY_ID, '安道爾' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AO' COUNTRY_ID, '安哥拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AI' COUNTRY_ID, '安圭拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AQ' COUNTRY_ID, '南極大陸' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AG' COUNTRY_ID, '安地卡及巴布達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AR' COUNTRY_ID, '阿根廷' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AM' COUNTRY_ID, '亞美尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AW' COUNTRY_ID, '阿路巴' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AU' COUNTRY_ID, '澳大利亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AT' COUNTRY_ID, '奧地利' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BZ' COUNTRY_ID, '貝里南' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BJ' COUNTRY_ID, '貝南' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BM' COUNTRY_ID, '百慕達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BT' COUNTRY_ID, '不丹' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BO' COUNTRY_ID, '玻利維亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BQ' COUNTRY_ID, '博奈爾島、聖尤斯特歇斯島和薩巴島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BA' COUNTRY_ID, '波士尼亞與赫塞哥維納' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BW' COUNTRY_ID, '波札那' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BV' COUNTRY_ID, '布威島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BR' COUNTRY_ID, '巴西' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IO' COUNTRY_ID, '英屬印度洋領地' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BN' COUNTRY_ID, '汶萊' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BG' COUNTRY_ID, '保加利亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BF' COUNTRY_ID, '布吉納法索' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BI' COUNTRY_ID, '浦隆地' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KH' COUNTRY_ID, '柬埔寨' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CM' COUNTRY_ID, '喀麥隆' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CA' COUNTRY_ID, '加拿大' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CV' COUNTRY_ID, '維德角' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KY' COUNTRY_ID, '開曼群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CF' COUNTRY_ID, '中非共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TD' COUNTRY_ID, '查德' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CL' COUNTRY_ID, '智利' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CN' COUNTRY_ID, '中國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CX' COUNTRY_ID, '聖誕島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CC' COUNTRY_ID, '可可斯群島   ' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CO' COUNTRY_ID, '哥倫比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KM' COUNTRY_ID, '葛摩' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CG' COUNTRY_ID, '剛果' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CD' COUNTRY_ID, '剛果民主共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CK' COUNTRY_ID, '柯克群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CR' COUNTRY_ID, '哥斯大黎加' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CI' COUNTRY_ID, '象牙海岸' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HR' COUNTRY_ID, '克羅埃西亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CU' COUNTRY_ID, '古巴' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CW' COUNTRY_ID, '古拉索' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CY' COUNTRY_ID, '塞普勒斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CZ' COUNTRY_ID, '捷克共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DK' COUNTRY_ID, '丹麥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DJ' COUNTRY_ID, '吉布地' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DM' COUNTRY_ID, '多米尼克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DO' COUNTRY_ID, '多明尼加' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'EC' COUNTRY_ID, '厄瓜多' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'EG' COUNTRY_ID, '埃及' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SV' COUNTRY_ID, '薩爾瓦多' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GQ' COUNTRY_ID, '赤道幾內亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ER' COUNTRY_ID, '厄利垂亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'EE' COUNTRY_ID, '愛沙尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ET' COUNTRY_ID, '衣索比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FK' COUNTRY_ID, '福克蘭群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FO' COUNTRY_ID, '法羅群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FJ' COUNTRY_ID, '斐濟' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FI' COUNTRY_ID, '芬蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FR' COUNTRY_ID, '法國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GF' COUNTRY_ID, '法屬圭亞那' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PF' COUNTRY_ID, '法屬波里尼西亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TF' COUNTRY_ID, '法國南方領地' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GA' COUNTRY_ID, '加彭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GM' COUNTRY_ID, '甘比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GE' COUNTRY_ID, '喬治亞共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'DE' COUNTRY_ID, '德國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GH' COUNTRY_ID, '迦納' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GI' COUNTRY_ID, '直布羅陀' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GR' COUNTRY_ID, '希臘' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GL' COUNTRY_ID, '格陵蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GD' COUNTRY_ID, '格瑞那達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GP' COUNTRY_ID, '哥德普洛' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GU' COUNTRY_ID, '關島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GT' COUNTRY_ID, '瓜地馬拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GG' COUNTRY_ID, '根西島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GN' COUNTRY_ID, '幾內亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GW' COUNTRY_ID, '幾內亞比索' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GY' COUNTRY_ID, '蓋亞納' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HT' COUNTRY_ID, '海地' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HM' COUNTRY_ID, '赫德島及麥當勞群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HN' COUNTRY_ID, '宏都拉斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HK' COUNTRY_ID, '香港' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'HU' COUNTRY_ID, '匈牙利' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IS' COUNTRY_ID, '冰島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IN' COUNTRY_ID, '印度' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ID' COUNTRY_ID, '印尼' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IR' COUNTRY_ID, '伊朗伊斯蘭共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IQ' COUNTRY_ID, '伊拉克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IE' COUNTRY_ID, '愛爾蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IM' COUNTRY_ID, '曼島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IL' COUNTRY_ID, '以色列' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'IT' COUNTRY_ID, '義大利' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'JM' COUNTRY_ID, '牙買加' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'JP' COUNTRY_ID, '日本' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'JE' COUNTRY_ID, '澤西島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'JO' COUNTRY_ID, '約旦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KZ' COUNTRY_ID, '哈薩克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KE' COUNTRY_ID, '肯亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KI' COUNTRY_ID, '吉里巴斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KP' COUNTRY_ID, '朝鮮民主主義人民共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KR' COUNTRY_ID, '大韓民國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KW' COUNTRY_ID, '科威特' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KG' COUNTRY_ID, '吉爾吉斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LA' COUNTRY_ID, '寮國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LV' COUNTRY_ID, '拉脫維亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LB' COUNTRY_ID, '黎巴嫩' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LS' COUNTRY_ID, '賴索扥王國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LR' COUNTRY_ID, '賴比瑞亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LY' COUNTRY_ID, '利比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LI' COUNTRY_ID, '列支敦斯登' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LT' COUNTRY_ID, '立陶宛' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LU' COUNTRY_ID, '盧森堡' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MO' COUNTRY_ID, '澳門' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MK' COUNTRY_ID, '馬其頓' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MG' COUNTRY_ID, '馬達加斯加' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MW' COUNTRY_ID, '馬拉威' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MY' COUNTRY_ID, '馬來西亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MV' COUNTRY_ID, '馬爾地夫' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ML' COUNTRY_ID, '馬利' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MT' COUNTRY_ID, '馬爾他' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MH' COUNTRY_ID, '馬紹爾群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MQ' COUNTRY_ID, '馬丁尼克島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AZ' COUNTRY_ID, '亞賽拜然' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BS' COUNTRY_ID, '巴哈馬' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BH' COUNTRY_ID, '巴林' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BD' COUNTRY_ID, '孟加拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BB' COUNTRY_ID, '巴貝多' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BY' COUNTRY_ID, '白俄羅斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BE' COUNTRY_ID, '比利時' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MC' COUNTRY_ID, '摩納哥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MN' COUNTRY_ID, '蒙古' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ME' COUNTRY_ID, '蒙特內哥羅' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MS' COUNTRY_ID, '蒙特色拉特島 ' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MA' COUNTRY_ID, '摩洛哥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MZ' COUNTRY_ID, '莫三比克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MM' COUNTRY_ID, '緬甸' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NA' COUNTRY_ID, '納米比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NR' COUNTRY_ID, '諾魯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NP' COUNTRY_ID, '尼泊爾' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NL' COUNTRY_ID, '荷蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NC' COUNTRY_ID, '新喀里多尼亞群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NZ' COUNTRY_ID, '紐西蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NI' COUNTRY_ID, '尼加拉瓜' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NE' COUNTRY_ID, '尼日' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NG' COUNTRY_ID, '奈及利亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NU' COUNTRY_ID, '紐威島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NF' COUNTRY_ID, '諾福克島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MP' COUNTRY_ID, '北馬里亞納群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NO' COUNTRY_ID, '挪威' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'OM' COUNTRY_ID, '阿曼' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PK' COUNTRY_ID, '巴基斯坦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PW' COUNTRY_ID, '帛琉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PS' COUNTRY_ID, '巴勒斯坦國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PA' COUNTRY_ID, '巴拿馬' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PG' COUNTRY_ID, '巴布亞紐幾內亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PY' COUNTRY_ID, '巴拉圭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PE' COUNTRY_ID, '祕魯共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PH' COUNTRY_ID, '菲律賓' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PN' COUNTRY_ID, '皮特康' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PL' COUNTRY_ID, '波蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PT' COUNTRY_ID, '葡萄牙' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PR' COUNTRY_ID, '波多黎各' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'QA' COUNTRY_ID, '卡達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'RE' COUNTRY_ID, '留尼旺' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'RO' COUNTRY_ID, '羅馬尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'RU' COUNTRY_ID, '俄羅斯聯邦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'RW' COUNTRY_ID, '盧安達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'BL' COUNTRY_ID, '聖巴泰勒米島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SH' COUNTRY_ID, '聖赫勒拿、亞森欣與垂斯坦昆哈' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'KN' COUNTRY_ID, '聖克里斯多福及尼維斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LC' COUNTRY_ID, '聖露西亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MF' COUNTRY_ID, '法屬聖馬丁' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'PM' COUNTRY_ID, '聖彼爾及米可龍群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VC' COUNTRY_ID, '聖文森及格瑞那丁' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'WS' COUNTRY_ID, '薩摩亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SM' COUNTRY_ID, '聖馬利諾' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ST' COUNTRY_ID, '聖多美及普林西比' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SA' COUNTRY_ID, '沙烏地阿拉伯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SN' COUNTRY_ID, '塞內加爾' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'RS' COUNTRY_ID, '塞爾維亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SC' COUNTRY_ID, '塞席爾' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SL' COUNTRY_ID, '獅子山' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SG' COUNTRY_ID, '新加坡' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SX' COUNTRY_ID, '荷屬聖馬丁' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SK' COUNTRY_ID, '捷克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SI' COUNTRY_ID, '斯拉維尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SB' COUNTRY_ID, '索羅門群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SO' COUNTRY_ID, '索馬利亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ZA' COUNTRY_ID, '南非' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GS' COUNTRY_ID, '南喬治亞及南三明治群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SS' COUNTRY_ID, '南蘇丹' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ES' COUNTRY_ID, '西班牙' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'LK' COUNTRY_ID, '斯里蘭卡' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SD' COUNTRY_ID, '蘇丹' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SR' COUNTRY_ID, '蘇利南' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SJ' COUNTRY_ID, '冷岸及央棉群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SZ' COUNTRY_ID, '史瓦濟蘭王國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SE' COUNTRY_ID, '瑞典' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'CH' COUNTRY_ID, '瑞士' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'SY' COUNTRY_ID, '敘利亞阿拉伯共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TW' COUNTRY_ID, '台灣' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TJ' COUNTRY_ID, '塔吉克' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TZ' COUNTRY_ID, '坦尚尼亞共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TH' COUNTRY_ID, '泰國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TL' COUNTRY_ID, '東帝汶民主共和國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TG' COUNTRY_ID, '多哥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TK' COUNTRY_ID, '扥克勞群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TO' COUNTRY_ID, '東加' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TT' COUNTRY_ID, '千里達托貝哥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TN' COUNTRY_ID, '突尼西亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TR' COUNTRY_ID, '土耳其' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TM' COUNTRY_ID, '土庫曼' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TC' COUNTRY_ID, '土克斯及開科斯群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'TV' COUNTRY_ID, '吐瓦魯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'UG' COUNTRY_ID, '烏干達' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'UA' COUNTRY_ID, '烏克蘭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AE' COUNTRY_ID, '阿拉伯聯合大公國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'GB' COUNTRY_ID, '英國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'US' COUNTRY_ID, '美國' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'UM' COUNTRY_ID, '美國外島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'UY' COUNTRY_ID, '烏拉圭' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'UZ' COUNTRY_ID, '烏茲別克斯坦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VU' COUNTRY_ID, '萬那杜' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VA' COUNTRY_ID, '梵蒂岡' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VE' COUNTRY_ID, '委內瑞拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VN' COUNTRY_ID, '越南' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VG' COUNTRY_ID, '維京群島 (英屬)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'VI' COUNTRY_ID, '維京群島 (美屬)' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'WF' COUNTRY_ID, '瓦利斯及福杜納群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'EH' COUNTRY_ID, '西撒哈拉' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'YE' COUNTRY_ID, '葉門' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ZM' COUNTRY_ID, '尚比亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'ZW' COUNTRY_ID, '辛巴威' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'AX' COUNTRY_ID, '奧蘭群島' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'NEW' COUNTRY_ID, '新國家' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, '99' COUNTRY_ID, '多個' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MR' COUNTRY_ID, '茅利塔尼亞' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MU' COUNTRY_ID, '模里西斯' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'YT' COUNTRY_ID, '馬約特' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MX' COUNTRY_ID, '墨西哥' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'FM' COUNTRY_ID, '密克羅尼西亞聯邦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO COUNTRY_TL TL USING
(SELECT  LANG,	COUNTRY_ID,  COUNTRY_DESC
FROM  (SELECT 11 LANG, 'MD' COUNTRY_ID, '摩爾多瓦' COUNTRY_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM COUNTRY base where dl.COUNTRY_ID = base.COUNTRY_ID)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE  SET tl.COUNTRY_DESC = use_this.COUNTRY_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, COUNTRY_ID, COUNTRY_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.COUNTRY_ID, use_this.COUNTRY_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO COUNTRY base USING
( SELECT COUNTRY_ID COUNTRY_ID, COUNTRY_DESC COUNTRY_DESC FROM COUNTRY_TL TL where lang = 11
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 11)) USE_THIS
ON ( base.COUNTRY_ID = use_this.COUNTRY_ID)
WHEN MATCHED THEN UPDATE SET base.COUNTRY_DESC = use_this.COUNTRY_DESC;
--
DELETE FROM COUNTRY_TL where lang = 11
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 11);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
