SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'CANNOT_CHANGE_REPL_WH' REASON_KEY, '無法變更此倉庫的狀態，因為此倉庫正用來作為此庫存單位補貨的貨源倉庫。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'CONSIGNMENT' REASON_KEY, '在寄售部門 %s1 與非寄售部門間，無法重新分類料號。原部門：%s1 新部門：%s2' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'COST_LOC_PENDING' REASON_KEY, '無法處理料號 %s1 與位置 %s2 的成本計算位置變更，因為有相同項目的待處理成本計算位置變更。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'FORECAST_REPL_METHOD' REASON_KEY, '此料號的位置是使用預測補貨方式，因此料號無法變更為非可預測。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_ACTIVATE_DATE' REASON_KEY, '新啟用日期 %s1 晚於目前停用日期 %s2。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_DEACTIVATE_DATE' REASON_KEY, '新停用日期 %s1 早於目前啟用日期 %s2。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_MAX_STOCK' REASON_KEY, '%s1 件數的新最大庫存小於 %s2 件數的目前最小庫存。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_MAX_SUPPLY_DAYS' REASON_KEY, '%s1 的新最大定時供應天數小於 %s2 的目前最小定時供應天數。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_MIN_STOCK' REASON_KEY, '%s1 件數的新最小庫存大於 %s2 件數的目前最大庫存。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_MIN_SUPPLY_DAYS' REASON_KEY, '%s1 的新最小定時供應天數大於 %s2 的目前最大定時供應天數。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_MULT_RUNS_PER_DAY' REASON_KEY, '只有補貨方式為「商店訂貨」、複查週期為每日，且庫存分類為「倉庫已存貨」或「倉庫連結」時，「一日多趟」指示才能設為 "Y"。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_REPL_ITEM_TYPE' REASON_KEY, '只能更新「基本料號庫存單位」、「流行庫存單位」及「流行」樣式的補貨屬性。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_REPL_ORDER_CTRL' REASON_KEY, '補貨位置為商店並且目前訂單控制為「採購員工作單」時，庫存分類就無法更新為 %s1。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_SOURCING_WH' REASON_KEY, '新的貨源倉庫沒有料號。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_STORE_CLOSE_DATE' REASON_KEY, '新關店日期 %s1 早於目前開店日期 %s2。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_STORE_OPEN_DATE' REASON_KEY, '新開店日期 %s1 晚於目前關店日期 %s2。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'INVALID_TSF_ZERO_SOH_IND' REASON_KEY, '只有補貨方式為「商店訂貨」，且庫存分類為「倉庫已存貨」時，「移轉零庫存量」指示才能設為 "Y"。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_IN_GROUP_ON_ORDER' REASON_KEY, '此料號群組的料號存在於作用中訂單上，因此無法變更此群組中任何料號的部門/類別/子類別' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_IN_USE' REASON_KEY, '此料號是作用中訂單、移轉或價格變動的一部分，因此無法變更狀態。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_LOC_NOT_ACTIVATE' REASON_KEY, '料號 %s1 在位置 %s2 上未生效。因此無法啟用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_NOT_FORECASTABLE' REASON_KEY, '料號不可預測，因此無法使用「即時供應」與「動態」補貨方式。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_NOT_RANGE_COST_LOC' REASON_KEY, '成本計算位置 %s1 未配置給料號 %s2，只有配置給料號的商店或倉庫才可用作成本計算位置。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_NOT_TOLERANCEABLE' REASON_KEY, '如果庫存分類為倉庫就無法更新允差。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_ON_APP_ORD' REASON_KEY, '要重新分類的指定料號或其下階/最下層料號在已核准的訂單上。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_ON_ORDER_ZONE' REASON_KEY, '無法變更成本區域，因為料號存在於一或多個訂單中。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_STATUS_DELETE' REASON_KEY, '%s1 %s2 的目前料號狀態為「已刪除」，因此無法變更狀態。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_SUPPLIER' REASON_KEY, '供應商 %s1 未與料號相關聯。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ITEM_SUPPLIER_COUNTRY' REASON_KEY, '供應商 %s1/原產國 %s2 未與料號相關聯。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'LAST_PHASE' REASON_KEY, '無法刪除。此料號只指定了一個季節。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'MAIN_EXISTS_AS_SUB' REASON_KEY, '主料號 %s1 無法指定至位置 %s2，因為主料號在該「位置」已作為「替代料號」存在。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'MAIN_ITEM_FORECASTS' REASON_KEY, '料號在替代料號與處理時為主料號，並且正在使用料號預測。無法切換為非可預測。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'MAIN_ITEM_LOC_MISMATCH' REASON_KEY, '位置 %s2 上沒有主料號 %s1。在「替代料號」對話框中只能使用現有「料號-位置」組合。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NOT_LIST_SKU_STYLE_S' REASON_KEY, '只能更新「基本料號庫存單位」、「流行庫存單位」或「流行」樣式的屬性。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NOT_LIST_SKU_STYLE_W' REASON_KEY, '只能更新「基本料號庫存單位」、「流行庫存單位」或「流行」樣式的屬性。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NOT_ON_REPL_NO_DEACTIVATE' REASON_KEY, '料號位置不在補貨中，因此無法停用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NOT_ON_REPL_NO_UPDATE' REASON_KEY, '料號位置不在補貨中，因此無法更新屬性。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NOT_SKU_STYLE' REASON_KEY, '料號指示屬性只存在於「基本料號庫存單位」與「流行」樣式層次。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_ASSOC_UDA_VALUE' REASON_KEY, '料號的新階層 %s1/%s2/%s3 具有預設「使用者定義屬性」，但沒有相關聯的預設「使用者定義屬性值」。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_BUYER_WKSHT' REASON_KEY, '目前庫存分類為 %s1 並且補貨位置為商店時，訂單控制就無法更新為「採購員工作單」。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_DOMAIN_EXISTS' REASON_KEY, '料號的商品階層未與區域相關聯，因此料號無法設為可預測。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_REQUIRED_UDA' REASON_KEY, '料號 %s1 必須與使用者定義屬性 %s2 相關聯。移動此記錄將違反這項條件。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_UPDATE_CHILD_COST_ZONE' REASON_KEY, '無法更新料號成本區域群組，因為料號為下階料號，並且上階料號必須具有相同的成本區域。如果上階料號為更新的一部分，料號仍可能已更新。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_UPD_EXIST_VAT' REASON_KEY, '在此加值稅區域與有效日期，庫存單位 %s1 已有加值稅類型為 "C" 或 "R" 的加值稅庫存單位記錄。其無法更新為 "B"。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'NO_UPD_VAT_TYPE' REASON_KEY, '在此加值稅區域與有效日期，庫存單位 %s1 具有加值稅類型為 "B" 的加值稅庫存單位記錄。其無法更新為 "C" 或 "R"。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ON_CLEAR_NO_ACTIVATE' REASON_KEY, '料號位置在清倉，因此無法啟用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ON_CONSIGN_NO_ACTIVATE' REASON_KEY, '料號位置在寄售，因此無法啟用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'ON_REPL_NO_ACTIVATE' REASON_KEY, '料號位置已在補貨，因此無法啟用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'PACK_FORECAST' REASON_KEY, '料號為無法預測的包裝料號。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'PACK_ITEM_LOC_MISMATCH' REASON_KEY, '位置 %s2 上沒有主要補貨包裝 %s1。在「替代料號」對話框中只能使用現有「料號-位置」組合。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'PACK_LOC_NO_EXIST' REASON_KEY, '主要補貨包裝 %s1 在位置 %s2 上不存在作用中狀態。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'PACK_STATUS' REASON_KEY, '主要補貨包裝 %s1 無效，因為狀態與其元件料號 %s2 不相同。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'RECLASS_EXIST' REASON_KEY, '料號存在於另一個暫緩重新分類中。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'RECLASS_NO_DOMAIN' REASON_KEY, '此料號可預測。部門 %s1、類別 %s2 以及子類別 %s3 的新商品階層未與區域相關聯，因此料號無法重新分類。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'REJECT_DEPT_SIZE' REASON_KEY, '新部門 %s1 中至少缺少一個必要的尺寸，因此無法重新分類樣式。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'REJECT_ITEM_LEVEL' REASON_KEY, '只有等級一料號才能重新分類。料號 %s1 是等級 %s2 料號。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'REJECT_ITEM_STATUS' REASON_KEY, '料號在作用中訂單上，因此無法變更部門。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SIMPLE_PACK' REASON_KEY, '無法重新分類簡單包裝 %s1。只能隨其元件料號一起重新分類。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SINGLE_UDA_EXISTS' REASON_KEY, '料號 %s1 已與使用者定義屬性 %s2 產生關聯，並且此使用者定義屬性只允許每個料號有一個關聯。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SKU_IN_ACTIVE_PACK' REASON_KEY, '此料號為生效包裝的一部分。無法變更其狀態。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SKU_ORD_EXIST' REASON_KEY, '料號有作用中訂單。無法變更其狀態' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SKU_TSF_EXIST' REASON_KEY, '料號有作用中移轉。無法變更其狀態' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SOURCE_WH_NO_EXIST' REASON_KEY, '料號並未與倉庫 %s1 關聯，或在該倉庫中未生效，因此該倉庫無法用作貨源倉庫。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'STOCK_COUNT_PEND' REASON_KEY, '此料號正在進行庫存盤點。此時無法加以重新分類。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'STYLE_SKU_ON_ORDER' REASON_KEY, '無法變更成本區域，因為料號是樣式，並且至少有一個庫存單位存在於一或多個訂單中。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUB_EXISTS_AS_MAIN' REASON_KEY, '替代料號 %s1 無法指定至位置 %s2，因為替代料號在該「位置」已作為「主料號」存在。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUB_ITEM_FORECASTS' REASON_KEY, '料號為替代料號並且正在使用其預測。無法切換為非可預測。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUB_ITEM_LOC_MISMATCH' REASON_KEY, '位置 %s2 上沒有替代料號 %s1。在「替代料號」對話框中只能使用現有「料號-位置」組合。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUB_ITEM_NO_ACTIVATE' REASON_KEY, '料號為替代料號，因此無法啟用。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUPP_NO_EXIST_PACK' REASON_KEY, '包裝 %s1 並未與供應商 %s2 以及原產國 %s3 關聯。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'SUP_LOC_NOT_SAME_ORG_UNIT' REASON_KEY, '無法建立供應商 %s1 與位置 %s2 的關聯，因為它們屬於不同的「組織單位」。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'UDA_REQD' REASON_KEY, '料號的新階層具有尚未指定給料號的必要使用者定義屬性。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO MC_REJECTION_REASONS_TL TL USING
(SELECT  LANG,	REASON_KEY,  REJECTION_REASON
FROM  (SELECT 11 LANG, 'XDOCK_WH_ITEM_NOT_ACTIVE' REASON_KEY, '庫存單位 %s1 在指定的越庫倉庫 %s2 中未生效。' REJECTION_REASON FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM MC_REJECTION_REASONS base where dl.REASON_KEY = base.REASON_KEY)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE  SET tl.REJECTION_REASON = use_this.REJECTION_REASON, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, REASON_KEY, REJECTION_REASON, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.REASON_KEY, use_this.REJECTION_REASON, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
-- If the primary language is same as this script language, the base table should hold the language string and not the _TL table.
-- Copy data to base table if primary language is same as this script language and then delete this language record from _TL table.
--
MERGE INTO MC_REJECTION_REASONS base USING
( SELECT REASON_KEY REASON_KEY, REJECTION_REASON REJECTION_REASON FROM MC_REJECTION_REASONS_TL TL where lang = 11
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 11)) USE_THIS
ON ( base.REASON_KEY = use_this.REASON_KEY)
WHEN MATCHED THEN UPDATE SET base.REJECTION_REASON = use_this.REJECTION_REASON;
--
DELETE FROM MC_REJECTION_REASONS_TL where lang = 11
AND EXISTS (SELECT 1 FROM SYSTEM_CONFIG_OPTIONS SCO WHERE SCO.DATA_INTEGRATION_LANG = 11);
--
---------------------------------------------------------------------------------------------------------------------------------------------
commit;
