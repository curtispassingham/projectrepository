The partitioning configuration files contained in this directory were used in the mock installation of RMS 14. 
They are not to be used to build out any type of productionor volume databse.
In order to ensure that partitioning is set up correctly in your RMS environment, use the partitioning configuration section of the install guide.
