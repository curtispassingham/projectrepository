#!/bin/ksh
#########################################################################################
#
# File:  partition.ksh
# Desc:  This script reads information from the partition_attributes file and references
#        any data definition files in the data_def directory to create a partitioned DDL
#        file from a non-partitioned DDL file. 
#
#########################################################################################

echo "############################################################################"
echo "# partition.ksh:"
echo "# This script will read the partition_attributes.cfg file and any referenced"
echo "# data definition files and generate partitioned DDL."
echo "############################################################################"

ATTRIBFILE=partition_attributes.cfg
if [ ! -f "$ATTRIBFILE" ];
then
   echo "The parameter file $ATTRIBFILE does not exist.  Exiting."
   return 1
fi

PREPARTDDL=`grep ^PREPARTDDL $ATTRIBFILE|awk -F= '{print $2}'`
if [ ! -f "$PREPARTDDL" ];
then
   echo "The non-partitioned DDL file $PREPARTDDL does not exist.  Exiting."
   return 1
fi

PARTDDL=`grep ^PARTDDL $ATTRIBFILE|awk -F= '{print $2}'`
if [ -f "$PARTDDL" ];
then
   rm $PARTDDL
fi
cp $PREPARTDDL $PARTDDL

echo "# The non-partitioned DDL file is $PREPARTDDL."
echo "# The partitioned DDL file that will be generated is $PARTDDL."
echo "############################################################################"

echo "Checking $ATTRIBFILE for errors"

ERROR=N
cat $ATTRIBFILE|grep -v ^#|grep -v ^PREPARTDDL|grep -v ^PARTDDL|grep -v '^[ 	]*$'|while read line;
do

   # Find the line number for error reporting and save it in $i
   ((i=`< $ATTRIBFILE sed "/^${line}$/bline
   d
   :line
   =
   d"`))

   unset TABLENAME
   unset PARTKEY
   unset PARTMETHOD
   unset PARTDEFFILE
   unset PARTCOUNT
   unset PARTINTERVAL
   unset SUBPARTKEY
   unset SUBPARTMETHOD
   unset SUBPARTDEFFILE
   unset SUBPARTCOUNT

   TABLENAME=`echo $line|awk -F, '{print $1}'`
   PARTKEY=`echo $line|awk -F, '{print $2}'`
   PARTMETHOD=`echo $line|awk -F, '{print $3}'`
   PARTDEFFILE=`echo $line|awk -F, '{print $4}'`
   PARTCOUNT=`echo $line|awk -F, '{print $5}'`
   PARTINTERVAL=`echo $line|awk -F, '{print $6}'`
   SUBPARTKEY=`echo $line|awk -F, '{print $7}'`
   SUBPARTMETHOD=`echo $line|awk -F, '{print $8}'`
   SUBPARTDEFFILE=`echo $line|awk -F, '{print $9}'`
   SUBPARTCOUNT=`echo $line|awk -F, '{print $10}'`

   if [ -z "$TABLENAME" -o -z "$PARTKEY" -o -z "$PARTMETHOD" ];
   then
      echo "*** ERROR *** $ATTRIBFILE line ${i} fields 1-3:  Required fields."
      ERROR=Y
      continue
   fi

   if [ "$PARTMETHOD" != "HASH" -a "$PARTMETHOD" != "RANGE" -a "$PARTMETHOD" != "LIST" -a "$PARTMETHOD" != "INTERVAL" ];
   then
      echo "*** ERROR *** $ATTRIBFILE line ${i} field 3:  Value must be RANGE, INTERVAL, HASH, or LIST."
      ERROR=Y
      continue
   fi

   ((CREATELINE=`< $PREPARTDDL sed "/CREATE TABLE ${TABLENAME}$/bline
   d
   :line
   =
   d"`))

   ((SLASHLINE=`< $PREPARTDDL sed -n $CREATELINE,'$bline
   d
   :line
   /\//bline2
   d
   :line2
   =
   q'`))

   ((PARTKEYLINE=`< $PREPARTDDL sed -n ${CREATELINE},${SLASHLINE}"bline
   d
   :line
   /[,]*${PARTKEY} /bline2
   d
   :line2
   =
   q"|wc -l`))

   if [ "$PARTKEYLINE" -eq 0 ];
   then
      echo "*** ERROR *** $ATTRIBFILE line ${i} field 2:  Partition key column $PARTKEY is not found in the $PREPARTDDL file for table $TABLENAME."
      ERROR=Y
   fi

   if [ "$PARTMETHOD" = "HASH" -o "$PARTMETHOD" = "INTERVAL" ];
   then
      if [ "$PARTMETHOD" = "HASH" ];
      then
         if [ -z "$PARTCOUNT" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Required field for HASH partitioned tables.  This field defines the number of HASH partitions to create."
            ERROR=Y
         else
            # Check for characters in PARTCOUNT field
            if [ ! -z "`echo $PARTCOUNT|sed -e s/[0-9]*//`" ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Value must be a number greater than zero.  This field defines the number of HASH partitions to create."
               ERROR=Y
            else
               # Verify PARTCOUNT is greater than zero
               ((PARTCOUNT=$PARTCOUNT)) # Format as a number
               if [ $PARTCOUNT -le 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Value must be a number greater than zero.  This field defines the number of HASH partitions to create."
                  ERROR=Y
               fi
            fi
         fi
      else
         if [ -z "$PARTCOUNT" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Required field For INTERVAL partitioned tables.  This field defines the interval value between partitions."
            ERROR=Y         
         else
            # Check for characters in PARTCOUNT field
            if [ ! -z "`echo $PARTCOUNT|sed -e s/[0-9]*//`" ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Value must be a number greater than zero.  This field defines the interval value between partitions."
               ERROR=Y
            else
               # Verify PARTCOUNT is greater than zero
               ((PARTCOUNT=$PARTCOUNT)) # Format as a number
               if [ $PARTCOUNT -le 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Value must be a number greater than zero.  This field defines the interval value between partitions."
                  ERROR=Y
               fi
            fi
         fi
         if [ -z "$PARTINTERVAL" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 6:  Required field For INTERVAL partitioned tables.  This field defines the interval unit."
            ERROR=Y
         elif [ "$PARTINTERVAL" != "DAY" -a "$PARTINTERVAL" != "MONTH" ];
         then   
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 6:  Value must be DAY or MONTH."
            ERROR=Y
         fi
      fi
   fi

   if [ "$PARTMETHOD" = "RANGE" -o "$PARTMETHOD" = "LIST" -o "$PARTMETHOD" = "INTERVAL" ];
   then
      if [ -z "$PARTDEFFILE" ];
      then
         echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Required field for RANGE, INTERVAL, and LIST partitioned tables.  This field defines the data definition file to use for this partition."
         ERROR=Y
      else
         PARTDEFFILE=data_def/$PARTDEFFILE
         if [ ! -f "$PARTDEFFILE" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Data Definition file $PARTDEFFILE does not exist."
            ERROR=Y
         elif [ `< $PARTDEFFILE grep -v ^#|wc -l` -eq 0 ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Data Definition file $PARTDEFFILE is empty."
            ERROR=Y
         elif [ `< $PARTDEFFILE grep '^[ 	]*$'|wc -l` -gt 0 ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Data Definition file $PARTDEFFILE contains blank lines."
            ERROR=Y
         else
            PARTKEYTYPE=`echo $PARTDEFFILE|awk -F. '{print $3}'`
            if [ "$PARTKEYTYPE" != "date" -a "$PARTKEYTYPE" != "varchar2" -a "$PARTKEYTYPE" != "number" ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Data Definition file $PARTDEFFILE must have a date, varchar2, or number extension."
               ERROR=Y
            fi
            if [ "$PARTKEYTYPE" = "date" ];
            then
               if [ `< $PARTDEFFILE grep -v ^#|sed /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/d|wc -l` -gt 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Data Definition file $PARTDEFFILE has invalid date formats.  The lines in date data definition files must be of the format DDMMYYYY."
                  ERROR=Y
               else
                  < $PARTDEFFILE grep -v ^#|awk '{printf "%s %s %s\n", substr($1,1,2), substr($1,3,2), substr($1,5)}'|while read DAY MONTH YEAR;
                  do
                     ((DD=$DAY))
                     ((MM=$MONTH))
                     ((YYYY=$YEAR))
                     ((YEARMOD=$YYYY%4))
                     if [ "$MM" -lt 1 -o "$MM" -gt 12 ] ||
                        [ "$DD" -lt 1 ] ||
                        [ "$MM" -eq 2 -a $YEARMOD -ne 0 -a "$DD" -gt 28 ] ||
                        [ "$MM" -eq 2 -a $YEARMOD -eq 0 -a "$DD" -gt 29 ] ||
                        [ \( "$MM" -eq 4 -o "$MM" -eq 6 -o "$MM" -eq 9 -o "$MM" -eq 11 \) -a "$DD" -gt 30 ] ||
                        [ \( "$MM" -eq 1 -o "$MM" -eq 3 -o "$MM" -eq 5 -o "$MM" -eq 7 -o "$MM" -eq 8 -o "$MM" -eq 10 -o "$MM" -eq 12 \) -a "$DD" -gt 31 ];
                     then
                        echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Data Definition file $PARTDEFFILE contains invalid date:  $DAY$MONTH$YEAR."
                        ERROR=Y
                     fi
                  done
               fi
            fi
            if [ "$PARTKEYTYPE" = "number" ];
            then
               if [ `< $PARTDEFFILE grep -v ^#|sed /^[0-9]*$/d|wc -l` -gt 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Data Definition file $PARTDEFFILE has invalid numbers."
                  ERROR=Y
               fi
            fi
            # Verify data def files have no duplicate values
            if [ `< $PARTDEFFILE grep -v ^#|wc -l` -ne `< $PARTDEFFILE grep -v ^#|sort -u|wc -l` ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Data Definition file $PARTDEFFILE has duplicate values."
               ERROR=Y
            fi
            if [ "$PARTMETHOD" = "RANGE" -o "$PARTMETHOD" = "INTERVAL" ];
            then
               if [ -f temp1 ];
               then
                  rm -f temp1
               fi
               if [ -f temp2 ];
               then
                  rm -f temp2
               fi
               if [ "$PARTKEYTYPE" = "date" ];
               then
                  # Date fields must be formatted in YYYYMMDD format to do a numerical comparison
                  < $PARTDEFFILE grep -v ^#|awk '{printf "%s%s%s\n", substr($1,5), substr($1,3,2), substr($1,1,2)}' >> temp1
                  < $PARTDEFFILE grep -v ^#|awk '{printf "%s%s%s\n", substr($1,5), substr($1,3,2), substr($1,1,2)}'|sort -n >> temp2
               elif [ "$PARTKEYTYPE" = "number" ]; # do numerical sort
               then
                  < $PARTDEFFILE grep -v ^# >> temp1
                  < $PARTDEFFILE grep -v ^#|sort -n >> temp2
               else # $PARTKEYTYPE = "varchar2" # do regular sort
                  < $PARTDEFFILE grep -v ^# >> temp1
                  < $PARTDEFFILE grep -v ^#|sort >> temp2
               fi
               if [ `diff temp1 temp2|wc -l` -gt 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 4:  Data Definition file $PARTDEFFILE must define partitions in ascending order when partitioning by RANGE."
                  ERROR=Y
               fi
               rm -f temp1 temp2
            fi
         fi
      fi
   fi

   if [ ! -z "$SUBPARTKEY" ];
   then
      #CREATELINE and SLASHLINE are determined above
      ((SUBPARTKEYLINE=`< $PREPARTDDL sed -n ${CREATELINE},${SLASHLINE}"bline
      d
      :line
      /[,]*${SUBPARTKEY} /bline2
      d
      :line2
      =
      q"|wc -l`))

      if [ "$SUBPARTKEYLINE" -eq 0 ];
      then
         echo "*** ERROR *** $ATTRIBFILE line ${i} field 2:  Sub-Partition key column $SUBPARTKEY is not found in the $PREPARTDDL file for table $TABLENAME."
         ERROR=Y
      fi

      if [ "$PARTMETHOD" != 'RANGE' -a "$PARTMETHOD" != 'INTERVAL' ];
      then
         echo "*** ERROR *** $ATTRIBFILE line ${i} field 3:  Composite partitioned tables can only be partitioned by RANGE or INTERVAL"
         ERROR=Y
      fi
      if [ "$SUBPARTMETHOD" != "HASH" -a "$SUBPARTMETHOD" != "LIST" ];
      then
         echo "*** ERROR *** $ATTRIBFILE line ${i} field 7:  Value must be HASH or LIST."
         ERROR=Y
      fi

      if [ "$SUBPARTMETHOD" = 'HASH' ];
      then
         if [ -z "$SUBPARTCOUNT" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 9:  Required field For HASH partitioned tables.  This field defines the number of HASH partitions to create."
            ERROR=Y
         else
            # Check for characters in SUBPARTCOUNT field
            if [ ! -z "`echo $SUBPARTCOUNT|sed -e s/[0-9]*//`" ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 9:  This value must be a number greater than zero."
               ERROR=Y
            else
               ((SUBPARTCOUNT=$SUBPARTCOUNT)) # format as integer
               if [ $SUBPARTCOUNT -le 0 ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 9:  This value must be a number greater than zero."
                  ERROR=Y
               fi
            fi
         fi

      else # $SUBPARTMETHOD = "LIST"
         if [ -z "$SUBPARTDEFFILE" ];
         then
            echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Required field for LIST sub-partitioned tables.  This field defines the data definition file to use for this partition."
            ERROR=Y
         else
            SUBPARTDEFFILE=data_def/$SUBPARTDEFFILE
            if [ ! -f "$SUBPARTDEFFILE" ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE does not exist."
               ERROR=Y
            elif [ `< $SUBPARTDEFFILE grep -v ^#|wc -l` -eq 0 ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE is empty."
               ERROR=Y
            elif [ `< $SUBPARTDEFFILE grep '^[      ]*$'|wc -l` -gt 0 ];
            then
               echo "*** ERROR *** $ATTRIBFILE line ${i} field 5:  Data Definition file $SUBPARTDEFFILE contains blank lines."
               ERROR=Y
            else
               SUBPARTKEYTYPE=`echo $SUBPARTDEFFILE|awk -F. '{print $3}'`
               if [ "$SUBPARTKEYTYPE" != "date" -a "$SUBPARTKEYTYPE" != "varchar2" -a "$SUBPARTKEYTYPE" != "number" ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE must have a date, varchar2, or date extension."
                  ERROR=Y
               fi
               if [ "$SUBPARTKEYTYPE" = "date" ];
               then
                  if [ `< $SUBPARTDEFFILE grep -v ^#|sed /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/d|wc -l` -gt 0 ];
                  then
                     echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE has invalid date formats.  The lines in date data definition files must be of the format DDMMYYYY."
                     ERROR=Y
                  fi
               fi

               if [ "$SUBPARTKEYTYPE" = "number" ];
               then
                  if [ `< $SUBPARTDEFFILE grep -v #^|sed /^[0-9]*$/d|wc -l` -gt 0 ];
                  then
                     echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE has invalid numbers."
                     ERROR=Y
                  fi
               fi
               # Verify data def files have no duplicate values
               if [ `< $SUBPARTDEFFILE grep -v^#|wc -l` -ne `< $SUBPARTDEFFILE grep -v^#|sort -u|wc -l` ];
               then
                  echo "*** ERROR *** $ATTRIBFILE line ${i} field 8:  Data Definition file $SUBPARTDEFFILE has duplicate values."
                  ERROR=Y
               fi
            fi
         fi
      fi
   fi

   if [ `grep -w "CREATE TABLE $TABLENAME" $PREPARTDDL|wc -l` -eq 0 ];
   then
      echo "Warning in $ATTRIBFILE line ${i} field 1:  The table $TABLENAME is not found in the $PREPARTDDL file.  Skipping."
   fi
done

if [ "$ERROR" = 'Y' ];
then
   echo "exiting because of errors"
   exit 1
fi

exponential()
{
   ((i=1))
   ((j=1))
   while [ $j -le $2 ];
   do
      ((i=$i*$1))
      ((j=$j+1))
   done
   echo $i
}

typeset -i PARTCOUNT
typeset -i SUBPARTCOUNT
typeset -i FACTOR
typeset -i TABLENAMELENGTH

< $ATTRIBFILE grep -v ^#|grep -v ^PREPARTDDL|grep -v ^PARTDDL|grep -v '^[     ]*$'|while read line;
do
   unset TABLENAME
   unset PARTKEY
   unset PARTMETHOD
   unset PARTDEFFILE
   unset PARTCOUNT
   unset PARTINTERVAL
   unset SUBPARTKEY
   unset SUBPARTMETHOD 
   unset SUBPARTDEFFILE
   unset SUBPARTCOUNT
   unset TABLESPACE

   TABLENAME=`echo $line|awk -F, '{print $1}'`

   if [ `grep -w "CREATE TABLE $TABLENAME" $PREPARTDDL|wc -l` -eq 0 ];
   then
      continue
   fi

   PARTKEY=`echo $line|awk -F, '{print $2}'`
   PARTMETHOD=`echo $line|awk -F, '{print $3}'`
   PARTINTERVAL=`echo $line|awk -F, '{print $6}'`
   if [ "$PARTMETHOD" != 'HASH' ];
   then
      PARTDEFFILE=data_def/`echo $line|awk -F, '{print $4}'`
      PARTKEYTYPE=`echo $PARTDEFFILE|awk -F. '{print $3}'`
   fi
   
   if [ "$PARTMETHOD" = 'HASH' -o "$PARTMETHOD" = 'INTERVAL' ];
   then
      let PARTCOUNT=`echo $line|awk -F, '{print $5}'`
   fi

   SUBPARTKEY=`echo $line|awk -F, '{print $7}'`
   SUBPARTMETHOD=`echo $line|awk -F, '{print $8}'`
   if [ "$SUBPARTMETHOD" != 'HASH' ];
   then
      SUBPARTDEFFILE=data_def/`echo $line|awk -F, '{print $9}'`
      SUBPARTKEYTYPE=`echo $SUBPARTDEFFILE|awk -F. '{print $3}'`
   else
      let SUBPARTCOUNT=`echo $line|awk -F, '{print $10}'`
   fi

   TABLESPACE=`echo $line|awk -F, '{print $11}'`
   if [ -z "$TABLESPACE" ];
   then
      TABLESPACE=RETAIL_DATA
   fi

   if [ "$PARTKEYTYPE" = 'date' ];
   then
      PARTKEYPREFIX=TO_DATE\(\'
      PARTKEYSUFFIX=\',\'DDMMYYYY\'\)
   elif [ "$PARTKEYTYPE" = 'varchar2' ];
   then
      PARTKEYPREFIX=\'
      PARTKEYSUFFIX=\'
   else
      PARTKEYPREFIX=""
      PARTKEYSUFFIX=""
   fi

   PARTTEMPFILE=$TABLENAME.part.sql
   if [ -f "$PARTTEMPFILE" ];
   then
      rm $PARTTEMPFILE
   fi

   if [ ! -z "$SUBPARTKEY" ];
   then
      if [ "$SUBPARTKEYTYPE" = 'date' ];
      then
         SUBKEYPREFIX=TO_DATE\(\'
         SUBKEYSUFFIX=\',\'DDMMYYYY\'\)
      elif [ "$SUBPARTKEYTYPE" = 'varchar2' ];
      then
         SUBKEYPREFIX=\'
         SUBKEYSUFFIX=\'
      else 
         SUBKEYPREFIX=""
         SUBKEYSUFFIX=""
      fi
   fi

   ((PARTNAMELENGTH=0))

   echo "Generating Partitioned DDL for ${TABLENAME}"
   if [ "$PARTMETHOD" = "INTERVAL" ];
   then
      echo " PARTITION BY RANGE (${PARTKEY})" >> $PARTTEMPFILE
      if [ "$PARTINTERVAL" = "DAY" ]; 
      then     
         echo " INTERVAL (NUMTODSINTERVAL($PARTCOUNT,'$PARTINTERVAL'))" >> $PARTTEMPFILE
      else
         echo " INTERVAL (NUMTOYMINTERVAL($PARTCOUNT,'$PARTINTERVAL'))" >> $PARTTEMPFILE
      fi
   else
      echo " PARTITION BY ${PARTMETHOD}(${PARTKEY})" >> $PARTTEMPFILE
   fi

   if [ ! -z "$SUBPARTKEY" ];
   then
      echo " SUBPARTITION BY "$SUBPARTMETHOD"("$SUBPARTKEY")" >> $PARTTEMPFILE
      echo " SUBPARTITION TEMPLATE" >> $PARTTEMPFILE
      echo " (" >> $PARTTEMPFILE
      if [ "$SUBPARTMETHOD" = "LIST" ];
      then
         ((i=1))
         COMMA=
         let FACTOR=`< $SUBPARTDEFFILE wc -l|awk '{printf "%d", length($1)}'`
         ((EXPFACTOR=`exponential 10 $FACTOR`))
         ((PARTNAMELENGTH=$FACTOR+2))
         cat $SUBPARTDEFFILE | while read line
         do
            ((j=$i+$EXPFACTOR))
            SPNUM=`echo $j|awk '{printf "%s\n", substr($1,2)'}`
            echo "${COMMA}SUBPARTITION S${SPNUM} VALUES (${SUBKEYPREFIX}${line}${SUBKEYSUFFIX}) TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
            ((i=$i+1))
            COMMA=,
         done
      else
         ((i=1))
         COMMA=
         let FACTOR=`echo $SUBPARTCOUNT|awk '{printf "%d", length($1)}'`
         ((EXPFACTOR=`exponential 10 $FACTOR`))
         ((PARTNAMELENGTH=$FACTOR+2))
         while [ $i -le $SUBPARTCOUNT ];
         do
            ((j=$i+$EXPFACTOR))
            SPNUM=`echo $j|awk '{printf "%s\n", substr($1,2)'}`
            echo "${COMMA}SUBPARTITION S${SPNUM} TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
            ((i=$i+1))
            COMMA=,
         done
      fi
      echo " )" >> $PARTTEMPFILE
   fi

   echo " (" >> $PARTTEMPFILE

   if [ "$PARTMETHOD" = "RANGE" -o "$PARTMETHOD" = "INTERVAL" ];
   then
      ((i=1))
      COMMA=
      let FACTOR=`< $PARTDEFFILE wc -l|awk '{printf "%d", length($1)}'`
      ((EXPFACTOR=`exponential 10 $FACTOR`))
      let TABLENAMELENGTH=`echo $TABLENAME|awk '{printf "%d", length($1)}'`
      if [ $FACTOR -lt 3 ]; # FACTOR has minimum size of 3 because of "MAX" partition name
      then
         ((PARTNAMELENGTH=$PARTNAMELENGTH+3+$TABLENAMELENGTH+1))  # 0 + "MAX" + TABLENAMELENGTH + "_"
      else
         ((PARTNAMELENGTH=$PARTNAMELENGTH+$FACTOR+$TABLENAMELENGTH+2)) # 0 + FACTOR + TABLENAMELENGTH + "_P"
      fi
      if [ $PARTNAMELENGTH -gt 30 ];
      then
         ((TABLECHOP=30-$PARTNAMELENGTH+$TABLENAMELENGTH))
         TABLENAMECHOP=`printf "%.${TABLECHOP}s" $TABLENAME`
      else
         TABLENAMECHOP=$TABLENAME
      fi
      while read line
      do
         ((j=$i+$EXPFACTOR))
         PNUM=`echo $j|awk '{printf "%s\n", substr($1,2)'}`
         echo "${COMMA}PARTITION ${TABLENAMECHOP}_P${PNUM} VALUES LESS THAN (${PARTKEYPREFIX}${line}${PARTKEYSUFFIX}) TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
         COMMA=,
         ((i=$i+1))
      done < $PARTDEFFILE
      if [ "$PARTMETHOD" = "RANGE" ];
      then
         echo "${COMMA}PARTITION ${TABLENAMECHOP}_MAX VALUES LESS THAN (MAXVALUE) TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
      fi   
   elif [ "$PARTMETHOD" = "LIST" ];
   then
      ((i=1))
      COMMA=
      let FACTOR=`< $PARTDEFFILE wc -l|awk '{printf "%d", length($1)}'`
      ((EXPFACTOR=`exponential 10 $FACTOR`))
      let TABLENAMELENGTH=`echo $TABLENAME|awk '{printf "%d", length($1)}'`
      ((PARTNAMELENGTH=$PARTNAMELENGTH+$FACTOR+$TABLENAMELENGTH+2))
      if [ $PARTNAMELENGTH -gt 30 ];
      then
         ((TABLECHOP=30-$PARTNAMELENGTH+$TABLENAMELENGTH))
         TABLENAMECHOP=`printf "%.${TABLECHOP}s" $TABLENAME`
      else
         TABLENAMECHOP=$TABLENAME
      fi
      cat $PARTDEFFILE | while read line
      do
         ((j=$i+$EXPFACTOR))
         PNUM=`echo $j|awk '{printf "%s\n", substr($1,2)'}`
         echo "${COMMA}PARTITION ${TABLENAMECHOP}_P${PNUM} VALUES (${PARTKEYPREFIX}${line}${PARTKEYSUFFIX}) TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
         ((i=$i+1))
         COMMA=,
      done
   else
      ((i=1))
      COMMA=
      let FACTOR=`echo $PARTCOUNT|awk '{printf "%d", length($1)}'`
      ((EXPFACTOR=`exponential 10 $FACTOR`))
      let TABLENAMELENGTH=`echo $TABLENAME|awk '{printf "%d", length($1)}'`
      ((PARTNAMELENGTH=$PARTNAMELENGTH+$FACTOR+$TABLENAMELENGTH+2))
      if [ $PARTNAMELENGTH -gt 30 ];
      then
         ((TABLECHOP=30-$PARTNAMELENGTH+$TABLENAMELENGTH))
         TABLENAMECHOP=`printf "%.${TABLECHOP}s" $TABLENAME`
      else
         TABLENAMECHOP=$TABLENAME
      fi
      while [ $i -le $PARTCOUNT ];
      do
         ((j=$i+$EXPFACTOR))
         PNUM=`echo $j|awk '{printf "%s\n", substr($1,2)'}`
         echo " "$COMMA"PARTITION "${TABLENAMECHOP}"_P${PNUM} TABLESPACE $TABLESPACE" >> $PARTTEMPFILE
         ((i=$i+1))
         COMMA=,
      done
   fi
   echo " )" >> $PARTTEMPFILE

   ((CREATELINE=`< $PARTDDL sed "/CREATE TABLE ${TABLENAME}$/bline
   d
   :line
   =
   d"`))

   ((SLASHLINE=`< $PARTDDL sed -n $CREATELINE,'$bline
   d
   :line
   /\//bline2
   d
   :line2
   =
   q'`))

   ((SLASHLINE=$SLASHLINE-1))
   sed "${SLASHLINE}r $PARTTEMPFILE" $PARTDDL > temp_part_ddl
   mv temp_part_ddl $PARTDDL
   rm $PARTTEMPFILE

done

echo "partition.ksh has generated the DDL for partitioned tables in the $PARTDDL file."
echo "Completed successfully"
