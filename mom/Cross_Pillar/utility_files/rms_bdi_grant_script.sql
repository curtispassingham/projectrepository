declare
  username varchar2(250) := '&1';
begin
  for rec in (select 'grant insert, update, delete on ' || '&_USER' || '.' || table_name || ' to ' || username as grant_statement from user_tables) loop
     execute immediate rec.grant_statement;
  end loop;
end;
/
