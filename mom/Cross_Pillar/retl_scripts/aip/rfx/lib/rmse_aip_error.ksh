#!/bin/ksh
##############################################################################
#                                                                            #
#  Functions that aid in helping scripts terminate gracefully and            #
#  enable arbitrary generation of reject files.                              #
#                                                                            #
##############################################################################

#  Global variables:

export REJ_FILELIST=""  # contains the list of reject files
export REJ_FILECNT=0    # counter for number of reject files

#  Function rmse_cleanup is called to remove all intermediate files.
#  May be extended for general cleanup as well.

function rmse_cleanup {

  #  Remove reject files:
  
  rm -f ${REJ_FILELIST}
  rm -f ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT
  rm -f ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST
    
  #  Remove empty .bad file:
  
  if [[ ! -s ${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad ]]; then
     rm -f ${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad
  fi
    
  ## set +f
  ## for badfile in $(ls -1 $ERR_DIR/$PROGRAM_NAME*.bad); do
  ##   if [ ! -s  $badfile ]; then  rm -f  $badfile; fi
  ## done
  ## set -f
}

#  Execute the rmse_terminate function upon receipt  
#  of signals 2 (INT), 9 (KILL), or 15 (TERM):

trap rmse_terminate INT TERM KILL

#  Function rmse_terminate - should be called after all flows to exit:

function rmse_terminate {
    rmse_cleanup
    if [[ $1 = 0 ]]; then
	exit 0
    else
	echo "Aborting ..."
	exit 1
    fi
}

#  Function getRejectFile can be called between cat > flow.xml >>EOF...EOF
#  and will echo a unique reject file and store the filename in REJ_FILELIST:

function getRejectFile {

    #  Get around subshell not being able to modify parent environment  
    #  by writing shell variable values to a file:
    
    REJ_FILECNT=`cat -s ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT`
    REJ_FILECNT=${REJ_FILECNT:=0}
    REJ_FILELIST=`cat -s ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST`
    CURR_REJFILE="${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad"

    print -n "`expr ${REJ_FILECNT} + 1`"  >               \
         ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT
    print -n "${REJ_FILELIST} ${CURR_REJFILE}"  >         \
         ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST

    #  Echo the reject file name back to the xml flow
    #  in the calling program:

    echo "${CURR_REJFILE}"
    return
}


##############################################################################
#                                                                            #
#  The checkerror function checks an exit status code, writes                #
#  error messages in the log file and calls rmse_terminate.                  #
#                                                                            #
#  Usage: checkerror  [-e error_to_check]  [-r reject_file]  [-d]            #
#                     [-m error_message]  [-w reject_message]                #
#                                                                            #
#  -e: error_to_check is the exit status code.                               #
#  -m: error_message is the message to produce if bad exit code.             #
#  -d: interpret exitcode with respect to DB2 (i.e. 1 is OK).                #
#  -r: reject_file is the reject file name.                                  #
#  -w: reject_message is the message to produce if there is a reject file.   #
#                                                                            #
##############################################################################

function checkerror {

   error_to_check=0
   check_reject="FALSE"
   is_db2="FALSE"
   error_message=" A fatal error occurred in $0"
   reject_message=" Some records were rejected in $0"
   
   while getopts "e:r:dm:w:" opt; do
      case $opt in
       e ) error_to_check=$OPTARG;;
       r ) reject_file=$OPTARG
           check_reject="TRUE";;
       d ) is_db2="TRUE";;
       m ) error_message=$OPTARG;;
       w ) reject_message=$OPTARG;;
       \? ) print 'checkerror usage: -e exitcode -r rejectfile -d -m errmess -w rejmess'
            rmse_terminate 1
      esac
   done 

   if [[ $check_reject = "TRUE" ]]; then
      if [[ -s $reject_file ]]; then
         echo "$PROGRAM_NAME "`date +"%T"` ": WARNING: " $reject_message >> $LOG_DIR/$LOG
      fi
   fi

   if [[ $error_to_check != 0 ]]; then
      if [[ $is_db2 = "TRUE" ]]; then
         if [[ $error_to_check > 1 ]]; then
            echo "$PROGRAM_NAME "`date +"%T"`": ERROR: " $error_message >> $LOG_DIR/$LOG
            rmse_terminate  $error_to_check
         fi
      else
         echo "$PROGRAM_NAME "`date +"%T"`": ERROR: " $error_message >> $LOG_DIR/$LOG
         rmse_terminate  $error_to_check
      fi
   fi
}
