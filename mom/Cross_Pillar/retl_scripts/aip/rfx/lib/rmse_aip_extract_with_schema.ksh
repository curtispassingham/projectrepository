#! /bin/ksh

function extract_with_schema
{
    PROGRAM_NAME=$1
    SCHEMAFILE=$2
    OUTPUTFILE=$3

    shift
    shift
    shift

    QUERY="$*"

    if [[ $PROGRAM_NAME = "rmse_daily_sales" ]] 
    then
      CONVERT_STR=" <OPERATOR type=\"convert\">
      <INPUT name=\"output.v\"/>
      <PROPERTY name=\"convertspec\">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield=\"LOC\" sourcefield=\"LOC\" newtype=\"int64\">
                  <CONVERTFUNCTION name=\"default\">
                  </CONVERTFUNCTION>
                  <TYPEPROPERTY name=\"nullable\" value=\"false\">
                  </TYPEPROPERTY>
               </CONVERT>
               <CONVERT destfield=\"TRAN_DATE\" sourcefield=\"TRAN_DATE\">
                  <CONVERTFUNCTION name=\"make_not_nullable\">
                     <FUNCTIONARG name=\"nullvalue\" value=\"date\">
                     </FUNCTIONARG>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield=\"TRAN_CODE\" sourcefield=\"TRAN_CODE\" newtype=\"int8\">
                  <CONVERTFUNCTION name=\"default\">
                  </CONVERTFUNCTION>
                  <TYPEPROPERTY name=\"nullable\" value=\"false\">
                  </TYPEPROPERTY>
               </CONVERT>
               <CONVERT destfield=\"DOMAIN_ID\" sourcefield=\"DOMAIN_ID\" newtype=\"int8\">
                  <CONVERTFUNCTION name=\"default\">
                  </CONVERTFUNCTION>
                  <TYPEPROPERTY name=\"nullable\" value=\"false\">
                  </TYPEPROPERTY>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
          <OUTPUT name=\"output1.v\"/>
       </OPERATOR>
       <OPERATOR type=\"export\">
	    <INPUT    name=\"output1.v\"/>
	    <PROPERTY name=\"outputfile\" value=\"${OUTPUTFILE}\"/>
	    <PROPERTY name=\"schemafile\" value=\"${SCHEMAFILE}\"/>
	</OPERATOR>"
    else
      if [[ $PROGRAM_NAME = "rmse_weekly_sales" ]] 
      then
         CONVERT_STR=" <OPERATOR type=\"convert\">
         <INPUT name=\"output.v\"/>
         <PROPERTY name=\"convertspec\">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield=\"ITEM\" sourcefield=\"ITEM\">
                     <CONVERTFUNCTION name=\"make_not_nullable\">
                        <FUNCTIONARG name=\"nullvalue\" value=\"0\">
                        </FUNCTIONARG>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield=\"LOC\" sourcefield=\"LOC\" newtype=\"int64\">
                     <CONVERTFUNCTION name=\"default\">
                     </CONVERTFUNCTION>
                     <TYPEPROPERTY name=\"nullable\" value=\"false\">
                     </TYPEPROPERTY>
                  </CONVERT>
                  <CONVERT destfield=\"SALES_TYPE\" sourcefield=\"SALES_TYPE\">
                     <CONVERTFUNCTION name=\"make_not_nullable\">
                        <FUNCTIONARG name=\"nullvalue\" value=\"R\">
                        </FUNCTIONARG>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield=\"DOMAIN_ID\" sourcefield=\"DOMAIN_ID\" newtype=\"int8\">
                     <CONVERTFUNCTION name=\"default\">
                     </CONVERTFUNCTION>
                     <TYPEPROPERTY name=\"nullable\" value=\"false\">
                     </TYPEPROPERTY>
                  </CONVERT>
                </CONVERTSPECS>
             ]]>
          </PROPERTY>
            <OUTPUT name=\"output1.v\"/>
         </OPERATOR>
         <OPERATOR type=\"export\">
	    <INPUT    name=\"output1.v\"/>
	    <PROPERTY name=\"outputfile\" value=\"${OUTPUTFILE}\"/>
	    <PROPERTY name=\"schemafile\" value=\"${SCHEMAFILE}\"/>
         </OPERATOR>"            
      else
          CONVERT_STR="<OPERATOR type=\"export\">
	       <INPUT    name=\"output.v\"/>
	       <PROPERTY name=\"outputfile\" value=\"${OUTPUTFILE}\"/>
	       <PROPERTY name=\"schemafile\" value=\"${SCHEMAFILE}\"/>
	   </OPERATOR>"
      fi      
   fi

   cat > ${PROGRAM_NAME}.xml - << EOF
    
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query" >
            <![CDATA[
            ${QUERY}
	        ]]>
         </PROPERTY>
      <OUTPUT   name = "output.v"/>
      </OPERATOR>
      ${CONVERT_STR}
   </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${PROGRAM_NAME}.xml

return $?
}
