#!/bin/ksh
##############################################################################
#
#  The log_num_recs function determines the number of records in the
#  given file and logs that number into the log and error files. It
#  will also (optionally) echo the number of records back to the calling
#  program.
#
#     Usage: log_num_recs  $1  [$2]
#
#            $1 = File name
#
#            $2 = "-e", if it is desired to have the number of records
#                 echoed back to the calling program. For example, if
#                 it were called in the following manner:
#                 num_recs=$(log_num_recs  $STORE_DATA_FILE  -e)
#                 then $num_recs would be equal to the number of records.
#
##############################################################################

function log_num_recs  {

   if [ -f  $1 ]; then
      NUM_RECS=$(wc -l $1  |  awk '{print $1}')
   else
      NUM_RECS=0
   fi
   message "Number of records in $1 = $NUM_RECS."
   
   if [ "$2" = "-e" ]; then  echo  $NUM_RECS; fi
   
   return
}
