#!/bin/ksh
##############################################################################
# This Function to produce message text with program name and timestamp
#     Usage: $1 = Message text
##############################################################################

function upd_retl_pot_ext_date
{
   FUNC_NAME=$1

 if [[ $FUNC_NAME = "CLOSED_ORDER" ]]
 then
    sqlplus $SQLPLUS_LOGON << EOF
    WHENEVER SQLERROR EXIT 4;
    UPDATE ${RMS_OWNER}.RETL_EXTRACT_DATES
       SET LAST_EXTR_CLOSED_POT_DATE = to_date(${VDATE},'yyyymmdd');
    COMMIT;
    exit;
EOF
 elif [[ $FUNC_NAME = "RECEIVED_QTY" ]]
 then
    sqlplus $SQLPLUS_LOGON << EOF
    WHENEVER SQLERROR EXIT 4;
    UPDATE ${RMS_OWNER}.RETL_EXTRACT_DATES
       SET LAST_EXTR_RECEIVED_POT_DATE = to_date(${LAST_EXTR_RECEIVED_POT_DATE},'yyyymmdd');
    COMMIT;
    exit;
EOF
 else
   checkerror -e 2 -m "**No Input parameter specified"
 fi
}
