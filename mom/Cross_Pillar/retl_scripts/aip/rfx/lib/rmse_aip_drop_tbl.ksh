#!/bin/ksh

#####################################################################
#  Drop table
#
#  Usage:  $1 = Table Name
#####################################################################

function drop_tbl
{
 if [[ $DB_ENV = "DB2" ]]
 then
    db2 "connect to $DBNAME user $MMUSER using $PASSWORD"
    db2 "DROP TABLE $1"
    checkerror -e $? -d -m "Dropping table $1 failed"
    db2 "terminate" >> $ERR_FILE
 elif [[ $DB_ENV = "ORA" ]]
 then
    sqlplus $SQLPLUS_LOGON << EOF >> $ERR_FILE
    WHENEVER SQLERROR EXIT 4;
    DROP TABLE $1;
EOF
 elif [[ $DB_ENV = "TER" ]]
 then
    bteq .logon $DBNAME/$MMUSER,$PASSWORD << EOF >> $ERR_FILE
    DROP TABLE $1;
    .exit;
EOF
 fi
}
