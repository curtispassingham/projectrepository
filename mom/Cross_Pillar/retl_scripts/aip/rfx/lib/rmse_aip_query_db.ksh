#!/bin/ksh

#####################################################################
#  Drop table
#
#  Usage:  $1 = Table Name
#####################################################################


function rmse_query_db
{
   DBNAME=$1
   QUERY=$2

 if [[ $DB_ENV = "DB2" ]]
 then
    exit -1;
 elif [[ $DB_ENV = "ORA" ]]
 then
    sqlplus $SQLPLUS_LOGON << EOF 
    WHENEVER SQLERROR EXIT 4;
    $QUERY;
EOF
 elif [[ $DB_ENV = "TER" ]]
 then
    exit -1;
EOF
 fi
 checkerror -e $? -m "Query \"$*\" failed"
}
