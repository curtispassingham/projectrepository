#!/bin/ksh
#####################################################################
#  Analyze table
#
#  Usage:  ${1} = Table Ananlyze
#####################################################################
function analyze_tbl
{
if [[ ${DB_ENV} = "DB2" ]]
then
   db2 "connect to ${DBNAME} user ${MMUSER} using ${PASSWORD}"
   db2 "runstats on table ${1} and detailed indexes all"
   checkerror -e $? -d -m "Can't analyze table ${1}"
   db2 "terminate" 
elif [[ ${DB_ENV} = "ORA" ]]
then
   if [[ ${2} != "" ]]
   then
      sqlplus $SQLPLUS_LOGON << EOF
        WHENEVER SQLERROR EXIT 4;
           execute sys.dbms_stats.gather_table_stats('${1}','${2}', null, 5);
        exit;
EOF
   else
      TBL=`echo ${1} | cut -d "." -f2`
      sqlplus $SQLPLUS_LOGON << EOF
        WHENEVER SQLERROR EXIT 4;
           execute sys.dbms_stats.gather_table_stats('${BA_OWNER}','${TBL}', null, 5);
      exit;
EOF
 checkerror -e $? -m "Analyzing table ${1} failed"
fi
fi
}
