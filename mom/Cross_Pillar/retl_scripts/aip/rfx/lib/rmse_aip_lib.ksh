#!/bin/ksh

. ${LIB_DIR}/rmse_aip_analyze_tbl.ksh
. ${LIB_DIR}/rmse_aip_error.ksh
. ${LIB_DIR}/rmse_aip_message.ksh
. ${LIB_DIR}/rmse_aip_drop_tbl.ksh
. ${LIB_DIR}/rmse_aip_log_num_recs.ksh
. ${LIB_DIR}/rmse_aip_simple_extract.ksh
. ${LIB_DIR}/rmse_aip_extract_with_schema.ksh
. ${LIB_DIR}/rmse_aip_query_db.ksh
. ${LIB_DIR}/rmsl_aip_update_retl_date.ksh

