#!/bin/ksh
##############################################################################
# This Function to produce message text with program name and timestamp
#     Usage: $1 = Message text
##############################################################################

function message 
{
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $LOG_FILE 
   echo "\n-----------------------------"  >> $ERR_FILE
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $ERR_FILE
   echo "-----------------------------"  >> $ERR_FILE
}
