#!/bin/ksh
########################################################
# Purpose: extracts RMS tsf in the well data for AIP
#          future delivery
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME='rmse_aip_tsf_in_well'


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE_W=${DATA_DIR}/${PROGRAM_NAME}_w.dat
export OUTPUT_FILE_S=${DATA_DIR}/${PROGRAM_NAME}_s.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_ORD_MULT_FILE_W=${DATA_DIR}/rmse_aip_tsf_in_well_reject_ord_mult_w.txt
export REJECT_ORD_MULT_FILE_S=${DATA_DIR}/rmse_aip_tsf_in_well_reject_ord_mult_s.txt


message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE}.xml << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
  ${DBREAD}
     <PROPERTY name = "query">
         <![CDATA[
            SELECT tsf.DAY,
                   tsf.LOC,
                   tsf.ITEM,
                   CASE tsf.PRIMARY_CASE_SIZE
                      WHEN 'C' THEN rtrim(substr(isup.SUPP_PACK_SIZE,0,6),'.')
                      WHEN 'I' THEN rtrim(substr(isup.INNER_PACK_SIZE,0,6),'.')
                      WHEN 'P' THEN rtrim(substr(isup.TI * isup.HI * isup.SUPP_PACK_SIZE,0,6),'.')
                   ELSE '1'
                   END ORDER_MULTIPLE,
                   rtrim(substr(SUM(tsf.RESERVE_QTY),0,8),'.') RESERVE_QTY,
                   tsf.LOC_TYPE
              FROM (SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0), h.APPROVAL_DATE ),'YYYYMMDD') DAY,
                           CASE WHEN h.FROM_LOC_TYPE = 'W'
                                THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                  FROM wh
                                                                                                                                 WHERE wh = physical_wh))
                                          THEN sif.FROM_LOC
                                          ELSE h.FROM_LOC
                                     END
                                ELSE h.FROM_LOC
                           END LOC,
                           im.ITEM,
                           NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                           isp.PRIMARY_CASE_SIZE,
                           h.TO_LOC_TYPE,
                           h.FROM_LOC_TYPE LOC_TYPE
                      FROM ${RMS_OWNER}.TSFHEAD h,
                           ${RMS_OWNER}.TSFDETAIL d,
                           ${RMS_OWNER}.ITEM_MASTER im,
                           ${RMS_OWNER}.ITEM_SUPPLIER isp,
                           ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                           (SELECT t.DEPT,
                                   t.CLASS,
                                   nvl(t.SUBCLASS, -999) as SUBCLASS,
                                   t.ORIGIN_TYPE,
                                   wh1.WH as ORIGIN,
                                   t.DESTINATION_TYPE,
                                   wh2.WH as DESTINATION,
                                   t.TRANSIT_TIME
                              FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                   ${RMS_OWNER}.WH wh1,
                                   ${RMS_OWNER}.WH wh2
                             WHERE t.ORIGIN_TYPE = 'WH'
                               AND t.ORIGIN = wh1.PHYSICAL_WH
                               AND wh1.WH != wh1.PHYSICAL_WH
                               AND t.DESTINATION_TYPE = 'WH'
                               AND t.DESTINATION = wh2.PHYSICAL_WH
                               AND wh2.WH != wh2.PHYSICAL_WH
                            UNION
                            SELECT t.DEPT,
                                   t.CLASS,
                                   nvl(t.SUBCLASS, -999) as SUBCLASS,
                                   t.ORIGIN_TYPE,
                                   wh1.WH as ORIGIN,
                                   t.DESTINATION_TYPE,
                                   t.DESTINATION,
                                   t.TRANSIT_TIME
                              FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                   ${RMS_OWNER}.WH wh1
                             WHERE t.ORIGIN_TYPE = 'WH'
                               AND t.DESTINATION_TYPE = 'ST'
                               AND t.ORIGIN = wh1.PHYSICAL_WH
                               AND wh1.WH != wh1.PHYSICAL_WH
                            UNION
                            SELECT t.DEPT,
                                   t.CLASS,
                                   nvl(t.SUBCLASS, -999) as SUBCLASS,
                                   t.ORIGIN_TYPE,
                                   t.ORIGIN,
                                   t.DESTINATION_TYPE,
                                   t.DESTINATION,
                                   t.TRANSIT_TIME
                              FROM ${RMS_OWNER}.TRANSIT_TIMES t
                             WHERE t.ORIGIN_TYPE = 'ST'
                               AND t.DESTINATION_TYPE = 'ST') t,
                           ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
                     WHERE h.TSF_NO=d.TSF_NO
                       AND d.ITEM=im.ITEM
                       AND d.ITEM=sif.ITEM(+)
                       AND d.TSF_NO=sif.TSF_NO(+)
                       AND im.STATUS='A'
                       AND im.ITEM_LEVEL=TRAN_LEVEL
                       AND im.ITEM = isp.ITEM
                       AND isp.PRIMARY_SUPP_IND = 'Y'
                       AND h.APPROVAL_DATE IS NOT NULL
                       AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                       AND h.STATUS IN ('A', 'S')
                       AND h.FROM_LOC_TYPE IN ('W', 'S')
                       AND t.dept=im.dept
                       AND t.CLASS=im.CLASS
                       AND t.origin=h.from_loc
                       AND t.destination=h.to_loc
                       AND NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0),h.APPROVAL_DATE ) >=
                               TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                       AND im.AIP_CASE_TYPE = 'I'
                       AND (im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                     UNION ALL
                    SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE, h.APPROVAL_DATE ),'YYYYMMDD') DAY,
                           CASE WHEN h.FROM_LOC_TYPE = 'W'
                                THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                  FROM wh
                                                                                                                                 WHERE wh = physical_wh))
                                          THEN sif.FROM_LOC
                                          ELSE h.FROM_LOC
                                     END
                                ELSE h.FROM_LOC
                           END LOC,
                           im.ITEM,
                           NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                           isp.PRIMARY_CASE_SIZE,
                           h.TO_LOC_TYPE,
                           h.FROM_LOC_TYPE LOC_TYPE
                      FROM ${RMS_OWNER}.TSFHEAD h,
                           ${RMS_OWNER}.TSFDETAIL d,
                           ${RMS_OWNER}.ITEM_MASTER im,
                           ${RMS_OWNER}.ITEM_SUPPLIER isp,
                           ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                           ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
                     WHERE h.TSF_NO=d.TSF_NO
                       AND d.ITEM=im.ITEM
                       AND d.ITEM=sif.ITEM(+)
                       AND d.TSF_NO=sif.TSF_NO(+)
                       AND im.STATUS='A'
                       AND im.ITEM_LEVEL=TRAN_LEVEL
                       AND im.ITEM = isp.ITEM
                       AND isp.PRIMARY_SUPP_IND = 'Y'
                       AND h.APPROVAL_DATE IS NOT NULL
                       AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                       AND h.STATUS IN ('A', 'S')
                       AND h.FROM_LOC_TYPE IN ('W', 'S')
                       AND NOT EXISTS (SELECT '1'
                                          FROM (SELECT t.DEPT,
                                                       t.CLASS,
                                                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                       t.ORIGIN_TYPE,
                                                       wh1.WH as ORIGIN,
                                                       t.DESTINATION_TYPE,
                                                       wh2.WH as DESTINATION,
                                                       t.TRANSIT_TIME
                                                  FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                       ${RMS_OWNER}.WH wh1,
                                                       ${RMS_OWNER}.WH wh2
                                                 WHERE t.ORIGIN_TYPE = 'WH'
                                                   AND t.ORIGIN = wh1.PHYSICAL_WH
                                                   AND wh1.WH != wh1.PHYSICAL_WH
                                                   AND t.DESTINATION_TYPE = 'WH'
                                                   AND t.DESTINATION = wh2.PHYSICAL_WH
                                                   AND wh2.WH != wh2.PHYSICAL_WH
                                                UNION
                                                SELECT t.DEPT,
                                                       t.CLASS,
                                                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                       t.ORIGIN_TYPE,
                                                       wh1.WH as ORIGIN,
                                                       t.DESTINATION_TYPE,
                                                       t.DESTINATION,
                                                       t.TRANSIT_TIME
                                                  FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                       ${RMS_OWNER}.WH wh1
                                                 WHERE t.ORIGIN_TYPE = 'WH'
                                                   AND t.DESTINATION_TYPE = 'ST'
                                                   AND t.ORIGIN = wh1.PHYSICAL_WH
                                                   AND wh1.WH != wh1.PHYSICAL_WH
                                                UNION
                                                SELECT t.DEPT,
                                                       t.CLASS,
                                                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                       t.ORIGIN_TYPE,
                                                       t.ORIGIN,
                                                       t.DESTINATION_TYPE,
                                                       t.DESTINATION,
                                                       t.TRANSIT_TIME
                                                  FROM ${RMS_OWNER}.TRANSIT_TIMES t
                                                 WHERE t.ORIGIN_TYPE = 'ST'
                                                   AND t.DESTINATION_TYPE = 'ST') i
                                         WHERE i.dept=im.dept
                                           AND i.CLASS=im.CLASS
                                           AND i.origin=h.from_loc
                                           AND i.destination=h.to_loc)
                       AND NVL(h.DELIVERY_DATE,h.APPROVAL_DATE) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                       AND im.AIP_CASE_TYPE = 'I'
                       AND (im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')) tsf,
                   ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup
             WHERE tsf.ITEM = isup.ITEM
               AND isup.PRIMARY_SUPP_IND = 'Y'
               AND isup.PRIMARY_COUNTRY_IND = 'Y'
          GROUP BY tsf.DAY,
                   tsf.LOC,
                   tsf.ITEM,
                   tsf.LOC_TYPE,
                   isup.INNER_PACK_SIZE,
                   isup.SUPP_PACK_SIZE,
                   (isup.ti * isup.hi * isup.SUPP_PACK_SIZE),
                   tsf.PRIMARY_CASE_SIZE,
                   tsf.TO_LOC_TYPE
         ]]>
      </PROPERTY>
      <OUTPUT name="informal_items.v"/>
  </OPERATOR>

  ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            WITH TRAN_DATA AS
               (SELECT t.DEPT,
                       t.CLASS,
                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                       t.ORIGIN_TYPE,
                       wh1.WH as ORIGIN,
                       t.DESTINATION_TYPE,
                       wh2.WH as DESTINATION,
                       t.TRANSIT_TIME
                  FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                       ${RMS_OWNER}.WH wh1,
                       ${RMS_OWNER}.WH wh2
                 WHERE t.ORIGIN_TYPE = 'WH'
                   AND t.ORIGIN = wh1.PHYSICAL_WH
                   AND wh1.WH != wh1.PHYSICAL_WH
                   AND t.DESTINATION_TYPE = 'WH'
                   AND t.DESTINATION = wh2.PHYSICAL_WH
                   AND wh2.WH != wh2.PHYSICAL_WH
                UNION
                SELECT t.DEPT,
                       t.CLASS,
                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                       t.ORIGIN_TYPE,
                       wh1.WH as ORIGIN,
                       t.DESTINATION_TYPE,
                       t.DESTINATION,
                       t.TRANSIT_TIME
                  FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                       ${RMS_OWNER}.WH wh1
                 WHERE t.ORIGIN_TYPE = 'WH'
                   AND t.DESTINATION_TYPE = 'ST'
                   AND t.ORIGIN = wh1.PHYSICAL_WH
                   AND wh1.WH != wh1.PHYSICAL_WH
                UNION
                SELECT t.DEPT,
                       t.CLASS,
                       nvl(t.SUBCLASS, -999) as SUBCLASS,
                       t.ORIGIN_TYPE,
                       t.ORIGIN,
                       t.DESTINATION_TYPE,
                       t.DESTINATION,
                       t.TRANSIT_TIME
                  FROM ${RMS_OWNER}.TRANSIT_TIMES t
                 WHERE t.ORIGIN_TYPE = 'ST'
                   AND t.DESTINATION_TYPE = 'ST'
                   )
            SELECT DAY,
                   LOC,
                   ITEM,
                   rtrim(substr(ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                   rtrim(substr(SUM(RESERVE_QTY*PACK_QTY),0,8),'.') RESERVE_QTY,
                   LOC_TYPE
              FROM (SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0), h.APPROVAL_DATE ),'YYYYMMDD') DAY,
                           CASE WHEN h.FROM_LOC_TYPE = 'W'
                                THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                  FROM wh
                                                                                                                                 WHERE wh = physical_wh))
                                          THEN sif.FROM_LOC
                                          ELSE h.FROM_LOC
                                     END
                                ELSE h.FROM_LOC
                           END LOC,
                           im.ITEM ITEM,
                           NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                           vpq.QTY ORDER_MULTIPLE,
                           vpq.QTY PACK_QTY,
                           h.FROM_LOC_TYPE LOC_TYPE
                      FROM ${RMS_OWNER}.TSFHEAD h,
                           ${RMS_OWNER}.TSFDETAIL d,
                           ${RMS_OWNER}.ITEM_MASTER im,
                           ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                           ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                           TRAN_DATA t,
                           ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
                     WHERE h.TSF_NO=d.TSF_NO
                       AND d.ITEM=im.ITEM
                       AND d.ITEM=sif.ITEM(+)
                       AND d.TSF_NO=sif.TSF_NO(+)
                       AND im.STATUS='A'
                       AND im.ITEM_LEVEL=im.TRAN_LEVEL
                       AND vpq.PACK_NO = im.ITEM
                       AND h.APPROVAL_DATE IS NOT NULL
                       AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                       AND h.STATUS IN ('A', 'S')
                       AND h.FROM_LOC_TYPE IN ('W', 'S')
                       AND t.dept=im.dept
                       AND t.CLASS=im.CLASS
                       AND t.origin=h.from_loc
                       AND t.destination=h.to_loc
                       AND NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0),h.APPROVAL_DATE ) >=
                              TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                       AND NVL(im.AIP_CASE_TYPE,'F') != 'I'
                       AND im.SIMPLE_PACK_IND = 'Y'
                       AND im.item IN (SELECT pm.pack_no
                                         FROM ${RMS_OWNER}.item_master im1,
                                              ${RMS_OWNER}.packitem pm
                                        WHERE pm.item = im1.item
                                          AND im1.forecast_ind = 'Y')
             UNION ALL
            SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE, h.APPROVAL_DATE),'YYYYMMDD') DAY,
                   CASE WHEN h.FROM_LOC_TYPE = 'W'
                                THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                  FROM wh
                                                                                                                                 WHERE wh = physical_wh))
                                          THEN sif.FROM_LOC
                                          ELSE h.FROM_LOC
                                     END
                                ELSE h.FROM_LOC
                           END LOC,
                   im.ITEM ITEM,
                   NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                   vpq.QTY ORDER_MULTIPLE,
                   vpq.QTY PACK_QTY,
                   h.FROM_LOC_TYPE LOC_TYPE
              FROM ${RMS_OWNER}.TSFHEAD h,
                   ${RMS_OWNER}.TSFDETAIL d,
                   ${RMS_OWNER}.ITEM_MASTER im,
                   ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                   ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                   ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
             WHERE h.TSF_NO=d.TSF_NO
               AND d.ITEM=im.ITEM
               AND d.ITEM=sif.ITEM(+)
               AND d.TSF_NO=sif.TSF_NO(+)
               AND im.STATUS='A'
               AND im.ITEM_LEVEL=im.TRAN_LEVEL
               AND vpq.PACK_NO = im.ITEM
               AND h.APPROVAL_DATE IS NOT NULL
               AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
               AND h.STATUS IN ('A', 'S')
               AND h.FROM_LOC_TYPE IN ('W', 'S')
               AND NOT EXISTS (SELECT '1'
                                 FROM TRAN_DATA i
                                WHERE i.dept=im.dept
                                  AND i.CLASS=im.CLASS
                                  AND i.origin=h.from_loc
                                  AND i.destination=h.to_loc)
               AND NVL(h.DELIVERY_DATE,h.APPROVAL_DATE) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
               AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
               AND im.SIMPLE_PACK_IND = 'Y'
               AND im.item IN (SELECT pm.pack_no
                                 FROM ${RMS_OWNER}.item_master im1,
                                      ${RMS_OWNER}.packitem pm
                                WHERE pm.item = im1.item
                                  AND im1.forecast_ind = 'Y')
             UNION
             /* When Regular item then*/
             SELECT DAY,
                    LOC,
                    ITEM,
                    RESERVE_QTY,
                    ORDER_MULTIPLE,
                    PACK_QTY,
                    LOC_TYPE
              FROM (
                    SELECT DAY,
                           LOC,
                           ITEM,
                           RESERVE_QTY,
                           ORDER_MULTIPLE,
                           PACK_QTY,
                           SIZE_RANK,
                           QTY_RANK,
                           LOC_TYPE,
                           RANK() OVER
                                      (PARTITION BY ITEM ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                      FROM
                          (
                                SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0), h.APPROVAL_DATE ),'YYYYMMDD') DAY,
                                       CASE WHEN h.FROM_LOC_TYPE = 'W'
                                            THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                              FROM wh
                                                                                                                                             WHERE wh = physical_wh))
                                                      THEN sif.FROM_LOC
                                                      ELSE h.FROM_LOC
                                                 END
                                            ELSE h.FROM_LOC
                                       END LOC,
                                       im2.ITEM ITEM,
                                       NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                                       vpq.QTY ORDER_MULTIPLE,
                                       1 PACK_QTY,
                                       h.FROM_LOC_TYPE LOC_TYPE,
                                       CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                            WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                            ELSE 3 END SIZE_RANK,
                                            RANK() OVER
                                                        (PARTITION BY im2.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                  FROM ${RMS_OWNER}.TSFHEAD h,
                                       ${RMS_OWNER}.TSFDETAIL d,
                                       ${RMS_OWNER}.ITEM_MASTER im,
                                       ${RMS_OWNER}.ITEM_MASTER im2,
                                       ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                                       TRAN_DATA t,
                                       ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                       ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                       ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
                                 WHERE h.TSF_NO=d.TSF_NO
                                   AND d.ITEM=im.ITEM
                                   AND d.ITEM=sif.ITEM(+)
                                   AND d.TSF_NO=sif.TSF_NO(+)
                                   AND im.STATUS='A'
                                   AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                   AND vpq.PACK_NO = im2.ITEM
                                   AND im2.SIMPLE_PACK_IND = 'Y'
                                   AND vpq.item = im.item
                                   AND isc.ITEM= vpq.ITEM
                                   AND h.APPROVAL_DATE IS NOT NULL
                                   AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                   AND h.STATUS IN ('A', 'S')
                                   AND h.FROM_LOC_TYPE IN ('W', 'S')
                                   AND t.dept=im.dept
                                   AND t.CLASS=im.CLASS
                                   AND t.origin=h.from_loc
                                   AND t.destination=h.to_loc
                                   AND NVL(h.DELIVERY_DATE - NVL(t.TRANSIT_TIME,0),h.APPROVAL_DATE ) >=
                                          TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                   AND NVL(im.AIP_CASE_TYPE,'F') != 'I'
                                   AND im.PACK_IND = 'N'
                                   AND im.FORECAST_IND = 'Y'
                         UNION ALL
                        SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE, h.APPROVAL_DATE),'YYYYMMDD') DAY,
                               CASE WHEN h.FROM_LOC_TYPE = 'W'
                                            THEN CASE WHEN h.TSF_TYPE = 'EG' or (h.TSF_TYPE = 'CO' and pco.oms_ind = 'Y' and h.FROM_LOC in (SELECT wh
                                                                                                                                              FROM wh
                                                                                                                                             WHERE wh = physical_wh))
                                                      THEN sif.FROM_LOC
                                                      ELSE h.FROM_LOC
                                                 END
                                            ELSE h.FROM_LOC
                                       END LOC,
                               im2.ITEM ITEM,
                               NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) RESERVE_QTY,
                               vpq.QTY ORDER_MULTIPLE,
                               1 PACK_QTY,
                               h.FROM_LOC_TYPE LOC_TYPE,
                               CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                    WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                    ELSE 3 END SIZE_RANK,
                                    RANK() OVER
                                                (PARTITION BY im2.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                          FROM ${RMS_OWNER}.TSFHEAD h,
                               ${RMS_OWNER}.TSFDETAIL d,
                               ${RMS_OWNER}.ITEM_MASTER im,
                               ${RMS_OWNER}.ITEM_MASTER im2,
                               ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                               ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                               ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                               ${RMS_OWNER}.PRODUCT_CONFIG_OPTIONS pco
                         WHERE h.TSF_NO=d.TSF_NO
                           AND d.ITEM=im.ITEM
                           AND d.ITEM=sif.ITEM(+)
                           AND d.TSF_NO=sif.TSF_NO(+)
                           AND im.STATUS='A'
                           AND im.ITEM_LEVEL=im.TRAN_LEVEL
                           AND vpq.PACK_NO = im2.ITEM
                           AND isc.ITEM= vpq.ITEM
                           AND im2.SIMPLE_PACK_IND = 'Y'
                           AND vpq.PACK_NO = im.ITEM
                           AND h.APPROVAL_DATE IS NOT NULL
                           AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                           AND h.STATUS IN ('A', 'S')
                           AND h.FROM_LOC_TYPE in ('W', 'S')
                           AND NOT EXISTS (SELECT '1'
                                             FROM TRAN_DATA i
                                            WHERE i.dept=im.dept
                                              AND i.CLASS=im.CLASS
                                              AND i.origin=h.from_loc
                                              AND i.destination=h.to_loc)
                           AND NVL(h.DELIVERY_DATE,h.APPROVAL_DATE) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                           AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                           AND im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                   )
             WHERE PACK_RANK = 1
               AND (   SIZE_RANK = 1
                    OR SIZE_RANK = 2
                    OR (    SIZE_RANK = 3
                        AND QTY_RANK  = 1))
                   )
          GROUP BY DAY,
                   LOC,
                   ITEM,
                   ORDER_MULTIPLE,
                   LOC_TYPE
         ]]>
      </PROPERTY>
      <OUTPUT name="formal_items.v"/>
  </OPERATOR>

   <OPERATOR type="filter">
      <INPUT    name="informal_items.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="aip_informal_rej.v"/>
      <OUTPUT   name="aip_informal_filter.v"/>
   </OPERATOR>

      <OPERATOR type="filter">
      <INPUT    name="formal_items.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="formal_items_rej.v"/>
      <OUTPUT   name="formal_items_filter.v"/>
   </OPERATOR>

   <OPERATOR type = "filter">
      <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
      <PROPERTY name = "rejects" value = "true"/>
      <INPUT name = "aip_informal_filter.v"/>
      <OUTPUT name = "informal_items_w.v"/>
      <OUTPUT name = "informal_items_s.v"/>
   </OPERATOR>

   <OPERATOR type = "filter">
      <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
      <PROPERTY name = "rejects" value = "true"/>
      <INPUT name = "formal_items_filter.v"/>
      <OUTPUT name = "formal_items_w.v"/>
      <OUTPUT name = "formal_items_s.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="informal_items_w.v"/>
      <INPUT name="formal_items_w.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC" sourcefield="LOC">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                     <CONVERTFUNCTION name="int64_from_dfloat"/>
                  </CONVERT>
                  <CONVERT destfield="ITEM" sourcefield="ITEM">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>

                  <CONVERT destfield="TSF_RESERVE_QTY" sourcefield="RESERVE_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>

                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE_W}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="informal_items_s.v"/>
      <INPUT name="formal_items_s.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC" sourcefield="LOC">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                     <CONVERTFUNCTION name="int64_from_dfloat"/>
                  </CONVERT>
                  <CONVERT destfield="ITEM" sourcefield="ITEM">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>

                  <CONVERT destfield="TSF_RESERVE_QTY" sourcefield="RESERVE_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>

                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE_S}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type = "filter">
      <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
      <PROPERTY name = "rejects" value = "true"/>
      <INPUT name = "aip_informal_rej.v"/>
      <OUTPUT name = "aip_informal_rej_w.v"/>
      <OUTPUT name = "aip_informal_rej_s.v"/>
   </OPERATOR>

   <OPERATOR type = "filter">
      <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
      <PROPERTY name = "rejects" value = "true"/>
      <INPUT name = "formal_items_rej.v"/>
      <OUTPUT name = "formal_items_rej_w.v"/>
      <OUTPUT name = "formal_items_rej_s.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="aip_informal_rej_w.v"/>
      <INPUT name="formal_items_rej_w.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
           <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE_W}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="aip_informal_rej_s.v"/>
      <INPUT name="formal_items_rej_s.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
           <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE_S}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0

