#!/bin/ksh
###################################################################
#   Purpose:  rmse_aip_wh_cur_inventory.ksh extracts RMS Warehouse
#             Current Inventory data.
#   First  :  extract data of each wh to temp outputfiles
#   Second :  concatenate all temp outputfiles to final outputfiles
###################################################################
# Running syntax: rmse_aip_wh_cur_inventory.ksh {F|D}
# D: single-thread
# F: multi-thread               if ITEM_LOC_SOH is partitioned
#    single-thread and parallel if ITEM_LOC_SOH is NOT partitioned
###################################################################

if [ ${1} = "F" -o ${1} = "D" ]; then
   EXTRACT_TYPE=${1}
else
   echo "Correct usage: rmse_wh_cur_inventory.ksh F|D"
   exit 1
fi

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_wh_cur_inventory"

##########################################################################
## Run config.env and lib.ksh to get necessay environment settings
## Delete STATUS_FILE, assuring to run config.env successfully next time
. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

WH_LIST_FILE=${LOG_DIR}/wh.lst
FINAL_OUTPUT_FILE=${DATA_DIR}/wr1_curinv.txt
FINAL_REJECT_FILE=${DATA_DIR}/rmse_aip_wh_cur_reject_ord_mult.txt


TIME_STAMP=`date +%Y%m%d_%H%M%S`
export PATH=${ORACLE_HOME}/bin:${PATH}

## As a guideline, MAX_THREAD should be set to around twice of CPU count
MAX_THREAD=16

## As a guideline, PARALLEL_DEGREE should be set to around CPU count
PARALLEL_DEGREE=1


run_single_wh()
{
   WH=${1}

   if [ ${EXTRACT_TYPE} = "F" ]; then
      if [ ${WH} = non_part ]; then
         # Uncomment the next line, for clients who want to use parallel
         #  PARALLEL_HINT="/*+ parallel(ils,${PARALLEL_DEGREE}) \*/"
         WH_CLAUSE=" "
         WH=""
      else
         PARALLEL_HINT=" "
         WH_CLAUSE="AND wh.wh = ${WH}"
      fi
   fi

   ####################### INCLUDES ########################
   ##### (this section must come after PROGRAM DEFINES) ####
   ## config.env and lib.ksh have to be run again, as PROGRAM_NAME has been changed

   . ${AIP_HOME}/rfx/etc/rmse_aip_config.env
   . ${LIB_DIR}/rmse_aip_lib.ksh


   ##################  OUTPUT DEFINES ######################
   ######## (this section must come after INCLUDES) ########
   ## T_OUTPUT_FILE is WH-specified
   ## They act as temporary outputfile and will be concatenated to final outputfile
   ## The final outputfile name (FINAL_OUTPUT_FILE) have been specified above

   export OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_wh_cur_inventory.schema
   export T_OUTPUT_FILE=${DATA_DIR}/wr1_curinv${WH}.txt
   export TEMP_TABLE="if_tran_data_temp"
   export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_wh_cur_reject_ord_mult${WH}.txt


   ## Backup previous temp outputfile if exist.
   if [ -s ${T_OUTPUT_FILE} ]
   then
      mv -f ${T_OUTPUT_FILE} ${T_OUTPUT_FILE}_${TIME_STAMP}_bak
   fi

   ## Backup previous temp rejectfile if exist.
   if [ -s ${REJECT_ORD_MULT_FILE} ]
   then
      mv -f ${REJECT_ORD_MULT_FILE} ${REJECT_ORD_MULT_FILE}_${TIME_STAMP}_bak
   fi

   if [[ ${EXTRACT_TYPE} = "F" || ${EXTRACT_TYPE} = "D" ]]
   then
      export PARA1=${EXTRACT_TYPE}
   else
      checkerror -e 2 -m "Option needs to be specified. Valid options are F or D"
   fi

   if [[ ${PARA1} = "F" ]]
   then

      export SQL_STATEMENT2="SELECT ci.WAREHOUSE,
                                    ci.ITEM RMS_SKU,
                                    CASE ci.PRIMARY_CASE_SIZE 
                                       WHEN 'C' THEN rtrim(substr(isc.SUPP_PACK_SIZE,0,6),'.')
                                       WHEN 'I' THEN rtrim(substr(isc.INNER_PACK_SIZE,0,6),'.')
                                       WHEN 'P' THEN rtrim(substr(isc.TI * isc.HI * isc.SUPP_PACK_SIZE,0,6),'.')
                                    ELSE '1' 
                                    END ORDER_MULT,
                                    substr(ci.CSOH,0,8) WH_CUR_INV,
                                    substr(ci.CUSTOMER_BACKORDER,0,8) WH_BO_INV
                               FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                    (SELECT ${PARALLEL_HINT} im.ITEM ITEM,
                                            ils.LOC WAREHOUSE,
                                            ils.STOCK_ON_HAND  - (${UNAVL_COLS})
                                            - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                 FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                      ${RMS_OWNER}.ALLOC_HEADER h,
                                                      ${RMS_OWNER}.ORDHEAD o,
                                                      ${RMS_OWNER}.ORDLOC ol
                                                WHERE h.ITEM = im.ITEM
                                                  AND h.WH = ils.LOC
                                                  AND d.ALLOC_NO = h.ALLOC_NO
                                                  AND o.ORDER_NO = h.ORDER_NO
                                                  AND o.ORDER_NO = ol.ORDER_NO
                                                  AND ol.ITEM = h.ITEM
                                                  AND ol.LOCATION = h.WH
                                                  AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                  AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                  AND o.STATUS = 'A') CSOH,
                                            isup.PRIMARY_CASE_SIZE,
                                            ils.CUSTOMER_BACKORDER 
                                       FROM ${RMS_OWNER}.ITEM_MASTER  im,
                                            ${RMS_OWNER}.ITEM_SUPPLIER isup,
                                            ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                            ${RMS_OWNER}.WH wh
                                      WHERE im.ITEM_LEVEL = im.TRAN_LEVEL
                                        AND im.STATUS = 'A'
                                        AND ils.ITEM = im.ITEM
                                        AND isup.ITEM = im.ITEM
                                        AND isup.PRIMARY_SUPP_IND = 'Y'
                                        AND im.PACK_IND = 'N'
                                        AND im.FORECAST_IND = 'Y'
                                        AND ils.LOC_TYPE = 'W' ${WH_CLAUSE}
                                        AND im.AIP_CASE_TYPE = 'I'
                                        AND wh.WH = ils.LOC
                                        AND im.INVENTORY_IND = 'Y'
                                        AND NOT(im.SELLABLE_IND = 'Y' AND
                                                im.ORDERABLE_IND = 'N'))  ci
                              WHERE isc.ITEM = ci.ITEM
                                AND isc.PRIMARY_SUPP_IND = 'Y'
                                AND isc.PRIMARY_COUNTRY_IND = 'Y'"

      export SQL_STATEMENT3="SELECT WAREHOUSE, 
                                    RMS_SKU, 
                                    rtrim(substr(ORDER_MULT,0,6),'.') ORDER_MULT, 
                                    substr(SUM(WH_CUR_INV),0,8) WH_CUR_INV, 
                                    substr(SUM(WH_BO_INV),0,8) WH_BO_INV
                               FROM 
                                    ( /* When Simple Pack Ind = Y */
                                     SELECT ${PARALLEL_HINT} ils.LOC WAREHOUSE,
                                            im.ITEM RMS_SKU,
                                            vpq.QTY ORDER_MULT,
                                            (TRUNC(ils.STOCK_ON_HAND  - (${UNAVL_COLS})
                                             - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                  FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                       ${RMS_OWNER}.ALLOC_HEADER h,
                                                       ${RMS_OWNER}.ORDHEAD o,
                                                       ${RMS_OWNER}.ORDLOC ol
                                                 WHERE h.ITEM = im.ITEM
                                                   AND h.WH = ils.LOC
                                                   AND d.ALLOC_NO = h.ALLOC_NO
                                                   AND o.ORDER_NO = h.ORDER_NO
                                                   AND o.ORDER_NO = ol.ORDER_NO
                                                   AND ol.ITEM = h.ITEM
                                                   AND ol.LOCATION = h.WH
                                                   AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                   AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                   AND o.STATUS = 'A'), 0) * vpq.QTY) WH_CUR_INV,
                                            ils.CUSTOMER_BACKORDER WH_BO_INV
                                       FROM ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                            ${RMS_OWNER}.ITEM_MASTER  im,
                                            ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                            ${RMS_OWNER}.WH wh,
                                            ${RMS_OWNER}.V_PACKSKU_QTY vpq
                                      WHERE im.STATUS = 'A'
                                        AND ils.ITEM = im.ITEM
                                        AND isc.ITEM = im.ITEM
                                        AND im.ITEM = vpq.PACK_NO
                                        AND isc.PRIMARY_SUPP_IND = 'Y'
                                        AND isc.PRIMARY_COUNTRY_IND = 'Y'
                                        AND im.SIMPLE_PACK_IND ='Y'
                                        AND im.item IN (SELECT pm.pack_no
                                                               FROM ${RMS_OWNER}.item_master im2,
                                                                    ${RMS_OWNER}.packitem pm
                                                              WHERE pm.item = im2.item
                                                                AND im2.forecast_ind = 'Y')
                                        AND ils.loc_type = 'W' ${WH_CLAUSE}
                                        AND wh.wh = ils.loc
                                        AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                        AND im.INVENTORY_IND = 'Y'
                                        AND NOT(im.SELLABLE_IND = 'Y' AND
                                                im.ORDERABLE_IND = 'N')
                                      UNION
                                      /*When regular item with simple pack and other conditions*/
                                     SELECT WAREHOUSE, 
                                            RMS_SKU, 
                                            ORDER_MULT, 
                                            WH_CUR_INV, 
                                            WH_BO_INV 
                                       FROM (
                                             SELECT WAREHOUSE, 
                                                    RMS_SKU, 
                                                    ORDER_MULT, 
                                                    WH_CUR_INV, 
                                                    WH_BO_INV, 
                                                    SIZE_RANK,
                                                    QTY_RANK,
                                                    RANK() OVER
                                                       (PARTITION BY RMS_SKU ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                                              FROM
                                                   ( SELECT ${PARALLEL_HINT} ils.LOC WAREHOUSE,
                                                            im2.ITEM RMS_SKU,
                                                            TRUNC(vpq.QTY, 0) ORDER_MULT,
                                                            (TRUNC(ils.STOCK_ON_HAND  - (0)
                                                             - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                                  FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                                       ${RMS_OWNER}.ALLOC_HEADER h,
                                                                       ${RMS_OWNER}.ORDHEAD o,
                                                                       ${RMS_OWNER}.ORDLOC ol
                                                                 WHERE h.ITEM = im.ITEM
                                                                   AND h.WH = ils.LOC
                                                                   AND d.ALLOC_NO = h.ALLOC_NO
                                                                   AND o.ORDER_NO = h.ORDER_NO
                                                                   AND o.ORDER_NO = ol.ORDER_NO
                                                                   AND ol.ITEM = h.ITEM
                                                                   AND ol.LOCATION = h.WH
                                                                   AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                                   AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                                   AND o.STATUS = 'A'), 0) ) WH_CUR_INV,
                                                            ils.CUSTOMER_BACKORDER WH_BO_INV,
                                                            CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                                 WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                                 ELSE 3 END SIZE_RANK,
                                                                 RANK() OVER
                                                                    (PARTITION BY im.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                       FROM ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                                            ${RMS_OWNER}.ITEM_MASTER  im,
                                                            ${RMS_OWNER}.ITEM_MASTER  im2,
                                                            ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                                            ${RMS_OWNER}.WH wh,
                                                            ${RMS_OWNER}.V_PACKSKU_QTY vpq
                                                      WHERE im.STATUS = 'A'
                                                        AND ils.ITEM = im.ITEM
                                                        AND isc.ITEM = im.ITEM
                                                        AND isc.PRIMARY_SUPP_IND = 'Y'
                                                        AND isc.PRIMARY_COUNTRY_IND = 'Y'
                                                        AND im.ITEM = vpq.ITEM
                                                        AND vpq.PACK_NO = im2.ITEM
                                                        AND im2.SIMPLE_PACK_IND = 'Y'
                                                        AND im.PACK_IND = 'N' 
                                                        AND im.forecast_ind = 'Y'
                                                        AND ils.loc_type = 'W' ${WH_CLAUSE}
                                                        AND wh.wh = ils.loc
                                                        AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                        AND im.INVENTORY_IND = 'Y'
                                                        AND NOT(im.SELLABLE_IND = 'Y' AND
                                                                im.ORDERABLE_IND = 'N')
                                                   )
                                                   )
                                      WHERE PACK_RANK = 1
                                        AND (   SIZE_RANK = 1
                                             OR SIZE_RANK = 2
                                             OR (    SIZE_RANK = 3
                                                 AND QTY_RANK  = 1))
                                    )
                            GROUP BY WAREHOUSE, 
                                     RMS_SKU, 
                                     ORDER_MULT"
   else

      export SQL_STATEMENT2="SELECT ci.WAREHOUSE,
                                    ci.ITEM RMS_SKU,
                                    CASE ci.PRIMARY_CASE_SIZE 
                                       WHEN 'C' THEN rtrim(substr(isc.SUPP_PACK_SIZE,0,6),'.')
                                       WHEN 'I' THEN rtrim(substr(isc.INNER_PACK_SIZE,0,6),'.')
                                       WHEN 'P' THEN rtrim(substr(isc.TI * isc.HI * isc.SUPP_PACK_SIZE,0,6),'.')
                                    ELSE '1' 
                                    END ORDER_MULT,
                                    substr(ci.CSOH,0,8) WH_CUR_INV,
                                    substr(ci.CUSTOMER_BACKORDER,0,8) WH_BO_INV
                               FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                    (SELECT im.ITEM ITEM,
                                            ils.LOC WAREHOUSE,
                                            ils.STOCK_ON_HAND  - (${UNAVL_COLS})
                                            - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                 FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                      ${RMS_OWNER}.ALLOC_HEADER h,
                                                      ${RMS_OWNER}.ORDHEAD o,
                                                      ${RMS_OWNER}.ORDLOC ol
                                                WHERE h.ITEM = im.ITEM
                                                  AND h.WH = ils.LOC
                                                  AND d.ALLOC_NO = h.ALLOC_NO
                                                  AND o.ORDER_NO = h.ORDER_NO
                                                  AND o.ORDER_NO = ol.ORDER_NO
                                                  AND ol.ITEM = h.ITEM
                                                  AND ol.LOCATION = h.WH
                                                  AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                  AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                  AND o.STATUS = 'A') CSOH,
                                            isup.PRIMARY_CASE_SIZE,
                                            ils.CUSTOMER_BACKORDER
                                       FROM ${RMS_OWNER}.ITEM_MASTER  im,
                                            ${RMS_OWNER}.ITEM_SUPPLIER isup,
                                            ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                            ${RMS_OWNER}.WH wh
                                      WHERE im.ITEM_LEVEL = im.TRAN_LEVEL
                                        AND im.STATUS = 'A'
                                        AND ils.ITEM = im.ITEM
                                        AND isup.ITEM = im.ITEM
                                        AND isup.PRIMARY_SUPP_IND = 'Y'
                                        AND im.PACK_IND = 'N'
                                        AND im.FORECAST_IND = 'Y'
                                        AND ils.LOC_TYPE = 'W'
                                        AND im.AIP_CASE_TYPE = 'I'
                                        AND wh.WH = ils.LOC
                                        AND im.INVENTORY_IND = 'Y'
                                        AND NOT(im.SELLABLE_IND = 'Y' AND
                                                im.ORDERABLE_IND = 'N'))  ci,
                                    ${BA_OWNER}.${TEMP_TABLE} tmp
                              WHERE isc.ITEM = ci.ITEM
                                AND isc.PRIMARY_SUPP_IND = 'Y'
                                AND isc.PRIMARY_COUNTRY_IND = 'Y'
                                AND ci.WAREHOUSE = tmp.LOCATION"

      export SQL_STATEMENT3=" SELECT WAREHOUSE, 
                                    RMS_SKU, 
                                    rtrim(substr(ORDER_MULT,0,6),'.') ORDER_MULT, 
                                    substr(SUM(WH_CUR_INV),0,8) WH_CUR_INV, 
                                    substr(SUM(WH_BO_INV),0,8) WH_BO_INV
                               FROM 
                                    ( /* When Simple Pack Ind = Y */
                                     SELECT ils.LOC WAREHOUSE, im.ITEM RMS_SKU,
                                            TRUNC(vpq.QTY,0) ORDER_MULT,
                                            (TRUNC(ils.STOCK_ON_HAND  - (${UNAVL_COLS})
                                             - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                  FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                       ${RMS_OWNER}.ALLOC_HEADER h,
                                                       ${RMS_OWNER}.ORDHEAD o,
                                                       ${RMS_OWNER}.ORDLOC ol
                                                 WHERE h.ITEM = im.ITEM
                                                   AND h.WH = ils.LOC
                                                   AND d.ALLOC_NO = h.ALLOC_NO
                                                   AND o.ORDER_NO = h.ORDER_NO
                                                   AND o.ORDER_NO = ol.ORDER_NO
                                                   AND ol.ITEM = h.ITEM
                                                   AND ol.LOCATION = h.WH
                                                   AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                   AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                   AND o.STATUS = 'A'), 0) * vpq.QTY) WH_CUR_INV,
                                            ils.CUSTOMER_BACKORDER WH_BO_INV
                                       FROM ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                            ${RMS_OWNER}.ITEM_MASTER  im,
                                            ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                            ${RMS_OWNER}.WH wh,
                                            ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                            ${BA_OWNER}.${TEMP_TABLE} tmp
                                      WHERE im.STATUS = 'A'
                                        AND ils.ITEM = im.ITEM
                                        AND isc.ITEM = im.ITEM
                                        AND isc.PRIMARY_SUPP_IND = 'Y'
                                        AND isc.PRIMARY_COUNTRY_IND = 'Y'
                                        AND vpq.PACK_NO = im.ITEM
                                        AND im.SIMPLE_PACK_IND ='Y'
                                        AND im.item IN (SELECT pm.pack_no
                                                               FROM ${RMS_OWNER}.item_master im2,
                                                                    ${RMS_OWNER}.packitem pm
                                                              WHERE pm.item = im2.item
                                                                AND im2.forecast_ind = 'Y')
                                        AND ils.loc_type = 'W'
                                        AND wh.wh = ils.loc
                                        AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                        AND ils.LOC = tmp.LOCATION
                                        AND im.INVENTORY_IND = 'Y'
                                        AND NOT(im.SELLABLE_IND = 'Y' AND
                                                im.ORDERABLE_IND = 'N')
                                    UNION
                                   SELECT  WAREHOUSE, 
                                           RMS_SKU, 
                                           ORDER_MULT, 
                                           WH_CUR_INV, 
                                           WH_BO_INV 
                                     FROM
                                           ( SELECT WAREHOUSE, 
                                                    RMS_SKU, 
                                                    ORDER_MULT, 
                                                    WH_CUR_INV, 
                                                    WH_BO_INV, 
                                                    SIZE_RANK,
                                                    QTY_RANK,
                                                    RANK() OVER
                                                       (PARTITION BY RMS_SKU ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                                               FROM (
                                                       SELECT ils.LOC WAREHOUSE, im2.ITEM RMS_SKU,
                                                               TRUNC(vpq.QTY,0) ORDER_MULT,
                                                        (TRUNC(ils.STOCK_ON_HAND  - (${UNAVL_COLS})
                                                         - (SELECT NVL(SUM(d.QTY_DISTRO),0)
                                                              FROM ${RMS_OWNER}.ALLOC_DETAIL d,
                                                                   ${RMS_OWNER}.ALLOC_HEADER h,
                                                                   ${RMS_OWNER}.ORDHEAD o,
                                                                   ${RMS_OWNER}.ORDLOC ol
                                                             WHERE h.ITEM = im.ITEM
                                                               AND h.WH = ils.LOC
                                                               AND d.ALLOC_NO = h.ALLOC_NO
                                                               AND o.ORDER_NO = h.ORDER_NO
                                                               AND o.ORDER_NO = ol.ORDER_NO
                                                               AND ol.ITEM = h.ITEM
                                                               AND ol.LOCATION = h.WH
                                                               AND NVL(ol.QTY_RECEIVED,0) < NVL(ol.QTY_ORDERED,0)
                                                               AND NVL(d.QTY_TRANSFERRED,0) < NVL(d.QTY_ALLOCATED,0)
                                                               AND o.STATUS = 'A'), 0) * DECODE (im.SIMPLE_PACK_IND,
                                                                                                        'Y',
                                                                                                        vpq.QTY,
                                                                                                        1)) WH_CUR_INV,
                                                        ils.CUSTOMER_BACKORDER WH_BO_INV,
                                                        CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                             WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                             ELSE 3 END SIZE_RANK,
                                                             RANK() OVER
                                                                (PARTITION BY im.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                   FROM ${RMS_OWNER}.ITEM_LOC_SOH ils,
                                                        ${RMS_OWNER}.ITEM_MASTER  im,
                                                        ${RMS_OWNER}.ITEM_MASTER  im2,
                                                        ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                                        ${RMS_OWNER}.WH wh,
                                                        ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                                        ${BA_OWNER}.${TEMP_TABLE} tmp
                                                  WHERE im.STATUS = 'A'
                                                    AND ils.ITEM = im.ITEM
                                                    AND isc.ITEM = im.ITEM
                                                    AND isc.PRIMARY_SUPP_IND = 'Y'
                                                    AND isc.PRIMARY_COUNTRY_IND = 'Y'
                                                    AND im.ITEM = vpq.ITEM
                                                    AND vpq.PACK_NO = im2.ITEM
                                                    AND im2.SIMPLE_PACK_IND = 'Y'
                                                    AND im.PACK_IND = 'N' 
                                                    AND im.forecast_ind = 'Y'
                                                    AND ils.loc_type = 'W'
                                                    AND wh.wh = ils.loc
                                                    AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                    AND ils.LOC = tmp.LOCATION
                                                    AND im.INVENTORY_IND = 'Y'
                                                    AND NOT(im.SELLABLE_IND = 'Y' AND
                                                            im.ORDERABLE_IND = 'N')
                                                   )
                                           )
                                    WHERE PACK_RANK = 1
                                      AND (   SIZE_RANK = 1
                                           OR SIZE_RANK = 2
                                           OR (    SIZE_RANK = 3
                                               AND QTY_RANK  = 1))
                                    )
                            GROUP BY WAREHOUSE, 
                                     RMS_SKU, 
                                     ORDER_MULT"
   fi

   message "Program started (wh:${WH}) ..."

   FLOW_FILE=$LOG_DIR/$PROGRAM_NAME${WH}.xml

   cat > $FLOW_FILE << EOF

   <FLOW name = "${PROGRAM_NAME}_${WH}.flw">

   <!--
   ########################################################################
       File # 24: Warehouse Current Inventory - Non-formal Pack Items:
   ########################################################################
   -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT2}
         ]]>
      </PROPERTY>
      <OUTPUT name="wh_nfp_inv.v"/>
   </OPERATOR>

   <OPERATOR type = "hash">
      <INPUT name = "wh_nfp_inv.v"/>
      <PROPERTY name = "key" value = "WAREHOUSE"/>
      <PROPERTY name = "key" value = "RMS_SKU"/>
      <PROPERTY name = "key" value = "ORDER_MULT"/>
      <PROPERTY name = "key" value = "WH_CUR_INV"/>
      <PROPERTY name = "key" value = "WH_BO_INV"/>
      <OPERATOR type = "sort">
         <PROPERTY name = "key" value = "WAREHOUSE"/>
         <PROPERTY name = "key" value = "RMS_SKU"/>
         <PROPERTY name = "key" value = "ORDER_MULT"/>
         <PROPERTY name = "key" value = "WH_CUR_INV"/>
         <PROPERTY name = "key" value = "WH_BO_INV"/>
         <OPERATOR type = "removedup">
            <PROPERTY name = "key" value = "WAREHOUSE"/>
            <PROPERTY name = "key" value = "RMS_SKU"/>
            <PROPERTY name = "key" value = "ORDER_MULT"/>
            <PROPERTY name = "keep" value = "last"/>
            <OUTPUT name = "wh_nfp_inv4.v"/>
       </OPERATOR>
         </OPERATOR>
      </OPERATOR>

      <OPERATOR type="filter">
         <INPUT    name="wh_nfp_inv4.v"/>
         <PROPERTY name="filter" value="ORDER_MULT GT 999999"/>
         <PROPERTY name="rejects" value="true"/>
         <OUTPUT   name="wh_nfp_inv4_reject.v"/>
         <OUTPUT   name="wh_nfp_inv4_filter.v"/>
      </OPERATOR>

      <!--  Convert fields to format required by the output schema:  -->

      <OPERATOR type="convert">
         <INPUT name="wh_nfp_inv4_filter.v" />
         <PROPERTY name="convertspec">
       <![CDATA[
          <CONVERTSPECS>
             <CONVERT destfield="WAREHOUSE" sourcefield="WAREHOUSE" newtype="int64">
           <CONVERTFUNCTION name="make_not_nullable">
              <FUNCTIONARG name="nullvalue" value="0"/>
           </CONVERTFUNCTION>
             </CONVERT>
             <CONVERT destfield="RMS_SKU" sourcefield="RMS_SKU" newtype="string">
           <CONVERTFUNCTION name="make_not_nullable">
              <FUNCTIONARG name="nullvalue" value="0"/>
           </CONVERTFUNCTION>
             </CONVERT>
             
             <CONVERT destfield="ORDER_MULT" sourcefield="ORDER_MULT" newtype="string">
           <CONVERTFUNCTION name="make_not_nullable">
              <FUNCTIONARG name="nullvalue" value="0"/>
           </CONVERTFUNCTION>
             </CONVERT>
             <CONVERT destfield="WH_CUR_INV" sourcefield="WH_CUR_INV" newtype="string">
           <CONVERTFUNCTION name="make_not_nullable">
              <FUNCTIONARG name="nullvalue" value="0"/>
           </CONVERTFUNCTION>
             </CONVERT>
             <CONVERT destfield="WH_BO_INV" sourcefield="WH_BO_INV" newtype="string">
           <CONVERTFUNCTION name="make_not_nullable">
              <FUNCTIONARG name="nullvalue" value="0"/>
           </CONVERTFUNCTION>
             </CONVERT>
          </CONVERTSPECS>
       ]]>
      </PROPERTY>
      <OUTPUT name="wh_nfp_2_mod.v"/>
   </OPERATOR>

   <!--
   #####################################################################
       File # 24: Warehouse Current Inventory - Formal Pack Items:
   #####################################################################
   -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT3}
         ]]>
      </PROPERTY>
      <OUTPUT name="wh_fp_inv.v"/>
   </OPERATOR>
   
   <OPERATOR type = "funnel">
      <INPUT name = "wh_fp_inv.v"/>
      <OUTPUT name = "wh_nfp_inv1.v"/>
   </OPERATOR>
   
   <OPERATOR type="filter">
      <INPUT    name="wh_nfp_inv1.v"/>
      <PROPERTY name="filter" value="ORDER_MULT GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="wh_fp_inv_reject.v"/>
      <OUTPUT   name="wh_fp_inv_filter.v"/>
   </OPERATOR>

   <!--  Convert fields to format required by the output schema:  -->

   <OPERATOR type="convert">
      <INPUT name="wh_fp_inv_filter.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
                 <CONVERT destfield="WAREHOUSE" sourcefield="WAREHOUSE" newtype="int64">
               <CONVERTFUNCTION name="make_not_nullable">
                  <FUNCTIONARG name="nullvalue" value="0"/>
               </CONVERTFUNCTION>
                 </CONVERT>
                 <CONVERT destfield="RMS_SKU" sourcefield="RMS_SKU" newtype="string">
               <CONVERTFUNCTION name="make_not_nullable">
                  <FUNCTIONARG name="nullvalue" value="0"/>
               </CONVERTFUNCTION>
                 </CONVERT>
                 
               <CONVERT destfield="ORDER_MULT" sourcefield="ORDER_MULT" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="WH_CUR_INV" sourcefield="WH_CUR_INV" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="WH_BO_INV" sourcefield="WH_BO_INV" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                      <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="wh_fp_inv_cvt.v" />
   </OPERATOR>

   <!--  Combine the warehouse current inventory datasets into one dataset:  -->

   <OPERATOR  type="collect" >
      <INPUT name="wh_nfp_2_mod.v" />     <!--  Non-formal pack items (rem data) -->
      <INPUT name="wh_fp_inv_cvt.v" />    <!--  Formal pack items  -->
      <OUTPUT name="wh_all_cur_inv.v" />  <!--  Combination of all four  -->
   </OPERATOR>

   <!--  Keep only the records where WH_CUR_INV > 0:  -->

   <OPERATOR type="filter" >
      <PROPERTY  name="filter" value="WH_CUR_INV LT 0" />
      <PROPERTY name="rejects" value="true"/>
      <INPUT name="wh_all_cur_inv.v"/>
      <OUTPUT name="wh_all_inv_lt_zero.v"/>
      <OUTPUT name="wh_all_inv_ge_zero.v"/>
   </OPERATOR>

   <OPERATOR type="fieldmod">
      <INPUT name="wh_all_inv_lt_zero.v"/>
      <PROPERTY name="drop" value="WH_CUR_INV"/>
      <OPERATOR  type="generator">
         <PROPERTY  name="schema">
            <![CDATA[
               <GENERATE>
                  <FIELD name="WH_CUR_INV" type="string" len="8">
                     <CONST value="0"/>
                  </FIELD>
               </GENERATE>
            ]]>
         </PROPERTY>
         <OPERATOR type="funnel">
            <INPUT name="wh_all_inv_ge_zero.v"/>
            <OUTPUT name="wh_cur_inv_non_zero.v"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <!--  Keep only the records where WH_BO_INV > 0:  -->

   <OPERATOR type="filter" >
      <PROPERTY  name="filter" value="WH_BO_INV LT 0" />
      <PROPERTY name="rejects" value="true"/>
      <INPUT name="wh_cur_inv_non_zero.v"/>
      <OUTPUT name="wh_bo_inv_lt_zero.v"/>
      <OUTPUT name="wh_bo_inv_ge_zero.v"/>
   </OPERATOR>

   <OPERATOR type="fieldmod">
      <INPUT name="wh_bo_inv_lt_zero.v"/>
      <PROPERTY name="drop" value="WH_BO_INV"/>
      <OPERATOR  type="generator">
         <PROPERTY  name="schema">
            <![CDATA[
               <GENERATE>
                  <FIELD name="WH_BO_INV" type="string" len="8">
                     <CONST value="0"/>
                  </FIELD>
               </GENERATE>
            ]]>
         </PROPERTY>
         <OPERATOR type="funnel">
            <INPUT name="wh_bo_inv_ge_zero.v"/>
            <OUTPUT name="wh_all_inv_non_zero.v"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>
   
   <OPERATOR type="export">
      <INPUT    name="wh_all_inv_non_zero.v"/>
      <PROPERTY name="outputfile" value="${T_OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

   <OPERATOR  type="collect" >
      <INPUT name="wh_fp_inv_reject.v" />
      <INPUT name="wh_nfp_inv4_reject.v" />
      <OUTPUT name="final_reject.v" />
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT name="final_reject.v"/>
      <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>
   
   </FLOW>

EOF

   $RETL_EXE $RETL_OPTIONS -f $FLOW_FILE

   #  Check for RETL flow errors:

   checkerror -e $? -m "Program failed - check ${ERR_FILE}"

   #  Remove the status file:

   if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

   message "Program completed successfully (wh:${WH})"

   #  Cleanup and exit:

   rmse_terminate 0
}

get_wh_list()
{
   sqlplus -s $SQLPLUS_LOGON <<!
   set pages 0
   set heading off
   set feedback off
   SELECT DISTINCT WH
     FROM ${RMS_OWNER}.WH
    ORDER BY WH;
   exit;
!
}

get_partname()
{
   message "Getting partition name..."
   sqlplus -s $SQLPLUS_LOGON <<!
   set pages 0
   set feedback off
   set heading off
   SELECT PARTITION_NAME, PARTITION_POSITION
     FROM SYS.DBA_TAB_PARTITIONS
    WHERE TABLE_NAME='ITEM_LOC_SOH'
      AND TABLE_OWNER=${RMS_OWNER}
    ORDER BY PARTITION_POSITION;
   exit;
!
}

if [ ${EXTRACT_TYPE} = "D" ]; then

   run_single_wh

else

   NO_OF_PART=`get_partname |wc -l`

   if [ ${NO_OF_PART} -eq 0 ]; then
      ## single-threading, but parallel
      run_single_wh non_part
   else

      ###############################################################
      ## Start multi-thread running of wh_cur_inventory
      ###############################################################

      ## Remove old wh.lst if exists.
      if [ -s ${WH_LIST_FILE} ]; then
         rm -f ${WH_LIST_FILE}
      fi

      ## Loop through all warehouse
      get_wh_list |while read WHNO
      do
         echo ${WHNO} >> ${WH_LIST_FILE}

         run_single_wh ${WHNO} &

         sleep 1
         TASK_RUNNING=`ps -ef|grep ${PROGRAM_NAME}|grep -v grep|wc -l`
         while [ ${TASK_RUNNING} -ge ${MAX_THREAD} ]
         do
            sleep 5
            TASK_RUNNING=`ps -ef|grep ${PROGRAM_NAME}|grep -v grep|wc -l`
         done
      done

      ###############################################################
      ## Concatenate all temp output files into final output files
      ###############################################################

      ## Wait until all thread finish before concatenating
      wait

      message "Concatenation started ..."

      ## Backup previous outputfile if exist.
      if [ -s ${FINAL_OUTPUT_FILE} ]; then
         mv -f ${FINAL_OUTPUT_FILE} ${FINAL_OUTPUT_FILE}_${TIME_STAMP}_bak
      fi

      ## Backup previous REJECTfile if exist.
      if [ -s ${FINAL_REJECT_FILE} ]; then
         mv -f ${FINAL_REJECT_FILE} ${FINAL_REJECT_FILE}_${TIME_STAMP}_bak
      fi

      cat ${WH_LIST_FILE} |while read WH
      do

         T_OUTPUT_FILE=${DATA_DIR}/wr1_curinv${WH}.txt
         REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_wh_cur_reject_ord_mult${WH}.txt
         cat ${T_OUTPUT_FILE} >> ${FINAL_OUTPUT_FILE}
         cat ${REJECT_ORD_MULT_FILE} >> ${FINAL_REJECT_FILE}

         if [ $? -eq 0 ]
         then
            rm -f ${T_OUTPUT_FILE}
            rm -f ${REJECT_ORD_MULT_FILE}
         else
            message "Failed to concatenate: ${T_OUTPUT_FILE}!"
            mv -f ${T_OUTPUT_FILE} ${T_OUTPUT_FILE}_${TIME_STAMP}_failed
            exit 1
         fi

      done

      message "Concatenation completed successfully!"

   fi
fi
