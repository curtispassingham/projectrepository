#!/bin/ksh 
###################################################################
#   Purpose: rmse_aip_store_cur_inventory.ksh extracts
#            RMS Store Current Inventory data.
###################################################################
# Running syntax: rmse_aip_store_cur_inventory.ksh {F|D}
# D: single-thread
# F: multi-thread               if ITEM_LOC_SOH is partitioned
#    single-thread and parallel if ITEM_LOC_SOH is NOT partitioned
###################################################################
  
if [ ${1} = "F" -o ${1} = "D" ]; then 
  EXTRACT_TYPE=${1}
else
  echo "Correct usage: rmse_aip_store_cur_inventory.ksh F|D"
  exit 1
fi

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_store_cur_inventory"
  
###########################################################################
## Run config.env and lib.ksh to get necessay environment settings
## Delete STATUS_FILE, assuring to run rmse_aip_config.env successfully next time
. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

export PATH=${ORACLE_HOME}/bin:${PATH}
TIME_STAMP=`date +%Y%m%d_%H%M%S`

## As a guideline, MAX_THREAD should be set to around twice of CPU count
MAX_THREAD=16

## As a guideline, PARALLEL_DEGREE should be set to around CPU count
PARALLEL_DEGREE=1

run_single_part()
{
  export THREAD_NO=${1} # input variable by calling script to determine query partition
  export PARTNAME=${2}

  if [ ${EXTRACT_TYPE} = "F" ]; then
    if [ ${PARTNAME} = non_part ]; then
    # Uncomment the next line, for clients who want to use parallel
    # PARALLEL_HINT="/*+ parallel(il,${PARALLEL_DEGREE}) \*/"
      PART_CLAUSE=" "
    else
      PARALLEL_HINT=" "
      PART_CLAUSE=" PARTITION (${PARTNAME}) "
    fi
  fi

  ####################### INCLUDES ########################
  ##### (this section must come after PROGRAM DEFINES) ####
  
  . ${AIP_HOME}/rfx/etc/rmse_aip_config.env
  . ${LIB_DIR}/rmse_aip_lib.ksh
  
  ##################  OUTPUT DEFINES ######################
  ######## (this section must come after INCLUDES) ########
  
  export OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_store_cur_inventory.schema
  export TEMP_TABLE="if_tran_data_temp"
  export PART_THREADS=256 # number of partitions
  export PARALLEL=1 # number of Java threads
  export RETL_OPTIONS="${RETL_OPTIONS} -n ${PARALLEL}"
  export OUTPUT_FILE=${DATA_DIR}/sr0_curinv_${THREAD_NO}.txt
  
  ## Backup previous output files if exists
  if [ -s ${OUTPUT_FILE} ]; then
    mv -f ${OUTPUT_FILE} ${OUTPUT_FILE}_${TIME_STAMP}_bak
  fi

  message "Program started (thread:${THREAD_NO}) ..."

  if [[ ${EXTRACT_TYPE} = "F" || ${EXTRACT_TYPE} = "D" ]]
  then
    export PARA1=${EXTRACT_TYPE}
  else
    checkerror -e 2 -m "Option needs to be specified. Valid options are F or D"
  fi

  if [[ ${PARA1} = "D" ]]
  then

  message "Update inv_resv_update_temp table"
  
  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_1"
  
  cat > ${FLOW_FILE}.xml << EOF
  <FLOW name = "${PROGRAM_NAME}_1.flw">
  
  ${DBREAD}
    <PROPERTY name = "query">
       <![CDATA[
        SELECT 'N' PROCESS_STATUS 
          FROM DUAL
        ]]>
        </PROPERTY>
        <OUTPUT name="update_inv_resv_update_temp.v"/>
        </OPERATOR>
        ${DBPREPSTMT}
           <INPUT    name="update_inv_resv_update_temp.v"/>
           <PROPERTY name="tablename" value="${RMS_OWNER}.INV_RESV_UPDATE_TEMP"/>
           <PROPERTY name="statement" value="UPDATE ${RMS_OWNER}.INV_RESV_UPDATE_TEMP 
                                                SET PROCESS_STATUS = 'I', 
                                                    LAST_UPDATE_DATETIME = SYSDATE, 
                                                    LAST_UPDATE_ID = USER 
                                              WHERE PROCESS_STATUS = ?"/>
           <PROPERTY name="fields" value="PROCESS_STATUS"/>
        </OPERATOR>
  
  </FLOW>
EOF

  ${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

  checkerror -e $? -m "Program failed - check ${ERR_FILE}"

  message "Create temp table ${TEMP_TABLE}"
  
  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_1"
  
  cat > ${FLOW_FILE}.xml << EOF
  <FLOW name = "${PROGRAM_NAME}_1.flw">
  
  ${DBREAD}
    <PROPERTY name = "query">
       <![CDATA[
        SELECT LOCATION,
               LOC_TYPE
          FROM (
                SELECT LOCATION,
                       LOC_TYPE
                  FROM ${RMS_OWNER}.IF_TRAN_DATA
                UNION 
                SELECT LOCATION,
                       LOC_TYPE
                  FROM ${RMS_OWNER}.INV_RESV_UPDATE_TEMP
                 WHERE PROCESS_STATUS = 'I'
                )
        ]]>
        </PROPERTY>
        ${DBWRITE_TEMP}
           <PROPERTY name = "table" value = "${TEMP_TABLE}"/>
        </OPERATOR>
     </OPERATOR>
  
  </FLOW>
EOF
  ${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml
  
  checkerror -e $? -m "Program failed - check ${ERR_FILE}"

  message "Delete from inv_resv_update_temp table"
  
  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_1"
  
  cat > ${FLOW_FILE}.xml << EOF
  <FLOW name = "${PROGRAM_NAME}_1.flw">
  
  ${DBREAD}
    <PROPERTY name = "query">
       <![CDATA[
        SELECT 'I' PROCESS_STATUS 
          FROM DUAL
        ]]>
        </PROPERTY>
        <OUTPUT name="delete_inv_resv_update_temp.v"/>
        </OPERATOR>
        ${DBPREPSTMT}
           <INPUT    name="delete_inv_resv_update_temp.v"/>
           <PROPERTY name="tablename" value="${RMS_OWNER}.INV_RESV_UPDATE_TEMP"/>
           <PROPERTY name="statement" value="DELETE FROM ${RMS_OWNER}.INV_RESV_UPDATE_TEMP WHERE PROCESS_STATUS = ?"/>
           <PROPERTY name="fields" value="PROCESS_STATUS"/>
        </OPERATOR>
  
  </FLOW>
EOF
  ${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml
  
  checkerror -e $? -m "Program failed - check ${ERR_FILE}"

  message "Analyze temp table ${TEMP_TABLE}"
  analyze_tbl "${BA_OWNER}.${TEMP_TABLE}"
  
  
  export SQL_STATEMENT="<PROPERTY name = \"query\">
      <![CDATA[
    SELECT il.LOC STORE,
           im.ITEM RMS_SKU,
           CASE WHEN (il.STOCK_ON_HAND - (${UNAVL_COLS}) >= 0)
                THEN substr(il.STOCK_ON_HAND - (${UNAVL_COLS}),0,8)
                ELSE '0'
           END AS STORE_CUR_INV,
           CASE WHEN (il.CUSTOMER_BACKORDER >= 0)
                THEN substr(il.CUSTOMER_BACKORDER,0,8)
                ELSE '0'
           END AS BACKORDER_QUANTITY
      FROM ${RMS_OWNER}.ITEM_MASTER  im,
           ${RMS_OWNER}.ITEM_LOC_SOH il,
           ${RMS_OWNER}.STORE s,
           ${BA_OWNER}.${TEMP_TABLE} tmp
     WHERE im.ITEM_LEVEL = im.TRAN_LEVEL
       AND im.STATUS = 'A'
       AND il.ITEM = im.ITEM
       AND im.PACK_IND = 'N' 
       AND im.FORECAST_IND = 'Y'
       AND il.LOC_TYPE = 'S'
       AND tmp.LOC_TYPE = il.LOC_TYPE
       AND il.LOC = s.STORE
       AND il.LOC = tmp.LOCATION
       AND s.STORE_OPEN_DATE <= TO_DATE('${VDATE}', 'YYYYMMDD')
       AND NVL(s.STORE_CLOSE_DATE,'04-APR-4444')
           >= TO_DATE('${VDATE}', 'YYYYMMDD')
       AND im.INVENTORY_IND = 'Y'
       AND NOT(im.SELLABLE_IND = 'Y' AND
               im.ORDERABLE_IND = 'N')
       AND s.STOCKHOLDING_IND = 'Y'                 
           
       ]]>
    </PROPERTY>"
  else
  export SQL_STATEMENT="<PROPERTY name = \"query\">
      <![CDATA[
    SELECT ${PARALLEL_HINT} il.LOC STORE,
           im.ITEM RMS_SKU,
           CASE WHEN (il.STOCK_ON_HAND - (${UNAVL_COLS}) >= 0)
                THEN substr(il.STOCK_ON_HAND - (${UNAVL_COLS}),0,8)
                ELSE '0'
           END AS STORE_CUR_INV,
           CASE WHEN (il.CUSTOMER_BACKORDER >= 0)
                THEN substr(il.CUSTOMER_BACKORDER,0,8)
                ELSE '0'
           END AS BACKORDER_QUANTITY
      FROM ${RMS_OWNER}.ITEM_MASTER  im,
           ${RMS_OWNER}.ITEM_LOC_SOH ${PART_CLAUSE} il,
           ${RMS_OWNER}.STORE s
     WHERE im.ITEM_LEVEL = im.TRAN_LEVEL
       AND im.STATUS = 'A'
       AND il.ITEM = im.ITEM
       AND im.PACK_IND = 'N' 
       AND im.FORECAST_IND = 'Y'
       AND il.LOC_TYPE = 'S'
       AND il.LOC = s.STORE
       AND s.STORE_OPEN_DATE <= TO_DATE('${VDATE}', 'YYYYMMDD')
       AND NVL(s.STORE_CLOSE_DATE,'04-APR-4444')
           >= TO_DATE('${VDATE}', 'YYYYMMDD')          
       AND im.INVENTORY_IND = 'Y'
       AND NOT(im.SELLABLE_IND = 'Y' AND
               im.ORDERABLE_IND = 'N')
       AND s.STOCKHOLDING_IND = 'Y'                 
       ]]>
   </PROPERTY>"
  
fi
  
  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_${THREAD_NO}"
  
  cat > ${FLOW_FILE}.xml << EOF
  <FLOW name = "${PROGRAM_NAME}_${THREAD_NO}.flw">
  
  <!--  File # 11: Store Current Inventory - Non-Pack and
                   Non-formal Pack Items:                         -->
  ${DBREAD}
    <PROPERTY name="numpartitions" value="${PARALLEL}"/>
    <PROPERTY name="sp_prequery" value="EXEC ALTER_AIP_SESS"/>
       ${SQL_STATEMENT}
          <OUTPUT name="store_inv_data.v"/>
       </OPERATOR>     
     <OPERATOR type="export">
        <INPUT    name="store_inv_data.v"/>
        <PROPERTY name="parallel" value="true"/>
        <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
     </OPERATOR>
  
  </FLOW>
  
EOF

  ${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml
  #  Check for errors:

  checkerror -e $? -m "Program failed - check ${ERR_FILE}"

  #  Remove the status file:
  
  if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

  message "Program completed successfully (thread:${THREAD_NO})"
  
  #  Cleanup and exit:
  
  rmse_terminate 0
} 

get_partname()
{
  message "Getting partition name..."
  sqlplus -s $SQLPLUS_LOGON <<!
  set pages 0
  set feedback off
  set heading off
  SELECT PARTITION_NAME||chr(32)||PARTITION_POSITION
      FROM  SYS.DBA_TAB_PARTITIONS
      WHERE TABLE_NAME='ITEM_LOC_SOH'
      AND TABLE_OWNER='${RMS_OWNER}'
      ORDER BY PARTITION_POSITION;
  exit;
!
}

####################################################
## Start multiple rmse_store_cur_inventory thread ##
## until overall thread# reachs MAX_THREAD        ##
####################################################

if [ ${EXTRACT_TYPE} = "D" ]; then
  run_single_part 1 delta

else
  NO_OF_PART=`get_partname |wc -l`

  if [ ${NO_OF_PART} -eq 0 ]; then
    ## single-threading, but parallel
    run_single_part 1 non_part
  else
    get_partname |while read PARTNAME THREAD_SEQ 
    do
       run_single_part ${THREAD_SEQ} ${PARTNAME} &

       sleep 1
       TASKS_RUNNING=`ps -ef |grep ${PROGRAM_NAME} |grep -v grep |wc -l`
       while [ ${TASKS_RUNNING} -ge ${MAX_THREAD} ]; do
          sleep 5
          TASKS_RUNNING=`ps -ef |grep ${PROGRAM_NAME} |grep -v grep |wc -l`
       done
    done
  fi
fi

wait