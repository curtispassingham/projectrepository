#!/bin/ksh
########################################################
# Purpose:  Extracts RMS item, pack, supplier and
#           supplier pack size information
########################################################

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_item_master"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

export OUTPUT_PURGE_ITEM_FILE=${DATA_DIR}/rmse_aip_purged_item.dat
export OUTPUT_PURGE_ITEM_SCHEMA=${SCHEMA_DIR}/rmse_aip_purged_item.schema

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT im.ITEM,
                      REPLACE(RTRIM(im.ITEM_DESC),CHR(10),'') ITEM_DESC,
                      im.ITEM_PARENT,
                      im.ITEM_GRANDPARENT,
                      NVL(p.ITEM, im.ITEM) AIP_SKU,
                      im.SUBCLASS,
                      im.CLASS,
                      im.DEPT,
                      im.FORECAST_IND,
                      isup.SUPPLIER,
                      isup.PRIMARY_SUPP_IND,
                      im.STANDARD_UOM,
                      ul.UOM_DESC_TRANS STANDARD_UOM_DESCRIPTION,
                      NVL(im.HANDLING_TEMP, 0) SKU_TYPE,
                      NVL(cd.CODE_DESC, 0) SKU_TYPE_DESCRIPTION,
                      im.PACK_IND,
                      im.SIMPLE_PACK_IND,
                      substr(NVL(p.QTY,0),0,6) PACK_QUANTITY,
                      im.ITEM_LEVEL,
                      im.TRAN_LEVEL,
                      im.RETAIL_LABEL_TYPE,
                      im.CATCH_WEIGHT_IND,
                      im.SELLABLE_IND,
                      im.ORDERABLE_IND,
                      im.DEPOSIT_ITEM_TYPE
                 FROM ${RMS_OWNER}.ITEM_MASTER im,
                      ${RMS_OWNER}.V_PACKSKU_QTY p,
                      ${RMS_OWNER}.ITEM_SUPPLIER isup,
                      ${RMS_OWNER}.UOM_CLASS_TL ul,
                      ${RMS_OWNER}.CODE_DETAIL cd
                WHERE im.ITEM = isup.ITEM
                  AND im.ITEM = p.PACK_NO (+)
                  AND im.STANDARD_UOM=ul.UOM
                  AND ul.LANG = get_primary_lang
                  AND im.HANDLING_TEMP=cd.CODE(+)
                  AND im.STATUS='A'
                  AND im.INVENTORY_IND ='Y'
                  AND im.ORDERABLE_IND ='Y'
                  AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                       OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                                      FROM ${RMS_OWNER}.ITEM_MASTER im1,
                                                                           ${RMS_OWNER}.PACKITEM pm
                                                                     WHERE pm.item = im1.item
                                                                       AND im1.forecast_ind = 'Y'
                                                                       AND im1.aip_case_type = 'F')))
            ]]>
         </PROPERTY>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="ITEM_DESC" sourcefield="ITEM_DESC">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="AIP_SKU" sourcefield="AIP_SKU">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="PACK_QUANTITY" sourcefield="PACK_QUANTITY" newtype="string">
                        <CONVERTFUNCTION name="default">
                           <FUNCTIONARG name="nullable" value="true"/>
                        </CONVERTFUNCTION>
                     </CONVERT>                    
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OPERATOR type="export">
               <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
               <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT KEY_VALUE ITEM
                 FROM ${RMS_OWNER}.DAILY_PURGE dp,
                      ${RMS_OWNER}.ITEM_MASTER im,
            ${RMS_OWNER}.V_PACKSKU_QTY p,
            ${RMS_OWNER}.ITEM_SUPPLIER isup,
            ${RMS_OWNER}.UOM_CLASS uc,
                      ${RMS_OWNER}.CODE_DETAIL cd
                WHERE dp.TABLE_NAME = 'ITEM_MASTER'
                  AND dp.KEY_VALUE = im.ITEM
        AND im.ITEM = isup.ITEM
        AND im.ITEM = p.PACK_NO (+)
        AND im.STANDARD_UOM=uc.UOM
        AND im.HANDLING_TEMP=cd.CODE(+)
        AND im.STATUS='A'
        AND im.INVENTORY_IND ='Y'
        AND im.ORDERABLE_IND ='Y'
        AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
              OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                             FROM ${RMS_OWNER}.ITEM_MASTER im1,
                                                                  ${RMS_OWNER}.PACKITEM pm
                                                            WHERE pm.item = im1.item
                                                              AND im1.forecast_ind = 'Y'
                                                                        AND im1.aip_case_type = 'F')))
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_PURGE_ITEM_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_PURGE_ITEM_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>

   </FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
