#!/bin/ksh
########################################################
# Purpose: extracts RMS off/on sale data and produce two
#          data files for AIP load
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_item_sale"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export ON_SALE_OUTPUT_FILE=${DATA_DIR}/dm0_onseffdt_.txt
export ON_SALE_OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_item_on_sale.schema
export OFF_SALE_OUTPUT_FILE=${DATA_DIR}/dm0_ofseffdt_.txt
export OFF_SALE_OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_item_off_sale.schema
export RETL_FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_item_sale_reject_ord_mult.txt

export SQL_STATEMENT="SELECT STORE,
                   RMS_SKU,
                   rtrim(substr(ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                   MIN(STATUS_UPDATE_DATE) STATUS_UPDATE_DATE,
                   STATUS
              FROM(SELECT se.LOCATION STORE,
                          se.ITEM RMS_SKU,
                          DECODE(im.PACK_IND, 'Y',
                                              (SELECT QTY
                                                 FROM ${RMS_OWNER}.V_PACKSKU_QTY vpq
                                                WHERE vpq.PACK_NO = im.ITEM),
                                                 isc.SUPP_PACK_SIZE) ORDER_MULTIPLE,
                          sd.STATUS_UPDATE_DATE,
                          sd.STATUS
                    FROM ${RMS_OWNER}.SIT_EXPLODE se,
                         ${RMS_OWNER}.SIT_DETAIL sd,
                         ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                         ${RMS_OWNER}.ITEM_MASTER im
                   WHERE se.ITEMLOC_LINK_ID = sd.ITEMLOC_LINK_ID
                     AND sd.STATUS = 'A'
                     AND se.ITEM = isc.ITEM
                     AND isc.PRIMARY_SUPP_IND = 'Y'
                     AND isc.PRIMARY_COUNTRY_IND = 'Y'
                     AND se.ITEM = im.ITEM
                     AND im.STATUS = 'A'
                     AND im.ITEM_LEVEL = im.TRAN_LEVEL
                     AND im.INVENTORY_IND ='Y'
                     AND im.ORDERABLE_IND ='Y'
                     AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                          OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                                         FROM ${RMS_OWNER}.ITEM_MASTER im1,
                                                                              ${RMS_OWNER}.PACKITEM pm
                                                                        WHERE pm.item = im1.item
                                                                          AND im1.forecast_ind = 'Y'
                                                                          AND im1.aip_case_type = 'F')))
                    AND sd.STATUS_UPDATE_DATE > TO_DATE(${VDATE}, 'YYYYMMDD'))
             GROUP BY STORE,
                      RMS_SKU,
                      ORDER_MULTIPLE,
                      STATUS
            UNION ALL
            SELECT STORE,
                   RMS_SKU,
                   rtrim(substr(ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                   MAX(STATUS_UPDATE_DATE) STATUS_UPDATE_DATE,
                   STATUS
              FROM(SELECT se.LOCATION STORE,
                          se.ITEM RMS_SKU,
                          DECODE(im.PACK_IND, 'Y',
                                              (SELECT QTY
                                                 FROM ${RMS_OWNER}.V_PACKSKU_QTY vpq
                                                WHERE vpq.PACK_NO = im.ITEM),
                                                 isc.SUPP_PACK_SIZE) ORDER_MULTIPLE,
                          sd.STATUS_UPDATE_DATE,
                          sd.STATUS
                    FROM ${RMS_OWNER}.SIT_EXPLODE se,
                         ${RMS_OWNER}.SIT_DETAIL sd,
                         ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                         ${RMS_OWNER}.ITEM_MASTER im
                   WHERE se.ITEMLOC_LINK_ID = sd.ITEMLOC_LINK_ID
                     AND sd.STATUS = 'C'
                     AND se.ITEM = isc.ITEM
                     AND isc.PRIMARY_SUPP_IND = 'Y'
                     AND isc.PRIMARY_COUNTRY_IND = 'Y'
                     AND se.ITEM = im.ITEM
                     AND im.STATUS = 'A'
                     AND im.ITEM_LEVEL = im.TRAN_LEVEL
                     AND im.INVENTORY_IND ='Y'
                     AND im.ORDERABLE_IND ='Y'
                     AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                          OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                                         FROM ${RMS_OWNER}.ITEM_MASTER im1,
                                                                              ${RMS_OWNER}.PACKITEM pm
                                                                        WHERE pm.item = im1.item
                                                                          AND im1.forecast_ind = 'Y'
                                                                          AND im1.aip_case_type = 'F')))
                     AND sd.STATUS_UPDATE_DATE > TO_DATE(${VDATE}, 'YYYYMMDD'))
             GROUP BY STORE,
                      RMS_SKU,
                      ORDER_MULTIPLE,
                      STATUS"

message "Program started ..."

cat > ${RETL_FLOW_FILE} << EOF


<FLOW name = "${PROGRAM_NAME}.flw">
  ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
          ${SQL_STATEMENT}
         ]]>
      </PROPERTY>
      <OUTPUT name="on_off_sale_initial.v"/>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT name="on_off_sale_initial.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="on_off_sale_reject.v"/>
      <OUTPUT   name="on_off_sale_filter.v"/>
   </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="on_off_sale_filter.v" />
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
                  <CONVERT destfield="STORE" sourcefield="STORE">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="RMS_SKU" sourcefield="RMS_SKU">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="STATUS_UPDATE_DATE" sourcefield="STATUS_UPDATE_DATE">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="on_off_sale.v"/>
      </OPERATOR>

   <OPERATOR type="filter">
      <INPUT name="on_off_sale.v"/>
      <PROPERTY  name="filter" value="STATUS EQ 'A'"/>
      <PROPERTY  name="rejects" value="true"/>
      <OUTPUT  name="on_sale_out.v"/>
      <OUTPUT  name="off_sale_out.v"/>
   </OPERATOR>

   <OPERATOR type="fieldmod">
      <INPUT name="on_sale_out.v"/>
      <PROPERTY name="rename" value="ON_SALE_EFFECTIVE_DATE=STATUS_UPDATE_DATE"/>
      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ON_SALE_OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${ON_SALE_OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="fieldmod">
      <INPUT    name="off_sale_out.v"/>
      <PROPERTY name="rename" value="OFF_SALE_EFFECTIVE_DATE=STATUS_UPDATE_DATE"/>
      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${OFF_SALE_OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OFF_SALE_OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="export">
       <INPUT name="on_off_sale_reject.v"/>
       <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>

</FLOW>

EOF
${RETL_EXE} ${RETL_OPTIONS} -f ${RETL_FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0

