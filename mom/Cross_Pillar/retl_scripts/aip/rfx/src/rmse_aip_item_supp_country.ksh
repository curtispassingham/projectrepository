#!/bin/ksh
########################################################
# Purpose:  Extracts RMS item, supplier and
#           supplier pack size information
########################################################

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_item_supp_country"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export AIP_OUTPUT_FILE=${DATA_DIR}/dmx_prdspllks.txt
export AIP_OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_dmx_prdspllks.schema
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_item_supp_country_reject_ord_mult.txt

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">

      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT isc.ITEM,
                      isc.SUPPLIER,
                      '1' EACH,
                      rtrim(substr(isc.SUPP_PACK_SIZE,0,6),'.') CASE,
                      rtrim(substr(isc.INNER_PACK_SIZE,0,6),'.') INNER,
                      rtrim(substr((isc.TI * isc.HI * isc.SUPP_PACK_SIZE),0,6),'.') PALLET,
                      isc.PRIMARY_SUPP_IND
                 FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                      ${RMS_OWNER}.ITEM_SUPPLIER isup,
                      ${RMS_OWNER}.ITEM_MASTER im
                WHERE isc.PRIMARY_COUNTRY_IND = 'Y'
                  AND im.ITEM = isc.ITEM
                  AND im.ITEM = isup.ITEM
                  AND im.STATUS = 'A'
                  AND im.TRAN_LEVEL = im.ITEM_LEVEL
                  AND im.INVENTORY_IND = 'Y'
                  AND im.AIP_CASE_TYPE = 'I'
                  AND im.PACK_IND = 'N'
                  AND im.FORECAST_IND = 'Y'
                  AND isup.SUPPLIER = isc.SUPPLIER
                  AND NVL(isup.SUPP_DISCONTINUE_DATE, to_date('${VDATE}','yyyymmdd')+1) > to_date('${VDATE}','yyyymmdd')
            ]]>
         </PROPERTY>
         <OUTPUT name="informal_packs.v"/>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT isc.ITEM,
                      isc.SUPPLIER,
                      DECODE(im.SIMPLE_PACK_IND,
                             'Y',
                             (SELECT rtrim(substr(QTY,0,6),'.')
                                FROM ${RMS_OWNER}.V_PACKSKU_QTY
                               WHERE PACK_NO = im.ITEM),
                             '1') ORDER_MULTIPLE,
                      isc.PRIMARY_SUPP_IND
                 FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                      ${RMS_OWNER}.ITEM_SUPPLIER isup,
                      ${RMS_OWNER}.ITEM_MASTER im
                WHERE isc.PRIMARY_COUNTRY_IND='Y'
                  AND im.ITEM = isc.ITEM
                  AND im.ITEM = isup.ITEM
                  AND im.STATUS='A'
                  AND im.INVENTORY_IND = 'Y'
                  AND NVL(im.AIP_CASE_TYPE,'F') != 'I'
                  AND im.TRAN_LEVEL = im.ITEM_LEVEL
                  AND isup.SUPPLIER = isc.SUPPLIER
                  AND NVL(isup.SUPP_DISCONTINUE_DATE, to_date('${VDATE}','yyyymmdd')+1) > to_date('${VDATE}','yyyymmdd')
                  AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                       OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                                      FROM ${RMS_OWNER}.item_master im1,
                                                                           ${RMS_OWNER}.packitem pm
                                                                     WHERE pm.item = im1.item
                                                                       AND im1.forecast_ind = 'Y')))
            ]]>
         </PROPERTY>
         <OUTPUT name="formal_packs.v"/>
      </OPERATOR>
   
      <OPERATOR type="copy">
         <INPUT name="informal_packs.v"/>
         <OUTPUT name="each.v"/>
         <OUTPUT name="inner.v"/>
         <OUTPUT name="case.v"/>
         <OUTPUT name="pallet.v"/>
      </OPERATOR>

      <OPERATOR type="copy">
         <INPUT name="formal_packs.v"/>
         <OUTPUT name="formal_packs_xform.v"/>
         <OUTPUT name="formal_packs_aip.v"/>
      </OPERATOR>
      
      <OPERATOR type="fieldmod">
         <INPUT name="each.v"/>
         <PROPERTY name="drop" value="INNER"/>
         <PROPERTY name="drop" value="CASE"/>
         <PROPERTY name="drop" value="PALLET"/>
         <PROPERTY name="rename" value="ORDER_MULTIPLE = EACH"/>
         <OUTPUT name="each_only.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <INPUT name="inner.v"/>
         <PROPERTY name="drop" value="EACH"/>
         <PROPERTY name="drop" value="CASE"/>
         <PROPERTY name="drop" value="PALLET"/>
         <PROPERTY name="rename" value="ORDER_MULTIPLE = INNER"/>
         <OUTPUT name="inner_only.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <INPUT name="case.v"/>
         <PROPERTY name="drop" value="INNER"/>
         <PROPERTY name="drop" value="EACH"/>
         <PROPERTY name="drop" value="PALLET"/>
         <PROPERTY name="rename" value="ORDER_MULTIPLE = CASE"/>
         <OUTPUT name="case_only.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <INPUT name="pallet.v"/>
         <PROPERTY name="drop" value="INNER"/>
         <PROPERTY name="drop" value="CASE"/>
         <PROPERTY name="drop" value="EACH"/>
         <PROPERTY name="rename" value="ORDER_MULTIPLE = PALLET"/>
         <OUTPUT name="pallet_only.v"/>
      </OPERATOR>

      <OPERATOR type="funnel">
         <INPUT name="each_only.v"/>
         <INPUT name="inner_only.v"/>
         <INPUT name="case_only.v"/>
         <INPUT name="pallet_only.v"/>
         <OUTPUT name="aip_informal_dup.v"/>   
      </OPERATOR>

      <OPERATOR type="copy">
         <INPUT name="aip_informal_dup.v"/>
         <OUTPUT name="aip_informal_dup_xform.v"/>
         <OUTPUT name="aip_informal_dup_aip.v"/>   
      </OPERATOR>      
      
      ### Flow 1 - Transform scripts output ###     
      <OPERATOR type="sort">
         <INPUT name="aip_informal_dup_xform.v"/>
         <PROPERTY name="key" value="ITEM"/>
         <PROPERTY name="key" value="ORDER_MULTIPLE"/>
         <PROPERTY name="key" value="PRIMARY_SUPP_IND"/>
         <PROPERTY name="order" value="asc"/>
         <OUTPUT name="aip_informal_dup_sorted_xform.v"/>
      </OPERATOR>
                     
      ### Flow 2 - Transform scripts output ### 
      <OPERATOR type="removedup">
         <INPUT name="aip_informal_dup_sorted_xform.v"/>
         <PROPERTY name="key" value = "ITEM"/>
         <PROPERTY name="key" value = "ORDER_MULTIPLE"/>
         <PROPERTY name="keep" value = "last"/>
         <OUTPUT name="aip_informal_xform.v"/>
      </OPERATOR>

      ### Flow 3 - Transform scripts output ###       -- maybe move higher
      <OPERATOR type="filter">
         <INPUT    name="aip_informal_xform.v"/>
         <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
         <PROPERTY name="rejects" value="true"/>
         <OUTPUT   name="reject_ord_multiple.v"/>
         <OUTPUT   name="aip_informal_ord_mult_filter_xform.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
         <INPUT    name="formal_packs_xform.v"/>
         <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
         <PROPERTY name="rejects" value="true"/>
         <OUTPUT   name="reject_formal_ord_multiple.v"/>
         <OUTPUT   name="aip_formal_ord_mult_filter_xform.v"/>
      </OPERATOR>

      ### Flow 4 - Transform scripts output ###
      <OPERATOR type="funnel">
         <INPUT name="aip_informal_ord_mult_filter_xform.v"/>
         <INPUT name="aip_formal_ord_mult_filter_xform.v"/>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                        <TYPEPROPERTY name="nullable" value="false"/>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OUTPUT name="aip_packs_funnel_xform.v"/>
         </OPERATOR>
      </OPERATOR>

      ### Flow 5 - Transform scripts output ###       
      <OPERATOR  type="fieldmod">
         <INPUT name="aip_packs_funnel_xform.v"/>
         <PROPERTY  name="keep" value="ITEM SUPPLIER ORDER_MULTIPLE PRIMARY_SUPP_IND"/>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
     
      ### Flow 1 - AIP output ###
      <OPERATOR type="sort">
         <INPUT name="aip_informal_dup_aip.v"/> 
         <PROPERTY name="key" value="ITEM"/>
         <PROPERTY name="key" value="SUPPLIER"/>         
         <PROPERTY name="key" value="ORDER_MULTIPLE"/>
         <PROPERTY name="order" value="asc"/>
         <OUTPUT name="aip_informal_dup_sorted_aip.v"/>
      </OPERATOR>      

      ### Flow 2 - AIP output ###            
      <OPERATOR type="removedup">
         <INPUT name="aip_informal_dup_sorted_aip.v"/>
         <PROPERTY name="key" value = "ITEM"/>
         <PROPERTY name="key" value = "SUPPLIER"/>
         <PROPERTY name="key" value = "ORDER_MULTIPLE"/>
         <PROPERTY name="key" value = "PRIMARY_SUPP_IND"/>
         <PROPERTY name="keep" value = "first"/>
         <OUTPUT name="aip_informal.v"/>
      </OPERATOR>
      
      ### Flow 3 - AIP output ###
      <OPERATOR type="filter">
         <INPUT    name="aip_informal.v"/>
         <PROPERTY name="filter" value="ORDER_MULTIPLE LE 999999"/>
         <PROPERTY name="rejects" value="false"/>
         <OUTPUT   name="aip_informal_ord_mult_filter.v"/>
      </OPERATOR>      
      
      ### Flow 4 - AIP output ###
      <OPERATOR type="funnel">
         <INPUT name="aip_informal_ord_mult_filter.v"/>
         <INPUT name="formal_packs_aip.v"/>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                        <TYPEPROPERTY name="nullable" value="false"/>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OUTPUT name="aip_packs_funnel.v"/>
         </OPERATOR>
      </OPERATOR>      
      
      ### Flow 5 - AIP output ###
      <OPERATOR  type="generator">
         <INPUT name="aip_packs_funnel.v"/>
         <PROPERTY  name="schema"> <![CDATA[
            <GENERATE>
               <FIELD name="COMMODITY_SUPPLIER_LINKS" type="string" len="1" >
                  <CONST value="1"/>
               </FIELD>
            </GENERATE> ]]>
         </PROPERTY>
         <OPERATOR  type="fieldmod">
            <PROPERTY  name="rename" value="RMS_SKU=ITEM"/>
            <OUTPUT name="aip_final_output.v"/>
         </OPERATOR>
      </OPERATOR>

      ### Flow 6 - AIP output ###
      <OPERATOR type="export">
         <INPUT name="aip_final_output.v"/>
         <PROPERTY name="outputfile" value="${AIP_OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${AIP_OUTPUT_SCHEMA}"/>
      </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="reject_ord_multiple.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="reject_ord_multiple_not_null.v"/>
      </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="reject_formal_ord_multiple.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="reject_formal_ord_multiple_not_null.v"/>
      </OPERATOR>

      <OPERATOR type="funnel">
         <INPUT name="reject_formal_ord_multiple_not_null.v"/>
         <INPUT name="reject_ord_multiple_not_null.v"/>
         <OUTPUT name="reject_total.v"/>
      </OPERATOR>      

      <OPERATOR type="export">
         <INPUT name="reject_total.v"/>
         <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
      </OPERATOR>

   </FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
