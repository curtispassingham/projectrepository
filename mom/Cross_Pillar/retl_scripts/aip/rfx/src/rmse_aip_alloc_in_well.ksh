#!/bin/ksh
########################################################
# Purpose: extracts RMS allocation data for AIP
#          future delivery
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_alloc_in_well"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_alloc_in_well_reject_ord_mult.txt

message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"
  
cat > ${FLOW_FILE}.xml << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
  ${DBREAD}
     <PROPERTY name = "query">
         <![CDATA[
            SELECT oc.DAY,
                   oc.LOC,
                   oc.ITEM,
                   CASE oc.PRIMARY_CASE_SIZE 
                        WHEN 'C' THEN rtrim(substr(isup.SUPP_PACK_SIZE,0,6),'.'),
                        WHEN 'I' THEN rtrim(substr(isup.INNER_PACK_SIZE,0,6),'.'),
                        WHEN 'P' THEN rtrim(substr(isup.TI * isup.HI * isup.SUPP_PACK_SIZE,0,6),'.')
                   ELSE '1'
                   END ORDER_MULTIPLE,
                  rtrim(substr(SUM(oc.RESERVE_QTY),0,8),'.') RESERVE_QTY,
				  oc.ORDER_NO
              FROM (SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) */
                           'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD')  DAY,
                           h.RELEASE_DATE D_DAY,
                           h.WH LOC,
                           im.ITEM,
                           isp.PRIMARY_CASE_SIZE,
                           d.TO_LOC_TYPE,
                           d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0) RESERVE_QTY,
                           h.ORDER_NO
                      FROM ${RMS_OWNER}.ALLOC_HEADER h,
                           ${RMS_OWNER}.ALLOC_DETAIL d,
                           ${RMS_OWNER}.ITEM_MASTER im,
                           ${RMS_OWNER}.ITEM_SUPPLIER isp
                     WHERE h.ALLOC_NO=d.ALLOC_NO
                       AND (h.ORDER_NO IS NULL OR
                            NOT EXISTS (SELECT '1'
                                          FROM ${RMS_OWNER}.ORDHEAD i
                                         WHERE i.order_type='CO'
                                           AND i.order_no=h.order_no))
                       AND h.ITEM=im.ITEM
                       and im.ITEM=isp.ITEM
                       AND isp.PRIMARY_SUPP_IND = 'Y'
                       AND (d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0))>0
                       AND h.RELEASE_DATE IS NOT NULL
                       AND im.STATUS='A'
                       AND (im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                       AND im.ITEM_LEVEL=TRAN_LEVEL
                       AND h.STATUS IN ('A','R')
                       AND im.AIP_CASE_TYPE = 'I') oc,
                   ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup
             WHERE D_DAY >= (TO_DATE('${VDATE}','YYYYMMDD'))- ${MAX_NOTAFTER_DAYS}
               AND oc.ITEM = isup.ITEM
               AND (oc.ORDER_NO is NULL OR
                    isup.SUPPLIER = (SELECT SUPPLIER
                                       FROM ${RMS_OWNER}.ORDHEAD
                                      WHERE order_no = oc.order_no))
               AND isup.PRIMARY_COUNTRY_IND = 'Y'
             GROUP BY oc.DAY,
                      oc.LOC,
                      oc.ITEM,
                      isup.INNER_PACK_SIZE,
                      isup.SUPP_PACK_SIZE,
                      (isup.ti * isup.hi * isup.SUPP_PACK_SIZE),
                      oc.PRIMARY_CASE_SIZE,
                      oc.TO_LOC_TYPE,
					  oc.ORDER_NO
         ]]>
      </PROPERTY>
      <OUTPUT name="informal_items.v"/>
   </OPERATOR>

  ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT DAY,
                   LOC,
                   ITEM,
                   rtrim(substr(ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                   rtrim(substr(SUM(RESERVE_QTY),0,8),'.') RESERVE_QTY,
				   ORDER_NO
              FROM (SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) */
                           'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD')  DAY,
                           h.RELEASE_DATE D_DAY,
                           h.WH LOC,
                           im.item,
                           (d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0))*vpq.QTY RESERVE_QTY,
                           vpq.QTY ORDER_MULTIPLE,
						   h.ORDER_NO
                      FROM ${RMS_OWNER}.ALLOC_HEADER h,
                           ${RMS_OWNER}.ALLOC_DETAIL d,
                           ${RMS_OWNER}.ITEM_MASTER im,
                           ${RMS_OWNER}.V_PACKSKU_QTY vpq
                     WHERE h.ALLOC_NO=D.ALLOC_NO
                       AND (h.ORDER_NO IS NULL OR
                             NOT EXISTS (SELECT '1'
                                           FROM ${RMS_OWNER}.ORDHEAD i
                                          WHERE i.order_type='CO'
                                            AND i.order_no=h.order_no))
                       AND h.ITEM=im.ITEM                     
                       AND (d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0))>0
                       AND h.RELEASE_DATE IS NOT NULL
                       AND im.STATUS='A'
                       AND im.SIMPLE_PACK_IND = 'Y' 
                       AND im.ITEM_LEVEL=im.TRAN_LEVEL
                       AND h.STATUS IN ('A', 'R')
                       AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                       AND VPQ.PACK_NO = im.ITEM
                       AND exists (select 'X'
                                     from ${RMS_OWNER}.item_master im1
                                    where im1.item = vpq.item
                                      and im1.forecast_ind = 'Y')
                    UNION
                    SELECT inner.DAY,
                           inner.D_DAY,
                           inner.LOC,
                           inner.ITEM,
                           inner.RESERVE_QTY,
                           inner.ORDER_MULTIPLE,
						   inner.ORDER_NO
                      FROM  (SELECT DAY,
                                    D_DAY,
                                    LOC,
                                    PACK_NO ITEM,
                                    SIZE_RANK,
                                    QTY_RANK,
                                    RANK() OVER
                                        (PARTITION BY ITEM ORDER BY SIZE_RANK) PACK_RANK, --To select one when multiple pack is found
                                    RESERVE_QTY,
                                    ORDER_MULTIPLE,
									ORDER_NO
                               FROM (SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) */
                                            'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD')  DAY,
                                             h.RELEASE_DATE D_DAY,
                                             h.WH LOC,
                                             vpq.PACK_NO PACK_NO,
                                             h.ITEM ITEM,
                                             CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                             WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                             ELSE 3 END SIZE_RANK,
                                             RANK() OVER
                                                (PARTITION BY h.ITEM ORDER BY vpq.QTY) QTY_RANK, --To select pack with smallest component quantity
                                             d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0) RESERVE_QTY,
                                             vpq.QTY ORDER_MULTIPLE,
											 h.ORDER_NO
                                        FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                             ${RMS_OWNER}.ALLOC_DETAIL d,
                                             ${RMS_OWNER}.ITEM_MASTER im,
                                             ${RMS_OWNER}.ITEM_MASTER im2,
                                             ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                             ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc
                                       WHERE h.ALLOC_NO=d.ALLOC_NO
                                         AND (h.ORDER_NO IS NULL OR
                                               NOT EXISTS (SELECT '1'
                                                             FROM ${RMS_OWNER}.ORDHEAD i
                                                            WHERE i.order_type='CO'
                                                              AND i.order_no=h.order_no))
                                         AND h.ITEM=im.ITEM
                                         AND (d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0))>0
                                         AND h.RELEASE_DATE IS NOT NULL
                                         AND im.STATUS='A'
                                         AND im.PACK_IND = 'N'
                                         AND im.FORECAST_IND = 'Y'
                                         AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                         AND h.STATUS IN ('A', 'R')
                                         AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                         AND im.ITEM = vpq.ITEM
                                         AND vpq.PACK_NO = im2.ITEM
                                         AND im2.SIMPLE_PACK_IND = 'Y'
                                         AND isc.ITEM= vpq.ITEM
                                         AND (   (h.ORDER_NO IS NULL AND isc.PRIMARY_SUPP_IND = 'Y')
                                              or (isc.SUPPLIER = (SELECT i.SUPPLIER
                                                                    FROM ${RMS_OWNER}.ORDHEAD i
                                                                   WHERE i.order_no=h.order_no)))
                                         AND isc.PRIMARY_COUNTRY_IND = 'Y')) inner
                     WHERE inner.PACK_RANK = 1
                       AND (   inner.SIZE_RANK = 1
                            OR inner.SIZE_RANK = 2
                            OR (    inner.SIZE_RANK = 3
                                AND inner.QTY_RANK  = 1))
                       )
             WHERE D_DAY >= (TO_DATE('${VDATE}','YYYYMMDD'))- ${MAX_NOTAFTER_DAYS}
          GROUP BY DAY,
                   LOC,
                   ITEM,
                   ORDER_MULTIPLE,
				   ORDER_NO
         ]]>
      </PROPERTY>
      <OUTPUT name="formal_items.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="informal_items.v"/>
      <OUTPUT name="aip_informal_dup.v"/>
   </OPERATOR>

   <OPERATOR type="hash">
      <INPUT name="aip_informal_dup.v"/>
      <PROPERTY name="key" value = "DAY"/>
      <PROPERTY name="key" value = "LOC"/>
      <PROPERTY name="key" value = "ITEM"/>
      <PROPERTY name="key" value = "ORDER_MULTIPLE"/>
      <PROPERTY name="key" value = "RESERVE_QTY"/>
	  <PROPERTY name="key" value = "ORDER_NO"/>
      <OPERATOR type="sort">
         <PROPERTY name="key" value = "DAY"/>
         <PROPERTY name="key" value = "LOC"/>
         <PROPERTY name="key" value = "ITEM"/>
         <PROPERTY name="key" value = "ORDER_MULTIPLE"/>
         <PROPERTY name="key" value = "RESERVE_QTY"/>
		 <PROPERTY name="key" value = "ORDER_NO"/>
         <OPERATOR type = "groupby">
            <PROPERTY name = "key" value = "DAY"/>
            <PROPERTY name = "key" value = "LOC"/>
            <PROPERTY name = "key" value = "ITEM"/>
            <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
            <PROPERTY name = "reduce" value = "RESERVE_QTY"/>
            <PROPERTY name = "sum" value = "RESERVE_QTY"/>
			<PROPERTY name = "key" value = "ORDER_NO"/>
            <OPERATOR type="removedup">
               <PROPERTY name="key" value = "DAY"/>
               <PROPERTY name="key" value = "LOC"/>
               <PROPERTY name="key" value = "ITEM"/>
               <PROPERTY name="key" value = "ORDER_MULTIPLE"/>
			   <PROPERTY name="key" value = "ORDER_NO"/>			   
               <PROPERTY name="keep" value = "last"/>
               <OUTPUT name="aip_informal.v"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="filter">
       <INPUT    name="aip_informal.v"/>
       <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
       <PROPERTY name="rejects" value="true"/>
       <OUTPUT   name="reject_ord_multiple_inf.v"/>
       <OUTPUT   name="aip_informal_ord_mult_filter.v"/>
   </OPERATOR>

   <OPERATOR type="filter">
       <INPUT    name="formal_items.v"/>
       <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
       <PROPERTY name="rejects" value="true"/>
       <OUTPUT   name="reject_ord_multiple_form.v"/>
       <OUTPUT   name="aip_formal_ord_mult_filter.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="aip_informal_ord_mult_filter.v"/>
      <INPUT name="aip_formal_ord_mult_filter.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC" sourcefield="LOC" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ITEM" sourcefield="ITEM">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ALLOC_RESERVE_QTY" sourcefield="RESERVE_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ORDER_NO" sourcefield="ORDER_NO" newtype="int64">
                     <TYPEPROPERTY name="nullable" value="true"/>
                  </CONVERT>				  
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="reject_ord_multiple_inf.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
      <OUTPUT name="reject_ord_multiple_inf_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="reject_ord_multiple_form.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
      <OUTPUT name="reject_ord_multiple_form_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="reject_ord_multiple_inf_not_null.v"/>
      <INPUT name="reject_ord_multiple_form_not_null.v"/>
      <OUTPUT name="reject_total.v"/>
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT name="reject_total.v"/>
      <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>

</FLOW>

EOF

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
