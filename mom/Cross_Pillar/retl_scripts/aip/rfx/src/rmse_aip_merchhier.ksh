#!/bin/ksh

########################################################
#  Script extracts merchandise hierarchy information 
#  from the RMS COMPHEAD, DIVIAION, GROUPS, DEPS, CLASS,
#  SUBCLASS tables
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="rmse_aip_merchhier"


########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD} 
         <PROPERTY name = "query">
            <![CDATA[
           SELECT sc.SUBCLASS,
                  REPLACE(RTRIM(sc.SUB_NAME),CHR(10),'') SUB_NAME,
                  sc.CLASS,
                  REPLACE(RTRIM(c.CLASS_NAME),CHR(10),'') CLASS_NAME,
                  c.DEPT,
                  REPLACE(RTRIM(dp.DEPT_NAME),CHR(10),'') DEPT_NAME,
                  dp.GROUP_NO,
                  REPLACE(RTRIM(g.GROUP_NAME),CHR(10),'') GROUP_NAME,
                  g.DIVISION,
                  REPLACE(RTRIM(dv.DIV_NAME),CHR(10),'') DIV_NAME,
                  ch.COMPANY,
                  REPLACE(RTRIM(ch.CO_NAME),CHR(10),'') CO_NAME, 
                  dp.PURCHASE_TYPE
               FROM ${RMS_OWNER}.SUBCLASS sc,
                  ${RMS_OWNER}.CLASS c,
                  ${RMS_OWNER}.DEPS dp,
                  ${RMS_OWNER}.GROUPS g,
                  ${RMS_OWNER}.DIVISION dv,
                  ${RMS_OWNER}.COMPHEAD ch
               WHERE sc.CLASS=c.CLASS
                  AND sc.DEPT=dp.DEPT
                  AND c.DEPT=dp.DEPT
                  AND dp.GROUP_NO=g.GROUP_NO
                  AND g.DIVISION=dv.DIVISION
            ]]>
         </PROPERTY>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="SUB_NAME" sourcefield="SUB_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="CLASS_NAME" sourcefield="CLASS_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="DEPT_NAME" sourcefield="DEPT_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="GROUP_NAME" sourcefield="GROUP_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="DIV_NAME" sourcefield="DIV_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="CO_NAME" sourcefield="CO_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OPERATOR type="export">
               <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
               <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>
   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
