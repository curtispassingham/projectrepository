#!/bin/ksh
################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_suppliers"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export AIP_OUTPUT_FILE=${DATA_DIR}/splr.txt
export AIP_OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_splr.schema
export AIP_DSD_OUTPUT_FILE=${DATA_DIR}/dmx_dirspl.txt
export AIP_DSD_OUTPUT_SCHEMA=${SCHEMA_DIR}/rmse_aip_dmx_dirspl.schema


message "Program started ..."

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT SUPPLIER,
                      REPLACE(RTRIM(SUP_NAME),CHR(10),'') SUP_NAME,
                      SUP_STATUS,
                      DECODE(DSD_IND,
                             'Y','1',
                             'N','0') DIRECT_SUPPLIER
                 FROM ${RMS_OWNER}.SUPS
                WHERE ((SUPPLIER_PARENT IS NOT NULL
                        AND EXISTS (SELECT '1' FROM SYSTEM_OPTIONS WHERE SUPPLIER_SITES_IND = 'Y'))
                         OR EXISTS (SELECT '1' FROM SYSTEM_OPTIONS WHERE SUPPLIER_SITES_IND = 'N'))
            ]]>
         </PROPERTY>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="SUP_NAME" sourcefield="SUP_NAME">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="DIRECT_SUPPLIER" sourcefield="DIRECT_SUPPLIER">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="0"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OPERATOR  type="copy">
               <OUTPUT name="copy1.v"/>
               <OUTPUT name="copy2.v"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="copy1.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>

      <OPERATOR type="filter">
         <INPUT name="copy2.v"/>
         <PROPERTY  name="filter" value="SUP_STATUS EQ 'A'"/>
         <OPERATOR type="copy">
            <OUTPUT name="copy2_1.v"/>
            <OUTPUT name="copy2_2.v"/>
         </OPERATOR>
      </OPERATOR>

      <OPERATOR  type="fieldmod">
         <PROPERTY  name="rename" value="SUPPLIER_DESCRIPTION=SUP_NAME"/>
         <INPUT name="copy2_1.v"/>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${AIP_OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${AIP_OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="copy2_2.v"/>
         <PROPERTY name="outputfile" value="${AIP_DSD_OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${AIP_DSD_OUTPUT_SCHEMA}"/>
      </OPERATOR>

   </FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
