#!/bin/ksh

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_substitute_items"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

#########################################################

message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"
  
cat > ${FLOW_FILE}.xml << EOF
<FLOW name = "${PROGRAM_NAME}.flw">
   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT sid.ITEM,
                   sid.LOCATION,
                   sid.SUB_ITEM,
                   sid.LOC_TYPE,
                   sid.START_DATE,
                   sid.END_DATE,
				   cd.CODE_DESC SUBSTITUTE_REASON
              FROM ${RMS_OWNER}.SUB_ITEMS_DETAIL sid,
			       ${RMS_OWNER}.CODE_DETAIL cd,
			       (SELECT wh loc 
                      FROM ${RMS_OWNER}.wh 
                    UNION ALL 
                    SELECT store loc 
                      FROM ${RMS_OWNER}.store 
                     WHERE store_type in ('C','F')) l
             WHERE sid.LOCATION = l.LOC
			   AND cd.CODE_TYPE = 'SUBR'
			   AND cd.CODE = sid.SUBSTITUTE_REASON 
			   AND exists(select 'Y' from ${RMS_OWNER}.ITEM_MASTER im where im.ITEM= sid.ITEM   AND im.STATUS = 'A' AND im.FORECAST_IND = 'Y')
			   AND exists(select 'Y' from ${RMS_OWNER}.ITEM_MASTER im where im.ITEM= sid.SUB_ITEM  AND im.STATUS = 'A' AND im.FORECAST_IND = 'Y')
			   AND sid.END_DATE >= get_vdate
         ]]>
      </PROPERTY>
      <OUTPUT   name="query_sub_items.v"/>
   </OPERATOR>


   <OPERATOR type="export">
      <INPUT    name="query_sub_items.v"/>
      <PROPERTY name="outputfile" value="$OUTPUT_FILE"/>
      <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA"/>
   </OPERATOR>

</FLOW>

EOF
${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml
checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed succesfully"

# cleanup and exit
rmse_terminate 0

