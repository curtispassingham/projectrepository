#!/bin/ksh

#########################################################################
#
#  pre_rmse_aip.ksh defines the system parameters. The parameters
#  are obtained from the data base tables and written into 
#  ".txt" files in the "etc" directory.
#
#########################################################################

export PROGRAM_NAME="pre_rmse_aip"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh

message "Program started ..."

########################################################
#  Back up existing output files
########################################################

# Note that prime_exch_rate is treated separately
OUT_LIST="consolidation_code \
          prime_currency_code \
          vat_ind \
          stkldgr_vat_incl_retl_ind \
          multi_currency_ind \
          class_level_vat_ind \
          domain_level \
          vdate \
          next_vdate \
          last_eom_date \
          curr_bom_date \
          max_backpost_days \
          last_extr_closed_pot_date \
          last_extr_received_pot_date"
             
for out in ${OUT_LIST} prime_exchng_rate; do
  if [[ -f ${ETC_DIR}/${out}.txt ]] ; then
    mv -f ${ETC_DIR}/${out}.txt ${ETC_DIR}/${out}.txt.old
  fi
done

########################################################
#  Create RETL flow in a logged file
#  This flow reads database-based system parameters.
########################################################
FLOW_FILE_1="${LOG_DIR}/${PROGRAM_NAME}_1.xml"
cat > ${FLOW_FILE_1} << EOF
   <FLOW name="${PROGRAM_NAME}_1.flw">

      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[ 
               SELECT CASE when CONSOLIDATION_IND =  'Y'
                  THEN 'C'
                  ELSE  'O' 
                  END AS CONSOLIDATION_CODE,
                  DECODE(DEFAULT_TAX_TYPE, 'SALES', 'N', 'Y') AS VAT_IND,
                  STKLDGR_VAT_INCL_RETL_IND,
                  MULTI_CURRENCY_IND,
                  CURRENCY_CODE,
                  CLASS_LEVEL_VAT_IND,
                  NVL(DOMAIN_LEVEL, 'X') DOMAIN_LEVEL
               FROM ${RMS_OWNER}.SYSTEM_OPTIONS
            ]]>	
         </PROPERTY>
         <OUTPUT name="sys_opt.v"/>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[ 
               SELECT VDATE,
                  VDATE + 1 NEXT_VDATE,
                  LAST_EOM_DATE,
                  LAST_EOM_DATE + 1 CURR_BOM_DATE, 
                  (VDATE - LAST_EOM_DATE) MAX_BACKPOST_DAYS
               FROM ${RMS_OWNER}.PERIOD, 
                  ${RMS_OWNER}.SYSTEM_VARIABLES
            ]]>       
         </PROPERTY>
         <OUTPUT name="sys_var.v"/>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[ 
               SELECT LAST_EXTR_CLOSED_POT_DATE
               FROM ${RMS_OWNER}.RETL_EXTRACT_DATES 
            ]]>       
         </PROPERTY>
         <OUTPUT name="last_extr_closed_pot.v"/>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[ 
               SELECT LAST_EXTR_RECEIVED_POT_DATE
               FROM ${RMS_OWNER}.RETL_EXTRACT_DATES 
             ]]>       
         </PROPERTY>
         <OUTPUT name="last_extr_received_pot.v"/>
      </OPERATOR>

      <OPERATOR type="copy">
         <INPUT name="sys_opt.v"/>
         <OUTPUT name="c_sys_opt1.v"/>
         <OUTPUT name="c_sys_opt2.v"/>
         <OUTPUT name="c_sys_opt3.v"/>
         <OUTPUT name="c_sys_opt4.v"/>
         <OUTPUT name="c_sys_opt5.v"/>
         <OUTPUT name="c_sys_opt6.v"/>
         <OUTPUT name="c_sys_opt7.v"/>
      </OPERATOR>

      <OPERATOR  type="fieldmod"> 
         <PROPERTY  name="keep" value="CONSOLIDATION_CODE"/>
         <INPUT name="c_sys_opt1.v"/>
         <OUTPUT name="o_sys_opt1.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/consolidation_code.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt1.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="CURRENCY_CODE"/>
         <INPUT name="c_sys_opt2.v"/>
         <OUTPUT name="o_sys_opt2.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/prime_currency_code.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt2.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="VAT_IND"/>
         <INPUT name="c_sys_opt3.v"/>
         <OUTPUT name="o_sys_opt3.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/vat_ind.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt3.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="STKLDGR_VAT_INCL_RETL_IND"/>
         <INPUT name="c_sys_opt4.v"/>
         <OUTPUT name="o_sys_opt4.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/stkldgr_vat_incl_retl_ind.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt4.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="MULTI_CURRENCY_IND"/>
         <INPUT name="c_sys_opt5.v"/>
         <OUTPUT name="o_sys_opt5.v"/>
      </OPERATOR>
   
      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/multi_currency_ind.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt5.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod"> 
         <PROPERTY  name="keep" value="CLASS_LEVEL_VAT_IND"/>
         <INPUT name="c_sys_opt6.v"/>
         <OUTPUT name="o_sys_opt6.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/class_level_vat_ind.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt6.v"/>
      </OPERATOR>
      
      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="DOMAIN_LEVEL"/>
         <INPUT name="c_sys_opt7.v"/>
         <OUTPUT name="o_sys_opt7.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/domain_level.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_opt7.v"/>
      </OPERATOR>
   
      <OPERATOR type="copy">
         <INPUT name="sys_var.v"/>
         <OUTPUT name="c_sys_var1.v"/>
         <OUTPUT name="c_sys_var2.v"/>
         <OUTPUT name="c_sys_var3.v"/>
         <OUTPUT name="c_sys_var4.v"/>
         <OUTPUT name="c_sys_var5.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod"> 
         <PROPERTY  name="keep" value="VDATE"/>
         <INPUT name="c_sys_var1.v"/>
         <OUTPUT name="o_sys_var1.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/vdate.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var1.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="NEXT_VDATE"/>
         <INPUT name="c_sys_var2.v"/>
         <OUTPUT name="o_sys_var2.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/next_vdate.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var2.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="LAST_EOM_DATE"/>
         <INPUT name="c_sys_var3.v"/>
         <OUTPUT name="o_sys_var3.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/last_eom_date.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var3.v"/>
      </OPERATOR>

      <OPERATOR type="fieldmod">
         <PROPERTY  name="keep" value="MAX_BACKPOST_DAYS"/>
         <INPUT name="c_sys_var4.v"/>
         <OUTPUT name="o_sys_var4.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/max_backpost_days.txt"/>
         <PROPERTY name="mode" value="overwrite"/>
         <INPUT name="o_sys_var4.v"/>
      </OPERATOR>

      <OPERATOR  type="fieldmod" >       
         <PROPERTY  name="keep" value="CURR_BOM_DATE"/>       
         <INPUT name="c_sys_var5.v"/>       
         <OUTPUT name="o_sys_var5.v"/>   
      </OPERATOR>   

      <OPERATOR type="export">       
         <PROPERTY name="outputfile" value="${ETC_DIR}/curr_bom_date.txt"/>       
         <INPUT name="o_sys_var5.v"/>   
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/last_extr_closed_pot_date.txt"/>
         <INPUT name="last_extr_closed_pot.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${ETC_DIR}/last_extr_received_pot_date.txt"/>
         <INPUT name="last_extr_received_pot.v"/>
      </OPERATOR>
   </FLOW>
EOF

########################################################
#  Execute the RETL flow file to read database-based
#  system parameters.
########################################################
${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE_1}

########################################################
#  RETL error check
########################################################
checkerror -e $? -m "Loading of all system variable from system options failed to complete"

########################################################
#  Verify non-zero-length output files were written
########################################################

for out in ${OUT_LIST} ; do
  if [[ ! -s ${ETC_DIR}/${out}.txt ]] ; then
    checkerror -e 1 -m "Error: ${out}.txt is either missing in ${ETC_DIR} or is a zero byte file"
  fi
done

########################################################
#  Load and transform file-based parameters
########################################################
VDATE=$(cat ${ETC_DIR}/vdate.txt)
CONSOLIDATION_CODE=$(cat ${ETC_DIR}/consolidation_code.txt)
PRIME_CURRENCY_CODE=$(cat ${ETC_DIR}/prime_currency_code.txt)
CONVERT_VDATE="TO_DATE('$VDATE','YYYYMMDD')"
LAST_EXTR_CLOSED_POT_DATE=$(cat ${ETC_DIR}/last_extr_closed_pot_date.txt)
LAST_EXTR_RECEIVED_POT_DATE=$(cat ${ETC_DIR}/last_extr_received_pot_date.txt)

########################################################
#  Create RETL flow in a logged file
#  This flow reads currency exchange information.
########################################################
FLOW_FILE_2="${LOG_DIR}/${PROGRAM_NAME}_2.xml"
cat > ${FLOW_FILE_2} << EOF 
   <FLOW name="${PROGRAM_NAME}_2.flw">
      ${DBREAD}
         <PROPERTY name="query">
            <![CDATA[ 
               SELECT cr.EXCHANGE_RATE PRIME_EXCHNG_RATE
               FROM ${RMS_OWNER}.CURRENCY_RATES cr
               WHERE cr.CURRENCY_CODE = '${PRIME_CURRENCY_CODE}'
                  AND cr.EXCHANGE_TYPE = '${CONSOLIDATION_CODE}'
                  AND cr.EFFECTIVE_DATE = 
                     (SELECT MAX(c2.EFFECTIVE_DATE)
                      FROM ${RMS_OWNER}.CURRENCY_RATES c2
                      WHERE c2.CURRENCY_CODE = cr.CURRENCY_CODE
                         AND c2.EXCHANGE_TYPE = '${CONSOLIDATION_CODE}'
                         AND c2.EFFECTIVE_DATE <= ${CONVERT_VDATE})
            ]]>      
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${ETC_DIR}/prime_exchng_rate.txt"/>
            <PROPERTY name="mode" value="overwrite"/>
         </OPERATOR>
      </OPERATOR>
   </FLOW>
EOF

########################################################
#  Execute the RETL flow file to read currency exchange
#  information.
########################################################
${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE_2}

########################################################
#  RETL error check
########################################################
checkerror -e $? -m "Extract of system variable for primary exchange rate failed to complete"

########################################################
#  Verify non-zero-length prime_exchng_rate file was 
#  written or override with default rate of 1.0 otherwise.
########################################################
if [[ ! -s ${ETC_DIR}/prime_exchng_rate.txt ]] ; then
  echo 1.0 > ${ETC_DIR}/prime_exchng_rate.txt
fi

########################################################
#  Remove the status file, if present
########################################################
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

########################################################
#  Successful completion!
########################################################
message "Program completed successfully"
