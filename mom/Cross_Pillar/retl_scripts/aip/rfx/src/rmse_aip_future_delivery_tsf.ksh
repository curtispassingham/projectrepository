#!/bin/ksh
########################################################
# Purpose: extracts RMS on-order and in-transit transfer
#          quantities for AIP future delivery
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME='rmse_aip_future_delivery_tsf'


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_future_delivery_tsf_reject_ord_mult.txt

export SQL_STATEMENT1="SELECT TRANSACTION_NUM,
                              t.DAY,
                              t.LOC,
                              t.ITEM,
                              CASE t.PRIMARY_CASE_SIZE 
                                 WHEN 'C' THEN rtrim(substr(isup.SUPP_PACK_SIZE,0,6),'.')
                                 WHEN 'I' THEN rtrim(substr(isup.INNER_PACK_SIZE,0,6),'.')
                                 WHEN 'P' THEN rtrim(substr(isup.TI * isup.HI * isup.SUPP_PACK_SIZE,0,6),'.')
                              ELSE '1'
                              END ORDER_MULTIPLE,
                              t.SUPPLIER,
                              rtrim(substr(SUM(t.TSF_QTY),0,8),'.') TSF_QTY,
                              rtrim(substr(SUM(t.IN_TRANSIT_TSF_QTY),0,8),'.') IN_TRANSIT_TSF_QTY,
                              rtrim(substr(SUM(t.ON_ORDER_TSF_QTY),0,8),'.') ON_ORDER_TSF_QTY,
                              t.LOC_TYPE,
                              t.TSF_TYPE
                         FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                              (SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0)),'YYYYMMDD') DAY,
                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                     THEN sif.TO_LOC
                                                     ELSE h.TO_LOC
                                                END
                                           ELSE h.TO_LOC
                                      END LOC,
                                      im.ITEM,
                                      isup.SUPPLIER,
                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                      isp.PRIMARY_CASE_SIZE,
                                      h.TO_LOC_TYPE LOC_TYPE,
                                      h.TSF_TYPE,
                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM
                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                      ${RMS_OWNER}.TSFDETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.ITEM_SUPPLIER isp,
                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                                      (SELECT t.DEPT,
                                              t.CLASS,
                                              nvl(t.SUBCLASS, -999) as SUBCLASS,
                                              t.ORIGIN_TYPE,
                                              wh1.WH as ORIGIN,
                                              t.DESTINATION_TYPE,
                                              wh2.WH as DESTINATION,
                                              t.TRANSIT_TIME
                                         FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                              ${RMS_OWNER}.WH wh1,
                                              ${RMS_OWNER}.WH wh2
                                        WHERE t.ORIGIN_TYPE = 'WH'
                                          AND t.ORIGIN = wh1.PHYSICAL_WH
                                          AND wh1.WH != wh1.PHYSICAL_WH
                                          AND t.DESTINATION_TYPE = 'WH'
                                          AND t.DESTINATION = wh2.PHYSICAL_WH
                                          AND wh2.WH != wh2.PHYSICAL_WH
                                       UNION
                                       SELECT t.DEPT,
                                              t.CLASS,
                                              nvl(t.SUBCLASS, -999) as SUBCLASS,
                                              t.ORIGIN_TYPE,
                                              wh1.WH as ORIGIN,
                                              t.DESTINATION_TYPE,
                                              t.DESTINATION,
                                              t.TRANSIT_TIME
                                         FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                              ${RMS_OWNER}.WH wh1
                                        WHERE t.ORIGIN_TYPE = 'WH'
                                          AND t.DESTINATION_TYPE = 'ST'
                                          AND t.ORIGIN = wh1.PHYSICAL_WH
                                          AND wh1.WH != wh1.PHYSICAL_WH
                                       UNION
                                       SELECT t.DEPT,
                                              t.CLASS,
                                              nvl(t.SUBCLASS, -999) as SUBCLASS,
                                              t.ORIGIN_TYPE,
                                              t.ORIGIN,
                                              t.DESTINATION_TYPE,
                                              t.DESTINATION,
                                              t.TRANSIT_TIME
                                         FROM ${RMS_OWNER}.TRANSIT_TIMES t
                                        WHERE t.ORIGIN_TYPE = 'ST'
                                          AND t.DESTINATION_TYPE = 'ST') t
                                WHERE h.TSF_NO=d.TSF_NO
                                  AND d.ITEM=im.ITEM
                                  AND d.ITEM=sif.ITEM(+)
                                  AND d.TSF_NO=sif.TSF_NO(+)
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=TRAN_LEVEL
                                  AND im.PACK_IND = 'N'
                                  AND im.FORECAST_IND = 'Y'
                                  AND h.APPROVAL_DATE IS NOT NULL
                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                  AND h.STATUS in ('A', 'S')
                                  AND h.TO_LOC_TYPE IN ('S','W')
                                  AND h.TO_LOC IS NOT NULL
                                  AND im.ITEM = isp.ITEM
                                  AND isp.PRIMARY_SUPP_IND = 'Y'
                                  AND isp.ITEM = isup.ITEM
                                  AND isp.SUPPLIER = isup.SUPPLIER
                                  AND isup.PRIMARY_COUNTRY_IND = 'Y'
                                  AND t.DEPT=im.DEPT
                                  AND t.CLASS=im.CLASS
                                  AND t.DESTINATION=h.TO_LOC
                                  AND t.ORIGIN=h.FROM_LOC
                                  AND im.AIP_CASE_TYPE = 'I'
                                  AND h.tsf_type != 'CO'
                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0))
                                        >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                             UNION
                               SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE ),'YYYYMMDD') DAY,
                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                     THEN sif.TO_LOC
                                                     ELSE h.TO_LOC
                                                END
                                           ELSE h.TO_LOC
                                      END LOC,
                                      im.ITEM,
                                      isup.SUPPLIER,
                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                      isp.PRIMARY_CASE_SIZE,
                                      h.TO_LOC_TYPE LOC_TYPE,
                                      h.TSF_TYPE,
                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM
                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                      ${RMS_OWNER}.TSFDETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.ITEM_SUPPLIER isp,
                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif
                                WHERE h.TSF_NO=d.TSF_NO
                                  AND d.ITEM=im.ITEM
                                  AND d.ITEM=sif.ITEM(+)
                                  AND d.TSF_NO=sif.TSF_NO(+)
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=TRAN_LEVEL
                                  AND im.PACK_IND = 'N'
                                  AND im.FORECAST_IND = 'Y'
                                  AND im.ITEM = isp.ITEM
                                  AND isp.PRIMARY_SUPP_IND = 'Y'
                                  AND isp.ITEM = isup.ITEM
                                  AND isp.SUPPLIER = isup.SUPPLIER
                                  AND isup.PRIMARY_COUNTRY_IND = 'Y'
                                  AND h.APPROVAL_DATE IS NOT NULL
                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                  AND h.STATUS in ('A', 'S')
                                  AND h.TO_LOC_TYPE IN ('S','W')
                                  AND h.TO_LOC IS NOT NULL
                                  AND im.AIP_CASE_TYPE = 'I'
                                  AND h.tsf_type != 'CO'
                                  AND NOT EXISTS (SELECT '1'
                                                    FROM (SELECT t.DEPT,
                                                                 t.CLASS,
                                                                 nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                                 t.ORIGIN_TYPE,
                                                                 wh1.WH as ORIGIN,
                                                                 t.DESTINATION_TYPE,
                                                                 wh2.WH as DESTINATION,
                                                                 t.TRANSIT_TIME
                                                            FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                                 ${RMS_OWNER}.WH wh1,
                                                                 ${RMS_OWNER}.WH wh2
                                                           WHERE t.ORIGIN_TYPE = 'WH'
                                                             AND t.ORIGIN = wh1.PHYSICAL_WH
                                                             AND wh1.WH != wh1.PHYSICAL_WH
                                                             AND t.DESTINATION_TYPE = 'WH'
                                                             AND t.DESTINATION = wh2.PHYSICAL_WH
                                                             AND wh2.WH != wh2.PHYSICAL_WH
                                                          UNION
                                                          SELECT t.DEPT,
                                                                 t.CLASS,
                                                                 nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                                 t.ORIGIN_TYPE,
                                                                 wh1.WH as ORIGIN,
                                                                 t.DESTINATION_TYPE,
                                                                 t.DESTINATION,
                                                                 t.TRANSIT_TIME
                                                            FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                                 ${RMS_OWNER}.WH wh1
                                                           WHERE t.ORIGIN_TYPE = 'WH'
                                                             AND t.DESTINATION_TYPE = 'ST'
                                                             AND t.ORIGIN = wh1.PHYSICAL_WH
                                                          UNION
                                                          SELECT t.DEPT,
                                                                 t.CLASS,
                                                                 nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                                 t.ORIGIN_TYPE,
                                                                 t.ORIGIN,
                                                                 t.DESTINATION_TYPE,
                                                                 t.DESTINATION,
                                                                 t.TRANSIT_TIME
                                                            FROM ${RMS_OWNER}.TRANSIT_TIMES t
                                                           WHERE t.ORIGIN_TYPE = 'ST'
                                                             AND t.DESTINATION_TYPE = 'ST') tt
                                                   WHERE tt.dept=im.dept
                                                     AND tt.class=im.class
                                                     AND tt.origin=h.from_loc
                                                     AND tt.destination=h.to_loc)
                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE)
                                         >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}) t
                        WHERE isup.item = t.item
                          AND isup.supplier = t.supplier
                          AND isup.primary_country_ind = 'Y'
                     GROUP BY TRANSACTION_NUM,
                              t.DAY,
                              t.LOC,
                              t.ITEM,
                              t.SUPPLIER,
                              t.LOC_TYPE,
                              t.TSF_TYPE,
                              isup.INNER_PACK_SIZE,
                              isup.SUPP_PACK_SIZE,
                              isup.TI,
                              isup.HI,
                              t.PRIMARY_CASE_SIZE"

export SQL_STATEMENT2="WITH TRAN_DATA AS
                        (
                           SELECT t.DEPT,
                                  t.CLASS,
                                  nvl(t.SUBCLASS, -999) as SUBCLASS,
                                  t.ORIGIN_TYPE,
                                  wh1.WH as ORIGIN,
                                  t.DESTINATION_TYPE,
                                  wh2.WH as DESTINATION,
                                  t.TRANSIT_TIME
                             FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                  ${RMS_OWNER}.WH wh1,
                                  ${RMS_OWNER}.WH wh2
                            WHERE t.ORIGIN_TYPE = 'WH'
                              AND t.ORIGIN = wh1.PHYSICAL_WH
                              AND wh1.WH != wh1.PHYSICAL_WH
                              AND t.DESTINATION_TYPE = 'WH'
                              AND t.DESTINATION = wh2.PHYSICAL_WH
                              AND wh2.WH != wh2.PHYSICAL_WH
                           UNION
                           SELECT t.DEPT,
                                  t.CLASS,
                                  nvl(t.SUBCLASS, -999) as SUBCLASS,
                                  t.ORIGIN_TYPE,
                                  wh1.WH as ORIGIN,
                                  t.DESTINATION_TYPE,
                                  t.DESTINATION,
                                  t.TRANSIT_TIME
                             FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                  ${RMS_OWNER}.WH wh1
                            WHERE t.ORIGIN_TYPE = 'WH'
                              AND t.DESTINATION_TYPE = 'ST'
                              AND t.ORIGIN = wh1.PHYSICAL_WH
                              AND wh1.WH != wh1.PHYSICAL_WH
                           UNION
                           SELECT t.DEPT,
                                  t.CLASS,
                                  nvl(t.SUBCLASS, -999) as SUBCLASS,
                                  t.ORIGIN_TYPE,
                                  t.ORIGIN,
                                  t.DESTINATION_TYPE,
                                  t.DESTINATION,
                                  t.TRANSIT_TIME
                             FROM ${RMS_OWNER}.TRANSIT_TIMES t
                            WHERE t.ORIGIN_TYPE = 'ST'
                              AND t.DESTINATION_TYPE = 'ST'
                        )
                       SELECT TRANSACTION_NUM,
                              a.DAY,
                              a.LOC,
                              a.ITEM,
                              rtrim(substr(a.ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                              a.SUPPLIER,
                              rtrim(substr(SUM(a.TSF_QTY * a.PACK_QTY),0,8),'.') TSF_QTY,
                              rtrim(substr(SUM(a.IN_TRANSIT_TSF_QTY * a.PACK_QTY),0,8),'.') IN_TRANSIT_TSF_QTY,
                              rtrim(substr(SUM(a.ON_ORDER_TSF_QTY * a.PACK_QTY),0,8),'.') ON_ORDER_TSF_QTY,
                              a.LOC_TYPE,
                              a.TSF_TYPE
                         FROM (SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0)),'YYYYMMDD') DAY,
                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                     THEN sif.TO_LOC
                                                     ELSE h.TO_LOC
                                                END
                                           ELSE h.TO_LOC
                                      END LOC,
                                      im.ITEM ITEM,
                                      vpq.QTY ORDER_MULTIPLE,
                                      isup.SUPPLIER,
                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                      h.TO_LOC_TYPE LOC_TYPE,
                                      h.TSF_TYPE,
                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM,
                                      vpq.QTY PACK_QTY
                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                      ${RMS_OWNER}.TSFDETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                      TRAN_DATA t
                                WHERE h.TSF_NO=d.TSF_NO
                                  AND d.ITEM=im.ITEM
                                  AND d.ITEM=sif.ITEM(+)
                                  AND d.TSF_NO=sif.TSF_NO(+)
                                  AND vpq.PACK_NO = im.ITEM
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                  AND im.SIMPLE_PACK_IND = 'Y' 
                                  AND im.item IN (SELECT pm.PACK_NO
                                                    FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                         ${RMS_OWNER}.PACKITEM pm
                                                   WHERE pm.ITEM = im2.ITEM
                                                     AND im2.FORECAST_IND = 'Y')
                                  AND h.APPROVAL_DATE IS NOT NULL
                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                  AND h.STATUS in ('A', 'S')
                                  AND h.TO_LOC_TYPE IN ('S','W')
                                  AND h.TO_LOC IS NOT NULL
                                  AND im.ITEM = isup.ITEM
                                  AND isup.PRIMARY_SUPP_IND = 'Y'
                                  AND isup.primary_country_ind = 'Y'
                                  AND t.DEPT=im.DEPT
                                  AND t.CLASS=im.CLASS
                                  AND t.DESTINATION=h.TO_LOC
                                  AND t.ORIGIN=h.FROM_LOC
                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                  AND h.tsf_type != 'CO'
                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0))
                                        >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                             UNION
                               SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE),'YYYYMMDD') DAY,
                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                     THEN sif.TO_LOC
                                                     ELSE h.TO_LOC
                                                END
                                           ELSE h.TO_LOC
                                      END LOC,
                                      im.ITEM ITEM,
                                      vpq.QTY ORDER_MULTIPLE,
                                      isup.SUPPLIER,
                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                      h.TO_LOC_TYPE LOC_TYPE,
                                      h.TSF_TYPE,
                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM,
                                      vpq.QTY PACK_QTY
                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                      ${RMS_OWNER}.TSFDETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif
                                WHERE h.TSF_NO=d.TSF_NO
                                  AND d.ITEM=im.ITEM
                                  AND d.ITEM=sif.ITEM(+)
                                  AND d.TSF_NO=sif.TSF_NO(+)
                                  AND vpq.PACK_NO = im.ITEM
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                  AND im.SIMPLE_PACK_IND = 'Y' 
                                  AND im.item IN (SELECT pm.PACK_NO
                                                    FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                         ${RMS_OWNER}.PACKITEM pm
                                                   WHERE pm.ITEM = im2.ITEM
                                                     AND im2.FORECAST_IND = 'Y')
                                  AND im.ITEM = isup.ITEM
                                  AND isup.PRIMARY_SUPP_IND = 'Y'
                                  AND isup.primary_country_ind = 'Y'
                                  AND h.APPROVAL_DATE IS NOT NULL
                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                  AND h.STATUS in ('A', 'S')
                                  AND h.TO_LOC_TYPE IN ('S','W')
                                  AND h.TO_LOC IS NOT NULL
                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                  AND h.tsf_type != 'CO'
                                  AND NOT EXISTS (SELECT '1'
                                                    FROM TRAN_DATA tt
                                                   WHERE tt.dept=im.dept
                                                     AND tt.class=im.class
                                                     AND tt.origin=h.from_loc
                                                     AND tt.destination=h.to_loc)
                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE)
                                         >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                 UNION
                                /*When regular item then*/
                                SELECT DAY, 
                                       LOC, 
                                       ITEM, 
                                       ORDER_MULTIPLE, 
                                       SUPPLIER, 
                                       TSF_QTY, 
                                       IN_TRANSIT_TSF_QTY,
                                       ON_ORDER_TSF_QTY, 
                                       LOC_TYPE, 
                                       TSF_TYPE,
                                       TRANSACTION_NUM, 
                                       PACK_QTY
                                  FROM (
                                        SELECT DAY, 
                                               LOC, 
                                               ITEM, 
                                               ORDER_MULTIPLE, 
                                               SUPPLIER, 
                                               TSF_QTY, 
                                               IN_TRANSIT_TSF_QTY,
                                               ON_ORDER_TSF_QTY, 
                                               LOC_TYPE, 
                                               TSF_TYPE,
                                               TRANSACTION_NUM, 
                                               PACK_QTY,
                                               SIZE_RANK, 
                                               QTY_RANK,
                                               RANK() OVER
                                                          (PARTITION BY ITEM ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                                          FROM
                                              (
                                              SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0)),'YYYYMMDD') DAY,
                                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                                     THEN sif.TO_LOC
                                                                     ELSE h.TO_LOC
                                                                END
                                                           ELSE h.TO_LOC
                                                      END LOC,
                                                      im2.ITEM ITEM,
                                                      vpq.QTY ORDER_MULTIPLE,
                                                      isup.SUPPLIER,
                                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                                      h.TO_LOC_TYPE LOC_TYPE,
                                                      h.TSF_TYPE,
                                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM,
                                                      1 PACK_QTY,
                                                      CASE WHEN isup.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                           WHEN isup.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                           ELSE 3 END SIZE_RANK,
                                                           RANK() OVER
                                                                      (PARTITION BY im2.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                                      ${RMS_OWNER}.TSFDETAIL d,
                                                      ${RMS_OWNER}.ITEM_MASTER im,
                                                      ${RMS_OWNER}.ITEM_MASTER im2,
                                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif,
                                                      TRAN_DATA t
                                                WHERE h.TSF_NO=d.TSF_NO
                                                  AND d.ITEM=im.ITEM
                                                  AND d.ITEM=sif.ITEM(+)
                                                  AND d.TSF_NO=sif.TSF_NO(+)
                                                  AND im.STATUS='A'
                                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                                  AND im.PACK_IND = 'N' 
                                                  AND im.FORECAST_IND = 'Y'
                                                  AND h.APPROVAL_DATE IS NOT NULL
                                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                                  AND h.STATUS in ('A', 'S')
                                                  AND h.TO_LOC_TYPE IN ('S','W')
                                                  AND h.TO_LOC IS NOT NULL
                                                  AND im2.ITEM = isup.ITEM
                                                  AND im2.SIMPLE_PACK_IND = 'Y'
                                                  AND vpq.PACK_NO = im2.ITEM
                                                  AND vpq.item = im.item
                                                  AND isup.PRIMARY_SUPP_IND = 'Y'
                                                  AND isup.primary_country_ind = 'Y'
                                                  AND t.DEPT=im.DEPT
                                                  AND t.CLASS=im.CLASS
                                                  AND t.DESTINATION=h.TO_LOC
                                                  AND t.ORIGIN=h.FROM_LOC
                                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                  AND h.tsf_type != 'CO'
                                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE + NVL(t.TRANSIT_TIME,0))
                                                        >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                             UNION
                                               SELECT 'D'|| TO_CHAR(NVL(h.DELIVERY_DATE,h.APPROVAL_DATE),'YYYYMMDD') DAY,
                                                      CASE WHEN h.TO_LOC_TYPE = 'W'
                                                           THEN CASE WHEN h.TSF_TYPE = 'EG'
                                                                     THEN sif.TO_LOC
                                                                     ELSE h.TO_LOC
                                                                END
                                                           ELSE h.TO_LOC
                                                      END LOC,
                                                      im2.ITEM ITEM,
                                                      vpq.QTY ORDER_MULTIPLE,
                                                      isup.SUPPLIER,
                                                      NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0) TSF_QTY,
                                                      NVL(d.SHIP_QTY,0)-NVL(d.RECEIVED_QTY,0) IN_TRANSIT_TSF_QTY,
                                                      NVL(d.TSF_QTY,0)-NVL(d.SHIP_QTY,0) ON_ORDER_TSF_QTY,
                                                      h.TO_LOC_TYPE LOC_TYPE,
                                                      h.TSF_TYPE,
                                                      decode(h.TO_LOC_TYPE,'W',h.tsf_no,null) TRANSACTION_NUM,
                                                      1 PACK_QTY,
                                                      CASE WHEN isup.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                           WHEN isup.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                           ELSE 3 END SIZE_RANK,
                                                           RANK() OVER
                                                                      (PARTITION BY im2.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                 FROM ${RMS_OWNER}.TSFHEAD h,
                                                      ${RMS_OWNER}.TSFDETAIL d,
                                                      ${RMS_OWNER}.ITEM_MASTER im,
                                                      ${RMS_OWNER}.ITEM_MASTER im2,
                                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isup,
                                                      ${RMS_OWNER}.SHIPITEM_INV_FLOW sif
                                                WHERE h.TSF_NO=d.TSF_NO
                                                  AND d.ITEM=im.ITEM
                                                  AND d.ITEM=sif.ITEM(+)
                                                  AND d.TSF_NO=sif.TSF_NO(+)
                                                  AND im.STATUS='A'
                                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                                  AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                                                       OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.PACK_NO
                                                                                                      FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                                                                           ${RMS_OWNER}.PACKITEM pm
                                                                                                     WHERE pm.ITEM = im2.ITEM
                                                                                                       AND im2.FORECAST_IND = 'Y')))
                                                  AND im.ITEM = isup.ITEM
                                                  AND isup.PRIMARY_SUPP_IND = 'Y'
                                                  AND isup.primary_country_ind = 'Y'
                                                  AND h.APPROVAL_DATE IS NOT NULL
                                                  AND im2.ITEM = isup.ITEM
                                                  AND im2.SIMPLE_PACK_IND = 'Y'
                                                  AND vpq.PACK_NO = im2.ITEM
                                                  AND vpq.item = im.item
                                                  AND (NVL(d.TSF_QTY,0)-NVL(d.RECEIVED_QTY,0))>0
                                                  AND h.STATUS in ('A', 'S')
                                                  AND h.TO_LOC_TYPE IN ('S','W')
                                                  AND h.TO_LOC IS NOT NULL
                                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                  AND h.tsf_type != 'CO'
                                                  AND NOT EXISTS (SELECT '1'
                                                                    FROM TRAN_DATA tt
                                                                   WHERE tt.dept=im.dept
                                                                     AND tt.class=im.class
                                                                     AND tt.origin=h.from_loc
                                                                     AND tt.destination=h.to_loc)
                                                  AND NVL(h.DELIVERY_DATE, h.APPROVAL_DATE)
                                                         >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                         )
                                       ) 
                                 WHERE PACK_RANK = 1
                                   AND (   SIZE_RANK = 1
                                        OR SIZE_RANK = 2
                                        OR (    SIZE_RANK = 3
                                            AND QTY_RANK  = 1))

                              ) a
                     GROUP BY TRANSACTION_NUM,
                              DAY,
                              LOC,
                              ITEM,
                              ORDER_MULTIPLE,
                              SUPPLIER,
                              LOC_TYPE,
                              TSF_TYPE"

message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE}.xml << EOF
                              
<FLOW name = "${PROGRAM_NAME}.flw">
   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT1}
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_tsf_nfp.v"/>
   </OPERATOR>

   <OPERATOR type = "filter">
      <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
      <PROPERTY name = "rejects" value = "true"/>
      <INPUT name = "future_delivery_tsf_nfp.v"/>
      <OUTPUT name = "future_delivery_tsf_w.v"/>
      <OUTPUT name = "future_delivery_tsf_s.v"/>
   </OPERATOR>

   <OPERATOR type = "funnel">
      <INPUT name = "future_delivery_tsf_w.v"/>
      <INPUT name = "future_delivery_tsf_s.v"/>
      <OUTPUT name = "future_delivery_tsf_nfp1.v"/>
   </OPERATOR>

   <OPERATOR type = "hash">
      <INPUT name = "future_delivery_tsf_nfp1.v"/>
      <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
      <PROPERTY name = "key" value = "LOC"/>
      <PROPERTY name = "key" value = "ITEM"/>
      <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
      <PROPERTY name = "key" value = "TSF_QTY"/>
      <PROPERTY name = "key" value = "IN_TRANSIT_TSF_QTY"/>
      <PROPERTY name = "key" value = "ON_ORDER_TSF_QTY"/>
      <OPERATOR type = "sort">
         <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
         <PROPERTY name = "key" value = "LOC"/>
         <PROPERTY name = "key" value = "ITEM"/>
         <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
         <PROPERTY name = "key" value = "TSF_QTY"/>
         <PROPERTY name = "key" value = "IN_TRANSIT_TSF_QTY"/>
         <PROPERTY name = "key" value = "ON_ORDER_TSF_QTY"/>
         <OPERATOR type = "removedup">
            <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
            <PROPERTY name = "key" value = "DAY"/>
            <PROPERTY name = "key" value = "LOC"/>
            <PROPERTY name = "key" value = "ITEM"/>
            <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
            <PROPERTY name = "keep" value = "last"/>
            <OUTPUT name = "future_delivery_tsf_nfp1a.v"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT    name="future_delivery_tsf_nfp1a.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="future_delivery_tsf_nfp1af_rej.v"/>
      <OUTPUT   name="future_delivery_tsf_nfp1af.v"/>
   </OPERATOR>

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT2}
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_tsf_fp.v"/>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT    name="future_delivery_tsf_fp.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="future_delivery_tsf_fpf_rej.v"/>
      <OUTPUT   name="future_delivery_tsf_fpf.v"/>
   </OPERATOR>

   <OPERATOR type = "funnel">
      <INPUT name = "future_delivery_tsf_nfp1af.v"/>
      <INPUT name = "future_delivery_tsf_fpf.v"/>
      <OUTPUT name = "future_delivery_tsf.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="future_delivery_tsf.v" />
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="TRANSACTION_NUM" sourcefield="TRANSACTION_NUM" newtype="int64">
                  <TYPEPROPERTY name="nullable" value="true"/>
               </CONVERT>
               <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
               <CONVERT destfield="LOC" sourcefield="LOC">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
                  <CONVERTFUNCTION name="int64_from_dfloat"/>
               </CONVERT>
               <CONVERT destfield="SUPPLIER" sourcefield="SUPPLIER">
                  <CONVERTFUNCTION name="int64_from_dfloat"/>
               </CONVERT>
               <CONVERT destfield="ITEM" sourcefield="ITEM">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="TSF_TYPE" sourcefield="TSF_TYPE">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="LOC_TYPE" sourcefield="LOC_TYPE">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="TSF_QTY" sourcefield="TSF_QTY" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="IN_TRANSIT_TSF_QTY" sourcefield="IN_TRANSIT_TSF_QTY" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
               <CONVERT destfield="ON_ORDER_TSF_QTY" sourcefield="ON_ORDER_TSF_QTY" newtype="string">
                  <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="0"/>
                  </CONVERTFUNCTION>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="future_delivery_tsf_nfp1af_rej.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_tsf_nfp1a_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="future_delivery_tsf_fpf_rej.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_tsf_fpf_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="future_delivery_tsf_nfp1a_rej_not_null.v"/>
      <INPUT name="future_delivery_tsf_fpf_rej_not_null.v"/>
      <OUTPUT name="reject_total.v"/>
   </OPERATOR>      

   <OPERATOR type="export">
       <INPUT name="reject_total.v"/>
       <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
