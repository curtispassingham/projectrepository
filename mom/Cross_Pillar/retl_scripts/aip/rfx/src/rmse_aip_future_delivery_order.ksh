#!/bin/ksh
########################################################
# Purpose: extracts RMS on order data for AIP
#          future delivery
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME='rmse_aip_future_delivery_order'


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_future_delivery_reject_order.txt

export SQL_STATEMENT1="SELECT TRANSACTION_NUM,
                              p.DAY,
                              p.SUPPLIER,
                              p.LOC,
                              p.ITEM,
                              CASE p.PRIMARY_CASE_SIZE 
                                 WHEN 'C' THEN rtrim(substr(isc.SUPP_PACK_SIZE,0,6),'.')
                                 WHEN 'I' THEN rtrim(substr(isc.INNER_PACK_SIZE,0,6),'.')
                                 WHEN 'P' THEN rtrim(substr(isc.TI * isc.HI * isc.SUPP_PACK_SIZE,0,6),'.')
                              ELSE '1'
                              END ORDER_MULTIPLE,
                              rtrim(substr(SUM(p.PO_QTY),0,8),'.') PO_QTY,
                              p.LOC_TYPE
                         FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                              (Select 'D'||TO_CHAR(o.not_after_date,'YYYYMMDD') DAY,
                                      o.supplier,
                                      o.location loc,
                                      im.item,
                                      o.qty_ordered-NVL(o.qty_received,0) po_qty,
                                      isp.PRIMARY_CASE_SIZE,
                                      o.loc_type,
                                      o.supp_pack_size,
                                      decode(o.loc_type,'W',o.order_no,null) TRANSACTION_NUM
                                 FROM ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.ITEM_SUPPLIER isp,
                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                      (SELECT h.not_after_date,
                                              l.order_no,
                                              h.supplier,
                                              l.location,
                                              l.loc_type,
                                              l.qty_ordered,
                                              l.qty_received,
                                              l.item,
                                              s.supp_pack_size
                                         FROM ${RMS_OWNER}.ORDHEAD h,
                                              ${RMS_OWNER}.ORDSKU s,
                                              ${RMS_OWNER}.ORDLOC l
                                        WHERE h.order_no=s.order_no
                                          AND h.order_type != 'CO'
                                          AND h.include_on_order_ind = 'Y'
                                          AND s.order_no = l.order_no
                                          AND s.item = l.item
                                          AND l.qty_ordered-NVL(l.qty_received,0)>0
                                          AND h.status='A'
                                          AND h.not_after_date >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                          AND l.loc_type IN ('S','W')) o
                                WHERE o.ITEM=im.item
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.tran_level
                                  AND im.PACK_IND = 'N'
                                  AND im.FORECAST_IND = 'Y'
                                  AND im.INVENTORY_IND = 'Y'
                                  AND NVL(im.DEPOSIT_ITEM_TYPE, 'X') != 'A'
                                  AND im.ITEM = isp.ITEM
                                  AND o.ITEM=isc.item
                                  AND o.SUPPLIER=isc.supplier
                                  AND o.SUPPLIER=isp.supplier
                                  AND im.AIP_CASE_TYPE = 'I'
                                  AND isc.PRIMARY_COUNTRY_IND='Y') p
                        WHERE isc.item = p.item
                          AND isc.supplier = p.supplier
                          AND isc.primary_country_ind = 'Y'
                     GROUP BY TRANSACTION_NUM,
                              p.DAY,
                              p.SUPPLIER,
                              p.LOC,
                              p.ITEM,
                              isc.INNER_PACK_SIZE,
                              isc.SUPP_PACK_SIZE,
                              isc.TI,
                              isc.HI,
                              p.PRIMARY_CASE_SIZE,
                              p.LOC_TYPE,
                              p.SUPP_PACK_SIZE"

export SQL_STATEMENT2="SELECT TRANSACTION_NUM,
                              DAY,
                              SUPPLIER,
                              LOC,
                              ITEM,
                              rtrim(substr(ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                              rtrim(substr(SUM(PO_QTY * PACK_QTY),0,8),'.') PO_QTY,
                              LOC_TYPE
                         FROM (/* When Simple Pack Ind = Y*/
                               Select 'D'||TO_CHAR(o.not_after_date,'YYYYMMDD') DAY,
                                      decode(o.loc_type,'W',o.order_no,null) TRANSACTION_NUM,
                                      o.supplier,
                                      o.location loc,
                                      im.item item,
                                      CASE WHEN o.loc_type='S'
                                           THEN 1
                                           WHEN o.loc_type='W'
                                           THEN CASE WHEN (o.qty_ordered-NVL(o.qty_received,0)) < isc.supp_pack_size
                                                     THEN 1
                                                     ELSE vpq.qty
                                                END
                                      END  order_multiple,
                                      o.qty_ordered-NVL(o.qty_received,0) po_qty,
                                      vpq.qty pack_qty,
                                      o.loc_type
                                 FROM ${RMS_OWNER}.item_master im,
                                      ${RMS_OWNER}.item_supp_country isc,
                                      ${RMS_OWNER}.v_packsku_qty vpq,
                                      (SELECT h.not_after_date,
                                              l.order_no,
                                              h.supplier,
                                              l.location,
                                              l.loc_type,
                                              l.qty_ordered,
                                              l.qty_received,
                                              l.item
                                         FROM ${RMS_OWNER}.ordhead h,
                                              ${RMS_OWNER}.ordloc l
                                        WHERE h.order_no=l.order_no
                                          AND h.include_on_order_ind = 'Y'
                                          AND h.order_type != 'CO'
                                          AND l.qty_ordered-NVL(l.qty_received,0)>0
                                          AND h.status='A'
                                          AND h.not_after_date >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                          AND l.loc_type IN ('S','W')) o
                                WHERE o.item=im.item
                                  AND im.status='A'
                                  AND NVL(im.deposit_item_type, 'X') != 'A'
                                  AND im.item_level=im.tran_level
                                  AND vpq.pack_no=im.ITEM
                                  AND im.SIMPLE_PACK_IND = 'Y'
                                  AND im.item IN (SELECT pm.PACK_NO
                                                          FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                               ${RMS_OWNER}.PACKITEM pm
                                                         WHERE pm.ITEM = im2.ITEM
                                                           AND im2.FORECAST_IND = 'Y')
                                  AND o.item=isc.item
                                  AND o.supplier=isc.supplier
                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                  AND isc.primary_country_ind='Y'
                                 UNION
                                   /* When regular item and other cases */
                                   SELECT DAY,
                                          TRANSACTION_NUM,
                                          SUPPLIER,
                                          LOC,
                                          ITEM,
                                          ORDER_MULTIPLE,
                                          PO_QTY,
                                          PACK_QTY,
                                          LOC_TYPE
                                     FROM (SELECT DAY,
                                                  TRANSACTION_NUM,
                                                  SUPPLIER,
                                                  LOC,
                                                  ITEM,
                                                  ORDER_MULTIPLE,
                                                  PO_QTY,
                                                  PACK_QTY,
                                                  LOC_TYPE,
                                                  SIZE_RANK,
                                                  QTY_RANK,
                                                  RANK() OVER
                                                             (PARTITION BY REG_ITEM ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                                             FROM
                                                 (
                                                 SELECT 'D'||TO_CHAR(o.not_after_date,'YYYYMMDD') DAY,
                                                      decode(o.loc_type,'W',o.order_no,null) TRANSACTION_NUM,
                                                      o.supplier,
                                                      o.location loc,
                                                      im.item reg_item,
                                                      im2.item item,
                                                      CASE WHEN o.loc_type='S'
                                                           THEN 1
                                                           WHEN o.loc_type='W'
                                                           THEN CASE WHEN (o.qty_ordered-NVL(o.qty_received,0)) < isc.supp_pack_size
                                                                     THEN 1
                                                                     ELSE vpq.qty
                                                                END
                                                      END  order_multiple,
                                                      o.qty_ordered-NVL(o.qty_received,0) po_qty,
                                                      1 pack_qty,
                                                      o.loc_type,
                                                      CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                             WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                             ELSE 3 END SIZE_RANK,
                                                             RANK() OVER
                                                                (PARTITION BY vpq.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                 FROM ${RMS_OWNER}.item_master im,
                                                      ${RMS_OWNER}.item_master im2,
                                                      ${RMS_OWNER}.item_supp_country isc,
                                                      ${RMS_OWNER}.v_packsku_qty vpq,
                                                      (SELECT h.not_after_date,
                                                              l.order_no,
                                                              h.supplier,
                                                              l.location,
                                                              l.loc_type,
                                                              l.qty_ordered,
                                                              l.qty_received,
                                                              l.item
                                                         FROM ${RMS_OWNER}.ordhead h,
                                                              ${RMS_OWNER}.ordloc l
                                                        WHERE h.order_no=l.order_no
                                                          AND h.include_on_order_ind = 'Y'
                                                          AND h.order_type != 'CO'
                                                          AND l.qty_ordered-NVL(l.qty_received,0)>0
                                                          AND h.status='A'
                                                          AND h.not_after_date >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                                          AND l.loc_type IN ('S','W')) o
                                                WHERE o.item=im.item
                                                  AND im.status='A'
                                                  AND NVL(im.deposit_item_type, 'X') != 'A'
                                                  AND im.item_level=im.tran_level
                                                  AND im.ITEM = vpq.ITEM
                                                  AND vpq.PACK_NO = im2.ITEM
                                                  AND im2.SIMPLE_PACK_IND = 'Y'
                                                  AND im.PACK_IND = 'N' 
                                                  AND im.FORECAST_IND = 'Y'
                                                  AND o.item=isc.item
                                                  AND o.supplier=isc.supplier
                                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                  AND isc.primary_country_ind='Y'
                                                  )
                                 )
                                    WHERE PACK_RANK = 1
                                      AND (   SIZE_RANK = 1
                                           OR SIZE_RANK = 2
                                           OR (    SIZE_RANK = 3
                                               AND QTY_RANK  = 1))
                                 )
                     GROUP BY TRANSACTION_NUM,
                              DAY,
                              SUPPLIER,
                              LOC,
                              ITEM,
                              ORDER_MULTIPLE,
                              LOC_TYPE"

message "Program started ..."

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               ${SQL_STATEMENT1}
            ]]>
         </PROPERTY>
         <OUTPUT name="future_delivery_order_nfp.v"/>
      </OPERATOR>

      <OPERATOR type = "funnel">
         <INPUT name = "future_delivery_order_nfp.v"/>
         <OUTPUT name = "future_deliver_order_nfp1.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
         <INPUT    name="future_deliver_order_nfp1.v"/>
         <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
         <PROPERTY name="rejects" value="true"/>
         <OUTPUT   name="reject_ord_multiple_nf.v"/>
         <OUTPUT   name="future_deliver_order_nfp1f.v"/>
      </OPERATOR>

      <OPERATOR type = "hash">
         <INPUT name = "future_deliver_order_nfp1f.v"/>
         <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
         <PROPERTY name = "key" value = "DAY"/>
         <PROPERTY name = "key" value = "SUPPLIER"/>
         <PROPERTY name = "key" value = "LOC"/>
         <PROPERTY name = "key" value = "ITEM"/>
         <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
         <OPERATOR type = "sort">
            <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
            <PROPERTY name = "key" value = "DAY"/>
            <PROPERTY name = "key" value = "SUPPLIER"/>
            <PROPERTY name = "key" value = "LOC"/>
            <PROPERTY name = "key" value = "ITEM"/>
            <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
            <OPERATOR type = "groupby">
               <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
               <PROPERTY name = "key" value = "DAY"/>
               <PROPERTY name = "key" value = "SUPPLIER"/>
               <PROPERTY name = "key" value = "LOC"/>
               <PROPERTY name = "key" value = "ITEM"/>
               <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
               <PROPERTY name = "reduce" value = "PO_QTY"/>
               <PROPERTY name = "sum" value = "PO_QTY"/>
               <PROPERTY name = "key" value = "LOC_TYPE"/>
               <OUTPUT name = "future_deliver_order_nfp1a.v"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>

      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               ${SQL_STATEMENT2}
            ]]>
         </PROPERTY>
         <OUTPUT name="future_deliver_order_fp.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
         <INPUT    name="future_deliver_order_fp.v"/>
         <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
         <PROPERTY name="rejects" value="true"/>
         <OUTPUT   name="reject_ord_multiple_f.v"/>
         <OUTPUT   name="future_deliver_order_fpf.v"/>
      </OPERATOR>

      <OPERATOR type = "funnel">
         <INPUT name = "future_deliver_order_nfp1a.v"/>
         <INPUT name = "future_deliver_order_fpf.v"/>
         <OUTPUT name = "future_deliver_order.v"/>
      </OPERATOR>

      <OPERATOR type="convert">
         <INPUT name="future_deliver_order.v" />
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="TRANSACTION_NUM" sourcefield="TRANSACTION_NUM" newtype="int64">
                     <TYPEPROPERTY name="nullable" value="true"/>
                  </CONVERT>

                  <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ITEM" sourcefield="ITEM">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
                  <CONVERT destfield="PO_QTY" sourcefield="PO_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC" sourcefield="LOC" newtype="int64">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC_TYPE" sourcefield="LOC_TYPE" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="SUPPLIER" sourcefield="SUPPLIER" newtype="int64">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="future_deliver_order1.v" />
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="future_deliver_order1.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="reject_ord_multiple_nf.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="reject_ord_multiple_nf_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="reject_ord_multiple_f.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="reject_ord_multiple_f_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="reject_ord_multiple_nf_rej_not_null.v"/>
      <INPUT name="reject_ord_multiple_f_rej_not_null.v"/>
      <OUTPUT name="reject_total.v"/>
   </OPERATOR>      

   <OPERATOR type="export">
       <INPUT name="reject_total.v"/>
       <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>

   </FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
