#!/bin/ksh

########################################################
# Script extracts all cancelled or closed              #
# purchased order and transfer items from              #
# ORDHEAD and TSFHEAD table in RMS                     #
########################################################

################## PROGRAM DEFINE #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_cl_po"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh

message "Program started ..."

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/closed_order.txt

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query" >
            <![CDATA[
               SELECT oh.ORDER_NO ORDER_NUMBER,
                      'P' ORDER_TYPE
                 FROM ${RMS_OWNER}.ORDHEAD oh
                WHERE (oh.CLOSE_DATE IS NOT NULL)
                  AND (oh.ORIG_IND='6')
                  AND (oh.CLOSE_DATE > to_date(${LAST_EXTR_CLOSED_POT_DATE},'yyyymmdd'))
                UNION
               SELECT tsf.TSF_NO ORDER_NUMBER,
                      'T' ORDER_TYPE
                 FROM ${RMS_OWNER}.TSFHEAD tsf
                WHERE (tsf.CLOSE_DATE IS NOT NULL)
                  AND (tsf.TSF_TYPE = 'AIP')
                  AND (tsf.CLOSE_DATE > to_date(${LAST_EXTR_CLOSED_POT_DATE},'yyyymmdd'))
                UNION
               SELECT ah.ALLOC_NO ORDER_NUMBER,
                      'A' ORDER_TYPE
                 FROM ${RMS_OWNER}.ALLOC_HEADER ah
                WHERE (ah.CLOSE_DATE IS NOT NULL)
                  AND (ah.ORIGIN_IND = 'AIP')
                  AND (ah.CLOSE_DATE > to_date(${LAST_EXTR_CLOSED_POT_DATE},'yyyymmdd'))
            ]]>
         </PROPERTY>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="ORDER_NUMBER" sourcefield="ORDER_NUMBER" newtype="int64">
                        <CONVERTFUNCTION name="int64_from_dfloat"/>
                        <TYPEPROPERTY name="nullable" value="false"/>
                     </CONVERT>
                     <CONVERT destfield="ORDER_TYPE" sourcefield="ORDER_TYPE">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value=" "/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OUTPUT name="po_transfer.v"/>
         </OPERATOR>
      </OPERATOR>
      <OPERATOR  type="export">
         <INPUT     name="po_transfer.v"/>
         <PROPERTY  name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY  name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR> 
   </FLOW>
EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into rmse_aip_cl_po.xml:
###############################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

#######################################################
#  Do error checking on results of the RETL execution:
#######################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

#######################################################
#  Update table when extraction was last performed
#######################################################

message "Update LAST_EXTR_CLOSED_POT_DATE in table RETL_EXTRACT_DATES"
upd_retl_pot_ext_date "CLOSED_ORDER"
checkerror -e $? -m "ERROR: update aip_retl_extract_dates failed - check $ERR_FILE"

#######################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#######################################################

if [ -f  ${STATUS_FILE} ]; then rm ${STATUS_FILE}; fi

message "Program completed succesfully"

#  Clean up and exit:

rmse_terminate 0
