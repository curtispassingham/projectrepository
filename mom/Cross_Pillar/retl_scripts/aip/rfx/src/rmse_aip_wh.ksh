#!/bin/ksh
########################################################
# Purpose: extracts RMS warehouse data and produce three
#          data file, one for RDF and two for AIP load
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_wh"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}_dat.schema
export AIP_OUTPUT_FILE1=${DATA_DIR}/rmse_aip_wh.txt
export AIP_OUTPUT_SCHEMA1=${SCHEMA_DIR}/rmse_aip_wh.schema
export AIP_OUTPUT_FILE2=${DATA_DIR}/rmse_aip_wh_type.txt
export AIP_OUTPUT_SCHEMA2=${SCHEMA_DIR}/rmse_aip_wh_type.schema


message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"
  
cat > ${FLOW_FILE}.xml << EOF
<FLOW name = "${PROGRAM_NAME}.flw">
   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT WH,
                   REPLACE(RTRIM(WH_NAME),CHR(10),'') WH_NAME,
                   FORECAST_WH_IND,
                   STOCKHOLDING_IND,
                   VWH_TYPE WH_TYPE
              FROM WH
         ]]>
      </PROPERTY>

      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="WH_NAME" sourcefield="WH_NAME">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR  type="copy">
            <OUTPUT name="rms_copy.v"/>
            <OUTPUT name="aip_copy.v"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR  type="fieldmod">
      <INPUT     name="aip_copy.v"/>
      <PROPERTY  name="rename" value="WAREHOUSE=WH"/>
      <PROPERTY  name="rename" value="WAREHOUSE_DESCRIPTION=WH_NAME"/>
      <OPERATOR  type="copy">
         <OUTPUT name="copy1.v"/>
         <OUTPUT name="copy2.v"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT name="copy1.v"/>
      <PROPERTY  name="filter" value="STOCKHOLDING_IND EQ 'Y'"/>
      <OPERATOR  type="fieldmod">
         <PROPERTY name="duplicate" value="WAREHOUSE_CHAMBER=WAREHOUSE"/>
         <PROPERTY name="duplicate" value="WAREHOUSE_CHAMBER_DESCRIPTION=WAREHOUSE_DESCRIPTION"/>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="WAREHOUSE_CHAMBER" sourcefield="WAREHOUSE_CHAMBER" newtype="string" >
                     <CONVERTFUNCTION name="string_from_int64" />
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OPERATOR type="export">
               <PROPERTY  name="schemafile" value="${AIP_OUTPUT_SCHEMA1}"/>
          <PROPERTY  name="outputfile" value="${AIP_OUTPUT_FILE1}"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>
   
   <OPERATOR type="filter">
      <INPUT name="copy2.v"/>
      <PROPERTY  name="filter" value="STOCKHOLDING_IND EQ 'Y'"/>
      <OPERATOR type="export">
         <PROPERTY  name="schemafile" value="${AIP_OUTPUT_SCHEMA2}"/>
         <PROPERTY  name="outputfile" value="${AIP_OUTPUT_FILE2}"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="rms_copy.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>
   
</FLOW>

EOF

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
