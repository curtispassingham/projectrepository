#!/bin/ksh
#######################################################
# Desription: The Program Extracts the data from RMS  
#             to the External System.
########################################################

################## PROGRAM DEFINES #####################

export PROGRAM_NAME="rmse_aip_item_loc_traits"

####################### INCLUDES ########################

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh

##################  OUTPUT DEFINES ######################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat 
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export RETL_FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

########################################################

message "Program started ..."

cat > ${RETL_FLOW_FILE} << EOF

<FLOW name = "${PROGRAM_NAME}.flw">
   ${DBREAD}
      <PROPERTY name="query">
         <![CDATA[  
            SELECT ilt.ITEM,
                   ilt.LOC,
                   ilt.REQ_SHELF_LIFE_ON_RECEIPT 
              FROM ${RMS_OWNER}.ITEM_LOC_TRAITS ilt, 
                   ${RMS_OWNER}.ITEM_MASTER im,
                   ${RMS_OWNER}.ITEM_LOC il
             WHERE im.ITEM = ilt.ITEM               
               AND im.STATUS='A'
               AND il.item=ilt.item
               AND il.loc=ilt.loc
               AND il.ranged_ind='Y'                
               AND ((im.PACK_IND = 'N' AND im.FORECAST_IND = 'Y')
                    OR (im.SIMPLE_PACK_IND = 'Y' AND im.item IN (SELECT pm.pack_no
                                                                   FROM item_master im1,
                                                                        packitem pm
                                                                  WHERE pm.item = im1.item
                                                                    AND im1.forecast_ind = 'Y')))
         ]]>
      </PROPERTY>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="REQ_SHELF_LIFE_ON_RECEIPT" 
                           sourcefield="REQ_SHELF_LIFE_ON_RECEIPT" newtype="int32" >
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT   name="read_item_loc_traits.v"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="read_item_loc_traits.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF
${RETL_EXE} ${RETL_OPTIONS} -f ${RETL_FLOW_FILE}

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

log_num_recs ${OUTPUT_FILE}

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
