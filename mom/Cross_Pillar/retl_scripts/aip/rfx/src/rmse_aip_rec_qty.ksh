#!/bin/ksh

########################################################
# Script extracts all received quantity of             #
# purchased order and transfer items from              #
# ORDHEAD and TSFHEAD table in RMS                     #
########################################################

################## PROGRAM DEFINE #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_rec_qty"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh

message "Program started ..."

# Set filename to trap the status of the sql script
STATUS=$LOG_DIR/$PROGRAM_NAME.$$
[ -f $STATUS ] && rm $STATUS

sqlplus -s $SQLPLUS_LOGON <<EOF

set heading off
set pause off
set serverout on
spool $STATUS

DECLARE

   L_dist_table        DISTRIBUTION_SQL.DIST_TABLE_TYPE;
   O_error_message     VARCHAR2(2000);
   L_result            BOOLEAN;
   L_location          VARCHAR2(1000);
   RAISE_ERROR         EXCEPTION;

   cursor C_GET_INTRANSIT_DETAILS is 
    with matching_orders as(
      select oh.order_no
        from ordhead oh
       where oh.orig_ind = '6'
         and (oh.close_date is null or
             oh.close_date >= (to_date('${VDATE}','yyyymmdd') - ${MAX_NOTAFTER_DAYS}))
         and oh.not_after_date is not null)
         select tran.order_no,
                tran.to_loc,
                tran.loc_type,
                tran.item,
                MAX(qty_intransit) qty_intransit
           from (
              (select s.order_no order_no,
                      s.to_loc to_loc,
                      s.to_loc_type loc_type,
                      sp.item item,
                      sum(sp.qty_expected) qty_intransit
                 from matching_orders mo, shipment s, shipsku sp
                where s.shipment = sp.shipment
                  and s.status_code = 'I'
                  and nvl(sp.qty_expected, 0) > 0
                  and s.order_no = mo.order_no
                group by s.order_no,
                         s.to_loc,
                         s.to_loc_type,
                         sp.item) 
               UNION ALL
              (select ad.doc order_no,
                      ah.loc to_loc,
                      ah.loc_type loc_type,
                      ad.item item,
                      sum(nvl(ad.qty_appointed, 0) - nvl(ad.qty_received, 0)) qty_intransit
                 from matching_orders mo,
                      appt_head       ah,
                      appt_detail     ad
                where ah.appt = ad.appt
                  and ah.status = 'SC' 
                  and ad.doc_type = 'P'
                  and (nvl(ad.qty_appointed, 0) - nvl(ad.qty_received, 0)) > 0
                  and ad.doc = mo.order_no
                group by ad.doc, 
                         ah.loc, 
                         ah.loc_type, 
                         ad.item)) tran
          group by tran.order_no,
                   tran.to_loc,
                   tran.loc_type,
                   tran.item;

   TYPE get_intransit_tbl IS TABLE OF C_GET_INTRANSIT_DETAILS%ROWTYPE INDEX BY BINARY_INTEGER;
   intransit_tbl get_intransit_tbl;

BEGIN
   L_location := 'Deleting temp_intransit_order_qty';
   delete temp_intransit_order_qty;
   ---
   L_location := 'Open C_GET_INTRANSIT_DETAILS';
   open C_GET_INTRANSIT_DETAILS;
   --
   L_location := 'Fetch C_GET_INTRANSIT_DETAILS';
   fetch C_GET_INTRANSIT_DETAILS bulk collect into intransit_tbl;
   --
   L_location := 'Close C_GET_INTRANSIT_DETAILS';
   close C_GET_INTRANSIT_DETAILS;

   if intransit_tbl.count > 0 then

      for i in intransit_tbl.first .. intransit_tbl.last LOOP
      
         if intransit_tbl(i).loc_type = 'W' then
         
            L_location := 'Calling DISTRIBUTION_SQL.DISTRIBUTE '; 
         
            L_result := DISTRIBUTION_SQL.DISTRIBUTE(O_error_message,
                                                    L_dist_table,
                                                    intransit_tbl(i).item, -- L_item
                                                    intransit_tbl(i).to_loc, -- I_loc: phy_loc
                                                    intransit_tbl(i).qty_intransit, -- I_qty
                                                    'ORDRCV', -- I_CMI: calling module indicator
                                                    NULL, -- I_inv_status
                                                    NULL, -- I_to_loc_type,
                                                    NULL, -- I_to_loc,
                                                    intransit_tbl(i).order_no, -- I_order_no
                                                    NULL, -- I_shipment
                                                    NULL);
            if L_result = FALSE then
               RAISE RAISE_ERROR;
            end if;
         
            for v_loc in L_dist_table.first .. L_dist_table.last LOOP
            
               if L_dist_table(v_loc).dist_qty > 0 then
               
                  insert into temp_intransit_order_qty
                     (order_no, item, location, qty_intransit)
                  values
                     (intransit_tbl(i).order_no, -- shipment
                      intransit_tbl(i).item, -- item
                      L_dist_table(v_loc).wh, -- location
                      L_dist_table(v_loc).dist_qty); -- qty_intransit
               
               end if; -- L_dist_table(v_loc).dist_qty > 0
            
            END LOOP; -- L_dist_table
         else
            -- intransit_tbl(i).loc_type = 'S'
            insert into temp_intransit_order_qty
               (order_no, item, location, qty_intransit)
            values
               (intransit_tbl(i).order_no, -- shipment
                intransit_tbl(i).item,     -- item
                intransit_tbl(i).to_loc,   -- location
                intransit_tbl(i).qty_intransit); -- qty_intransit
         end if; -- intransit_tbl(i).loc_type = 'W'
      
      END LOOP; --rcpt_tbl
   
   end if; --rcpt_tbl.count > 0

   COMMIT;

   dbms_output.put_line('SUCCESSFUL');
EXCEPTION
   when OTHERS then
      rollback;
      dbms_output.put_line('Error IN ' || L_location || '  Error = ' ||O_error_message);
end;
/
spool off
exit;
EOF

# Get program status from status file
if [ -r $STATUS ]; then
   while read line; do
      if [ "$line" = "SUCCESSFUL" ]
      then 
         PROGRAM_STATUS=$line
      fi 
   done < $STATUS
fi

# Removing status file
if [ -f $STATUS ]
then
   rm $STATUS
fi

if [ "$PROGRAM_STATUS" = "SUCCESSFUL" ]
then
   message "Running the flow"

   FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml
   
   ##################  OUTPUT DEFINES ######################
   ######## (this section must come after INCLUDES) ########
   
   export OUTPUT_FILE=${DATA_DIR}/received_qty.txt
   
   export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
   
   #############################################################
   #  Copy the RETL flow to an xml file to be executed by rfx:
   #############################################################

   cat > ${FLOW_FILE} << EOF

      <FLOW name = "${PROGRAM_NAME}.flw">
         ${DBREAD}
            <PROPERTY name = "query" >
               <![CDATA[
                  SELECT oh.ORDER_NO ORDER_NUMBER,
                         'P' ORDER_TYPE,
                         os.ITEM RMS_SKU,
                         rtrim(substr(os.SUPP_PACK_SIZE,0,6),'.') ORDER_MULTIPLE, 
                         rtrim(substr(NVL(pks.PACK_QTY, 0),0,6),'.') PACK_QTY,
                         case when ol.LOC_TYPE='S'
                            then ol.LOCATION
                         end STORE,
                         case when ol.LOC_TYPE='W'
                            then ol.LOCATION
                         end WAREHOUSE,
                         oh.NOT_AFTER_DATE RECEIVED_DATE,
                         substr(NVL(ol.QTY_RECEIVED,0),0,8) QUANTITY,
                         substr(NVL(itq.QTY_INTRANSIT, 0),0,8) INTRN_QUANTITY
                    FROM ${RMS_OWNER}.ordhead oh,
                         ${RMS_OWNER}.ordloc  ol,
                         ${RMS_OWNER}.ordsku  os,
                         ${RMS_OWNER}.temp_intransit_order_qty itq,
                         ${RMS_OWNER}.item_master im,
                         (SELECT PACK_NO ITEM,
                                 SUM(QTY) PACK_QTY
                            FROM ${RMS_OWNER}.V_PACKSKU_QTY
                           GROUP BY PACK_NO)  pks
                   WHERE (oh.ORDER_NO = ol.ORDER_NO)
                     AND (ol.ORDER_NO = os.ORDER_NO)
                     AND (ol.ITEM = os.ITEM)
                     AND (os.ITEM = im.ITEM)
                     AND (os.ITEM = pks.ITEM (+))
                     AND (oh.ORIG_IND = '6')
                     AND (ol.ORDER_NO = itq.ORDER_NO(+))
                     AND (ol.ITEM = itq.ITEM(+))
                     AND (ol.LOCATION = itq.LOCATION(+))
                     AND (ol.QTY_RECEIVED IS NOT NULL or itq.QTY_INTRANSIT IS NOT NULL)
                     AND (oh.CLOSE_DATE IS NULL
                         OR oh.CLOSE_DATE >= (to_date('${VDATE}','yyyymmdd') - ${MAX_NOTAFTER_DAYS})) 
                     AND oh.NOT_AFTER_DATE IS NOT NULL
                     AND im.INVENTORY_IND = 'Y'
                   UNION
                  SELECT th.TSF_NO ORDER_NUMBER,
                         'T' ORDER_TYPE,
                         td.ITEM RMS_SKU,
                         rtrim(substr(td.SUPP_PACK_SIZE,0,6),'.') ORDER_MULTIPLE,
                         rtrim(substr(NVL(pks.PACK_QTY, 0),0,6),'.') PACK_QTY,
                         case when th.TO_LOC_TYPE='S'
                            then th.TO_LOC
                         end STORE,
                         case when th.TO_LOC_TYPE='W'
                            then th.TO_LOC
                         end WAREHOUSE,
                         th.DELIVERY_DATE RECEIVED_DATE,
                         substr(td.RECEIVED_QTY,0,8) QUANTITY,
                         '0' INTRN_QUANTITY
                    FROM ${RMS_OWNER}.tsfhead               th,
                         ${RMS_OWNER}.tsfdetail             td,
                         ${RMS_OWNER}.item_master           im,
                         (SELECT PACK_NO ITEM,
                                 SUM(QTY) PACK_QTY
                            FROM ${RMS_OWNER}.V_PACKSKU_QTY
                           GROUP BY PACK_NO)                pks
                   WHERE (th.TSF_NO = td.TSF_NO)
                     AND (td.ITEM = im.ITEM)
                     AND (td.ITEM = pks.ITEM (+))
                     AND (th.TSF_TYPE = 'AIP')
                     AND td.RECEIVED_QTY IS NOT NULL
                     AND (th.CLOSE_DATE IS NULL
                         OR th.CLOSE_DATE >= (to_date('${VDATE}','yyyymmdd')-${MAX_NOTAFTER_DAYS}))
                     AND th.DELIVERY_DATE IS NOT NULL
                     AND im.INVENTORY_IND = 'Y'
                   UNION
                  SELECT ah.ALLOC_NO ORDER_NUMBER,
                         'A' ORDER_TYPE,
                         ah.ITEM RMS_SKU,
                         rtrim(substr(os.SUPP_PACK_SIZE,0,6),'.') ORDER_MULTIPLE,
                         rtrim(substr(NVL(pks.PACK_QTY, 0),0,6),'.') PACK_QTY,
                         case when ad.TO_LOC_TYPE='S'
                            then ad.TO_LOC
                         end STORE,
                         case when ad.TO_LOC_TYPE='W'
                            then ad.TO_LOC
                         end WAREHOUSE,
                         ah.RELEASE_DATE RECEIVED_DATE,
                         substr(ad.QTY_RECEIVED,0,8) QUANTITY,
                         '0' INTRN_QUANTITY
                    FROM ${RMS_OWNER}.alloc_header          ah,
                         ${RMS_OWNER}.alloc_detail          ad,
                         ${RMS_OWNER}.item_master           im,
                         ${RMS_OWNER}.ordsku                os,
                         (SELECT PACK_NO ITEM,
                                 SUM(QTY) PACK_QTY
                            FROM ${RMS_OWNER}.V_PACKSKU_QTY
                           GROUP BY PACK_NO)                pks
                   WHERE (ah.ALLOC_NO = ad.ALLOC_NO)
                     AND (ah.ITEM = im.ITEM)
                     AND (ah.ITEM = pks.ITEM (+))
                     AND (ah.ORIGIN_IND = 'AIP')
                     AND (ah.ORDER_NO = os.ORDER_NO)
                     AND (ah.ITEM = os.ITEM)
                     AND ad.QTY_RECEIVED IS NOT NULL
                     AND (ah.CLOSE_DATE IS NULL
                         OR ah.CLOSE_DATE >= (to_date('${VDATE}','yyyymmdd')-${MAX_NOTAFTER_DAYS}))
                     AND ah.RELEASE_DATE IS NOT NULL
                     AND ah.ORDER_NO IS NOT NULL
                     AND im.INVENTORY_IND = 'Y'
               ]]>
            </PROPERTY>
      
            <OPERATOR type="convert">
               <PROPERTY name="convertspec">
                  <![CDATA[
                     <CONVERTSPECS>
                        <CONVERT destfield="ORDER_TYPE" sourcefield="ORDER_TYPE">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value=" "/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="RMS_SKU" sourcefield="RMS_SKU">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value=" "/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="RECEIVED_DATE" sourcefield="RECEIVED_DATE">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value="00000101"/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ORDER_NUMBER" sourcefield="ORDER_NUMBER" newtype="int64">
                           <CONVERTFUNCTION name="int64_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/> 
                        </CONVERT>
                        <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                           <CONVERTFUNCTION name="string_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="PACK_QTY" sourcefield="PACK_QTY" newtype="string">
                           <CONVERTFUNCTION name="string_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="STORE" sourcefield="STORE" newtype="int64">
                           <CONVERTFUNCTION name="int64_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="WAREHOUSE" sourcefield="WAREHOUSE" newtype="int64">
                           <CONVERTFUNCTION name="int64_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="QUANTITY" sourcefield="QUANTITY" newtype="string">
                           <CONVERTFUNCTION name="string_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="INTRN_QUANTITY" sourcefield="INTRN_QUANTITY" newtype="string">
                           <CONVERTFUNCTION name="string_from_dfloat"/>
                           <TYPEPROPERTY name="nullable" value="true"/>
                        </CONVERT>
                        <CONVERT destfield="ORDER_NUMBER" sourcefield="ORDER_NUMBER">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value="-999"/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value=""/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                        <CONVERT destfield="PACK_QTY" sourcefield="PACK_QTY">
                           <CONVERTFUNCTION name="make_not_nullable">
                              <FUNCTIONARG name="nullvalue" value="0"/>
                           </CONVERTFUNCTION>
                        </CONVERT>
                     </CONVERTSPECS>
                  ]]>
               </PROPERTY>
               <OUTPUT name="received_transfer.v"/>
            </OPERATOR>
         </OPERATOR>
         <OPERATOR  type="export">
            <INPUT     name="received_transfer.v"/>
            <PROPERTY  name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY  name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR> 
      </FLOW>
EOF

   ###############################################################################
   #  Execute the flow
   ###############################################################################
   
   ${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}
   
   ###############################################################################
   #  Handle RETL errors
   ###############################################################################
   
   checkerror -e $? -m "Program failed - check ${ERR_FILE}"
   
   # Remove the status file
   if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi
   
   message "Program completed successfully"
   
   # cleanup and exit
   rmse_terminate 0

else
   message "Error populating temp_intransit_order_qty"
   # cleanup and exit
   rmse_terminate 0
fi
