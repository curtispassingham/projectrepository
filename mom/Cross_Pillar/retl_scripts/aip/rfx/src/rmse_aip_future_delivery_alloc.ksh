#!/bin/ksh
########################################################
# Purpose: extracts RMS allocation data for AIP
#          future delivery
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_future_delivery_alloc"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_ORD_MULT_FILE=${DATA_DIR}/rmse_aip_future_delivery_alloc_reject_ord_mult.txt

export SQL_STATEMENT1="SELECT TRANSACTION_NUM,
                              a.DAY,
                              a.LOC,
                              a.TO_LOC_TYPE LOC_TYPE,
                              a.ITEM,
                              CASE a.PRIMARY_CASE_SIZE
                                 WHEN 'C' THEN rtrim(substr(isc.SUPP_PACK_SIZE,0,6),'.')
                                 WHEN 'I' THEN rtrim(substr(isc.INNER_PACK_SIZE,0,6),'.')
                                 WHEN 'P' THEN rtrim(substr(isc.TI * isc.HI * isc.SUPP_PACK_SIZE,0,6),'.')
                                 ELSE '1'
                              END ORDER_MULTIPLE,
                              a.SUPPLIER,
                              rtrim(substr(SUM(a.IN_TRANSIT_ALLOC_QTY),0,8),'.') IN_TRANSIT_ALLOC_QTY,
                              rtrim(substr(SUM(a.ON_ORDER_ALLOC_QTY),0,8),'.') ON_ORDER_ALLOC_QTY
                         FROM ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                              (SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                      'D'|| TO_CHAR(h.RELEASE_DATE+NVL(TRANSIT_TIME,0),'YYYYMMDD') DAY,
                                      h.RELEASE_DATE+NVL(TRANSIT_TIME,0) D_DAY,
                                      decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                      d.TO_LOC LOC,
                                      d.TO_LOC_TYPE,
                                      im.ITEM,
                                      DECODE(h.ORDER_NO,
                                             NULL,
                                             (SELECT isup.SUPPLIER
                                                FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                               WHERE isup.ITEM = im.ITEM
                                                 AND isup.PRIMARY_SUPP_IND = 'Y'),
                                             (SELECT ordh.SUPPLIER
                                                FROM ${RMS_OWNER}.ORDHEAD ordh
                                               WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                      NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                      d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                      isp.PRIMARY_CASE_SIZE,
                                      h.ALLOC_NO
                                 FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                      ${RMS_OWNER}.ALLOC_DETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.ITEM_SUPPLIER isp,
                                      (SELECT t.DEPT,
                                              t.CLASS,
                                              nvl(t.SUBCLASS, -999) as SUBCLASS,
                                              t.ORIGIN_TYPE,
                                              wh1.WH as ORIGIN,
                                              t.DESTINATION_TYPE,
                                              wh2.WH as DESTINATION,
                                              t.TRANSIT_TIME
                                         FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                              ${RMS_OWNER}.V_WH wh1,
                                              ${RMS_OWNER}.V_WH wh2
                                        WHERE t.ORIGIN_TYPE = 'WH'
                                          AND t.ORIGIN = wh1.PHYSICAL_WH
                                          AND wh1.WH != wh1.PHYSICAL_WH
                                          AND t.DESTINATION_TYPE = 'WH'
                                          AND t.DESTINATION = wh2.PHYSICAL_WH
                                          AND wh2.WH != wh2.PHYSICAL_WH
                                       UNION
                                       SELECT t.DEPT,
                                              t.CLASS,
                                              nvl(t.SUBCLASS, -999) as SUBCLASS,
                                              t.ORIGIN_TYPE,
                                              wh1.WH as ORIGIN,
                                              t.DESTINATION_TYPE,
                                              t.DESTINATION,
                                              t.TRANSIT_TIME
                                         FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                              ${RMS_OWNER}.V_WH wh1
                                        WHERE t.ORIGIN_TYPE = 'WH'
                                          AND t.DESTINATION_TYPE = 'ST'
                                          AND t.ORIGIN = wh1.PHYSICAL_WH
                                          AND wh1.WH != wh1.PHYSICAL_WH) t
                                WHERE h.ALLOC_NO=d.ALLOC_NO
                                  AND (h.ORDER_NO IS NULL OR
                                       NOT EXISTS (SELECT '1'
                                                     FROM ${RMS_OWNER}.ORDHEAD i
                                                    WHERE i.order_type = 'CO'
                                                      AND i.order_no=h.order_no))
                                  AND h.ITEM=im.ITEM
                                  AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                  AND h.RELEASE_DATE IS NOT NULL
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                  AND im.PACK_IND = 'N'
                                  AND im.FORECAST_IND = 'Y'
                                  AND im.ITEM = isp.ITEM
                                  AND isp.PRIMARY_SUPP_IND = 'Y'
                                  AND h.STATUS in ('A', 'R')
                                  AND t.DEPT=im.DEPT
                                  AND t.CLASS=im.CLASS
                                  AND t.DESTINATION=d.TO_LOC
                                  AND t.ORIGIN=h.wh
                                  AND im.AIP_CASE_TYPE = 'I'
                                  AND h.RELEASE_DATE+NVL(TRANSIT_TIME,0) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                             UNION
                              SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                     'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD') DAY,                                     
                                     h.RELEASE_DATE D_DAY,
                                     decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                     d.TO_LOC LOC,
                                     d.TO_LOC_TYPE,
                                     im.ITEM,
                                     DECODE(h.ORDER_NO,
                                            NULL,
                                            (SELECT isup.SUPPLIER
                                               FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                              WHERE isup.ITEM = im.ITEM
                                                AND isup.PRIMARY_SUPP_IND = 'Y'),
                                            (SELECT ordh.SUPPLIER
                                               FROM ${RMS_OWNER}.ORDHEAD ordh
                                              WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                     NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                     d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                     isp.PRIMARY_CASE_SIZE,
                                     h.ALLOC_NO
                                FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                     ${RMS_OWNER}.ALLOC_DETAIL d,
                                     ${RMS_OWNER}.ITEM_MASTER im,
                                     ${RMS_OWNER}.ITEM_SUPPLIER isp
                               WHERE h.ALLOC_NO=d.ALLOC_NO
                                 AND (h.ORDER_NO IS NULL OR
                                      NOT EXISTS (SELECT '1'
                                                    FROM ${RMS_OWNER}.ORDHEAD i
                                                   WHERE i.order_type = 'CO'
                                                     AND i.order_no=h.order_no))
                                 AND h.ITEM=im.ITEM
                                 AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                 AND h.RELEASE_DATE IS NOT NULL
                                 AND im.STATUS='A'
                                 AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                 AND im.PACK_IND = 'N'
                                 AND im.FORECAST_IND = 'Y'
                                 AND im.AIP_CASE_TYPE = 'I'
                                 AND im.ITEM = isp.ITEM
                                 AND isp.PRIMARY_SUPP_IND = 'Y'
                                 AND h.STATUS in ('A','R')
                                 AND NOT EXISTS (SELECT '1'
                                                   FROM (SELECT t.DEPT,
                                                                t.CLASS,
                                                                nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                                t.ORIGIN_TYPE,
                                                                wh1.WH as ORIGIN,
                                                                t.DESTINATION_TYPE,
                                                                wh2.WH as DESTINATION,
                                                                t.TRANSIT_TIME
                                                           FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                                ${RMS_OWNER}.V_WH wh1,
                                                                ${RMS_OWNER}.V_WH wh2
                                                          WHERE t.ORIGIN_TYPE = 'WH'
                                                            AND t.ORIGIN = wh1.PHYSICAL_WH
                                                            AND wh1.WH != wh1.PHYSICAL_WH
                                                            AND t.DESTINATION_TYPE = 'WH'
                                                            AND t.DESTINATION = wh2.PHYSICAL_WH
                                                            AND wh2.WH != wh2.PHYSICAL_WH
                                                         UNION
                                                         SELECT t.DEPT,
                                                                t.CLASS,
                                                                nvl(t.SUBCLASS, -999) as SUBCLASS,
                                                                t.ORIGIN_TYPE,
                                                                wh1.WH as ORIGIN,
                                                                t.DESTINATION_TYPE,
                                                                t.DESTINATION,
                                                                t.TRANSIT_TIME
                                                           FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                                                ${RMS_OWNER}.V_WH wh1
                                                          WHERE t.ORIGIN_TYPE = 'WH'
                                                            AND t.DESTINATION_TYPE = 'ST'
                                                            AND t.ORIGIN = wh1.PHYSICAL_WH
                                                            AND wh1.WH != wh1.PHYSICAL_WH) i
                                                  WHERE i.class=im.class
                                                    AND i.dept=im.dept
                                                    AND i.destination=d.to_loc
                                                    AND i.origin=h.wh)
                                 AND h.RELEASE_DATE >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}) a
                        WHERE isc.item = a.item
                          AND isc.supplier = a.supplier
                          AND isc.primary_country_ind = 'Y'
                     GROUP BY TRANSACTION_NUM,
                              a.DAY,
                              a.LOC,
                              a.ITEM,
                              a.SUPPLIER,
                              a.TO_LOC_TYPE,
                              isc.INNER_PACK_SIZE,
                              isc.TI,
                              isc.HI,
                              isc.SUPP_PACK_SIZE,
                              a.PRIMARY_CASE_SIZE"

export SQL_STATEMENT2=" WITH TRAN_DATA AS 
                        (
                          SELECT t.DEPT,
                                 t.CLASS,
                                 nvl(t.SUBCLASS, -999) as SUBCLASS,
                                 t.ORIGIN_TYPE,
                                 wh1.WH as ORIGIN,
                                 t.DESTINATION_TYPE,
                                 wh2.WH as DESTINATION,
                                 t.TRANSIT_TIME
                            FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                 ${RMS_OWNER}.V_WH wh1,
                                 ${RMS_OWNER}.V_WH wh2
                           WHERE t.ORIGIN_TYPE = 'WH'
                             AND t.ORIGIN = wh1.PHYSICAL_WH
                             AND wh1.WH != wh1.PHYSICAL_WH
                             AND t.DESTINATION_TYPE = 'WH'
                             AND t.DESTINATION = wh2.PHYSICAL_WH
                             AND wh2.WH != wh2.PHYSICAL_WH
                          UNION
                          SELECT t.DEPT,
                                 t.CLASS,
                                 nvl(t.SUBCLASS, -999) as SUBCLASS,
                                 t.ORIGIN_TYPE,
                                 wh1.WH as ORIGIN,
                                 t.DESTINATION_TYPE,
                                 t.DESTINATION,
                                 t.TRANSIT_TIME
                            FROM ${RMS_OWNER}.TRANSIT_TIMES t,
                                 ${RMS_OWNER}.V_WH wh1
                           WHERE t.ORIGIN_TYPE = 'WH'
                             AND t.DESTINATION_TYPE = 'ST'
                             AND t.ORIGIN = wh1.PHYSICAL_WH
                             AND wh1.WH != wh1.PHYSICAL_WH
                        )
                        SELECT TRANSACTION_NUM,
                              a.DAY,
                              a.LOC,
                              a.TO_LOC_TYPE LOC_TYPE,
                              a.ITEM,
                              rtrim(substr(a.ORDER_MULTIPLE,0,6),'.') ORDER_MULTIPLE,
                              a.SUPPLIER,
                              rtrim(substr(SUM(a.IN_TRANSIT_ALLOC_QTY * a.PACK_QTY),0,8),'.') IN_TRANSIT_ALLOC_QTY,
                              rtrim(substr(SUM(a.ON_ORDER_ALLOC_QTY * a.PACK_QTY),0,8),'.') ON_ORDER_ALLOC_QTY
                         FROM (/*When Simple Pack Ind = Y*/
                               SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                      'D'|| TO_CHAR(h.RELEASE_DATE+NVL(TRANSIT_TIME,0),'YYYYMMDD') DAY,
                                      h.RELEASE_DATE+NVL(TRANSIT_TIME,0) D_DAY,
                                      decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                      d.TO_LOC LOC,
                                      im.ITEM ITEM,
                                      vpq.QTY ORDER_MULTIPLE,
                                      DECODE(h.ORDER_NO,
                                             NULL, (SELECT isup.SUPPLIER
                                                      FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                                     WHERE isup.ITEM = im.ITEM
                                                       AND isup.PRIMARY_SUPP_IND = 'Y'),
                                             (SELECT ordh.SUPPLIER
                                                FROM ${RMS_OWNER}.ORDHEAD ordh
                                               WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                      NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                      d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                      vpq.QTY PACK_QTY,
                                      im.SIMPLE_PACK_IND,
                                      d.TO_LOC_TYPE,
                                      h.ALLOC_NO
                                 FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                      ${RMS_OWNER}.ALLOC_DETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                      TRAN_DATA t
                                WHERE h.ALLOC_NO=d.ALLOC_NO
                                  AND (h.ORDER_NO IS NULL OR
                                       NOT EXISTS (SELECT '1'
                                                     FROM ${RMS_OWNER}.ORDHEAD i
                                                    WHERE i.order_type = 'CO'
                                                      AND i.order_no=h.order_no))
                                  AND h.ITEM=im.ITEM
                                  AND vpq.PACK_NO = im.item
                                  AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                  AND h.RELEASE_DATE IS NOT NULL
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                  AND im.SIMPLE_PACK_IND = 'Y' 
                                  AND im.item IN (SELECT pm.PACK_NO
                                                    FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                         ${RMS_OWNER}.PACKITEM pm
                                                   WHERE pm.ITEM = im2.ITEM
                                                     AND im2.FORECAST_IND = 'Y')
                                  AND h.STATUS in ('A', 'R')
                                  AND t.DEPT=im.DEPT
                                  AND t.CLASS=im.CLASS
                                  AND t.DESTINATION=d.TO_LOC
                                  AND t.ORIGIN=h.wh
                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                  AND h.RELEASE_DATE+NVL(TRANSIT_TIME,0) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                             UNION
                               SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                      'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD') DAY,                                      
                                      h.RELEASE_DATE D_DAY,
                                      decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                      d.TO_LOC LOC,
                                      im.ITEM ITEM,
                                      vpq.QTY ORDER_MULTIPLE,
                                      DECODE(h.ORDER_NO,
                                             NULL, (SELECT isup.SUPPLIER
                                                      FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                                     WHERE isup.ITEM = im.ITEM
                                                       AND isup.PRIMARY_SUPP_IND = 'Y'),
                                             (SELECT ordh.SUPPLIER
                                                FROM ${RMS_OWNER}.ORDHEAD ordh
                                               WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                      NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                      d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                      vpq.QTY PACK_QTY,
                                      im.SIMPLE_PACK_IND,
                                      d.TO_LOC_TYPE,
                                      h.ALLOC_NO
                                 FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                      ${RMS_OWNER}.ALLOC_DETAIL d,
                                      ${RMS_OWNER}.ITEM_MASTER im,
                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq
                                WHERE h.ALLOC_NO=d.ALLOC_NO
                                  AND (h.ORDER_NO IS NULL OR
                                       NOT EXISTS (SELECT '1'
                                                     FROM ${RMS_OWNER}.ORDHEAD i
                                                    WHERE i.order_type = 'CO'
                                                      AND i.order_no=h.order_no))
                                  AND h.ITEM=im.ITEM
                                  AND vpq.PACK_NO = im.item
                                  AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                  AND h.RELEASE_DATE IS NOT NULL
                                  AND im.STATUS='A'
                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                  AND im.SIMPLE_PACK_IND = 'Y' 
                                  AND im.item IN (SELECT pm.PACK_NO
                                                    FROM ${RMS_OWNER}.ITEM_MASTER im2,
                                                         ${RMS_OWNER}.PACKITEM pm
                                                   WHERE pm.ITEM = im2.ITEM
                                                     AND im2.FORECAST_IND = 'Y')
                                  AND h.STATUS in ('A','R')
                                  AND NOT EXISTS (SELECT '1'
                                                    FROM TRAN_DATA i
                                                   WHERE i.class=im.class
                                                     AND i.dept=im.dept
                                                     AND i.destination=d.to_loc
                                                     AND i.origin=h.wh)
                                  AND h.RELEASE_DATE >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                UNION
                                /*When regular item then*/
                                SELECT inner.DAY, 
                                       inner.D_DAY, 
                                       inner.TRANSACTION_NUM, 
                                       inner.LOC, 
                                       inner.ITEM, 
                                       inner.ORDER_MULTIPLE, 
                                       inner.SUPPLIER, 
                                       inner.IN_TRANSIT_ALLOC_QTY, 
                                       inner.ON_ORDER_ALLOC_QTY,
                                       inner.PACK_QTY, 
                                       inner.SIMPLE_PACK_IND, 
                                       inner.TO_LOC_TYPE,
                                       inner.ALLOC_NO
                                  FROM (
                                        SELECT DAY, 
                                               D_DAY, 
                                               TRANSACTION_NUM, 
                                               LOC, 
                                               ITEM, 
                                               ORDER_MULTIPLE, 
                                               SUPPLIER, 
                                               IN_TRANSIT_ALLOC_QTY, 
                                               ON_ORDER_ALLOC_QTY,
                                               PACK_QTY, 
                                               SIMPLE_PACK_IND, 
                                               TO_LOC_TYPE,
                                               ALLOC_NO, 
                                               SIZE_RANK, 
                                               QTY_RANK,
                                               RANK() OVER
                                                          (PARTITION BY ITEM ORDER BY SIZE_RANK) PACK_RANK --To select one when multiple pack is found
                                          FROM
                                              (
                                                SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                                      'D'|| TO_CHAR(h.RELEASE_DATE+NVL(TRANSIT_TIME,0),'YYYYMMDD') DAY,
                                                      h.RELEASE_DATE+NVL(TRANSIT_TIME,0) D_DAY,
                                                      decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                                      d.TO_LOC LOC,
                                                      im2.ITEM ITEM,
                                                      vpq.QTY ORDER_MULTIPLE,
                                                      DECODE(h.ORDER_NO,
                                                             NULL, (SELECT isup.SUPPLIER
                                                                      FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                                                     WHERE isup.ITEM = im2.ITEM
                                                                       AND isup.PRIMARY_SUPP_IND = 'Y'),
                                                             (SELECT ordh.SUPPLIER
                                                                FROM ${RMS_OWNER}.ORDHEAD ordh
                                                               WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                                      NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                                      d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                                      1 PACK_QTY,
                                                      im.SIMPLE_PACK_IND,
                                                      d.TO_LOC_TYPE,
                                                      h.ALLOC_NO,
                                                      CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                             WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                             ELSE 3 END SIZE_RANK,
                                                             RANK() OVER
                                                                (PARTITION BY h.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                 FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                                      ${RMS_OWNER}.ALLOC_DETAIL d,
                                                      ${RMS_OWNER}.ITEM_MASTER im,
                                                      ${RMS_OWNER}.ITEM_MASTER im2,
                                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                                                      TRAN_DATA t
                                                WHERE h.ALLOC_NO=d.ALLOC_NO
                                                  AND (h.ORDER_NO IS NULL OR
                                                       NOT EXISTS (SELECT '1'
                                                                     FROM ${RMS_OWNER}.ORDHEAD i
                                                                    WHERE i.order_type = 'CO'
                                                                      AND i.order_no=h.order_no))
                                                  AND h.ITEM=im.ITEM
                                                  AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                                  AND h.RELEASE_DATE IS NOT NULL
                                                  AND im.STATUS='A'
                                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                                  AND im.ITEM = vpq.ITEM
                                                  AND vpq.PACK_NO = im2.ITEM
                                                  AND im2.SIMPLE_PACK_IND = 'Y'
                                                  AND im.PACK_IND = 'N' 
                                                  AND im.FORECAST_IND = 'Y'
                                                  AND h.STATUS in ('A', 'R')
                                                  AND t.DEPT=im.DEPT
                                                  AND t.CLASS=im.CLASS
                                                  AND t.DESTINATION=d.TO_LOC
                                                  AND t.ORIGIN=h.wh
                                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                  AND h.RELEASE_DATE+NVL(TRANSIT_TIME,0) >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS}
                                             UNION
                                               SELECT /*+ PARALLEL (item_master,2) PARALLEL (alloc_detail,6) PARALLEL (transit_times,6) */
                                                      'D'|| TO_CHAR(h.RELEASE_DATE,'YYYYMMDD') DAY,
                                                      h.RELEASE_DATE D_DAY,
                                                      decode(d.TO_LOC_TYPE,'W',h.alloc_no,null) TRANSACTION_NUM,
                                                      d.TO_LOC LOC,
                                                      im2.ITEM ITEM,
                                                      vpq.QTY ORDER_MULTIPLE,
                                                      DECODE(h.ORDER_NO,
                                                             NULL, (SELECT isup.SUPPLIER
                                                                      FROM ${RMS_OWNER}.ITEM_SUPPLIER isup
                                                                     WHERE isup.ITEM = im2.ITEM
                                                                       AND isup.PRIMARY_SUPP_IND = 'Y'),
                                                             (SELECT ordh.SUPPLIER
                                                                FROM ${RMS_OWNER}.ORDHEAD ordh
                                                               WHERE ordh.ORDER_NO = h.ORDER_NO)) SUPPLIER,
                                                      NVL(d.QTY_TRANSFERRED,0)-NVL(d.QTY_RECEIVED,0) IN_TRANSIT_ALLOC_QTY,
                                                      d.QTY_ALLOCATED-NVL(d.QTY_TRANSFERRED,0) ON_ORDER_ALLOC_QTY,
                                                      1 PACK_QTY,
                                                      im.SIMPLE_PACK_IND,
                                                      d.TO_LOC_TYPE,
                                                      h.ALLOC_NO,
                                                      CASE WHEN isc.SUPP_PACK_SIZE  = vpq.QTY THEN 1  --pack component qty = supplier pack size
                                                             WHEN isc.INNER_PACK_SIZE = vpq.QTY THEN 2  --component qty = item's inner size
                                                             ELSE 3 END SIZE_RANK,
                                                             RANK() OVER
                                                                (PARTITION BY h.ITEM ORDER BY vpq.QTY) QTY_RANK --To select pack with smallest component quantity
                                                 FROM ${RMS_OWNER}.ALLOC_HEADER h,
                                                      ${RMS_OWNER}.ALLOC_DETAIL d,
                                                      ${RMS_OWNER}.ITEM_MASTER im,
                                                      ${RMS_OWNER}.ITEM_MASTER im2,
                                                      ${RMS_OWNER}.V_PACKSKU_QTY vpq,
                                                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc
                                                WHERE h.ALLOC_NO=d.ALLOC_NO
                                                  AND (h.ORDER_NO IS NULL OR
                                                       NOT EXISTS (SELECT '1'
                                                                     FROM ${RMS_OWNER}.ORDHEAD i
                                                                    WHERE i.order_type = 'CO'
                                                                      AND i.order_no=h.order_no))
                                                  AND h.ITEM=im.ITEM
                                                  AND d.QTY_ALLOCATED-NVL(d.QTY_RECEIVED,0)>0
                                                  AND h.RELEASE_DATE IS NOT NULL
                                                  AND im.STATUS='A'
                                                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                                                  AND im.ITEM = vpq.ITEM
                                                  AND vpq.PACK_NO = im2.ITEM
                                                  AND im2.SIMPLE_PACK_IND = 'Y'
                                                  AND isc.ITEM= vpq.ITEM
                                                  AND NVL(im.AIP_CASE_TYPE, 'F') != 'I'
                                                  AND im.PACK_IND = 'N' 
                                                  AND im.FORECAST_IND = 'Y'
                                                  AND h.STATUS in ('A','R')
                                                  AND NOT EXISTS (SELECT '1'
                                                                    FROM TRAN_DATA i
                                                                   WHERE i.class=im.class
                                                                     AND i.dept=im.dept
                                                                     AND i.destination=d.to_loc
                                                                     AND i.origin=h.wh)
                                                  AND h.RELEASE_DATE >= TO_DATE('${VDATE}','YYYYMMDD') - ${MAX_NOTAFTER_DAYS} 
                                              )
                                       ) inner
                                 WHERE inner.PACK_RANK = 1
                                   AND (   inner.SIZE_RANK = 1
                                        OR inner.SIZE_RANK = 2
                                        OR (    inner.SIZE_RANK = 3
                                            AND inner.QTY_RANK  = 1))

                              ) a
                     GROUP BY TRANSACTION_NUM,
                              DAY,
                              LOC,
                              ITEM,
                              SUPPLIER,
                              TO_LOC_TYPE,
                              ORDER_MULTIPLE,
                              SUPPLIER"

message "Program started ..."

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > ${FLOW_FILE}.xml << EOF
<FLOW name = "${PROGRAM_NAME}.flw">
   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT1}
         ]]>
      </PROPERTY>
      <OPERATOR type = "filter">
         <PROPERTY name="filter" value = "LOC_TYPE EQ 'W'"/>
         <PROPERTY name = "rejects" value = "true"/>
         <OUTPUT name = "future_delivery_alloc_w.v"/>
         <OUTPUT name = "future_delivery_alloc_s.v"/>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type = "funnel">
      <INPUT name = "future_delivery_alloc_w.v"/>
      <INPUT name = "future_delivery_alloc_s.v"/>
      <OUTPUT name = "future_delivery_alloc_inf.v"/>
   </OPERATOR>

   <OPERATOR type = "hash">
      <INPUT name = "future_delivery_alloc_inf.v"/>
      <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
      <PROPERTY name = "key" value = "DAY"/>
      <PROPERTY name = "key" value = "SUPPLIER"/>
      <PROPERTY name = "key" value = "LOC"/>
      <PROPERTY name = "key" value = "LOC_TYPE"/>
      <PROPERTY name = "key" value = "ITEM"/>
      <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
      <PROPERTY name = "key" value = "IN_TRANSIT_ALLOC_QTY"/>
      <PROPERTY name = "key" value = "ON_ORDER_ALLOC_QTY"/>
      <OPERATOR type = "sort">
         <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
         <PROPERTY name = "key" value = "DAY"/>
         <PROPERTY name = "key" value = "SUPPLIER"/>
         <PROPERTY name = "key" value = "LOC"/>
         <PROPERTY name = "key" value = "LOC_TYPE"/>
         <PROPERTY name = "key" value = "ITEM"/>
         <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
         <PROPERTY name = "key" value = "IN_TRANSIT_ALLOC_QTY"/>
         <PROPERTY name = "key" value = "ON_ORDER_ALLOC_QTY"/>
         <OPERATOR type = "removedup">
            <PROPERTY name = "key" value = "TRANSACTION_NUM"/>
            <PROPERTY name = "key" value = "DAY"/>
            <PROPERTY name = "key" value = "SUPPLIER"/>
            <PROPERTY name = "key" value = "LOC"/>
            <PROPERTY name = "key" value = "LOC_TYPE"/>
            <PROPERTY name = "key" value = "ITEM"/>
            <PROPERTY name = "key" value = "ORDER_MULTIPLE"/>
            <PROPERTY name = "keep" value = "last"/>
            <OUTPUT name = "future_delivery_order_nfp1a.v"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT    name="future_delivery_order_nfp1a.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="future_delivery_order_nfp1af_rej.v"/>
      <OUTPUT   name="future_delivery_order_nfp1af.v"/>
   </OPERATOR>

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            ${SQL_STATEMENT2}
         ]]>
      </PROPERTY>
      <OUTPUT name = "future_delivery_order_fp.v"/>
   </OPERATOR>

   <OPERATOR type="filter">
      <INPUT    name="future_delivery_order_fp.v"/>
      <PROPERTY name="filter" value="ORDER_MULTIPLE GT 999999"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="future_delivery_order_fpf_rej.v"/>
      <OUTPUT   name="future_delivery_order_fpf.v"/>
   </OPERATOR>

   <OPERATOR type = "funnel">
      <INPUT name = "future_delivery_order_fpf.v"/>
      <INPUT name = "future_delivery_order_nfp1af.v"/>
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="TRANSACTION_NUM" sourcefield="TRANSACTION_NUM" newtype="int64">
             <TYPEPROPERTY name="nullable" value="true"/>
                  </CONVERT>
                  <CONVERT destfield="DAY" sourcefield="DAY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ITEM" sourcefield="ITEM">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                     <CONVERTFUNCTION name="string_from_dfloat"/>
                     <TYPEPROPERTY name="nullable" value="false"/>
                  </CONVERT>
                  <CONVERT destfield="IN_TRANSIT_ALLOC_QTY" sourcefield="IN_TRANSIT_ALLOC_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="ON_ORDER_ALLOC_QTY" sourcefield="ON_ORDER_ALLOC_QTY" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC_TYPE" sourcefield="LOC_TYPE" newtype="string">
                     <CONVERTFUNCTION name="make_not_nullable">
                     <FUNCTIONARG name="nullvalue" value="x"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="LOC" sourcefield="LOC">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="0"/>
                     </CONVERTFUNCTION>
                     <CONVERTFUNCTION name="int64_from_dfloat"/>
                  </CONVERT>
                  <CONVERT destfield="SUPPLIER" sourcefield="SUPPLIER">
                     <CONVERTFUNCTION name="int64_from_dfloat"/>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="future_delivery_order_nfp1af_rej.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_order_nfp1af_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="convert">
      <INPUT name="future_delivery_order_fpf_rej.v"/>
      <PROPERTY name="convertspec">
         <![CDATA[
            <CONVERTSPECS>
               <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                  <CONVERTFUNCTION name="string_from_dfloat"/>
                  <TYPEPROPERTY name="nullable" value="false"/>
               </CONVERT>
            </CONVERTSPECS>
         ]]>
      </PROPERTY>
      <OUTPUT name="future_delivery_order_fpf_rej_not_null.v"/>
   </OPERATOR>

   <OPERATOR type="funnel">
      <INPUT name="future_delivery_order_nfp1af_rej_not_null.v"/>
      <INPUT name="future_delivery_order_fpf_rej_not_null.v"/>
      <OUTPUT name="reject_total.v"/>
   </OPERATOR>      

   <OPERATOR type="export">
       <INPUT name="reject_total.v"/>
       <PROPERTY name="outputfile" value="${REJECT_ORD_MULT_FILE}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
