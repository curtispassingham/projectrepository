#!/bin/ksh
############################################
# Purpose: Run RMS -> AIP Retl programs    #
############################################
job[0]=pre_rmse_aip.ksh
job[1]=rmse_aip_item_master.ksh
job[2]=rmse_aip_item_supp_country.ksh
job[3]=rmse_aip_merchhier.ksh
job[4]=rmse_aip_orghier.ksh
job[5]=rmse_aip_store.ksh
job[6]=rmse_aip_suppliers.ksh
job[7]=rmse_aip_wh.ksh
job[8]=rmse_aip_item_retail.ksh
job[9]=rmse_aip_item_loc_traits.ksh
job[10]=rmse_aip_substitute_items.ksh
job[11]="rmse_aip_store_cur_inventory.ksh F"
job[12]="rmse_aip_wh_cur_inventory.ksh F"
job[13]=rmse_aip_future_delivery_alloc.ksh
job[14]=rmse_aip_alloc_in_well.ksh
job[15]=rmse_aip_future_delivery_order.ksh
job[16]=rmse_aip_future_delivery_tsf.ksh
job[17]=rmse_aip_tsf_in_well.ksh
job[18]=rmse_aip_item_sale.ksh
job[19]=rmse_aip_cl_po.ksh
job[20]=rmse_aip_rec_qty.ksh

x=0
if [ $# -lt 1 ]
then
    # set -A job aipt_future_delivery.ksh
    # print "number of jobs to run is : "${#job[*]}
    echo Run all AIP RETL programs
    while [ $x -lt ${#job[*]} ]
    do
       echo `expr $x + 1`   ${job[$x]}
       ${job[$x]}
       let x=$x+1
    done
else
    echo Run selected AIP RETL programs
    for i in $*
    do
       echo `expr $x + 1`  $i
       $i
       let x=$x+1
    done
fi

echo Programs executed successfully
