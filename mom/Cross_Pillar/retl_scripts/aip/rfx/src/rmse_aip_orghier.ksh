#!/bin/ksh

##########################################################################
#
#  The rmse_aip_orghier.ksh program extracts organizational heirarchy data  
#  from the COMPHEAD, CHAIN, AREA, REGION and DISTRICT RMS tables and 
#  places this data into a flat file (rmse_aip_orghier.dat) to be accessed
#  by AIP data transformation programs.
#
##########################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="rmse_aip_orghier"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}"
  
cat > ${FLOW_FILE}.xml << EOF
<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the organizational heirarchy data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT ch.COMPANY,
                   REPLACE(RTRIM(ch.CO_NAME),CHR(10),'') CO_NAME,
                   c.CHAIN,
                   REPLACE(RTRIM(c.CHAIN_NAME),CHR(10),'') CHAIN_NAME,
                   a.AREA,
                   REPLACE(RTRIM(a.AREA_NAME),CHR(10),'') AREA_NAME,
                   r.REGION,
                   REPLACE(RTRIM(r.REGION_NAME),CHR(10),'')REGION_NAME,
                   d.DISTRICT,
                   REPLACE(RTRIM(d.DISTRICT_NAME),CHR(10),'') DISTRICT_NAME
              FROM ${RMS_OWNER}.COMPHEAD ch,
                   ${RMS_OWNER}.CHAIN c,
                   ${RMS_OWNER}.AREA a,
                   ${RMS_OWNER}.REGION r,
                   ${RMS_OWNER}.DISTRICT d
             WHERE c.CHAIN = a.CHAIN (+)
               AND a.AREA = r.AREA (+)
               AND r.REGION = d.REGION (+)
         ]]>
      </PROPERTY>
      <!--  Only Chain and Company data is specified as mandatory in the schema file  -->
      <OPERATOR type="convert">
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="CHAIN_NAME" sourcefield="CHAIN_NAME">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="CO_NAME" sourcefield="CO_NAME">
                     <CONVERTFUNCTION name="make_not_nullable">
                        <FUNCTIONARG name="nullvalue" value="NULL"/>
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </OPERATOR>
</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into rmse_aip_orghier.xml:
###############################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}.xml

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully."

#  Cleanup and exit:

rmse_terminate 0
