#!/bin/ksh
########################################################
# Purpose:  Extracts RMS item, pack, supplier and
#           supplier pack size information
########################################################

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_aip_item_retail"


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. ${AIP_HOME}/rfx/etc/rmse_aip_config.env
. ${LIB_DIR}/rmse_aip_lib.ksh



##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema


message "Program started ..."

FLOW_FILE=${LOG_DIR}/${PROGRAM_NAME}.xml

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT im.ITEM,
                      im.ITEM AIP_SKU,
                      im.SUBCLASS,
                      im.CLASS,
                      im.DEPT,
                      im.STANDARD_UOM,
                      ul.UOM_DESC_TRANS STANDARD_UOM_DESCRIPTION,
                      NVL(im.HANDLING_TEMP, 0) SKU_TYPE,
                      NVL(cd.CODE_DESC, 0) SKU_TYPE_DESCRIPTION,
                      '1' ORDER_MULTIPLE,
                      '0' PACK_QUANTITY
                 FROM ${RMS_OWNER}.ITEM_MASTER im,
                      ${RMS_OWNER}.ITEM_SUPPLIER isup,
                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                      ${RMS_OWNER}.UOM_CLASS_TL ul,
                      ${RMS_OWNER}.CODE_DETAIL cd
                WHERE im.ITEM = isup.ITEM
                  AND im.STANDARD_UOM=ul.UOM
                  AND ul.lang = get_primary_lang
                  AND im.HANDLING_TEMP=cd.CODE(+)
                  AND cd.code_type(+)='HTMP'
                  AND isup.ITEM=isc.ITEM
                  AND isup.SUPPLIER=isc.SUPPLIER
                  AND im.PACK_IND='N'
                  AND isc.SUPP_PACK_SIZE>1
                  AND im.STATUS='A'
                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                  AND im.FORECAST_IND = 'Y'
                  AND im.INVENTORY_IND = 'Y'
                UNION
               SELECT im_c.ITEM,
                      im_c.ITEM AIP_SKU,
                      im_c.SUBCLASS,
                      im_c.CLASS,
                      im_c.DEPT,
                      im_c.STANDARD_UOM,
                      ul.UOM_DESC_TRANS STANDARD_UOM_DESCRIPTION,
                      NVL(im_c.HANDLING_TEMP, 0) SKU_TYPE,
                      NVL(cd.CODE_DESC, 0) SKU_TYPE_DESCRIPTION,
                      '1' ORDER_MULTIPLE,
                      substr(pb.QTY,0,6) PACK_QUANTITY
                 FROM ${RMS_OWNER}.ITEM_MASTER im,
                      ${RMS_OWNER}.ITEM_SUPPLIER isup,
                      ${RMS_OWNER}.ITEM_SUPP_COUNTRY isc,
                      ${RMS_OWNER}.UOM_CLASS_TL ul,
                      ${RMS_OWNER}.CODE_DETAIL cd,
                      ${RMS_OWNER}.V_PACKSKU_QTY pb,
                      ${RMS_OWNER}.ITEM_MASTER im_c
                WHERE im_c.ITEM = isup.ITEM
                  AND im_c.STANDARD_UOM=ul.UOM
                  AND ul.lang = get_primary_lang
                  AND im_c.HANDLING_TEMP=cd.CODE(+)
                  AND cd.code_type(+)='HTMP'      
                  AND isup.ITEM=isc.ITEM
                  AND isup.SUPPLIER=isc.SUPPLIER
                  AND im.ITEM=pb.PACK_NO
                  AND pb.ITEM=im_c.ITEM
                  AND im.PACK_IND='Y'
                  AND im.STATUS='A'
                  AND im.ITEM_LEVEL=im.TRAN_LEVEL
                  AND im.INVENTORY_IND = 'Y'
                  AND (im.SIMPLE_PACK_IND='Y' AND im.item IN (SELECT pm.pack_no
                                                                FROM item_master im1,
                                                                     ${RMS_OWNER}.PACKITEM pm
                                                               WHERE pm.item = im1.item
                                                                 AND im1.forecast_ind = 'Y'))
            ]]>
         </PROPERTY>
         <OPERATOR type="convert">
            <PROPERTY name="convertspec">
               <![CDATA[
                  <CONVERTSPECS>
                     <CONVERT destfield="ITEM" sourcefield="ITEM">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="AIP_SKU" sourcefield="AIP_SKU">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="ORDER_MULTIPLE" sourcefield="ORDER_MULTIPLE" newtype="string">
                        <CONVERTFUNCTION name="string_from_dfloat"/>
                        <TYPEPROPERTY name="nullable" value="false"/>
                     </CONVERT>
                     <CONVERT destfield="PACK_QUANTITY" sourcefield="PACK_QUANTITY" newtype="string">
                        <CONVERTFUNCTION name="default"/>
                        <TYPEPROPERTY name="nullable" value="true"/>
                     </CONVERT>
                     <CONVERT destfield="SUBCLASS" sourcefield="SUBCLASS" newtype="int16">
                        <CONVERTFUNCTION name="int16_from_dfloat"/>
                        <TYPEPROPERTY name="nullable" value="true"/>
                     </CONVERT>
                     <CONVERT destfield="CLASS" sourcefield="CLASS" newtype="int16">
                        <CONVERTFUNCTION name="int16_from_dfloat"/>
                        <TYPEPROPERTY name="nullable" value="true"/>
                     </CONVERT>
                     <CONVERT destfield="DEPT" sourcefield="DEPT" newtype="int16">
                        <CONVERTFUNCTION name="int16_from_dfloat"/>
                        <TYPEPROPERTY name="nullable" value="true"/>
                     </CONVERT>
                     <CONVERT destfield="SUBCLASS" sourcefield="SUBCLASS">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="-1"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="CLASS" sourcefield="CLASS">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="-1"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="DEPT" sourcefield="DEPT">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="-1"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="STANDARD_UOM" sourcefield="STANDARD_UOM">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                     <CONVERT destfield="STANDARD_UOM_DESCRIPTION" sourcefield="STANDARD_UOM_DESCRIPTION">
                        <CONVERTFUNCTION name="make_not_nullable">
                           <FUNCTIONARG name="nullvalue" value="NULL"/>
                        </CONVERTFUNCTION>
                     </CONVERT>
                  </CONVERTSPECS>
               ]]>
            </PROPERTY>
            <OPERATOR type="export">
               <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
               <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>

   </FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into rmse_aip_item_retail.xml:
###############################################

${RETL_EXE} ${RETL_OPTIONS} -f ${FLOW_FILE}

#######################################################
#  Do error checking on results of the RETL execution:
#######################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

# Remove the status file
if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
