#! /bin/ksh

########################################################
# Copyright (c) 2003, Retek Inc.  All rights reserved.
# $Workfile:   rmse_extract_with_schema.ksh  $
# $Revision: 289615 $
# $Modtime:   03/25/2003  $
########################################################

function extract_with_schema
{
    PROGRAM_NAME=$1
    SCHEMAFILE=$2
    OUTPUTFILE=$3

    shift
    shift
    shift

    QUERY="$*"

    cat > ${PROGRAM_NAME}.xml - << EOF
    
    <FLOW name = "${PROGRAM_NAME}.flw">
	    ${DBREAD}
	    <PROPERTY name = "query" >
	        <![CDATA[
	            ${QUERY}
	        ]]>
	    </PROPERTY>
	    <OUTPUT   name = "output.v"/>
	</OPERATOR>
    
	<OPERATOR type="export">
	    <INPUT    name="output.v"/>
	    <PROPERTY name="outputfile" value="${OUTPUTFILE}"/>
	    <PROPERTY name="schemafile" value="${SCHEMAFILE}"/>
	</OPERATOR>
    </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${PROGRAM_NAME}.xml

return $?
}
