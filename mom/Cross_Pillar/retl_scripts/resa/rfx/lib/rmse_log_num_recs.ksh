#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   dwi_log_num_recs.ksh  $
# $Revision: 289615 $
# $Modtime:   Jun 10 2002 08:37:38  $
########################################################

##############################################################################
# This Function logs the number of records in the given file
#     Usage: $1 = File name
##############################################################################

function log_num_recs 
{
   if [[ -f $1 ]]
   then 
      NUM_RECS=`wc -l $1 | awk '{print $1}'`
   else
      NUM_RECS=0
   fi
   message "Number of records in $1 = ${NUM_RECS}"
}
