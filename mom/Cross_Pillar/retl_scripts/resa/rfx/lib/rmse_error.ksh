#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   dwi_error.ksh  $
# $Revision: 289615 $
# $Modtime:   Jun 10 2002 08:37:38  $
########################################################


##############################################################################
#                                                                            #
# Functions that aid in helping scripts terminate gracefully and arbitrary   #
# generation of reject files                                                 #
#                                                                            #
##############################################################################


# global variables
export REJ_FILELIST=""  # contains the list of reject files
export REJ_FILECNT=0    # counter for number of reject files


# function cleanup - called to remove all intermediate files.  May be
# extended for general cleanup as well.
function rmse_cleanup {
    # remove reject files
    rm -f ${REJ_FILELIST}
    rm -f ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT
    rm -f ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST
    
    # remove .bad file, if it is empty
    if [[ ! -s ${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad ]]; then
       rm -f ${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad
    fi
}


trap rmse_terminate INT TERM KILL

# function terminate - should be called in all flows to exit
function rmse_terminate {
    rmse_cleanup
    if [[ $1 = 0 ]]; then
	exit 0
    else
	echo "Aborting..."
	exit 1
    fi
}


# can be called between a cat > flow.xml >>EOF...EOF and will echo
# a unique reject file and store the filename in REJ_FILELIST
function getRejectFile {
    # get around subshell not being able to modify parent environment by 
    # writing shell variable values to a file
    REJ_FILECNT=`cat -s ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT`
    REJ_FILECNT=${REJ_FILECNT:=0}
    REJ_FILELIST=`cat -s ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST`
    CURR_REJFILE="${ERR_DIR}/${PROGRAM_NAME}${REJ_FILECNT}.bad"

    # echo reject file to xml flow
    echo "${CURR_REJFILE}"

    print -n "`expr ${REJ_FILECNT} + 1`" > ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILECNT
    print -n "${REJ_FILELIST} ${CURR_REJFILE}" > ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST
}


##############################################################################
#                                                                            #
# Function to produce test status codes and produce error messages           #
#                                                                            #
# Usage: [-e] exitcode [-m] message to produce on bad exit code              #
#        [-d] interpret exitcode with respect to DB2 (i.e. 1 is OK)          #
#        [-r] reject file name [-w] message to produce on presence of rject  #
#                                                                            #
##############################################################################

function checkerror {

   error_to_check=0
   check_reject="FALSE"
   is_db2="FALSE"
   error_message=" A fatal error occurred in $0"
   reject_message=" Some records were rejected in $0"
   
   while getopts "e:r:dm:w:" opt; do
      case $opt in
       e ) error_to_check=$OPTARG;;
       r ) reject_file=$OPTARG
           check_reject="TRUE";;
       d ) is_db2="TRUE";;
       m ) error_message=$OPTARG;;
       w ) reject_message=$OPTARG;;
       \? ) print 'checkerror usage: -e exitcode -r rejectfile -d -m errmess -w rejmess'
            rmse_terminate 1
      esac
   done 

   if [[ $check_reject = "TRUE" ]]; then
      if [[ -s $reject_file ]]; then
         echo "$PROGRAM_NAME "`date +"%T"` ": WARNING: " $reject_message >> $LOG_DIR/$LOG
      fi
   fi

   if [[ $error_to_check != 0 ]]; then
      if [[ $is_db2 = "TRUE" ]]; then
         if [[ $error_to_check > 1 ]]; then
            echo "$PROGRAM_NAME "`date +"%T"`": ERROR: " $error_message >> $LOG_DIR/$LOG
            rmse_terminate $error_to_check
         fi
      else
         echo "$PROGRAM_NAME "`date +"%T"`": ERROR: " $error_message >> $LOG_DIR/$LOG
         rmse_terminate $error_to_check
      fi
   fi

}
