#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   dwi_message.ksh  $
# $Revision: 289615 $
# $Modtime:   Jul 03 2002 14:30:42  $
########################################################

##############################################################################
# This Function to produce message text with program name and timestamp
#     Usage: $1 = Message text
##############################################################################

function message 
{
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $LOG_FILE 
   echo "\n-----------------------------"  >> $ERR_FILE
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $ERR_FILE
   echo "-----------------------------"  >> $ERR_FILE
}
