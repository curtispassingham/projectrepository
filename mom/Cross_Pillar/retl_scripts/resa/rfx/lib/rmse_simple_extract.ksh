#! /bin/ksh

########################################################
# Copyright (c) 2003, Retek Inc.  All rights reserved.
# $Workfile:   rmse_simple_extract.ksh  $
# $Revision: 289615 $
# $Modtime:   03/25/2003  $
########################################################

function simple_extract
{
    PROGRAM_NAME=$1
    OUTPUTFILE=$2

    shift
    shift

    QUERY="$*"

    cat > ${PROGRAM_NAME}.xml - << EOF
    
    <FLOW name = "${PROGRAM_NAME}.flw">
	    ${DBREAD}
	    <PROPERTY name = "query" value = "${QUERY}" />
	    <OUTPUT   name = "output.v"/>
	</OPERATOR>
    
	<OPERATOR type="export">
	    <INPUT    name="output.v"/>
	    <PROPERTY name="outputfile" value="${OUTPUTFILE}"/>
<!--	    <PROPERTY name="schemafile" value="${SCHEMAFILE}"/> -->
	</OPERATOR>
    </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${PROGRAM_NAME}.xml

return $?
}
