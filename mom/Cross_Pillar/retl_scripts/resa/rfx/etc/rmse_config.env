#!/bin/ksh

########################################################
# Copyright (c) 2006, Oracle Corp.  All rights reserved.
# $Workfile:   rmse_config.env  $
# $Revision: 1.2 $
# $Modtime:   Sep 29 2006 10:51:04  $
########################################################

########################################################
# Turn on debugging if RMSE_DEBUG is set.
########################################################
if [[ ${RMSE_DEBUG:=0} != 0 ]]
then
   set -x
fi

########################################################
# Environment configuration script to be used by DWI   #
# dimension extraction programs.                       #
########################################################
export DBNAME=cs0110g
export RMS_OWNER=rmsdev120a
export BA_OWNER=rmsdev120auser
export LOAD_TYPE=conventional

## The following setting need to be defined for the
## Oracle port and Oracle host.
export ORACLE_PORT=1525
export ORACLE_HOST=mspdev38.us.oracle.com

##The folloiwng setting is for dbuseralias being replaced for connectstring and dbuserid
export RETL_WALLET_ALIAS="ENTER A VALUE"

##The folloiwng setting is for SQLPLUS/CONN login which basically refers to Oracle Wallet Alias
export ORACLE_WALLET_ALIAS="ENTER A VALUE"

## Define SQLPlUS/CONN logon
export SQLPLUS_LOGON="/@${ORACLE_WALLET_ALIAS}"

## Valid values for LANGUAGE are: en, fr, ja, es
## en = english, fr = french, ja = japanese, es = spanish
export LANGUAGE=en

export DATA_DIR=$MMHOME/data
export REJ_DIR=$MMHOME/data
export LOG_DIR=$MMHOME/log
export ERR_DIR=$MMHOME/error
export RSC_DIR=$MMHOME/rfx/include
export SCHEMA_DIR=$MMHOME/rfx/schema
export BIN_DIR=$MMHOME/rfx/bin
export LIB_DIR=$MMHOME/rfx/lib
export ETC_DIR=$MMHOME/rfx/etc
export SRC_DIR=$MMHOME/rfx/src
export BKM_DIR=$MMHOME/rfx/bookmark
export TEMP_DIR=/tmp
export DB_ENV=ORA
export NLS_NUMERIC_CHARACTERS=".,"

if [ -s ${ETC_DIR}/vdate.txt ]
then
   export VDATE=$(cat ${ETC_DIR}/vdate.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": vdate.txt doesn't exist under ${ETC_DIR}" 
   exit 1
fi

if [ -s ${ETC_DIR}/next_vdate.txt ]
then
   export NEXT_VDATE=$(cat ${ETC_DIR}/next_vdate.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": next_vdate.txt doesn't exist under ${ETC_DIR}"
   exit 1
fi

# Set the date to be used in the naming of the log/error/reject/status/bookmark files
if [[ $PROGRAM_NAME = "pre_dwi_extract" ]]
then
   export FILE_DATE=${NEXT_VDATE}
else
   export FILE_DATE=${VDATE}
fi

# Name the log file
LOG=${FILE_DATE}.log
export LOG_FILE=${LOG_DIR}/${LOG}

# Name the error and reject files based on the input/output file names
if [[ $1 != "" ]]
then
   STRIPPED_FILENAME=`basename $1`
   export ERR_FILE=${ERR_DIR}/${PROGRAM_NAME}.${STRIPPED_FILENAME}.${FILE_DATE}
   export REJ_FILE=${REJ_DIR}/${PROGRAM_NAME}.${STRIPPED_FILENAME}.rej.${FILE_DATE}
   export STATUS_FILE=${ERR_DIR}/${PROGRAM_NAME}.${STRIPPED_FILENAME}.status.${FILE_DATE}
   export BOOKMARK_FILE=${BKM_DIR}/${PROGRAM_NAME}.${STRIPPED_FILENAME}.bkm.${FILE_DATE}
else
   export ERR_FILE=${ERR_DIR}/${PROGRAM_NAME}.${FILE_DATE}
   export REJ_FILE=${REJ_DIR}/${PROGRAM_NAME}.rej.${FILE_DATE}
   export STATUS_FILE=${ERR_DIR}/${PROGRAM_NAME}.status.${FILE_DATE}
   export BOOKMARK_FILE=${BKM_DIR}/${PROGRAM_NAME}.bkm.${FILE_DATE}
fi

# Set up redirection of standard output and standard error into the error file
exec 1>>$ERR_FILE 2>&1

if  [[ ! -f ${STATUS_FILE} ]]
then
   touch ${STATUS_FILE}
else
   echo "-->${PROGRAM_NAME} has already started" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/vat_ind.txt ]
then
   export VAT_IND=$(cat ${ETC_DIR}/vat_ind.txt)
else 
   echo "$PROGRAM_NAME "`date +"%T"`": vat_ind.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/class_level_vat_ind.txt ]
then
   export CLASS_LEVEL_VAT_IND=$(cat ${ETC_DIR}/class_level_vat_ind.txt)
else 
   echo "$PROGRAM_NAME "`date +"%T"`": class_level_vat_ind.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/domain_level.txt ]
then
   export DOMAIN_LEVEL=$(cat ${ETC_DIR}/domain_level.txt)
else 
   echo "$PROGRAM_NAME "`date +"%T"`": domain_level.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/stkldgr_vat_incl_retl_ind.txt ]
then
   export STKLDGR_VAT_INCL_RETL_IND=$(cat ${ETC_DIR}/stkldgr_vat_incl_retl_ind.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": stkldgr_vat_incl_retl_ind.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/multi_currency_ind.txt ]
then
   export MULTI_CURRENCY_IND=$(cat ${ETC_DIR}/multi_currency_ind.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": multi_currency_ind.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/prime_exchng_rate.txt ]
then
   export PRIME_EXCHNG_RATE=$(cat ${ETC_DIR}/prime_exchng_rate.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": prime_exchng_rate.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/last_eom_date.txt ]
then
   export LAST_EOM_DATE=$(cat ${ETC_DIR}/last_eom_date.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": last_eom_date.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/max_backpost_days.txt ]
then
   export MAX_BACKPOST_DAYS=$(cat ${ETC_DIR}/max_backpost_days.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": max_backpost_days.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/prime_currency_code.txt ]
then
   export PRIME_CURRENCY_CODE=$(cat ${ETC_DIR}/prime_currency_code.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": prime_currency_code.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

if [ -s ${ETC_DIR}/consolidation_code.txt ]
then
   export CONSOLIDATION_CODE=$(cat ${ETC_DIR}/consolidation_code.txt)
else
   echo "$PROGRAM_NAME "`date +"%T"`": consolidation_code.txt doesn't exist under ${ETC_DIR}" >> $LOG_FILE
   exit 1
fi

####################################################################
# Set up database connection either using thin or using oci
####################################################################


if [[ ${CONN_TYPE} = "oci" ]]
then
   export JDBCREADDRIVER="<PROPERTY name=\"jdbcdriverstring\" value=\"oracle.jdbc.driver.OracleDriver\"/>"
   export JDBCCONN="<PROPERTY name=\"jdbcconnectionstring\" value=\"jdbc:oracle:thin:@msp52062.us.oracle.com:1521/dvols100\"/>"
   export JDBCWRITEDRIVER="<PROPERTY name=\"jdbcdriver\" value=\"oci\"/>"
else
   export JDBCREADDRIVER=""
   export JDBCCONN=""
   export JDBCWRITEDRIVER=""
fi

if [[ $DB_ENV = "ORA" ]]
then
   export DBREAD="<OPERATOR type=\"oraread\">
      <PROPERTY name=\"sid\" value=\"$DBNAME\"/>
      <PROPERTY name=\"dbuseralias\" value=\"$RETL_WALLET_ALIAS\"/>
      <PROPERTY name=\"maxdescriptors\" value=\"100\"/>
      <PROPERTY name=\"datetotimestamp\" value=\"false\"/>
      <PROPERTY name=\"port\" value=\"${ORACLE_PORT}\"/>
      <PROPERTY name=\"hostname\" value=\"${ORACLE_HOST}\"/>"
   export DBWRITE="<OPERATOR type=\"orawrite\">
      <PROPERTY name=\"sid\" value=\"$DBNAME\"/>
      <PROPERTY name=\"dbuseralias\" value=\"$RETL_WALLET_ALIAS\"/>
      <PROPERTY name=\"maxdescriptors\" value=\"100\"/>
      <PROPERTY name=\"port\" value=\"${ORACLE_PORT}\"/>
      <PROPERTY name=\"hostname\" value=\"${ORACLE_HOST}\"/>"
   export DBWRITE_TEMP="<OPERATOR type=\"orawrite\">
      <PROPERTY name=\"sid\" value=\"$DBNAME\"/>
      <PROPERTY name=\"dbuseralias\" value=\"$RETL_WALLET_ALIAS\"/>
      <PROPERTY name=\"maxdescriptors\" value=\"100\"/>
      <PROPERTY name=\"method\" value=\"$LOAD_TYPE\"/>
      <PROPERTY name=\"mode\" value=\"truncate\"/>
      <PROPERTY name=\"schemaowner\" value=\"${BA_OWNER}\"/>
      <PROPERTY name=\"createtablemode\" value=\"recreate\"/>
      <PROPERTY name=\"port\" value=\"${ORACLE_PORT}\"/>
      <PROPERTY name=\"hostname\" value=\"${ORACLE_HOST}\"/>"
   export DBPREPSTMT="<OPERATOR type=\"preparedstatement\">
      <PROPERTY name=\"sid\" value=\"${DBNAME}\"/>
      <PROPERTY name=\"dbuseralias\" value=\"${RETL_WALLET_ALIAS}\"/>
      <PROPERTY name=\"schemaowner\"  value=\"${BA_OWNER}\"/>
      <PROPERTY name=\"maxdescriptors\" value=\"100\"/>
      <PROPERTY name=\"port\" value=\"${DBPORT}\"/>
      <PROPERTY name=\"hostname\" value=\"${DBHOST}\"/>"
   export DBWRITE_TEMP2="${DBWRITE_TEMP}"
   export DBWRITE_TEMP3="${DBWRITE_TEMP}"
   export NVL='NVL'
   export CONVERT_VDATE="TO_DATE('$VDATE','YYYYMMDD')"
   export CONVERT_LAST_EOM_DATE="TO_DATE('$LAST_EOM_DATE','YYYYMMDD')"
fi

## RMS Extract Config Items
export RFX_EXE=rfx
export RFX_OPTIONS="-sSCHEMAFILE"
export RETL_EXE=rfx
export RETL_OPTIONS="-f -"
