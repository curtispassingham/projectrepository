####################################################################
#
#  File:    clndhier.awk
#  Module:  Foundation    
#
#  Date:    10/20/98
#  Author:  Grant Wyckoff (as prod.awk for Sku Explosion)
#
#  Changes:
#  Feb 2000   jmb   created clndhier.awk for RMS 9.0 to RDF 9.0
#
#  Description:
#
#  Processes RMS output file clndmstr.dat into clnd.dat format
#
#
#
####################################################################

#
# Remove all trailing spaces from a string.
#
function trim( inStr )
{
   e      = match( inStr, /[ ]*$/ )

   if ( e > 0 ) { return substr( inStr, 1, e-1 ) }
   else         { return inStr }
}

#
# Remove any non-acumate type characters from size position IDs
#
function clean( inStr )
{
   wrkStr     = inStr
   wrkStr     = trim( wrkStr )
   wrkStr     = toupper( wrkStr )
   didClean   = 0

   do {
      e = match( wrkStr, /[^A-Z&$%0-9_]/ );

      if ( e > 0 )
      {
         part1    = substr( wrkStr, 1, e-1)
         part2    = substr( wrkStr, e+1 )
         wrkStr   = part1 "_" part2
         didClean = 1
      }
   } while ( e > 0 )

   if ( didClean == 1 )
   {
      Cleaned[ inStr ] = wrkStr
   }

   return wrkStr
}

   #
   # Check the length of the input string.  If it is too long,
   #  make a note for later reporting but don't trim it.
   #
function checkLength( inStr, maxLen )
{
   if ( length( inStr ) > maxLen )
   {
      ++lengthErr[ inStr ]
   }
}

#
# Pad a string to the specified length; truncate it if necessary.
#
function pad( inStr, len )
{
   e      = len - length( inStr )

   if ( e > 0 )
   {
      spaces  = ""
      for ( i = 0; i < e; ++i)
         spaces = spaces " "

      return inStr spaces;
   }
  else if ( e < 0 )
   {
      return substr( inStr, 1, len )
   }
   else
   {
      return inStr
   }

}

#
# Function to derive day of week
#
function dayofweek(inStr)
{
   if (inStr == 1) {
      return "Monday"
   } else if (inStr == 2) {
      return "Tuesday"
   } else if (inStr == 3) {
      return "Wednesday"
   } else if (inStr == 4) { 
      return "Thursday"
   } else if (inStr == 5) {
      return "Friday"
   } else if (inStr == 6) {
      return "Saturday"
   } else if (inStr == 7) {
      return "Sunday"
   } else {
      return "Invalid Day of Week"
   }
}

#
# Function to derive month 
#
function month(inStr)
{
   if (inStr == 1) {
      return "January"
   } else if (inStr == 2) {
      return "February"
   } else if (inStr == 3) {
      return "March"
   } else if (inStr == 4) {
      return "April"
   } else if (inStr == 5) {
      return "May"
   } else if (inStr == 6) {
      return "June"
   } else if (inStr == 7) {
      return "July"
   } else if (inStr == 8) {
      return "August"
   } else if (inStr == 9) {
      return "September"
   } else if (inStr == 10) {
      return "October"
   } else if (inStr == 11) {
      return "November"
   } else if (inStr == 12) {
      return "December"
   } else {
      return "Invalid Month"
   }
}
#
# Apply this code to every line of the input file.
#
{

space   =   " "
line_cleaned = 0

   # 
   # Read every field of input file into a variable
   # makes a good table of the rms output file
   #

   rms_year_id    =  substr( $0 , 1  , 4 )
   rms_half_id    =  substr( $0 , 5  , 1 )
   rms_qrtr_id    =  substr( $0 , 6  , 1 )
   rms_moy_id     =  substr( $0 , 7  , 2 )
   rms_woy_id     =  substr( $0 , 9  , 2 )
   rms_dow_id     =  substr( $0 , 11 , 1 )
   rms_date_id    =  substr( $0 , 12 , 8 )
   rms_yy_id      =  substr( $0 , 12,  4 )
   rms_mm_id      =  substr( $0 , 16 , 2 )
   rms_dd_id      =  substr( $0 , 18 , 2 )
  
   #
   # Remove the trailing spaces from every component for processing
   #


   rms_year_id   =  clean(   rms_year_id    )
   line_cleaned  =  didClean + line_cleaned
   rms_half_id   =  clean(   rms_half_id    )
   line_cleaned  =  didClean + line_cleaned
   rms_qrtr_id   =  clean(   rms_qrtr_id    )
   line_cleaned  =  didClean + line_cleaned
   rms_moy_id    =  clean(   rms_moy_id    )
   line_cleaned  =  didClean + line_cleaned
   rms_woy_id    =  clean(   rms_woy_id     )
   line_cleaned  =  didClean + line_cleaned
   rms_dow_id    =  clean(   rms_dow_id     )
   line_cleaned  =  didClean + line_cleaned
   rms_date_id   =  clean(   rms_date_id    )
   line_cleaned  =  didClean + line_cleaned

   #
   # Set output variables in prod.dat format
   #

   year_id    =   "A" rms_year_id
   year_d     =   "FY" rms_year_id
   half_id    =   "S" rms_half_id "_" rms_year_id
   half_d     =   "Season " rms_half_id "_" rms_year_id
   qrtr_id    =   "Q" rms_qrtr_id "_" rms_year_id       
   qrtr_d     =    rms_year_id "_Q" rms_qrtr_id
   mnth_id    =   "M" sprintf("%02d", rms_moy_id) "_"  rms_year_id
   mnth_d    =    rms_year_id "_" rms_moy_id

   date_id    =   rms_date_id     
   date_d     =   rms_dd_id "/" rms_mm_id "/" rms_yy_id 
   week_id    =   "W" sprintf("%02d", rms_woy_id) "_" rms_year_id
   week_d     =   rms_year_id "_WK" sprintf("%02d", rms_woy_id) 

   dow_d      =   dayofweek(rms_dow_id)
   dow_id     =   substr(dow_d, 0, 3)

   #
   #  Generate the output with the new columns
   #
   if (line_cleaned == 0)
   {
   printf( "%-8s"      ,   date_id    ) 
   printf( "%-20s"     ,   date_d     )
   printf( "%-8s"      ,   week_id    ) 
   printf( "%-20s"     ,   week_d     )
   printf( "%-8s"      ,   mnth_id    )
   printf( "%-20s"     ,   mnth_d     ) 
   printf( "%-8s"      ,   qrtr_id    ) 
   printf( "%-20s"     ,   qrtr_d     )
   printf( "%-8s"      ,   half_id    ) 
   printf( "%-20s"     ,   half_d     )
   printf( "%-8s"      ,   year_id    ) 
   printf( "%-20s"     ,   year_d     )
   printf( "%-8s"      ,   dow_id     )
   printf( "%-20s"     ,   dow_d      )
   printf( "\n"   )
   }
#
# This is the end of the main section
#
}
