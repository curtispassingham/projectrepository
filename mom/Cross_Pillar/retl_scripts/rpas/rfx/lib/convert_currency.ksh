#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   convert_currency.ksh  $
# $Revision: 289615 $
# $Modtime:   Jan 06 2004 10:16:38  $
########################################################

########################################################
# Script defines convert_currency parameter            #
########################################################

CONVERT_CURRENCY=""
GENERATE_AMT=""

# input and output dataset names are defaulted if calling modules have not defined any
if [[ ${INPUT_DATASET} = "" ]]
then 
   export INPUT_DATASET=${PROGRAM_NAME}_preconvert
fi

if [[ ${OUTPUT_DATASET} = "" ]]
then 
   export OUTPUT_DATASET=${PROGRAM_NAME}_postconvert
fi

if [[ ${CONV_CURR_REJ_FILE} = "" ]]
then 
   export CONV_CURR_REJ_FILE=${REJ_FILE}
fi

# define column name with _idnt
export IDNT_TYPE_COLUMN=${IDNT_TYPE}_IDNT

# if these variables are not empty, then CONVERT_CURRENCY is worthy to be generated.
if [[ ${IDNT_TYPE} != "" && ${CONVERT_AMT_FIELDS} != "" ]]
then
   if [[ ${MULTI_CURRENCY_IND} = "Y" ]]
   then
   {
      # hash and sort the exchng rate temporary tables before outerjoin operator
      CONVERT_CURRENCY="${DBREAD}
         <PROPERTY name = \"query\" >
	    <![CDATA[ 
               SELECT ${IDNT_TYPE_COLUMN}, 
                      day_dt, 
                      local_exchng_rate
                 FROM ${BA_OWNER}.${IDNT_TYPE}_EXCHNG_RATE_TEMP
	    ]]>
         </PROPERTY>
         <OPERATOR type = \"hash\">
            <PROPERTY name = \"key\" value = \"${IDNT_TYPE_COLUMN}\"/>
            <PROPERTY name = \"key\" value = \"DAY_DT\"/>
            <OPERATOR type = \"sort\">
               <PROPERTY name = \"key\" value = \"${IDNT_TYPE_COLUMN}\"/>
               <PROPERTY name = \"key\" value = \"DAY_DT\"/>
               <OUTPUT name=\"${IDNT_TYPE}_rate_temp.v\"/>
            </OPERATOR>
         </OPERATOR>
      </OPERATOR>"

      # hash and sort the input dataset from the driving query of the calling module. 
      CONVERT_CURRENCY="${CONVERT_CURRENCY}
        <OPERATOR type=\"hash\">
           <INPUT    name=\"$INPUT_DATASET.v\"/>
           <PROPERTY name=\"key\" value=\"${IDNT_TYPE_COLUMN}\"/>
           <PROPERTY name=\"key\" value=\"DAY_DT\"/>
           <OPERATOR type=\"sort\">
              <PROPERTY name=\"key\" value=\"${IDNT_TYPE_COLUMN}\"/>
              <PROPERTY name=\"key\" value=\"DAY_DT\"/>   
              <OUTPUT   name=\"${INPUT_DATASET}_temp.v\"/>
           </OPERATOR>
        </OPERATOR>

        <OPERATOR type = \"leftouterjoin\">
           <PROPERTY name = \"key\" value = \"${IDNT_TYPE_COLUMN}\"/>
           <PROPERTY name = \"key\" value = \"DAY_DT\"/>
           <PROPERTY name = \"nullvalue\" value = \"LOCAL_EXCHNG_RATE=-1\"/>
           <INPUT name = \"${INPUT_DATASET}_temp.v\"/>
           <INPUT name = \"${IDNT_TYPE}_rate_temp.v\"/>
           <OPERATOR type=\"filter\" >
              <PROPERTY name=\"filter\" value=\"not LOCAL_EXCHNG_RATE eq -1\"/>
              <PROPERTY name=\"rejects\" value=\"true\"/>
              <OUTPUT name=\"not_null_${INPUT_DATASET}.v\"/>
              <OUTPUT name=\"${INPUT_DATASET}_rej.v\"/>
           </OPERATOR>
        </OPERATOR>"

      if [[ ${PRIMARY2LOCAL} = "Y" ]]
      then
      {
         CONVERT_CURRENCY="${CONVERT_CURRENCY}
            <OPERATOR type=\"binop\">
               <INPUT name=\"not_null_${INPUT_DATASET}.v\"/>
               <PROPERTY name=\"left\" value=\"LOCAL_EXCHNG_RATE\"/>
               <PROPERTY name=\"operator\" value=\"/\"/>
               <PROPERTY name=\"constright\" value=\"${PRIME_EXCHNG_RATE}\"/>
               <PROPERTY name=\"dest\" value=\"EXCHNG_RATE\"/>
               <PROPERTY name=\"desttype\" value=\"dfloat\"/>
               <OUTPUT name=\"cal_${INPUT_DATASET}_0.v\"/>
            </OPERATOR>"
      }
      else
      {
         CONVERT_CURRENCY="${CONVERT_CURRENCY}
            <OPERATOR type=\"binop\">
               <INPUT name=\"not_null_${INPUT_DATASET}.v\"/>
               <PROPERTY name=\"constleft\" value=\"${PRIME_EXCHNG_RATE}\"/>
               <PROPERTY name=\"operator\" value=\"/\"/>
               <PROPERTY name=\"right\" value=\"LOCAL_EXCHNG_RATE\"/>
               <PROPERTY name=\"dest\" value=\"EXCHNG_RATE\"/>
               <PROPERTY name=\"desttype\" value=\"dfloat\"/>
               <OUTPUT name=\"cal_${INPUT_DATASET}_0.v\"/>
            </OPERATOR>"
      }
      fi

      integer N=0
      for field in ${CONVERT_AMT_FIELDS}
      do
         GENERATE_AMT="${GENERATE_AMT}
         <OPERATOR type=\"binop\">
            <INPUT name=\"cal_${INPUT_DATASET}_$N.v\"/>
            <PROPERTY name=\"left\" value=\"${field}\"/>
            <PROPERTY name=\"operator\" value=\"*\"/>
            <PROPERTY name=\"right\" value=\"EXCHNG_RATE\"/>
            <PROPERTY name=\"dest\" value=\"${field}_TMP\"/>
            <PROPERTY name=\"desttype\" value=\"dfloat\"/>"
            integer N="N + 1"
            GENERATE_AMT="${GENERATE_AMT}
           <OUTPUT name=\"cal_${INPUT_DATASET}_$N.v\"/>
        </OPERATOR>"
      done
      GENERATE_AMT="${GENERATE_AMT}
         <OPERATOR type=\"fieldmod\">
         <INPUT name=\"cal_${INPUT_DATASET}_${N}.v\"/>"

      if [[ ${PRIMARY2LOCAL} = "Y" ]]
      then
      {
         for field in ${CONVERT_AMT_FIELDS}
         do
            GENERATE_AMT="${GENERATE_AMT}
               <PROPERTY name=\"drop\" value=\"${field}_LCL\"/>
               <PROPERTY name=\"rename\" value=\"${field}_LCL=${field}_TMP\"/>"
         done
      }
      else
      {
         for field in ${CONVERT_AMT_FIELDS}
         do
            GENERATE_AMT="${GENERATE_AMT}
               <PROPERTY name=\"rename\" value=\"${field}_LCL=${field}\"/>
               <PROPERTY name=\"rename\" value=\"${field}=${field}_TMP\"/>"
         done
            GENERATE_AMT="${GENERATE_AMT}
               <PROPERTY name=\"drop\" value=\"LOCAL_EXCHNG_RATE\"/>
               <PROPERTY name=\"drop\" value=\"EXCHNG_RATE\"/>"
      }
      fi

      GENERATE_AMT="${GENERATE_AMT}
        <OUTPUT name=\"$OUTPUT_DATASET.v\"/>
      </OPERATOR>"

      CONVERT_CURRENCY="${CONVERT_CURRENCY} 
      ${GENERATE_AMT}"

      CONVERT_CURRENCY="${CONVERT_CURRENCY}
         <OPERATOR  type=\"generator\">
            <INPUT name=\"${INPUT_DATASET}_rej.v\"/>
            <PROPERTY name=\"schema\">
               <![CDATA[
                  <GENERATE>
                     <FIELD name=\"ERROR_MESSAGE\" type=\"string\">
                        <CONST value=\"Unable to convert currency for ${IDNT_TYPE_COLUMN}, DAY_DT\"/>
                     </FIELD>
                  </GENERATE>
               ]]>
            </PROPERTY>
            <OPERATOR type=\"fieldmod\">
               <PROPERTY name=\"keep\" value=\"ERROR_MESSAGE\"/>
               <PROPERTY name=\"keep\" value=\"${IDNT_TYPE_COLUMN}\"/>
               <PROPERTY name=\"keep\" value=\"DAY_DT\"/>
               <OPERATOR  type=\"export\">
                  <PROPERTY name=\"outputfile\" value=\"${CONV_CURR_REJ_FILE}\"/>
               </OPERATOR>
            </OPERATOR>
         </OPERATOR>"
   }
   else
   {
      CONVERT_CURRENCY="<OPERATOR type=\"generator\">
         <INPUT name=\"$INPUT_DATASET.v\"/>
         <PROPERTY name=\"schema\">
            <![CDATA[
               <GENERATE>"
               for field in ${CONVERT_AMT_FIELDS}
               do
                  CONVERT_CURRENCY=" ${CONVERT_CURRENCY} 
                     <FIELD name=\"${field}_LCL\" type=\"dfloat\" nullable=\"true\">
                     <CONST value=\"\"/>
                        </FIELD>"
               done
               CONVERT_CURRENCY=" ${CONVERT_CURRENCY}
                  </GENERATE>
               ]]>
               </PROPERTY>
               <OUTPUT name=\"${OUTPUT_DATASET}.v\"/>
            </OPERATOR>"
   }
   fi
fi
