#!/bin/ksh

########################################################
# Copyright (c) 2005, Retek Inc.  All rights reserved.
# $Workfile:  rmse_rpas_error_check.ksh  $
# $Revision: 289615 $
# $Modtime:   Aug 10  2005  18:56:37 CDT  $
########################################################

############################################################################
#
#  The rmse_rpas_error_check.ksh script initializes three constants
#  (PRESERVE_FILES_FLG, START_TIME, and ERR_CODE_FILE), and the
#  the ERR_DESC array, and defines three functions (error_check,
#  check_time and prep_log_file).
#
#  The error_check function is the main function in this script and 
#  the other two functions are called by it. The error_check function
#  is called from rmse_rpas.ksh, rdft.ksh and rdft_outage.ksh to determine
#  if a RETL program has encountered an error and what general type of 
#  error it was.
#
#  PRESERVE_FILES_FLG can be set to one during testing and maintenance
#  to prevent the error_check function from deleting TEMP_ERR_FILE
#  and TEMP_LOG_FILE as an aid to debugging.
#
#  START_TIME is used by the check_time function to filter out the
#  log file lines that were written during an earlier execution. The
#  format of this value is HH:MM:SS.
#
############################################################################

PRESERVE_FILES_FLG=0

START_TIME=$(date +%T)

###########################################################################
#  Get the value of DATE_TYPE as defined in rmse_rpas_config.env (this may 
#  be either "current_date" or "vdate". If it is set to "current_date",
#  the current date is being used to name the error files. If it is
#  "vdate" then the date in the vdate.txt file is to be used.
###########################################################################
  
FILE_DATE_TYPE=$(nawk 'substr($1,1,10) == "DATE_TYPE="{
    split($1, DT, "\"")
    date_type =  DT[2]
    print  date_type
    if(date_type == "vdate" || date_type == "current_date") exit 0
    else  exit  1
  }'  $ETC_DIR/rmse_rpas_config.env)
  
if [ $? -ne 0 ] ; then 
  echo
  echo ERROR - An error was encountered in rmse_rpas_error_check.ksh during initialization.
  echo
  echo DATE_TYPE may have an invalid value in rmse_rpas_config.env.
  echo
  echo DATE_TYPE in rmse_rpas_config.env is currently set to \"$FILE_DATE_TYPE\".
  echo
  exit 1
fi

##############################################################################
#
#  Set up the temporary error code file name:
#
#  ERR_CODE_FILE is the path and name of a file in the error directory
#  where the program name, error type number, exit status  and error
#  description are written each time error_check is called. This 
#  information is then read from this file by the calling program at the
#  end of its execution to enable it to print out a summary report, showing
#  the completion status of each RETL program.
#
##############################################################################

ERR_CODE_FILE=$ERR_DIR/${PROGRAM_NAME}_error_codes.txt

if [ "$PROGRAM_NAME" = "" ]; then
  echo
  echo "******  ERROR  ******"
  echo
  echo PROGRAM_NAME not defined !!!
  echo
  exit 1
fi

#  Start with a clean error code file:

if [ -f  $ERR_CODE_FILE ]; then rm $ERR_CODE_FILE; fi

#################################################
#  Define the error description messages:
#################################################

ERR_DESC[0]="Program completed successfully."
ERR_DESC[1]="\"ORA-\" found in error file (Oracle error)."
ERR_DESC[2]="\"ERROR\" message found in error or log file."
ERR_DESC[3]="\"FATAL\" message found in error file."
ERR_DESC[4]="\"Error\" or \"error\" msg in error or log file."
ERR_DESC[5]="\"Aborting\" message found in error file."
ERR_DESC[6]="\"WARN\" or \"WARNING\" msg in error or log file."
ERR_DESC[7]="Empty or non-existent log file."
ERR_DESC[8]="Empty or non-existent error file."
ERR_DESC[9]="Unknown error type."
ERR_DESC[10]="Non-zero exit status code."
ERR_DESC[11]="\"No such file\" message was found in error file."
ERR_DESC[12]="RETL Flow Error found in error file"
ERR_DESC[13]="\"not found\" error message found in error file."
ERR_DESC[14]="Invalid call to error_check function."
ERR_DESC[15]="\"WARN\" msg in error file."

###############################################################################
#
#  The error_check function is called from rmse_rpas.ksh, rdft.ksh
#  and rdft_outage.ksh to determine if a RETL program encountered
#  an error and what general type of error it was.
#
#  This function checks the exit status code and scans the error and
#  log files for errors by looking for key words that would indicate
#  that an error had occurred. This function calls the check_time
#  function which is also defined in this script.
#
#  Each time this function is called, a record is added to a temporary  
#  file ($ERR_CODE_FILE), with the program name, error code and
#  exit status code. This file is then accessed after all programs 
#  that are invoked by rdft.ksh, etc. have completed. The 
#  information in this file is used to display a summary on the
#  monitor screen of the completion results of each program.
#
#  Usage: error_check  prog_nam  exit_status  [START_TIME]
#
#  prog_nam: Name of program for which error conditions are to be checked.
#
#  exit_status: Exit status code of the program.
#
#  START_TIME: Optional program start time used only during test and
#              maintenance when error_check is called without any RETL
#              program having been run, using pre-existing log and error 
#              files. These log and error files may contain a non-current 
#              time of day in the records to be analysed. The format of
#              this argument must be HH:MM:SS.
#
#  External variables referenced by the error_check function:
#
#      PROGRAM_NAME - The name of the calling program (not the name of
#                     program for which possible errors will be analysed -
#                     that program name is passed to the error_check 
#                     function as the first argument).
#
#      ETC_DIR -      The path for the etc directory (initiallized by
#                     rmse_rpas_config.env).
#
#      LOG_DIR -      The path for the log directory (initiallized by
#                     rmse_rpas_config.env).
#
#      ERR_DIR -      The path for the error directory (initiallized by
#                     rmse_rpas_config.env).
#
#      LOG_DIR -     The path for the log file (initiallized by
#                     rmse_rpas_config.env).
#
#  Possible error codes that are placed in $ERR_CODE_FILE:
#  -------------------------------------------------------
#  0 =  No errors were found.
#  1 = "ORA-" was found in the error file (an Oracle error was detected).
#  2 = "ERROR" was found in the error or log file (an error may have been 
#       detected by the checkerror function).
#  3 = "FATAL" was found in the error file.
#  4 = "Error" or "error" was found in the error or log file.
#  5 = "Aborting" found in the error file (an rmse_terminate error message).
#  6 = "WARN" or "WARNING" was found in the error or log file (a possible
#       checkerror warning message indicating that a reject file has been
#       created).
#  7 = The log is empty or non-existent.
#  8 = The error is empty or non-existent.
#  9 = Unknown error type.
#  10 = A non-zero exit status code was detected after completion 
#      of the program.
#  11 = "No such file" message was found in error file.
#  12 = RETL Flow Error found in error file.
#  13 = "not found" error message found in error file.
#  14 = Invalid call to error_check function.
#  15 = "WARN" or "WARNING" was found in the error or log file, but
#       for rdft.ksh and outage.ksh, this type of error will not
#       be considered a program failure.
#
#  NOTE: One or two extra variables are written into the error code file
#        for each error found that are only used for maintenance and testing.
#        These are the number of error message lines found in the error file
#        and the same information for the log file. These are not used by the
#        calling programs, but can be examined by displaying the contents of
#        the file.
#
#  Written by D. W. Maas
#
###############################################################################

function error_check  {

  if [ $# -lt 2 ]; then
    echo $prog_nam 14 $exit_status err=$nerr Desc=${ERR_DESC[14]}     \
        >> $ERR_CODE_FILE
    return 14
  fi
  
  prog_nam=$1
  
  exit_status=$2
  
  #  Optional start time is only used in test and maintenance mode:
  
  if [ "$3" != "" ]; then  START_TIME=$3; fi
  
  if [ $prog_nam = pre_rmse_rpas ]; then ERR_FILE_DATE=$FILE_DATE
  
  else
  
    #########################################################################
    #  Get the date that was used to name the error file (this may be either
    #  the current date or the date in the vdate.txt file, depending on how
    #  DATE_TYPE is set in rmse_rpas_config.env):
    #########################################################################
 
    if [ $FILE_DATE_TYPE = "current_date" ]; then
      ERR_FILE_DATE=$(date +"%Y%m%d")
    elif [ -s  ${ETC_DIR}/vdate.txt ]; then
      ERR_FILE_DATE=$(cat $ETC_DIR/vdate.txt)
    else
      echo
      echo ERROR - Error encountered in rmse_rpas_error_check.ksh.
      echo
      echo ERROR: vdate.txt file was not found or is empty. 
      echo
      echo vdate.txt file is expected to be in $ETC_DIR
      echo
      exit 1
    fi
  fi
  
  ########################################################
  #  Define the error file and log file names and the 
  #  temporary files for the given program:
  ########################################################
  
  PROG_ERR_FILE=$ERR_DIR/$prog_nam.$ERR_FILE_DATE
  
  PROG_LOG_FILE=$LOG_DIR/$ERR_FILE_DATE.log
  
  #  TEMP_ERR_FILE is the path and name of a temporary file in the error
  #  directory where only that part of the error file that is relevant  
  #  to the latest execution of the current program is placed, before the
  #  scanning is performed:
  
  TEMP_ERR_FILE=$ERR_DIR/${prog_nam}_temp_error_file.txt
  
  #  TEMP_LOG_FILE is the path and name of a temporary file in the log
  #  directory which is a copy of the log file with the program name  
  #  and run time inserted at the beginning of every record in which  
  #  they were missing:

  TEMP_LOG_FILE=$LOG_DIR/${prog_nam}_temp_log_file.txt
  
  #  Initialize the file existence flags and then test whether 
  #  either the log or error file is empty or non-existent: 
  
  log_file_exists=0
  err_file_exists=0
  
  if [ -s  $PROG_LOG_FILE ]; then  log_file_exists=1; fi
  if [ -s  $PROG_ERR_FILE ]; then  err_file_exists=1; fi
  
  if [ $log_file_exists -eq 0 ] || [ $err_file_exists -eq 0 ]; then
  
    #  Either the error file or the log file was empty or non-existent.
    #  Check the program exit status code first:
   
    if [ $exit_status -ne 0 ]; then  #  Non-zero exit status code:
      echo $prog_nam 10 $exit_status Desc=${ERR_DESC[10]}  >>  $ERR_CODE_FILE
      if [ $PRESERVE_FILES_FLG -eq 0 ]; then
        if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
        if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
      fi
      return 10
      
    else 
     
    #  Exit status code was 0, so error code should indicate 
    #  which file was empty or not found:
    
      if [ $log_file_exists -eq 0 ]; then  errtyp=7
      elif [ $err_file_exists -eq 0 ]; then  errtyp=8
      fi
      echo $prog_nam $errtyp $exit_status Desc=${ERR_DESC[errtyp]} >> $ERR_CODE_FILE
      if [ $PRESERVE_FILES_FLG -eq 0 ]; then
        if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
        if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
      fi
      return $errtyp
      
    fi  #  End of "if [ $exit_status -ne 0 ]" test.
    
  fi  #  End of "[ $log_file_exists -eq 0 ] ..." test.
    
  warn_flg=0
  
  #  Ensure that these temporary files exist to avoid potential grep errors:
  
  touch $TEMP_ERR_FILE
  touch $TEMP_LOG_FILE 
  
  #  prep_log_file adds the program name and time of day to the beginning
  #  of each record in the log file for any records where they are missing
  #  and places the resulting file into $TEMP_LOG_FILE:
  
  prep_log_file
  
  #  Find the first line in the error file that was written
  #  during the last execution of the given program:
  
  first_line=$(nawk 'BEGIN{ ln = 0 }                        \
      $1 == "\\n******"{ if( $2 == "STARTING") ln = NR }       \
      END{ print ln }'  $PROG_ERR_FILE)
  
  #  Copy the lines from the last execution only
  #  from the error file into a temporary error file:
  
  if [ $first_line -gt 0 ]; then
    awk -v fline=$first_line 'NR >= fline {print $0}' $PROG_ERR_FILE > $TEMP_ERR_FILE
  else
    cp   $PROG_ERR_FILE  $TEMP_ERR_FILE
  fi

  #  Look in the error file for Oracle errors ("ORA-"). If found, write
  #  a line into the error code file containing the program name, error
  #  code, program exit status code and the number of error message lines
  #  found in the error file, along with a description of the type of error:
    
  nerr=$(grep -c "ORA-"  $TEMP_ERR_FILE)
  if [ $nerr -gt 0 ]; then
    echo $prog_nam 1 $exit_status err=$nerr Desc=${ERR_DESC[1]}      \
         >> $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 1
  fi
  
  #  Look in the error file for RETL Flow errors ("Ennn:"):
  
  err_num=""
  nerr2=$(grep -c "^E[0-9][0-9]:"  $TEMP_ERR_FILE)
  nerr3=$(grep -c "^E[0-9][0-9][0-9]:"  $TEMP_ERR_FILE)
  if [ $nerr2 -gt 0 ]; then
    nerr=$nerr2
    err_num=$(grep "^E[0-9][0-9]:"  $TEMP_ERR_FILE  |  head -1  |      \
        nawk -F: '{print $1}')
  elif [ $nerr3 -gt 0 ]; then
    nerr=$nerr3
    err_num=$(grep "^E[0-9][0-9][0-9]:"  $TEMP_ERR_FILE  |  head -1  |      \
        nawk -F: '{print $1}')
  fi
  if [ $nerr2 -gt 0 ] || [ $nerr3 -gt 0 ]; then
    echo "$prog_nam 12 $exit_status err=$nerr Desc=${ERR_DESC[12]} ($err_num)" \
         >>  $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 12
  fi 

  #  Look in the error file for a "No such file" error message:
    
  nerr=$(grep -c "No such file"  $TEMP_ERR_FILE)
  if [ $nerr -gt 0 ]; then
    echo $prog_nam 11 $exit_status err=$nerr Desc=${ERR_DESC[11]}      \
         >>  $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 11
  fi
  
  #  Look in the error file for a "not found" error message:
    
  nerr=$(grep -c "not found"  $TEMP_ERR_FILE)
  if [ $nerr -gt 0 ]; then
    echo $prog_nam 13 $exit_status err=$nerr Desc=${ERR_DESC[13]}      \
         >>  $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 13
  fi
  
  #  Look in the error file and the log file for "ERROR". If it is found,
  #  write a line into the error code file containing the program name,
  #  error code, program exit status code and the number of error message
  #  lines found in the error and log files, along with a description of
  #  the type of error:
    
  nerr=$(grep -c "ERROR"  $TEMP_ERR_FILE)
  nlog=$(grep "^$prog_nam "  $TEMP_LOG_FILE  |  grep  "ERROR"  |  check_time)
    if [ $nerr -gt 0 ] || [ $nlog -gt 0 ]; then
    echo $prog_nam 2 $exit_status err=$nerr log=$nlog Desc=${ERR_DESC[2]}     \
         >> $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 2
  fi
  
  #  Look in the error file for "FATAL":
  
  nerr=$(grep -c "FATAL"  $TEMP_ERR_FILE)
  if [ $nerr -gt 0 ]; then
    echo $prog_nam 3 $exit_status err=$nerr Desc=${ERR_DESC[3]}       \
         >>  $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 3
  fi
  
  #  Look in the error file for "Aborting":
    
  nerr=$(grep -c "Aborting"  $TEMP_ERR_FILE)
  if [ $nerr -gt 0 ]; then
    echo $prog_nam 5 $exit_status err=$nerr Desc=${ERR_DESC[5]}      \
         >>  $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 5
  fi
    
  #  Look in the error file and the log file for "error" (ignore case):
   
  nerr=$(grep -i "error"  $TEMP_ERR_FILE  |  grep -v "<PROPERTY"  |   \
       grep -vi  "no error"  |  grep -vic  "without error")
  nlog=$(grep "^$prog_nam "  $TEMP_LOG_FILE  |  grep -i  "error"  |       \
       grep -vi  "no error"  |  grep -vi  "without error"  |  check_time)
  if [ $nerr -gt 0 ] || [ $nlog -gt 0 ]; then
    echo $prog_nam 4 $exit_status err=$nerr log=$nlog Desc=${ERR_DESC[4]}     \
         >> $ERR_CODE_FILE
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 4
  fi

  #  Check for a non-zero exit status code:

  if [ $exit_status -ne 0 ]; then
    echo $prog_nam 10 $exit_status Desc=${ERR_DESC[10]}  >>  $ERR_CODE_FILE 
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    return 10
  fi
  
  #  Look in the error file for "WARN" and in the log file for "WARNING",
  #  returning an error code of 15 for any warning # 115, # 117 or # 142,
  #  and an error code of 6 for all others:
  
  ret_code=0
  nwarn=$(grep  "WARN"  $TEMP_ERR_FILE  |  grep -vc  W115)  #  All but # 115.
  nwarn_all=$(grep -c "WARN"  $TEMP_ERR_FILE)  #  All Warnings in error file.
  nwarn_more=$(grep  "WARN"  $TEMP_ERR_FILE  |  grep -v  W115        \
              |  grep -v  W117  |  grep -vc  W142)  #  More serious Warnings.
  let nwarn_less=nwarn_all-nwarn_more  #  Less serious Warnings.
  
  nlog=$(grep "^$prog_nam "  $TEMP_LOG_FILE  |  grep  "WARNING" | check_time)
  
  #  Treat rmse_rpas.ksh separately until it is modified to be compatible with
  #  the way rdft.ksh and outage.ksh handle warning messages:
  
  if [ $PROGRAM_NAME = "rmse_rpas" ]; then
    if [ $nwarn -gt 0 ] || [ $nlog -gt 0 ]; then
      #  "WARN" or "WARNING" (other than # 115) was found in error or log file:
      echo $prog_nam 6 $exit_status err=$nwarn log=$nlog Desc=${ERR_DESC[6]}  \
           >> $ERR_CODE_FILE
      ret_code=6
    fi
  else  #  Error handling for rdft.ksh and outage.ksh only:

    if [ $nwarn_more -gt 0 ] || [ $nlog -gt 0 ];then # If more serious Warnings:
    
      #  "WARN" or "WARNING" was found in error or log file (if Warning
      #  in error file, it was other than # 115, # 117 or # 142):
      echo $prog_nam 6 $exit_status err=$nwarn_more log=$nlog Desc=${ERR_DESC[6]} \
           >> $ERR_CODE_FILE
      ret_code=6

    elif [ $nwarn_less -gt 0 ]; then  #  If less serious Warnings:
    
      #  "WARN" # 115, # 117 or # 142 was found in error file:
      echo $prog_nam 15 $exit_status err=$nwarn_less log=$nlog Desc=${ERR_DESC[6]} \
           >> $ERR_CODE_FILE
      ret_code=15
    fi
  fi  #  End of Error handling for rdft.ksh and outage.ksh only.
  
    if [ $PRESERVE_FILES_FLG -eq 0 ]; then
      if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
      if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
    fi
    
  if [ $ret_code -ne 0 ]; then  return  $ret_code; fi

  #  If this point is reached, no predictable errors were found:

  echo $prog_nam 0 $exit_status Desc=${ERR_DESC[0]}  >>  $ERR_CODE_FILE
  if [ $PRESERVE_FILES_FLG -eq 0 ]; then
    if [ -f $TEMP_ERR_FILE ]; then  rm  $TEMP_ERR_FILE; fi
    if [ -f $TEMP_LOG_FILE ]; then  rm  $TEMP_LOG_FILE; fi
  fi
  return 0

}  #  End of function error_check

##############################################################################
#
#  The check_time function checks the lines of the log file that are 
#  passed to it by the previous grep commands, filters out all lines
#  in which the time stamp is earlier than START_TIME, and returns a
#  count of the number of lines remaining.
#
#  Only a single number is returned by this function, and that is a count 
#  of the number of lines passed to it that were written at the same time  
#  or after the program started running.
#
#  This function is called only by the error_check function.
#
#  It expects the input file to be piped to it, so $1 can only be 
#  referenced once and it is used as the input file to the awk script.
#
#  External variables referenced:
#
#      START_TIME - Calling program start time in HH:MM:SS format.
#
##############################################################################

function check_time  {

  nawk -v start_time=$START_TIME  'BEGIN{
    count = 0
    start_time_nbr = time_to_nbr(start_time)
  }
  NF > 1 {
    time = trim_col($2)
    time_nbr = time_to_nbr(time)
    if( time >= start_time)  count++
  }
  function trim_col (t)  {
    len = length(t)
    last_char = substr(t, len, 1)
    if( last_char == ":")  return  substr(t, 1, len - 1)
    else  return  t
  }
  function time_to_nbr (t)  {
    split(t, T, ":")
    return  T[1]  T[2]  T[3]
  }
  END {
    print count
    
  }'  $1  #  End of awk program
  
}  #  End of function check_time

#######################################################################
#
#  The prep_log_file function checks the lines of the log file
#  for the time of day as the second item on the line and if it
#  is not present, it adds the program name and time to the beginning
#  of the line. The program name and time are obtained from the last
#  record where they were present. This function is only called from
#  the error_check function.
#
#######################################################################

function prep_log_file  {

  nawk 'index($2,":") > 0 {prog = $1; time = $2; print; next}
  {print prog, time, $0 }'  $PROG_LOG_FILE  >  $TEMP_LOG_FILE
}

