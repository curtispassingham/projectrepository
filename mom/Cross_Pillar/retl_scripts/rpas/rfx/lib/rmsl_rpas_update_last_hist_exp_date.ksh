#!/bin/ksh
########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   rmsl_rpas_update_last_hist_exp_date.ksh  $
# $Revision: 289615 $
# $Modtime:   Feb 08 2006 13:59:54  $
########################################################

########################################################
# Script creates a temp table to allow all the rows
# pulled out of the item_loc_soh table to be updated
# with a new date.  The temp table contains each of
# the ROWIDs and the VDATE.
#
# This is called from rmse_rpas_weekly_sales.ksh.
########################################################

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########
function do_update_ils_last_hist
{
   DBNAME=$1
   TABLENAME=$2

 if [[ $DB_ENV = "DB2" ]]
 then
    exit -1;
 elif [[ $DB_ENV = "ORA" ]]
 then
    echo Updating rows with VDATE=\'$VDATE\'.
    sqlplus /nolog << EOF
    conn $SQLPLUS_LOGON;
    WHENEVER SQLERROR EXIT 4;
    UPDATE item_loc_soh 
         SET last_hist_export_date = TO_DATE('${VDATE}', 'YYYYMMDD')
       WHERE rowid in (select CHARTOROWID(row_id) 
                                       from ${TABLENAME}) ;
EOF

 elif [[ $DB_ENV = "TER" ]]
 then
    exit -1;
EOF
 fi
 checkerror -e $? -m "Update of last_hist_export_date failed"
}

function update_ils_last_hist
{
##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export INPUTFILE=${DATA_DIR}/rmse_rpas_weekly_sales.dat
export SCHEMAFILE=${SCHEMA_DIR}/rmse_rpas_weekly_sales.schema
export TABLENAME=ils_last_hist_export_date_temp

export REJ_FILE=${DATA_DIR}/${PROGRAM_NAME}.rej.dat

########################## MAIN ########################

# . Import the weekly_sales data file and load appropriate
#   data into temp table.

RETL_FLOW_FILE=$LOG_DIR/${PROGRAM_NAME}_update.xml

cat > $RETL_FLOW_FILE << EOF

<FLOW name="${PROGRAM_NAME}_update.flw">

   <!-- Read in the stock on hand data:  -->

   <OPERATOR  type="import">
      <PROPERTY  name="schemafile" value="${SCHEMAFILE}"/>
      <PROPERTY  name="inputfile" value="${INPUTFILE}"/>
      <PROPERTY  name="rejectfile" value="${REJ_FILE}"/>
      <OUTPUT name="import_weekly_sales.v"/>
   </OPERATOR>
   
   <!-- Keep only the records that indicate zero stock on hand:  -->
   <OPERATOR  type="fieldmod">
      <PROPERTY  name="keep" value="ROW_ID"/>
      <INPUT name="import_weekly_sales.v"/>
      <OUTPUT name="updated_rows.v"/>
   </OPERATOR>
  
   ${DBWRITE}
      <PROPERTY name="tablename" value="${TABLENAME}" />
      <PROPERTY name="createtablemode" value="recreate" />"
      <PROPERTY name="mode" value="truncate" />
      <INPUT name="updated_rows.v" />
   </OPERATOR>
</FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}

checkerror -e $? -m "Program failed - check $ERR_FILE"

#Check for the reject file and log an error if any records were rejected.
if [[ -s $REJ_FILE ]]
then
   log_num_recs ${REJ_FILE}
   message "**Records rejected by ${PROGRAM_NAME}_update"
   exit 1
else
   rm ${STATUS_FILE}
   rm -f ${REJ_FILE}
fi


# . Run the update to update rows identified in the temp
#   table with the new date.
do_update_ils_last_hist ${DBNAME} ${TABLENAME}
checkerror -e $? -m "Program failed - check $ERR_FILE"

# . Remove the temp table.
drop_tbl ${TABLENAME}
checkerror -e $? -m "Program failed - check $ERR_FILE"
}

