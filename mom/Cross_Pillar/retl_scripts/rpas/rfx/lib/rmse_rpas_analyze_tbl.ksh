#!/bin/ksh

########################################################
# Copyright (c) 2002, Retek Inc.  All rights reserved.
# $Workfile:   rmse_rpas_analyze_tbl.ksh  $
# $Revision: 289615 $
# $Modtime:   Feb 08 2006 08:10:16  $
########################################################

#####################################################################
#  Analyze table
#
#  Usage:  $1 = Table Ananlyze
#####################################################################
function analyze_tbl
{
if [[ $DB_ENV = "DB2" ]]
then
   db2 "connect to $DBNAME user $MMUSER using $PASSWORD"
   db2 "runstats on table $1 and detailed indexes all"
   checkerror -e $? -d -m "Can't analyze table $1"
   db2 "terminate" 
elif [[ $DB_ENV = "ORA" ]]
then
   if [[ $2 != "" ]]
   then
      sqlplus /nolog << EOF
        conn $SQLPLUS_LOGON;
        WHENEVER SQLERROR EXIT 4;
        BEGIN
           sys.dbms_stats.gather_table_stats('$1','$2');
        END;
        exit;
EOF
   else
      TBL=`echo $1 | cut -d "." -f2`
      sqlplus /nolog << EOF
        conn $SQLPLUS_LOGON;
        WHENEVER SQLERROR EXIT 4;
        BEGIN
           sys.dbms_stats.gather_table_stats('${BA_OWNER}','${TBL}');
        END;
      exit;
EOF
 checkerror -e $? -m "Analyzing table $1 failed"
fi
fi
}
