#!/bin/ksh

###################################################################
#
#  This script pulls the daily/weekly forecast items into RMS
#
#  Usage: rmsl_rpas_forecast.ksh  daily | weekly
#
#  During the loading of each domain file the following steps
#  are performed:
#
#  1. Truncate the partition in the RMS forecast table
#     which corresponds to the domain ID.
#     NOTE:  partition names should always be in the format:
#     [tablename]_P[domainID]
#  2. Append a domain field and insert the domain_id into each
#     record.
#  3. Load the forecast data into the RMS forecast table.
#
###################################################################

###################################################################
#  DEFINITIONS
###################################################################

export PROGRAM_NAME="rmsl_rpas_forecast"

###################################################################
#  INCLUDES
###################################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export TMP_FILE=${DATA_DIR}/${PROGRAM_NAME}_tmp
export TMP_FILE_VAL=${DATA_DIR}/${PROGRAM_NAME}_val

###############################################################
#  Get the appropriate RMS table name, schema file name and  
#  list of forecast data input files, depending on whether  
#  this processing run is for daily or weekly data:
###############################################################

set +f  #  Enable special character recognition for this section only.

case $1 in
  daily) 
    #  Loading daily forecast - set schema file and target table
    #  and populate the list of input files to load:
      
    message "Program started. Loading daily forecasts ..."
    
    TABLENAME="daily_item_forecast"
    INPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}_daily.schema
    if [ ! -e ${DATA_DIR}/d?demand.??* ] ; then
      message "${DATA_DIR}/d?demand.??* does not exist"
      rmse_terminate 1
    else
      export INPUT_FILE_LIST=$(echo ${DATA_DIR}/d?demand.??*)
    fi
    ;;
      
  weekly)
    #  Loading weekly forecast - set schema file and target table
    #  and populate the list of input files to load:
      
    message "Program started. Loading weekly forecasts ..."
      
    TABLENAME="item_forecast"
    INPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}_weekly.schema
    if [ ! -e ${DATA_DIR}/w?demand.??* ] ; then
      message "${DATA_DIR}/w?demand.??* does not exist"
      rmse_terminate 1
    else
      export INPUT_FILE_LIST=$(echo ${DATA_DIR}/w?demand.??*)
    fi
    ;;
    
  *)
    #  No argument or invalid argument specified:
      
    message "First argument must be either weekly or daily!"
    message "Program terminated."
    rmse_terminate 1 
    ;;
esac

set -f  #  Disable special character recognition, so that special 
        #  characters can be used in the RETL flow, if needed.
        
forecast_type=$1
        
message "INPUT_FILE_LIST = ${INPUT_FILE_LIST}"

nbr_files_processed=0

################################################################
#
#  Begin the loop to process each of the forecast data files:
#
################################################################

for INPUT_FILE in ${INPUT_FILE_LIST} ; do

  nr=$(wc -l ${INPUT_FILE}  |  nawk '{print $1}')

  message "Processing input file: $(basename ${INPUT_FILE}) (${nr} records)"
message $(basename ${INPUT_FILE}) 
  export DOMAIN_ID=`expr $(echo $(basename ${INPUT_FILE}) | cut -d'.' -f2) + 0`

  ###  Verify input file is not empty:

  if [[ ! -s ${INPUT_FILE} ]] ; then
    message "File ${INPUT_FILE} is empty."
    continue
  fi

  ###  Verify domain is in proper range (1 to 999),
  ###  and if domain_id is less than 10, convert   
  ###  it to an integer to remove the leading zero:

  if [[ ${DOMAIN_ID} -lt 01 || ${DOMAIN_ID} -gt 999 ]] ; then
    message "Invalid file: ${INPUT_FILE}"
    continue
  ### elif [[ ${DOMAIN_ID} -lt 10 ]] ; then
    ### typeset -i DOMAIN_ID
  fi

   FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_val.xml"

    cat > ${FLOW_FILE} << EOF

    <FLOW name = "${PROGRAM_NAME}_val.flw">

      ${DBREAD}
       <PROPERTY name = "query">
        <![CDATA[
           SELECT domain_id 
           FROM ${RMS_OWNER}.domain 
           WHERE domain_id=${DOMAIN_ID}
          ]]>
       </PROPERTY>
       <OPERATOR  type="export">
        <PROPERTY name="outputfile" value="${TMP_FILE_VAL}"/>
       </OPERATOR>
      </OPERATOR>

    </FLOW>
EOF

    message "Running the RETL flow to validate domain exists in RMS  ..."

    ###############################################################################
    #  Execute the preceding RETL flow statements:
    ###############################################################################

    ${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

    retl_stat=$?

    message "RETL flow completed for Domain ${DOMAIN_ID}. Exit status: ${retl_stat}"

    ###############################################################################
    #  Handle RETL errors
    ###############################################################################
   
    checkerror -e ${retl_stat} -m "RETL flow failed - check ${ERR_FILE}"
   
   if [[ ! -s ${TMP_FILE_VAL} ]] ; then
      message "Domain ${DOMAIN_ID} does not exists in RMS."
      rm ${TMP_FILE_VAL}
      continue
   else
      message "Domain ${DOMAIN_ID} is exists in RMS."
      rm ${TMP_FILE_VAL}
   fi

  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}_pre.xml"

  cat > ${FLOW_FILE} << EOF

    <FLOW name = "${PROGRAM_NAME}_pre.flw">

      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT get_partition(UPPER('${RMS_OWNER}'),
                                    UPPER('${TABLENAME}'),
                                   ${DOMAIN_ID})
                 FROM DUAL
            ]]>
         </PROPERTY>
         <OPERATOR  type="export">
            <PROPERTY name="outputfile" value="${TMP_FILE}"/>
         </OPERATOR>
      </OPERATOR>

    </FLOW>
EOF

  message "Running the RETL flow to get Partition name  ..."

  ###############################################################################
  #  Execute the preceding RETL flow statements:
  ###############################################################################

  ${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

  retl_stat=$?

  message "RETL flow completed for partiton name Domain ${DOMAIN_ID}. Exit status: ${retl_stat}"

  ###############################################################################
  #  Handle RETL errors
  ###############################################################################
 
  checkerror -e ${retl_stat} -m "RETL flow failed - check ${ERR_FILE}"

   if [[ -f ${TMP_FILE} ]]
   then
      export PARTITION_NAME_DOMAIN=$(cat ${TMP_FILE})
      rm ${TMP_FILE}
   else
      checkerror -e 2 -m "** Temporary file is missing."
   fi

  ##########################################################################
  #  Open a sqlplus connection and TRUNCATE the partition for this domain:
  ##########################################################################

  message "Truncating partition for Domain ID ${DOMAIN_ID}"

  sqlplus $SQLPLUS_LOGON << EOF >> ${ERR_FILE}
    WHENEVER SQLERROR EXIT 4;
    ALTER TABLE ${RMS_OWNER}.${TABLENAME} TRUNCATE PARTITION ${PARTITION_NAME_DOMAIN};
    DELETE FROM ${RMS_OWNER}.FORECAST_REBUILD WHERE DOMAIN_ID=(${DOMAIN_ID});
    INSERT INTO ${RMS_OWNER}.FORECAST_REBUILD VALUES(${DOMAIN_ID});
    EXIT;
EOF

  sql_exit_stat=$?

  if [ ${sql_exit_stat} -eq 0 ]; then
    stat=completed
  else
    stat=FAILED
  fi

  message "Truncate ${stat} (Domain ${DOMAIN_ID}). Exit status: ${sql_exit_stat}"

  checkerror -e ${sql_exit_stat} -m "SQL ALTER TABLE ${TABLENAME} TRUNCATE PARTITION failed - check ${ERR_FILE}"

  ###############################################################################
  #  Create a disk-based flow file
  ###############################################################################

  FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

  cat > ${FLOW_FILE} << EOF

    <FLOW name = "${PROGRAM_NAME}.flw">

      <!--  Read in the forecast file (dfdemand.??* or wfdemand.??*   -->
      <!--  depending on whether daily or weekly forecast data,     -->
      <!--  resp., is being processed):                             -->
   
      <OPERATOR type="import">
        <PROPERTY name="inputfile" value="${INPUT_FILE}"/>
        <PROPERTY name="schemafile" value="${INPUT_SCHEMA}"/>
        <PROPERTY  name="rejectfile" value="$(getRejectFile)"/>
        <OUTPUT    name="import.v"/>
      </OPERATOR>
   
      <!--  Add the DOMAIN_ID field and fill the field  -->
      <!--  with the domain id of the current file:     -->

      <OPERATOR  type="generator">
        <INPUT name="import.v" />
        <OUTPUT name="gen.v"/>
        <PROPERTY  name="schema"> <![CDATA[
          <GENERATE>
            <FIELD name="DOMAIN_ID" type="int16">
              <CONST value="${DOMAIN_ID}"/>
            </FIELD>
          </GENERATE> ]]>
        </PROPERTY>
      </OPERATOR>
   
      <!--  Load the forecast data with the Domain ID into the   -->
      <!--  appropriate RMS forecast table (DAILY_ITEM_FORECAST  -->
      <!--  or ITEM_FORECAST):                                   -->

      ${DBWRITE}
        <PROPERTY name="tablename" value="${RMS_OWNER}.${TABLENAME}" />
        <PROPERTY name="mode" value="append" />
        <PROPERTY name="outputdelimiter" value="0x02" />
        <INPUT name="gen.v" />
      </OPERATOR>
    </FLOW>
EOF

  message "Running the RETL flow for $(basename ${INPUT_FILE}) ..."

  ###############################################################################
  #  Execute the preceding RETL flow statements:
  ###############################################################################

  ${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

  retl_stat=$?

  message "RETL flow completed for Domain ${DOMAIN_ID}. Exit status: ${retl_stat}"

  ###############################################################################
  #  Handle RETL errors
  ###############################################################################
  
  checkerror -e ${retl_stat} -m "RETL flow failed - check ${ERR_FILE}"

  nbr_files_processed=`expr ${nbr_files_processed} + 1`

done

message "Processing completed for all ${forecast_type} files for table:     ${TABLENAME}"

message "Number of valid, non-empty input files processed: ${nbr_files_processed}"

###############################################################################
#  Concatenate all of the reject files together:
###############################################################################

if [ -s  ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST ]; then
  REJ_FILELIST=`cat ${ERR_DIR}/${PROGRAM_NAME}.REJ_FILELIST`
  cat ${REJ_FILELIST} > ${REJ_FILE}
fi

###############################################################################
#  Check for the reject file and log an error if any records were rejected:
###############################################################################

if [[ -s ${REJ_FILE} ]] ; then
  log_num_recs ${REJ_FILE}
  message "*** One or more records were rejected by ${PROGRAM_NAME}"
  rmse_terminate 1
fi

###############################################################################
#  Remove the status file, if present
###############################################################################

if [[ -f ${STATUS_FILE} ]] ; then 
  rm  ${STATUS_FILE} 
fi

###############################################################################
# Remove empty reject file, if present
###############################################################################

if [[ -f ${REJ_FILE} ]] ; then 
  rm ${REJ_FILE}
fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully."

###############################################################################
#  Cleanup and exit:
###############################################################################

rmse_terminate 0
