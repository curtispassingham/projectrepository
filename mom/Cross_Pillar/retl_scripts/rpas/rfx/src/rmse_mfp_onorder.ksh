#!/bin/ksh

########################################################
#  Script extracts item aggregates from orders.	
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="rmse_mfp_onorder"

########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh
perform_extract ()
{
########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_MFP_ONORDER_FILE=${DATA_DIR}/rmse_mfp_onorder_reject.txt
###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
select eow_date,
       item_id,
       to_char(location) location,
       to_char(round(sum(on_order_units),2)) on_order_units,
       to_char(round(sum(on_order_cost),2)) on_order_cost,
       to_char(round(sum(on_order_retail),2)) on_order_retail,
       to_char(error_desc) error_desc
from
(
with loc_summary as
(select l.*, DECODE('${CURR_PARAM}', 'P', s.currency_code, l.currency_code) output_currency
from
   (select store loc,
           'S' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.store
     where stockholding_ind='Y'
 union all
    select wh loc,
          'W' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.wh
     where stockholding_ind='Y')l, system_options s),
loc_exchange_rates as
( select rate.*, 
         CASE when rate.effective_date <= vdate and nvl(rate.next_effective_date, vdate + 1) > vdate then 'Y'
              else 'N'
         end current_rate_ind
    from ( select l.*, c.currency_cost_dec, c.currency_rtl_dec, r.exchange_rate, r.effective_date,
                  lead(r.effective_date) over (partition by l.loc,r.currency_code order by effective_date) next_effective_date
             from ( select ls.*, s.currency_code primary_curr,
                           CASE when consolidation_ind = 'Y' then 'C'      
                                else 'O' 
                           end exchange_type,
                           CASE when '${CURR_PARAM}' = 'P' then s.currency_code
                                else ls.currency_code
                           end output_curr
                      from loc_summary ls,
                           system_options s
                  ) l, currency_rates r, currencies c
           where r.currency_code(+) = l.currency_code
             and r.exchange_type(+) = l.exchange_type
             and c.currency_code = l.output_curr
         ) rate, period
),

order_item_summary as
   -- build a view of items on order, including both packs and non-packs
   (select od.order_no,
           ol.item,
           im.dept,
           im.class,
           im.pack_ind,
           ol.location,
           ol.loc_type,
           ls.vat_region,
           od.supplier,
           os.origin_country_id,
           case
              when ol.qty_ordered-NVL(ol.qty_received,0) > 0 then
                 ol.qty_ordered-NVL(ol.qty_received,0)
              else
                 0  -- over-receiving
           end on_order_units,
           CASE when od.currency_code = lr.output_curr
                   then ol.unit_cost
                when '${CURR_PARAM}' = 'L'
                   then ROUND(ol.unit_cost * NVL(lr.exchange_rate, 1) / NVL(od.exchange_rate,1), lr.currency_cost_dec)
                else ROUND(ol.unit_cost / NVL(od.exchange_rate,1), lr.currency_cost_dec) 
           END unit_cost,
           ol.unit_retail,
           od.currency_code
      from ${RMS_OWNER}.ordhead od,
           ${RMS_OWNER}.ordsku os,
           ${RMS_OWNER}.ordloc ol,
           loc_summary ls,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.deps d,
           loc_exchange_rates lr,
           ${RMS_OWNER}.period p
     where od.order_no     = ol.order_no
       and od.status       = 'A'
       and os.order_no     = ol.order_no
       and os.item         = ol.item
       and ol.item         = im.item
       and od.not_after_date <= (p.vdate + 7)
       and ol.location     = ls.loc
       and ol.loc_type     = ls.loc_type
       and im.status       = 'A'
       and NVL(im.deposit_item_type, 'X') not in ('A','T') --exclude deposit container and return item
       and im.inventory_ind = 'Y'                          --exclude non-inventory items
       and im.item_level <= im.tran_level                  --exclude reference items (item_level > tran_level)
       and NOT (im.item_xform_ind = 'Y' and im.sellable_ind = 'Y') --exclude sellable only transform items on PO
       and im.dept         = d.dept     --exclude consignment and concession items on PO
       and d.purchase_type = 0
       and lr.loc(+) = ol.location
       and lr.loc_type(+) = ol.loc_type
       and lr.current_rate_ind(+) = 'Y'
),
order_tax_item_summary as
   -- build a view of item/locs on order that we need to find tax for, including non-pack, pack, component items
   -- no duplicates
   (select ois.item,   --all items on order, pack or non-pack
           ois.dept,
           ois.class,
           ois.location,
           ois.loc_type,
           ois.vat_region
      from order_item_summary ois
     union
    select vpq.item,  --component items
           ois.dept,
           ois.class,
           ois.location,
           ois.loc_type,
           ois.vat_region
      from order_item_summary ois,
           ${RMS_OWNER}.v_packsku_qty vpq
     where ois.item = vpq.pack_no),
vat_rate_summary as
   --simple vat
   --only build the vat summary if default_tax_type is 'SVAT'
   --exclude items in classes with class_vat_ind = 'N', since they have an effective vat rate of 0 (NULL)
   (select item,
           vat_region,
           vat_rate/100 vat_rate,
           0 tax_rate,
           0 tax_amount,
           active_date
      from (select v.item,
                   v.vat_region,
                   v.vat_rate,
                   v.active_date,
                   row_number() over (partition by v.item, v.vat_region order by v.active_date desc, v.vat_type desc) rn
              from ${RMS_OWNER}.vat_item v,
                   ${RMS_OWNER}.item_master im,
                   ${RMS_OWNER}.class c,
                   ${RMS_OWNER}.system_options so
             where v.item = im.item
               and c.dept = im.dept
               and c.class = im.class
               and v.vat_type in ('R', 'B')
               and v.active_date <= (select vdate from ${RMS_OWNER}.period)
               and (so.class_level_vat_ind = 'N' or (so.class_level_vat_ind = 'Y' and c.class_vat_ind = 'Y'))
               and so.default_tax_type = 'SVAT')
     where rn = 1),
gtax_rate_summary as
   --gtax
   --only build the gtax summary if default_tax_type is 'GTAX'
   --limit to item/locs on order
   (select item,
           loc,
           loc_type,
           0 vat_rate,
           tax_rate/100 tax_rate,
           tax_amount,
           active_date
      from (select gt.item,
                   otis.location loc,
                   otis.loc_type,
                   gt.cum_tax_pct tax_rate,
                   gt.cum_tax_value tax_amount,
                   gt.effective_from_date active_date,
                   row_number() over (partition by gt.item, gt.loc order by gt.effective_from_date desc) rn
              from ${RMS_OWNER}.gtax_item_rollup gt,
                   order_tax_item_summary otis,
                   ${RMS_OWNER}.system_options so
             where otis.item            = gt.item
               and otis.location        = gt.loc
               and gt.loc_type          in ('ST', 'WH')
               and gt.effective_from_date   <= (select vdate from ${RMS_OWNER}.period)
               and so.default_tax_type  = 'GTAX')
     where rn = 1),
tax_rate_summary as
   --combine vat_rate_summary and gtax_rate_summary into a single view of item/loc/date/tax rate
   --limit to items on po
   (select so.default_tax_type,
           vrs.item,
           otis.location loc,
           otis.loc_type,
           vrs.vat_rate vat_rate,
           0 tax_rate,
           0 tax_amount,
           vrs.active_date
      from vat_rate_summary vrs,
           order_tax_item_summary otis,
           ${RMS_OWNER}.system_options so
     where vrs.item = otis.item
       and vrs.vat_region = otis.vat_region
       and so.default_tax_type = 'SVAT'
 union all
    select so.default_tax_type,
           grs.item,
           grs.loc,
           grs.loc_type,
           0 vat_rate,
           grs.tax_rate tax_rate,
           grs.tax_amount,
           grs.active_date
      from gtax_rate_summary grs,
           ${RMS_OWNER}.system_options so
     where so.default_tax_type = 'GTAX'),
pack_item_summary as
   -- build a view of a component item's total cost and total retail as part of a pack from a supplier/origin country/loc
   (select p.pack_no,
           isc.supplier,
           isc.origin_country_id,
           il.loc,
           p.item,
           MIN(p.qty*isc.unit_cost) total_pack_item_cost,
           DECODE(trs.default_tax_type,
                  'GTAX',
                   MIN((il.unit_retail-(il.unit_retail-nvl(trs.tax_amount,0))*
                        nvl(trs.tax_rate,0)-nvl(trs.tax_amount,0))*p.qty),
                   MIN(il.unit_retail/(1+nvl(trs.vat_rate,0))*p.qty)
                 ) total_pack_item_retail --without tax 
      from order_item_summary ois,
           ${RMS_OWNER}.v_packsku_qty p,
           ${RMS_OWNER}.item_loc il, --for componnet item's unit_retail
           ${RMS_OWNER}.item_supp_country isc, --for component item's unit_cost
           tax_rate_summary trs
     where ois.pack_ind = 'Y'
       and ois.item     = p.pack_no
       and p.item       = isc.item     --Use component item for unit_cost
       and ois.supplier = isc.supplier --Use order's supplier and origin country
       and ois.origin_country_id = isc.origin_country_id
       and p.item       = il.item      --Use component item for unit_retail
       and ois.location = il.loc
       and il.item      = trs.item(+)  --Use component item's tax rate
       and il.loc       = trs.loc(+)
       and il.loc_type  = trs.loc_type(+)
  group by p.pack_no, isc.supplier, isc.origin_country_id, il.loc, p.item, trs.default_tax_type),
pack_item_pct_summary as
   -- build a view of a component item's %cost and %retail w.r.t. the pack's total cost and total retail
   (select pack_no,
           supplier,
           origin_country_id,
           loc,
           item,
           total_pack_item_cost/DECODE(SUM(total_pack_item_cost) OVER (PARTITION BY pack_no, supplier, origin_country_id, loc),
                                       0, 1,
                                       SUM(total_pack_item_cost) OVER (PARTITION BY pack_no, supplier, origin_country_id, loc)) pct_cost, -- to avoid divide by 0
           total_pack_item_retail/DECODE(SUM(total_pack_item_retail) OVER (PARTITION BY pack_no, supplier, origin_country_id, loc),
                                         0, 1,
                                         SUM(total_pack_item_retail) OVER (PARTITION BY pack_no, supplier, origin_country_id, loc)) pct_retail -- to avoid divide by 0
      from pack_item_summary),
on_order_summary as
   -- build a view of items on order, including both non-packs on PO and pack component items on PO.
   (-- Non-packs on PO
    select sv.last_eow_date+7 eow_date,
           ois.item,
           ois.location,
           ois.on_order_units,
           ois.unit_cost*ois.on_order_units on_order_cost,
           DECODE(trs.default_tax_type,
                  'GTAX',
                  (ois.unit_retail-(ois.unit_retail-nvl(trs.tax_amount,0))*
                        nvl(trs.tax_rate,0)-nvl(trs.tax_amount,0))*ois.on_order_units,
                   ois.unit_retail/(1+nvl(trs.vat_rate,0))*ois.on_order_units
                 ) on_order_retail,
            ois.currency_code currency_code
      from order_item_summary ois,
           tax_rate_summary trs,
           ${RMS_OWNER}.system_variables sv
     where ois.pack_ind    = 'N'
       and ois.item        = trs.item(+)
       and ois.location    = trs.loc(+)
       and ois.loc_type    = trs.loc_type(+)
 UNION ALL
    --packs on PO
    select sv.last_eow_date+7 eow_date,
           vpq.item,    --component item
           ois.location,
           ois.on_order_units*vpq.qty on_order_units,
           --total cost prorated for the component item: pack unit_cost * on_order_units * component item's weighted cost percentage
           ois.unit_cost*ois.on_order_units*ps.pct_cost on_order_cost,
           --total retail (without tax) prorated for the component item: 
             --pack unit_retail/(1+tax_rate) * on_order_units * component item's weighted retail percentage
           DECODE(trs.default_tax_type,
                  'GTAX',
                  (ois.unit_retail-(ois.unit_retail-nvl(trs.tax_amount,0))*
                        nvl(trs.tax_rate,0)-nvl(trs.tax_amount,0))*ois.on_order_units*ps.pct_retail,
                   ois.unit_retail/(1+nvl(trs.vat_rate,0))*ois.on_order_units*ps.pct_retail
                 ) on_order_retail,
           ois.currency_code currency_code
      from order_item_summary ois,
           ${RMS_OWNER}.v_packsku_qty vpq,
           pack_item_pct_summary ps,
           tax_rate_summary trs,
           ${RMS_OWNER}.system_variables sv
     where ois.pack_ind    = 'Y'
       and ois.item        = vpq.pack_no
       and ps.pack_no      = vpq.pack_no
       and ps.item         = vpq.item
       and ps.supplier     = ois.supplier
       and ps.origin_country_id = ois.origin_country_id
       and ps.loc          = ois.location
       and ois.item        = trs.item(+)  --pack's tax_rate
       and ois.location    = trs.loc(+)
       and ois.loc_type    = trs.loc_type(+))
--Main Query
--Switch item id with parent item:diff if the item is aggregated by diff.
--Aggregate indicators can only be defined at the top level. For a 3-level item, it would be at grandparent item.
--If item_aggregate_ind = 'Y', at least one diff's aggregate_ind must be 'Y'.
--If parent or grandparent item's item_aggregate_ind = 'Y', output should always be aggregated by the parent item:diff.
select s.eow_date, 
       s.item_id, 
       s.location, 
       s.on_order_units, 
       s.on_order_cost,
       CASE when '${CURR_PARAM}' = 'L' or lr.currency_code = lr.primary_curr then s.on_order_retail
       else ROUND(s.on_order_retail/NVL(lr.exchange_rate,1), lr.currency_rtl_dec)   
       END on_order_retail,
       CASE WHEN ls.output_currency != s.currency_code AND lr.exchange_rate IS NULL THEN '${ERROR_CODE}'
       END error_desc
  from
(select eow_date,
       case when im2.item_aggregate_ind = 'N' then
               im1.item_parent
            else
               im1.item_parent||(case when(im2.diff_1_aggregate_ind='Y') then ':'|| im1.diff_1 else NULL end)||
                                (case when(im2.diff_2_aggregate_ind='Y') then ':'|| im1.diff_2 else NULL end)||
                                (case when(im2.diff_3_aggregate_ind='Y') then ':'|| im1.diff_3 else NULL end)||
                                (case when(im2.diff_4_aggregate_ind='Y') then ':'|| im1.diff_4 else NULL end)
       end item_id,
       location,
       on_order_units,
       on_order_cost,       
       on_order_retail,
       currency_code
  from on_order_summary os,
       ${RMS_OWNER}.item_master im1, -- item on order or component of pack on order
       ${RMS_OWNER}.item_master im2  -- parent or grandparent item to check aggregate ind
 where os.item = im1.item
   and im1.item_parent is NOT NULL
   and NVL(im1.item_grandparent, im1.item_parent) = im2.item
union all
select eow_date,
       os.item item_id,
       location,
       on_order_units,
       on_order_cost,
       on_order_retail,
       currency_code
  from on_order_summary os,
       ${RMS_OWNER}.item_master im1 -- item on order or component of pack on order
 where os.item = im1.item
   and im1.item_parent is NULL
)s,loc_exchange_rates lr, loc_summary ls
where lr.loc(+) = s.location
  and ls.loc = s.location
  and lr.current_rate_ind(+) = 'Y'
)
group by eow_date,
         item_id,
         location,
         error_desc
             ]]>
         </PROPERTY>
         <OUTPUT name = "output.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
      <INPUT    name="output.v"/>
      <PROPERTY name="filter" value="error_desc IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="error_data.v"/>
      <OUTPUT   name="output_data_final.v"/>
      </OPERATOR>
   
      <OPERATOR type="export">
         <INPUT    name="output_data_final.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
      
      
     <OPERATOR type="export">
         <INPUT    name="error_data.v"/>
         <PROPERTY name="outputfile" value="${REJECT_MFP_ONORDER_FILE}"/>
      </OPERATOR>
   </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"
###############################################################################

# Log the number of records written to the output file
log_num_recs ${OUTPUT_FILE}

#Check for the reject file and log an error if any records were rejected.
if [[ -s ${REJECT_MFP_ONORDER_FILE} ]]
then
   log_num_recs ${REJECT_MFP_ONORDER_FILE}
   message "**Records rejected by ${PROGRAM_NAME}"
   exit 1
else
   rm -f ${REJECT_MFP_ONORDER_FILE}
   message "Program completed successfully"
fi
###############################################################################
# Remove the status file
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################
}
########################################################
#  INPUT DEFINES
#  This section must come after INCLUDES
########################################################
ERROR_CODE="Exchange rate does not exist"
opt=$1

if [ -z "$opt" ]; then
export CURR_PARAM="L"
export opt="L"
fi 

case $opt in
   P)
     # Primary Currency -
     
     message "Primary currency selected for amounts"
     export CURR_PARAM="P"
     perform_extract
     ;;
   L)
     # Local Currency -
     
     message "Local currency selected for amounts"
     export CURR_PARAM="L"
     perform_extract
     ;;
     
   *)
    # Argument specified is NULL:
    # No argument or other argument specified:
    if [ "$opt" -ne "P" ] || [ "$opt" -ne "L" ] || ! [ -z "$opt" ]; then 
     message "Wrong input parameter passed"
     message "Argument must be either Primary(P) or Local(L) or Default(NULL)!" 
     message "Program terminated." 
     rmse_terminate 1
    fi ;;
esac  
rmse_terminate 0
