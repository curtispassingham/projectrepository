#!/bin/ksh

################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_rdf_daily_sales"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. $RMS_RPAS_HOME/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

#################################################################
#                                                               #
################  CUSTOMIZABLE FOR EACH CLIENT:  ################
#                                                               #
#  The USE_IF_TRAN_DATA constant must be set to "Y" or "N",     #
#  depending on customer preference. The effect of this         #
#  switch is as follows:                                        #
#                                                               #
#  USE_IF_TRAN_DATA = Y: The IF_TRAN_DATA table will be used    #
#                        in the queries.                        #
#                                                               #
#  USE_IF_TRAN_DATA = N: The TRAN_DATA_HISTORY table will be    # 
#                        used in the queries, instead of        #
#                        IF_TRAN_DATA.                          #
#                                                               #
#################################################################

USE_IF_TRAN_DATA=Y

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

message "Program started. USE_IF_TRAN_DATA = $USE_IF_TRAN_DATA."

if [ $USE_IF_TRAN_DATA = "Y" ]; then

  message "Using IF_TRAN_DATA table. DOMAIN_LEVEL = $DOMAIN_LEVEL."

  #  Comments describing the WHERE clause in the following queries:
  #
  #     AND ((itd.loc_type = 'S' 
  #           AND itd.tran_code = 1  #  Net Sales tran_code
  #           AND itd.units > 0)     #  Adjusted for Gross Sales by
  #                                  #  excluding returns (negative units)
  #          or (itd.loc_type = 'W' 
  #              and itd.tran_code = 32)) #  Transfers-Out tran_code

  case $DOMAIN_LEVEL in
  
    D) export QUERY="SELECT  /*+ PARALLEL(itd, $NO_OF_CPUS) */
             ils.loc,
             itd.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             dd.domain_id
        FROM ${RMS_OWNER}.domain_dept dd,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0) 
              OR
              (itd.loc_type = 'W'
               AND itd.tran_code = 32))
         AND im.dept = dd.dept
         AND itd.dept = dd.dept
         AND itd.item = im.item
         AND not exists( select 'Y' 
                           from ${RMS_OWNER}.sub_items_detail sid 
                          where ( sid.item = im.item or sid.sub_item = im.item ) 
                            AND sid.substitute_reason='P' 
                            AND itd.tran_date between sid.start_date and sid.end_date)
         AND ils.loc = itd.location
         AND ils.item = im.item
       GROUP BY ils.loc,
                itd.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                dd.domain_id
    UNION 
	SELECT  /*+ PARALLEL(itd, $NO_OF_CPUS) */
             ils.loc,
             sid.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             dd.domain_id
        FROM ${RMS_OWNER}.domain_dept dd,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.sub_items_detail sid
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0) 
              OR
              (itd.loc_type = 'W'
               AND itd.tran_code = 32))
         AND im.dept = dd.dept
         AND itd.dept = dd.dept
         AND itd.item = im.item
         AND ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
         AND itd.tran_date between sid.start_date and sid.end_date
         AND ils.loc = itd.location
         AND ils.item = im.item
       GROUP BY ils.loc,
                sid.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                dd.domain_id
      ORDER BY 2, 1";;
      
    C) export QUERY="SELECT  /*+ PARALLEL(itd, $NO_OF_CPUS) */
             ils.loc,
             itd.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             dc.domain_id
        FROM ${RMS_OWNER}.domain_class dc,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0)
              OR
               (itd.loc_type = 'W'
                AND itd.tran_code = 32))
         AND im.dept = dc.dept
         AND im.class = dc.class
         AND itd.dept = dc.dept
         AND itd.class = dc.class
         AND itd.item = im.item
         AND not exists( select 'Y' 
                           from ${RMS_OWNER}.sub_items_detail sid 
                          where ( sid.item = im.item or sid.sub_item = im.item ) 
                            AND sid.substitute_reason='P' 
                            AND itd.tran_date between sid.start_date and sid.end_date)
         AND ils.loc = itd.location
         AND ils.item = im.item
       GROUP BY ils.loc,
                itd.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                dc.domain_id
    UNION				
	SELECT  /*+ PARALLEL(itd, $NO_OF_CPUS) */
             ils.loc,
             sid.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             dc.domain_id
        FROM ${RMS_OWNER}.domain_class dc,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.sub_items_detail sid
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0)
              OR
               (itd.loc_type = 'W'
                AND itd.tran_code = 32))
         AND im.dept = dc.dept
         AND im.class = dc.class
         AND itd.dept = dc.dept
         AND itd.class = dc.class
         AND itd.item = im.item
         AND ils.loc = itd.location
         AND ils.item = im.item
         AND ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
         AND itd.tran_date between sid.start_date and sid.end_date
       GROUP BY ils.loc,
                sid.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                dc.domain_id
      ORDER BY 2, 1";;
      
    S) export QUERY="SELECT  /*+ USE_HASH(DS ITD ILS) PARALLEL_INDEX(ILS, PK_ITEM_LOC_SOH, $NO_OF_CPUS) PARALLEL(DS $NO_OF_CPUS) PARALLEL(ITD $NO_OF_CPUS) */
             ils.loc,
             itd.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             ds.domain_id
        FROM ${RMS_OWNER}.domain_subclass ds,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0)
              OR
              (itd.loc_type = 'W'
               AND itd.tran_code = 32))
         AND im.dept = ds.dept
         AND im.class = ds.class
         AND im.subclass = ds.subclass
         AND itd.dept = ds.dept
         AND itd.class = ds.class
         AND itd.subclass = ds.subclass
         AND itd.item = im.item
         AND not exists( select 'Y' 
                           from ${RMS_OWNER}.sub_items_detail sid 
                          where ( sid.item = im.item or sid.sub_item = im.item ) 
                            AND sid.substitute_reason='P' 
                            AND itd.tran_date between sid.start_date and sid.end_date)
         AND ils.loc = itd.location
         AND ils.item = im.item
       GROUP BY ils.loc,
                itd.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                ds.domain_id
	UNION			
	SELECT  /*+ USE_HASH(DS ITD ILS) PARALLEL_INDEX(ILS, PK_ITEM_LOC_SOH, $NO_OF_CPUS) PARALLEL(DS $NO_OF_CPUS) PARALLEL(ITD $NO_OF_CPUS) */
             ils.loc,
             sid.item,
             itd.tran_date,
             SUM(itd.units) SUM_UNITS,
             NVL(itd.sales_type,'I') SALES_TYPE,
             itd.tran_code,
             ds.domain_id
        FROM ${RMS_OWNER}.domain_subclass ds,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.if_tran_data itd,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.sub_items_detail sid
       WHERE im.forecast_ind = 'Y'
         AND ((itd.loc_type = 'S'
               AND itd.tran_code = 1
               AND itd.units > 0)
              OR
              (itd.loc_type = 'W'
               AND itd.tran_code = 32))
         AND im.dept = ds.dept
         AND im.class = ds.class
         AND im.subclass = ds.subclass
         AND itd.dept = ds.dept
         AND itd.class = ds.class
         AND itd.subclass = ds.subclass
         AND itd.item = im.item
         AND ils.loc = itd.location
         AND ils.item = im.item
         AND ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
         AND itd.tran_date between sid.start_date and sid.end_date
       GROUP BY ils.loc,
                sid.item,
                itd.tran_date,
                itd.sales_type,
                itd.tran_code,
                ds.domain_id
      ORDER BY 2, 1";;
      
    *) echo "Invalid Domain Level ($DOMAIN_LEVEL)!!";;
  esac

else

  message "Using TRAN_DATA_HISTORY table. DOMAIN_LEVEL = $DOMAIN_LEVEL ..."

  case $DOMAIN_LEVEL in
    D) export QUERY="SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */ 
          ils.loc,
          tdh.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dd.domain_id
      FROM ${RMS_OWNER}.domain_dept dd,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.tran_data_history tdh ,
           ${RMS_OWNER}.item_loc_soh ils
      WHERE im.forecast_ind = 'Y'
      AND   tdh.loc_type = 'S' and tdh.tran_code = 1 
      AND  tdh.location       = ils.loc
      AND  tdh.units > 0
      AND  tdh.item         = im.item
      AND  tdh.item         = ils.item
      AND  not exists( select 'Y' 
                         from ${RMS_OWNER}.sub_items_detail sid 
                        where ( sid.item = im.item or sid.sub_item = im.item ) 
                          AND sid.substitute_reason='P' 
                          AND tdh.tran_date between sid.start_date and sid.end_date)
      AND  ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
             AND dd.load_sales_ind = 'N')
             OR (dd.load_sales_ind = 'Y'))
      AND im.dept          = dd.dept
      AND tdh.dept=dd.dept
      GROUP BY ils.loc,
             tdh.item,
             tdh.tran_date,
             tdh.sales_type,
             tdh.tran_code,
             dd.domain_id
      UNION ALL   
      SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */ 
          ils.loc, 
          tdh.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dd.domain_id
      FROM ${RMS_OWNER}.domain_dept dd,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.tran_data_history tdh ,
           ${RMS_OWNER}.item_loc_soh ils
      WHERE im.forecast_ind = 'Y'
      AND   tdh.loc_type = 'W' and tdh.tran_code = 32
      AND  tdh.location       = ils.loc
      AND  tdh.item         = im.item
      AND  tdh.item         = ils.item
      AND  not exists( select 'Y' 
                         from ${RMS_OWNER}.sub_items_detail sid 
                        where ( sid.item = im.item or sid.sub_item = im.item ) 
                          AND sid.substitute_reason='P' 
                          AND tdh.tran_date between sid.start_date and sid.end_date)
      AND  ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
            AND dd.load_sales_ind = 'N')
            OR (dd.load_sales_ind = 'Y'))
      AND im.dept          = dd.dept
      AND tdh.dept=dd.dept
      GROUP BY ils.loc,
               tdh.item,
               tdh.tran_date,
               tdh.sales_type,
               tdh.tran_code,
               dd.domain_id
	UNION 		   
	SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */ 
          ils.loc,
          sid.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dd.domain_id
      FROM ${RMS_OWNER}.domain_dept dd,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.tran_data_history tdh ,
           ${RMS_OWNER}.item_loc_soh ils,
           ${RMS_OWNER}.sub_items_detail sid
      WHERE im.forecast_ind = 'Y'
      AND   tdh.loc_type = 'S' and tdh.tran_code = 1 
      AND  tdh.location       = ils.loc
      AND  tdh.units > 0
      AND  tdh.item         = im.item
      AND  tdh.item         = ils.item
      AND  ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
      AND  tdh.tran_date between sid.start_date and sid.end_date
      AND  ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
             AND dd.load_sales_ind = 'N')
             OR (dd.load_sales_ind = 'Y'))
      AND im.dept          = dd.dept
      AND tdh.dept=dd.dept
      GROUP BY ils.loc,
             sid.item,
             tdh.tran_date,
             tdh.sales_type,
             tdh.tran_code,
             dd.domain_id
      UNION  
      SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */ 
          ils.loc, 
          sid.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dd.domain_id
      FROM ${RMS_OWNER}.domain_dept dd,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.tran_data_history tdh ,
           ${RMS_OWNER}.item_loc_soh ils,
           ${RMS_OWNER}.sub_items_detail sid
      WHERE im.forecast_ind = 'Y'
      AND  tdh.loc_type = 'W' and tdh.tran_code = 32
      AND  tdh.location       = ils.loc
      AND  tdh.item         = im.item
      AND  tdh.item         = ils.item
      AND  ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
      AND  tdh.tran_date between sid.start_date and sid.end_date
      AND  ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
            AND dd.load_sales_ind = 'N')
            OR (dd.load_sales_ind = 'Y'))
      AND im.dept          = dd.dept
      AND tdh.dept=dd.dept
      GROUP BY ils.loc,
               sid.item,
               tdh.tran_date,
               tdh.sales_type,
               tdh.tran_code,
               dd.domain_id
      ORDER BY 2, 1";;
      
    C) export QUERY="SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */
          ils.loc,
          tdh.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dc.domain_id
      FROM ${RMS_OWNER}.tran_data_history tdh,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.item_loc_soh ils,
           ${RMS_OWNER}.domain_class dc
      WHERE im.forecast_ind = 'Y'
      AND   ((tdh.loc_type = 'S' 
               AND tdh.tran_code = 1
               AND tdh.units > 0)
               OR (tdh.loc_type = 'W' and tdh.tran_code = 32))
      AND tdh.location      = ils.loc
      AND tdh.item         = im.item
      AND tdh.item         = ils.item
      AND not exists( select 'Y' 
                        from ${RMS_OWNER}.sub_items_detail sid 
                       where ( sid.item = im.item or sid.sub_item = im.item ) 
                         AND sid.substitute_reason='P' 
                         AND tdh.tran_date between sid.start_date and sid.end_date)
      AND ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
               AND dc.load_sales_ind = 'N')
           OR (dc.load_sales_ind = 'Y'))
      AND im.dept          = dc.dept
      AND im.class         = dc.class
      GROUP BY ils.loc,
               tdh.item,
               tdh.tran_date,
               tdh.sales_type,
               tdh.tran_code,
	       dc.domain_id
	UNION	   
	SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */
          ils.loc,
          sid.item,
          tdh.tran_date,
          SUM(tdh.units) SUM_UNITS,
          NVL(tdh.sales_type,'I') SALES_TYPE,
          tdh.tran_code,
          dc.domain_id
      FROM ${RMS_OWNER}.tran_data_history tdh,
           ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.item_loc_soh ils,
           ${RMS_OWNER}.domain_class dc,
           ${RMS_OWNER}.sub_items_detail sid
      WHERE im.forecast_ind = 'Y'
      AND   ((tdh.loc_type = 'S' 
               AND tdh.tran_code = 1
               AND tdh.units > 0)
               OR (tdh.loc_type = 'W' and tdh.tran_code = 32))
      AND tdh.location      = ils.loc
      AND tdh.item         = im.item
      AND tdh.item         = ils.item
      AND ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
      AND  tdh.tran_date between sid.start_date and sid.end_date
      AND ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
               AND dc.load_sales_ind = 'N')
           OR (dc.load_sales_ind = 'Y'))
      AND im.dept          = dc.dept
      AND im.class         = dc.class
      GROUP BY ils.loc,
               sid.item,
               tdh.tran_date,
               tdh.sales_type,
               tdh.tran_code,
	       dc.domain_id
      ORDER BY 2, 1";;
      
    S) export QUERY="SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */
             ils.loc,
             tdh.item,
             tdh.tran_date,
             SUM(tdh.units) SUM_UNITS,
             NVL(tdh.sales_type,'I') SALES_TYPE,
             tdh.tran_code,
	           ds.domain_id
      FROM   ${RMS_OWNER}.tran_data_history tdh,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.domain_subclass ds
      WHERE im.forecast_ind = 'Y'
      AND ((tdh.loc_type = 'S' 
               AND tdh.tran_code = 1
               AND tdh.units > 0)
               OR (tdh.loc_type = 'W' and tdh.tran_code = 32))
      AND tdh.location = ils.loc
      AND tdh.item        = im.item
      AND tdh.item        = ils.item
      AND not exists( select 'Y' 
                        from ${RMS_OWNER}.sub_items_detail sid 
                       where ( sid.item = im.item or sid.sub_item = im.item ) 
                         AND sid.substitute_reason='P' 
                         AND tdh.tran_date between sid.start_date and sid.end_date)
      AND ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
              AND ds.load_sales_ind = 'N')
              OR (ds.load_sales_ind = 'Y'))
      AND im.dept          = ds.dept
      AND im.class         = ds.class
      AND im.subclass      = ds.subclass
      GROUP BY ils.loc,
                tdh.item,
                tdh.tran_date,
                tdh.sales_type,
                tdh.tran_code,
		ds.domain_id
	UNION	
	SELECT  /*+ PARALLEL(tdh, $NO_OF_CPUS) */
             ils.loc,
             sid.item,
             tdh.tran_date,
             SUM(tdh.units) SUM_UNITS,
             NVL(tdh.sales_type,'I') SALES_TYPE,
             tdh.tran_code,
	     ds.domain_id
      FROM   ${RMS_OWNER}.tran_data_history tdh,
             ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.domain_subclass ds,
             ${RMS_OWNER}.sub_items_detail sid
      WHERE im.forecast_ind = 'Y'
      AND ((tdh.loc_type = 'S' 
               AND tdh.tran_code = 1
               AND tdh.units > 0)
               OR (tdh.loc_type = 'W' and tdh.tran_code = 32))
      AND tdh.location = ils.loc
      AND tdh.item        = im.item
      AND tdh.item        = ils.item
      AND ((im.item = sid.item OR im.item = sid.sub_item ) AND sid.substitute_reason='P')
      AND  tdh.tran_date between sid.start_date and sid.end_date
      AND ((tdh.tran_date > NVL(ils.last_hist_export_date, tdh.tran_date - 1)
              AND ds.load_sales_ind = 'N')
              OR (ds.load_sales_ind = 'Y'))
      AND im.dept          = ds.dept
      AND im.class         = ds.class
      AND im.subclass      = ds.subclass
      GROUP BY ils.loc,
                sid.item,
                tdh.tran_date,
                tdh.sales_type,
                tdh.tran_code,
		ds.domain_id
      ORDER BY 2, 1";;
      
    *) echo "Invalid Domain Level ($DOMAIN_LEVEL)!!";;
  esac

fi

# Things to do:
# . Dyanmically query the DB to get the domain level (above).
# . Add post-processing to remove special characters.

########################################################
#  Extract the daily sales data from the RMS database:
########################################################

extract_with_schema  $PROGRAM_NAME  $OUTPUT_SCHEMA  $OUTPUT_FILE  $QUERY

################################################################
#  Check for errors that may have occurred during the extract:
################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

#########################################################
# Log the number of records written to the output file:
#########################################################

log_num_recs $OUTPUT_FILE

############################################################################
#  Check for the reject file and log an error if any records were rejected:
############################################################################

if [[ -s $REJ_FILE ]]; then
   log_num_recs $REJ_FILE
   message "******  Records were rejected by ${PROGRAM_NAME}"
   rmse_terminate 1
else
   rm  ${STATUS_FILE}
   rm -f  ${REJ_FILE}
   message "Program completed successfully"
fi

#  Cleanup and exit:

rmse_terminate 0
