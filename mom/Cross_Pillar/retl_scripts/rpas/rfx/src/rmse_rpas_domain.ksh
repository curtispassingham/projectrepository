#!/bin/ksh

########################################################
#  Script extracts domain dimension information from the
#  RMS domain table
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################
export PROGRAM_NAME="rmse_rpas_domain"

########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT d.DOMAIN_ID, 
                      dm.DOMAIN_DESC, 
                      d.DEPT, 
                      NULL CLASS, 
                      NULL SUBCLASS, 
                      d.LOAD_SALES_IND
               FROM ${RMS_OWNER}.DOMAIN_DEPT d,
                    ${RMS_OWNER}.DOMAIN dm, 
                    ${RMS_OWNER}.SYSTEM_OPTIONS s 
               WHERE s.DOMAIN_LEVEL = 'D'
                 AND d.DOMAIN_ID = dm.DOMAIN_ID 
               UNION 
               SELECT c.DOMAIN_ID,
                      dm.DOMAIN_DESC, 
                      c.DEPT, 
                      c.CLASS, 
		              NULL SUBCLASS, 
                      c.LOAD_SALES_IND 
               FROM ${RMS_OWNER}.DOMAIN_CLASS c, 
                    ${RMS_OWNER}.DOMAIN dm, 
                    ${RMS_OWNER}.SYSTEM_OPTIONS s 
               WHERE s.DOMAIN_LEVEL = 'C' 
                 AND c.DOMAIN_ID = dm.DOMAIN_ID
               UNION 
               SELECT ds.DOMAIN_ID, 
                      dm.DOMAIN_DESC, 
                      ds.DEPT, 
                      ds.CLASS, 
                      ds.SUBCLASS, 
                      ds.LOAD_SALES_IND 
               FROM ${RMS_OWNER}.DOMAIN_SUBCLASS ds, 
                    ${RMS_OWNER}.DOMAIN dm, 
                    ${RMS_OWNER}.SYSTEM_OPTIONS s 
               WHERE s.DOMAIN_LEVEL = 'S'
                 AND ds.DOMAIN_ID = dm.DOMAIN_ID
            ]]>
         </PROPERTY>
         <OUTPUT  name = "query.v"/>
      </OPERATOR>
    
      <OPERATOR type="convert">
         <INPUT name="query.v"/>
         <PROPERTY name="convertspec">
            <![CDATA[
               <CONVERTSPECS>
                  <CONVERT destfield="DEPT" sourcefield="DEPT" newtype="int16">
                     <CONVERTFUNCTION name="default">
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="CLASS" sourcefield="CLASS" newtype="int16">
                     <CONVERTFUNCTION name="default">
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="SUBCLASS" sourcefield="SUBCLASS" newtype="int16">
                     <CONVERTFUNCTION name="default">
                     </CONVERTFUNCTION>
                  </CONVERT>
                  <CONVERT destfield="DOMAIN_ID" sourcefield="DOMAIN_ID" newtype="int8">
                     <CONVERTFUNCTION name="default">
                     </CONVERTFUNCTION>
                  </CONVERT>
               </CONVERTSPECS>
            ]]>
         </PROPERTY>
         <OUTPUT name="output.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="output.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>

   </FLOW>

EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
