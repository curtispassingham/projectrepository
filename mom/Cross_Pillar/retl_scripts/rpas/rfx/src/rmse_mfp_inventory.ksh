#!/bin/ksh

########################################################
#  Script extracts item aggregates from orders.	
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="rmse_mfp_inventory"

########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

###############################################################################
#  Function perform_extract
###############################################################################

perform_extract_initial ()
{

########################################################
#  OUTPUT DEFINES 
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.I.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_MFP_INVENTORY_FILE=${DATA_DIR}/${PROGRAM_NAME}_reject.txt

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
select eow_date,
       item_id,
       to_char(location) location,
       to_char(sum(clearance_inventory_units)) clearance_inventory_units,
       to_char(round(sum(clearance_inventory_cost),2)) clearance_inventory_cost,
       to_char(round(sum(clearance_inventory_retail),2)) clearance_inventory_retail,
       to_char(sum(regular_inventory_units)) regular_inventory_units,
       to_char(round(sum(regular_inventory_cost),2)) regular_inventory_cost,
       to_char(round(sum(regular_inventory_retail),2)) regular_inventory_retail,
       to_char(sum(receipt_units)) receipt_units,
       to_char(round(sum(receipt_cost),2)) receipt_cost,
       to_char(round(sum (receipt_retail),2)) receipt_retail,
       error_desc
from
(
with loc_summary as
(select l.*, DECODE('${CURR_PARAM}', 'P', s.currency_code, l.currency_code) output_currency
from
   (select store loc,
           'S' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.store
     where stockholding_ind='Y'
 union all
    select wh loc,
           'W' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.wh
     where stockholding_ind='Y') l, system_options s),
loc_exchange_rates as
(select rate.*, 
         CASE when rate.effective_date <= vdate and nvl(rate.next_effective_date, vdate + 1) > vdate then 'Y'
              ELSE 'N'
         END current_rate_ind
from
( select l.*, r.exchange_rate, r.effective_date, 
         lead(r.effective_date) over (partition by l.loc,r.currency_code order by effective_date) next_effective_date
    from ( select ls.*, c.currency_cost_dec, c.currency_rtl_dec,
                           CASE when consolidation_ind = 'Y' then 'C'
                                else 'O' 
                           end exchange_type
                      from loc_summary ls,
                           currencies c,
                           system_options s
                     where c.currency_code = ls.currency_code
                       and ls.currency_code != ls.output_currency
                  ) l, currency_rates r
  where r.currency_code(+) = l.currency_code
    and r.exchange_type(+) = l.exchange_type
)rate,period
),
tran_data_agg as
(select (case when (select 
                         (case when (vdate+(7-curr_454_day)) >= TRUNC(tran_date) 
                               then (7-(mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)))
                               else decode((mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)),0,7,(mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)))
                          end) 
                      from ${RMS_OWNER}.period)=7 then 
          TRUNC(tran_date) else
         ((TRUNC(tran_date))+(7-(select
                                      (case when (vdate+(7-curr_454_day)) >= TRUNC(tran_date) 
                                            then (7-(mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)))
                                            else decode((mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)),0,7,(mod(abs((vdate+(7-curr_454_day))-TRUNC(tran_date)),7)))
                                       end)
                                       from ${RMS_OWNER}.period))) 
         end ) eow_tran_date,
         td1.item, 
         td1.location, 
         td1.loc_type,
         td1.units,
         NVL2(lr.exchange_type, ROUND(total_cost/lr.exchange_rate, lr.currency_cost_dec), total_cost) total_cost,
         NVL2(lr.exchange_type, ROUND(total_retail/lr.exchange_rate, lr.currency_rtl_dec), total_retail) total_retail
         from ${RMS_OWNER}.tran_data_history td1,
              ${RMS_OWNER}.period per,
              loc_exchange_rates lr,
              loc_summary ls
              where item IS NOT NULL
                and lr.loc(+) = td1.location
                and lr.loc_type(+) = td1.loc_type
                and lr.effective_date(+) <= td1.tran_date 
                and NVL(lr.next_effective_date(+), td1.tran_date + 1) > td1.tran_date
                AND ls.loc(+) = lr.loc
                and ls.loc_type(+) = lr.loc_type                
                and td1.tran_date between (select first_day
                    from ${RMS_OWNER}.calendar
                   where year_454=(select to_number(to_char(vdate,'YYYY'),9999)-2 from ${RMS_OWNER}.period)
                     and month_454=1) and (select LAST_EOW_DATE_UNIT from ${RMS_OWNER}.SYSTEM_VARIABLES)
                     and td1.tran_code=20)
select td.eow_tran_date eow_date,
                  im.item_parent||(case when(im2.diff_1_aggregate_ind='Y') then ':'|| im.diff_1 else NULL end)||
                                  (case when(im2.diff_2_aggregate_ind='Y') then ':'|| im.diff_2 else NULL end)||
                                  (case when(im2.diff_3_aggregate_ind='Y') then ':'|| im.diff_3 else NULL end)||
                                  (case when(im2.diff_4_aggregate_ind='Y') then ':'|| im.diff_4 else NULL end) item_id,
                  td.location location,
                  0 clearance_inventory_units,
                  0 clearance_inventory_cost,
                  0 clearance_inventory_retail,
                  0 regular_inventory_units,
                  0 regular_inventory_cost,
                  0 regular_inventory_retail,
                  nvl(td.units,0) receipt_units,
                  nvl(td.total_cost,0) receipt_cost,
                  nvl(td.total_retail,0) receipt_retail,
                  CASE when ls.output_currency != ls.currency_code and lr.exchange_rate is NULL then '${ERROR_CODE}'
                  END error_desc
             from ${RMS_OWNER}.item_master im,
                  tran_data_agg td,
                  ${RMS_OWNER}.item_master im2,
                  loc_summary ls,
                  loc_exchange_rates lr
            where im.status = 'A'
              and NVL(im.deposit_item_type, 'X') not in ('A', 'T')  -- exclude deposit container and return items
              and im.inventory_ind = 'Y'                            -- exclude non-inventory items
              and im.item_level <= im.tran_level                    -- exclude reference items (item_level > tran_level)
              and exists (select 'x' from ${RMS_OWNER}.deps ds      -- exclude consignment/concession items
                           where ds.dept=im.dept 
                             and ds.purchase_type=0)
              and NOT (im.item_xform_ind = 'Y' and im.sellable_ind = 'Y') -- exclude sellable only transform items
              and im2.item=NVL(im.item_grandparent, NVL(im.item_parent, im.item))
              and im2.item_aggregate_ind is not NULL
              and im.item_parent is not NULL
              and im.pack_ind='N'
              and im.item=td.item
              and ls.loc=td.location
              and ls.loc_type=td.loc_type
              and lr.loc(+) = ls.loc
              and lr.loc_type(+) = ls.loc_type
              and lr.current_rate_ind(+) = 'Y'
               
UNION ALL

select td.eow_tran_date eow_date,
       im.item item_id,
       td.location location,
       0 clearance_inventory_units,
       0 clearance_inventory_cost,
       0 clearance_inventory_retail,
       0 regular_inventory_units,
       0 regular_inventory_cost,
       0 regular_inventory_retail,
       nvl(td.units,0) receipt_units,
       nvl(td.total_cost,0) receipt_cost,
       nvl(td.total_retail,0) receipt_retail,
       CASE when ls.output_currency != ls.currency_code and lr.exchange_rate is NULL then '${ERROR_CODE}'
       END error_desc
          from ${RMS_OWNER}.item_master im,
               tran_data_agg td,
               loc_summary ls,
               loc_exchange_rates lr
         where im.status = 'A'
           and NVL(im.deposit_item_type, 'X') not in ('A', 'T')  -- exclude deposit container and return items
           and im.inventory_ind = 'Y'                            -- exclude non-inventory items
           and im.item_level <= im.tran_level                    -- exclude reference items (item_level > tran_level)
           and exists (select 'x' from ${RMS_OWNER}.deps ds      -- exclude consignment/concession items
                        where ds.dept=im.dept 
                          and ds.purchase_type=0)
           and NOT (im.item_xform_ind = 'Y' and im.sellable_ind = 'Y') -- exclude sellable only transform items
           and im.item_parent is NULL 
           and im.pack_ind='N'
           and td.item=im.item
           and ls.loc=td.location
           and ls.loc_type=td.loc_type
           and lr.loc(+) = ls.loc
           and lr.loc_type(+) = ls.loc_type
           and lr.current_rate_ind(+) = 'Y'
             )
group by eow_date,
         item_id,
         location,
         error_desc
         
           ]]>
         </PROPERTY>
         <OUTPUT name = "output.v"/>
      </OPERATOR>

      <OPERATOR type="filter">
      <INPUT    name="output.v"/>
      <PROPERTY name="filter" value="error_desc IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="error_data.v"/>
      <OUTPUT   name="output_data_final.v"/>
      </OPERATOR>
   
      <OPERATOR type="export">
         <INPUT    name="output_data_final.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
      
      
     <OPERATOR type="export">
         <INPUT    name="error_data.v"/>
         <PROPERTY name="outputfile" value="${REJECT_MFP_INVENTORY_FILE}"/>
      </OPERATOR>
   </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
# Log the number of records written to the output file
log_num_recs ${OUTPUT_FILE}

#Check for the reject file and log an error if any records were rejected.
if [[ -s ${REJECT_MFP_INVENTORY_FILE} ]]
then
   log_num_recs ${REJECT_MFP_INVENTORY_FILE}
   message "**Records rejected by ${PROGRAM_NAME}"
   exit 1
else
   rm -f ${REJECT_MFP_INVENTORY_FILE}
   message "Program completed successfully"
fi
###############################################################################
# Remove the status file
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################
}

###############################################################################
#  Function perform_extract
###############################################################################

perform_extract ()
{

########################################################
#  OUTPUT DEFINES 
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.W.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema
export REJECT_MFP_INVENTORY_FILE=${DATA_DIR}/${PROGRAM_NAME}_reject.txt

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
select eow_date,
       item_id,
       to_char(location) location,
       to_char(sum(clearance_inventory_units)) clearance_inventory_units,
       to_char(round(sum(clearance_inventory_cost),2)) clearance_inventory_cost,
       to_char(round(sum(clearance_inventory_retail),2)) clearance_inventory_retail,
       to_char(sum(regular_inventory_units)) regular_inventory_units,
       to_char(round(sum(regular_inventory_cost),2)) regular_inventory_cost,
       to_char(round(sum(regular_inventory_retail),2)) regular_inventory_retail,
       to_char(sum(receipt_units)) receipt_units,
       to_char(round(sum(receipt_cost),2)) receipt_cost,
       to_char(round(sum(receipt_retail),2)) receipt_retail,
       error_desc
from
(
with loc_summary as
(select l.*, DECODE('${CURR_PARAM}', 'P', s.currency_code, l.currency_code) output_currency
from
   (select store loc,
           'S' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.store
     where stockholding_ind='Y'
 union all
    select wh loc,
           'W' loc_type,
           vat_region,
           currency_code
      from ${RMS_OWNER}.wh
     where stockholding_ind='Y') l, system_options s),
loc_exchange_rates as
( select rate.*, 
         CASE when rate.effective_date <= vdate and nvl(rate.next_effective_date, vdate + 1) > vdate then 'Y'
              else 'N'
         end current_rate_ind
    from ( select l.*, r.exchange_rate, r.effective_date, 
                  lead(r.effective_date) over (partition by l.loc,r.currency_code order by effective_date) next_effective_date
             from ( select ls.*, c.currency_cost_dec, c.currency_rtl_dec,
                           CASE when consolidation_ind = 'Y' then 'C'
                                else 'O' 
                           end exchange_type
                      from loc_summary ls,
                           currencies c,
                           system_options s
                     where c.currency_code = ls.currency_code
                       and ls.currency_code != ls.output_currency
                  ) l, currency_rates r
           where r.currency_code(+) = l.currency_code
             and r.exchange_type(+) = l.exchange_type
         ) rate, period
),
item_loc_summary as
   --build a view of items, loc, soh, av_cost and unit_retail, include both pack and non-pack.
   (select  /*+ PARALLEL(il) PARALLEL(ils)*/ im.item,   
           im.dept,
           im.class,
           im.subclass,
           im.pack_ind,
           ils.loc,
           ils.loc_type,
           ls.vat_region,
           il.clear_ind,
           ils.stock_on_hand stock_on_hand,
           NVL(ils.pack_comp_soh,0) pack_comp_soh,
           ils.stock_on_hand + NVL(ils.pack_comp_soh,0) units,
           NVL(ils.av_cost,0) av_cost,
           NVL(il.unit_retail,0) unit_retail
      from ${RMS_OWNER}.item_master im,
           ${RMS_OWNER}.item_loc il,
           ${RMS_OWNER}.item_loc_soh ils,
           ${RMS_OWNER}.deps d,
           ${RMS_OWNER}.loc_summary ls
     where im.item      = il.item
       and ils.item     = il.item
       and ils.loc      = il.loc
       and ils.loc_type = il.loc_type
       -- Only extract non-pack or packs at warehouses that are received as 'P'
       and (im.pack_ind = 'N' or (im.pack_ind = 'Y' and il.loc_type = 'W' and NVL(il.receive_as_type, 'E') = 'P'))
       and im.status    = 'A'
       and NVL(im.deposit_item_type, 'X') not in ('A', 'T') --exclude deposit container and return items
       and im.inventory_ind = 'Y'                           --exclude non-inventory items
       and im.item_level <= im.tran_level                   --exclude reference items (item_level > tran_level)
       and NOT (im.item_xform_ind = 'Y' and im.sellable_ind = 'Y') --exclude sellable only transform items
       and im.dept      = d.dept                            --exclude consignment and concession items
       and d.purchase_type = 0
       and ls.loc       = il.loc
       and ls.loc_type  = il.loc_type),
  vat_rate_summary as
   --combine vat_rate_summary and gtax_rate_summary into a single view of item/loc/date/tax rate		   
   (select ils.item,
           ils.loc vat_region,
           ils.loc_type,
           vat_type tax_type,
           vat_rate tax_rate,
           active_date effective_from
      from 
   --simple vat
   --only build the vat summary if default_tax_type is 'SVAT' 
   --exclude items in classes with class_vat_ind = 'N', since they have an effective vat rate of 0 (NULL)
   (select item,
           vat_region,
           vat_type,
           vat_rate,
           active_date
      from (select v.item,
                   v.vat_region,
                   v.vat_type,
                   v.vat_rate,
                   v.active_date,
                   row_number() over (partition by v.item, v.vat_region order by v.active_date desc, v.vat_type desc) rn
              from ${RMS_OWNER}.vat_item v,
                   ${RMS_OWNER}.item_master im,
                   ${RMS_OWNER}.class c,
                   ${RMS_OWNER}.system_options so
             where v.item = im.item
               and c.dept = im.dept
               and c.class = im.class
               and v.vat_type in ('R', 'B')
               and v.active_date <= (select vdate from period)
               and ((so.class_level_vat_ind = 'N')
                or (so.class_level_vat_ind = 'Y'
               and c.class_vat_ind = 'Y'))
               and so.default_tax_type = 'SVAT')
     where rn = 1)vrs ,     item_loc_summary ils
          where vrs.item = ils.item 
            and vrs.vat_region = ils.vat_region
),		   
gtax_rate_summary as
   --gtax
   --only build the gtax summary if default_tax_type is 'GTAX'
   (select item,
           loc,
           loc_type,
           cum_tax_pct/100 tax_rate,
           cum_tax_value tax_amount,
           effective_from
      from (select gt.item,
                   ils.loc,
                   ils.loc_type,
                   gt.cum_tax_pct,
                   gt.cum_tax_value,
                   gt.effective_from_date effective_from,
                   row_number() over (partition by gt.item, gt.loc order by gt.effective_from_date desc) rn
              from gtax_item_rollup gt,
                   item_loc_summary ils,
                   system_options so
             where gt.item              = ils.item
               and ils.loc              = gt.loc
               and gt.loc_type          in ('ST', 'WH')
               and gt.effective_from_date   <= (select vdate from period)
               and so.default_tax_type  = 'GTAX')
     where rn = 1),
tax_rate_summary as
   --combine vat_rate_summary and gtax_rate_summary into a single view of item/loc/date/tax rate
   (select so.default_tax_type,
           ils.item,
           ils.loc,
           ils.loc_type,
           vrs.tax_rate vat_rate,
           0 tax_rate,
           0 tax_amount,
           vrs.effective_from
      from vat_rate_summary vrs,
           item_loc_summary ils,
           system_options so
     where vrs.item = ils.item
       and vrs.vat_region = ils.vat_region
       and so.default_tax_type = 'SVAT'
 union all
    select so.default_tax_type,
           item,
           loc,
           loc_type,
           0 vat_rate,
           tax_rate,
           tax_amount,
           effective_from
      from gtax_rate_summary grs,
           system_options so
     where so.default_tax_type = 'GTAX'),
pack_item_summary as
   (select /*+ PARALLEL(il)*/ p.pack_no,
           p.item,
           ils.loc,
           ils.loc_type,
--is: why the MIN?
           DECODE(trs.default_tax_type,
                  'GTAX',
                   MIN(il.unit_retail-((il.unit_retail-nvl(trs.tax_amount,0))*
                        nvl(trs.tax_rate,1)-nvl(trs.tax_amount,0))*p.qty),
                   MIN(il.unit_retail/(1+nvl(trs.vat_rate,0))*p.qty)
                 ) total_pack_item_retail
      from ${RMS_OWNER}.item_loc_summary ils, -- pack
           ${RMS_OWNER}.v_packsku_qty p,
           ${RMS_OWNER}.item_loc il,  --component items for unit retail
           ${RMS_OWNER}.tax_rate_summary trs --component item's tax rate
     where ils.item       = p.pack_no
       and ils.pack_ind   = 'Y'
       and ils.loc_type   = 'W'
       and p.item         = il.item
       and ils.loc        = il.loc
       and ils.loc_type   = il.loc_type
       and il.item        = trs.item(+)  --Use component item's tax rate
       and il.loc         = trs.loc(+)
       and il.loc_type    = trs.loc_type(+)
   group by p.pack_no, p.item, ils.loc, ils.loc_type, trs.default_tax_type),
pack_item_pct_summary as
   -- build a view of a component item's %cost and %retail w.r.t. the pack's total cost and total retail
   (select pack_no,
           item,
           loc,
           loc_type,
           total_pack_item_retail/DECODE(SUM(total_pack_item_retail) OVER (PARTITION BY pack_no, loc),
                                         0, 1,
                                         SUM(total_pack_item_retail) OVER (PARTITION BY pack_no, loc)) pct_retail -- to avoid divide by 0
      from pack_item_summary),
tran_data_agg as
   (select item,
           location,
           SUM(units) units,
           SUM(total_cost) total_cost,
           SUM(total_retail) total_retail
      from ${RMS_OWNER}.tran_data_history,
           ${RMS_OWNER}.system_variables
     where tran_date between LAST_EOW_DATE_UNIT+1 and LAST_EOW_DATE_UNIT+7
       and tran_code = 20
       and item IS NOT NULL
   group by item, location),
inventory_item_summary as
   --build a view of item/locs to extract, including both non-packs and pack component items.
   --When a pack is received at a warehouse where receive_as_type is 'P', in addition to
   --updating pack's item_loc_soh.stock_on_hand, component item's item_loc_soh.pack_comp_soh
   --and item_loc_soh.av_cost are also updated to reflect the received qty. As a result,
   --we can use component item's stock_on_hand + pack_comp_soh (i.e. ils.units) to derive
   --inventory_units and inventory_cost. No pack prorating is needed.
   --For inventory_retail, since ITEM_LOC.UNIT_RETAIL contains tax and tax_rate can
   --potentially vary between pack and its component items, we'll use pack's unit_retail
   --and prorate it to its component items.
   (--Non-packs
    select sv.last_eow_date_unit + 7 eow_date,
           ils.item,
           ils.loc location,
           decode(ils.clear_ind,'Y',ils.units,0) clearance_inventory_units,
           decode(ils.clear_ind,'Y',ils.units,0)*ils.av_cost clearance_inventory_cost,
           decode(ils.clear_ind,
                  'Y',
                  ils.stock_on_hand,0) *
             decode(trs.default_tax_type,
                    'GTAX',
                    ils.unit_retail-((ils.unit_retail-nvl(trs.tax_amount,0)) *
                                     nvl(trs.tax_rate,1) - nvl(trs.tax_amount,0)),
                    ils.unit_retail/(1+nvl(trs.vat_rate,0))) clearance_inventory_retail,
           decode(ils.clear_ind,'N',ils.units,0) regular_inventory_units,
           decode(ils.clear_ind,'N',ils.units,0)*ils.av_cost regular_inventory_cost,
           decode(ils.clear_ind,
                  'N',
                  ils.stock_on_hand,0) *
             decode(trs.default_tax_type,
                    'GTAX',
                    ils.unit_retail-((ils.unit_retail-nvl(trs.tax_amount,0)) *
                                     nvl(trs.tax_rate,1) - nvl(trs.tax_amount,0)),
                    ils.unit_retail/(1+nvl(trs.vat_rate,0))) regular_inventory_retail,
           NVL(td.units,0) receipt_units,
           NVL(td.total_cost,0) receipt_cost,
           NVL(td.total_retail,0) receipt_retail
      from item_loc_summary ils,
           tran_data_agg td,
           tax_rate_summary trs,
           ${RMS_OWNER}.system_variables sv
     where ils.pack_ind    = 'N'
       and ils.item        = td.item(+)
       and ils.loc         = td.location(+)
       and ils.item        = trs.item(+)
       and ils.loc         = trs.loc(+)
       and ils.loc_type    = trs.loc_type(+)
 UNION ALL
    --packs:
    --inventory units and inventory cost are already included in the non-pack calculation via pack_comp_soh.
        select /*+ PARALLEL(il) */ sv.last_eow_date_unit + 7 eow_date,
           p.item,  --component item
           ils.loc location,
           0 clearance_inventory_units,
           0 clearance_inventory_cost,
           --
           decode(ils.clear_ind,
                  'Y',
                  ils.stock_on_hand,0)*
             decode(trs.default_tax_type,
                    'GTAX',
                    ils.unit_retail-((ils.unit_retail-nvl(trs.tax_amount,0)) *
                                     nvl(trs.tax_rate,1) - nvl(trs.tax_amount,0)),
                    ils.unit_retail/(1+nvl(trs.vat_rate,0)))
             * ps.pct_retail clearance_inventory_retail,
           --
           0 regular_inventory_units,
           0 regular_inventory_cost,
           --
           decode(ils.clear_ind,
                  'N',
                  ils.stock_on_hand,0)*
             decode(trs.default_tax_type,
                    'GTAX',
                    ils.unit_retail-((ils.unit_retail-nvl(trs.tax_amount,0)) *
                                     nvl(trs.tax_rate,1) - nvl(trs.tax_amount,0)),
                    ils.unit_retail/(1+nvl(trs.vat_rate,0)))
             * ps.pct_retail regular_inventory_retail,
           --
           0 receipt_units,  --packs are never on tran_data
           0 receipt_cost,
           0 receipt_retail
      from item_loc_summary ils,
           ${RMS_OWNER}.v_packsku_qty p,
           ${RMS_OWNER}.item_loc il,  --pack
           tax_rate_summary trs, --pack
           pack_item_pct_summary ps, --component item's retaill contribution pct
           ${RMS_OWNER}.system_variables sv
     where ils.pack_ind    = 'Y'
       and p.pack_no       = ils.item
       and ils.item        = il.item  --pack's unit_retail
       and ils.loc         = il.loc
       and ils.loc_type    = il.loc_type
       and ils.item        = trs.item(+)  --pack's tax rate
       and ils.loc         = trs.loc(+)
       and ils.loc_type    = trs.loc_type(+)
       and ps.pack_no      = p.pack_no
       and ps.item         = p.item
       and ps.loc          = ils.loc
       and ps.loc_type     = ils.loc_type)
--Main Query
--Switch item id with parent item:diff if the item is aggregated by diff.
--Aggregate indicators can only be defined at the top level. For a 3-level item, it would be at grandparent item.
--If item_aggregate_ind = 'Y', at least one diff's aggregate_ind must be 'Y'.
--If parent or grandparent item's item_aggregate_ind = 'Y', output should always be aggregated by the parent item:diff.
select eow_date,
       case when im2.item_aggregate_ind = 'N' then
                 im1.item_parent
            else
                 im1.item_parent||(case when(im2.diff_1_aggregate_ind='Y') then ':'|| im1.diff_1 else NULL end)||
                                  (case when(im2.diff_2_aggregate_ind='Y') then ':'|| im1.diff_2 else NULL end)||
                                  (case when(im2.diff_3_aggregate_ind='Y') then ':'|| im1.diff_3 else NULL end)||
                                  (case when(im2.diff_4_aggregate_ind='Y') then ':'|| im1.diff_4 else NULL end)
            end item_id,
       location,
       clearance_inventory_units,
       NVL2(lr.exchange_type, ROUND(clearance_inventory_cost/lr.exchange_rate,lr.currency_cost_dec), clearance_inventory_cost)clearance_inventory_cost,
       NVL2(lr.exchange_type, ROUND(clearance_inventory_retail/lr.exchange_rate, lr.currency_rtl_dec), clearance_inventory_retail)clearance_inventory_retail,
       regular_inventory_units,
       NVL2(lr.exchange_type, ROUND(regular_inventory_cost/lr.exchange_rate, lr.currency_cost_dec),regular_inventory_cost)regular_inventory_cost,
       NVL2(lr.exchange_type, ROUND(regular_inventory_retail/lr.exchange_rate, lr.currency_rtl_dec), regular_inventory_retail)regular_inventory_retail,
       receipt_units,
       NVL2(lr.exchange_type, ROUND(receipt_cost/lr.exchange_rate, lr.currency_cost_dec),receipt_cost)receipt_cost,
       NVL2(lr.exchange_type, ROUND(receipt_retail/lr.exchange_rate, lr.currency_rtl_dec),receipt_retail)receipt_retail,
       CASE when ls.output_currency != ls.currency_code and lr.exchange_rate is NULL then '${ERROR_CODE}'
       END error_desc
  from inventory_item_summary os,
       loc_exchange_rates lr,
       loc_summary ls,
       ${RMS_OWNER}.item_master im1, -- item or component of pack
       ${RMS_OWNER}.item_master im2  -- parent or grandparent item to check aggregate ind
 where os.item = im1.item
   and im1.item_parent is NOT NULL
   and NVL(im1.item_grandparent, im1.item_parent) = im2.item
   and lr.loc(+) = os.location
   and ls.loc = os.location
   and lr.current_rate_ind(+) = 'Y'
union all
select eow_date,
       os.item item_id,
       location,
       clearance_inventory_units,
       NVL2(lr.exchange_type, ROUND(clearance_inventory_cost/lr.exchange_rate,lr.currency_cost_dec), clearance_inventory_cost)clearance_inventory_cost,
       NVL2(lr.exchange_type, ROUND(clearance_inventory_retail/lr.exchange_rate, lr.currency_rtl_dec), clearance_inventory_retail)clearance_inventory_retail,
       regular_inventory_units,
       NVL2(lr.exchange_type, ROUND(regular_inventory_cost/lr.exchange_rate, lr.currency_cost_dec),regular_inventory_cost)regular_inventory_cost,
       NVL2(lr.exchange_type, ROUND(regular_inventory_retail/lr.exchange_rate, lr.currency_rtl_dec),regular_inventory_retail)regular_inventory_retail,
       receipt_units,
       NVL2(lr.exchange_type, ROUND(receipt_cost/lr.exchange_rate, lr.currency_cost_dec),receipt_cost)receipt_cost,
       NVL2(lr.exchange_type, ROUND(receipt_retail/lr.exchange_rate, lr.currency_rtl_dec),receipt_retail)receipt_retail,
       CASE when ls.output_currency != ls.currency_code and lr.exchange_rate is NULL then '${ERROR_CODE}'
       END error_desc
  from inventory_item_summary os,
       loc_exchange_rates lr,
       loc_summary ls,
       ${RMS_OWNER}.item_master im1 -- item on order or component of pack on order
 where os.item = im1.item
   and im1.item_parent is NULL
   and lr.loc(+) = os.location
   and ls.loc = os.location
   and lr.current_rate_ind(+) = 'Y'
)
group by eow_date,
         item_id,
         location,
         error_desc
           ]]>
         </PROPERTY>
         <OUTPUT name = "output.v"/>
      </OPERATOR>
      
      <OPERATOR type="filter">
      <INPUT    name="output.v"/>
      <PROPERTY name="filter" value="error_desc IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT   name="error_data.v"/>
      <OUTPUT   name="output_data_final.v"/>
      </OPERATOR>
   
      <OPERATOR type="export">
         <INPUT    name="output_data_final.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
      
      
     <OPERATOR type="export">
         <INPUT    name="error_data.v"/>
         <PROPERTY name="outputfile" value="${REJECT_MFP_INVENTORY_FILE}"/>
      </OPERATOR>
      
   </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"
###############################################################################

# Log the number of records written to the output file
log_num_recs ${OUTPUT_FILE}

#Check for the reject file and log an error if any records were rejected.
if [[ -s ${REJECT_MFP_INVENTORY_FILE} ]]
then
   log_num_recs ${REJECT_MFP_INVENTORY_FILE}
   message "**Records rejected by ${PROGRAM_NAME}"
   exit 1
else
   rm -f ${REJECT_MFP_INVENTORY_FILE}
   message "Program completed successfully"
fi


###############################################################################
# Remove the status file
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################
}


########################################################
#  INPUT DEFINES
#  This section must come after INCLUDES
########################################################
ERROR_CODE="Exchange rate does not exist"
opt=$2

if [ -z "$opt" ]; then
export CURR_PARAM="L"
fi 

case $opt in
   P)
     # Primary Currency -
     
     message "Primary currency selected for amounts"
     export CURR_PARAM="P"
     ;;
   L)
     # Local Currency -
     
     message "Local currency selected for amounts"
     export CURR_PARAM="L"
     ;;
     
   *)
    # Argument specified is NULL:
    # No argument or other argument specified:
    if [ "$opt" -ne "P" ] || [ "$opt" -ne "L" ] || ! [ -z "$opt" ]; then 
     message "Wrong input parameter passed"
     message "Argument must be either Primary(P) or Local(L) or Default(NULL)!" 
     message "Program terminated." 
     rmse_terminate 1
    fi ;;
esac  
    
case $1 in
  I)
    #  Full extract -

    message "Program started. Loading full extract ..."
    perform_extract_initial
    ;;

  W)
    #  Weekly extract -

    message "Program started. Loading weekly extracts ..."
    perform_extract
    ;;

  *)
    #  No argument or invalid argument specified:

    message "First argument must be either weekly(W) or initial(I)!" 
    message "Program terminated." 
    rmse_terminate 1
    ;;
esac


rmse_terminate 0
