#!/bin/ksh

########################################################
#  Script extracts supplier information from the 
#  RMS sups table                                      
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="rmse_rpas_suppliers"

########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF

   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query">
            <![CDATA[
               SELECT SUPPLIER,
                      SUP_NAME
               FROM ${RMS_OWNER}.SUPS
            ]]>
         </PROPERTY>
         <OUTPUT name = "output.v"/>
      </OPERATOR>
      <OPERATOR type="export">
         <INPUT    name="output.v"/>
         <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
         <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
      </OPERATOR>
   </FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f ${STATUS_FILE} ]]; then rm ${STATUS_FILE}; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
