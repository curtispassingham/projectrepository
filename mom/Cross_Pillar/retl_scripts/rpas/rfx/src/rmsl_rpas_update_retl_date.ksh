#!/bin/ksh

export PROGRAM_NAME="rmsl_rpas_update_retl_date"

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

if [[ $1 = "CLOSED_ORDER" ]]
then
  sqlplus /nolog << EOF
    conn  $SQLPLUS_LOGON;
    WHENEVER SQLERROR EXIT 4;
    UPDATE ${RMS_OWNER}.RETL_EXTRACT_DATES
       SET LAST_EXTR_CLOSED_POT_DATE = to_date(${VDATE},'yyyymmdd');
    COMMIT;
    exit;
EOF
elif [[ $1 = "RECEIVED_QTY" ]]
then
  sqlplus /nolog << EOF
    conn $SQLPLUS_LOGON;
    WHENEVER SQLERROR EXIT 4;
    UPDATE ${RMS_OWNER}.RETL_EXTRACT_DATES
       SET LAST_EXTR_RECEIVED_POT_DATE = to_date(${LAST_EXTR_RECEIVED_POT_DATE},'yyyymmdd');
    COMMIT;
    exit;
EOF
else
  checkerror -e 2 -m "**No Input parameter specified**. Usage: ${PROGRAM_NAME} CLOSED_ORDER | RECEIVED_QTY"
fi
checkerror -e $? -m "ERROR: update retl_extract_dates failed - check $ERR_FILE"
