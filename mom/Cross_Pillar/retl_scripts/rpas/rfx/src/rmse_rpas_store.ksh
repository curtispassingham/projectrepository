#!/bin/ksh

###################################################################
#  The rmse_rpas_store.ksh program extracts store information from RMS
#  and writes it into a flat file to be used by the RDF Transform
#  programs to build the files to be loaded into RDF.
###################################################################

################## PROGRAM DEFINE #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME="rmse_rpas_store"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=$DATA_DIR/$PROGRAM_NAME.dat

export OUTPUT_SCHEMA=$SCHEMA_DIR/$PROGRAM_NAME.schema

#############################################################
#  Copy the RETL flow to an xml file to be executed by rfx:
#############################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "$PROGRAM_NAME.flw">

   <!--  Read in the store data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT s.STORE,
                   s.STORE_NAME,
                   s.DISTRICT,
                   s.STORE_OPEN_DATE,
                   s.STORE_CLOSE_DATE,
                   s.STORE_CLASS,
                   cd.CODE_DESC STORE_CLASS_DESCRIPTION,
                   s.STORE_FORMAT,
                   sf.FORMAT_NAME,
                   s.channel_id,
                   cl.channel_name
              FROM ${RMS_OWNER}.STORE s,
                   ${RMS_OWNER}.STORE_FORMAT sf,
                   ${RMS_OWNER}.CODE_DETAIL cd,
                   ${RMS_OWNER}.CHANNELS cl
             WHERE s.STORE_FORMAT = sf.STORE_FORMAT(+)
               AND s.STORE_CLASS = cd.CODE
               AND s.STORE_TYPE in ('C','F')
               AND cd.CODE_TYPE = 'CSTR'
               AND (s.STORE_CLOSE_DATE >= TO_DATE('${VDATE}','YYYYMMDD') OR s.STORE_CLOSE_DATE is NULL)
               AND s.channel_id= cl.channel_id(+)
         ]]>
      </PROPERTY>
      
      <!--  Write out the store data to a flat file:  -->
      
      <OPERATOR type="export">
         <PROPERTY name="outputfile" value="$OUTPUT_FILE"/>
         <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA"/>
      </OPERATOR>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into rmse_rpas_store.xml:
###############################################

$RFX_EXE $RFX_OPTIONS  -f $RETL_FLOW_FILE

#######################################################
#  Do error checking on results of the RETL execution:
#######################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

#######################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#######################################################

if [ -f  $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Clean up and exit:

rmse_terminate 0
