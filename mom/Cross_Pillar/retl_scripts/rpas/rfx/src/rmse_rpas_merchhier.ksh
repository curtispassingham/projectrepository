#!/bin/ksh

########################################################
#  Script extracts merchandise hierarchy information 
#  from the RMS COMPHEAD, DIVIAION, GROUPS, DEPS, CLASS,
#  SUBCLASS tables
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME="rmse_rpas_merchhier"


########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh


########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD} 
         <PROPERTY name = "query">
            <![CDATA[
           SELECT sc.SUBCLASS,
	          sc.SUB_NAME,
                  sc.CLASS,
                  c.CLASS_NAME,
                  c.DEPT,
                  dp.DEPT_NAME,
                  dp.GROUP_NO,
                  g.GROUP_NAME,
                  g.DIVISION,
                  dv.DIV_NAME,
                  ch.COMPANY,
                  ch.CO_NAME     
               FROM ${RMS_OWNER}.SUBCLASS sc,
                  ${RMS_OWNER}.CLASS c,
                  ${RMS_OWNER}.DEPS dp,
                  ${RMS_OWNER}.GROUPS g,
                  ${RMS_OWNER}.DIVISION dv,
                  ${RMS_OWNER}.COMPHEAD ch
               WHERE sc.CLASS=c.CLASS
                  AND sc.DEPT=dp.DEPT
                  AND c.DEPT=dp.DEPT
                  AND dp.GROUP_NO=g.GROUP_NO
                  AND g.DIVISION=dv.DIVISION
            ]]>
         </PROPERTY>
         <OPERATOR type="export">
            <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
            <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
         </OPERATOR>
      </OPERATOR>
   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
