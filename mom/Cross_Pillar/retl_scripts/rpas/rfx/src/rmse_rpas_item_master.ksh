#!/bin/ksh

########################################################
# Script extracts item information from the RMS  
# item_master table  
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME='rmse_rpas_item_master'

########################################################
#  INCLUDES ########################
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

########################################################
#  OUTPUT DEFINES 
#  This section must come after INCLUDES
########################################################

export OUTPUT_FILE=$DATA_DIR/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=$SCHEMA_DIR/${PROGRAM_NAME}.schema

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

###############################################################################
#  Create a disk-based flow file
###############################################################################

FLOW_FILE="${LOG_DIR}/${PROGRAM_NAME}.xml"

cat > ${FLOW_FILE} << EOF
   <FLOW name = "${PROGRAM_NAME}.flw">
      ${DBREAD}
         <PROPERTY name = "query" >
	        <![CDATA[ 
               select im.item,
                      im.item_desc,
                      im.item_parent,
                      im.item_grandparent,
                      im.item_level,
                      im.tran_level,
                      im.subclass,
                      im.class,
                      im.dept,
                      im.forecast_ind,
                      isup.supplier,
                      d1.diff_1_type,
                      d1.diff_1,
                      d1.diff_desc_1,
                      d1.diff_file_position_1,
                      im_diff.diff_1_aggregate_ind,
                      d2.diff_2_type,
                      d2.diff_2,
                      d2.diff_desc_2,
                      d2.diff_file_position_2,
                      im_diff.diff_2_aggregate_ind,
                      d3.diff_3_type,
                      d3.diff_3,
                      d3.diff_desc_3,
                      d3.diff_file_position_3,
                      im_diff.diff_3_aggregate_ind,
                      d4.diff_4_type,
                      d4.diff_4,
                      d4.diff_desc_4,
                      d4.diff_file_position_4,
                      im_diff.diff_4_aggregate_ind
               from   ${RMS_OWNER}.item_master im,
                      ${RMS_OWNER}.item_master im_diff,
                      ${RMS_OWNER}.item_supplier isup,
                      (select d.diff_id diff_1,
                              m.rdf_diff_type_map diff_1_type,
                              d.diff_desc diff_desc_1,
                              m.file_position diff_file_position_1
                       from   ${RMS_OWNER}.diff_ids d,
                              ${RMS_OWNER}.if_rdf_diff_map m
                       where  d.diff_type = m.diff_type
                       union
                       select g.diff_group_id diff_1,
                              m.rdf_diff_type_map diff_1_type,
                              g.diff_group_desc diff_desc_1,
                              m.file_position diff_file_position_1
                        from ${RMS_OWNER}.diff_group_head g,
                             ${RMS_OWNER}.if_rdf_diff_map m
                       where g.diff_type = m.diff_type) d1,
                      (select d.diff_id diff_2,
                              m.rdf_diff_type_map diff_2_type,
                              d.diff_desc diff_desc_2,
                              m.file_position diff_file_position_2
                       from   ${RMS_OWNER}.diff_ids d,
                              ${RMS_OWNER}.if_rdf_diff_map m
                       where  d.diff_type = m.diff_type
                       union
                       select g.diff_group_id diff_2,
                              m.rdf_diff_type_map diff_2_type,
                              g.diff_group_desc diff_desc_2,
                              m.file_position diff_file_position_2
                        from ${RMS_OWNER}.diff_group_head g,
                             ${RMS_OWNER}.if_rdf_diff_map m
                       where g.diff_type = m.diff_type) d2,
                      (select d.diff_id diff_3,
                              m.rdf_diff_type_map diff_3_type,
                              d.diff_desc diff_desc_3,
                              m.file_position diff_file_position_3
                       from   ${RMS_OWNER}.diff_ids d,
                              ${RMS_OWNER}.if_rdf_diff_map m
                       where  d.diff_type = m.diff_type
                       union
                       select g.diff_group_id diff_3,
                              m.rdf_diff_type_map diff_3_type,
                              g.diff_group_desc diff_desc_3,
                              m.file_position diff_file_position_3
                        from ${RMS_OWNER}.diff_group_head g,
                             ${RMS_OWNER}.if_rdf_diff_map m
                       where g.diff_type = m.diff_type) d3,
                      (select d.diff_id diff_4,
                              m.rdf_diff_type_map diff_4_type,
                              d.diff_desc diff_desc_4,
                              m.file_position diff_file_position_4
                       from   ${RMS_OWNER}.diff_ids d,
                              ${RMS_OWNER}.if_rdf_diff_map m
                       where  d.diff_type = m.diff_type
                       union
                       select g.diff_group_id diff_4,
                              m.rdf_diff_type_map diff_4_type,
                              g.diff_group_desc diff_desc_4,
                              m.file_position diff_file_position_4
                        from ${RMS_OWNER}.diff_group_head g,
                             ${RMS_OWNER}.if_rdf_diff_map m
                      where g.diff_type = m.diff_type) d4
               where im.item = isup.item
                     and im_diff.item = NVL(im.item_grandparent, NVL(im.item_parent, im.item))
                     and isup.primary_supp_ind = 'Y'
                     and im.status = 'A'
                     and im.diff_1 = d1.diff_1(+)
                     and im.diff_2 = d2.diff_2(+)
                     and im.diff_3 = d3.diff_3(+)
                     and im.diff_4 = d4.diff_4(+)
            ]]>
         </PROPERTY>
         <OUTPUT   name = "output.v"/>
      </OPERATOR>

      <OPERATOR type="export">
         <INPUT    name="output.v"/>
         <PROPERTY name="outputfile" value="$OUTPUT_FILE"/>
         <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA"/>
      </OPERATOR>
   </FLOW>
EOF

###############################################################################
#  Execute the flow
###############################################################################

${RFX_EXE} ${RFX_OPTIONS} -f ${FLOW_FILE}

###############################################################################
#  Handle RETL errors
###############################################################################

checkerror -e $? -m "Program failed - check $ERR_FILE"

###############################################################################
# Remove the status file
###############################################################################

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
# cleanup and exit
###############################################################################

rmse_terminate 0
