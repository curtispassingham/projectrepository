#!/bin/ksh

########################################################
# Script extracts weekly sales data.                   #
########################################################

###############################################################################
#  PROGRAM DEFINES 
###############################################################################

PROGRAM_NAME='rmse_rpas_weekly_sales'

###############################################################################
#  INCLUDES
###############################################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh
. ${LIB_DIR}/rmsl_rpas_update_last_hist_exp_date.ksh

###############################################################################
#  OUTPUT DEFINES
###############################################################################

OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat
OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

###############################################################################
#  Set up the query based on domain level (dept, clss, or scls)
###############################################################################

case ${DOMAIN_LEVEL} in
    D) export QUERY="
       SELECT /*+ PARALLEL(ilh, ${NO_OF_CPUS}) */ 
             im.item,
             ils.loc,
             TO_CHAR(ilh.eow_date,'YYYYMMDD') eow_date,
             NVL(ilh.sales_issues,0) sales_issues,
             ilh.sales_type,
             ROWIDTOCHAR(ils.rowid) ROW_ID,
             dd.domain_id
        FROM ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.item_loc_hist ilh,
             ${RMS_OWNER}.domain_dept dd,
             ${RMS_OWNER}.period p
       WHERE im.forecast_ind = 'Y'
         AND ilh.item    = im.item
         AND ilh.loc      = ils.loc
         AND ilh.item     = ils.item
         AND im.dept = dd.dept
         AND (ilh.eow_date >= NVL(ils.last_hist_export_date + 7, ilh.eow_date)
               AND dd.load_sales_ind = 'N')
         AND ilh.eow_date <= TO_DATE(p.vdate)
      UNION ALL         
      SELECT /*+ PARALLEL(ilh, ${NO_OF_CPUS}) */ 
             im.item,
             ils.loc,
             TO_CHAR(ilh.eow_date,'YYYYMMDD') eow_date,
             NVL(ilh.sales_issues,0) sales_issues,
             ilh.sales_type,
             ROWIDTOCHAR(ils.rowid) ROW_ID,
             dd.domain_id
        FROM ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.item_loc_hist ilh,
             ${RMS_OWNER}.domain_dept dd,
             ${RMS_OWNER}.period p
       WHERE im.forecast_ind = 'Y'
         AND ilh.item    = im.item
         AND ilh.loc      = ils.loc
         AND ilh.item     = ils.item
         AND im.dept = dd.dept
         AND dd.load_sales_ind = 'Y'
         AND ilh.eow_date <= TO_DATE(p.vdate)
    ORDER BY 1,
             2
              ";;
    C) export QUERY="
      SELECT /*+ PARALLEL(ilh, ${NO_OF_CPUS}) */ 
             im.item,
             ils.loc,
             TO_CHAR(ilh.eow_date,'YYYYMMDD') eow_Date,
             NVL(ilh.sales_issues,0) sales_issues,
             ilh.sales_type,
             ROWIDTOCHAR(ils.rowid) ROW_ID,
	     dc.domain_id
        FROM ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.item_loc_hist ilh,
             ${RMS_OWNER}.domain_class dc,
             ${RMS_OWNER}.period p
       WHERE im.forecast_ind = 'Y'
         AND ilh.item    = im.item
         AND ilh.loc      = ils.loc
         AND ilh.item     = ils.item
         AND im.dept = dc.dept
         AND im.class = dc.class
         AND ((ilh.eow_date >= NVL(ils.last_hist_export_date + 7, ilh.eow_date)
               AND dc.load_sales_ind = 'N')
             OR (dc.load_sales_ind = 'Y'))
         AND ilh.eow_date <= TO_DATE(p.vdate)
    ORDER BY ilh.item,
             ilh.loc
		    ";;
    S) export QUERY="
      SELECT /*+ PARALLEL(ilh, ${NO_OF_CPUS}) */ 
             im.item,
             ils.loc,
             TO_CHAR(ilh.eow_date,'YYYYMMDD') eow_Date,
             NVL(ilh.sales_issues,0) sales_issues,
             ilh.sales_type,
             ROWIDTOCHAR(ils.rowid) ROW_ID,
	     ds.domain_id
        FROM ${RMS_OWNER}.item_master im,
             ${RMS_OWNER}.item_loc_soh ils,
             ${RMS_OWNER}.item_loc_hist ilh,
             ${RMS_OWNER}.domain_subclass ds,
             ${RMS_OWNER}.period p
       WHERE im.forecast_ind = 'Y'
         AND ilh.item    = im.item
         AND ilh.loc      = ils.loc
         AND ilh.item     = ils.item
         AND im.dept = ds.dept
         AND im.class = ds.class
         AND im.subclass = ds.subclass
         AND ((ilh.eow_date >= NVL(ils.last_hist_export_date + 7, ilh.eow_date)
               AND ds.load_sales_ind = 'N')
             OR (ds.load_sales_ind = 'Y'))
         AND ilh.eow_date <= TO_DATE(p.vdate)
    ORDER BY ilh.item,
             ilh.loc
		    ";;
    *) echo "Invalid Domain Level (${DOMAIN_LEVEL})!!";;
esac

###############################################################################
#  MAIN PROGRAM CONTENT
###############################################################################

###############################################################################
#  Friendly (necessary?) start message
###############################################################################

message "Program started ..."

# TODO: Add post-processing to remove special characters.

###############################################################################
#  Extract the weekly sales data from the RMS database and error check
###############################################################################

extract_with_schema ${PROGRAM_NAME} ${OUTPUT_SCHEMA} ${OUTPUT_FILE} ${QUERY}
checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
#  Update the last extracted records date and error check
###############################################################################

update_ils_last_hist
checkerror -e $? -m "Program failed - check ${ERR_FILE}"

###############################################################################
# Log the number of records written to the output file
###############################################################################

log_num_recs ${OUTPUT_FILE}

if [[ -s ${REJ_FILE} ]] ; then
   log_num_recs ${REJ_FILE}
   message "******  Records rejected by ${PROGRAM_NAME}"
   rmse_terminate 1
fi

###############################################################################
#  Remove status and reject files, if present
###############################################################################

if [[ -f ${STATUS_FILE} ]] ; then rm -f ${STATUS_FILE} ; fi
if [[ -f ${REJ_FILE} ]] ; then rm -f ${REJ_FILE} ; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
#  Cleanup and exit
###############################################################################

rmse_terminate 0

