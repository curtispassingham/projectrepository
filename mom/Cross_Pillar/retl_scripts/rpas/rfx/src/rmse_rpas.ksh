#!/bin/ksh

########################################################################
#
#  The rmse_rpas.ksh script runs all of the necessary data extraction
#  scripts (rmse_rpas*.ksh) needed to produce the files used by
#  subsequent transformation scripts per integrated application
#  (RDF etc.)
#
#  Most of these scripts are run in parallel (as background jobs)
# 
#  Usage: rmse_rpas.ksh  [-c]
#
#  -c: The presence of this option will cause FILE_DATE in
#      rmse_rpas_config.env to be set to the current date instead
#      of using VDATE.                           
#
#  Adapted substantially from rdft.ksh
#
########################################################################

echo
echo  WARNING: Be sure you have cleaned up all status files
echo  in the error directory before rerunning.

PROGRAM_NAME=rmse_rpas

#################################
#  Get the command line options:
#################################

c_option=""
if [ "$1" = -c ]; then c_option=-c; shift; fi

#############################################################################
#  Define the environment variables (the "-e" option is used because
#  only a few of the initial environment variables need to be defined 
#  and the redirection of all output must be avoided, so this option  
#  causes most of rmse_rpas_config.env to be skipped - the "$* is needed
#  to avoid losing any arguments that might have been passed to rdft.ksh):
#############################################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env -t -r $c_option $*

##########################################
#  Define the rmse_rpas_error_check function:
##########################################

. ${LIB_DIR}/rmse_rpas_error_check.ksh

#  Total number of programs to be executed in parallel
#  (Does not count pre_rmse_rpas.ksh)
NBR_OF_PROGRAMS=11  

###############################################################################
#  PLEASE NOTE - CUSTOMIZABLE FOR EACH CLIENT:  
#
#  Initialize the program name and enablement arrays.
#  To disable a script, simply set it's PROG_ENABLED[] value to "False", 
#  DO NOT REMOVE THE LINE, CHANGE PROG[] VALUES OR ADJUST THE INDICES!!!
###############################################################################

 PROG[1]="rmse_rpas_daily_sales      " ; PROG_ENABLED[1]=True
 PROG[2]="rmse_rpas_domain           " ; PROG_ENABLED[2]=True
 PROG[3]="rmse_rpas_item_master      " ; PROG_ENABLED[3]=True
 PROG[4]="rmse_rpas_merchhier        " ; PROG_ENABLED[4]=True
 PROG[5]="rmse_rpas_orghier          " ; PROG_ENABLED[5]=True
 PROG[6]="rmse_rpas_stock_on_hand    " ; PROG_ENABLED[6]=True
 PROG[7]="rmse_rpas_store            " ; PROG_ENABLED[7]=True
 PROG[8]="rmse_rpas_suppliers        " ; PROG_ENABLED[8]=True
 PROG[9]="rmse_rpas_weekly_sales     " ; PROG_ENABLED[9]=True
 PROG[10]="rmse_rpas_wh               " ; PROG_ENABLED[10]=True
 PROG[11]="rmse_rpas_attributes       "

###############################################################################
#  NOT CUSTOMIZABLE BY CLIENT. 
#  Enablement of rmse_rpas_attributes is controlled by attribute-active 
#  configuration flags.
###############################################################################

if [ ${PROD_ATTRIBUTES_ACTIVE} = True ] || [ ${LOC_ATTRIBUTES_ACTIVE} = True ] ; then
  PROG_ENABLED[11]=True
else
  PROG_ENABLED[11]=False
fi

###############################################################################
#  Run pre_rmse_rpas.ksh to completion before the parallelizable scripts
###############################################################################

$SRC_DIR/pre_rmse_rpas.ksh  $c_option

exit_status=$?
error_check  pre_rmse_rpas  $exit_status
err_typ=$? 
if [ ${err_typ} -ne 0 ]; then
  echo
  echo "*******  ERROR in pre-rmse_rpas.ksh - processing halted.  ******"
  echo
  echo " Exit status code: ${exit_status}"
  echo " Error code:  ${err_typ} -- ${ERR_DESC[err_typ]}"
  echo
  exit ${err_typ}
else
  echo " pre_rmse_rpas completed successfully."
fi

###############################################################################
#  Run all parallelizable extraction scripts in parallel:
###############################################################################

k=1
while [ ${k} -le ${NBR_OF_PROGRAMS} ] ; do
  if [ ${PROG_ENABLED[k]} = True ] ; then
    prog_name=$(echo ${PROG[k]} | nawk '{print $1 ".ksh"}')
    (${SRC_DIR}/${prog_name} 
    exit_status=$?
    error_check  ${PROG[k]}  ${exit_status} ) &
  fi
  let k=k+1
done  

###############################################################################
#  Wait until all background programs have finished:
###############################################################################

wait  

echo
echo " **********************************************************"
echo " **   All RETL extract programs have completed.         **"
echo " **                                                      **"
echo " **********************************************************"

######################################################
#  Now display the run summary on the screen:
######################################################

echo
echo " Program Completion Status:"
echo " =========================="
echo
echo "     Program             Error Type    Description"
echo "     -------             ----------    -----------"

#  Display pre_rmse_rpas.ksh summary

nbr_failed=0
err_typ=$(nawk -v prog=pre_rmse_rpas '$1 == prog {
        print $2; exit}'  ${ERR_CODE_FILE})
err_desc=$(nawk -v prog=pre_rmse_rpas '$1 == prog {
         locd=index($0, "Desc=")
         if(locd > 0)  print substr($0, locd+5)
         else  print "Misc. error"
         exit}'  ${ERR_CODE_FILE})
if [ "${err_typ}" = "" ]; then  err_typ=9; fi
echo " 0. pre_rmse_rpas                ${err_typ}    ${err_desc}"
if [ ${err_typ} -gt 0 ]; then let nbr_failed=nbr_failed+1; fi
echo

#  Display parallelized script summaries

k=1
while [ ${k} -le ${NBR_OF_PROGRAMS} ]; do
  if [ ${PROG_ENABLED[k]} = True ] ; then
    err_typ=$(nawk -v prog=${PROG[k]} '$1 == prog {
            print $2; exit}'  ${ERR_CODE_FILE})
    err_desc=$(nawk -v prog=${PROG[k]} '$1 == prog {
             locd=index($0, "Desc=")
             if(locd > 0)  print substr($0, locd+5)
             else  print "Misc. error"
             exit}'  ${ERR_CODE_FILE})
    if [ "${err_typ}" = "" ]; then  err_typ=9; fi
    echo " ${k}. ${PROG[k]}  ${err_typ}    ${err_desc}"
    if [ ${err_typ} -gt 0 ]; then let nbr_failed=nbr_failed+1; fi
  else
    echo " ${k}. ${PROG[k]}       DISABLED"
  fi
  let k=k+1
  echo
done

###########################################
#  Display the final completion messages:
###########################################

if [ ${nbr_failed} -gt 0 ]; then
  echo
  echo "***********************************************************************"
  echo 
  echo "***  ONE OR MORE ERRORS WERE ENCOUNTERED !!!  ***"
  echo 
  echo "***  Number of RMS Extract Programs that FAILED: ${nbr_failed}"
  echo 
  echo " Check the log file (${LOG}) in ${LOG_DIR}"
  echo " and the error files in ${ERR_DIR}"
  echo
  echo "***********************************************************************"
  echo
  echo "Completed the ${PROGRAM_NAME}.ksh script WITH ERRORS on `date`."
  echo
  echo  "WARNING: Be sure you have cleaned up all status files"
  echo  "         in the error directory before re-running ${PROGRAM_NAME}.ksh."
else
  if [ -f ${STATUS_FILE} ]; then  rm ${STATUS_FILE}; fi
  echo
  echo "Completed the ${PROGRAM_NAME}.ksh script with NO errors on `date`."
fi
echo

exit ${nbr_failed}
