#!/bin/ksh

########################################################
# Script extracts attributes information from the      #
# RMS uda tables                                       #
#                                                      #
# This script provides a framework of UDA extract.     #
# Each client will have to customerize it to refect    #
# the UDA ids associated with the desired attributes   #
# (e.g. season, brand, ethnic, etc.) 
########################################################


################## PROGRAM DEFINES #####################
########## (must be the first set of defines) ##########

export PROGRAM_NAME='rmse_rpas_attributes'


####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINES) ####

. $RMS_RPAS_HOME/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh


##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export UDA_IN_FILE=$ETC_DIR/uda.txt
export OUTPUT_FILE=$DATA_DIR/${PROGRAM_NAME}.dat
export OUTPUT_SCHEMA=$SCHEMA_DIR/${PROGRAM_NAME}.schema

MAX_UDA_CNT=4
lp_cnt=0

RETL_FLOW_FILE=$LOG_DIR/${PROGRAM_NAME}.xml

message "Program started ..."

for UDA_ID_IN in `cat ${UDA_IN_FILE}` ; do
  lp_cnt=`expr ${lp_cnt} + 1`
  eval export "UDA_ID_${lp_cnt}=${UDA_ID_IN}"
  if [ ${lp_cnt} -gt ${MAX_UDA_CNT} ] ; then
    message "Only ${MAX_UDA_CNT} UDAs are allowed in ${UDA_IN_FILE}"
    break
  fi
done

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

    ${DBREAD}
      <PROPERTY name = "query" >
        <![CDATA[
          select im.item,
                 co.company,
                 co.co_name
           from ${RMS_OWNER}.item_master im,
                ${RMS_OWNER}.item_supplier isup,
                ${RMS_OWNER}.comphead co
          where im.item = isup.item
            and isup.primary_supp_ind = 'Y'
            and im.forecast_ind = 'Y'
        ]]>
      </PROPERTY>
      <OUTPUT   name = "items.v"/>
    </OPERATOR>
    
    ${DBREAD}
      <PROPERTY name = "query" >
        <![CDATA[
          select UI.item, UI.uda_value, RTRIM(SUBSTRB(UV.uda_value_desc,1,250)) uvd
          from ${RMS_OWNER}.uda_item_lov UI, 
               ${RMS_OWNER}.UDA U, 
               ${RMS_OWNER}.UDA_VALUES UV, 
               ${RMS_OWNER}.item_master I
          where U.uda_id=${UDA_ID_1} and I.forecast_ind='Y' and UI.item=I.item and
            U.uda_id=UI.uda_id and UI.uda_id=UV.UDA_ID and UV.uda_value=UI.uda_value
        ]]>
      </PROPERTY>
      <OUTPUT name = "seasonal.v"/>
    </OPERATOR>
    
    <OPERATOR type="fieldmod">
      <PROPERTY name="rename" value="UDA_VALUE_1=UDA_VALUE" />
      <PROPERTY name="rename" value="UDA_VALUE_DESC_1=uvd" />
      <INPUT  name = "seasonal.v"/>
      <OUTPUT name = "renamedseasonal.v"/>
    </OPERATOR>
    
    ${DBREAD}
      <PROPERTY name = "query" >
        <![CDATA[
          select UI.item, UI.uda_value, RTRIM(SUBSTRB(UV.uda_value_desc,1,250)) uvd
          from ${RMS_OWNER}.uda_item_lov UI, 
               ${RMS_OWNER}.UDA U, 
               ${RMS_OWNER}.UDA_VALUES UV, 
               ${RMS_OWNER}.item_master I
          where U.uda_id=${UDA_ID_2} and I.forecast_ind='Y' and UI.item=I.item and
            U.uda_id=UI.uda_id and UI.uda_id=UV.UDA_ID and UV.uda_value=UI.uda_value
        ]]>
      </PROPERTY>
      <OUTPUT name = "ethnic.v"/>
    </OPERATOR>
    
    <OPERATOR type="fieldmod">
      <PROPERTY name="rename" value="UDA_VALUE_2=UDA_VALUE" />
      <PROPERTY name="rename" value="UDA_VALUE_DESC_2=uvd" />
      <INPUT  name = "ethnic.v"/>
      <OUTPUT name = "renamedethnic.v"/>
    </OPERATOR>
    
    ${DBREAD}
      <PROPERTY name = "query" >
        <![CDATA[
          select UI.item, UI.uda_value, RTRIM(SUBSTRB(UV.uda_value_desc,1,250)) uvd
          from ${RMS_OWNER}.uda_item_lov UI, 
               ${RMS_OWNER}.UDA U, 
               ${RMS_OWNER}.UDA_VALUES UV, 
               ${RMS_OWNER}.item_master I
          where U.uda_id=${UDA_ID_3} and I.forecast_ind='Y' and UI.item=I.item and
            U.uda_id=UI.uda_id and UI.uda_id=UV.UDA_ID and UV.uda_value=UI.uda_value
        ]]>
      </PROPERTY>
      <OUTPUT name = "brand.v"/>
    </OPERATOR>
    
    <OPERATOR type="fieldmod">
      <PROPERTY name="rename" value="UDA_VALUE_3=UDA_VALUE" />
      <PROPERTY name="rename" value="UDA_VALUE_DESC_3=uvd" />
      <INPUT  name = "brand.v"/>
      <OUTPUT name = "renamedbrand.v"/>
    </OPERATOR>
    
    ${DBREAD}
      <PROPERTY name = "query" >
        <![CDATA[
          select UI.item, UI.uda_value, RTRIM(SUBSTRB(UV.uda_value_desc,1,250)) uvd
          from ${RMS_OWNER}.uda_item_lov UI, 
               ${RMS_OWNER}.UDA U, 
               ${RMS_OWNER}.UDA_VALUES UV, 
               ${RMS_OWNER}.item_master I
          where U.uda_id=${UDA_ID_4} and I.forecast_ind='Y' and UI.item=I.item and
            U.uda_id=UI.uda_id and UI.uda_id=UV.UDA_ID and UV.uda_value=UI.uda_value
        ]]>
      </PROPERTY>
      <OUTPUT name = "geographic.v"/>
    </OPERATOR>
    
    <OPERATOR type="fieldmod">
      <PROPERTY name="rename" value="UDA_VALUE_4=UDA_VALUE" />
      <PROPERTY name="rename" value="UDA_VALUE_DESC_4=uvd" />
      <INPUT  name = "geographic.v"/>
      <OUTPUT name = "renamedgeographic.v"/>
    </OPERATOR>
    
    <OPERATOR type="sort">
      <PROPERTY name="key" value="ITEM" />
      <INPUT    name="items.v"/>
      <OUTPUT   name="sorteditems.v"/>
    </OPERATOR>
    
    <OPERATOR type="sort">
      <PROPERTY name="key" value="ITEM" />
      <PROPERTY name="removedup" value="true" />
      <INPUT    name="renamedseasonal.v"/>
      <OUTPUT   name="sortedseasonal.v"/>
    </OPERATOR>
    
    <OPERATOR type="sort">
      <PROPERTY name="key" value="ITEM" />
      <PROPERTY name="removedup" value="true" />
      <INPUT    name="renamedethnic.v"/>
      <OUTPUT   name="sortedethnic.v"/>
    </OPERATOR>
    
    <OPERATOR type="sort">
      <PROPERTY name="key" value="ITEM" />
      <PROPERTY name="removedup" value="true" />
      <INPUT    name="renamedbrand.v"/>
      <OUTPUT   name="sortedbrand.v"/>
    </OPERATOR>
    
    <OPERATOR type="sort">
      <PROPERTY name="key" value="ITEM" />
      <PROPERTY name="removedup" value="true" />
      <INPUT    name="renamedgeographic.v"/>
      <OUTPUT   name="sortedgeographic.v"/>
    </OPERATOR>
    
    <OPERATOR type="leftouterjoin" name="leftouterjoin,0" >
      <PROPERTY name="key" value="ITEM" />
      <INPUT    name="sorteditems.v"/>
      <INPUT    name="sortedseasonal.v"/>
      <OUTPUT   name="output1.v"/>
    </OPERATOR>

    <OPERATOR type="leftouterjoin" name="leftouterjoin,0" >
      <PROPERTY name="key" value="ITEM" />
      <INPUT    name="output1.v"/>
      <INPUT    name="sortedethnic.v"/>
      <OUTPUT   name="output2.v"/>
    </OPERATOR>

    <OPERATOR type="leftouterjoin" name="leftouterjoin,0" >
      <PROPERTY name="key" value="ITEM" />
      <INPUT    name="output2.v"/>
      <INPUT    name="sortedbrand.v"/>
      <OUTPUT   name="output3.v"/>
    </OPERATOR>

    <OPERATOR type="leftouterjoin" name="leftouterjoin,0" >
      <PROPERTY name="key" value="ITEM" />
      <INPUT    name="output3.v"/>
      <INPUT    name="sortedgeographic.v"/>
      <OUTPUT   name="output.v"/>
    </OPERATOR>

   <OPERATOR type="export">
      <INPUT    name="output.v"/>
      <PROPERTY name="outputfile" value="$OUTPUT_FILE"/>
      <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA"/>
   </OPERATOR>

</FLOW>

EOF

${RFX_EXE} ${RFX_OPTIONS} -f ${RETL_FLOW_FILE}

checkerror -e $? -m "Program failed - check $ERR_FILE"

# Remove the status file
if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully"

# cleanup and exit
rmse_terminate 0
