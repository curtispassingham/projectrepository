#!/bin/ksh

############################################################
#
#  Program name: rmse_rpas_wh.ksh
#
#  Purpose: rmse_rpas_wh.ksh extracts RMS warehouse data and 
#           produces one data file for RDF 
#
############################################################

################## PROGRAM DEFINE #####################
##########    (must be the first define)     ##########

export PROGRAM_NAME="rmse_rpas_wh"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####
. $RMS_RPAS_HOME/rfx/etc/rmse_rpas_config.env
. $LIB_DIR/rmse_rpas_lib.ksh

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=$DATA_DIR/$PROGRAM_NAME.dat

export OUTPUT_SCHEMA=$SCHEMA_DIR/$PROGRAM_NAME.schema

message "Program started ..."

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "$PROGRAM_NAME.flw">

  <!-- Obtain the Warehouse data from the RMS database table (WH): -->

  ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT  w.WH,
                    w.WH_NAME,
                    w.FORECAST_WH_IND,
                    w.STOCKHOLDING_IND,
                    w.CHANNEL_ID, 
                    ch.CHANNEL_NAME
                    
                    -- , VWH_TYPE WH_TYPE
              FROM  ${RMS_OWNER}.WH w, 
                    ${RMS_OWNER}.CHANNELS ch
              WHERE w.STOCKHOLDING_IND = 'Y'
              and w.channel_id=ch.channel_id(+) 
         ]]>
      </PROPERTY>
         <OUTPUT name="wh_data.v"/>
   </OPERATOR>
   
<!--  Write out the Warehouse Data File:  -->

   <OPERATOR type="export">
      <INPUT    name="wh_data.v"/>
      <PROPERTY name="outputfile" value="$OUTPUT_FILE"/>
      <PROPERTY name="schemafile" value="$OUTPUT_SCHEMA"/>
   </OPERATOR>

</FLOW>

EOF

##############################################
#  Execute the RETL flow that was 
#  previously copied into rmse_rpas_wh.xml:
##############################################

$RFX_EXE  $RFX_OPTIONS  -f $RETL_FLOW_FILE
exit_stat=$?

message "Program completed. Exit status code: $exit_stat"

#######################################################
#  Do error checking on results of the RETL execution:
#######################################################

checkerror -e $exit_stat -m "Program failed - check $ERR_FILE"

#######################################################
#  Remove the status file, log the completion message
#  to the log and error files and clean up the files:
#######################################################

if [ -f $STATUS_FILE ]; then rm $STATUS_FILE; fi

message "Program completed successfully"

#  Cleanup and exit:

rmse_terminate 0
