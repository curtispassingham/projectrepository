#!/bin/ksh

##########################################################################
#
#  The rmse_rpas_orghier.ksh program extracts organizational heirarchy data  
#  from the COMPHEAD, CHAIN, AREA, REGION and DISTRICT RMS tables and 
#  places this data into a flat file (rmse_rpas_orghier.dat) to be accessed
#  by RDF data transformation programs to prepare files to be loaded
#  into the RDF tables.
#
##########################################################################

################## PROGRAM DEFINE #####################
#########     (must be the first define)     ##########

export PROGRAM_NAME="rmse_rpas_orghier"

####################### INCLUDES ########################
##### (this section must come after PROGRAM DEFINE) ####

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

####################################################
#  Log start message and define the RETL flow file
####################################################

message "Program started ..."

RETL_FLOW_FILE=$LOG_DIR/$PROGRAM_NAME.xml

##################  OUTPUT DEFINES ######################
######## (this section must come after INCLUDES) ########

export OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}.dat

export OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}.schema

########################################################
#  Copy the RETL flow to a file to be executed by rfx:
########################################################

cat > $RETL_FLOW_FILE << EOF

<FLOW name = "${PROGRAM_NAME}.flw">

<!--  Read in the organizational heirarchy data from the RMS tables:  -->

   ${DBREAD}
      <PROPERTY name = "query">
         <![CDATA[
            SELECT ch.COMPANY,
                   ch.CO_NAME,
                   c.CHAIN,
                   c.CHAIN_NAME,
                   a.AREA,
                   a.AREA_NAME,
                   r.REGION,
                   r.REGION_NAME,
                   d.DISTRICT,
                   d.DISTRICT_NAME
              FROM ${RMS_OWNER}.COMPHEAD ch,
                   ${RMS_OWNER}.CHAIN c,
                   ${RMS_OWNER}.AREA a,
                   ${RMS_OWNER}.REGION r,
                   ${RMS_OWNER}.DISTRICT d
             WHERE c.CHAIN = a.CHAIN (+)
               AND a.AREA = r.AREA (+)
               AND r.REGION = d.REGION (+)
         ]]>
      </PROPERTY>
      <OUTPUT   name = "org_data.v"/>
   </OPERATOR>
   
   <!--  Write out the organizational heirarchy data to a flat file:  -->

   <OPERATOR type="export">
      <INPUT    name="org_data.v"/>
      <PROPERTY name="outputfile" value="${OUTPUT_FILE}"/>
      <PROPERTY name="schemafile" value="${OUTPUT_SCHEMA}"/>
   </OPERATOR>

</FLOW>

EOF

###############################################
#  Execute the RETL flow that had previously
#  been copied into rmse_rpas_orghier.xml:
###############################################

${RFX_EXE} ${RFX_OPTIONS} -f $RETL_FLOW_FILE

#######################################
#  Do error checking:
#######################################

checkerror -e $? -m "RETL flow failed - check $ERR_FILE"

#  Remove the status file:

if [[ -f $STATUS_FILE ]]; then rm $STATUS_FILE; fi

message "Program completed successfully."

#  Cleanup and exit:

rmse_terminate 0
