#!/bin/ksh

########################################################
#  Script extracts stock on hand either at store or at
#  both store and warehouse level, depending on the
#  value of the configuration parameter ISSUES_ACTIVE:
#  - ISSUES_ACTIVE = "True"
#      ==> stores extracted to rmse_rpas_stock_on_hand_sales.dat
#      ==> issues extracted to rmse_rpas_stock_on_hand_issues.dat
#  - ISSUES_ACTIVE != "True"
#      ==> stores extracted to rmse_rpas_stock_on_hand_sales.dat
########################################################

########################################################
#  PROGRAM DEFINES
#  These must be the first set of defines
########################################################

export PROGRAM_NAME='rmse_rpas_stock_on_hand'

########################################################
#  INCLUDES
#  This section must come after PROGRAM DEFINES
########################################################

. ${RMS_RPAS_HOME}/rfx/etc/rmse_rpas_config.env
. ${LIB_DIR}/rmse_rpas_lib.ksh

###############################################################################
#  Function perform_extract
#  This function performs the complete database extract, writes the output
#  .dat file, and handles any errors. Functionality is located in this function
#  so that it can be called iteratively from main script logic (see bottom of
#  this script) for separate cases of stores and issues extract.
###############################################################################

perform_extract ()
{
   ###############################################################################
   #  Override the dat and schema files for sales or issues export
   ###############################################################################

   OUTPUT_FILE=${DATA_DIR}/${PROGRAM_NAME}_${EXTRACT_TYPE}.dat
   OUTPUT_SCHEMA=${SCHEMA_DIR}/${PROGRAM_NAME}_${EXTRACT_TYPE}.schema

   ###############################################################################
   #  Query information message
   ###############################################################################

   message "perform_extract - EXTRACT_TYPE=${EXTRACT_TYPE}.  LOC_TYPE=${LOC_TYPE}"

   ###############################################################################
   #  Set up the database query based on domain level (dept, clss, or scls)
   ###############################################################################

   case ${DOMAIN_LEVEL} in

      D) export QUERY="
            SELECT ils.item,
                   ils.loc,
                   stock_on_hand
            FROM   ${RMS_OWNER}.item_loc_soh ils,
                   ${RMS_OWNER}.item_master im,
                   ${RMS_OWNER}.domain_dept dd
                   ${JOIN_TABLE}
            WHERE  stock_on_hand <= 0
              AND  loc_type = '${LOC_TYPE}'
              AND  forecast_ind = 'Y'
              AND  im.status = 'A'
              AND  ils.item = im.item
              AND  dd.dept = im.dept
              ${WHERE_CLAUSE}
         ";;

      C) export QUERY="
            SELECT ils.item,
                   ils.loc,
                   stock_on_hand
            FROM   ${RMS_OWNER}.item_loc_soh ils,
                   ${RMS_OWNER}.item_master im,
                   ${RMS_OWNER}.domain_class dc
                   ${JOIN_TABLE}
            WHERE  stock_on_hand <= 0
              AND  loc_type = '${LOC_TYPE}'
              AND  forecast_ind = 'Y'
              AND  im.status = 'A'
              AND  ils.item = im.item
              AND  dc.dept = im.dept
              AND  dc.class = im.class
              ${WHERE_CLAUSE}
         ";;

      S) export QUERY="
            SELECT ils.item,
                   ils.loc,
                   stock_on_hand
            FROM   ${RMS_OWNER}.item_loc_soh ils,
                   ${RMS_OWNER}.item_master im,
                   ${RMS_OWNER}.domain_subclass ds
                   ${JOIN_TABLE}
            WHERE  stock_on_hand <= 0
              AND  loc_type = '${LOC_TYPE}'
              AND  forecast_ind = 'Y'
              AND  im.status = 'A'
              AND  ils.item = im.item
              AND  ds.dept = im.dept
              AND  ds.class = im.class
              AND  ds.subclass = im.subclass
              ${WHERE_CLAUSE}
         ";;

      *) message "Invalid Domain Level ($DOMAIN_LEVEL)!!";;

   esac

   #  TODO: Dynamically query the DB to get the domain level (above).
   #  TODO: Add post-processing to remove special characters.

   ###############################################################################
   #  Perform query
   ###############################################################################

   extract_with_schema ${PROGRAM_NAME}_${EXTRACT_TYPE} ${OUTPUT_SCHEMA} ${OUTPUT_FILE} ${QUERY}

   ###############################################################################
   #  Handle extract errors
   ###############################################################################

   checkerror -e $? -m "${EXTRACT_TYPE} Extract failed - check ${ERR_FILE}"

   ###############################################################################
   #  Check for the reject file and log an error if any records were rejected.
   ###############################################################################

   if [[ -s $REJ_FILE ]] ; then
      log_num_recs ${REJ_FILE}
      message "**Records rejected by ${PROGRAM_NAME} while writing ${OUTPUT_FILE}"
      exit 1
   fi

   ###############################################################################
   #  Remove the reject file, if present
   ###############################################################################

   if [[ -f ${REJ_FILE} ]] ; then rm -f ${REJ_FILE} ; fi

   ###############################################################################
   #  Report success!
   ###############################################################################

   message "Creation of ${OUTPUT_FILE} completed succesfully"
}

###############################################################################
#  BEGIN MAIN SCRIPT PROCESSING
#  Always extract the sales. Extract issues only if ISSUES_ACTIVE configuration
#  parameter is set to "True"
###############################################################################

###############################################################################
#  Friendly start message
###############################################################################

message "Program started. ISSUES_ACTIVE: ${ISSUES_ACTIVE}.   DOMAIN_LEVEL: ${DOMAIN_LEVEL}"

###############################################################################
#  Configure and run the sales extract
###############################################################################

EXTRACT_TYPE="sales"
LOC_TYPE="S"
JOIN_TABLE=",${RMS_OWNER}.store s"
WHERE_CLAUSE="AND s.store=ils.loc
              AND s.store_type in ('C','F')"
perform_extract

###############################################################################
#  Conditionally configure and run the issues extract
###############################################################################

if [[ ${ISSUES_ACTIVE} = "True" ]] ; then
   EXTRACT_TYPE="issues"
   LOC_TYPE="W"
   JOIN_TABLE=""
   WHERE_CLAUSE=""
   perform_extract
fi

###############################################################################
#  Remove the status file
###############################################################################

if [[ -f ${STATUS_FILE} ]] ; then rm -f ${STATUS_FILE} ; fi

###############################################################################
#  Report success!
###############################################################################

message "Program completed successfully"

###############################################################################
#  Cleanup and exit
###############################################################################

rmse_terminate 0

