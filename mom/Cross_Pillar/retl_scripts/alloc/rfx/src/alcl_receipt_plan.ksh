#!/bin/ksh

########################################################
# Copyright (c) 2005, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/src/alcl_receipt_plan.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

##########################################################################
#
#  This Allocation script loads/updates/inserts receipt data into
#  the alc_receipt_plan table from a receipt_XX.dat file where XX is the
#  Domain ID (00-99).
#
#  Usage: alcl_receipt_plan.ksh  receipt_data_input_file  [thread_number]
#
#  Author: D. W. Maas
#
##########################################################################

if [ $# -eq 0 ]; then
  echo
  echo " ERROR - You must enter a receipt data input file name when running this"
  echo "         program. The thread number is optional."
  echo
  echo " Usage: alcl_receipt_plan.ksh  receipt_data_input_file  [thread_number]"
  echo
  echo " The default value for thread_number is 0."
  echo
  exit
fi
  
####################  SCRIPT SETUP  ######################

export PROGRAM_NAME='alcl_receipt_plan'

. $ALCHOME/rfx/etc/alc_config.env

. ${LIB_DIR}/alc_lib.ksh

################  DEFINITION OF CONSTANTS  ##################

export INPUT_FILE=$1

INPUT_SCHEMA=$PROGRAM_NAME.schema

if [ "$2" != "" ]; then THREAD=$2
else  THREAD=0
fi

export THREAD

export TARGET_TABLE="alc_receipt_plan"
export TEMP_TABLE_1="${PROGRAM_NAME}_TEMP_1_${THREAD}"
export TEMP_TABLE_2="${PROGRAM_NAME}_TEMP_2_${THREAD}"
export TEMP_TABLE_3="${PROGRAM_NAME}_TEMP_3_${THREAD}"

#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_1 table later on:

export CREATE_TABLE_1_SQL="
CREATE TABLE ${TEMP_TABLE_1}
(
  DEPT             VARCHAR2(4),
  CLASS            VARCHAR2(4),
  SUBCLASS         VARCHAR2(4),
  ITEM_ID          VARCHAR2(25),
  LOC              VARCHAR2(20)  NOT NULL,
  DIFF_TYPE_1      VARCHAR2(1),
  DIFF1_ID         VARCHAR2(10),
  DIFF_TYPE_2      VARCHAR2(1),
  DIFF2_ID         VARCHAR2(10),
  DIFF_TYPE_3      VARCHAR2(1),
  DIFF3_ID         VARCHAR2(10),
  DIFF_TYPE_4      VARCHAR2(1),
  DIFF4_ID         VARCHAR2(10),
  EOW_DATE         DATE          NOT NULL,
  QTY              NUMBER(12,4)  NOT NULL
) NOLOGGING
"
#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_2 table later on:

export CREATE_TABLE_2_SQL="
CREATE TABLE ${TEMP_TABLE_2}
(
  LOC              VARCHAR2(40 BYTE)   NOT NULL,
  DEPT             VARCHAR2(40 BYTE),
  CLASS            VARCHAR2(40 BYTE),
  SUBCLASS         VARCHAR2(40 BYTE),
  ITEM_ID          VARCHAR2(40 BYTE),
  DIFF1_ID         VARCHAR2(40 BYTE),
  DIFF2_ID         VARCHAR2(40 BYTE),
  DIFF3_ID         VARCHAR2(40 BYTE),
  DIFF4_ID         VARCHAR2(40 BYTE),
  EOW_DATE         DATE                NOT NULL,
  QTY              NUMBER(12,4)        NOT NULL
) NOLOGGING
"

#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_3 table later on:

export CREATE_TABLE_3_SQL="
CREATE TABLE ${TEMP_TABLE_3}
(
  LOC              VARCHAR2(40 BYTE)   NOT NULL,
  DEPT             VARCHAR2(40 BYTE),
  CLASS            VARCHAR2(40 BYTE),
  SUBCLASS         VARCHAR2(40 BYTE),
  ITEM_ID          VARCHAR2(40 BYTE),
  DIFF1_ID         VARCHAR2(40 BYTE),
  DIFF2_ID         VARCHAR2(40 BYTE),
  DIFF3_ID         VARCHAR2(40 BYTE),
  DIFF4_ID         VARCHAR2(40 BYTE),
  EOW_DATE         DATE                NOT NULL,
  QTY              NUMBER(12,4)        NOT NULL
) NOLOGGING
"


export INDEX_KEYS="LOC,DEPT,CLASS,SUBCLASS,ITEM_ID,DIFF1_ID,DIFF2_ID,DIFF3_ID,DIFF4_ID"

#################### MAIN PROGRAM ######################

message "Program started for thread ${THREAD}..."

#  Drop the temp tables, but don't error if they do not exist:

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_1}" 0

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_2}" 0

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_3}" 0

#  Create the temp tables:

query_db "${DBNAME}" "${CREATE_TABLE_1_SQL}"

query_db "${DBNAME}" "${CREATE_TABLE_2_SQL}"

query_db "${DBNAME}" "${CREATE_TABLE_3_SQL}"

#  Commented out for performance reasons:
## log_num_recs ${INPUT_FILE}

##############################################################
#  Copy the first RETL flow to a file for later execution:
##############################################################

cat > $LOG_DIR/${PROGRAM_NAME}_A_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_A.flw">

   <!--  *****************************************************  -->
   <!--  **********  Load in the receipt file          **********  -->
   <!--  *****************************************************  -->
   
   <OPERATOR  type="import">
      <PROPERTY  name="inputfile" value="${DATA_DIR}/${INPUT_FILE}"/>
      <PROPERTY  name="schemafile" value="${SCHEMA_DIR}/$INPUT_SCHEMA"/>
      <OUTPUT name="receipt_data.v"/>
   </OPERATOR>
   
   <!--  Place all records with no item number or no DIFFn_ID values
         in one dataset and those with an item number and one or 
         more DIFFn_ID values in a second dataset:                       -->   
         
   <OPERATOR type="filter">
      <INPUT name="receipt_data.v"/>
      <PROPERTY name="filter" value="DIFF_TYPE_1 EQ '-'
          AND DIFF_TYPE_2 EQ '-' AND DIFF_TYPE_3 EQ '-'
          OR ITEM_ID IS_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT name="no_item_nbr_or_no_diffs.v"/>
      <OUTPUT name="item_nbr_with diffs.v"/>
   </OPERATOR>

   <!--  Write the item receipt data to a temporary  
         table to be used in the next RETL Flow:        -->
   
   ${DBWRITE}
      <INPUT name="item_nbr_with diffs.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_1}" />
      <PROPERTY name="method" value="conventional"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>
   
   <!--  Drop the diff_type fields in the no_item_nbr_or_no_diffs dataset:   -->
   
   <OPERATOR type="fieldmod">
      <INPUT name="no_item_nbr_or_no_diffs.v"/>
      <PROPERTY name="drop" value="DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="DIFF_TYPE_4"/>
      <OUTPUT name="dept_class_subclass_or_no_diff_types.v"/>
   </OPERATOR>
   
   <!--  ***************************************************************  -->
   <!--  *******  Write the dept_class_subclass_or_no_diff_types  ******  -->
   <!--  *******  receipt data into a temporary table to be merged   ******  -->
   <!--  *******  into the alc_receipt_plan table after the RETL scripts  ******  -->
   <!--  *******  have finished:                                  ******  -->
   <!--  ***************************************************************  --> 

   ${DBWRITE}
      <INPUT name="dept_class_subclass_or_no_diff_types.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_3}" />
      <PROPERTY name="method" value="conventional"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>
   
</FLOW>
EOF

############################################
#  Run the first RETL flow:
############################################

message "Running the first RETL flow (A) - Thread number: $THREAD"

${RFX_EXE} $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_A_$THREAD.xml

rfx_stat=$?

message "Completed exec. of 1st RETL flow (A). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "First RETL flow (A) failed - check $ERR_FILE"

##################################################################
#  Copy the second RETL flow to a file for later execution:
##################################################################

cat > $LOG_DIR/${PROGRAM_NAME}_B_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_B.flw">

   <!--  ****************************************************************  -->
   <!--  Join the DIFF_TYPE column from the DIFF_GROUP_HEAD table with     -->
   <!--  the TEMP_TABLE_1 table containing the Diff data (from             -->
   <!--  the previous RETL Flow). Join only for rows where ITEM in         -->
   <!--  TEMP_TABLE_1 is also found in ITEM_MASTER:                        -->
   <!--  ****************************************************************  -->

   ${DBREAD}
      <PROPERTY name = "query">
          <![CDATA[
                    select dpf.DEPT,
                           dpf.CLASS,
                           dpf.SUBCLASS,
                           dpf.ITEM_ID,
                           dpf.LOC,
                           dpf.DIFF_TYPE_1,
                           dpf.DIFF1_ID,
                           dpf.DIFF_TYPE_2,
                           dpf.DIFF2_ID,
                           dpf.DIFF_TYPE_3,
                           dpf.DIFF3_ID,
                           dpf.DIFF_TYPE_4,
                           dpf.DIFF4_ID,
                           dpf.EOW_DATE,
                           dpf.QTY,
                           d1.diff_type IM_DIFF_TYPE_1,
                           d2.diff_type IM_DIFF_TYPE_2,
                           d3.diff_type IM_DIFF_TYPE_3,
                           d4.diff_type IM_DIFF_TYPE_4
                    from diff_group_head d1,
                         diff_group_head d2,
                         diff_group_head d3,
                         diff_group_head d4,
                         item_master im,
                         $TEMP_TABLE_1 dpf
                    where im.diff_1 = d1.diff_group_id (+)
                    and   im.diff_2 = d2.diff_group_id (+)
                    and   im.diff_3 = d3.diff_group_id (+)
                    and   im.diff_4 = d4.diff_group_id (+)
                    and   dpf.ITEM_ID = im.ITEM
          ]]>
      </PROPERTY>
      <OUTPUT name="item_receipt_with_dgh_diff_type.v"/>
   </OPERATOR>

   <!--  Add DIFF fields to item receipt dataset:  -->
   
   <OPERATOR type="generator">
      <INPUT name="item_receipt_with_dgh_diff_type.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="DIFF1" type="string" maxlength="10" nullable="true">
               <CONST value=""/>
            </FIELD>            
            <FIELD name="DIFF2" type="string" maxlength="10" nullable="true">
               <CONST value=""/>
            </FIELD>
            <FIELD name="DIFF3" type="string" maxlength="10" nullable="true">
               <CONST value=""/>
            </FIELD>  
            <FIELD name="DIFF4" type="string" maxlength="10" nullable="true">
               <CONST value=""/>
            </FIELD>  
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="item_receipt_with_extra_diff.v"/>
   </OPERATOR>
   
   <!--  Place the diffs in the correct DIFF field  
         in the item receipt dataset:                    -->
   
   <OPERATOR type="parser">
      <INPUT name="item_receipt_with_extra_diff.v"/>
      <PROPERTY name = "expression">  
        <![CDATA[
          if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_1)  {
            RECORD.DIFF1 = RECORD.DIFF1_ID;
          }
          else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_2)  {
            RECORD.DIFF2 = RECORD.DIFF1_ID;
          }
          else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_3)  {
            RECORD.DIFF3 = RECORD.DIFF1_ID;
          }
          else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_4)  {
            RECORD.DIFF4 = RECORD.DIFF1_ID;
          }
          else if(RECORD.DIFF_TYPE_1 != "")  {
            RECORD.DIFF1 = RECORD.DIFF1_ID; 
          }
                 
          if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_1)  {
            RECORD.DIFF1 = RECORD.DIFF2_ID;
          }
          else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_2)  {
            RECORD.DIFF2 = RECORD.DIFF2_ID;
          }
          else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_3)  {
            RECORD.DIFF3 = RECORD.DIFF2_ID;
          }
          else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_4)  {
            RECORD.DIFF4 = RECORD.DIFF2_ID;
          }
          else if(RECORD.DIFF_TYPE_2 != "")  {
            RECORD.DIFF2 = RECORD.DIFF2_ID; 
          }
        
          if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_1)  {
            RECORD.DIFF1 = RECORD.DIFF3_ID;
          }
          else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_2)  {
            RECORD.DIFF2 = RECORD.DIFF3_ID;
          }
          else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_3)  {
            RECORD.DIFF3 = RECORD.DIFF3_ID;
          }
          else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_4)  {
            RECORD.DIFF4 = RECORD.DIFF3_ID;
          }
          else if(RECORD.DIFF_TYPE_3 != "")  {
            RECORD.DIFF3 = RECORD.DIFF3_ID; 
          }
          if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_1)  {
      RECORD.DIFF1 = RECORD.DIFF4_ID;
    }
    else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_2)  {
      RECORD.DIFF2 = RECORD.DIFF4_ID;
    }
    else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_3)  {
       RECORD.DIFF3 = RECORD.DIFF4_ID;
    }
    else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_4)  {
       RECORD.DIFF4 = RECORD.DIFF4_ID;
    }
    else if(RECORD.DIFF_TYPE_4 != "")  {
       RECORD.DIFF4 = RECORD.DIFF4_ID; 
          }
        ]]>
      </PROPERTY>
      <OUTPUT name="item_receipt_diffs_in_correct_order.v"/>
   </OPERATOR>
   
   <!--  Drop the DIFFn_ID, DIFF_TYPE_n and IM_DIFF_TYPE_n fields
         and change the names of the DIFFn fields back to DIFFn_ID 
         in the item receipt dataset:                                    -->
   
   <OPERATOR type="fieldmod">
      <INPUT name="item_receipt_diffs_in_correct_order.v"/>
      <PROPERTY name="drop" value="DIFF1_ID"/>
      <PROPERTY name="drop" value="DIFF2_ID"/>
      <PROPERTY name="drop" value="DIFF3_ID"/>
      <PROPERTY name="drop" value="DIFF4_ID"/>
      <PROPERTY name="rename" value="DIFF1_ID=DIFF1"/>
      <PROPERTY name="rename" value="DIFF2_ID=DIFF2"/>
      <PROPERTY name="rename" value="DIFF3_ID=DIFF3"/>
      <PROPERTY name="rename" value="DIFF4_ID=DIFF4"/>
      <PROPERTY name="drop" value="DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="DIFF_TYPE_4"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_4"/>
      <OUTPUT name="item_receipt.v"/>
   </OPERATOR>

   <!--  **************************************************************  -->
   <!--  *******  Write the item receipt data into                 *******  -->
   <!--  *******  a temporary table to be merged into the       *******  -->
   <!--  *******  alc_receipt_plan table later:                         *******  -->
   <!--  **************************************************************  --> 
     
   ${DBWRITE}
     <INPUT name="item_receipt.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_2}" />
      <PROPERTY name="method" value="direct"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>

</FLOW>
EOF

############################################
#  Run the second RETL flow:
############################################

message "Running the second RETL flow (B) - Thread number: $THREAD"

${RFX_EXE}  $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_B_$THREAD.xml

rfx_stat=$?

message "Completed exec. of 2nd RETL flow (B). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "Second RETL flow (B) failed - check $ERR_FILE"

# NOTE: Bookmark could be inserted here


##################################################################
#  Copy the Third RETL flow to a file for later execution:
##################################################################

cat > $LOG_DIR/${PROGRAM_NAME}_C_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_C.flw">

${DBREAD}
         <PROPERTY name = "query">
             <![CDATA[
             select distinct(temp2.LOC) LOC,
                  temp2.DEPT,
                  temp2.CLASS,
                  temp2.SUBCLASS,
                  temp2.ITEM_ID,
                  temp2.DIFF1_ID,
                  temp2.DIFF2_ID,
              temp2.DIFF3_ID,
              temp2.DIFF4_ID,
              temp2.EOW_DATE,
              temp2.qty 
       from 
            item_master par, item_master chi , $TEMP_TABLE_2 temp2
       where  
            temp2.item_id=par.item and  par.item=chi.item_parent and  
                nvl(temp2.diff1_ID,' ') = (case PAR.diff_1_aggregate_ind when 'Y' then chi.diff_1 ELSE '' END) AND 
              nvl(temp2.diff2_ID,' ') = (case PAR.diff_2_aggregate_ind when 'Y' then chi.diff_2 ELSE ' ' END) AND
              nvl(temp2.diff3_ID,' ') = (case PAR.diff_3_aggregate_ind when 'Y' then chi.diff_3 ELSE ' ' END) AND
                    nvl(temp2.diff4_ID,' ') = (case PAR.diff_4_aggregate_ind when 'Y' then chi.diff_4 ELSE ' ' END)
               ]]>
         </PROPERTY>
         <OUTPUT name="item_profile_with_only_valid_records.v"/>
   </OPERATOR>

   ${DBWRITE}
        <INPUT name="item_profile_with_only_valid_records.v"/>
         <PROPERTY name="tablename" value="${TEMP_TABLE_3}" />
         <PROPERTY name="method" value="direct"/>
         <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>

</FLOW>
EOF

############################################
#  Run the Third RETL flow:
############################################

message "Running the Third RETL flow (C) - Thread number: $THREAD"

${RFX_EXE} $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_C_$THREAD.xml

rfx_stat=$?

message "Completed execution of 3rd RETL flow (C). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "Third RETL flow (C) failed - check $ERR_FILE"

# NOTE: Bookmark could be inserted here


############################################
#  Create an INDEX for TEMP_TABLE_3 table:
############################################

message "Building the ${TEMP_TABLE_3}_i1 INDEX for table ${TEMP_TABLE_3}"

.  ${LIB_DIR}/alc_create_idx.ksh "${TEMP_TABLE_3}_i1" "${TEMP_TABLE_3}"     \
      "${INDEX_KEYS}"

############################################
#  Analyze the TEMP_TABLE_3 table:
############################################

message "Analyzing temp table $ALC_OWNER.${TEMP_TABLE_3}"

analyze_tbl $ALC_OWNER.${TEMP_TABLE_3}

###############################################################
#  Create the SEQUENCE needed in the MERGE INTO that follows:
###############################################################

#  Check to see if the SEQUENCE already exists:

seq=`. ${LIB_DIR}/alc_check_seq.ksh  alc_receipt_plan_seq  $DBNAME`

if [ $seq -eq 0 ]; then  #  If it does not exist, create it:

  message "Creating the SEQUENCE alc_receipt_plan_seq to be used in the MERGE INTO the $TARGET_TABLE table."

  create_seq="CREATE SEQUENCE alc_receipt_plan_seq INCREMENT BY 1 START WITH 1000000"

  query_db  "$DBNAME" "$create_seq"
fi

#  NOTE: Bookmark could be inserted here

#########################################################################
#  Build the sql "MERGE INTO" command to merge the dept-class-subclass
#  receipt data and the item receipt data (which is now in 
#  TEMP_TABLE_3) into the alc_receipt_plan table:
#########################################################################

export MERGE_STATEMENT="
    MERGE /*+ APPEND index(aps, ${TEMP_TABLE_3}_i1) index(apt, alc_receipt_plan_i1) */
    INTO ${TARGET_TABLE} apt
    USING ${TEMP_TABLE_3} aps
    ON ( apt.loc  = aps.loc
          --  NOTE : each row returns true if there are nulls since MERGE
          --  acts like an innerjoin and will not match records on fields
          --  that are null
    AND ( (apt.dept = aps.dept) OR (aps.dept IS NULL AND apt.dept IS NULL) )
    AND ( (apt.class = aps.class) OR (aps.class IS NULL
         AND apt.class IS NULL) )
    AND ( (apt.subclass = aps.subclass) OR (aps.subclass IS NULL
         AND apt.subclass IS NULL) )
    AND ( (apt.ITEM = aps.ITEM_ID) OR (aps.ITEM_ID IS NULL
         AND apt.ITEM IS NULL) )
    AND ( (apt.DIFF1 = aps.DIFF1_ID) OR (aps.DIFF1_ID IS NULL
         AND apt.DIFF1 IS NULL) )
    AND ( (apt.DIFF2 = aps.DIFF2_ID) OR (aps.DIFF2_ID IS NULL
         AND apt.DIFF2 IS NULL) )
    AND ( (apt.DIFF3 = aps.DIFF3_ID) OR (aps.DIFF3_ID IS NULL
         AND apt.DIFF3 IS NULL) )
    AND ( (apt.DIFF4 = aps.DIFF4_ID) OR (aps.DIFF4_ID IS NULL
         AND apt.DIFF4 IS NULL) )
    AND  (apt.EOW_DATE = aps.EOW_DATE)  )
    WHEN MATCHED THEN UPDATE SET apt.qty = aps.qty
    WHEN NOT MATCHED THEN INSERT
       ( apt.receipt_plan_id, apt.loc, apt.dept, apt.class,
         apt.subclass, apt.ITEM, apt.DIFF1, apt.DIFF2,
         apt.DIFF3, apt.DIFF4, apt.EOW_DATE, apt.qty)
    VALUES( alc_receipt_plan_seq.nextval, aps.loc, aps.dept, aps.class,
            aps.subclass, aps.ITEM_ID, aps.DIFF1_ID, aps.DIFF2_ID,
            aps.DIFF3_ID, aps.DIFF4_ID, aps.EOW_DATE, aps.qty);
     commit
"

#########################################################################
#  Perform the data base "MERGE INTO" command that has just been built:
#########################################################################

message "Merging new receipt data into ${TARGET_TABLE}"

query_db "${DBNAME}" "${MERGE_STATEMENT}"

message "Program completed thread ${THREAD} successfully."

############################################
#  Remove the status file:
############################################

rm  ${STATUS_FILE}

#  Terminate successfully:

rmse_terminate 0
