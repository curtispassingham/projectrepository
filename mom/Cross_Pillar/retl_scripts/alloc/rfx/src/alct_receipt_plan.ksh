#!/bin/ksh

########################################################
# Copyright (c) 2005, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/src/alct_receipt_plan.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

##############################################################################
#
#  alct_receipt_plan.ksh
#
#  This script Transforms the receipt Data files received from
#  RPAS/RDF to the format needed by the alcl_receipt.ksh script
#  which will use the data to load, update or insert into the
#  ALC_receipt table.
#
#  Usage: alct_receipt_plan.ksh
#
#  Example input receipt Data file names:
#
#      p1dept.01 -- Product Level is Department
#      p1clss.01 -- Product Level is Class
#      p1scls.01 -- Product Level is Subclass
#      p1itpt.01 -- Product Level is Item
#
#  Example output receipt Data file name: receipt_01.dat
#
#  Author: D. W. Maas
#
##############################################################################

INPUT_DIR=$ALCHOME/data
OUTPUT_DIR=$ALCHOME/data

today=`date +%Y%m%d`
time=`date +%T`
ERROR_PATHNAME=$ALCHOME/error/alct_receipt_plan_err.$today
LOG_PATHNAME=$ALCHOME/log/$today.log

echo "alct_receipt_plan $time: Program started." >> $LOG_PATHNAME
echo "******  STARTING alct_receipt_plan: `date`  ******" > $ERROR_PATHNAME


test_mode=0
dbg1=0
dbg2=0

CHK_DIFF_NBR_FLG=0    #  If this flag is set, the diff number (2nd character)
                      #  in the input file names will be checked to ensure that
                      #  it is a number that is >= 1 and <= MAX_DIFF_NBR. 
DISPLAY_OUTPUT_FLG=0  #  If this flag is set, the contents of the output file 
                      #  will be displayed on the screen after the completion
                      #  of execution. (Used in test mode.)
COL_NBR_FLG=0         #  If this flag is set, header lines will be displayed
                      #  on the screen, showing column numbers and the expected
                      #  starting position for each field of output data. This 
                      #  should only be set if DISPLAY_OUTPUT_FLG is set.

PROFILE_NAME_LEN=9        #  Every input file name must be exactly this length.
FILENAME_FIRST_CHAR="p"   #  The first character of every file name must be
                          #  this character.
MAX_DIFF_NBR=4            #  The second character of the file name must be a
                          #  DIFF number no greater than this number.
MAX_DIFF_IDS=4            #  Maximun number of DIFFs per input file record.
MAX_DIFF_LEN=11           #  Maximun length of a DIFF in the input file record.
                          #  (This does not count the leading "_" (undrscore)
                          #  character.)
                          
#  Product levels:

PROD_LEVEL_1=dept         #  This string starting in the third character 
                          #  position of the input file name indicates that the
                          #  file contains receipt data at the Department level.
PROD_LEVEL_2=clss         #  Input receipt data is at the Class level.
PROD_LEVEL_3=scls         #  Input receipt data is at the Subclass level.
PROD_LEVEL_4=itpt         #  Input receipt data is at the Item Parent or Style
                          #  level.

if [ $DISPLAY_OUTPUT_FLG -eq 0 ]; then  COL_NBR_FLG=0; fi

##################################################################
#  Starting column position and length for every field in each 
#  record of the input file, regardless of the file type:
##################################################################

DEPT_ST=1 ;     DEPT_LEN=4    
CLASS_ST=5 ;    CLASS_LEN=4
SUBCLASS_ST=9 ; SUBCLASS_LEN=4
ITEM_ST=1 ;     ITEM_LEN=25
LOCID_ST=26 ;   LOCID_LEN=20
DIFFID_ST=46 ;  DIFFID_LEN=48
EOW_DATE_ST=94;   EOW_DATE_LEN=8
QTY_ST=102 ;     QTY_LEN=12

#  Display the "Usage" line if the "-h" argument is entered:

if [ "$1" = -h ]; then
  echo
  grep  Usage:  $ALCHOME/rfx/src/alct_receipt_plan.ksh  |  grep -v grep
  echo
  exit
fi

#  Delete previous output files:

rm  $OUTPUT_DIR/receipt_*.dat  2>/dev/null
 
if [ -f  trans_temp ]; then rm  trans_temp; fi  #  Delete temporary file.

#####################################################
#  Print header column names and column numbers
#  if flag is set:
#####################################################

if [ $COL_NBR_FLG -eq 1 ]; then
  echo
  nawk  'BEGIN{
      print"D   C   S   Item or Style            Location            Diff1      Diff2      Diff3      Diff4      EOW     Qty"
      print"|   |   |   |                        |                   |          |          |          |          |       |"
      for(i=0; i<=11; i++)  {
        k = i
        if( k == 0)   printf("         ")
        else  {
          if( k > 9)  k = k - 10
          printf("%-10d", k)
        }
      }
      print""
      for(i=1; i<=12; i++)  printf("1234567890")
      print""
  }'
fi

nbr_errors=0
nbr_input_files=0

#####################################################
#  Loop through all $INPUT_DIR file names, looking
#  for valid receipt Data input file names:
#####################################################

for receipt_file in `ls -1 $INPUT_DIR`; do

receipt_input_path=$INPUT_DIR/$receipt_file
if [ $dbg2 -eq 1 ]; then
  if [ ! -f  $receipt_input_path ]; then
    echo receipt FILE NOT FOUND OR IS A DIRECTORY
    echo
    continue
  fi
fi

if [ $dbg1 -eq 1 ]; then
  echo
  echo File name: $receipt_file
fi

if [ ! -f  $receipt_input_path ]; then continue; fi

####################################################
#  Check to see if receipt file is a valid receipt
#  input file name and get the prod level:
####################################################

prod_level=`echo  $receipt_file  |  nawk   \
    -v name_len=$PROFILE_NAME_LEN          \
    -v first_char=$FILENAME_FIRST_CHAR     \
    -v pl1=$PROD_LEVEL_1                   \
    -v pl2=$PROD_LEVEL_2                   \
    -v pl3=$PROD_LEVEL_3                   \
    -v pl4=$PROD_LEVEL_4   '
   length($0) != name_len { print "invalid"; exit}  #  wrong length
   substr($1,1,1) != first_char { print "invalid"; exit}  #  wrong first char.
   substr($1,7,1) != "." { print "invalid"; exit}  #  7th char. not a dot 
   index("0123456789", substr($1,8,1)) <= 0 { print "invalid"; exit} # not an integer
   index("123456789", substr($1,9,1)) <= 0 { print "invalid"; exit} # not an integer
   substr($1,3,4) == pl1 { print pl1; exit}  #  dept
   substr($1,3,4) == pl2 { print pl2; exit}  #  clss
   substr($1,3,4) == pl3 { print pl3; exit}  #  scls
   substr($1,3,4) == pl4 { print pl4; exit}  #  itpt
   { print "invalid"; exit                   #  fails all tests
}'`

#  If not a valid receipt input file name, skip this file:

if [ $prod_level = "invalid" ]; then continue; fi

if [ $dbg1 -eq 1 ]; then
  echo " Input receipt file = $receipt_file     prod_level: $prod_level"
fi

##################################################
#  Check the DIFF number of the input file 
#  name if CHK_DIFF_NBR_FLG is set:
##################################################

if [ $CHK_DIFF_NBR_FLG -eq 1 ]; then

  diff_nbr=`echo $receipt_file | nawk -v max_diff_nbr=$MAX_DIFF_NBR   '{
      dnbr = substr($0, 2, 1)
      if( dnbr < 1 )  print 0
      else if( dnbr > max_diff_nbr )  print 0
      else if( (2 * dnbr)/2 != dnbr )  print 0
      else  print dnbr
      exit}'`

  if [ $diff_nbr -eq 0 ]; then
    echo
    echo ERROR - CANNOT DETERMINE DIFF NUMBER FROM DIFF PROFILE FILE NAME.
    echo receipt Data file name: $receipt_file
    echo
    exit
  fi
fi

#################################################
#  Get the DOMAIN ID from the input file name:
#################################################

domain=`echo  $receipt_file  |  nawk '{
    ## print" $0 =", $0, "   "
    dom2 = substr($0, 8, 2)
    if( substr(dom2, 1, 1) == "0")  dom = substr(dom2, 2, 1)
    else  dom = dom2
    ## print" dom =", dom, " "
    if( dom < 1 )  print 0
    else  if((2 * dom)/2 != dom)  print 0
    else  {
      if(dom < 10)  dom = "0" dom
      print dom
    }
    exit}'`
    
if [ $domain -lt 1 ] || [ $domain -gt 99 ]; then
  echo
  echo ERROR - CANNOT DETERMINE DOMAIN ID FROM receipt DATA FILE NAME.
  echo receipt Data file name: $receipt_file
  echo
  exit
fi

#############################################
#  Build the output file name and pathname 
#  using the DOMAIN ID:
#############################################

receipt_output_file="receipt_$domain.dat"
receipt_output_path=$OUTPUT_DIR/$receipt_output_file

#################################################
#  For test & maintenance only:
#################################################

if [ $dbg1 -eq 1 ]; then
  echo
  echo receipt_file=$receipt_file
  echo prod_level=$prod_level
  echo diff_nbr=$diff_nbr
  echo domain  = $domain
  echo receipt_output_file: $receipt_output_file
  echo receipt_output_path = $receipt_output_path  
  echo
fi

let nbr_input_files=nbr_input_files+1

######################################################
#  Transform the receipt data records into the format
#  needed by the alcl_receipt.ksh script:
######################################################

nawk  -v receipt_file=$receipt_file           \
      -v prod_level=$prod_level         \
      -v max_diff_ids=$MAX_DIFF_IDS     \
      -v pl1=$PROD_LEVEL_1              \
      -v pl2=$PROD_LEVEL_2              \
      -v pl3=$PROD_LEVEL_3              \
      -v pl4=$PROD_LEVEL_4              \
      -v dept_st=$DEPT_ST               \
      -v dept_len=$DEPT_LEN             \
      -v class_st=$CLASS_ST             \
      -v class_len=$CLASS_LEN           \
      -v subclass_st=$SUBCLASS_ST       \
      -v subclass_len=$SUBCLASS_LEN     \
      -v item_st=$ITEM_ST               \
      -v item_len=$ITEM_LEN             \
      -v locid_st=$LOCID_ST             \
      -v locid_len=$LOCID_LEN           \
      -v diffid_st=$DIFFID_ST           \
      -v diffid_len=$DIFFID_LEN         \
      -v max_diff_len=$MAX_DIFF_LEN     \
      -v eow_date_st=$EOW_DATE_ST       \
      -v eow_date_len=$EOW_DATE_LEN     \
      -v qty_st=$QTY_ST                 \
      -v qty_len=$QTY_LEN               \
      -v test_mode=$test_mode           \
      -v error_file=$ERROR_PATHNAME     \
      -v time=$time                     \
'BEGIN{
  print"alct_receipt_plan.ksh:", time, "Program starting. Input file:", FILENAME  \
      >>  error_file
}
{  
  #  Beginning of loop which reads the receipt Data input records:
  
  ## print" FILENAME, NR, NF:", FILENAME, NR, NF
  
  #  Initialize everything to nulls:

  dept = ""
  class = ""
  subclass = ""
  item = ""
  at_least_one_diff = 0
  
  for(i=1; i<=max_diff_ids; i++)  DIFF[i] = ""
  
  #  If reading a Department level file:
  
  if( prod_level == pl1 )  dept = $1 
  
  #  If reading a Class level file:
  
  if( prod_level == pl2 )  {          
    dept = substr($0, dept_st, dept_len)
    class = substr($0, class_st, class_len)
  }
  #  If reading a Subclass level file:
  
  if( prod_level == pl3 )  {          
    dept = substr($0, dept_st, dept_len)
    class = substr($0, class_st, class_len)
    subclass = substr($0, subclass_st, subclass_len)
  }
  #  If reading an Item-Parent or Style level file:
  
  if( prod_level == pl4 )  item = substr($0, item_st, item_len)  

  location = substr($0, locid_st,  locid_len)  #  Location ID
  
  diff_str = substr($0, diffid_st+1, diffid_len-1)  #  Diff type and ID

  eow_date = substr($0, eow_date_st, eow_date_len)  #  End-of-Week Date
   
  # Get the Quantity:
  
  qty = substr($0, qty_st, qty_len)
  qty = qty/10000  #  Input data has no decimal point.
  
  #  Split up the DIFF string into separate DIFFs. The underscore character
  #  is the separation character between separate DIFFs:
  
  ## Example Diff ID field character string: _Smedium_Cblue_Lshort-slv
  ## Corresponding diff_str: Smedium_Cblue_Lshort-slv (no leading "_")
  
  nd = split(diff_str, DIFFA, "_")
  
  #  Check for too many Diffs:

  if( nd > max_diff_ids )  {
    print" ERROR - too many DIFF IDs (" nd ") in record", NR, "of", receipt_file     \
         >>  error_file
    print" Maximum number of Diffs allowed:", max_diff_ids  >>  error_file
    print" DIFF ID string: <"  diff_str ">"  >>  error_file
    print"Bad Record follows:"  >>  error_file
    print $0  >>  error_file
    exit 2
  }
  
  #  Ensure that every Diff is no longer than the maximum 
  #  allowed (truncate if necessary), and determine if   
  #  there is at least one diff: 

  for(i=1; i<=nd; i++)  {
    if(length(DIFFA[i]) <= max_diff_len)  DIFF[i] = DIFFA[i]
    else  DIFF[i] = substr(DIFFA[i], 1, max_diff_len)
    if(substr(DIFF[i], 1, 1) != " ")  at_least_one_diff = 1
  }
  
  #  If there are no diffs, insert a dash (hyphen) into the diff type
  #  position for all three missing diffs:
  
  if(at_least_one_diff == 0)  {
    for(i=1; i<=max_diff_ids; i++)  DIFF[i] = "-"
  }
    
  #  For test mode only:
    
  if(test_mode == 1)  {
    print"" >> "trans_temp"
    print" receipt_file:", receipt_file >> "trans_temp"
    print"  Location = <" location ">" >> "trans_temp"
    print"  DIFF[1]  = <" DIFF[1] ">" >> "trans_temp"
    print"  DIFF[2]  = <" DIFF[2] ">" >> "trans_temp"
    print"  DIFF[3]  = <" DIFF[3] ">" >> "trans_temp"
    print"  DIFF[4]  = <" DIFF[4] ">" >> "trans_temp"
    print" eow_date  =", eow_date >> "trans_temp"
    print" qty =", qty >> "trans_temp"
    printf("<1234567890123456789012>\n") >> "trans_temp"
    printf("<          %12.4f>\n", qty) >> "trans_temp"
  }
  ###############################################################
  #  Write out the output record which will become the input
  #  data for the alcl_receipt.ksh script (do not change
  #  the number of blank spaces in the format string):
  ###############################################################
  
  #printf("%-4s%-4s%-4s%-25s%-20s%-11s%-11s%-11s           %8s%14.4f\n",
   printf("%-4s%-4s%-4s%-25s%-20s%-11s%-11s%-11s%-11s%8s%14.4f\n",
   dept, class, subclass, item, location, DIFF[1], DIFF[2], DIFF[3], DIFF[4], eow_date, qty)
    
}
END{
  print"alct_receipt_plan.ksh:", time, "Transform completed. File:",
      receipt_file  >>  error_file
  
}'  $receipt_input_path  >>  $receipt_output_path

#  END OF AWK PROGRAM

###########################################################################

exit_stat=$?

echo "$receipt_file processing completed. Exit status: $exit_stat"     \
     >>  $ERROR_PATHNAME

if [ $exit_stat -ne 0 ]; then
  let nbr_errors=nbr_errors+1
fi

#################################################
#  For test & maintenance purposes only:
#################################################

if [ $dbg2 -eq 1 ]; then
  echo
  echo " nawk exit code:" $exit_stat
  echo " receipt_file (input file): $receipt_input_path"
  echo " receipt_out (output file): `wc  $receipt_output_path`"
fi

done  #  End of for loop ("for receipt_file in ls -1 $INPUT_DIR").

#################################################
#  Dump out the output files (if flag is set):
#################################################

if [ $DISPLAY_OUTPUT_FLG -eq 1 ]; then

  if [ $COL_NBR_FLG -eq 0 ]; then 
    echo
    echo $receipt_output_path:
    echo ---------------------
  fi

  cat  $receipt_output_path
  echo
fi

if [ $test_mode -eq 1 ]; then
  echo
  echo
  echo trans_temp:
  echo -----------
  cat  trans_temp
  echo
fi

cd  $OUTPUT_DIR

nbr_output_files=`(ls -1 receipt_*.dat 2>/dev/null)  |  wc -l  |
      nawk '{if($1 == "")  print 0; else print $1}'`


if [ $nbr_errors -eq 0 ]; then
  echo "Number of input files processed = $nbr_input_files. Number of output files produced = $nbr_output_files" \
       >> $LOG_PATHNAME
  echo "alct_receipt_plan $time: Program completed without errors."      \
       >> $LOG_PATHNAME
  echo "Number of input files processed = $nbr_input_files. Number of output files produced = $nbr_output_files" \
       >> $ERROR_PATHNAME
  echo "alct_receipt_plan completed without errors: `date`"   \
       >> $ERROR_PATHNAME

  echo
  echo alct_receipt_plan.ksh has completed with no errors. 
  echo Number of input files processed: $nbr_input_files
  echo
  echo Output files produced:
  echo ======================
  echo
  echo " File Name     No. Recs."
  echo " ---------     ---------"
  wc -l  receipt_*.dat  |  nawk '{print"", $2, "    ", $1}'
  echo

else
  echo "alct_receipt_plan $time: Program completed with $nbr_errors ERRORS."  \
       >> $LOG_PATHNAME
  echo "alct_receipt_plan completed with $nbr_errors ERRORS: `date`"   \
       >> $ERROR_PATHNAME
  echo alct_receipt_plan.ksh has completed with $nbr_errors errors.
fi
