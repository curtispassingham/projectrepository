#!/bin/ksh

########################################################
# Copyright (c) 2005, Retek Inc.  All rights reserved.
# $Workfile:   alct_size_profile.ksh  $
# $Revision: 1149527 $
# $Modtime:    May 16  2005  16:49:09 CDT  $
########################################################

##############################################################################
#
#  alct_size_profile.ksh
#
#  This script Transforms the Diff Profile file (size curve data) received 
#  from RPAS/RDF to the format needed by the alcl_size_profile.ksh script
#  which will use the data to load/update/insert into the ALC_SIZE_PROFILE
#  table.
#
#  Usage: alct_size_profile.ksh
#
#  Example input Diff profile file names:
#
#      d1dept.01 -- Product Level is Department
#      d1clss.01 -- Product Level is Class
#      d1scls.01 -- Product Level is Subclass
#      d1itpt.01 -- Product Level is Item-Parent or Style
#
#  Example output Diff profile file name: profile_01.dat
#
#  Author: D. W. Maas
#
##############################################################################

INPUT_DIR=$ALCHOME/data
OUTPUT_DIR=$ALCHOME/data

today=`date +%Y%m%d`
time=`date +%T`
ERROR_PATHNAME=$ALCHOME/error/alct_size_profile_err.$today
LOG_PATHNAME=$ALCHOME/log/$today.log

echo "alct_size_profile $time: Program started." >> $LOG_PATHNAME
echo "******  STARTING alct_size_profile: `date`  ******" > $ERROR_PATHNAME

test_mode=0
dbg1=0
dbg2=0

CHK_DIFF_NBR_FLG=0    #  If this flag is set, the diff number (2nd character)
                      #  in the input file names will be checked to ensure that
                      #  it is a number that is >= 1 and <= MAX_DIFF_NBR. 
DISPLAY_OUTPUT_FLG=0  #  If this flag is set, the contents of the output file 
                      #  will be displayed on the screen after the completion
                      #  of execution. (Used in test mopde.)
COL_NBR_FLG=0         #  If this flag is set, header lines will be displayed
                      #  on the screen, showing column numbers and the expected
                      #  starting position for each field of output data. This 
                      #  should only be set if DISPLAY_OUTPUT_FLG is set.

PROFILE_NAME_LEN=9        #  Every input file name must be exactly this length.
FILENAME_FIRST_CHAR="d"   #  The first character of every file name must be
                          #  this character.
MAX_DIFF_NBR=4            #  The second character of the file name must be a
                          #  DIFF number no greater than this number.
MAX_DIFF_IDS=4            #  Maximun number of DIFFs per input file record.
MAX_DIFF_LEN=11           #  Maximun length of a DIFF in the input file record.
                          #  This does not count the "_" (underscore) character.
                          
#  Product levels:

PROD_LEVEL_1=dept         #  This string starting in the third character 
                          #  position of the input file name indicates that the
                          #  file contains Diff data at the Department level.
PROD_LEVEL_2=clss         #  Input Diff data is at the Class level.
PROD_LEVEL_3=scls         #  Input Diff data is at the Subclass level.
PROD_LEVEL_4=itpt         #  Input Diff data is at the Item Parent or Style
                          #  level.

if [ $DISPLAY_OUTPUT_FLG -eq 0 ]; then  COL_NBR_FLG=0; fi

###################################################################
#  Starting column position and length for every field in each 
#  record of the input file, regardless of the file type:
###################################################################

DEPT_ST=1 ;     DEPT_LEN=4
CLASS_ST=5 ;    CLASS_LEN=4
SUBCLASS_ST=9 ; SUBCLASS_LEN=4
ITEM_ST=1 ;     ITEM_LEN=25
LOCID_ST=26 ;   LOCID_LEN=20
DIFFID_ST=46 ;  DIFFID_LEN=48
QTY_ST=94 ;     QTY_LEN=12

#  Display the "Usage" line if the "-h" argument is entered:

if [ "$1" = -h ]; then
  echo
  grep  Usage:  $ALCHOME/rfx/src/alct_size_profile.ksh  |  grep -v grep
  echo
  exit
fi

#  Delete previous output files:

rm  $OUTPUT_DIR/profile_*.dat  2>/dev/null

if [ -f  trans_temp ]; then rm  trans_temp; fi  #  Delete temporary file.

#####################################################
#  Print header column names and column numbers
#  if flag is set:
#####################################################

if [ $COL_NBR_FLG -eq 1 ]; then
  echo
  nawk  'BEGIN{
      print"D   C   S   Item or Style            Location            Diff1      Diff2      Diff3      Diff4      Qty"
      print"|   |   |   |                        |                   |          |          |          |          |"
      for(i=0; i<=10; i++)  {
        k = i
        if( k == 0)   printf("         ")
        else  {
          if( k > 9)  k = k - 10
          printf("%-10d", k)
        }
      }
      print""
      for(i=1; i<=11; i++)  printf("1234567890")
      print""
  }'
fi

nbr_errors=0
nbr_input_files=0

#####################################################
#  Loop through all $INPUT_DIR file names, looking
#  for valid Diff Profile input file names:
#####################################################

for profile_file in `ls -1 $INPUT_DIR`; do

profile_input_path=$INPUT_DIR/$profile_file
if [ $dbg2 -eq 1 ]; then
  if [ ! -f  $profile_input_path ]; then
    echo PROFILE FILE NOT FOUND OR IS A DIRECTORY
    echo
    continue
  fi
fi

if [ $dbg1 -eq 1 ]; then
  echo
  echo File name: $profile_file
fi

if [ ! -f  $profile_input_path ]; then continue; fi

####################################################
#  Check to see if profile_file is a valid size
#  profile input file name and get the prod level:
####################################################

prod_level=`echo  $profile_file  |  nawk   \
    -v name_len=$PROFILE_NAME_LEN          \
    -v first_char=$FILENAME_FIRST_CHAR     \
    -v pl1=$PROD_LEVEL_1                   \
    -v pl2=$PROD_LEVEL_2                   \
    -v pl3=$PROD_LEVEL_3                   \
    -v pl4=$PROD_LEVEL_4   '
   length($0) != name_len { print "invalid"; exit}  #  wrong length
   substr($1,1,1) != first_char { print "invalid"; exit}  #  wrong first char.
   substr($1,7,1) != "." { print "invalid"; exit}  #  7th char. not a dot 
   index("0123456789", substr($1,8,1)) <= 0 { print "invalid"; exit} # not an integer
   index("123456789", substr($1,9,1)) <= 0 { print "invalid"; exit} # not an integer
   substr($1,3,4) == pl1 { print pl1; exit}  #  dept
   substr($1,3,4) == pl2 { print pl2; exit}  #  clss
   substr($1,3,4) == pl3 { print pl3; exit}  #  scls
   substr($1,3,4) == pl4 { print pl4; exit}  #  itpt
   { print "invalid"; exit                   #  fails all tests
}'`

#  If not a valid profile input file name, skip this file:

if [ $prod_level = "invalid" ]; then continue; fi

if [ $dbg1 -eq 1 ]; then
  echo " Input profile file = $profile_file     prod_level: $prod_level"
fi

##################################################
#  Check the DIFF number of the input file 
#  name if CHK_DIFF_NBR_FLG is set:
##################################################

if [ $CHK_DIFF_NBR_FLG -eq 1 ]; then

  diff_nbr=`echo $profile_file | nawk -v max_diff_nbr=$MAX_DIFF_NBR   '{
      dnbr = substr($0, 2, 1)
      if( dnbr < 1 )  print 0
      else if( dnbr > max_diff_nbr )  print 0
      else if( (2 * dnbr)/2 != dnbr )  print 0
      else  print dnbr
      exit}'`

  if [ $diff_nbr -eq 0 ]; then
    echo
    echo ERROR - CANNOT DETERMINE DIFF NUMBER FROM DIFF PROFILE FILE NAME.
    echo Diff Profile file name: $profile_file
    echo
    exit
  fi
fi

#################################################
#  Get the DOMAIN ID from the input file name:
#################################################

## echo "profile_file - 2 = $profile_file"

domain=`echo  $profile_file  |  nawk '{
    ## print" $0 =", $0, "   "
    dom2 = substr($0, 8, 2)
    if( substr(dom2, 1, 1) == "0")  dom = substr(dom2, 2, 1)
    else  dom = dom2
    ## print" dom =", dom, " "
    if( dom < 1 )  print 0
    else  if((2 * dom)/2 != dom)  print 0
    else  {
      if(dom < 10)  dom = "0" dom
      print dom
    }
    exit}'`
    
if [ $domain -lt 1 ] || [ $domain -gt 99 ]; then
  echo
  echo ERROR - CANNOT DETERMINE DOMAIN ID FROM DIFF PROFILE FILE NAME.
  echo Diff Profile file name: $profile_file
  echo
  exit
fi

#############################################
#  Build the output file name and pathname 
#  from the DOMAIN ID:
#############################################

profile_output_file="profile_$domain.dat"
profile_output_path=$OUTPUT_DIR/$profile_output_file

#################################################
#  For test & maintenance only:
#################################################

if [ $dbg1 -eq 1 ]; then
  echo
  echo profile_file=$profile_file
  echo prod_level=$prod_level
  echo diff_nbr=$diff_nbr
  echo domain  = $domain
  echo profile_output_file: $profile_output_file
  echo profile_output_path = $profile_output_path  
  echo
fi

let nbr_input_files=nbr_input_files+1

########################################################
#  Transform the Diff (size) Profile records into the  
#  format needed by the alcl_size_profile.ksh script:
########################################################

nawk  -v profile_file=$profile_file     \
      -v prod_level=$prod_level         \
      -v max_diff_ids=$MAX_DIFF_IDS     \
      -v pl1=$PROD_LEVEL_1              \
      -v pl2=$PROD_LEVEL_2              \
      -v pl3=$PROD_LEVEL_3              \
      -v pl4=$PROD_LEVEL_4              \
      -v dept_st=$DEPT_ST               \
      -v dept_len=$DEPT_LEN             \
      -v class_st=$CLASS_ST             \
      -v class_len=$CLASS_LEN           \
      -v subclass_st=$SUBCLASS_ST       \
      -v subclass_len=$SUBCLASS_LEN     \
      -v item_st=$ITEM_ST               \
      -v item_len=$ITEM_LEN             \
      -v locid_st=$LOCID_ST             \
      -v locid_len=$LOCID_LEN           \
      -v diffid_st=$DIFFID_ST           \
      -v diffid_len=$DIFFID_LEN         \
      -v max_diff_len=$MAX_DIFF_LEN     \
      -v qty_st=$QTY_ST                 \
      -v qty_len=$QTY_LEN               \
      -v test_mode=$test_mode           \
      -v error_file=$ERROR_PATHNAME     \
      -v log_file=$LOG_PATHNAME         \
      -v time=$time                     \
'BEGIN{
  print"alct_size_profile.ksh:", time, "Data transform (awk) starting. Input file:",
      FILENAME  >>  error_file
}
{  
  #  Beginning of loop which reads the Diff Profile input records:
  
  ## print" FILENAME, NR, NF:", FILENAME, NR, NF
  
  #  Initialize everything to nulls:

  dept = ""
  class = ""
  subclass = ""
  item = ""
  for(i=1; i<=max_diff_ids; i++)  DIFF[i] = ""
  
  for(i=1; i<=4; i++)   DIFF[i] = " "
  
  #  If reading a Department level file:
  
  if( prod_level == pl1 )  dept = $1 
  
  #  If reading a Class level file:
  
  if( prod_level == pl2 )  {          
    dept = substr($0, dept_st, dept_len)
    class = substr($0, class_st, class_len)
  }
  #  If reading a Subclass level file:
  
  if( prod_level == pl3 )  {          
    dept = substr($0, dept_st, dept_len)
    class = substr($0, class_st, class_len)
    subclass = substr($0, subclass_st, subclass_len)
  }
  #  If reading an Item-Parent or Style level file:
  
  if( prod_level == pl4 )  item = substr($0, item_st, item_len)  

  location = substr($0, locid_st,  locid_len)  #  Location ID
  
  diff_str = substr($0, diffid_st+1, diffid_len-1)  #  Diff type and ID
  
  # Get the Quantity (decimal fraction for the Diffs):
  
  qty = substr($0, qty_st, qty_len)
  qty = qty/10000  #  Input data has no decimal point.

  #  If at the Item-Parent or Style level, ensure  
  #  that the diff string has at least one diff:
  
  diff_chk = split(diff_str, X, " ")
  if( diff_chk == 0 && prod_level == pl4)  {
    print" ERROR - no DIFF IDs found in record", NR, "of", profile_file \
            >>  error_file
    print"Bad Record:"  >>  error_file
    print $0  >>  error_file
    exit 1
  }

  #  Split up the DIFF string into separate DIFFs. The underscore character
  #  is the separation character between separate DIFFs:
  
  ## Example Diff ID field character string: _Smedium_Cblue_Lshort-slv
  ## Corresponding diff_str: Smedium_Cblue_Lshort-slv (no leading "_")
  
  nd = split(diff_str, DIFFA, "_")
  
  #  Check for too many Diffs:

  if( nd > max_diff_ids )  {
    print""  >>  error_file
    print" ERROR - too many DIFF IDs in current record of Diff Profile File." \
         >>  error_file
    print""  >>  error_file
    print" Diff Profile file name:", profile_file  >>  error_file
    print" Record number", NR  >>  error_file
    print" Number of Diffs found: ", nd  >>  error_file
    print" Maximum number allowed:", max_diff_ids  >>  error_file
    print" DIFF ID string: <"  diff_str ">"  >>  error_file
    print""  >>  error_file
    print"Bad Record:"  >>  error_file
    print $0  >>  error_file
    print""  >>  error_file
    exit 2
  }
  
  #  Ensure that every Diff is no longer than the maximum
  #  allowed (truncate if necessary): 

  for(i=1; i<=nd; i++)  {
    if(length(DIFFA[i]) <= max_diff_len)  DIFF[i] = DIFFA[i]
    else  DIFF[i] = substr(DIFFA[i], 1, max_diff_len)
  }
  #  For test mode only:
    
  if(test_mode == 1)  {
    print"" >> "trans_temp"
    print" profile_file:", profile_file >> "trans_temp"
    print"  Location = <" location ">" >> "trans_temp"
    print"  DIFF[1]  = <" DIFF[1] ">" >> "trans_temp"
    print"  DIFF[2]  = <" DIFF[2] ">" >> "trans_temp"
    print"  DIFF[3]  = <" DIFF[3] ">" >> "trans_temp"
    print"  DIFF[4]  = <" DIFF[4] ">" >> "trans_temp"
    print" qty =", qty >> "trans_temp"
  }
  
  ###############################################################
  #  Write out the output record which will become the input
  #  data for the alcl_size_profile.ksh script (do not change
  #  the number of blank spaces in the format string):
  ###############################################################
  
  #printf("%-4s%-4s%-4s%-25s%-20s%-11s%-11s%-11s%-11s      %14.4f\n",
   printf("%-4s%-4s%-4s%-25s%-20s%-11s%-11s%-11s%-11s%14.4f\n",
    dept, class, subclass, item, location, DIFF[1], DIFF[2], DIFF[3], DIFF[4], qty)
}
END{
  print "alct_size_profile.ksh:", time, "Transform completed. File:",
      profile_file  >>  error_file
          
}'  $profile_input_path  >>  $profile_output_path

#  END OF AWK PROGRAM

###########################################################################

exit_stat=$?

echo "$profile_file processing completed. Exit status: $exit_stat"     \
       >>  $ERROR_PATHNAME

if [ $exit_stat -ne 0 ]; then
  let nbr_errors=nbr_errors+1
fi

#################################################
#  For test & maintenance purposes only:
#################################################

if [ $dbg2 -eq 1 ]; then
  echo
  echo " nawk exit code:" $exit_stat
  echo " profile_file (input file): $profile_input_path"
  echo " profile_out (output file): `wc  $profile_output_path`"
fi

done  #  End of "for profile_file in ls -1 $INPUT_DIR"

#################################################
#  Dump out the output files (if flag is set):
#################################################

if [ $DISPLAY_OUTPUT_FLG -eq 1 ]; then

  if [ $COL_NBR_FLG -eq 0 ]; then 
    echo
    echo $profile_output_path:
    echo ---------------------
  fi

  cat  $profile_output_path
  echo
fi

if [ $test_mode -eq 1 ]; then
  echo
  echo
  echo trans_temp:
  echo -----------
  cat  trans_temp
  echo
fi

cd  $OUTPUT_DIR

nbr_output_files=`(ls -1 profile_*.dat 2>/dev/null)  |  wc -l  |
      nawk '{if($1 == "")  print 0; else print $1}'`

if [ $nbr_errors -eq 0 ]; then
  echo "Number of input files processed = $nbr_input_files. Number of output files produced = $nbr_output_files" \
       >> $LOG_PATHNAME
  echo "alct_size_profile $time: Program completed without errors."      \
       >> $LOG_PATHNAME
  echo "Number of input files processed = $nbr_input_files. Number of output files produced = $nbr_output_files" \
       >> $ERROR_PATHNAME
  echo "alct_size_profile completed without errors: `date`"   \
       >> $ERROR_PATHNAME

  echo
  echo alct_size_profile.ksh has completed with no errors.
  echo Number of input files processed: $nbr_input_files
  echo
  echo Output files produced:
  echo ======================
  echo
  echo " File Name        No. Recs."
  echo " ---------        ---------"
  wc -l  profile_*.dat  |  nawk '{print"", $2, "   ", $1}'
  echo

else
  echo "alct_size_profile $time: Program completed with $nbr_errors ERRORS."  \
       >> $LOG_PATHNAME
  echo "alct_size_profile completed with $nbr_errors ERRORS: `date`"   \
       >> $ERROR_PATHNAME
  echo alct_size_profile.ksh has completed with $nbr_errors errors.
fi
