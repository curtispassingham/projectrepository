#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $Workfile:   alcl_size_profile.ksh  $
# $Revision: 1149527 $
# $Modtime:    Oct 20  2005  15:32:24 CDT  $
# Originally created: May 16  2005  16:49:09 CDT
########################################################

##########################################################################
#
#  This Allocation script loads/updates/inserts size profile data into
#  the ALC_SIZE_PROFILE table from a profile_XX.dat file where XX is the
#  Domain ID (00-99).
#
#  Usage: alcl_size_profile.ksh  input_file  [thread_number]
#
#  Author: D. W. Maas
#
##########################################################################

if [ $# -eq 0 ]; then
  echo
  echo " ERROR - You must enter a Size Profile data input file name when"
  echo "         running this program. The thread number is optional."
  echo
  echo " Usage: alcl_size_profile.ksh  input_file  [thread_number]"
  echo
  echo " The default value for thread_number is 0."
  echo
  exit
fi

####################  SCRIPT SETUP  ######################

export PROGRAM_NAME='alcl_size_profile'

. $ALCHOME/rfx/etc/alc_config.env

. ${LIB_DIR}/alc_lib.ksh

################  DEFINITION OF CONSTANTS  ##################

export INPUT_FILE=$1

INPUT_SCHEMA=$PROGRAM_NAME.schema

if [ "$2" != "" ]; then THREAD=$2
else  THREAD=0
fi

export THREAD

export TARGET_TABLE="ALC_SIZE_PROFILE"
export TEMP_TABLE_1="${PROGRAM_NAME}_TEMP_1_${THREAD}"
export TEMP_TABLE_2="${PROGRAM_NAME}_TEMP_2_${THREAD}"
export TEMP_TABLE_3="${PROGRAM_NAME}_TEMP_3_${THREAD}"


#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_1 table later on:

export CREATE_TABLE_1_SQL="
CREATE TABLE ${TEMP_TABLE_1}
(
  DEPT             VARCHAR2(4),
  CLASS            VARCHAR2(4),
  SUBCLASS         VARCHAR2(4),
  ITEM             VARCHAR2(25),
  LOC              VARCHAR2(20)  NOT NULL,
  DIFF_TYPE_1      VARCHAR2(1)   ,
  DIFF1            VARCHAR2(10)  ,
  DIFF_TYPE_2      VARCHAR2(1),
  DIFF2            VARCHAR2(10),
  DIFF_TYPE_3      VARCHAR2(1),
  DIFF3            VARCHAR2(10),
  DIFF_TYPE_4      VARCHAR2(1),
  DIFF4            VARCHAR2(10),
  QTY              NUMBER(12,4)  NOT NULL
) NOLOGGING
"
#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_2 table later on:

export CREATE_TABLE_2_SQL="
CREATE TABLE ${TEMP_TABLE_2}
(
  LOC              VARCHAR2(40 BYTE)            NOT NULL,
  DEPT             VARCHAR2(40 BYTE),
  CLASS            VARCHAR2(40 BYTE),
  SUBCLASS         VARCHAR2(40 BYTE),
  STYLE            VARCHAR2(40 BYTE),
  SIZE1            VARCHAR2(40 BYTE),
  SIZE2            VARCHAR2(40 BYTE),
  SIZE3            VARCHAR2(40 BYTE),
  SIZE4            VARCHAR2(40 BYTE),
  SIZE_GROUP1      VARCHAR2(40 BYTE),
  SIZE_GROUP2      VARCHAR2(40 BYTE),
  QTY              NUMBER(12,4)                 NOT NULL
) NOLOGGING
"

#  Set up the command string to be used in the sql CREATE TABLE in the
#  query_db function to create the temporary TEMP_TABLE_3 table later on:

export CREATE_TABLE_3_SQL="
CREATE TABLE ${TEMP_TABLE_3}
(
  LOC                 VARCHAR2(40 BYTE)            NOT NULL,
  DEPT                VARCHAR2(40 BYTE),
  CLASS               VARCHAR2(40 BYTE),
  SUBCLASS            VARCHAR2(40 BYTE),
  STYLE               VARCHAR2(40 BYTE),
  SIZE_PROFILE_LEVEL  NUMBER  (2,0)    ,
  SIZE1               VARCHAR2(40 BYTE),
  SIZE2               VARCHAR2(40 BYTE),
  SIZE3               VARCHAR2(40 BYTE),
  SIZE4               VARCHAR2(40 BYTE),
  SIZE_GROUP1         VARCHAR2(40 BYTE),
  SIZE_GROUP2         VARCHAR2(40 BYTE),
  QTY                 NUMBER(12,4)                 NOT NULL
) NOLOGGING
"
export INDEX_KEYS="LOC,DEPT,CLASS,SUBCLASS,STYLE,SIZE1,SIZE2,SIZE3,SIZE4"

#################### MAIN PROGRAM ######################

message "Program started for thread ${THREAD}..."

#  Drop the temp tables, but don't error if they do not exist:

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_1}" 0

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_2}" 0

query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE_3}" 0

#  Create the temp tables:

query_db "${DBNAME}" "${CREATE_TABLE_1_SQL}"

query_db "${DBNAME}" "${CREATE_TABLE_2_SQL}"

query_db "${DBNAME}" "${CREATE_TABLE_3_SQL}"


#  Commented out for performance reasons:
## log_num_recs ${INPUT_FILE}

#######################################################
#  Copy the first RETL flow to a file for later execution:
#######################################################

cat > $LOG_DIR/${PROGRAM_NAME}_A_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_A.flw">

   <!--  *****************************************************  -->
   <!--  **********  Load in the size profile file  **********  -->
   <!--  *****************************************************  -->
   
   <OPERATOR  type="import">
      <PROPERTY  name="inputfile" value="${DATA_DIR}/${INPUT_FILE}"/>
      <PROPERTY  name="schemafile" value="${SCHEMA_DIR}/$INPUT_SCHEMA"/>
      <OUTPUT name="profile.v"/>
   </OPERATOR>
   
   <!--  Separate the rows into item  dept class subclass rows :          -->

   <OPERATOR type="filter">
      <INPUT name="profile.v"/>
      <PROPERTY name="filter" value="ITEM IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT name="item_profile.v"/>
      <OUTPUT name="dept_cls_subcls_profile_level.v"/>
   </OPERATOR>
   
   <OPERATOR type="filter">
      <INPUT name="dept_cls_subcls_profile_level.v"/>
      <PROPERTY name="filter" value="SUBCLASS IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT name="subclass_profile.v"/>
      <OUTPUT name="dept_cls_profile_level.v"/>
   </OPERATOR>

   <OPERATOR type="generator">
      <INPUT name="subclass_profile.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE_PROFILE_LEVEL" type="int8">
               <CONST value="3"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="subclass_profile_with_level.v"/>
   </OPERATOR>
   
   <OPERATOR type="filter">
      <INPUT name="dept_cls_profile_level.v"/>
      <PROPERTY name="filter" value="CLASS IS_NOT_NULL"/>
      <PROPERTY name="rejects" value="true"/>
      <OUTPUT name="class_profile.v"/>
      <OUTPUT name="dept_profile.v"/>
   </OPERATOR>

   <OPERATOR type="generator">
      <INPUT name="class_profile.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE_PROFILE_LEVEL" type="int8">
               <CONST value="2"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="class_profile_with_level.v"/>
   </OPERATOR>
   
   <OPERATOR type="generator">
      <INPUT name="dept_profile.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE_PROFILE_LEVEL" type="int8">
               <CONST value="1"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="dept_profile_with_level.v"/>
   </OPERATOR>
   
   <OPERATOR type="collect">
      <INPUT name="subclass_profile_with_level.v"/>
      <INPUT name="class_profile_with_level.v"/>
      <INPUT name="dept_profile_with_level.v"/>
      <OUTPUT name="dept_cls_subcls_profile.v"/>
   </OPERATOR>
   
   
   <!--  Write the item profile data to a temporary  
         table to be used in the next RETL Flow:        -->
   
   ${DBWRITE}
      <INPUT name="item_profile.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_1}" />
      <PROPERTY name="method" value="conventional"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>
   
   <!--  Add the SIZE_GROUP1 column to the dept-class-subclass  
         profile dataset and set it to a default value:           -->
   
   <OPERATOR type="generator">
      <INPUT name="dept_cls_subcls_profile.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE_GROUP1" type="string" maxlength="25">
               <CONST value="X"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="dcs_profile_with_sizegp.v"/>
   </OPERATOR>
   
   <!--  Change the column names from ITEM to STYLE and from DIFFn to SIZEn
         and drop the diff_type fields in the dept-class-subclass dataset:   -->
   
   <OPERATOR type="fieldmod">
      <INPUT name="dcs_profile_with_sizegp.v"/>
      <PROPERTY name="rename" value="STYLE=ITEM"/>
      <PROPERTY name="rename" value="SIZE1=DIFF1"/>
      <PROPERTY name="rename" value="SIZE2=DIFF2"/>
      <PROPERTY name="rename" value="SIZE3=DIFF3"/>
      <PROPERTY name="rename" value="SIZE4=DIFF4"/>
      <PROPERTY name="drop" value="DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="DIFF_TYPE_4"/>
      <OUTPUT name="dcs_profile.v"/>
   </OPERATOR>
   
   <!--  **************************************************************  -->
   <!--  *******  Write the dept-class-subclass profile data    *******  -->
   <!--  *******  into a temporary table to be merged into the  *******  -->
   <!--  *******  ALC_SIZE_PROFILE table after the RETL scripts  ******  -->
   <!--  *******  have finished:                                 ******  -->
   <!--  **************************************************************  --> 
   
   ${DBWRITE}
      <INPUT name="dcs_profile.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_3}" />
      <PROPERTY name="method" value="conventional"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>
   
</FLOW>
EOF

############################################
#  Run the first RETL flow:
############################################

message "Running the first RETL flow (A) - Thread number: $THREAD"

${RFX_EXE} $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_A_$THREAD.xml

rfx_stat=$?

message "Completed execution of 1st RETL flow (A). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "First RETL flow (A) failed - check $ERR_FILE"

##################################################################
#  Copy the second RETL flow to a file for later execution:
##################################################################

cat > $LOG_DIR/${PROGRAM_NAME}_B_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_B.flw">

   <!--  ****************************************************************  -->
   <!--  Join the DIFF_TYPE column from the DIFF_GROUP_HEAD table with     -->
   <!--  the TEMP_TABLE_1 table containing the Diff Profile data (from     -->
   <!--  the previous RETL Flow). Join only for rows where ITEM in         -->
   <!--  TEMP_TABLE_1 is also found in ITEM_MASTER in approved status:     -->
   <!--  ****************************************************************  -->

   ${DBREAD}
      <PROPERTY name = "query">
          <![CDATA[
                    select dpf.DEPT,
                           dpf.CLASS,
                           dpf.SUBCLASS,
                           dpf.ITEM,
                           dpf.LOC,
                           dpf.DIFF_TYPE_1,
                           dpf.DIFF1,
                           dpf.DIFF_TYPE_2,
                           dpf.DIFF2,
                           dpf.DIFF_TYPE_3,
                           dpf.DIFF3,
                           dpf.DIFF_TYPE_4,
                           dpf.DIFF4,
                           dpf.QTY,
                           d1.diff_type IM_DIFF_TYPE_1,
                           d2.diff_type IM_DIFF_TYPE_2,
                           d3.diff_type IM_DIFF_TYPE_3,
                           d4.diff_type IM_DIFF_TYPE_4
                    from diff_group_head d1,
                         diff_group_head d2,
                         diff_group_head d3,
                         diff_group_head d4,
                         item_master im,
                         $TEMP_TABLE_1 dpf
                    where im.diff_1 = d1.diff_group_id (+)
                    and   im.diff_2 = d2.diff_group_id (+)
                    and   im.diff_3 = d3.diff_group_id (+)
                    and   im.diff_4 = d4.diff_group_id (+)
                    and   dpf.ITEM = im.ITEM
                    and   im.status = 'A'
          ]]>
      </PROPERTY>
      <OUTPUT name="item_profile_with_dgh_diff_type.v"/>
   </OPERATOR>

   <!--  Add the SIZE_GROUP1 column to the item profile 
         dataset and set it to a default value:            -->
   
   <OPERATOR type="generator">
      <INPUT name="item_profile_with_dgh_diff_type.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE_GROUP1" type="string" maxlength="25">
               <CONST value="X"/>
            </FIELD>
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="item_profile_with_sizegp.v"/>
   </OPERATOR>
   
   <!--  Add SIZE fields to item profile dataset:  -->
   
   <OPERATOR type="generator">
      <INPUT name="item_profile_with_sizegp.v"/>
      <PROPERTY  name="schema"> <![CDATA[
         <GENERATE>
            <FIELD name="SIZE1" type="string" maxlength="10" >
               <CONST value=""/>
            </FIELD>            
            <FIELD name="SIZE2" type="string" maxlength="10" >
               <CONST value=""/>
            </FIELD>
            <FIELD name="SIZE3" type="string" maxlength="10" >
               <CONST value=""/>
            </FIELD>  
            <FIELD name="SIZE4" type="string" maxlength="10" >
               <CONST value=""/>
            </FIELD>  
         </GENERATE> ]]>
      </PROPERTY>
      <OUTPUT name="item_profile_with_size.v"/>
   </OPERATOR>
   
   <!--  Place the diffs in the correct SIZE field  
         in the item profile dataset:                    -->
  
   <OPERATOR type="parser">
      <INPUT name="item_profile_with_size.v"/>
      <PROPERTY name = "expression">  
        <![CDATA[
        if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_1)  {
          RECORD.SIZE1 = RECORD.DIFF1;
        }
        else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_2)  {
          RECORD.SIZE2 = RECORD.DIFF1;
        }
        else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_3)  {
          RECORD.SIZE3 = RECORD.DIFF1;
        }
        else if(RECORD.DIFF_TYPE_1 == RECORD.IM_DIFF_TYPE_4)  {
          RECORD.SIZE4 = RECORD.DIFF1;
        }
        else { RECORD.SIZE1 = RECORD.DIFF1; }
         
        if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_1)  {
          RECORD.SIZE1 = RECORD.DIFF2;
        }
        else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_2)  {
          RECORD.SIZE2 = RECORD.DIFF2;
        }
        else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_3)  {
          RECORD.SIZE3 = RECORD.DIFF2;
        }
        else if(RECORD.DIFF_TYPE_2 == RECORD.IM_DIFF_TYPE_4)  {
          RECORD.SIZE4 = RECORD.DIFF2;
        }
        else { RECORD.SIZE2 = RECORD.DIFF2; }
        
        if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_1)  {
          RECORD.SIZE1 = RECORD.DIFF3;
        }
        else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_2)  {
          RECORD.SIZE2 = RECORD.DIFF3;
        }
        else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_3)  {
          RECORD.SIZE3 = RECORD.DIFF3;
        }
        else if(RECORD.DIFF_TYPE_3 == RECORD.IM_DIFF_TYPE_4)  {
          RECORD.SIZE4 = RECORD.DIFF3;
        }
        else { RECORD.SIZE3 = RECORD.DIFF3; }
        if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_1)  {
     RECORD.SIZE1 = RECORD.DIFF4;
  }
  else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_2)  {
    RECORD.SIZE2 = RECORD.DIFF4;
  }
  else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_3)  {
    RECORD.SIZE3 = RECORD.DIFF4;
  }
  else if(RECORD.DIFF_TYPE_4 == RECORD.IM_DIFF_TYPE_4)  {
    RECORD.SIZE4 = RECORD.DIFF4;
  }
  else { RECORD.SIZE4 = RECORD.DIFF4; }
        ]]>
      </PROPERTY>
      <OUTPUT name="item_profile_sizes_in_correct_order.v"/>
   </OPERATOR>
   
   <!--  Change the name of the ITEM column to STYLE and drop
         the DIFFn, DIFF_TYPE_n and IM_DIFF_TYPE_n fields in
         the item profile dataset:                                -->
   
   <OPERATOR type="fieldmod">
      <INPUT name="item_profile_sizes_in_correct_order.v"/>
      <PROPERTY name="rename" value="STYLE=ITEM"/>
      <PROPERTY name="drop" value="SIZE1"/>
      <PROPERTY name="drop" value="SIZE2"/>
      <PROPERTY name="drop" value="SIZE3"/>
      <PROPERTY name="drop" value="SIZE4"/>
      <PROPERTY name="rename" value="SIZE1=DIFF1"/>
      <PROPERTY name="rename" value="SIZE2=DIFF2"/>
      <PROPERTY name="rename" value="SIZE3=DIFF3"/>
      <PROPERTY name="rename" value="SIZE4=DIFF4"/>
      <PROPERTY name="drop" value="DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="DIFF_TYPE_4"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_1"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_2"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_3"/>
      <PROPERTY name="drop" value="IM_DIFF_TYPE_4"/>
      <OUTPUT name="style_profile.v"/>
   </OPERATOR>

   <!--  **************************************************************  -->
   <!--  *******  Write the style (item) profile data into      *******  -->
   <!--  *******  a temporary table to be merged into the       *******  -->
   <!--  *******  ALC_SIZE_PROFILE table later:                 *******  -->
   <!--  **************************************************************  --> 
     
   ${DBWRITE}
     <INPUT name="style_profile.v"/>
      <PROPERTY name="tablename" value="${TEMP_TABLE_2}" />
      <PROPERTY name="method" value="direct"/>
      <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>

</FLOW>
EOF

############################################
#  Run the second RETL flow:
############################################

message "Running the second RETL flow (B) - Thread number: $THREAD"

${RFX_EXE} $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_B_$THREAD.xml

rfx_stat=$?

message "Completed execution of 2nd RETL flow (B). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "Second RETL flow (B) failed - check $ERR_FILE"

# NOTE: Bookmark could be inserted here

##################################################################
#  Copy the Third RETL flow to a file for later execution:
##################################################################

cat > $LOG_DIR/${PROGRAM_NAME}_C_${THREAD}.xml << EOF

<FLOW name="${PROGRAM_NAME}_C.flw">

${DBREAD}
         <PROPERTY name = "query">
             <![CDATA[
         select temp2.LOC loc,temp2.DEPT dept,
       temp2.CLASS class,
       temp2.SUBCLASS subclass,
       temp2.Style style,
       temp2.size1 size1,
       temp2.size2 size2,
       temp2.size3 size3,
       temp2.size4 size4,
       temp2.size_group1 size_group1,
       temp2.size_group2 size_group2,
       temp2.qty qty,
       decode(
              decode (im.diff_1_aggregate_ind, 'Y',  decode(temp2.size1, null, 1, 0), 0) +
              decode (im.diff_2_aggregate_ind, 'Y',  decode(temp2.size2, null, 1, 0), 0) +
              decode (im.diff_3_aggregate_ind, 'Y',  decode(temp2.size3, null, 1, 0), 0) +
              decode (im.diff_4_aggregate_ind, 'Y',  decode(temp2.size4, null, 1, 0), 0),
              0,
              5,
              4) size_profile_level
       
    from    
       item_master im,
       $TEMP_TABLE_2 temp2
    where    
             nvl(temp2.size1,' ') = nvl(im.diff_1,' ') 
       and   nvl(temp2.size2,' ') = nvl(im.diff_2,' ')
       and   nvl(temp2.size3,' ') = nvl(im.diff_3,' ')
       and   nvl(temp2.size4,' ') =  nvl(im.diff_4,' ')
       and   temp2.style = im.ITEM_parent
               ]]>
         </PROPERTY>
         <OUTPUT name="item_profile_with_only_valid_records.v"/>
   </OPERATOR>

   ${DBWRITE}
        <INPUT name="item_profile_with_only_valid_records.v"/>
         <PROPERTY name="tablename" value="${TEMP_TABLE_3}" />
         <PROPERTY name="method" value="direct"/>
         <PROPERTY name="outputdelimiter" value="0x02" />
   </OPERATOR>

</FLOW>
EOF

############################################
#  Run the Third RETL flow:
############################################

message "Running the Third RETL flow (C) - Thread number: $THREAD"

${RFX_EXE} $RFX_OPTIONS -f $LOG_DIR/${PROGRAM_NAME}_C_$THREAD.xml

rfx_stat=$?

message "Completed execution of 3rd RETL flow (C). Exit status: $rfx_stat"

############################################
#  Check for errors:
############################################

checkerror -e $rfx_stat -m "Third RETL flow (C) failed - check $ERR_FILE"

# NOTE: Bookmark could be inserted here

############################################
#  Create an INDEX for TEMP_TABLE_3 table:
############################################

message "Building the ${TEMP_TABLE_3}_i1 INDEX for table ${TEMP_TABLE_3}"

.  ${LIB_DIR}/alc_create_idx.ksh "${TEMP_TABLE_3}_i1" "${TEMP_TABLE_3}"     \
      "${INDEX_KEYS}"

############################################
#  Analyze the TEMP_TABLE_3 table:
############################################

message "Analyzing temp table $ALC_OWNER.${TEMP_TABLE_3}"

analyze_tbl $ALC_OWNER.${TEMP_TABLE_3}

###############################################################
#  Create the SEQUENCE needed in the MERGE INTO that follows:
###############################################################

#  Check to see if the SEQUENCE already exists:

seq=`. ${LIB_DIR}/alc_check_seq.ksh  alc_size_profile_seq  $DBNAME`

if [ $seq -eq 0 ]; then  #  If it does not exist, create it:

  message "Creating the SEQUENCE alc_size_profile_seq to be used in the MERGE INTO the $TARGET_TABLE table."

  create_seq="CREATE SEQUENCE alc_size_profile_seq INCREMENT BY 1 START WITH 1000000"

  query_db  "$DBNAME" "$create_seq"

fi

#  Check to see if the SEQUENCE already exists:

seq=`. ${LIB_DIR}/alc_check_seq.ksh  alc_gid_profile_seq  $DBNAME`

if [ $seq -eq 0 ]; then  #  If it does not exist, create it:

  message "Creating the SEQUENCE alc_gid_profile_seq to be used in the GID_PROFILE_ID in ALC_GID_PROFILE"

  create_seq="CREATE SEQUENCE alc_gid_profile_seq INCREMENT BY 1 START WITH 1000000"

  query_db  "$DBNAME" "$create_seq"

fi

#  Check to see if the SEQUENCE already exists:

seq=`. ${LIB_DIR}/alc_check_seq.ksh  alc_gid_header_seq  $DBNAME`

if [ $seq -eq 0 ]; then  #  If it does not exist, create it:

  message "Creating the SEQUENCE alc_gid_header_seq to be used in the ID in ALC_GID_HEADER"

  create_seq="CREATE SEQUENCE alc_gid_header_seq INCREMENT BY 1 START WITH 1000000"

  query_db  "$DBNAME" "$create_seq"

fi

#  NOTE: Bookmark could be inserted here

#########################################################################
#  Check if GID exist on table
#########################################################################
#Look for file
if [ -f ${DATA_DIR}/${SPO_GID_FILENAME} -a  -s ${DATA_DIR}/${SPO_GID_FILENAME} ]; then

  GID=`head -1 ${DATA_DIR}/${SPO_GID_FILENAME}`
  GID=$(echo ${GID} | sed -e 's/\r//g')
  GID_DESC=`head -2 ${DATA_DIR}/${SPO_GID_FILENAME}| tail -1`
  GID_DESC=$(echo ${GID_DESC} | sed -e 's/\r//g')
# file exist load record insert or update if it exist on header and profile table
  export EXIST_QUERY="
      SELECT 'X'
        FROM ALC_GID_HEADER
       WHERE GID ='${GID}'
  "
  
  GID_EXIST=`query_db "${DBNAME}" "${EXIST_QUERY}"`
  if [ "$( echo ${GID_EXIST} | sed -e 's/ //g')" = "X" ]; then
  
    #get GID_PROFILE_ID
    export GID_PROFILE_ID_STATEMENT="
        SELECT GID_PROFILE_ID
          FROM ALC_GID_PROFILE
         WHERE GID_ID = (SELECT ID FROM ALC_GID_HEADER WHERE GID = '${GID}')
    "
    GID_PROFILE_ID_TEMP=`query_db "${DBNAME}" "${GID_PROFILE_ID_STATEMENT}"`
    
    export DB_STATEMENT="
        UPDATE ALC_GID_HEADER
           SET GID_DESC = '${GID_DESC}'
             , LAST_UPDATED_BY = USER
             , LAST_UPDATE_DATE =sysdate
             , LAST_UPDATE_LOGIN =USER
         WHERE GID = '${GID}';
         "

  else
    # Record does not exist.. insert
    export GID_ID_STATEMENT="
        SELECT alc_gid_header_seq.nextval from dual
    "
    GID_ID=`query_db "${DBNAME}" "${GID_ID_STATEMENT}"`
    export GID_PROFILE_ID_STATEMENT="
        SELECT alc_gid_profile_seq.nextval from dual
    "
    GID_PROFILE_ID_TEMP=`query_db "${DBNAME}" "${GID_PROFILE_ID_STATEMENT}"`
    export DB_STATEMENT="
          INSERT INTO ALC_GID_HEADER
           (ID,GID,GID_DESC,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER)
          VALUES
           (${GID_ID},'${GID}','${GID_DESC}',USER,sysdate,USER,sysdate,user,1);
          INSERT INTO ALC_GID_PROFILE
            (GID_ID, GID_PROFILE_ID,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN,OBJECT_VERSION_NUMBER)
          VALUES
          (${GID_ID},${GID_PROFILE_ID_TEMP},USER,sysdate,USER,sysdate,user,1);
      "
    
  fi
  GID_PROFILE_ID=${GID_PROFILE_ID_TEMP}
else
  GID_PROFILE_ID=null
fi
# GID Tables sql




# BEGIN
  export DB_START="
      BEGIN
      "  

# END
  export DB_END="
      END;
      "  

# get the gid_profile_id for loading into size_profile table. If GID file does not exist value is null

#########################################################################
#  Build the sql "MERGE INTO" command to merge the dept-class-subclass
#  profile data and the item (style) profile data (which is now in 
#  TEMP_TABLE_3) into the ALC_SIZE_PROFILE table:
#########################################################################
#          --  NOTE : each row returns true if there are nulls since MERGE
#          --  acts like an innerjoin and will not match records on fields
#          --  that are null

export MERGE_STATEMENT="
    MERGE /*+ APPEND index(asps, ${TEMP_TABLE_3}_i1) index(aspt, alc_size_profile_i1) */
    INTO ${TARGET_TABLE} aspt
    USING ${TEMP_TABLE_3} asps
    ON ( aspt.loc  = asps.loc
    AND ( (aspt.dept = asps.dept) OR (asps.dept IS NULL AND aspt.dept IS NULL) )
    AND ( (aspt.class = asps.class) OR (asps.class IS NULL AND aspt.class IS NULL) )
    AND ( (aspt.subclass = asps.subclass) OR (asps.subclass IS NULL AND aspt.subclass IS NULL) )
    AND ( (aspt.style = asps.style) OR (asps.style IS NULL AND aspt.style IS NULL) )
    AND ( (aspt.size1 = asps.size1) OR (asps.size1 IS NULL AND aspt.size1 IS NULL) )
    AND ( (aspt.size2 = asps.size2) OR (asps.size2 IS NULL AND aspt.size2 IS NULL) )
    AND ( (aspt.size3 = asps.size3) OR (asps.size3 IS NULL AND aspt.size3 IS NULL) )
    AND ( (aspt.size4 = asps.size4) OR (asps.size4 IS NULL AND aspt.size4 IS NULL) )
       )
    WHEN MATCHED THEN UPDATE
       SET qty = asps.qty
          ,last_updated_by = USER
          ,last_update_date= sysdate
          ,last_update_login=USER
          ,size_profile_level=asps.size_profile_level
    WHEN NOT MATCHED THEN
       INSERT
         ( size_profile_id, loc, dept, class,
           subclass, style, size_profile_level, size1, size2, size3,
           size4, size_group1, size_group2, qty,
           created_by, creation_date,last_updated_by, last_update_date,last_update_login, object_version_number, gid_profile_id  )
       VALUES( alc_size_profile_seq.nextval, asps.loc, asps.dept, asps.class,
               asps.subclass, asps.style, asps.size_profile_level, asps.size1, asps.size2, asps.size3,
               asps.size4, asps.size_group1, asps.size_group2, asps.qty,
               USER,sysdate,USER,sysdate,USER,1,${GID_PROFILE_ID});
     commit;
"

#########################################################################
#  Perform the data base "MERGE INTO" command that has just been built:
#########################################################################

RUN_SQL="${DB_START} ${DB_STATEMENT} ${MERGE_STATEMENT} ${DB_END}"

message "Merging the new size profile data into ${TARGET_TABLE}"

query_db "${DBNAME}" "${RUN_SQL}"

message "Program completed thread ${THREAD} successfully."

############################################
#  Remove the status file:
############################################

rm  ${STATUS_FILE}

#  Terminate successfully:

rmse_terminate 0
