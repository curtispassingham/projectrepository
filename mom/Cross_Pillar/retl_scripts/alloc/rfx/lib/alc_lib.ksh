#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_lib.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

. ${LIB_DIR}/alc_analyze_tbl.ksh
. ${LIB_DIR}/alc_error.ksh
. ${LIB_DIR}/alc_log_num_recs.ksh
. ${LIB_DIR}/alc_message.ksh
. ${LIB_DIR}/alc_query_db.ksh
