#!/bin/ksh

########################################################
# Copyright (c) 2005, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_check_seq.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

#####################################################################
#
#  check_seq.ksh prints a non-zero value if the given Oracle
#                SEQUENCE_NAME exists; else it prints a zero.
#
#  Usage: check_seq  sequence_name  [dbname]
#
#  $1 = sequence_name
#
#  $2 = dbname (default is $DBNAME)
#
#####################################################################

tst_flg=0
  
sequence_name=`echo $1  |  nawk '{print toupper($0)}'`

if [ "$2" != "" ]; then  dbname=$2
else  dbname=$DBNAME
fi
  
if [ $tst_flg -eq 1 ]; then
  echo
  echo  dbname = $dbname
  echo  sequence_name = $sequence_name
  echo  
fi
 
$SQLPLUS_EXE $SQLPLUS_OPTIONS $SQLPLUS_LOGON<< EOF
  WHENEVER SQLERROR EXIT 4;
  set head off
  select count(*) from ALL_SEQUENCES where SEQUENCE_NAME = '$sequence_name';
EOF

exit_stat=$?
  
if [ $tst_flg -eq 1 ]; then  
  echo
  if [ $exit_stat -ne 0 ]; then 
    echo Sequence check on Database $dbname FAILED. Sequence name: $sequence_name
    echo
  fi
  echo "check_seq exit status: $exit_stat"
  echo
fi
  
checkerror -e $exit_stat -m "Sequence check on Database $DBNAME FAILED. Sequence name: $sequence_name"
