#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_do_cleanup.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

##############################################################################
#
#  Usage: do_cleanup  $1  $2
#
#  $1 = Databse name
#  $2 = Table name
#
##############################################################################

#  Override the normal do_cleanup function for this script:

function do_cleanup {
   #  Remove temp table if necessary:
   
   query_db "${DBNAME}" "DROP TABLE ${TEMP_TABLE}"
   
   ## echo "Cleaning up"
}
