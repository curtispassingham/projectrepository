#!/bin/ksh

########################################################
# Copyright (c) 2002, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_query_db.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

#####################################################################
#
#  query_db function
#
#  Usage: query_db  database_name  sqlplus_command_string  [error_flag]
#
#  $1 = database_name
#  $2 = sqlplus_command_string
#  $3 = error_flag (optional)
#
#  If error_flag is present and is a "0", no error will be 
#  reported when the table does not exist.
#
#####################################################################

function query_db
{
  DBNAME=$1
  QUERY=$2
  error_flag=$3

  if [[ $DB_ENV = "DB2" ]]; then  #  DB2 Data Base:
    exit_stat=-1
  elif [[ $DB_ENV = "TER" ]]; then  #  TeraData Data Base:
    exit_stat=-1
    
  elif [[ $DB_ENV = "ORA" ]]; then  #  Oracle Data Base:
 
    $SQLPLUS_EXE $SQLPLUS_OPTIONS $SQLPLUS_LOGON << EOF > $TEMP_DIR/query_db_out
    set head off
    set echo off
    set feed off
    set auto off
    WHENEVER SQLERROR EXIT 4;
    $QUERY
    /
EOF
    exit_stat=$?
    
    #  If $error_flag is a zero, don't error if object does not exist:
    
    dne=0
    if [ "$error_flag" = "0" ]; then
      dne=`grep -c "does not exist"  $TEMP_DIR/query_db_out`
      if [ $dne -gt 0 ]; then  exit_stat=0; fi
    fi
  fi
  
  #  Only print out the sqlplus output if error_flag is not zero
  #  or if there was no "does not exist" type of error:
  
  if [ $dne -eq 0 ]; then  cat  $TEMP_DIR/query_db_out; fi
  
  if [ -f  $TEMP_DIR/query_db_out ]; then rm $TEMP_DIR/query_db_out; fi
  
  ##  message "query_db exit status: $exit_stat"
  checkerror -e $exit_stat -m "Query on Database $DBNAME FAILED. Query was: \"$QUERY\""
}
