#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_log_num_recs.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

##############################################################################
#
#  This Function logs the number of records in the given file.
#
#  Usage: log_num_recs  $1
#
#  $1 = File name
#
##############################################################################

function log_num_recs 
{
   if [[ -f $1 ]]
   then 
      NUM_RECS=`wc -l  $1  |  awk '{print $1; exit}'`
   else
      NUM_RECS=0
   fi
   message "Number of records in $1 = ${NUM_RECS}"
}
