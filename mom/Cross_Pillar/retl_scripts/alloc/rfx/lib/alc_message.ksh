#!/bin/ksh

########################################################
# Copyright (c) 2001, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_message.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

##############################################################################
#
#  This Function to produce message text with program name and timestamp
#
#  Usage: message  "Message text"
#
#  $1 = Message text
#
##############################################################################

function message 
{
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $LOG_FILE 
   
   echo "\n-----------------------------"  >>  $ERR_FILE
   echo "$PROGRAM_NAME "`date +"%T"`": $1"  >> $ERR_FILE
   echo "-----------------------------"  >>    $ERR_FILE
}
