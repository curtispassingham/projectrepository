#!/bin/ksh

########################################################
# Copyright (c) 2002, Retek Inc.  All rights reserved.
# $HeadURL: svn+ssh://scmadm@rgbusvn.us.oracle.com/svn/rgbuprod/rms/releases/RMS_16_0_1_RC1/Cross_Pillar/retl_scripts/alloc/rfx/lib/alc_analyze_tbl.ksh $
# $Revision: 1149527 $
# $Date: 2015-12-16 18:56:22 -0600 (Wed, 16 Dec 2015) $
########################################################

#####################################################################
#
#  Analyze table function
#
#  Usage: analyze_tbl  $1  
#
#  $1 = $TABLE_NAME
#
#  Where: TABLE_NAME = Table to Analyze
#
#####################################################################

function analyze_tbl
{
if [[ $DB_ENV = "DB2" ]]

then  #  DB2 Data Base:

   db2 "connect to $DBNAME user $ALC_OWNER using $PASSWORD"
   db2 "runstats on table $1 and detailed indexes all"
   checkerror -e $? -d -m "Can't analyze table $1"
   db2 "terminate" 
   
elif [[ $DB_ENV = "ORA" ]]

then  #  Oracle Data Base:

   if [ $# -ge 2 ]; then
   
   # Assume the Table Owner is in $1 and the Table Name is in $2:
   
     TBL_OWNER=$1
     TBL_NAME=$2
     
   else
   
   #  TABLE NAME is in $1:
   
     TBL_NAME=`echo $1`
   fi
   
   echo "oracle alias $ORACLE_WALLET_ALIAS"
   $SQLPLUS_EXE $SQLPLUS_OPTIONS $SQLPLUS_LOGON << EOF
     WHENEVER SQLERROR EXIT 4;
     BEGIN
        sys.dbms_stats.gather_table_stats('${TBL_NAME}');
     END;
   exit;
EOF

fi
   
checkerror -e $? -m "Analyzing table $1 failed"

}
