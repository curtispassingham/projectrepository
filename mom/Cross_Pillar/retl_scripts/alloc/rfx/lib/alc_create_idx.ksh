#!/bin/ksh

##############################################################################
#
#  This script creates a Database Table Index. 
#
#  Usage: create_idx.ksh  indexName  table_name  columns_to_index
#
#          $1 = Index Name
#          $2 = Table Name
#          $3 = Columns to be indexed (eg. SKU_KEY, LOC_KEY ...)
#
#  Adapted from: /projects/ofdrdw10.2/dev/rdw10.2/dev/rfx/lib/create_idx.ksh
#
#  May 09  2005  15:55:41 CDT
#
##############################################################################

if [[ $DB_ENV = "DB2" ]]
then
   db2 "connect to $DBNAME user $ALC_OWNER using $PASSWORD " 
   db2 "CREATE INDEX $1 on $2 ($3)" 
   checkerror -e $? -m "Building index on table $2 failed"
   db2 "terminate" 
   
elif [[ $DB_ENV = "ORA" ]]
then
   $SQLPLUS_EXE $SQLPLUS_OPTIONS $SQLPLUS_LOGON << EOF  
     WHENEVER SQLERROR EXIT 4;
     CREATE INDEX $1 on $2 ($3);
   exit;  
EOF
   
elif [[ $DB_ENV = "TER" ]]
then
   bteq .logon $DBNAME/$ALC_OWNER,$PASSWORD << EOF 
     CREATE INDEX $1 ($3) on $2;
   .exit
EOF

fi

checkerror -e $? -m "Build index on table $2 failed"
