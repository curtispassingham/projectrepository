#! /bin/ksh

#--------------------------------------------------------------------------------------------------
# File: batch_wrapper.ksh
# Desc: This file is a wrapper script to call batches which takes $UP as input parameter
#--------------------------------------------------------------------------------------------------

# Common functions library and environment variables
. ${MMHOME}/oracle/lib/src/batch_wrapper.lib
. ${MMHOME}/oracle/lib/src/rmsksh.lib


pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

errorFile="${MMHOME}/error/err.${pgmName}.${exeDate}"
logFile="${MMHOME}/log/${exeDate}.log"

#------------------------------------------------------------------------------------------------------------------
# Function Name: USAGE
# Purpose      : Defines how the program should be invoked
#------------------------------------------------------------------------------------------------------------------
function USAGE
{
   echo "USAGE: `basename $0` <batch name> <connect string> <Optional - Job Specific Parameter>

   <batch name>                             Batch to be run.
   
   <connect string>                         Username/password@db. Use "'$UP'" if using Oracle Wallet.

   <Optional - Job Specific Parameter>      Optional - Additional Job Specific Parameter."
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [[ $# -lt 2 ]]; then
   USAGE
   exit ${NON_FATAL}
fi

#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$2
batchName=$1
batchPath="${rmsBinDir}/${batchName}"

if [[ -n ${connectStr%/*} ]]; then
   USER=${connectStr%/*}
else
   USER="default user"
fi

#Calling the function from batch_wrapper.lib to check the existence and permission of directories
checkDirectories
if [ $? -ne 0 ]; then
   LOG_ERROR "Failed while checking for the existence and permission of directories." "checkDirectories" ${FATAL} ${errorFile} ${logFile} ${pgmName} 
   exit ${FATAL}
fi

LOG_MESSAGE "Started by ${USER}" "" "" ${logFile} ${pgmName} ${pgmPID}
# Check the connection status
conncheck=`echo "exit" | ${ORACLE_HOME}/bin/sqlplus -s -l ${connectStr}`
con_status=$?
if [[ ${con_status} -ne ${OK} ]]; then
   LOG_ERROR "Connection failed Username/Password is invalid" "CONNECTION_CHECK" "${con_status}" "${errorFile}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
else
   LOG_MESSAGE "Connection established successfully." "" "${OK}" "${logFile}" "${pgmName}" "${pgmPID}"
fi

#Calling restart recovery function
getBatchInfo ${batchName}
retVal=$?
if [ $retVal -ne 0 ]; then
   LOG_ERROR "Restart recovery not configured for the batch ${batchName}" "getBatchInfo" ${FATAL} ${errorFile} ${logFile} ${pgmName}
   exit ${FATAL}
fi

# Get available thread
getmaxAvailThread ${batchName}
threadNum=$availThread
failed=$batchName.$$

if [ $threadNum -eq 0 ]; then
   LOG_ERROR "No threads available to run ${batchName} batch." "" "" "${errorFile}" "${logFile}" "${pgmName}" "${pgmPID}"
   exit ${FATAL}
fi

# Adjust for any remaining arguments to be passed to the batch
shift 2
ADDL_OPTIONS=${1:+$@}

# Execute the batch
  while [[ ${threadNum} -ne 0 ]]
  do
    (
	 ${batchPath} ${connectStr} ${ADDL_OPTIONS}|| touch $failed;
	 )&
	threadNum=`expr $threadNum - 1`
  done

# Wait for all of the threads to complete
wait

# Check for the existence of a failed file from any of the threads
# and determine exit status
if [ -f ${failed} ]; then
   rm -f $failed
   LOG_ERROR "Thread failed with error." "${batchName}" ${FATAL} ${errorFile} ${logFile} ${pgmName}
   exit ${FATAL}
else
   LOG_MESSAGE "Program ${pgmName} terminated successfully." "" ${OK} ${logFile} ${pgmName} ${pgmPID}
   exit ${OK}
fi
