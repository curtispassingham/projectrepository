#! /bin/ksh

#-------------------------------------------------------------------------------
#  File: cfagen.ksh 
#
#  Desc: This shell script will create a file with CFA extension table, staging 
#         table, and views script.
#-------------------------------------------------------------------------------

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
stmpDate=`date`           # Today's date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
FATAL=255
NON_FATAL=1
OK=0
TRUE=1
FALSE=0


USAGE="Usage: $pgmName.$pgmExt <connect> <Level E for entity or G for group set> <Entity/Group Set ID> <Gen synonym Y/YP/N> <Drop existing STG table Y/N>"

function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE

   return $retCode
}

function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi

   return $retCode
}

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set termout off;
      set verify off;
      set echo off
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


function CHECK_ENTITY_ID
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_exist           VARCHAR2(1);
         L_error_type      VARCHAR2(255):= NULL;
      BEGIN
         if NOT CFA_SETUP_ENTITY_SQL.ENTITY_EXIST(:GV_script_error,
                                                  L_exist,
                                                  ${entityId}) then
            SQL_LIB.API_MSG(L_error_type, :GV_script_error);
            raise FUNCTION_ERROR;
         end if;

         if L_exist = 'N' then
            :GV_script_error := 'Invalid Entity ID.';
            :GV_return_code := ${NON_FATAL};
         end if;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Failed in CHECK_ENTITY_ID" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}

function CHECK_GRP_SET_ID
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR            EXCEPTION;
         L_error_type              VARCHAR2(255):= NULL;
         L_attrib_group_set        CFA_ATTRIB_GROUP_SET%ROWTYPE;
         L_attrib_group_set_label  CFA_ATTRIB_GROUP_SET_LABELS%ROWTYPE;
      BEGIN
         if NOT CFA_SETUP_GROUP_SET_SQL.GET_GRP_SET_REC(:GV_script_error,
                                                        L_attrib_group_set,
                                                        L_attrib_group_set_label,
                                                        ${grpSetId}) then
            SQL_LIB.API_MSG(L_error_type, :GV_script_error);
            raise FUNCTION_ERROR;
         end if;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Failed in CHECK_GRP_SET_ID" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function CHK_STG_EXISTS
{
   sqlTxt="
      DECLARE
         FUNCTION_ERROR            EXCEPTION;
         L_error_type              VARCHAR2(255):= NULL;
         L_ext_entity_id           CFA_EXT_ENTITY.EXT_ENTITY_ID%TYPE;
         L_group_set_id            CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
      BEGIN

         if '${entityId}' = 'ANY' then
            L_ext_entity_id := NULL;
         else
            L_ext_entity_id := to_number('${entityId}');
         end if;
         
         if '${grpSetId}' = 'ALL' then
            L_group_set_id := NULL;
         else
            L_group_set_id := to_number('${grpSetId}');
         end if;
         
         if NOT CFA_GEN_SQL.STGS_EXISTS(:GV_script_error,
                                        L_ext_entity_id,
                                        L_group_set_id) then
            SQL_LIB.API_MSG(L_error_type, :GV_script_error);
            raise FUNCTION_ERROR;
         end if;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Failed in CHK_STG_EXISTS" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function GET_TBL_OWNER
{

TBLOWNER=`sqlplus -s $connectStr  <<EOF
set pause off
set echo off
set heading off
set feedback off
set verify off
set pages 0
select table_owner
        from system_options;
exit;
EOF`

}

function GEN_EXT_TBL
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   L_exists            VARCHAR2(1);
   L_table             CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
   L_ext_table         CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE;
   L_prim_key_cols     VARCHAR2(4000);

   cursor C_EXT_TBL is
      select ent.base_rms_table,
             ent.custom_ext_table
        from cfa_ext_entity ent
       where ent.ext_entity_id = ${entityId};

   cursor C_CHECK_TABLE is
      select 'Y'
        from all_tables
       where owner  in  ('${TBLOWNER}', USER)
         and table_name = L_ext_table;

      cursor C_GET_COL_DEF is
      select a.column_name||' '||a.data_type||decode(a.data_type,'NUMBER','('||nvl(a.data_precision, 0)||')','VARCHAR2','('||a.data_length||')','DATE',NULL)|| decode (nullable,'N',' NOT NULL')||',' col,
             a.column_name
        from all_tab_columns a, all_constraints b, all_cons_columns c
       where a.table_name  = L_table
         and a.owner       = '${TBLOWNER}'
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
       order by c.position;

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   ---
   open C_EXT_TBL;
   fetch C_EXT_TBL into L_table, L_ext_table;
   close C_EXT_TBL;
   ---
   open C_CHECK_TABLE;
   fetch C_CHECK_TABLE into L_exists;
   close C_CHECK_TABLE;
   ---
   if L_exists = 'Y' then
     DBMS_OUTPUT.PUT_LINE('-- No script is genrated for the '|| L_ext_table ||' table, as it is  already exist.');
   else
     DBMS_OUTPUT.PUT_LINE('--  Creating Extension Table: '|| L_ext_table);
     DBMS_OUTPUT.PUT_LINE('PROMPT  Creating Extension Table: '|| L_ext_table); 

     DBMS_OUTPUT.PUT_LINE('create table '||L_ext_table||'( ');

     FOR i in C_GET_COL_DEF LOOP
        DBMS_OUTPUT.PUT_LINE( i.col);
        L_prim_key_cols := L_prim_key_cols||i.column_name||',';
     END LOOP;
     L_prim_key_cols := substr(L_prim_key_cols,1,length(L_prim_key_cols)-1);
     
     DBMS_OUTPUT.PUT_LINE('GROUP_ID    NUMBER(10) NOT NULL,');
     DBMS_OUTPUT.PUT_LINE('VARCHAR2_1 VARCHAR2(250),VARCHAR2_2 VARCHAR2(250),VARCHAR2_3 VARCHAR2(250),VARCHAR2_4 VARCHAR2(250),VARCHAR2_5 VARCHAR2(250),VARCHAR2_6 VARCHAR2(250),VARCHAR2_7 VARCHAR2(250),VARCHAR2_8 VARCHAR2(250),VARCHAR2_9 VARCHAR2(250),VARCHAR2_10 VARCHAR2(250),');
     DBMS_OUTPUT.PUT_LINE('NUMBER_11 NUMBER,NUMBER_12 NUMBER,NUMBER_13 NUMBER,NUMBER_14 NUMBER,NUMBER_15 NUMBER,NUMBER_16 NUMBER,NUMBER_17 NUMBER,NUMBER_18 NUMBER,NUMBER_19 NUMBER,NUMBER_20 NUMBER,');
     DBMS_OUTPUT.PUT_LINE('DATE_21 DATE, DATE_22 DATE, DATE_23 DATE, DATE_24 DATE, DATE_25 DATE);');
     
     DBMS_OUTPUT.PUT_LINE('-----------------------  ');
     DBMS_OUTPUT.PUT_LINE('--  Creating the primary key');
     DBMS_OUTPUT.PUT_LINE('alter table '||L_ext_table||' add primary key ('|| L_prim_key_cols||',GROUP_ID);');

     DBMS_OUTPUT.PUT_LINE('-----------------------  ');
     DBMS_OUTPUT.PUT_LINE('--  Creating foreign key');
     DBMS_OUTPUT.PUT_LINE('alter table '||L_ext_table||' add foreign key ('||L_prim_key_cols||')'||' REFERENCES '||L_table||'('||L_prim_key_cols||');');

     DBMS_OUTPUT.PUT_LINE('-----------------------  ');
     DBMS_OUTPUT.PUT_LINE('--  Setting the entity active');
     DBMS_OUTPUT.PUT_LINE('update cfa_ext_entity set active_ind = ''Y''');
     DBMS_OUTPUT.PUT_LINE('where ext_entity_id = ${entityId};');

     DBMS_OUTPUT.PUT_LINE('-- End of script for Extension Table: '|| L_ext_table);
   end if;
END;
/
EOF

   if [[ $? -ne ${OK} ]]; then
      echo "Failed in GEN_EXT_TBL" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function GEN_VIEW_GRP_SET
{
  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE
   ---
   L_table              CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
   L_group_set_id       CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
   L_group_set_id_list  VARCHAR2(4000) := NULL;
   L_first_column       VARCHAR2(1);
   L_first_grp          CFA_ATTRIB_GROUP.GROUP_ID%TYPE;

   cursor C_PRIM_KEY is
      select a.column_name,
             c.position
        from all_tab_columns a,
             all_constraints b,
             all_cons_columns c
       where a.table_name  = L_table
         and a.owner       = '${TBLOWNER}'
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
      UNION ALL
      select a.column_name,
             c.position
        from all_tab_columns a,
             all_constraints b,
             all_cons_columns c
       where a.table_name  = L_table
         and a.owner       = '${TBLOWNER}'
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'U'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
         and not exists (select 'x'
                           from all_tab_columns a,
                                all_constraints b,
                                all_cons_columns c
                          where a.table_name  = L_table
                            and a.owner       = '${TBLOWNER}'
                            and a.owner       = b.owner
                            and a.owner       = c.owner
                            and a.table_name  = b.table_name
                            and b.constraint_type  = 'P'
                            and b.constraint_name = c.constraint_name
                            and a.column_name = c.column_name )
   order by 2;

   cursor C_GROUP_SET is
      select ent.base_rms_table,
             ent.custom_ext_table,
             grps.group_set_view_name,
             grps.group_set_id
        from cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id  = grps.ext_entity_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}')
         and exists (select 'x'
                       from cfa_attrib_group grp,
                            cfa_attrib att
                      where att.group_id     = grp.group_id
                        and grp.group_set_id = grps.group_set_id
                        and att.active_ind   = 'N');

   cursor C_VIEW_COLS is
      select grp.group_id,
             attr.attrib_id,
             view_col_name,
             storage_col_name,
             'G'||grp.group_id||'.'||storage_col_name col
        from cfa_attrib attr,
             cfa_attrib_group grp
       where grp.group_id       = attr.group_id
         and grp.group_set_id   = L_group_set_id
    order by grp.display_seq, 
             attr.display_seq;
                         
   cursor C_GROUP is
      select grp.group_id
        from cfa_attrib_group grp
       where grp.group_set_id   = L_group_set_id
       order by grp.display_seq;



BEGIN
   dbms_output.enable(1100000);  -- allows the output to grow to 1000000 bytes
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   ---
   FOR t in C_GROUP_SET LOOP
      L_table        := t.base_rms_table;
      L_group_set_id := t.group_set_id;

      if L_group_set_id_list IS NULL then
         L_group_set_id_list := t.group_set_id;
      else
         L_group_set_id_list := L_group_set_id_list ||', '|| t.group_set_id;
      end if;
      ---
      DBMS_OUTPUT.PUT_LINE('--  Creating Group Set view: '|| t.group_set_view_name);
      DBMS_OUTPUT.PUT_LINE('PROMPT  Creating Group Set view: '|| t.group_set_view_name);
      ---
      DBMS_OUTPUT.PUT_LINE('CREATE OR REPLACE VIEW '||t.group_set_view_name||'(');
      ---
          OPEN C_GROUP;
      FETCH C_GROUP INTO L_first_grp;
      CLOSE C_GROUP;
      ---         
      L_first_column := 'Y';
      FOR i in C_PRIM_KEY LOOP
         if L_first_column = 'Y' then
            DBMS_OUTPUT.PUT_LINE(i.column_name);
            L_first_column := 'N';
         else
            DBMS_OUTPUT.PUT_LINE(','||i.column_name);
         end if;
      END LOOP;

      FOR i in C_VIEW_COLS LOOP
         DBMS_OUTPUT.PUT_LINE(','||i.view_col_name);
      END LOOP;

      DBMS_OUTPUT.PUT_LINE(' ) AS SELECT * FROM (SELECT ');
      L_first_column := 'Y';
      FOR i in C_PRIM_KEY LOOP
         if L_first_column = 'Y' then
            DBMS_OUTPUT.PUT_LINE('G'||L_first_grp||'.'||i.column_name);
            L_first_column := 'N';
         else
            DBMS_OUTPUT.PUT_LINE('      ,G'||L_first_grp||'.'||i.column_name);
         end if;
      END LOOP;

      FOR i in C_VIEW_COLS LOOP
            DBMS_OUTPUT.PUT_LINE('      ,'||i.col||' '||i.view_col_name);
      END LOOP;

           L_first_column := 'Y';
      FOR i in C_GROUP LOOP
         if L_first_column = 'Y' then
            DBMS_OUTPUT.PUT_LINE(' from '||t.custom_ext_table||' G'||i.group_id);
            L_first_column := 'N';
         else
            DBMS_OUTPUT.PUT_LINE(', '||t.custom_ext_table||' G'||i.group_id);
         end if;
      END LOOP;
      L_first_column := 'Y';
      FOR i in C_GROUP LOOP
         if L_first_column = 'Y' then
            DBMS_OUTPUT.PUT_LINE(' where G'||i.group_id||'.group_id = '||i.group_id);
            L_first_column := 'N';
         else
            DBMS_OUTPUT.PUT_LINE('and G'||i.group_id||'.group_id = '||i.group_id);
         end if;
      END LOOP;
      L_first_column := 'Y';
      FOR i in C_GROUP LOOP
         if L_first_column = 'Y' then
            L_first_column := 'N';
         else
            FOR k in C_PRIM_KEY LOOP
               DBMS_OUTPUT.PUT_LINE('and G'||L_first_grp||'.'||k.column_name||' = G'||i.group_id||'.'||k.column_name);
            END LOOP;
         end if;
      END LOOP;
      
      DBMS_OUTPUT.PUT_LINE(');');


      DBMS_OUTPUT.PUT_LINE('--  End of create Group Set view: '|| t.group_set_view_name);
   END LOOP;

   if L_group_set_id_list is not NULL then
      DBMS_OUTPUT.PUT_LINE('-----------------------  ');
      DBMS_OUTPUT.PUT_LINE('--  Setting the group set active');
      DBMS_OUTPUT.PUT_LINE('update cfa_attrib_group_set set active_ind = ''Y''');
      DBMS_OUTPUT.PUT_LINE('where group_set_id in ('||L_group_set_id_list||');');
   end if;
END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in GEN_VIEW_GRP_SET" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}


function GEN_STG_TBL
{

  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

WHENEVER SQLERROR EXIT ${FATAL}

DECLARE

   L_exists                VARCHAR2(1);
   L_stg_table             VARCHAR2(30) := NULL;
   L_base_table            VARCHAR2(30) := NULL;
   L_group_set_id          CFA_ATTRIB_GROUP_SET.GROUP_SET_ID%TYPE;
   L_error_type            VARCHAR2(255):= NULL;
   L_nullable_key_exists   BOOLEAN := FALSE;
   L_prim_cnstr            VARCHAR2(4000) := NULL;
   L_const_ctr             NUMBER := 0;
   L_vdate                 DATE  := get_vdate;
   L_lowest_allowed_value  VARCHAR2(8);
   L_highest_allowed_value VARCHAR2(8);
   
   cursor C_STG_TABLE is
      select grps.staging_table_name,
             grps.group_set_id,
             ent.base_rms_table
        from cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id  = grps.ext_entity_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}')
         and grps.staging_table_name is NOT NULL
         and exists (select 'x'
                       from cfa_attrib_group grp,
                            cfa_attrib att
                      where att.group_id     = grp.group_id
                        and grp.group_set_id = grps.group_set_id
                        and att.active_ind   = 'N');

   cursor C_CHECK_TABLE is
      select 'Y'
        from all_tables
       where owner   in  ('${TBLOWNER}', USER)
         and table_name = L_stg_table;

   --entity key columns can be PK or UK (nullable or not null columns)
   cursor C_ENTITY_KEY is
      select k.key_col column_name,
             case
                when a.data_type = 'VARCHAR2' then
                   a.data_type||'('||a.data_length||')'
                when a.data_type = 'NUMBER' then
                   decode(a.data_scale, 0, a.data_type||'('||a.data_precision||')',
                                        a.data_type||'('||a.data_precision||','||a.data_scale||')')
                when a.data_type = 'DATE' then
                   a.data_type
                else
                   a.data_type
             end column_type,
             decode(a.nullable, 'Y', NULL, 
                                'N', 'NOT NULL', NULL) nullable
        from cfa_ext_entity_key k,
             all_tab_columns a
       where k.base_rms_table = L_base_table
         and a.table_name     = k.base_rms_table
         and k.key_col        = a.column_name
         and a.owner          = '${TBLOWNER}'
    order by k.key_number;

   cursor C_ATTRIB_COLS is
      select a.view_col_name,
             a.data_type,
             a.ui_widget,
             decode(a.data_type, 'VARCHAR2', a.data_type||'('||a.maximum_length||')',
                                 'NUMBER', a.data_type||'('||a.maximum_length||')',
                                 'DATE', a.data_type,
                                 a.data_type) column_type,
             case
                when a.lowest_allowed_value is NULL then
                   NULL
                when a.data_type = 'NUMBER' then
                   to_char(a.lowest_allowed_value)
                when a.data_type = 'DATE' then
                   to_char(a.lowest_allowed_value)
             end lowest_allowed_value,
             case
                when a.highest_allowed_value is NULL then
                   NULL
                when a.data_type = 'NUMBER' then
                   to_char(a.highest_allowed_value)
                when a.data_type = 'DATE' then
                   to_char(a.highest_allowed_value)
             end highest_allowed_value
        from cfa_attrib a,
             cfa_attrib_group g
       where g.group_set_id = L_group_set_id
         and a.group_id     = g.group_id
    order by g.group_id, a.attrib_id;

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');

   FOR s IN C_STG_TABLE LOOP
      L_stg_table    := s.staging_table_name;
      L_base_table   := s.base_rms_table;
      L_group_set_id := s.group_set_id;
      L_const_ctr    := 0;
      L_exists       := 'N';
      L_prim_cnstr   := NULL;
     
      DBMS_OUTPUT.PUT_LINE('--  Creating Group set staging table: '|| L_stg_table);
      ---
      open C_CHECK_TABLE;
      fetch C_CHECK_TABLE into L_exists;
      close C_CHECK_TABLE;
      --
      if '${dropStg}' = 'Y' and
         L_exists = 'Y' then
         DBMS_OUTPUT.PUT_LINE('PROMPT  Dropping staging table: '||L_stg_table);
         DBMS_OUTPUT.PUT_LINE('DROP TABLE '||L_stg_table||';');
         DBMS_OUTPUT.PUT_LINE('-----------------------  ');
      end if;

      DBMS_OUTPUT.PUT_LINE('PROMPT  Creating staging table: '||L_stg_table);
      DBMS_OUTPUT.PUT_LINE('CREATE TABLE '||L_stg_table || ' (PROCESS_ID NUMBER(10) NOT NULL ');
       
      FOR rec IN C_ENTITY_KEY LOOP
         if rec.nullable is NULL then
            L_nullable_key_exists := TRUE;
         end if;
         DBMS_OUTPUT.PUT_LINE(', '||rec.column_name||' '||rec.column_type||' '||rec.nullable);
         L_prim_cnstr := L_prim_cnstr||', '||rec.column_name;
      END LOOP;

      FOR rec IN C_ATTRIB_COLS LOOP
         DBMS_OUTPUT.PUT_LINE(', '||rec.view_col_name||' '||rec.column_type);
      END LOOP;

      if L_nullable_key_exists = TRUE then
         -- create unique key clause if any of the entity key is nullable. No storage clause.
         DBMS_OUTPUT.PUT_LINE(', ' ||' CONSTRAINT UK_'||L_stg_table||' UNIQUE (PROCESS_ID'||L_prim_cnstr||') ');
      else
         -- create primary key clause if no entity key is nullable. Add storage clause for the PK.
         DBMS_OUTPUT.PUT_LINE(', ' ||' CONSTRAINT PK_'||L_stg_table||' PRIMARY KEY (PROCESS_ID'||L_prim_cnstr||') ');
      end if;

      FOR rec IN C_ATTRIB_COLS LOOP
         if rec.lowest_allowed_value is NOT NULL and
            rec.highest_allowed_value is NOT NULL then 
            if  rec.data_type = 'DATE' then
               L_lowest_allowed_value := to_char(L_vdate+rec.lowest_allowed_value,'YYYYMMDD');
               L_highest_allowed_value := to_char(L_vdate+rec.highest_allowed_value,'YYYYMMDD');
            end if;

            L_const_ctr := L_const_ctr + 1;
            if  rec.data_type = 'DATE' then
               DBMS_OUTPUT.PUT_LINE(', '||'CONSTRAINT CHK_'||L_stg_table||L_const_ctr||' CHECK ('||rec.view_col_name||' BETWEEN to_date('||q'[']'||L_lowest_allowed_value||q'[','YYYYMMDD')]'||' and to_date('||q'[']'||L_highest_allowed_value||q'[','YYYYMMDD')]'||') ENABLE');
            else
               DBMS_OUTPUT.PUT_LINE(', '||'CONSTRAINT CHK_'||L_stg_table||L_const_ctr||' CHECK ('||rec.view_col_name||' BETWEEN '||rec.lowest_allowed_value||' and '||rec.highest_allowed_value||') ENABLE');
            end if ;

         elsif rec.lowest_allowed_value is NOT NULL then
            L_const_ctr := L_const_ctr + 1;
            DBMS_OUTPUT.PUT_LINE(', '||'CONSTRAINT CHK_'||L_stg_table||L_const_ctr||' CHECK ('||rec.view_col_name||' >= '||rec.lowest_allowed_value||') ENABLE'); 

         elsif rec.highest_allowed_value is NOT NULL then
            L_const_ctr := L_const_ctr + 1;
            DBMS_OUTPUT.PUT_LINE(', '||'CONSTRAINT CHK_'||L_stg_table||L_const_ctr||' CHECK ('||rec.view_col_name||' <= '||rec.highest_allowed_value||') ENABLE');
         end if;

         if rec.ui_widget = 'CB' then
            L_const_ctr := L_const_ctr + 1;
            DBMS_OUTPUT.PUT_LINE(', '||'CONSTRAINT CHK_'||L_stg_table||L_const_ctr||' CHECK ('||rec.view_col_name||' in (''Y'',''N'')) ENABLE');
         end if;
      END LOOP;

      DBMS_OUTPUT.PUT_LINE(' );');
      ---
      DBMS_OUTPUT.PUT_LINE('--  End of script for Group set staging table: '|| L_stg_table);
   END LOOP;
END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in GEN_STG_TBL" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi

}


function GEN_VIEW_GRP
{

  sqlplus -s $connectStr  <<EOF>> $fileName
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   L_first_column       VARCHAR2(1);
   L_prim_col           VARCHAR2(4000);
   L_column_list        VARCHAR2(4000);
   L_grp_id_list        VARCHAR2(4000) := NULL;
   L_attr_id_list       VARCHAR2(4000) := NULL;
   L_table              CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
   L_group_id           CFA_ATTRIB_GROUP.GROUP_ID%TYPE;

   cursor C_GROUPS is
      select ent.base_rms_table,
             ent.custom_ext_table,
             grp.group_view_name,
             grp.group_id
        from cfa_attrib_group grp,
             cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id = grps.ext_entity_id
         and grp.group_set_id  = grps.group_set_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}')
         and exists (select 'x'
                       from cfa_attrib att
                      where att.group_id   = grp.group_id
                        and att.active_ind = 'N');

   cursor C_PRIM_KEY is
      select a.column_name,
             c.position
        from all_tab_columns a,
             all_constraints b,
             all_cons_columns c
       where a.table_name  = L_table
         and a.owner       = '${TBLOWNER}'
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'P'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
      UNION ALL
      select a.column_name,
             c.position
        from all_tab_columns a,
             all_constraints b,
             all_cons_columns c
       where a.table_name  = L_table
         and a.owner       = '${TBLOWNER}'
         and a.owner       = b.owner
         and a.owner       = c.owner
         and a.table_name  = b.table_name
         and b.constraint_type  = 'U'
         and b.constraint_name = c.constraint_name
         and a.column_name = c.column_name
         and not exists (select 'x'
                           from all_tab_columns a,
                                all_constraints b,
                                all_cons_columns c
                          where a.table_name  = L_table
                            and a.owner       = '${TBLOWNER}'
                            and a.owner       = b.owner
                            and a.owner       = c.owner
                            and a.table_name  = b.table_name
                            and b.constraint_type  = 'P'
                            and b.constraint_name = c.constraint_name
                            and a.column_name = c.column_name )
   order by 2;

   cursor C_VIEW_COLS is
      select grp.group_id,
             attr.attrib_id,
             view_col_name,
             storage_col_name
        from cfa_attrib attr,
             cfa_attrib_group grp
       where grp.group_id = attr.group_id
         and grp.group_id = L_group_id;
BEGIN
   ---
   DBMS_OUTPUT.ENABLE(1100000);
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');

   L_first_column := 'Y';
   FOR i in C_PRIM_KEY LOOP
      if L_first_column = 'Y' then
         L_prim_col := i.column_name;
         L_first_column := 'N';
      else
         L_prim_col := L_prim_col||','||i.column_name;
      end if;
   END LOOP;

   FOR v in C_GROUPS LOOP
      L_table := v.base_rms_table;
      L_group_id := v.group_id;
      L_column_list := NULL;

      if L_grp_id_list IS NULL then
         L_grp_id_list := v.group_id;
      else
         L_grp_id_list := L_grp_id_list||', '||v.group_id;
      end if;
      ---
      DBMS_OUTPUT.PUT_LINE('--  Creating Group view: '|| v.group_view_name);
      DBMS_OUTPUT.PUT_LINE('PROMPT  Creating Group view: '|| v.group_view_name);
      ---
      DBMS_OUTPUT.PUT_LINE('CREATE OR REPLACE VIEW '||v.group_view_name||'( ');
     
      L_first_column := 'Y';
      FOR i in C_PRIM_KEY LOOP
         if L_first_column = 'Y' then
            L_prim_col := i.column_name;
            L_first_column := 'N';
         else
            L_prim_col := L_prim_col||','||i.column_name;
         end if;
      END LOOP; 
      DBMS_OUTPUT.PUT_LINE(L_prim_col);

      FOR i in C_VIEW_COLS LOOP
         DBMS_OUTPUT.PUT_LINE(','||i.view_col_name);
         L_column_list := L_column_list ||','||i.storage_col_name||' '||i.view_col_name;
      
         if L_attr_id_list IS NULL then
            L_attr_id_list := i.attrib_id;
         else
            L_attr_id_list := L_attr_id_list||', '||i.attrib_id;
         end if;
      END LOOP;
      
      DBMS_OUTPUT.PUT_LINE(' ) AS SELECT * FROM ');
      DBMS_OUTPUT.PUT_LINE('(SELECT ' || L_prim_col || L_column_list || ' from '||v.custom_ext_table||' where group_id ='||v.group_id||');');
 
      ---
      DBMS_OUTPUT.PUT_LINE('--  End of script for  Group view: '|| v.group_view_name);
      DBMS_OUTPUT.PUT_LINE('-----------------------  ');
   END LOOP;

   if L_grp_id_list is not NULL then
      DBMS_OUTPUT.PUT_LINE('--  Setting the groups  active');
      DBMS_OUTPUT.PUT_LINE('update cfa_attrib_group set active_ind = ''Y''');
      DBMS_OUTPUT.PUT_LINE(' where group_id in ('||L_grp_id_list||');');
      DBMS_OUTPUT.PUT_LINE('-----------------------  ');
      DBMS_OUTPUT.PUT_LINE('--  Setting the attributes  active');
      DBMS_OUTPUT.PUT_LINE('update cfa_attrib set active_ind = ''Y''');
      DBMS_OUTPUT.PUT_LINE(' where attrib_id in ('||L_attr_id_list||');');
      DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
      DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
      DBMS_OUTPUT.PUT_LINE('commit;');
   end if;

   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('--  END of file ${fileName}.');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
END;
/

EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in GEN_VIEW_GRP" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi

}


function GEN_SYNONYMS
{

  sqlplus -s $connectStr  <<EOF> $synonymFile
SET ECHO OFF
SET HEADING OFF
SET LINESIZE 150
SET VERIFY OFF
SET FEEDBACK OFF
SET SERVEROUTPUT ON

DECLARE

   L_table             CFA_EXT_ENTITY.BASE_RMS_TABLE%TYPE;
   L_ext_table         CFA_EXT_ENTITY.CUSTOM_EXT_TABLE%TYPE;

   CURSOR C_USERS IS
      select owner 
        from all_synonyms 
       where synonym_name= L_table
         and TABLE_OWNER = '${TBLOWNER}';
   
   cursor C_EXT_TBL is
      select distinct ent.base_rms_table,
             ent.custom_ext_table
        from cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id  = grps.ext_entity_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}');


   cursor C_GROUP_SET is
      select grps.group_set_view_name,
             grps.staging_table_name
        from cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id  = grps.ext_entity_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}')
         and exists (select 'x'
                       from cfa_attrib_group grp,
                            cfa_attrib att
                      where att.group_id     = grp.group_id
                        and grp.group_set_id = grps.group_set_id
                        and att.active_ind   = 'N');

   cursor C_GROUPS is
      select grp.group_view_name
        from cfa_attrib_group grp,
             cfa_attrib_group_set grps,
             cfa_ext_entity ent
       where ent.ext_entity_id = grps.ext_entity_id
         and grp.group_set_id  = grps.group_set_id
         and grps.ext_entity_id = DECODE('${entityId}', 'ANY', grps.ext_entity_id, '${entityId}')
         and grps.group_set_id  = DECODE('${grpSetId}', 'ALL', grps.group_set_id, '${grpSetId}')
         and exists (select 'x'
                       from cfa_attrib att
                      where att.group_id   = grp.group_id
                        and att.active_ind = 'N');

BEGIN
   ---
   DBMS_OUTPUT.PUT_LINE('--  Genereated by cfagen.sql on ${stmpDate}');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');

   ---
   open C_EXT_TBL;
   fetch C_EXT_TBL into L_table, L_ext_table;
   close C_EXT_TBL;

   FOR s IN C_USERS LOOP
     DBMS_OUTPUT.PUT_LINE('-- Synonyms for user: ' || s.owner);
     IF (s.owner =  'PUBLIC' and '$crtSynonym' != 'Y') then 
      if '${entityId}' != 'ANY' then
         DBMS_OUTPUT.PUT_LINE('create or replace PUBLIC synonym '||L_ext_table ||' for '||'${TBLOWNER}'||'.'||L_ext_table||';');
      end if;

      FOR t IN C_GROUP_SET LOOP
         DBMS_OUTPUT.PUT_LINE('create or replace PUBLIC synonym '||t.group_set_view_name ||' for '||'${TBLOWNER}'||'.'||t.group_set_view_name||';');
         if t.staging_table_name is NOT NULL then
            DBMS_OUTPUT.PUT_LINE('create or replace PUBLIC synonym '||t.staging_table_name ||' for '||'${TBLOWNER}'||'.'||t.staging_table_name||';');
         end if;
      END LOOP;
    
      FOR g IN C_GROUPS LOOP
         DBMS_OUTPUT.PUT_LINE('create or replace PUBLIC synonym '||g.group_view_name ||' for '||'${TBLOWNER}'||'.'||g.group_view_name||';');
      END LOOP;
      DBMS_OUTPUT.PUT_LINE('---------------------------');
    ELSE
     if '${entityId}' != 'ANY' then
         DBMS_OUTPUT.PUT_LINE('create or replace synonym '||s.owner||'.'||L_ext_table ||' for '||'${TBLOWNER}'||'.'||L_ext_table||';');
      end if;

      FOR t IN C_GROUP_SET LOOP
         DBMS_OUTPUT.PUT_LINE('create or replace synonym '||s.owner||'.'||t.group_set_view_name ||' for '||'${TBLOWNER}'||'.'||t.group_set_view_name||';');
         if t.staging_table_name is NOT NULL then
            DBMS_OUTPUT.PUT_LINE('create or replace synonym '||s.owner||'.'||t.staging_table_name ||' for '||'${TBLOWNER}'||'.'||t.staging_table_name||';');
         end if;
      END LOOP;
    
      FOR g IN C_GROUPS LOOP
         DBMS_OUTPUT.PUT_LINE('create or replace synonym '||s.owner||'.'||g.group_view_name ||' for '||'${TBLOWNER}'||'.'||g.group_view_name||';');
      END LOOP;
      DBMS_OUTPUT.PUT_LINE('---------------------------');
   END IF;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
   DBMS_OUTPUT.PUT_LINE('--  END of file ${synonymFile}.');
   DBMS_OUTPUT.PUT_LINE('------------------------------------------------------------------------------');
END;
/
EOF

  if [[ $? -ne ${OK} ]]; then
      echo "Failed in GEN_SYNONYMS" >> $ERRORFILE
      return ${FATAL}
   else
      return ${OK}
   fi
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 5 ]
then
   echo $USAGE
   exit 1
fi

connectStr=$1
genLevel=$2
genId=$3
crtSynonym=$4
dropStg=$5

USER=${connectStr%/*}

if [[ $genLevel != 'E' && $genLevel != 'G' ]]; then
   echo "Invalid generate level, please enter E for Entity or G for Group Set level."
   exit ${FATAL}
fi

if [[ $crtSynonym != 'N' && $crtSynonym != 'YP' && $crtSynonym != 'Y' ]]; then
   echo "Please enter Y to create synonym script, YP to create synonym and Public synonyms script or N."
   exit ${FATAL}
fi

if [[ $dropStg != 'N' && $dropStg != 'Y' ]]; then
   echo "Please enter Y to drop the staging table if exist or N."
   exit ${FATAL}
fi


LOG_MESSAGE "Started by ${USER}"

if [[ $genLevel = 'E' ]]; then
   entityId=$genId
   grpSetId='ALL'
   CHECK_ENTITY_ID

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   fileName="gen_cfa_entity_${genId}.sql"
   synonymFile="gen_cfa_entity_${genId}_synonym.sql"
else
   entityId='ANY'
   grpSetId=$genId
   CHECK_GRP_SET_ID

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi

   fileName="gen_cfa_grp_set_${genId}.sql"
   synonymFile="gen_cfa_grp_set_${genId}_synonym.sql"
fi

if [[ $dropStg = 'N' ]]; then
  CHK_STG_EXISTS

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

echo "--  Genereated by cfagen.sql on ${stmpDate}  --">$fileName

GET_TBL_OWNER

if [[ $entityId != 'ANY' ]]; then
   GEN_EXT_TBL

   if [[ $? -ne ${OK} ]]; then
      exit ${FATAL}
   fi
fi

GEN_VIEW_GRP_SET
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
fi

GEN_STG_TBL
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
fi

GEN_VIEW_GRP
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
fi

if [[ $crtSynonym = 'Y' || $crtSynonym = 'YP' ]]; then
   GEN_SYNONYMS
fi

if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
else
   LOG_MESSAGE "Terminated Successfully"
   exit ${OK}
fi

