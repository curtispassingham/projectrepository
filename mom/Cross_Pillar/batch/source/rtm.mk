include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/rtm_include.mk

#--- Standard targets.

all: rtm-all FORCE

ALL: rtm-ALL FORCE

libs: rtm-libs FORCE

includes: rtm-includes FORCE

libchange: rtm-libchange FORCE

clean: rtm-clean FORCE

clobber: rtm-clobber FORCE

install: rtm-install FORCE

lint: rtm-lint FORCE

depend: rtm-depend

FORCE:

#--- rtm targets

rtm: rtm-all FORCE

rtm-ALL: rtm-libs $(RTM_EXECUTABLES) FORCE

rtm-all: rtm-libs $(RTM_GEN_EXECUTABLES) FORCE

rtm-libs: rtm-includes $(RTM_LIBS) FORCE

rtm-includes: $(RTM_INCLUDES) FORCE

rtm-libchange: FORCE
	rm -f $(RTM_EXECUTABLES)

rtm-clean: FORCE
	rm -f $(RTM_C_SRCS:.c=.o)
	rm -f $(RTM_PC_SRCS:.pc=.o)
	rm -f $(RTM_PC_SRCS:.pc=.c)
	rm -f $(RTM_PC_SRCS:.pc=.lis)
	rm -f $(RTM_EXECUTABLES:=.lint)
	RTM_LIBS="$(RTM_LIBS)"; \
	for f in $$RTM_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rtm-clobber: libchange clean FORCE
	rm -f $(RTM_LIBS)
	rm -f $(RTM_INCLUDES)

rtm-install: FORCE
	-@RTM_EXECUTABLES="$(RTM_EXECUTABLES)"; \
	for f in $$RTM_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rtm-lint: FORCE
	-@RTM_EXECUTABLES="$(RTM_EXECUTABLES)"; \
	for f in $$RTM_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rtm.mk $$e.lint; \
	done


rtm-depend: rtm.d

rtm.d: $(RTM_C_SRCS) $(RTM_PC_SRCS)
	@echo "Making dependencies rtm.d..."
	@$(MAKEDEPEND) $(RTM_C_SRCS) $(RTM_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- Executable targets.

cednld: $(CEDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CEDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

htsupld: $(HTSUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HTSUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lcadnld: $(LCADNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LCADNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lcmdnld: $(LCMDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LCMDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lcup798: $(LCUP798_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LCUP798_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lcupld: $(LCUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LCUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

tranupld: $(TRANUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TRANUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

#--- Executable lint targets.

cednld.lint: $(CEDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CEDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

htsupld.lint: $(HTSUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HTSUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lcadnld.lint: $(LCADNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LCADNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lcmdnld.lint: $(LCMDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LCMDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lcup798.lint: $(LCUP798_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LCUP798_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lcupld.lint: $(LCUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LCUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

tranupld.lint: $(TRANUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TRANUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include rtm.d
