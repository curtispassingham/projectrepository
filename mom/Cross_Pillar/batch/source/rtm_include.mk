#--- Object file groupings.

CEDNLD_PC_OBJS = cednld.o

HTSUPLD_PC_OBJS = htsupld.o

LCADNLD_PC_OBJS = lcadnld.o

LCMDNLD_PC_OBJS = lcmdnld.o

LCUP798_PC_OBJS = lcup798.o

LCUPLD_PC_OBJS = lcupld.o

TRANUPLD_PC_OBJS = tranupld.o

RTM_C_SRCS = 

RTM_PC_SRCS = \
	$(CEDNLD_PC_OBJS:.o=.pc) \
	$(HTSUPLD_PC_OBJS:.o=.pc) \
	$(LCADNLD_PC_OBJS:.o=.pc) \
	$(LCMDNLD_PC_OBJS:.o=.pc) \
	$(LCUP798_PC_OBJS:.o=.pc) \
	$(LCUPLD_PC_OBJS:.o=.pc) \
	$(TRANUPLD_PC_OBJS:.o=.pc) 

#--- Target groupings.

RTM_GEN_EXECUTABLES = \
	cednld \
	htsupld \
	lcadnld \
	lcmdnld \
	lcup798 \
	lcupld \
	tranupld 

RTM_EXECUTABLES= \
	$(RTM_GEN_EXECUTABLES)

RTM_LIBS = 

RTM_INCLUDES = 

#--- Put list of all targets for rtm.mk here

RTM_TARGETS = \
	$(RTM_EXECUTABLES) \
	$(RTM_EXECUTABLES:=.lint) \
	$(RTM_C_SRCS:.c=.o) \
	$(RTM_PC_SRCS:.pc=.o) \
	$(RTM_PC_SRCS:.pc=.c) \
	$(RTM_LIBS) \
	$(RTM_INCLUDES) \
	rtm-all \
	rtm \
	rtm-ALL \
	rtm-libs \
	rtm-includes \
	rtm-clobber \
	rtm-libchange \
	rtm-clean \
	rtm-install \
	rtm-lint \
	rtm-depend
