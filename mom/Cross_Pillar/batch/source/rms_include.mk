#--- Object file groupings.

CCPRG_PC_OBJS = ccprg.o

CNTRMAIN_PC_OBJS = cntrmain.o

CNTRORDB_PC_OBJS = cntrordb.o

CNTRPRSS_PC_OBJS = cntrprss.o

COSTEVENTPRG_PC_OBJS = costeventprg.o

CREMHIERDLY_PC_OBJS = cremhierdly.o

DEALCLS_PC_OBJS = dealcls.o

DEALFINC_PC_OBJS = dealfinc.o

DEALACT_PC_OBJS = dealact.o

DEALDAY_PC_OBJS = dealday.o

DEALFCT_PC_OBJS = dealfct.o

DEALINC_PC_OBJS = dealinc.o

DEALPRG_PC_OBJS = dealprg.o

DEALUPLD_PC_OBJS = dealupld.o

DFRTBLD_PC_OBJS = dfrtbld.o

DISCOTBAPPLY_PC_OBJS = discotbapply.o

DISTROPCPUB_PC_OBJS = distropcpub.o

DITINSRT_PC_OBJS = ditinsrt.o

DLYPRG_PC_OBJS = dlyprg.o

DOCCLOSE_PC_OBJS = docclose.o

DTESYS_PC_OBJS = dtesys.o

DUMMYCTN_PC_OBJS = dummyctn.o

EDIDLCON_PC_OBJS = edidlcon.o

EDIDLINV_PC_OBJS = edidlinv.o

EDIDLORD_PC_OBJS = edidlord.o

EDIDLPRD_PC_OBJS = edidlprd.o

EDIUPACK_PC_OBJS = ediupack.o

EDIUPAVL_PC_OBJS = ediupavl.o

ELCEXCPRG_PC_OBJS = elcexcprg.o

FCEXEC_PC_OBJS = fcexec.o

FCTHREADEXEC_PC_OBJS = fcthreadexec.o

FCSTPRG_PC_OBJS = fcstprg.o

FTMEDNLD_PC_OBJS = ftmednld.o

GENPREISS_PC_OBJS = genpreiss.o

GRADUPLD_PC_OBJS = gradupld.o

HSTBLD_PC_OBJS = hstbld.o

HSTBLD_DIFF_PC_OBJS = hstbld_diff.o

HSTBLDMTH_PC_OBJS = hstbldmth.o

HSTBLDMTH_DIFF_PC_OBJS = hstbldmth_diff.o

HSTPRG_PC_OBJS = hstprg.o

HSTPRG_DIFF_PC_OBJS = hstprg_diff.o

HSTWKUPD_PC_OBJS = hstwkupd.o

HSTMTHUPD_PC_OBJS = hstmthupd.o

IBCALC_PC_OBJS = ibcalc.o

IBEXPL_PC_OBJS = ibexpl.o

INVCLSHP_PC_OBJS = invclshp.o

INVAPRG_PC_OBJS = invaprg.o

INVPRG_PC_OBJS = invprg.o

LCLRBLD_PC_OBJS = lclrbld.o

LIFSTKUP_PC_OBJS = lifstkup.o

MRT_PC_OBJS = mrt.o

MRTPRG_PC_OBJS = mrtprg.o

MRTRTV_PC_OBJS = mrtrtv.o

MRTUPD_PC_OBJS = mrtupd.o

NWPPURGE_PC_OBJS = nwppurge.o

NWPYEAREND_PC_OBJS = nwpyearend.o

ONICTEXT_PC_OBJS = onictext.o

ONORDDNLD_PC_OBJS = onorddnld.o

ONORDEXT_PC_OBJS = onordext.o

ORDDSCNT_PC_OBJS = orddscnt.o

ORDAUTCL_PC_OBJS = ordautcl.o

ORDINVUPLD_PC_OBJS = ordinvupld.o

ORDPRG_PC_OBJS = ordprg.o

ORDREV_PC_OBJS = ordrev.o

ORDUPD_PC_OBJS = ordupd.o

OTBDLORD_PC_OBJS = otbdlord.o

OTBDLSAL_PC_OBJS = otbdlsal.o

OTBDNLD_PC_OBJS = otbdnld.o

OTBPRG_PC_OBJS = otbprg.o

OTBUPLD_PC_OBJS = otbupld.o

POSCDNLD_PC_OBJS = poscdnld.o

PRCHSTPRG_PC_OBJS = prchstprg.o

PREPOST_PC_OBJS = prepost.o

RECLSDLY_PC_OBJS = reclsdly.o

REFMVLOCPRIMADDR_PC_OBJS = refmvlocprimaddr.o

REFMVL10NENTITY_PC_OBJS = refmvl10nentity.o

REQEXT_PC_OBJS = reqext.o

REPLADJ_PC_OBJS = repladj.o

REPLSIZEPROFILE_PC_OBJS = replsizeprofile.o

RILMAINT_PC_OBJS = rilmaint.o

RPLAPPRV_PC_OBJS = rplapprv.o

RPLATHISTPRG_PC_OBJS = rplathistprg.o

RPLATUPD_PC_OBJS = rplatupd.o

RPLBLD_PC_OBJS = rplbld.o

RPLPRG_PC_OBJS = rplprg.o

RPLPRG_MONTH_PC_OBJS = rplprg_month.o

RPLSPLIT_PC_OBJS = rplsplit.o

RPMMOVAVG_PC_OBJS = rpmmovavg.o

RTVPRG_PC_OBJS = rtvprg.o

SALAPND_PC_OBJS = salapnd.o

SALDLY_PC_OBJS = saldly.o

SALEOH_PC_OBJS = saleoh.o

SALMAINT_PC_OBJS = salmaint.o

SALMTH_PC_OBJS = salmth.o

SALPRG_PC_OBJS = salprg.o

SALSTAGE_PC_OBJS = salstage.o

SALWEEK_PC_OBJS = salweek.o

SCCEXT_PC_OBJS = sccext.o

SCHEDPRG_PC_OBJS = schedprg.o

SITMAIN_PC_OBJS = sitmain.o

SOUTDNLD_PC_OBJS = soutdnld.o

STKDLY_PC_OBJS = stkdly.o

STKPRG_PC_OBJS = stkprg.o

STKSCHEDXPLD_PC_OBJS = stkschedxpld.o

STKUPD_PC_OBJS = stkupd.o

STKVAR_PC_OBJS = stkvar.o

STKXPLD_PC_OBJS = stkxpld.o

STLGDNLD_PC_OBJS = stlgdnld.o

SUPCNSTR_PC_OBJS = supcnstr.o

SUPMTH_PC_OBJS = supmth.o

SUPSPLIT_PC_OBJS = supsplit.o

TAMPERCTN_PC_OBJS = tamperctn.o

TAXDNLD_PC_OBJS = taxdnld.o

TAXEVNTPRG_PC_OBJS = taxevntprg.o

TCKTDNLD_PC_OBJS = tcktdnld.o

TSFCLOSE_PC_OBJS = tsfclose.o

TSFPRG_PC_OBJS = tsfprg.o

VATDLXPL_PC_OBJS = vatdlxpl.o

VENDINVC_PC_OBJS = vendinvc.o

VENDINVF_PC_OBJS = vendinvf.o

VRPLBLD_PC_OBJS = vrplbld.o

WASTEADJ_PC_OBJS = wasteadj.o

WFORDCLS_PC_OBJS = wfordcls.o

WFRETCLS_PC_OBJS = wfretcls.o

WFORDPRG_PC_OBJS = wfordprg.o

WFRTNPRG_PC_OBJS = wfrtnprg.o

RMS_C_SRCS = \
   $(OCIROQ_C_OBJS:.o=.c)

RMS_PC_SRCS = \
   $(CCPRG_PC_OBJS:.o=.pc) \
   $(CNTRMAIN_PC_OBJS:.o=.pc) \
   $(CNTRORDB_PC_OBJS:.o=.pc) \
   $(CNTRPRSS_PC_OBJS:.o=.pc) \
   $(COSTEVENTPRG_PC_OBJS:.o=.pc) \
   $(CREMHIERDLY_PC_OBJS:.o=.pc) \
   $(DEALCLS_PC_OBJS:.o=.pc) \
   $(DEALFINC_PC_OBJS:.o=.pc) \
   $(DEALACT_PC_OBJS:.o=.pc) \
   $(DEALDAY_PC_OBJS:.o=.pc) \
   $(DEALFCT_PC_OBJS:.o=.pc) \
   $(DEALINC_PC_OBJS:.o=.pc) \
   $(DEALPRG_PC_OBJS:.o=.pc) \
   $(DEALUPLD_PC_OBJS:.o=.pc) \
   $(DFRTBLD_PC_OBJS:.o=.pc) \
   $(DISCOTBAPPLY_PC_OBJS:.o=.pc) \
   $(DISTROPCPUB_PC_OBJS:.o=.pc) \
   $(DITINSRT_PC_OBJS:.o=.pc) \
   $(DLYPRG_PC_OBJS:.o=.pc) \
   $(DOCCLOSE_PC_OBJS:.o=.pc) \
   $(DTESYS_PC_OBJS:.o=.pc) \
   $(DUMMYCTN_PC_OBJS:.o=.pc) \
   $(EDIDLCON_PC_OBJS:.o=.pc) \
   $(EDIDLINV_PC_OBJS:.o=.pc) \
   $(EDIDLORD_PC_OBJS:.o=.pc) \
   $(EDIDLPRD_PC_OBJS:.o=.pc) \
   $(EDIUPACK_PC_OBJS:.o=.pc) \
   $(EDIUPAVL_PC_OBJS:.o=.pc) \
   $(ELCEXCPRG_PC_OBJS:.o=.pc) \
   $(FCEXEC_PC_OBJS:.o=.pc) \
   $(FCTHREADEXEC_PC_OBJS:.o=.pc) \
   $(FCSTPRG_PC_OBJS:.o=.pc) \
   $(FTMEDNLD_PC_OBJS:.o=.pc) \
   $(GENPREISS_PC_OBJS:.o=.pc) \
   $(GRADUPLD_PC_OBJS:.o=.pc) \
   $(HSTBLD_PC_OBJS:.o=.pc) \
   $(HSTBLD_DIFF_PC_OBJS:.o=.pc) \
   $(HSTBLDMTH_PC_OBJS:.o=.pc) \
   $(HSTBLDMTH_DIFF_PC_OBJS:.o=.pc) \
   $(HSTPRG_PC_OBJS:.o=.pc) \
   $(HSTPRG_DIFF_PC_OBJS:.o=.pc) \
   $(HSTMTHUPD_PC_OBJS:.o=.pc) \
   $(HSTWKUPD_PC_OBJS:.o=.pc) \
   $(IBCALC_PC_OBJS:.o=.pc) \
   $(IBEXPL_PC_OBJS:.o=.pc) \
   $(IFDAYDNLD_PC_OBJS:.o=.pc) \
   $(INVCLSHP_PC_OBJS:.o=.pc) \
   $(INVAPRG_PC_OBJS:.o=.pc) \
   $(INVPRG_PC_OBJS:.o=.pc) \
   $(LCLRBLD_PC_OBJS:.o=.pc) \
   $(LIFSTKUP_PC_OBJS:.o=.pc) \
   $(MRT_PC_OBJS:.o=.pc) \
   $(MRTPRG_PC_OBJS:.o=.pc) \
   $(MRTRTV_PC_OBJS:.o=.pc) \
   $(MRTUPD_PC_OBJS:.o=.pc) \
   $(NWPPURGE_PC_OBJS:.o=.pc) \
   $(NWPYEAREND_PC_OBJS:.o=.pc) \
   $(ONICTEXT_PC_OBJS:.o=.pc) \
   $(ONORDDNLD_PC_OBJS:.o=.pc) \
   $(ONORDEXT_PC_OBJS:.o=.pc) \
   $(ORDDSCNT_PC_OBJS:.o=.pc) \
   $(ORDAUTCL_PC_OBJS:.o=.pc) \
   $(ORDINVUPLD_PC_OBJS:.o=.pc) \
   $(ORDPRG_PC_OBJS:.o=.pc) \
   $(ORDREV_PC_OBJS:.o=.pc) \
   $(ORDUPD_PC_OBJS:.o=.pc) \
   $(OTBDLORD_PC_OBJS:.o=.pc) \
   $(OTBDLSAL_PC_OBJS:.o=.pc) \
   $(OTBDNLD_PC_OBJS:.o=.pc) \
   $(OTBPRG_PC_OBJS:.o=.pc) \
   $(OTBUPLD_PC_OBJS:.o=.pc) \
   $(POSCDNLD_PC_OBJS:.o=.pc) \
   $(PRCHSTPRG_PC_OBJS:.o=.pc) \
   $(PREPOST_PC_OBJS:.o=.pc) \
   $(RECLSDLY_PC_OBJS:.o=.pc) \
   $(REFMVLOCPRIMADDR_PC_OBJS:.o=.pc) \
   $(REFMVL10NENTITY_PC_OBJS:.o=.pc) \
   $(REQEXT_PC_OBJS:.o=.pc) \
   $(REPLADJ_PC_OBJS:.o=.pc) \
   $(REPLSIZEPROFILE_PC_OBJS:.o=.pc) \
   $(RILMAINT_PC_OBJS:.o=.pc) \
   $(RPLAPPRV_PC_OBJS:.o=.pc) \
   $(RPLATHISTPRG_PC_OBJS:.o=.pc) \
   $(RPLATUPD_PC_OBJS:.o=.pc) \
   $(RPLBLD_PC_OBJS:.o=.pc) \
   $(RPLEXT_PC_OBJS:.o=.pc) \
   $(RPLPRG_PC_OBJS:.o=.pc) \
   $(RPLPRG_MONTH_PC_OBJS:.o=.pc) \
   $(RPLSPLIT_PC_OBJS:.o=.pc) \
   $(RPMMOVAVG_PC_OBJS:.o=.pc) \
   $(RTVPRG_PC_OBJS:.o=.pc) \
   $(SALAPND_PC_OBJS:.o=.pc) \
   $(SALDLY_PC_OBJS:.o=.pc) \
   $(SALEOH_PC_OBJS:.o=.pc) \
   $(SALMAINT_PC_OBJS:.o=.pc) \
   $(SALMTH_PC_OBJS:.o=.pc) \
   $(SALPRG_PC_OBJS:.o=.pc) \
   $(SALSTAGE_PC_OBJS:.o=.pc) \
   $(SALWEEK_PC_OBJS:.o=.pc) \
   $(SCCEXT_PC_OBJS:.o=.pc) \
   $(SCHEDPRG_PC_OBJS:.o=.pc) \
   $(SITMAIN_PC_OBJS:.o=.pc) \
   $(SOUTDNLD_PC_OBJS:.o=.pc) \
   $(STKDLY_PC_OBJS:.o=.pc) \
   $(STKPRG_PC_OBJS:.o=.pc) \
   $(STKSCHEDXPLD_PC_OBJS:.o=.pc) \
   $(STKUPD_PC_OBJS:.o=.pc) \
   $(STKUPLD_PC_OBJS:.o=.pc) \
   $(STKVAR_PC_OBJS:.o=.pc) \
   $(STKXPLD_PC_OBJS:.o=.pc) \
   $(STLGDNLD_PC_OBJS:.o=.pc) \
   $(SUPCNSTR_PC_OBJS:.o=.pc) \
   $(SUPMTH_PC_OBJS:.o=.pc) \
   $(SUPSPLIT_PC_OBJS:.o=.pc) \
   $(TAMPERCTN_PC_OBJS:.o=.pc) \
   $(TAXDNLD_PC_OBJS:.o=.pc) \
   $(TAXEVNTPRG_PC_OBJS:.o=.pc) \
   $(TCKTDNLD_PC_OBJS:.o=.pc) \
   $(TSFCLOSE_PC_OBJS:.o=.pc) \
   $(TSFPRG_PC_OBJS:.o=.pc) \
   $(VATDLXPL_PC_OBJS:.o=.pc) \
   $(VENDINVC_PC_OBJS:.o=.pc) \
   $(VENDINVF_PC_OBJS:.o=.pc) \
   $(VRPLBLD_PC_OBJS:.o=.pc) \
   $(WASTEADJ_PC_OBJS:.o=.pc) \
   $(WFORDCLS_PC_OBJS:.o=.pc) \
   $(WFRETCLS_PC_OBJS:.o=.pc) \
   $(WFORDPRG_PC_OBJS:.o=.pc) \
   $(WFRTNPRG_PC_OBJS:.o=.pc)

#--- Target groupings.

RMS_GEN_EXECUTABLES = \
   ccprg \
   cntrmain \
   cntrordb \
   cntrprss \
   costeventprg \
   cremhierdly \
   dealcls \
   dealfinc \
   dealact \
   dealday \
   dealfct \
   dealinc \
   dealprg \
   dealupld \
   dfrtbld \
   discotbapply \
   distropcpub \
   ditinsrt \
   dlyprg \
   docclose \
   dtesys \
   dummyctn\
   edidlcon \
   edidlinv \
   edidlord \
   edidlprd \
   ediupack \
   ediupavl \
   elcexcprg \
   fcexec \
   fcthreadexec \
   fcstprg \
   ftmednld \
   genpreiss \
   gradupld \
   hstbld \
   hstbld_diff \
   hstbldmth \
   hstbldmth_diff \
   hstprg \
   hstprg_diff \
   hstmthupd \
   hstwkupd \
   ibcalc \
   ibexpl \
   invclshp \
   invaprg \
   invprg \
   lclrbld \
   lifstkup \
   mrt \
   mrtprg \
   mrtrtv \
   mrtupd \
   nwppurge \
   nwpyearend \
   onictext \
   onorddnld \
   onordext \
   ordautcl \
   ordinvupld \
   orddscnt \
   ordprg \
   ordrev \
   ordupd \
   otbdlord \
   otbdlsal \
   otbdnld \
   otbprg \
   otbupld \
   poscdnld \
   prchstprg \
   prepost \
   reclsdly \
   refmvlocprimaddr \
   refmvl10nentity \
   reqext \
   repladj \
   replsizeprofile \
   rilmaint \
   rplapprv \
   rplathistprg \
   rplatupd \
   rplbld \
   rplprg \
   rplprg_month \
   rplsplit \
   rpmmovavg \
   rtvprg \
   salapnd \
   saldly \
   saleoh \
   salmaint \
   salmth \
   salprg \
   salstage \
   salweek \
   sccext \
   schedprg \
   sitmain \
   soutdnld \
   stkdly \
   stkprg \
   stkschedxpld \
   stkupd \
   stkvar \
   stkxpld \
   stlgdnld \
   supcnstr \
   supmth \
   supsplit \
   tamperctn \
   taxdnld \
   taxevntprg \
   tcktdnld \
   tsfclose \
   tsfprg \
   vatdlxpl \
   vendinvc \
   vendinvf \
   vrplbld \
   wasteadj \
   wfordcls \
  wfretcls \
   wfordprg \
   wfrtnprg

RMS_EXECUTABLES= \
   $(RMS_GEN_EXECUTABLES)

RMS_LIBS =

RMS_INCLUDES =

#--- External systems

RDM_RMS_C_SRCS =

RDM_RMS_PC_SRCS =

RDM_RMS_GEN_EXECUTABLES =

RDM_RMS_EXECUTABLES = \
   $(RDM_RMS_GEN_EXECUTABLES)

RDM_RMS_LIBS =

RDM_RMS_INCLUDES =

#--- Put list of all targets for rms.mk here

RMS_TARGETS = \
   $(RMS_EXECUTABLES) \
   $(RMS_EXECUTABLES:=.lint) \
   $(RMS_C_SRCS:.c=.o) \
   $(RMS_PC_SRCS:.pc=.o) \
   $(RMS_PC_SRCS:.pc=.c) \
   $(RMS_LIBS) \
   $(RMS_INCLUDES) \
   $(RDM_RMS_EXECUTABLES) \
   $(RDM_RMS_EXECUTABLES:=.lint) \
   $(RDM_RMS_SRCS:.c=.o) \
   $(RDM_RMS_PC_SRCS:.pc=.o) \
   $(RDM_RMS_PC_SRCS:.pc=.c) \
   $(RDM_RMS_LIBS) \
   $(RDM_RMS_INCLUDES) \
   rms-all \
   rms \
   rms-ALL \
   rms-libs \
   rms-includes \
   rms-clobber \
   rms-libchange \
   rms-clean \
   rms-install \
   rms-lint \
   rms-depend \
   rdm-rms-all \
   rdm-rms \
   rdm-rms-ALL \
   rdm-rms-libs \
   rdm-rms-includes \
   rdm-rms-clobber \
   rdm-rms-libchange \
   rdm-rms-clean \
   rdm-rms-install \
   rdm-rms-lint \
   rdm-rms-depend \
   rms-rdm-all \
   rms-rdm \
   rms-rdm-ALL \
   rms-rdm-libs \
   rms-rdm-includes \
   rms-rdm-clobber \
   rms-rdm-libchange \
   rms-rdm-clean \
   rms-rdm-install \
   rms-rdm-lint \
   rms-rdm-depend
