#! /bin/ksh

#-------------------------------------------------------------------------------
#  File:  cfastgload.ksh
#
#  Desc:  This shell script will be used to load records from the CFA staging
#         tables into the extaction tables.
#-------------------------------------------------------------------------------

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
FATAL=255
NON_FATAL=1
OK=0
TRUE=1
FALSE=0


USAGE="Usage: $pgmName.$pgmExt <connect> <input staging table> <delete rec in staging table Y/N> [input process id]"

function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE

   return $retCode
}

function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi

   return $retCode
}

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code  := 0;
      EXEC :GV_script_error := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${connectStr}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}

function LOAD_ATTRIB
{
   
   sqlTxt="
      DECLARE
         FUNCTION_ERROR    EXCEPTION;
         L_process_id      VARCHAR2(10) := '${processId}';
         L_error_type      VARCHAR2(255):= NULL;
      BEGIN

         if L_process_id = 'ALL' then
            L_process_id := NULL;
         end if;
         if NOT CFA_LOAD_SQL.LOAD_ATTRIB(:GV_script_error,
                                         '${stgTbl}',
                                         L_process_id, 
                                         '${delStg}') then
            SQL_LIB.API_MSG(L_error_type, :GV_script_error);
            raise FUNCTION_ERROR;
         end if;

         COMMIT;

      EXCEPTION
         when FUNCTION_ERROR then
            ROLLBACK;
            :GV_return_code := ${FATAL};
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"

   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "LOAD_ATTRIB Table: $stgTbl Process ID: $processId" >> $ERRORFILE
      return ${FATAL}
   else
      LOG_MESSAGE " Completed loading Table: $stgTbl Process ID: $processId"
      return ${OK}
   fi
}

#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------

# Test for the number of input arguments
if [ $# -lt 3 ]
then
   echo $USAGE
   exit 1
fi

connectStr=$1
stgTbl=$2
delStg=$3
processId=$4

if [[ $delStg != 'N' && $delStg != 'Y' ]]; then
   echo "Please enter Y delete records in the staging table or N."
   exit ${FATAL}
fi

if [[ -z $processId ]]; then
  processId='ALL'
fi

USER=${connectStr%/*}

LOG_MESSAGE "Started by ${USER}"

LOAD_ATTRIB

if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
else
   LOG_MESSAGE "Terminated Successfully"
   exit ${OK}
fi

