include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/rms_include.mk

#--- Standard targets.

ALL: rms-ALL FORCE

all: rms-all FORCE

libs: rms-libs FORCE

iNCludes: rms-includes FORCE

libchange: rms-libchange rdm-rms-libchange FORCE

clean: rms-clean rdm-rms-clean FORCE

clobber: rms-clobber rdm-rms-clobber FORCE

install: rms-install rdm-rms-install FORCE

lint: rms-lint rdm-rms-lint FORCE

depend: rms-depend rdm-rms-depend

FORCE:

#--- RMS specific

rms: rms-all FORCE

rms-ALL: rms-libs $(RMS_EXECUTABLES) $(RDM_RMS_EXECUTABLES) FORCE

rms-all: rms-libs $(RMS_GEN_EXECUTABLES) FORCE

rms-libs: rms-includes $(RMS_LIBS) FORCE

rms-includes: $(RMS_INCLUDES) FORCE

rms-libchange: FORCE
	rm -f $(RMS_EXECUTABLES)

rms-clean: FORCE
	rm -f $(RMS_C_SRCS:.c=.o)
	rm -f $(RMS_PC_SRCS:.pc=.o)
	rm -f $(RMS_PC_SRCS:.pc=.c)
	rm -f $(RMS_PC_SRCS:.pc=.lis)
	rm -f $(RMS_EXECUTABLES:=.lint)
	RMS_LIBS="$(RMS_LIBS)"; \
	for f in $$RMS_LIBS; \
	do \
		n=llib-l`expr "$$f" : 'lib\(.*\)\.a' \| "$$f" : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rms-clobber: rms-libchange rms-clean FORCE
	rm -f $(RMS_LIBS)
	rm -f $(RMS_INCLUDES)

rms-install: FORCE
	-@RMS_EXECUTABLES="$(RMS_EXECUTABLES)"; \
	for f in $$RMS_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rms-lint: FORCE
	-@RMS_EXECUTABLES="$(RMS_EXECUTABLES)"; \
	for e in $$RMS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $$e.lint; \
	done


rms-depend: rms.d

rms.d: $(RMS_C_SRCS) $(RMS_PC_SRCS)
	@echo "Making dependencies $@..."
	@$(MAKEDEPEND) $(RMS_C_SRCS) $(RMS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- External systems

rdm-rms: rdm-rms-libs $(RDM_RMS_GEN_EXECUTABLES) FORCE

rdm-rms-ALL: rdm-rms-libs $(RDM_RMS_EXECUTABLES) FORCE

rdm-rms-all: rdm-rms FORCE

rdm-rms-libs: rdm-rms-includes $(RDM_RMS_LIBS) FORCE

rdm-rms-includes: $(RDM_RMS_INCLUDES) FORCE

rdm-rms-libchange: FORCE
	rm -f $(RDM_RMS_EXECUTABLES)

rdm-rms-clean: FORCE
	rm -f $(RDM_RMS_C_SRCS:.c=.o)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.o)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.c)
	rm -f $(RDM_RMS_PC_SRCS:.pc=.lis)
	rm -f $(RDM_RMS_EXECUTABLES:=.lint)
	RMS_LIBS="$(RDM_RMS_LIBS)"; \
	for f in $$RDM_RMS_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\).a' \| $$f : 'lib\(.*\).$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rdm-rms-clobber: rdm-rms-libchange rdm-rms-clean FORCE
	rm -f $(RDM_RMS_LIBS)
	rm -f $(RDM_RMS_INCLUDES)

rdm-rms-install: FORCE
	-@RDM_RMS_EXECUTABLES="$(RDM_RMS_EXECUTABLES)"; \
	for f in $$RDM_RMS_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rdm-rms-lint: FORCE
	-@RDM_RMS_EXECUTABLES="$(RDM_RMS_EXECUTABLES)"; \
	for e in $$RDM_RMS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $$e.lint; \
	done


rdm-rms-depend: rdm-rms.d

rdm-rms.d: $(RDM_RMS_C_SRCS) $(RDM_RMS_PC_SRCS)
	@echo "Making dependencies $@..."
	@$(MAKEDEPEND) $(RDM_RMS_C_SRCS) $(RDM_RMS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`


rms-rdm: rdm-rms FORCE

rms-rdm-ALL: rdm-rms-ALL FORCE

rms-rdm-all: rdm-rms-all FORCE

rms-rdm-libs: rdm-rms-libs FORCE

rms-rdm-includes: rdm-rms-includes FORCE

rms-rdm-libchange: rdm-rms-libchange FORCE

rms-rdm-clean: rdm-rms-clean FORCE

rms-rdm-clobber: rdm-rms-clobber FORCE

rms-rdm-install: rdm-rms-install FORCE

rms-rdm-lint: rdm-rms-lint FORCE

rms-rdm-depend: rdm-rms-depend

rms-rdm.d: rdm-rms.d

#--- Executable targets.


ccprg: $(CCPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CCPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

cntrmain: $(CNTRMAIN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CNTRMAIN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

cntrordb: $(CNTRORDB_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CNTRORDB_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

cntrprss: $(CNTRPRSS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CNTRPRSS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

costeventprg: $(COSTEVENTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(COSTEVENTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

cremhierdly: $(CREMHIERDLY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CREMHIERDLY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealupld: $(DEALUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealcls: $(DEALCLS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALCLS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealfinc: $(DEALFINC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALFINC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealact: $(DEALACT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALACT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealday: $(DEALDAY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALDAY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealfct: $(DEALFCT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALFCT_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealinc $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealinc: $(DEALINC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALINC_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealinc $(LDFLAGS)
	chmod $(MASK) `basename $@`

dealprg: $(DEALPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DEALPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dfrtbld: $(DFRTBLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DFRTBLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

discotbapply: $(DISCOTBAPPLY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DISCOTBAPPLY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

distropcpub: $(DISTROPCPUB_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DISTROPCPUB_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ditinsrt: $(DITINSRT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DITINSRT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dlyprg: $(DLYPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DLYPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

docclose: $(DOCCLOSE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DOCCLOSE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dtesys: $(DTESYS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DTESYS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

dummyctn: $(DUMMYCTN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(DUMMYCTN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

edidlcon: $(EDIDLCON_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIDLCON_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

edidlinv: $(EDIDLINV_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIDLINV_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

edidlord: $(EDIDLORD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIDLORD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

edidlprd: $(EDIDLPRD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIDLPRD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ediupack: $(EDIUPACK_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIUPACK_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ediupavl: $(EDIUPAVL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(EDIUPAVL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

elcexcprg: $(ELCEXCPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ELCEXCPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

fcexec: $(FCEXEC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FCEXEC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

fcthreadexec: $(FCTHREADEXEC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FCTHREADEXEC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

fcstprg: $(FCSTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FCSTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ftmednld: $(FTMEDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(FTMEDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

genpreiss: $(GENPREISS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(GENPREISS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

gradupld: $(GRADUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(GRADUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstbld: $(HSTBLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTBLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstbld_diff: $(HSTBLD_DIFF_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTBLD_DIFF_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstbldmth: $(HSTBLDMTH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTBLDMTH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstbldmth_diff: $(HSTBLDMTH_DIFF_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTBLDMTH_DIFF_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstprg: $(HSTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstprg_diff: $(HSTPRG_DIFF_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTPRG_DIFF_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstmthupd: $(HSTMTHUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTMTHUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

hstwkupd: $(HSTWKUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(HSTWKUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ibcalc: $(IBCALC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(IBCALC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ibexpl: $(IBEXPL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(IBEXPL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

invclshp: $(INVCLSHP_PC_OBJS)
	$(LD) -o `basename $@` `echo $(INVCLSHP_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

invaprg: $(INVAPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(INVAPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

invprg: $(INVPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(INVPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lclrbld: $(LCLRBLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LCLRBLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

lifstkup: $(LIFSTKUP_PC_OBJS)
	$(LD) -o `basename $@` `echo $(LIFSTKUP_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

mrt: $(MRT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(MRT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

mrtprg: $(MRTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(MRTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

mrtrtv: $(MRTRTV_PC_OBJS)
	$(LD) -o `basename $@` `echo $(MRTRTV_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

mrtupd: $(MRTUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(MRTUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

nwppurge: $(NWPPURGE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(NWPPURGE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

nwpyearend: $(NWPYEAREND_PC_OBJS)
	$(LD) -o `basename $@` `echo $(NWPYEAREND_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

onictext: $(ONICTEXT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ONICTEXT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

onorddnld: $(ONORDDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ONORDDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

onordext: $(ONORDEXT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ONORDEXT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

orddscnt: $(ORDDSCNT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDDSCNT_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealord $(LDFLAGS)
	chmod $(MASK) `basename $@`

ordautcl: $(ORDAUTCL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDAUTCL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ordinvupld: $(ORDINVUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDINVUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ordprg: $(ORDPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ordrev: $(ORDREV_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDREV_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ordupd: $(ORDUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ORDUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

otbdlord: $(OTBDLORD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(OTBDLORD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

otbdlsal: $(OTBDLSAL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(OTBDLSAL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

otbdnld: $(OTBDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(OTBDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

otbprg: $(OTBPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(OTBPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

otbupld: $(OTBUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(OTBUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

poscdnld: $(POSCDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(POSCDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

prchstprg: $(PRCHSTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(PRCHSTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

prepost: $(PREPOST_PC_OBJS)
	$(LD) -o `basename $@` `echo $(PREPOST_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

reclsdly: $(RECLSDLY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RECLSDLY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

refmvlocprimaddr: $(REFMVLOCPRIMADDR_PC_OBJS)
	$(LD) -o `basename $@` `echo $(REFMVLOCPRIMADDR_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

refmvl10nentity: $(REFMVL10NENTITY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(REFMVL10NENTITY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

reqext: $(REQEXT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(REQEXT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

repladj: $(REPLADJ_PC_OBJS)
	$(LD) -o `basename $@` `echo $(REPLADJ_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`
	
replsizeprofile: $(REPLSIZEPROFILE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(REPLSIZEPROFILE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rilmaint: $(RILMAINT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RILMAINT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplapprv: $(RPLAPPRV_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLAPPRV_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplathistprg: $(RPLATHISTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLATHISTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplatupd: $(RPLATUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLATUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplbld: $(RPLBLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLBLD_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchcreateord $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplprg: $(RPLPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLPRG_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lpartition $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplprg_month: $(RPLPRG_MONTH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLPRG_MONTH_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lpartition $(LDFLAGS)
	chmod $(MASK) `basename $@`

rplsplit: $(RPLSPLIT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPLSPLIT_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchcreateord $(LDFLAGS)
	chmod $(MASK) `basename $@`

rpmmovavg: $(RPMMOVAVG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RPMMOVAVG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

rtvprg: $(RTVPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(RTVPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

salapnd: $(SALAPND_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALAPND_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saldly: $(SALDLY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALDLY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saleoh: $(SALEOH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALEOH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

salmaint: $(SALMAINT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALMAINT_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lpartition $(LDFLAGS)
	chmod $(MASK) `basename $@`
salmth: $(SALMTH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALMTH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

salprg: $(SALPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

salstage: $(SALSTAGE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALSTAGE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

salweek: $(SALWEEK_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SALWEEK_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sccext: $(SCCEXT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SCCEXT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

schedprg: $(SCHEDPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SCHEDPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sitmain: $(SITMAIN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SITMAIN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

soutdnld: $(SOUTDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SOUTDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkdly: $(STKDLY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STKDLY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkprg: $(STKPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STKPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkschedxpld: $(STKSCHEDXPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STKSCHEDXPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkupd: $(STKUPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STKUPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkvar: $(STKVAR_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STKVAR_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stkxpld: $(STKXPLD_PC_OBJS) $(STKXPLD_C_OBJS)
	$(LD) -o `basename $@` `echo $(STKXPLD_PC_OBJS) $(STKXPLD_C_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

stlgdnld: $(STLGDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(STLGDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

supcnstr: $(SUPCNSTR_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SUPCNSTR_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchsupcstrr $(LDFLAGS)
	chmod $(MASK) `basename $@`

supmth: $(SUPMTH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SUPMTH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

supsplit: $(SUPSPLIT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SUPSPLIT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

tamperctn: $(TAMPERCTN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TAMPERCTN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

taxdnld: $(TAXDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TAXDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

taxevntprg: $(TAXEVNTPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TAXEVNTPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

tcktdnld: $(TCKTDNLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TCKTDNLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

tsfclose: $(TSFCLOSE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TSFCLOSE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

tsfprg: $(TSFPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(TSFPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

vatdlxpl: $(VATDLXPL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(VATDLXPL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

vendinvc: $(VENDINVC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(VENDINVC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

vendinvf: $(VENDINVF_PC_OBJS)
	$(LD) -o `basename $@` `echo $(VENDINVF_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

vrplbld: $(VRPLBLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(VRPLBLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

wasteadj: $(WASTEADJ_PC_OBJS)
	$(LD) -o `basename $@` `echo $(WASTEADJ_PC_OBJS) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchl10n $(LDFLAGS)
	chmod $(MASK) `basename $@`

wfordcls: $(WFORDCLS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(WFORDCLS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`
  
wfretcls: $(WFRETCLS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(WFRETCLS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`  

wfordprg: $(WFORDPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(WFORDPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

wfrtnprg: $(WFRTNPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(WFRTNPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`


#--- Executable lint targets.

ccprg.lint: $(CCPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CCPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

cntrmain.lint: $(CNTRMAIN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CNTRMAIN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

cntrordb.lint: $(CNTRORDB_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CNTRORDB_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

cntrprss.lint: $(CNTRPRSS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CNTRPRSS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

costeventprg.lint: $(COSTEVENTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(COSTEVENTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

cremhierdly.lint: $(CREMHIERDLY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CREMHIERDLY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealupld.lint: $(DEALUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealcls.lint: $(DEALCLS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALCLS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealfinc.lint: $(DEALFINC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALFINC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealact.lint: $(DEALACT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALACT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealday.lint: $(DEALDAY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALDAY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealfct.lint: $(DEALFCT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALFCT_PC_OBJS:.o=.c) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealinc $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealinc.lint: $(DEALINC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALINC_PC_OBJS:.o=.c) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealinc $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dealprg.lint: $(DEALPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DEALPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dfrtbld.lint: $(DFRTBLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DFRTBLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

discotbapply.lint: $(DISCOTBAPPLY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DISCOTBAPPLY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

distropcpub.lint: $(DISTROPCPUB_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DISTROPCPUB_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ditinsrt.lint: $(DITINSRT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DITINSRT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dlyprg.lint: $(DLYPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DLYPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

docclose.lint: $(DOCCLOSE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DOCCLOSE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dtesys.lint: $(DTESYS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DTESYS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

dummyctn.lint: $(DUMMYCTN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(DUMMYCTN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

edidlcon.lint: $(EDIDLCON_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIDLCON_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

edidlinv.lint: $(EDIDLINV_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIDLINV_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

edidlord.lint: $(EDIDLORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIDLORD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

edidlprd.lint: $(EDIDLPRD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIDLPRD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ediupack.lint: $(EDIUPACK_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIUPACK_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ediupavl.lint: $(EDIUPAVL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(EDIUPAVL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

elcexcprg.lint: $(ELCEXCPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ELCEXCPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@` 

fcexec.lint: $(FCEXEC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FCEXEC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@` 

fcthreadexec.lint: $(FCTHREADEXEC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FCTHREADEXEC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@` 

fcstprg.lint: $(FCSTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FCSTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ftmednld.lint: $(FTMEDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(FTMEDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

genpreiss.lint: $(GENPREISS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(GENPREISS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

gradupld.lint: $(GRADUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(GRADUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstbld.lint: $(HSTBLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTBLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstbld_diff.lint: $(HSTBLD_DIFF_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTBLD_DIFF_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstbldmth.lint: $(HSTBLDMTH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTBLDMTH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstbldmth_diff.lint: $(HSTBLDMTH_DIFF_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTBLDMTH_DIFF_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstprg.lint: $(HSTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstprg_diff.lint: $(HSTPRG_DIFF_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTPRG_DIFF_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstmthupd.lint: $(HSTMTHUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTMTHUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

hstwkupd.lint: $(HSTWKUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(HSTWKUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ibcalc.lint: $(IBCALC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(IBCALC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ibexpl.lint: $(IBEXPL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(IBEXPL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

invclshp.lint: $(INVCLSHP_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(INVCLSHP_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

invaprg.lint: $(INVAPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(INVAPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

invprg.lint: $(INVPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(INVPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lclrbld.lint: $(LCLRBLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LCLRBLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

lifstkup.lint: $(LIFSTKUP_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(LIFSTKUP_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

mrt.lint: $(MRT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(MRT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

mrtprg.lint: $(MRTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(MRTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

mrtrtv.lint: $(MRTRTV_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(MRTRTV_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

mrtupd.lint: $(MRTUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(MRTUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

nwppurge.lint: $(NWPPURGE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(NWPPURGE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

nwpyearend.lint: $(NWPYEAREND_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(NWPYEAREND_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

onictext.lint: $(ONICTEXT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ONICTEXT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

onorddnld.lint: $(ONORDDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ONORDDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

onordext.lint: $(ONORDEXT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ONORDEXT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

orddscnt.lint: $(ORDDSCNT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDDSCNT_PC_OBJS:.o=.c) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchdealord $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ordautcl.lint: $(ORDAUTCL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDAUTCL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ordinvupld.lint: $(ORDINVUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDINVUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ordprg.lint: $(ORDPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ordrev.lint: $(ORDREV_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDREV_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ordupd.lint: $(ORDUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ORDUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

otbdlord.lint: $(OTBDLORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(OTBDLORD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

otbdlsal.lint: $(OTBDLSAL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(OTBDLSAL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

otbdnld.lint: $(OTBDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(OTBDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

otbprg.lint: $(OTBPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(OTBPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

otbupld.lint: $(OTBUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(OTBUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

poscdnld.lint: $(POSCDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(POSCDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

prchstprg.lint: $(PRCHSTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(PRCHSTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

prepost.lint: $(PREPOST_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(PREPOST_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

reclsdly.lint: $(RECLSDLY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RECLSDLY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

refmvlocprimaddr.lint: $(REFMVLOCPRIMADDR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(REFMVLOCPRIMADDR_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

refmvl10nentity.lint: $(REFMVL10NENTITY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(REFMVL10NENTITY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

reqext.lint: $(REQEXT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(REQEXT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

repladj.lint: $(REPLADJ_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(REPLADJ_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

replsizeprofile.lint: $(REPLSIZEPROFILE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(REPLSIZEPROFILE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rilmaint.lint: $(RILMAINT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RILMAINT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplapprv.lint: $(RPLAPPRV_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLAPPRV_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplathistprg.lint: $(RPLATHISTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLATHISTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplatupd.lint: $(RPLATUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLATUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplbld.lint: $(RPLBLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLBLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplprg.lint: $(RPLPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplprg_month.lint: $(RPLPRG_MONTH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLPRG_MONTH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rpmmovavg.lint: $(RPMMOVAVG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPMMOVAVG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rplsplit.lint: $(RPLSPLIT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RPLSPLIT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

rtvprg.lint: $(RTVPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(RTVPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

salapnd.lint: $(SALAPND_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALAPND_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saldly.lint: $(SALDLY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALDLY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saleoh.lint: $(SALEOH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALEOH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

salmaint.lint: $(SALMAINT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALMAINT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`
salmth.lint: $(SALMTH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALMTH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

salprg.lint: $(SALPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

salstage.lint: $(SALSTAGE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALSTAGE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

salweek.lint: $(SALWEEK_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SALWEEK_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sccext.lint: $(SCCEXT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SCCEXT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

schedprg.lint: $(SCHEDPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SCHEDPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sitmain.lint: $(SITMAIN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SITMAIN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

soutdnld.lint: $(SOUTDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SOUTDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkdly.lint: $(STKDLY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKDLY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkprg.lint: $(STKPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkschedxpld.lint: $(STKSCHEDXPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKSCHEDXPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkupd.lint: $(STKUPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKUPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkvar.lint: $(STKVAR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKVAR_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stkxpld.lint: $(STKXPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STKXPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

stlgdnld.lint: $(STLGDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(STLGDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

supcnstr.lint: $(SUPCNSTR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SUPCNSTR_PC_OBJS:.o=.c) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchsupcstrr $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

supmth.lint: $(SUPMTH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SUPMTH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

supsplit.lint: $(SUPSPLIT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SUPSPLIT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

tamperctn.lint: $(TAMPERCTN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TAMPERCTN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

taxdnld.lint: $(TAXDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TAXDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

taxevntprg.lint: $(TAXEVNTPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TAXEVNTPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

tcktdnld.lint: $(TCKTDNLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TCKTDNLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

tsfclose.lint: $(TSFCLOSE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TSFCLOSE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

tsfprg.lint: $(TSFPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(TSFPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

vatdlxpl.lint: $(VATDLXPL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(VATDLXPL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

vendinvc.lint: $(VENDINVC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(VENDINVC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

vendinvf.lint: $(VENDINVF_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(VENDINVF_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

vrplbld.lint: $(VRPLBLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(VRPLBLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

wasteadj.lint: $(WASTEADJ_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(WASTEADJ_PC_OBJS:.o=.c) | xargs -n1 basename` -L. -L$(RETEK_LIB_BIN) $(SHARED_LIB_RUN). $(SHARED_LIB_RUN)$(RETEK_LIB_BIN) -lbatchl10n $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

wfordcls.lint: $(WFORDCLS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(WFORDCLS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`
  
wfretcls.lint: $(WFRETCLS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(WFRETCLS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`  

wfordprg.lint: $(WFORDPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(WFORDPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

wfrtnprg.lint: $(WFRTNPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(WFRTNPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include rms.d
include rdm-rms.d
