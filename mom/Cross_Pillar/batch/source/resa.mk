include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L. -L$(RETEK_LIB_BIN) -lresa
PRODUCT_LINT_FLAGS = $(RETEK_LIB_BIN)/llib-lresa.ln
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/resa_include.mk

#--- Standard targets.

ALL: resa-ALL FORCE

all: resa-all FORCE

libs: resa-libs FORCE

includes: resa-includes FORCE

libchange: resa-libchange FORCE

clean: resa-clean FORCE

clobber: resa-clobber FORCE

install: resa-install FORCE

lint: resa-lint FORCE

depend: resa-depend

FORCE:

#--- resa targets

resa: resa-all FORCE

resa-ALL: resa-libs $(ReSA_ALL_EXECUTABLES) FORCE

resa-all: resa-libs $(ReSA_GEN_EXECUTABLES) FORCE

resa-libs: resa-includes $(ReSA_LIBS) FORCE

resa-includes: $(ReSA_INCLUDES) FORCE

resa-libchange: FORCE
	rm -f $(ReSA_ALL_EXECUTABLES)

resa-clean: FORCE
	rm -f $(ReSA_ALL_C_SRCS:.c=.o)
	rm -f $(ReSA_ALL_PC_SRCS:.pc=.o)
	rm -f $(ReSA_ALL_PC_SRCS:.pc=.c)
	rm -f $(ReSA_ALL_PC_SRCS:.pc=.lis)
	rm -f $(ReSA_ALL_EXECUTABLES:=.lint)
	ReSA_LIBS="$(ReSA_LIBS)"; \
	for f in $$ReSA_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

resa-clobber: libchange clean FORCE
	rm -f $(ReSA_LIBS)
	rm -f $(ReSA_INCLUDES)

resa-install: FORCE
	-@ReSA_ALL_EXECUTABLES="$(ReSA_ALL_EXECUTABLES)"; \
	for f in $$ReSA_ALL_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

resa-lint: FORCE
	-@ReSA_ALL_EXECUTABLES="$(ReSA_ALL_EXECUTABLES)"; \
	for f in $$ReSA_ALL_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/resa.mk $$e.lint; \
	done


resa-depend: resa.d

resa.d: $(ReSA_ALL_C_SRCS) $(ReSA_ALL_PC_SRCS)
	@echo "Making dependencies resa.d..."
	@$(MAKEDEPEND) $(ReSA_ALL_C_SRCS) $(ReSA_ALL_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- External systems

ach: resa-ach FORCE

im: resa-im FORCE

rdw: resa-rdw FORCE

rms: resa-rms FORCE

uar: resa-uar FORCE

resa-ach: $(ReSA_ACH_EXECUTABLES) FORCE

resa-im: $(ReSA_IM_EXECUTABLES) FORCE

resa-rdw: $(ReSA_RDW_EXECUTABLES) FORCE

resa-rms: $(ReSA_RMS_EXECUTABLES) FORCE

resa-uar: $(ReSA_UAR_EXECUTABLES) FORCE

ach-resa: resa-ach FORCE

im-resa: resa-im FORCE

rdw-resa: resa-rdw FORCE

rms-resa: resa-rms FORCE

uar-resa: resa-uar FORCE

#--- Executable targets.

saescheat: $(SAESCHEAT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAESCHEAT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpach: $(SAEXPACH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPACH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpgl: $(SAEXPGL_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPGL_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpim: $(SAEXPIM_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPIM_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpdw: $(saexpdw_PC_OBJS)
	$(LD) -o `basename $@` `echo $(saexpdw_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexprms: $(SAEXPRMS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPRMS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpsim: $(SAEXPSIM_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPSIM_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saexpuar: $(SAEXPUAR_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAEXPUAR_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sagetref: $(SAGETREF_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAGETREF_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saimpadj: $(SAIMPADJ_C_OBJS) $(SAIMPADJ_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAIMPADJ_C_OBJS) $(SAIMPADJ_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saimptlog: $(SAIMPTLOG_C_OBJS) $(SAIMPTLOG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAIMPTLOG_C_OBJS) $(SAIMPTLOG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saimptlogi: $(SAIMPTLOGI_C_OBJS) $(SAIMPTLOGI_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAIMPTLOGI_C_OBJS) $(SAIMPTLOGI_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saimptlogfin: $(SAIMPTLOGFIN_C_OBJS) $(SAIMPTLOGFIN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAIMPTLOGFIN_C_OBJS) $(SAIMPTLOGFIN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saimptlogtdup_upd: $(SAIMPTLOGTDUP_UPD_C_OBJS) $(SAIMPTLOGTDUP_UPD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAIMPTLOGTDUP_UPD_C_OBJS) $(SAIMPTLOGTDUP_UPD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saordinvexp: $(SAORDINVEXP_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAORDINVEXP_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

ang_saplgen: $(ANG_SAPLGEN_PC_OBJS)
	$(LD) -o `basename $@` `echo $(ANG_SAPLGEN_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sapreexp: $(SAPREEXP_C_OBJS) $(SAPREEXP_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAPREEXP_C_OBJS) $(SAPREEXP_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

saprepost: $(SAPREPOST_C_OBJS) $(SAPREPOST_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAPREPOST_C_OBJS) $(SAPREPOST_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sapurge: $(SAPURGE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAPURGE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sarules: $(SARULES_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SARULES_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

sastdycr: $(SASTDYCR_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SASTDYCR_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

savouch: $(SAVOUCH_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SAVOUCH_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

satotals: $(SATOTALS_PC_OBJS)
	$(LD) -o `basename $@` `echo $(SATOTALS_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`


#--- Executable lint targets.

saescheat.lint: $(SAESCHEAT_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAESCHEAT_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpach.lint: $(SAEXPACH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPACH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpgl.lint: $(SAEXPGL_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPGL_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpim.lint: $(SAEXPIM_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPIM_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpdw.lint: $(saexpdw_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(saexpdw_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexprms.lint: $(SAEXPRMS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPRMS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpsim.lint: $(SAEXPSIM_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPSIM_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saexpuar.lint: $(SAEXPUAR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAEXPUAR_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sagetref.lint: $(SAGETREF_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAGETREF_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saimpadj.lint: $(SAIMPADJ_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAIMPADJ_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saimptlog.lint: $(SAIMPTLOG_C_OBJS:.o=.c) $(SAIMPTLOG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAIMPTLOG_C_OBJS:.o=.c) $(SAIMPTLOG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saimptlogi.lint: $(SAIMPTLOGI_C_OBJS:.o=.c) $(SAIMPTLOGI_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAIMPTLOGI_C_OBJS:.o=.c) $(SAIMPTLOGI_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saimptlogfin.lint: $(SAIMPTLOGFIN_C_OBJS:.o=.c) $(SAIMPTLOGFIN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAIMPTLOGFIN_C_OBJS:.o=.c) $(SAIMPTLOGFIN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saordinvexp.lint: $(SAORDINVEXP_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAORDINVEXP_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

saimptlogtdup_upd.lint: $(SAIMPTLOGTDUP_UPD_C_OBJS:.o=.c) $(SAIMPTLOGTDUP_UPD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAIMPTLOGTDUP_UPD_C_OBJS:.o=.c) $(SAIMPTLOGTDUP_UPD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

ang_saplgen.lint: $(ANG_SAPLGEN_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(ANG_SAPLGEN_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sapreexp.lint: $(SAPREEXP_C_OBJS:.o=.c) $(SAPREEXP_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAPREEXP_C_OBJS:.o=.c) $(SAPREEXP_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sapreexp.lint: $(SAPREPOST_C_OBJS:.o=.c) $(SAPREPOST_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAPREPOST_C_OBJS:.o=.c) $(SAPREPOST_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sapurge.lint: $(SAPURGE_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAPURGE_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sarules.lint: $(SARULES_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SARULES_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

sastdycr.lint: $(SASTDYCR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SASTDYCR_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

satotals.lint: $(SATOTALS_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SATOTALS_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

savouch.lint: $(SAVOUCH_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(SAVOUCH_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include resa.d
