#--- Object file groupings.

XXADEO_ITEM_LFCY_PREPROC_PC_OBJS = xxadeo_item_lfcy_preproc.o

XXADEO_ITEM_LFCY_PC_OBJS = xxadeo_item_lfcy.o

XXADEO_ITEM_ASSORTMENT_PC_OBJS = xxadeo_item_assortment.o

XXADEO_ITEM_ASSORTMENT_PREPROC_PC_OBJS = xxadeo_item_assortment_preproc.o

XXADEO_ITEM_ASSORTMENT_PURGE_PC_OBJS = xxadeo_item_assortment_purge.o

XXADEO_PC_SRCS = \
   $(XXADEO_ITEM_LFCY_PC_OBJS:.o=.pc) \
   $(XXADEO_ITEM_LFCY_PREPROC_PC_OBJS:.o=.pc) \
   $(XXADEO_ITEM_ASSORTMENT_PC_OBJS:.o=.pc) \
   $(XXADEO_ITEM_ASSORTMENT_PREPROC_PC_OBJS:.o=.pc) \
   $(XXADEO_ITEM_LFCY_ASSORTMENT_PURGE_PC_OBJS:.o=.pc)

#--- Target groupings.

XXADEO_GEN_EXECUTABLES = \
   xxadeo_item_lfcy \
   xxadeo_item_lfcy_preproc \
   xxadeo_item_assortment \
   xxadeo_item_assortment_preproc \
   xxadeo_item_assortment_purge

XXADEO_EXECUTABLES = \
   $(XXADEO_GEN_EXECUTABLES)

XXADEO_LIBS =

XXADEO_INCLUDES =

#--- External systems

#--- Put list of all targets for xxadeo.mk here

XXADEO_TARGETS = \
   $(XXADEO_EXECUTABLES) \
   $(XXADEO_EXECUTABLES:=.lint) \
   $(XXADEO_PC_SRCS:.pc=.o) \
   $(XXADEO_PC_SRCS:.pc=.c) \
   xxadeo-all \
   xxadeo \
   xxadeo-clean
