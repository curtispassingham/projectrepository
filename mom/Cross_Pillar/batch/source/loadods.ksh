#! /bin/ksh
#-------------------------------------------------------------------------------------
# File:  loadods.ksh
# Desc:  This shell script will be used to load data into s9t_folder table.
#        This script will be called from ld_iindfiles.ksh passing input file as parameter.
#-------------------------------------------------------------------------------------
# Common functions library and environment variables
 . ${MMHOME}/oracle/lib/src/rmsksh.lib
pgmName='loadods.ksh'
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date

# File locations
LOGDIR="${MMHOME}/log"
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate
ERRINDFILE=err.ind
FATAL=255
NON_FATAL=1
OK=0

USAGE="Usage: $pgmName.$pgmExt <connect> <input file>"

function CHECK_EXIST_IN_DB
{
   # Retrieve process to process into an indexed array
   delExistFile=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   delete from s9t_folder
    where file_name = '${fileNameWithExt}';
   COMMIT;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Unable to check if file exist in database." "CHECK_EXIST_IN_DB" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

  return ${OK}
}

function GET_NEXT_SEQUENCE
{
   newSeq=`$ORACLE_HOME/bin/sqlplus -s ${connectStr} <<EOF
   set pause off
   set pagesize 0
   set feedback off
   set verify off
   set heading off
   set echo off
   select s9t_folder_seq.nextval from dual;
   exit;
   EOF`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Program failed to get the next sequence for s9t_folder." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

   # Check the returned sequence
   if [ -z ${newSeq} ]; then
      LOG_ERROR "Unable to retrieve the sequence value from s9t_folder_seq." "GET_NEXT_SEQUENCE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   else
      LOG_MESSAGE "Next sequence value retrieved." "GET_NEXT_SEQUENCE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
   fi
  return ${OK}
}

function CREATE_CTL_FILE
{
   # Create ctl file
   cat > "${MMHOME}/log/${fileName}.ctl" <<EOF
   LOAD DATA
   infile "${MMHOME}/log/${fileName}.data"
   APPEND
   INTO TABLE S9T_FOLDER
   fields terminated by "," optionally enclosed by '"'
   TRAILING NULLCOLS
   ( file_id,
     file_name,
     file_path filler char(4000),
     ODS_BLOB LOBFILE(file_path) TERMINATED BY EOF)
EOF

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "Program failed while creating control file." "CREATE_CTL_FILE" ${FATAL} ${ERRORFILE} ${LOGFILE} ${pgmName}
      exit ${FATAL}
   fi

  return ${OK}
}
#-------------------------------------------------------------------------
#                               MAIN
#-------------------------------------------------------------------------
# Test for the number of input arguments
if [ $# -lt 2 ]
then
   echo $USAGE
   exit ${NON_FATAL}
fi
#------------------------------------------------------------
# Validate input parameters
#------------------------------------------------------------
connectStr=$1
USER=${connectStr%/*}
inputFile=$2
fileNameWithExt=`basename $2`
fileName=${fileNameWithExt%.*}

LOG_MESSAGE "${pgmName}.${pgmExt} started by ${USER} for input file ${inputFile}." "MAIN function" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
CHECK_EXIST_IN_DB
GET_NEXT_SEQUENCE
echo "${newSeq},${fileNameWithExt},\"${inputFile}\"" > "${MMHOME}/log/${fileName}.data"
CREATE_CTL_FILE
CTLFILE="${MMHOME}/log/$fileName.ctl"
DATAFILE="${MMHOME}/log/$fileName.data"

dtStamp=`date +"%G%m%d%H%M%S"`
sqlldrFile=${fileName##*/}
sqlldrLog=${MMHOME}/log/$sqlldrFile.${dtStamp}.log
sqlldr ${connectStr} silent=feedback,header \
   control=${CTLFILE} \
   log=${sqlldrLog} \
   data=${DATAFILE} \
   bad=${MMHOME}/log/$sqlldrFile.bad \
   discard=${MMHOME}/log/$sqlldrFile.dsc 
sqlldr_status=$?

# Check execution status
if [[ $sqlldr_status != ${OK} ]]; then
   LOG_MESSAGE "${fileName##*/}; Error while loading file." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
else
   LOG_MESSAGE "${fileName##*/}; Completed file loading." "LOAD_FILE" ${OK} ${LOGFILE} ${pgmName} ${pgmPID}
    rm -f $CTLFILE
    rm -f $DATAFILE
    rm -f $sqlldrLog
fi
exit ${OK}