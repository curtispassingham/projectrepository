include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS = 
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/proc/src/recs_include.mk

#--- Standard targets.

ALL: recs-ALL FORCE

all: recs-all FORCE

libs: recs-libs FORCE

includes: recs-includes FORCE

libchange: recs-libchange FORCE

clean: recs-clean FORCE

clobber: recs-clobber FORCE

install: recs-install FORCE

lint: recs-lint FORCE

depend: recs-depend

FORCE:

#--- recs targets

recs: recs-all FORCE

recs-ALL: recs-libs $(ReCS_EXECUTABLES) FORCE

recs-all: recs-libs $(ReCS_GEN_EXECUTABLES) FORCE

recs-libs: recs-includes $(ReCS_LIBS) FORCE

recs-includes: $(ReCS_INCLUDES) FORCE

recs-libchange: FORCE
	rm -f $(ReCS_EXECUTABLES)

recs-clean: FORCE
	rm -f $(ReCS_C_SRCS:.c=.o)
	rm -f $(ReCS_PC_SRCS:.pc=.o)
	rm -f $(ReCS_PC_SRCS:.pc=.c)
	rm -f $(ReCS_PC_SRCS:.pc=.lis)
	rm -f $(ReCS_EXECUTABLES:=.lint)
	ReCS_LIBS="$(ReCS_LIBS)"; \
	for f in $$ReCS_LIBS; \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

recs-clobber: libchange clean FORCE
	rm -f $(ReCS_LIBS)
	rm -f $(ReCS_INCLUDES)

recs-install: FORCE
	-@ReCS_EXECUTABLES="$(ReCS_EXECUTABLES)"; \
	for f in $$ReCS_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

recs-lint: FORCE
	-@ReCS_EXECUTABLES="$(ReCS_EXECUTABLES)"; \
	for f in $$ReCS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/recs.mk $$e.lint; \
	done


recs-depend: recs.d

recs.d: $(ReCS_C_SRCS) $(ReCS_PC_SRCS)
	@echo "Making dependencies recs.d..."
	@$(MAKEDEPEND) $(ReCS_C_SRCS) $(ReCS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

#--- Executable targets.

cmpprg: $(CMPPRG_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CMPPRG_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

cmpupld: $(CMPUPLD_PC_OBJS)
	$(LD) -o `basename $@` `echo $(CMPUPLD_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

#--- Executable lint targets.

cmpprg.lint: $(CMPPRG_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CMPPRG_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) 'basename $@'

cmpupld.lint: $(CMPUPLD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(CMPUPLD_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) 'basename $@'

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include recs.d


