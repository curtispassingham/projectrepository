#--- Object file groupings.

SAESCHEAT_PC_OBJS = \
	saescheat.o \
	saescheat_nextesn.o

SAEXPACH_PC_OBJS = saexpach.o

SAEXPGL_PC_OBJS = saexpgl.o

SAEXPIM_PC_OBJS = saexpim.o

saexpdw_PC_OBJS = saexpdw.o

SAEXPRMS_PC_OBJS = saexprms.o

SAEXPSIM_PC_OBJS = saexpsim.o

SAEXPUAR_PC_OBJS = saexpuar.o

SAGETREF_PC_OBJS = sagetref.o

SAIMPADJ_PC_OBJS = saimpadj.o

SAIMPTLOG_C_OBJS = \
	saimptlog.o \
	saimptlog_ccval.o \
	saimptlog_final.o \
	saimptlog_init.o \
	saimptlog_manval.o \
	saimptlog_output.o \
	saimptlog_rtlog.o \
	saimptlog_sqlldr.o \
	saimptlog_tdup.o \
	saimptlog_loadtdup.o

SAIMPTLOG_PC_OBJS = \
	saimptlog_uom.o \
	saimptlog_datastat.o \
	saimptlog_nextesn.o \
	saimptlog_nextmtsn.o \
	saimptlog_nexttsn.o \
	saimptlog_nextvsn.o \
	saimptlog_rounding.o

SAIMPTLOGI_C_OBJS = \
	saimptlog.o \
	saimptlog_ccval.o \
	saimptlog_final.o \
	saimptlog_init.o \
	saimptlog_manval.o \
	saimptlog_output.o \
	saimptlog_rtlog.o \
	saimptlog_tdup.o \
	saimptlog_loadtdup.o
SAIMPTLOGI_PC_OBJS = \
	saimptlog_uom.o \
	saimptlog_datastat.o \
	saimptlog_insert.o \
	saimptlog_nextesn.o \
	saimptlog_nextmtsn.o \
	saimptlog_nexttsn.o \
	saimptlog_nextvsn.o \
	saimptlog_rounding.o

SAIMPTLOGFIN_C_OBJS = 
SAIMPTLOGFIN_PC_OBJS = \
	saimptlogfin.o \
	saimptlog_nextbgsn.o

SAIMPTLOGTDUP_UPD_C_OBJS = \
	saimptlog_loadtdup.o
SAIMPTLOGTDUP_UPD_PC_OBJS = \
	saimptlogtdup_upd.o

ANG_SAPLGEN_PC_OBJS = ang_saplgen.o

SAORDINVEXP_PC_OBJS = saordinvexp.o

SAPREEXP_PC_OBJS = sapreexp.o

SAPREPOST_PC_OBJS = saprepost.o

SAPURGE_PC_OBJS = sapurge.o

SARULES_PC_OBJS = sarules.o

SASTDYCR_PC_OBJS = sastdycr.o

SATOTALS_PC_OBJS = satotals.o

SAVOUCH_PC_OBJS = savouch.o

ReSA_C_SRCS = \
	$(SAIMPTLOG_C_OBJS:.o=.c) \
	$(SAIMPTLOGI_C_OBJS:.o=.c) \
	$(SAIMPTLOGFIN_C_OBJS:.o=.c) \
	$(SAIMPTLOGTDUP_UPD_C_OBJS:.o=.c)

ReSA_PC_SRCS = \
	$(ANG_SAPLGEN_PC_OBJS:.o=.pc) \
	$(SAESCHEAT_PC_OBJS:.o=.pc) \
	$(SAGETREF_PC_OBJS:.o=.pc) \
	$(SAIMPADJ_PC_OBJS:.o=.pc) \
	$(SAIMPTLOG_PC_OBJS:.o=.pc) \
	$(SAIMPTLOGI_PC_OBJS:.o=.pc) \
	$(SAIMPTLOGFIN_PC_OBJS:.o=.pc) \
	$(SAIMPTLOGTDUP_UPD_PC_OBJS:.o=.pc) \
	$(SAORDINVEXP_PC_OBJS:.o=.pc) \
	$(SAPREEXP_PC_OBJS:.o=.pc) \
	$(SAPREPOST_PC_OBJS:.o=.pc) \
	$(SAPURGE_PC_OBJS:.o=.pc) \
	$(SARULES_PC_OBJS:.o=.pc) \
	$(SASTDYCR_PC_OBJS:.o=.pc) \
	$(SATOTALS_PC_OBJS:.o=.pc) \
	$(SAVOUCH_PC_OBJS:.o=.pc)

#--- Target groupings.

ReSA_GEN_EXECUTABLES = \
	ang_saplgen \
	saescheat \
	sagetref \
	saimpadj \
	saimptlog \
	saimptlogi \
	saimptlogfin \
	saimptlogtdup_upd \
	saordinvexp \
	sapreexp \
	saprepost \
	sapurge \
	sarules \
	sastdycr \
	satotals \
	savouch

ReSA_EXECUTABLES = \
	$(ReSA_GEN_EXECUTABLES)

ReSA_LIBS = 

ReSA_INCLUDES = 

#--- External systems

ReSA_ACH_EXECUTABLES = \
	saexpach

ReSA_GL_EXECUTABLES = \
	saexpgl

ReSA_IM_EXECUTABLES = \
	saexpim

ReSA_RDW_EXECUTABLES = \
	saexpdw

ReSA_RMS_EXECUTABLES = \
	saexprms

ReSA_SIM_EXECUTABLES = \
	saexpsim

ReSA_UAR_EXECUTABLES = \
	saexpuar

#--- All

ReSA_ALL_C_SRCS = \
	$(ReSA_C_SRCS)

ReSA_ALL_PC_SRCS = \
	$(ReSA_PC_SRCS) \
	$(SAEXPACH_PC_OBJS:.o=.pc) \
	$(SAEXPGL_PC_OBJS:.o=.pc) \
	$(SAEXPIM_PC_OBJS:.o=.pc) \
	$(saexpdw_PC_OBJS:.o=.pc) \
	$(SAEXPRMS_PC_OBJS:.o=.pc) \
	$(SAEXPSIM_PC_OBJS:.o=.pc) \
	$(SAEXPUAR_PC_OBJS:.o=.pc)

ReSA_ALL_EXECUTABLES = \
	$(ReSA_EXECUTABLES) \
	$(ReSA_ACH_EXECUTABLES) \
	$(ReSA_GL_EXECUTABLES) \
	$(ReSA_IM_EXECUTABLES) \
	$(ReSA_RDW_EXECUTABLES) \
	$(ReSA_RMS_EXECUTABLES) \
	$(ReSA_SIM_EXECUTABLES) \
	$(ReSA_UAR_EXECUTABLES)

ReSA_ALL_LIBS = \
	$(ReSA_LIBS)

ReSA_ALL_INCLUDES = \
	$(ReSA_INCLUDES)

#--- Put list of all targets for resa.mk here

ReSA_TARGETS = \
	$(ReSA_ALL_EXECUTABLES) \
	$(ReSA_ALL_C_SRCS:.c=.o) \
	$(ReSA_ALL_PC_SRCS:.pc=.o) \
	$(ReSA_ALL_PC_SRCS:.pc=.c) \
	$(ReSA_ALL_LIBS) \
	$(ReSA_ALL_INCLUDES) \
	resa-all \
	resa \
	resa-ALL \
	resa-libs \
	resa-includes \
	resa-clobber \
	resa-libchange \
	resa-clean \
	resa-install \
	resa-lint \
	resa-depend \
	resa-ach \
	resa-im \
	resa-rdw \
	resa-rms \
	resa-uar \
	ach-resa \
	im-resa \
	rdw-resa \
	rms-resa \
	uar-resa
