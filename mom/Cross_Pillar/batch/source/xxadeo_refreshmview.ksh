#!/bin/ksh 
#----------------------------------------------------------------------------------------#
#  File   : xxadeo_refreshmview.ksh                                                      #
#  Desc   : This korn shell script refreshes all materialized view of the key passed     #
#           as input. The second optional parameter is whether the underlying            #
#           materialized views that the input mview is made of should also be            #
#           refreshed or not.                                                            #
#           This script does a complete refresh of the materialized view.                #
#  Company: ADEO                                                                         #
#  Created: Elsa Barros                                                                  #
#  Date   : 18-10-2018                                                                   #
#----------------------------------------------------------------------------------------#

# Test for the number of input arguments
if [ $# -lt 2 ]
then
   echo 'USAGE: xxadeo_refreshmview.ksh <connect> <key of the mview> <[ nested refresh indicator Y/N ]>'
   exit 1
elif [ $# -eq 2 ] 
then
   NESTED="TRUE"
elif [ $# -eq 3 ]  
then 
    case "$3" in 
    "Y")NESTED="TRUE"
    ;;
    "N")NESTED="FALSE"
    ;;
    *) echo 'USAGE: xxadeo_refreshmview.ksh <connect> <key of the mview> <[ nested refresh indicator Y/N ]>'
    exit 1
    ;;
    esac 
else
   echo 'USAGE: xxadeo_refreshmview.ksh <connect> <key of the mview> <[ nested refresh indicator Y/N ]>'
   exit 1
fi

# global variables
 
CONNECT=$1
MVIEW_KEY=$2

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date


LOGFILE="${MMHOME}/log/$exeDate.log"
ERRFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind 

OK=0 
FATAL=255 
NON_FATAL=1 

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

###############################################################################################

### Check if err file already exists. If yes, rename it.
if [ -e $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

### Check if log file already exists. If yes, rename it.
if [ -e $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi


USAGE="Usage: `basename $0`  <connect>\n"

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------------
# Function Name: REFRESH_MVIEW
# Purpose      : Refresh Materialized View
#-------------------------------------------------------------------------------
function REFRESH_MVIEW
{
   ERROR_MESSAGE=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                  set serveroutput off
                  set feedback off
                  set heading off
                  set trimspool on
                  set pagesize 0
                  set linesize 7000
                 
                  variable o_error_message char(2000);
              DECLARE		 
                 --
                 PACKAGE_EXCEPTION EXCEPTION; 
                 --
                 L_error_message VARCHAR2(2000) := null;
				 L_mview_name    VARCHAR2(50);
                 --
                 cursor C_get_mview is
                   select mv_name
                     from xxadeo_m_view_refresh_cfg
                    where upper(mv_scope) = upper('${MVIEW_KEY}');				 
                 --				 
              BEGIN
                --
				for rec in C_get_mview loop
				  --
				  L_mview_name := rec.mv_name;
				  --
                  DBMS_MVIEW.REFRESH(L_mview_name,'C',NULL,FALSE,FALSE,0,0,0,FALSE,$NESTED,FALSE);
                  --  
			    end loop;
				--
              EXCEPTION     
                WHEN PACKAGE_EXCEPTION THEN
                  :o_error_message:=SUBSTR(L_error_message, 1, 255); 
                WHEN OTHERS THEN
                  ROLLBACK;
                  :o_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247);
              END;
	          /
              PRINT :O_error_message; 
EOF`   
	  
    if [[ -n ${ERROR_MESSAGE} ]]; then
       LOG_MESSAGE "Aborted in process."          
       echo -e  "" >> $LOGFILE
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"          
       echo -e  "${ERROR_MESSAGE}\n" >> $ERRFILE          
       
       return ${FATAL}
    fi
	
    
    return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: MAIN_PROCESS
# Purpose      : Function to spool the files to the filesytem
#-------------------------------------------------------------------------
function MAIN_PROCESS
{   
   REFRESH_MVIEW
   if [[ $? -ne ${OK} ]]; then
       return ${FATAL}   
   fi
}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
echo $CONNECT
echo $ORACLE_HOME 

output=$($ORACLE_HOME/bin/sqlplus $CONNECT < /dev/null | grep 'Connected to' | wc -l) 
echo ${output}
if [ ${output} -eq 0 ];then 
   LOG_MESSAGE "Logon Error" 
   echo  -e "" >> ${LOGFILE} 
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
   exit ${FATAL} 
fi 

USER=`sqlplus -s $CONNECT << EOF
      set heading off;
      set pagesize 0;
      select user from dual;
      exit;
EOF`
	  

# Log Begin Work
LOG_MESSAGE "Started by ${USER}"
echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"

# Call Process
MAIN_PROCESS

#Log finished work
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
else
   LOG_MESSAGE "Terminated Successfully xxadeo_refreshmview by ${USER}"
   echo  -e "" >> ${LOGFILE}
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"
fi
 
exit ${OK}
