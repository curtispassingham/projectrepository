#--- Object file groupings.

CMPPRG_PC_OBJS = cmpprg.o

CMPUPLD_PC_OBJS = cmpupld.o

ReCS_C_SRCS = 

ReCS_PC_SRCS = \
	$(CMPPRG_PC_OBJS:.o=.pc) \
	$(CMPUPLD_PC_OBJS:.o=.pc)

#--- Target groupings.

ReCS_GEN_EXECUTABLES = \
	cmpprg \
	cmpupld

ReCS_EXECUTABLES= \
	$(ReCS_GEN_EXECUTABLES)

ReCS_LIBS = 

ReCS_INCLUDES = 

#--- Put list of all targets for recs.mk here

ReCS_TARGETS = \
	$(ReCS_EXECUTABLES) \
	$(ReCS_EXECUTABLES:=.lint) \
	$(ReCS_C_SRCS:.c=.o) \
	$(ReCS_PC_SRCS:.pc=.o) \
	$(ReCS_PC_SRCS:.pc=.c) \
	$(ReCS_LIBS) \
	$(ReCS_INCLUDES) \
	recs-all \
	recs \
	recs-ALL \
	recs-libs \
	recs-includes \
	recs-clobber \
	recs-libchange \
	recs-clean \
	recs-install \
	recs-lint \
	recs-depend
