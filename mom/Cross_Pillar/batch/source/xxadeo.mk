
include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS =
PRODUCT_CFLAGS =
PRODUCT_LDFLAGS = -L.
PRODUCT_LINT_FLAGS =
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk

#--- include ${MMHOME}/oracle/proc/src/xxadeo_include.mk
include ./xxadeo_include.mk

#--- Standard targets.

ALL: $(XXADEO_EXECUTABLES)

all: $(XXADEO_GEN_EXECUTABLES)

#--- RMS specific

clean:
	rm -f $(XXADEO_PC_SRCS:.pc=.o)
	rm -f $(XXADEO_PC_SRCS:.pc=.c)
	rm -f $(XXADEO_PC_SRCS:.pc=.lis)
	rm -f $(XXADEO_EXECUTABLES:=.lint)
	rm -f $(XXADEO_EXECUTABLES)

install: FORCE
	-@XXADEO_EXECUTABLES="$(XXADEO_EXECUTABLES)"; \
	for f in $$XXADEO_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_PROC_BIN)/$$f ] || [ $$f -nt $(RETEK_PROC_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_PROC_BIN)/$$f"; \
				cp $$f $(RETEK_PROC_BIN)/$$f; \
				chmod $(MASK) $(RETEK_PROC_BIN)/$$f; \
			fi; \
		fi; \
	done

rms-lint: FORCE
	-@RMS_EXECUTABLES="$(RMS_EXECUTABLES)"; \
	for e in $$RMS_EXECUTABLES; \
	do \
		$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $$e.lint; \
	done


rms-depend: rms.d

rms.d: $(RMS_C_SRCS) $(RMS_PC_SRCS)
	@echo "Making dependencies $@..."
	@$(MAKEDEPEND) $(RMS_C_SRCS) $(RMS_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`


#--- Executable targets.


xxadeo_item_lfcy: $(XXADEO_ITEM_LFCY_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_LFCY_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_item_lfcy_preproc: $(XXADEO_ITEM_LFCY_PREPROC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_LFCY_PREPROC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_item_assortment: $(XXADEO_ITEM_ASSORTMENT_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_ASSORTMENT_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_item_assortment_preproc: $(XXADEO_ITEM_ASSORTMENT_PREPROC_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_ASSORTMENT_PREPROC_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

xxadeo_item_assortment_purge: $(XXADEO_ITEM_ASSORTMENT_PURGE_PC_OBJS)
	$(LD) -o `basename $@` `echo $(XXADEO_ITEM_ASSORTMENT_PURGE_PC_OBJS) | xargs -n1 basename` $(LDFLAGS)
	chmod $(MASK) `basename $@`

#--- Executable lint targets.

xxadeo_item_lfcy.lint: $(XXADEO_ITEM_LFCY_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXDEO_ITEM_LFCY_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

xxadeo_item_lfcy_proproc.lint: $(XXADEO_ITEM_LFCY_PREPROC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) `echo $(XXDEO_ITEM_LFCY_PREPROC_PC_OBJS:.o=.c) | xargs -n1 basename` $(LINT_LDFLAGS) >`basename $@` 2>&1
	chmod $(MASK) `basename $@`

#--- Library targets.

#--- Include targets.

#--- Object dependencies.

include xxadeo.d
