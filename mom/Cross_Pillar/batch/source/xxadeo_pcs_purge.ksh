#!/bin/ksh 
#----------------------------------------------------------------------------------------#
#  File   : xxadeo_pcs_purge.ksh                                                         #
#  Desc   : The batch will fetch all records, from the store level and BU level tables,  #
#           where status is active and where the effective date is the next day.         #
#           With the same combinations « Item/supplier/store » and « Item/supplier/BU », #
#           returned from the first fetch, the batch will fetch all records where status #
#           is active and where the effective date is smaller than VDate +1 and update   #
#           the status of all records to ‘I’.                                            #
#           After the processing has been completed, the purge process will run.         #
#  Company: ADEO                                                                         #
#  Created: Elsa Barros                                                                  #
#  Date   : 15-10-2018                                                                   #
#----------------------------------------------------------------------------------------#

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo 'USAGE: xxadeo_pcs_purge.ksh </@tnsaliasname> <purge_days>'
   exit 1
fi

# global variables
 
PURGE_DAYS=`echo $2 ` 
CONNECT=$1

pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date


LOGFILE="${MMHOME}/log/$exeDate.log"
ERRFILE="${MMHOME}/error/err.$pgmName.$exeDate.$pgmPID"
ERRINDFILE=err.ind 

OK=0 
FATAL=255 
NON_FATAL=1 

echo 'LOGFILE=' $LOGFILE
echo 'ERRFILE=' $ERRFILE

###############################################################################################

### Check if err file already exists. If yes, rename it.
if [ -e $ERRFILE ]
then
   mv -f $ERRFILE $ERRFILE.`date +%H%M%S`
fi

### Check if log file already exists. If yes, rename it.
if [ -e $LOGFILE ]
then
   mv -f $LOGFILE $LOGFILE.`date +%H%M%S`
fi


USAGE="Usage: `basename $0`  <connect>\n"

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------------
# Function Name: PCS_LIFE_CYCLE
# Purpose      : calls the package XXADEO_PCS_SQL.PCS_LIFE_CYCLE
#-------------------------------------------------------------------------------
function PCS_LIFE_CYCLE
{
   ERROR_MESSAGE=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                  set serveroutput off
                  set feedback off
                  set heading off
                  set trimspool on
                  set pagesize 0
                  set linesize 7000
                 
                  variable o_error_message char(2000);
              DECLARE		 
                 --
                 PACKAGE_EXCEPTION EXCEPTION; 
                 --
                 L_error_message VARCHAR2(2000) := null;
                 --     
              BEGIN
                --
		        IF XXADEO_PCS_SQL.PCS_LIFE_CYCLE(O_error_message    => L_error_message) = FALSE THEN
                  --
                  RAISE PACKAGE_EXCEPTION;
                  --
                END IF;
                --
                COMMIT;
                --     
              EXCEPTION     
                WHEN PACKAGE_EXCEPTION THEN
                  :o_error_message:=SUBSTR(L_error_message, 1, 255); 
                WHEN OTHERS THEN
                  ROLLBACK;
                  :o_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247);
              END;
	          /
              PRINT :O_error_message; 
EOF`   
	  
    if [[ -n ${ERROR_MESSAGE} ]]; then
       LOG_MESSAGE "Aborted in process."          
       echo -e  "" >> $LOGFILE
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"          
       echo -e  "${ERROR_MESSAGE}\n" >> $ERRFILE          
       
       return ${FATAL}
    fi
	
    
    return ${OK}
}
#-------------------------------------------------------------------------------
# Function Name: PCS_PURGE_PCS
# Purpose      : calls the package XXADEO_PCS_SQL.PURGE_PCS
#-------------------------------------------------------------------------------
function PCS_PURGE
{

   ERROR_MESSAGE=`$ORACLE_HOME/bin/sqlplus -s $CONNECT <<EOF
                  set serveroutput off
                  set feedback off
                  set heading off
                  set trimspool on
                  set pagesize 0
                  set linesize 7000
                 
                  variable o_error_message char(2000);
              DECLARE		 
                 --
                 PACKAGE_EXCEPTION EXCEPTION; 
                 --
                 L_error_message VARCHAR2(2000) := null;
				 L_purge_days    NUMBER         := 0;
                 --     
              BEGIN
                --
				L_purge_days := '$PURGE_DAYS';
				--
		        IF XXADEO_PCS_SQL.PURGE_PCS(O_error_message    => L_error_message,
			                                I_purge_days       => L_purge_days) = FALSE THEN
                  --
                  RAISE PACKAGE_EXCEPTION;
                  --
                END IF;
                --
                COMMIT;
                --     
              EXCEPTION     
                WHEN PACKAGE_EXCEPTION THEN
                  :o_error_message:=SUBSTR(L_error_message, 1, 255); 
                WHEN OTHERS THEN
                  ROLLBACK;
                  :o_error_message:='@Error :'||SUBSTR(SQLERRM, 1, 247);
              END;
	          /
              PRINT :O_error_message; 
EOF`   
	  
    if [[ -n ${ERROR_MESSAGE} ]]; then
       LOG_MESSAGE "Aborted in process."          
       echo -e  "" >> $LOGFILE
       echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"          
       echo -e  "${ERROR_MESSAGE}\n" >> $ERRFILE          
       
       return ${FATAL}
    fi
    return ${OK}
}
#-------------------------------------------------------------------------
# Function Name: MAIN_PROCESS
# Purpose      : Function to spool the files to the filesytem
#-------------------------------------------------------------------------
function MAIN_PROCESS
{   
   PCS_LIFE_CYCLE
   if [[ $? -ne ${OK} ]]; then
       return ${FATAL}   
   fi
   PCS_PURGE
   if [[ $? -ne ${OK} ]]; then
       return ${FATAL}   
   fi
   return ${OK}
}
#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
echo $CONNECT
echo $ORACLE_HOME 

output=$($ORACLE_HOME/bin/sqlplus $CONNECT < /dev/null | grep 'Connected to' | wc -l) 
echo ${output}
if [ ${output} -eq 0 ];then 
   LOG_MESSAGE "Logon Error" 
   echo  -e "" >> ${LOGFILE} 
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg" 
   exit ${FATAL} 
fi 

USER=`sqlplus -s $CONNECT << EOF
      set heading off;
      set pagesize 0;
      select user from dual;
      exit;
EOF`
	  

# Log Begin Work
LOG_MESSAGE "Started by ${USER}"
echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"

# Call Process
MAIN_PROCESS

#Log finished work
if [[ $? -ne ${OK} ]]; then
   exit ${FATAL}
else
   LOG_MESSAGE "Terminated Successfully xxadeo_pcs_purge by ${USER}"
   echo  -e "" >> ${LOGFILE}
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg"
fi

exit ${OK}
