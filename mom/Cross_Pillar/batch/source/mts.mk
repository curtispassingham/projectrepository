.SUFFIXES:
.SUFFIXES:  .random-suffix-to-suppress-HP-warning

include ${MMHOME}/oracle/proc/src/rms_include.mk
include ${MMHOME}/oracle/proc/src/rtm_include.mk
include ${MMHOME}/oracle/proc/src/recs_include.mk
include ${MMHOME}/oracle/proc/src/resa_include.mk
include ${MMHOME}/oracle/proc/src/fif11i_include.mk

$(RMS_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $@

$(RTM_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/rtm.mk $@

$(ReCS_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/recs.mk $@

$(ReSA_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/resa.mk $@

$(FIF_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/fif11i.mk $@

clobber libchange clean install lint depend: FORCE
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/rms.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/rtm.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/recs.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/resa.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/proc/src/fif11i.mk $@
FORCE:
