#! /bin/ksh
#-------------------------------------------------------------------------
#  Desc:  UNIX shell cleanup from the RMS_ASYNC_STATUS and RMS_ASYNC_RETRY.
#-------------------------------------------------------------------------
pgmName=`basename $0`
pgmName=${pgmName##*/}    # remove the path
pgmExt=${pgmName##*.}     # get the extension
pgmName=${pgmName%.*}     # get the program name
pgmPID=$$                 # get the process ID
exeDate=`date +"%h_%d"`   # get the execution date
LOGFILE="${MMHOME}/log/$exeDate.log"
ERRORFILE="${MMHOME}/error/err.$pgmName."$exeDate

OK=0
FATAL=255

USAGE="Usage: `basename $0` [-t <# days>] <connect> "

#-------------------------------------------------------------------------
# Function Name: LOG_ERROR
# Purpose      : Log the error messages to the error file.
#-------------------------------------------------------------------------
function LOG_ERROR
{
   errMsg=`echo $1`       # echo message to a single line
   errFunc=$2
   retCode=$3

   dtStamp=`date +"%G%m%d%H%M%S"`
   echo "$pgmName~$dtStamp~$errFunc~$errMsg" >> $ERRORFILE
   if [[ $retCode -eq ${FATAL} ]]; then
      LOG_MESSAGE "Aborted in" $errFunc $retCode
   fi
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: LOG_MESSAGE
# Purpose      : Log the messages to the log file.
#-------------------------------------------------------------------------
function LOG_MESSAGE
{
   logMsg=`echo $1`       # echo message to a single line
   logFunc=$2
   retCode=$3

   dtStamp=`date +"%a %b %e %T"`
   echo "$dtStamp Program: $pgmName: PID=$pgmPID: $logMsg $logFunc" >> $LOGFILE
   return $retCode
}

#-------------------------------------------------------------------------
# Function Name: EXEC_SQL
# Purpose      : Used for executing the sql statements.
#-------------------------------------------------------------------------

function EXEC_SQL
{
   sqlTxt=$*

   sqlReturn=`echo "set feedback off;
      set heading off;
      set term off;
      set verify off;
      set serveroutput on size 1000000;

      VARIABLE GV_return_code    NUMBER;
      VARIABLE GV_script_error   CHAR(255);

      EXEC :GV_return_code   := 0;
      EXEC :GV_script_error  := NULL;

      WHENEVER SQLERROR EXIT ${FATAL}
      $sqlTxt
      /

      print :GV_script_error;
      exit  :GV_return_code;
      " | sqlplus -s ${CONNECT}`

   if [[ $? -ne ${OK} ]]; then
      LOG_ERROR "${sqlReturn}" "EXEC_SQL" ${FATAL}
      return ${FATAL}
   fi

   return ${OK}
}


#------------------------------------------------------------------------------------
# Function Name: CLEANUP_QUEUE
# Purpose      : calls deletes records from the RMS_ASYNC_STATUS and RMS_ASYNC_RETRY
#------------------------------------------------------------------------------------

function CLEANUP_QUEUE
{
   sqlTxt="
      DECLARE
         L_days           NUMBER;
      BEGIN

         select ${DAYS}
           into L_days
           from dual;

         delete
           from rms_async_retry
          where rms_async_id in (select rms_async_id
                                   from rms_async_status
                                  where (trunc(sysdate) - trunc(last_update_datetime)) >= L_days);

         delete
           from rms_async_status
          where (trunc(sysdate) - trunc(last_update_datetime)) >= L_days;

          commit;

      EXCEPTION
         when OTHERS then
            ROLLBACK;
            :GV_script_error := SQLERRM;
            :GV_return_code := ${FATAL};
      END;"
   EXEC_SQL ${sqlTxt}

   if [[ $? -ne ${OK} ]]; then
      echo "Cleanup Failed" >>${ERRORFILE}
      return ${FATAL}
   else
      LOG_MESSAGE "Successfully Completed"
      return ${OK}
   fi
}

#-----------------------------------------------
# Main program starts
# Parse the command line
#-----------------------------------------------

#default
DAYS=7

# Parse the command line
while getopts t: CMD
   do
      case $CMD in
         t)  DAYS=$OPTARG;;
         *)  echo $0: Unknown option $OPTARG
             echo -e $USAGE
             exit 1;;
      esac
   done

shift $OPTIND-1

# Test for the number of input arguments
if [ $# -lt 1 ]
then
   echo $USAGE
   exit 1
fi

CONNECT=$1
USER=${CONNECT%/*}

LOG_MESSAGE "Started by ${USER}"

CLEANUP_QUEUE

exit 0
