/* std_err.h */

/* BEGIN DOC

standard variable declarations for error handling
function will be declared locally within programs
all variables are passed to the WRITE_ERROR function
(in oracle.pc) to generate more meaningful error messaging.

program is the name of the current program
	- this should be global and defined in the program
err_data is a string description of the error and will be
	defined for every call to WRITE_ERROR
table is the name of the table(s) upon which the error occured
	- it will be set for every call to WRITE_ERROR

function will be declared locally within a program and defined at
	the top of each function

macros:
	NO_DATA_FOUND checks for SQLCODE 1403
	SQL_ERROR_FOUND checks for non-zero non-1403 SQLCODE
END DOC */

char PROGRAM[30];
char err_data[1379]; /*MAX_FILENAME_LEN + 355*/
char table[255];
/* make function local to program */
/* char function[255]; */

/* macros */
#define RESOURCE_BUSY      (SQLCODE == -54)
#define NO_DATA_FOUND      (SQLCODE == 1403)
#define SQL_ERROR_FOUND    (SQLCODE != 0 && SQLCODE != 1403)
#define NUM_RECORDS_PROCESSED sqlca.sqlerrd[2]
#define DUP_VAL_FOUND      (SQLCODE == -1)

/* define error codes to be passed into WRITE_ERROR function */
#define RET_FUNCTION_ERR 103
#define RET_FILE_ERR     104
#define RET_PROC_ERR     105
#define RET_EDI_ERR      108


