/*=========================================================================

   partition.h: partitioning header file

=========================================================================*/

#ifndef PARTITION_H
#define PARTITION_H

#ifdef DEBUG
#define VERBOSE(a1) printf(a1)
#define VERBOSE2(a1,a2) printf(a1,a2)
#define VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#else
#define VERBOSE(a1)
#define VERBOSE2(a1,a2)
#define VERBOSE3(a1,a2,a3)
#define VERBOSE4(a1,a2,a3,a4)
#endif

#ifdef TDUP_DEBUG
#define TDUP_VERBOSE(a1) printf(a1)
#define TDUP_VERBOSE2(a1,a2) printf(a1,a2)
#define TDUP_VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define TDUP_VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#else
#define TDUP_VERBOSE(a1)
#define TDUP_VERBOSE2(a1,a2)
#define TDUP_VERBOSE3(a1,a2,a3)
#define TDUP_VERBOSE4(a1,a2,a3,a4)
#endif

#ifdef RRC_DEBUG
#define RRC_VERBOSE(a1) printf(a1)
#define RRC_VERBOSE2(a1,a2) printf(a1,a2)
#define RRC_VERBOSE3(a1,a2,a3) printf(a1,a2,a3)
#define RRC_VERBOSE4(a1,a2,a3,a4) printf(a1,a2,a3,a4)
#define RRC_VERBOSE5(a1,a2,a3,a4,a5) printf(a1,a2,a3,a4,a5)
#else
#define RRC_VERBOSE(a1)
#define RRC_VERBOSE2(a1,a2)
#define RRC_VERBOSE3(a1,a2,a3)
#define RRC_VERBOSE4(a1,a2,a3,a4)
#define RRC_VERBOSE5(a1,a2,a3,a4,a5)
#endif

/* behavior defines */
#define ADD_DECIMAL_POINTS


#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "retek_2.h"



#ifndef LINUX
char *strdup( const char * ); /* seems to be missing from C library proto's?? */
#endif


/*-------------------------------------------------------------------------
global variables
-------------------------------------------------------------------------*/
#ifdef MAIN_MODULE
  #define GLOBAL
#else
  #define GLOBAL extern
#endif

GLOBAL int ExitCode;       /* main program exit code */

GLOBAL long int SQLCODE;     /* oracle status code */


GLOBAL char logmsg[ 256 ];     /* message buffer for logging */

GLOBAL int gi_rej_record;     

/* function prototypes */
/* in partadd.pc */
int partition_add_half( char *ls_table_name,
                        char *ls_owner,
                        int  li_range_part_count,
                        int  li_next_half,
                        int  li_mon_upper_bound);
int partition_add_date(char *ls_table_name,
                       char *is_owner,
                       char *is_date,
                       int   li_advance_days);
int partition_add_month(char *ls_table_name,
                        char *is_owner,
                        char *is_date);

/* in partrbldu.pc */
int rebuild_unusable( char *ls_table_name,
                      char *ls_owner );
int rebuild_unusable_sub( char *ls_table_name,
                          char *ls_owner);

/* in partcheck.pc */
extern int check_expected_partition( char *ls_table_name,
                              char *ls_owner,
                              int  li_column_count,
                              char **la_check_array );

#endif
