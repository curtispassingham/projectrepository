/*=========================================================================

   partcheck.pc: check to see if the partitions in a table match what
                 is expected 

=========================================================================*/

#include "partition.h"
EXEC SQL INCLUDE SQLCA.H;


/*-------------------------------------------------------------------------
-------------------------------------------------------------------------*/
int check_expected_partition( char *ls_table_name,
                              char *ls_owner,
                              int li_column_count,
                              char **la_check_array )
{
   char *function = "check_expected_partition";
   int  li_partition_name_row_count = 0;
   char ls_partition_key_name[4001];
   int  rv = 0;  /* 0 for unexpected partition, > 0 for expected partition */
   int  i;  /* index for for loop to check index names */

   EXEC SQL DECLARE c_partition_name_row_count CURSOR FOR
        SELECT count(*) 
          FROM all_part_key_columns
         WHERE owner = :ls_owner
           AND name = :ls_table_name;

   EXEC SQL OPEN c_partition_name_row_count;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"Open c_partition_name_row_count");
      sprintf(table, "all_part_key_columns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }
   EXEC SQL FETCH c_partition_name_row_count INTO :li_partition_name_row_count;
   if ( SQL_ERROR_FOUND || NO_DATA_FOUND ) 
   {
      sprintf(err_data,"Fetch c_partition_name_row_count");
      sprintf(table, "all_part_key_columns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }
   EXEC SQL CLOSE c_partition_name_row_count;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"Close c_partition_name_row_count");
      sprintf(table, "all_part_key_columns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   if ( li_partition_name_row_count != li_column_count )
   {
      sprintf(err_data,"Unrecognized partitioning");
      sprintf(table, "");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(OK);
   }

   EXEC SQL DECLARE c_partition_key_name CURSOR FOR
        SELECT column_name 
          FROM all_part_key_columns
         WHERE owner = :ls_owner
           AND name = :ls_table_name
        ORDER BY column_position;

   EXEC SQL OPEN c_partition_key_name;
   if ( SQL_ERROR_FOUND )
   {
      sprintf(err_data,"Open c_partition_key_name");
      sprintf(table, "all_part_key_columns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   for ( i = 0; i < li_column_count; i++ )
   {
      EXEC SQL FETCH c_partition_key_name INTO :ls_partition_key_name;
      if ( SQL_ERROR_FOUND || NO_DATA_FOUND ) 
      {
         sprintf(err_data,"Fetch c_partition_key_name");
         sprintf(table, "all_part_key_columns");
         WRITE_ERROR(SQLCODE,function,table,err_data);
         return(FATAL);
      }

      if ( strcmp( ls_partition_key_name, la_check_array[i] ) == 0 ) 
      {
         rv++;
      }
   }  /* end of for loop */

   EXEC SQL CLOSE c_partition_key_name;
   if SQL_ERROR_FOUND
   {
      sprintf(err_data,"Close c_partition_key_name");
      sprintf(table, "all_part_key_columns");
      WRITE_ERROR(SQLCODE,function,table,err_data);
      return(FATAL);
   }

   if ( rv == li_column_count )
   {
      return( 1 );  /* the table is partitioned as expected */
   }
   else
   {
      return( OK );  /* the table is not partitioned as expected */
   }

} /* end of check_expected_partition */

