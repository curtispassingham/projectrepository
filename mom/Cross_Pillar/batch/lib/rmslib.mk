include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS =
PRODUCT_LDFLAGS =
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/lib/src/rmslib_include.mk

#--- Standard targets.

all: rmslib-all

libs: rmslib-libs

includes: rmslib-includes

clean: rmslib-clean FORCE

clobber: rmslib-clobber FORCE

install: rmslib-install FORCE

lint: rmslib-lint FORCE

depend: rmslib-depend

rmslib-all: rmslib-libs $(RMSLIB_EXECUTABLES)

rmslib-libs: rmslib-includes $(RMSLIB_LIBS)

rmslib-includes: $(RMSLIB_INCLUDES)

rmslib-clean: FORCE
	rm -f $(RMSLIB_C_SRCS:.c=.o)
	rm -f $(RMSLIB_C_SRCS:.c=.ln)
	rm -f $(RMSLIB_PC_SRCS:.pc=.o)
	rm -f $(RMSLIB_PC_SRCS:.pc=.c)
	rm -f $(RMSLIB_PC_SRCS:.pc=.lis)
	rm -f $(RMSLIB_PC_SRCS:.pc=.ln)
	for f in $(RMSLIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.lint; \
		rm -f $$n; \
	done

rmslib-clobber: rmslib-clean FORCE
	rm -f $(RMSLIB_EXECUTABLES)
	rm -f $(RMSLIB_LIBS)
	for f in $(RMSLIB_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

rmslib-install: FORCE
	-@RMSLIB_INCLUDES="$(RMSLIB_INCLUDES)"; \
	for f in $$RMSLIB_INCLUDES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_SRC)/$$f ] || [ $$f -nt $(RETEK_LIB_SRC)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_SRC)/$$f"; \
				cp $$f $(RETEK_LIB_SRC)/$$f; \
				chmod $(MASK) $(RETEK_LIB_SRC)/$$f; \
			fi; \
		fi; \
	done; \
	RMSLIB_EXECUTABLES="$(RMSLIB_EXECUTABLES)"; \
	for f in $$RMSLIB_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				cp $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
	done; \
	for f in $(RMSLIB_LIBS); \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				mv $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		if [ -x $$n ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$n ] || [ $$f -nt $(RETEK_LIB_BIN)/$$n ]; \
			then \
				echo "Copying $$n to $(RETEK_LIB_BIN)/$$n"; \
				cp $$n $(RETEK_LIB_BIN)/$$n; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$n; \
			fi; \
		fi; \
	done;

rmslib-lint: FORCE
	-@for l in $(RMSLIB_LIBS); \
	do \
		n=`expr $$l : 'lib\(.*\)\.a' \| $$l : 'lib\(.*\)\.$(SHARED_SUFFIX)'`; \
		$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk llib-l$$n.ln; \
	done

rmslib-depend: rmslib.d

rmslib.d: $(RMSLIB_C_SRCS) $(RMSLIB_PC_SRCS) ${MMHOME}/oracle/lib/src/oracle.mk
	@echo "Making dependencies rmslib.d..."
	@$(MAKEDEPEND) $(RMSLIB_C_SRCS) $(RMSLIB_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

${MMHOME}/oracle/lib/src/oracle.mk: ${MMHOME}/oracle/lib/src/oramake FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/retek.mk $@

FORCE:

#--- Executable targets.

#--- Executable lint targets.

#--- Library targets by product.

rms: dealinc orderscale createord dealord partition reploci l10n

orderscale: libbatchsupcstrr.a libsupcstrr.$(SHARED_SUFFIX)

createord: libbatchcreateord.a libcreateord.$(SHARED_SUFFIX)

dealord: libbatchdealord.a libdealord.$(SHARED_SUFFIX)

dealinc: libbatchdealinc.a libdealinc.$(SHARED_SUFFIX)

partition: libpartition.a

reploci: libreploci.a

l10n: libbatchl10n.a libl10n.$(SHARED_SUFFIX)

#--- Library targets.

libbatchsupcstrr.a: $(LIBBATCHSUPCSTRR_C_OBJS) $(LIBBATCHSUPCSTRR_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHSUPCSTRR_C_OBJS) $(LIBBATCHSUPCSTRR_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libsupcstrr.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBSUPCSTRR_C_OBJS) $(LIBSUPCSTRR_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBSUPCSTRR_C_OBJS) $(LIBSUPCSTRR_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

libbatchcreateord.a: $(LIBBATCHCREATEORD_C_OBJS) $(LIBBATCHCREATEORD_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHCREATEORD_C_OBJS) $(LIBBATCHCREATEORD_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libcreateord.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBCREATEORD_C_OBJS) $(LIBCREATEORD_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBCREATEORD_C_OBJS) $(LIBCREATEORD_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

libbatchdealord.a: $(LIBBATCHDEALORD_C_OBJS) $(LIBBATCHDEALORD_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHDEALORD_C_OBJS) $(LIBBATCHDEALORD_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libdealord.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBDEALORD_C_OBJS) $(LIBDEALORD_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBDEALORD_C_OBJS) $(LIBDEALORD_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

libbatchdealinc.a: $(LIBBATCHDEALINC_C_OBJS) $(LIBBATCHDEALINC_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHDEALINC_C_OBJS) $(LIBBATCHDEALINC_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libdealinc.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBDEALINC_C_OBJS) $(LIBDEALINC_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBDEALINC_C_OBJS) $(LIBDEALINC_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

libpartition.a: $(LIBPARTITION_C_OBJS) $(LIBPARTITION_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBPARTITION_C_OBJS) $(LIBPARTITION_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libreploci.a: $(LIBREPLOCI_C_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBREPLOCI_C_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libbatchl10n.a: $(LIBBATCHL10N_C_OBJS) $(LIBBATCHL10N_PC_OBJS)
	rm -f `basename $@`
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBBATCHL10N_C_OBJS) $(LIBBATCHL10N_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

libl10n.$(SHARED_SUFFIX): demo_rdbms.mk $(LIBCLNTSH) $(LIBL10N_C_OBJS) $(LIBL10N_PC_OBJS)
	$(MAKE) -f rmslib_rdbms.mk extproc_rmslib SHARED_LIBNAME=`basename $@` OBJS="`echo $(LIBL10N_C_OBJS) $(LIBL10N_PC_OBJS) | xargs -n1 basename | tr '\012' ' '`" GENCLNTSH=: GENCLNTSH64=:
	chmod $(MASK) `basename $@`

#--- Library lint targets.

llib-lbatchsupcstrr.ln: $(LIBBATCHSUPCSTRR_C_OBJS:.o=.c) $(LIBBATCHSUPCSTRR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchsupcstrr `echo $(LIBBATCHSUPCSTRR_C_OBJS:.o=.c) $(LIBBATCHSUPCSTRR_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lsupcstrr.ln: $(LIBSUPCSTRR_C_OBJS:.o=.c) $(LIBSUPCSTRR_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o supcstrr `echo $(LIBSUPCSTRR_C_OBJS:.o=.c) $(LIBSUPCSTRR_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lbatchcreateord.ln: $(LIBBATCHCREATEORD_C_OBJS:.o=.c) $(LIBBATCHCREATEORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchcreateord `echo $(LIBBATCHCREATEORD_C_OBJS:.o=.c) $(LIBBATCHCREATEORD_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lcreateord.ln: $(LIBCREATEORD_C_OBJS:.o=.c) $(LIBCREATEORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o createord `echo $(LIBCREATEORD_C_OBJS:.o=.c) $(LIBCREATEORD_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lbatchdealord.ln: $(LIBBATCHDEALORD_C_OBJS:.o=.c) $(LIBBATCHDEALORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchdealord `echo $(LIBBATCHDEALORD_C_OBJS:.o=.c) $(LIBBATCHDEALORD_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-ldealord.ln: $(LIBDEALORD_C_OBJS:.o=.c) $(LIBDEALORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o dealord `echo $(LIBDEALORD_C_OBJS:.o=.c) $(LIBDEALORD_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lbatchdealinc.ln: $(LIBBATCHDEALORD_C_OBJS:.o=.c) $(LIBBATCHDEALORD_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchdealinc `echo $(LIBBATCHDEALORD_C_OBJS:.o=.c) $(LIBBATCHDEALORD_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-ldealinc.ln: $(LIBDEALINC_C_OBJS:.o=.c) $(LIBDEALINC_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o dealinc `echo $(LIBDEALINC_C_OBJS:.o=.c) $(LIBDEALINC_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lpartition.ln: $(LIBPARTITION_C_OBJS:.o=.c) $(LIBPARTITION_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o partition `echo $(LIBPARTITION_C_OBJS:.o=.c) $(LIBPARTITION_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lreploci.ln: $(LIBREPLOCI_C_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o partition `echo $(LIBREPLOCI_C_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-lbatchl10n.ln: $(LIBBATCHL10N_C_OBJS:.o=.c) $(LIBBATCHL10N_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o batchl10n `echo $(LIBBATCHL10N_C_OBJS:.o=.c) $(LIBBATCHL10N_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

llib-ll10n.ln: $(LIBL10N_C_OBJS:.o=.c) $(LIBL10N_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o l10n `echo $(LIBL10N_C_OBJS:.o=.c) $(LIBL10N_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

#--- Oracle library targets that need to be checked.

$(LIBCLNTSH): FORCE
	@if [ ! -r $(LIBCLNTSH) ]; then \
		echo "$(LIBCLNTSH) does not exist."; \
		echo "This must be made by executing $(GENCLNTSH) while logged in as oracle."; \
		if [ -d ${ORACLE_HOME}/lib64 ]; then \
			echo "The 64 bit version of $(LIBCLNTSH) may also need to be built."; \
			echo "This can be done by executing $(GENCLNTSH64) while logged in as oracle."; \
		fi; \
		false; \
	else \
		true; \
	fi

demo_rdbms.mk: ${ORACLE_HOME}/rdbms/demo/demo_rdbms.mk FORCE
	cmp -s ${ORACLE_HOME}/rdbms/demo/demo_rdbms.mk $@ || cp ${ORACLE_HOME}/rdbms/demo/demo_rdbms.mk $@

#--- Object dependencies.

include rmslib.d
