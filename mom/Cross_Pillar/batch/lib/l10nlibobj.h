#ifndef LIBL10NOBJ_H
   #define LIBL10NOBJ_H

   /* This header contains all the objects that will be used */
   /* by the wrapper and loalized functions.                 */
   #define LEN_PROGRAM_NAME   30
   #define LEN_ENTITY         6
   #define LEN_DOC_TYPE       6
   #define LEN_DOC_ID         12

   #define NULL_PROGRAM_NAME  LEN_PROGRAM_NAME + 1
   #define NULL_ENTITY        LEN_ENTITY       + 1
   #define NULL_DOC_TYPE      LEN_DOC_TYPE     + 1
   #define NULL_DOC_ID        LEN_DOC_ID       + 1

   /* This will hold the address for the localized function */
   /* when doing a function callback.                       */
   typedef int (*func_ptr)(void*);
   func_ptr pf_func_ptr;

   /* These are the variables that will be used by the wrapper */
   /* function in identifying the localized function name to   */
   /* be used in the callback.                                 */
   char ps_function_name[NULL_PROGRAM_NAME];
   char ps_function_key[NULL_PROGRAM_NAME];
   char ps_country_id[NULL_COUNTRY_ID];
   char ps_source_entity[NULL_ENTITY];
   char ps_source_type[NULL_LOC_TYPE];
   char ps_source_id[NULL_LOC];
   char ps_doc_type[NULL_DOC_TYPE];
   char ps_doc_id[NULL_DOC_ID];

   /* This is the name of the actual bacth program and should */
   /* be populated in the main section of the batch.          */
   char ps_program_name[NULL_PROGRAM_NAME];

   /* Struct to hold the localized function list of the batch program. */
   typedef struct function_array
   {
      char       function_name[NULL_PROGRAM_NAME];
      func_ptr   function_ptr;
   }function_array;

   /* The index defines the maximum number of localized functions a batch may have. */
   function_array pa_function_array[10];

   /* Struct to hold all the localized batches and their corresponding function list. */
   typedef struct
   {
      char             batch_name[NULL_PROGRAM_NAME];
      function_array   *function_array_ptr;
      int              function_counter;
   }batch_array;

   /* Global counters to be used by the wrapper. */
   int pi_func_num;  /* Determines the actual count of localized functions for a batch. */
   int pi_base_ind;  /* Determines if localized logic will be used or not. */
   int pi_batch_num; /* Determines the number of localized batches for a certain localization. */
#endif
