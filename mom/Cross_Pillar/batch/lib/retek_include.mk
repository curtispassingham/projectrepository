#--- Object file groupings.

LIBRETEK_C_OBJS = \
	saoranumadd.o \
	saoranumdiv.o \
	saoranuminit.o \
	saoranummul.o \
	saoranummuld.o \
	saoranumqtyrtl.o \
	saoranumround.o \
	saoranumsub.o

LIBRETEK_PC_OBJS = \
	4_5_4.o \
	curconv.o \
	intrface.o \
	oracle.o \
	restart.o \
	retek_2.o

RETEK_C_SRCS = \
	$(LIBRETEK_C_OBJS:.o=.c)

RETEK_PC_SRCS = \
	$(LIBRETEK_PC_OBJS.o=.pc)

C_SRCS = \
	$(LIBRETEK_C_OBJS:.o=.c) \
	$(RMSLIB_C_OBJS:.o=.c) \
	$(RESALIB_C_OBJS:.o=.c)

PC_SRCS = \
	$(LIBRETEK_PC_OBJS:.o=.pc) \
	$(RMSLIB_PC_OBJS:.o=.pc) \
	$(RESALIB_PC_OBJS:.o=.pc)

#--- Target groupings.

RETEK_EXECUTABLES =

EXECUTABLES = \
	$(RETEK_EXECUTABLES) \
	$(RMSLIB_EXECUTABLES) \
	$(RESALIB_EXECUTABLES)

RETEK_LIBS = \
	libretek.a

LIBS = \
	$(RETEK_LIBS) \
	$(RMSLIB_LIBS) \
	$(RESALIB_LIBS)

RETEK_INCLUDES = \
	4_5_4.h \
	common.h \
	curconv.h \
	intrface.h \
	oracle.h \
	restart.h \
	retek.h \
	retek_2.h \
	rpl.h \
	saoranum.h \
	std_err.h \
	std_len.h \
	std_rest.h

INCLUDES = \
	$(RETEK_INCLUDES) \
	$(RESALIB_INCLUDES) \
	$(RMSLIB_INCLUDES)

RETEK_LN = \
	llib-lbatchretek.ln \
	llib-lretek.ln

LN = \
	$(RETEK_LN) \
	$(RMSLIB_LN) \
	$(RESALIB_LN)

#--- Put list of all targets for retek.mk here

RETEK_TARGETS = \
	$(RETEK_EXECUTABLES) \
	$(RETEK_EXECUTABLES:=.lint) \
	$(EXECUTABLES) \
	$(EXECUTABLES:=.lint) \
	$(RETEK_C_SRCS:.c=.o) \
	$(C_SRCS:.c=.o) \
	$(RETEK_PC_SRCS:.pc=.o) \
	$(RETEK_PC_SRCS:.pc=.c) \
	$(PC_SRCS:.pc=.o) \
	$(PC_SRCS:.pc=.c) \
	$(RETEK_LIBS) \
	$(LIBS) \
	$(RETEK_INCLUDES) \
	$(INCLUDES) \
	$(RETEK_LN) \
	$(LN) \
	retek-all \
	retek-libs \
	retek-includes \
	retek-clobber \
	retek-clean \
	retek-install \
	retek-lint \
	retek-depend \
	retek

