/* NEW SYSTEM HEADER: retek_2(.h) */

/* This file is included by all C code within Retek and serves to centralize
   system includes, macro defines, globals, function prototypes, and,
   especially, structs for use in restart/recovery library (retek_2.pc).

   Creation on 12-JUN-99
   Created by Jin Zhu
   Assembled from the following old Retek library headers:
      retek.h
      restart.h
      std_rest.h (no longer used)
   Removed from retek.h:
      includes of termio.h and restart.h
      mapping of library function names to 7-char names for old linkers
   Removed from restart.h:
      constant macros MAX_STATUS_LEN, MAX_SCHEMA_LEN, MAX_ERR_MSG
      macros NTS() and SSL()
      all old restart/recovery globals
      all old restart/recovery function prototypes
   Removed from std_rest.h:
      all old restart/recovery globals
   Added include of stdarg.h
   Added global gi_no_commit defined in oracle.pc
   Added function prototypes for oracle.pc
   Added constant macros for restart/recovery:
      MAX_OUT_FILE_STRING_LEN
      MAX_NUM_OF_IN_FILES
      MAX_NUM_OF_OUT_FILES
      NO_THREAD_AVAILABLE
   Added Retek file struct (rtk_file) and protoypes of its operation functions
   Added struct init_parameter and associated constant macros
   Added new restart/recovery globals:
      gi_error_flag
      gi_non_fatal_err_flag
   Added new restart/recovery function prototypes

   Modified 11/19/99
   By David Zabinski

   Removed floatingpoint.h inclusion.

   Modified 01/28/2000
   By Sebastien Chan-Tin
   Changed MAX_NUM_OF_IN_FILES and MAX_NUM_OF_OUT_FILES to
      1/2 of UNIX maximum each
*/

#ifndef RETEK_2_H
#define RETEK_2_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <memory.h>
#include <math.h>
#include <stdarg.h>
#include <std_err.h>
#include <std_len.h>
#include <limits.h>
#include <common.h>
#include <oracle.h>
#include <4_5_4.h>
#include <unistd.h>

/********************************\
|  Globals defined in oracle.pc  |
\********************************/
extern int gi_no_commit;   /* Flag for NO_COMMIT command line option */

/*********************************\
| Function defined in intrface.pc |
\*********************************/
extern int valid_all_numeric(char * str,
                             int    desired_len);

/****************************************\
|  Constant macros for restart/recovery  |
\****************************************/
#define MAX_DRIVER_NAME_LEN            25
#define MAX_OUT_FILE_STRING_LEN        255
#define OPEN_MAX_FILE                  256
#define MAX_NUM_OF_IN_FILES            (OPEN_MAX_FILE / 2)
#define MAX_NUM_OF_OUT_FILES           (OPEN_MAX_FILE / 2)

/************************\
|  Retek file structure  |
\************************/
#define NOT_PAD            1000  /* Flag not to pad thread_val */
#define PAD                1001  /* Flag to pad thread_val at the end */
#define TEMPLATE           1002  /* Flag to pad thread_val using filename template */
#define MAX_FILENAME_LEN   1024
typedef struct
{
   FILE* fp;                              /* File pointer */
   char  filename[MAX_FILENAME_LEN + 1];  /* Filename */
   int   pad_flag;   /* Flag whether to pad thread_val to filename */
} rtk_file;

/***********************************\
|  File struct operation functions  |
\***********************************/
extern int   set_filename(rtk_file* file_struct, char* file_name, int pad_flag);
extern FILE* get_FILE(rtk_file* file_struct);
extern int   rtk_print(rtk_file* file_struct, char* format, ...);
extern int   rtk_seek(rtk_file* file_struct, long offset, int whence);

/******************************************************************\
|  Structure for order and types of retek_init() parameters        |
|  Note:                                                           |
|    1. Need to be initialized at each batch program.              |
|    2. The lengths of name, type and sub_type should not exceed   |
|       the definitions here.                                      |
|    3. Type can only be: "int", "long", "string", or "rtk_file".  |
|    4. For type "int" or "long", use "" as sub_type.              |
|    5. For type "string", sub_type can only be "S" (start string) |
|       or "I" (image string).                                     |
|    6. For type "rtk_file", sub_type can only be "I" (input) or   |
|       "O" (output).                                              |
\******************************************************************/
#define NULL_PARA_NAME        51
#define NULL_PARA_TYPE        21
#define NULL_PARA_SUB_TYPE    2
typedef struct
{
   char name[NULL_PARA_NAME];
   char type[NULL_PARA_TYPE];
   char sub_type[NULL_PARA_SUB_TYPE];
} init_parameter;

/****************************\
|  Restart/recovery globals  |
\****************************/
extern int  gi_error_flag;           /* Fatal error flag     */
extern int  gi_non_fatal_err_flag;   /* Non-fatal error flag */

/****************************************************************\
|  Restart/recovery functions                                    |
|  Note:                                                         |
|    Parameters must be passed in for arguments before ellipsis. |
\****************************************************************/
extern int  retek_init(int num_args, init_parameter parmeter[], ...);
extern int  retek_commit(int num_args, ...);
extern int  commit_point_reached(int num_args, ...);
extern int  retek_force_commit(int num_args, ...);
extern int  retek_close(void);
extern int  retek_refresh_thread(void);
extern void increment_current_count(void);
extern int  parse_name_for_thread_val(char* name);
extern int  is_new_start(void);
extern int  limit_commit_max_ctr(unsigned int new_max_ctr);

/****************************************************************\
|  Security-related functions                                            |
|                                                                |
\****************************************************************/
extern int check_filename(const char* filename);

#endif   /* !defined(RETEK_2_H) */


