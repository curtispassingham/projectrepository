/* restart.h */

/* BEGIN DOC

This file includes function prototypes for restart/recovery functions.  See 
restart.pc for more documentation

END DOC */

#ifndef RESTART_H
#define RESTART_H

#include <string.h>
#include <common.h>

/* declare externally declared global variables */
extern char g_s_restart_thread_val[MAX_THREAD_LEN + 1];
extern unsigned int g_i_restart_max_counter;
extern long g_l_restart_current_count;
extern char g_s_restart_name[];
extern unsigned int g_i_retry_max_ctr;
extern unsigned int g_i_lock_wait_time; 
extern char g_p_restart_schema[];
extern unsigned char g_c_restart_process_type;

/* standard restart variables */
extern char    restart_cur_string[MAX_START_STRING_LEN + 1];
extern char    restart_new_string[MAX_START_STRING_LEN + 1];
extern char    restart_err_msg[MAX_ERR_MSG + 1];
extern char    restart_application_image[255];
extern char    restart_driver_name[MAX_DRIVER_NAME_LEN + 1 ];
extern char    restart_num_threads[MAX_THREAD_LEN + 1];
extern char   *restart_num_threads_ptr;

/****************************\
|  Restart/recovery globals  |
\****************************/
extern int  gi_error_flag;           /* Fatal error flag     */
extern int  gi_non_fatal_err_flag;   /* Non-fatal error flag */

/********************************\
|  Globals defined in oracle.pc  |
\********************************/
extern int gi_no_commit;   /* Flag for NO_COMMIT command line option */

/* declare prototypes - all exist in restart2.pc 	*/

int restart_init(char passed_array[][255], 
                 char passed_application_image[][255],
                 char * p_restart_num_threads,
                 char * p_restart_driver_name);

int restart_file_init(long * file_start_pointer,
                      char passed_application_image[][255]);

int restart_commit(char * p_restart_cur_string, 
                   char * s_restart_new_string, 
                   char * s_restart_appl_image);

int restart_file_commit(FILE * p_file_pointer, 
                        char * s_restart_appl_image);

int restart_close(char * s_restart_err_msg);

int restart_refresh_thread(void);

void get_schema(char * s_user_string); 

void parse_array_args(char *copied_from, 
		      char copied_to[][255]);

int restart_file_write(FILE ** fp, 
                       char *temp_name, 
                       char *final_name);

int restart_cat(FILE ** cat_fp, 
                char * temp_file_name, 
                char * final_file_name);

int restart_parse_name(char * name);

#endif 
