/* BEGIN DOC
**
**
**  Intrface.h
**
**  Library header file for interface programs
**
**  Library functions included in intrface.pc library file
**
**  S. Standiford
**  30-AUG-96
**
**  Mods: 1. Add retek_get_record(), retek_write_rej(),
**           retek_write_to_rej_file(), retek_check_file_err(),
**           and retek_process_dtl_ftail();
**        2. Comment out internal use functions val_XXXXX()
**           and file_structure().
**  J. Zhu
**  22-JUN-99
*/

/**********************************/
/* #define constant field lengths */
/**********************************/

/** field lengths generic to all interface files */

/* all records */
#define LEN_REC_TYPE       5
#define NULL_REC_TYPE      6
#define LEN_REC_NO        10
#define NULL_REC_NO       11

/* file header records */
#define LEN_FILE_DATE     14  /* format YYYYMMDDHHMMSS */
#define NULL_FILE_DATE    15  /* format YYYYMMDDHHMMSS */
#define LEN_FILE_TYPE      4
#define NULL_FILE_TYPE     5

/* transaction trailer records */
#define LEN_DTL_LINE_CNT   6
#define NULL_DTL_LINE_CNT  7

/* file trailer records */
#define LEN_TOT_LINE_CNT  10
#define NULL_TOT_LINE_CNT 11



/* declare error codes */

#define REQ_FIELD_IS_EMPTY   1412
#define NOT_ALL_NUMERIC      1722
#define NOT_IN_RANGE         0000
#define STRING_TOO_LONG      1438

/***************************************/
/* declare externally declared globals */
/***************************************/

extern long g_l_rec_cnt;      /* count of recs between file header & trailer */
extern int  g_i_dtl_cnt;      /* count of details between trans head & tail */
extern int  g_i_beg_tran;        /* byte location of beginning of current trans */
extern int  g_i_beg_tran_hldr;   /* holds beg.trans location until thead is read */
extern long g_l_rej_cnt;      /* count of number of records rejected */
extern long g_l_cur_tran;        /* current transaction number */
extern char g_s_prev_type[LEN_REC_TYPE + 1];  /* previous record type */
extern char g_s_cur_rec[LEN_REC_NO + 1];           /* current record number */




/*****************************************/
/* functional prototypes from intrfac.pc */
/*****************************************/

int get_record(void *struct_ptr, 
               int   struct_size,
               FILE *in_file, 
               FILE *rej_file,
               char *rec_type); 

int write_rej(FILE *in_file,
              FILE *rej_file);

int write_to_rej_file(void * err_buff,
                      int    err_buff_size,
                      FILE * rej_file);

int process_dtl_ftail(FILE * in_file,
                      FILE * rej_file);

int check_file_err(FILE * check_file,
                   char * file_type);

int valid_all_numeric(char * str,
                      int    desired_len);

int valid_all_numeric_signed(char * str,
                             int    desired_len);

int left_shift(char * str);

void nullpad(char * str, 
             int    str_len);

int valid_date(char * str);

int field_has_value(char * str);

int all_blank(char * str);

int get_schema_name(char * userid,
                    char * schema);

void zero_pad(int  i_exp_len, 
                char * c_str);

#ifdef RETEK_2_H

/******************************************************************/
/* Added function prototypes for passing pointers to new rtk_file */
/* sturct. Each function with retek_ prefix here has the same     */
/* functionality with the original function.                      */
/******************************************************************/
int retek_get_record(void *     struct_ptr, 
                     int        struct_size, 
                     rtk_file * in_file,
                     rtk_file * rej_file,
                     char *     rec_type);

int retek_write_rej(rtk_file * rej_file,
                    rtk_file * in_file);

int retek_write_to_rej_file(void *     err_buff,
                            int        err_buf_size,
                            rtk_file * rej_file);

int retek_process_dtl_ftail(rtk_file * in_file,
                            rtk_file * rej_file);

int retek_check_file_err(rtk_file * check_file,
                         char *     file_type);
#endif   /* defined(RETEK_2_H) */



