/* SYSTEM HEADER: RETEK(.h) */

/* This file is included by all C code within RETEK and serves to centralize
   system includes and to map meaningful library function names to 7 character
   names for portability to systems with a more restrictive linker. 

   Modification on 1-AUG-95
   Modified by Fred Potthoff
   Added macros MATCH(), NTS(), and SSL()

   Modifications on 25-JUN-96
   Modified by Susan Standiford
   Added include of restart2.h 
      - restart constant definitions and prototypes

   Modifications on 28-JUN-96
   Modified by Susan Standiford
   Added include of std_err.h 
      - standard error definitions used for error handling
      - function variable will be declared locally
	
	Modifications on 25-OCT-97
	Modified by Susan Standiford
	Renamed file to retek.h from merman.h

   Modifications on 16-JUN-99
   Modified by Jin Zhu
   Removed - includes of termio.h and restart.h
           - mapping of library function names to 7-char names for old linkers
   Added function prototypes for oracle.pc after changes to LOGON & WRITE_ERROR

   Modifications on 17-NOV-99
   Modified by David Zabinski
   Removed include of floatingpoint.h 
*/

#ifndef RETEK_H
#define RETEK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <memory.h>
#include <termio.h>
#include <math.h>
#include <restart.h>
#include <std_err.h>
#include <std_len.h>
#include <oracle.h>
#include <4_5_4.h>

#endif   /* !defined(RETEK_H) */
