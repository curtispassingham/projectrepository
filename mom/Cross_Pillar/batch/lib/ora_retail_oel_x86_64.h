/*=========================================================================

   ORA_RETAIL_OEL_X86_64.H: header file for Oracle Enterprise Linux 64-bit

   
   This is a special header file used for fixing compilation errors when
   building the Oracle Retail batch programs on Oracle Enterprise x86-64
   Linux. This file is included by make file platform_linux_oel_x86_64.mk.
   It is included first, before any system or Oracle Retail header files,
   to override definitions that are causing compilation errors and
   to include any header files that are not being properly included for
   some reason.
   
   NOTE: Any #define macros should precede any #include statements in
         this file.
   
   Problems fixed by this file:
   1) Header file linux/limits.h is included due to a missing OPEN_MAX
      definition.
   2) File values.h is included due to a missing DBL_MAX definition.

=========================================================================*/

#ifndef ORA_RETAIL_OEL_X86_64_H
#define ORA_RETAIL_OEL_X86_64_H

#include <linux/limits.h>
#include <values.h>

#endif   /* !defined(ORA_RETAIL_OEL_X86_64_H) */
