#ifndef LIBL10N_H
   #define LIBL10N_H

   #include <l10nlibobj.h>
   #include <l10nfinlib.h>

   /* These functions will be used to retrieve the localization and */
   /* localized fucntion that will be used.                         */
   int wrapper_function();
   int set_func_pointer();
   int load_function_array(batch_array *ia_batch_array);

   /* Each localized library should have a version of this function   */
   /* to populate their respective arrays. The non localized version  */
   /* of this function will only have null values for the batch array.*/
   /* This function should be called in the init() part.              */
   int load_batch_arrays();
#endif
