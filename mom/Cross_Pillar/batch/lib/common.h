
#ifndef COMMON_H
#define COMMON_H

/**********\
|  Macros  |
\**********/
#define MATCH(a,b)    !strcmp((char *) a,b)
#define NTS(a)        a.arr[a.len] = '\000'
#define SSL(a)        a.len = (unsigned short) strlen((char *) a.arr)

#define FATAL     -1
#define NON_FATAL  1
#define OK         0

/****************************************************\
|              Memory allocation macros              |
|  The variable 'function' is expected to be defined |
|  in the calling procedure.                         |
\****************************************************/
#define FREE(p) \
   do { \
      free(p); \
      p = NULL; \
   } while (0)

#define CALLOC(ptr, cast, ele, sz) \
   do { \
      if ((ptr = cast calloc((ele), (sz))) == NULL) \
      { \
         sprintf(err_data, "CALLOC: Unable to allocate memory for %s", #ptr); \
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data); \
         return(FATAL); \
      } \
   /*CONSTCOND*/ \
   } while (0)

#define REALLOC(ptr, cast, ele, sz) \
   do { \
      if ((ptr = cast realloc(ptr, ((ele) * (sz)))) == NULL) \
      { \
         sprintf(err_data, "REALLOC: Unable to allocate memory for %s", #ptr); \
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data); \
         return(FATAL); \
      } \
   /*CONSTCOND*/ \
   } while (0)

#define MALLOC(ptr, cast, ele, sz) \
   do { \
      if ((ptr = cast malloc(ptr, ((ele) * (sz)))) == NULL) \
      { \
         sprintf(err_data, "MALLOC: Unable to allocate memory for %s", #ptr); \
         WRITE_ERROR(RET_FUNCTION_ERR, function, "", err_data); \
         return(FATAL); \
      } \
   /*CONSTCOND*/ \
   } while (0)

#define REALLOC_STRING(ptr, ele, sz) REALLOC(ptr, (char (*)[sz]), ele, sz)
#define REALLOC_DOUBLE(ptr, ele) REALLOC(ptr, (double *), ele, sizeof(double))
#define REALLOC_SHORT(ptr, ele) REALLOC(ptr, (short *), ele, sizeof(short))
#define REALLOC_INT(ptr, ele) REALLOC(ptr, (int *), ele, sizeof(int))
#define REALLOC_LONG(ptr, ele) REALLOC(ptr, (long *), ele, sizeof(long))
#define REALLOC_FLOAT(ptr, ele) REALLOC(ptr, (float *), ele, sizeof(float))

#define CALLOC_STRING(ptr, ele, sz) CALLOC(ptr, (char (*)[sz]), ele, sz)
#define CALLOC_DOUBLE(ptr, ele) CALLOC(ptr, (double *), ele, sizeof(double))
#define CALLOC_SHORT(ptr, ele) CALLOC(ptr, (short *), ele, sizeof(short))
#define CALLOC_INT(ptr, ele) CALLOC(ptr, (int *), ele, sizeof(int))
#define CALLOC_LONG(ptr, ele) CALLOC(ptr, (long *), ele, sizeof(long))
#define CALLOC_FLOAT(ptr, ele) CALLOC(ptr, (int *), ele, sizeof(float))

#define MALLOC_STRING(ptr, ele, sz) MALLOC(ptr, (char (*)[sz]), ele, sz)
#define MALLOC_DOUBLE(ptr, ele) MALLOC(ptr, (double *), ele, sizeof(double))
#define MALLOC_SHORT(ptr, ele) MALLOC(ptr, (short *), ele, sizeof(short))
#define MALLOC_INT(ptr, ele) MALLOC(ptr, (int *), ele, sizeof(int))
#define MALLOC_LONG(ptr, ele) MALLOC(ptr, (long *), ele, sizeof(long))
#define MALLOC_FLOAT(ptr, ele) MALLOC(ptr, (int *), ele, sizeof(float))


#define ADD_VAT(id_retail, id_vat_rate)    ( (double)id_retail * (1+((double)id_vat_rate/100)))
#define REMOVE_VAT(id_retail, id_vat_rate) ( (double)id_retail / (1+((double)id_vat_rate/100)))

/*******************************************\
|  Macros for maximum oracle array sizes.   |
|  These are hard oracle limits.            |
\*******************************************/
#define MAX_ORACLE_ARRAY_SIZE 32000
#define MAX_FETCH_ARRAY_SIZE  MAX_ORACLE_ARRAY_SIZE
#define MAX_INSERT_ARRAY_SIZE MAX_ORACLE_ARRAY_SIZE
#define MAX_UPDATE_ARRAY_SIZE MAX_ORACLE_ARRAY_SIZE
#define MAX_DELETE_ARRAY_SIZE MAX_ORACLE_ARRAY_SIZE

/* define constants for restart functions */
#define MAX_THREAD_LEN			10
#define MAX_STATUS_LEN			20
#define MAX_NAME_LEN			50
#define MAX_SCHEMA_LEN			15
#define MAX_FLAG_LEN			1
#define MAX_COUNTER_LEN			6
#define MAX_START_STRING_LEN		255
#define MAX_APPLICATION_IMAGE_LEN	1000
#define MAX_OPERATOR_ID			20
#define MAX_ERR_MSG			255
#define MAX_DRIVER_NAME_LEN		25
#define MAX_RETRY_COUNT                 10
#define NO_THREAD_AVAILABLE             1403

/***************************************\
|  Macros for termination return codes  |
\***************************************/
#define SUCCEEDED        EXIT_SUCCESS
#define FAILED           255
#define WARNING          EXIT_SUCCESS
#define NO_THREADS       1
#define REJECTED_RECORDS 254
#define LOCKED_RECORDS   253

#endif   /* !defined(COMMON_H) */
