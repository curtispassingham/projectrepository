include ${MMHOME}/oracle/lib/src/oracle.mk
PRODUCT_PROCFLAGS = 
PRODUCT_CFLAGS =
PRODUCT_LDFLAGS =
PRODUCT_LINT_FLAGS = 
include ${MMHOME}/oracle/lib/src/platform.mk
include ${MMHOME}/oracle/lib/src/rules.mk
include ${MMHOME}/oracle/lib/src/retek_include.mk
include ${MMHOME}/oracle/lib/src/rmslib_include.mk
include ${MMHOME}/oracle/lib/src/resalib_include.mk

#--- Standard targets.

all: retek-all FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

libs: retek-libs FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

includes: retek-includes FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

clean: retek-clean FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

clobber: retek-clobber FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

install: retek-install FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

lint: retek-lint FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

depend: retek-depend FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

$(RMSLIB_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/rmslib.mk $@

$(RESALIB_TARGETS): FORCE
	@$(MAKE) -f ${MMHOME}/oracle/lib/src/resalib.mk $@

retek-all: retek-libs $(RETEK_EXECUTABLES)

retek-libs: retek-includes $(RETEK_LIBS)

retek-includes: $(RETEK_INCLUDES)

retek-clean: FORCE
	rm -f $(C_SRCS:.c=.o)
	rm -f $(C_SRCS:.c=.ln)
	rm -f $(PC_SRCS:.pc=.o)
	rm -f $(PC_SRCS:.pc=.c)
	rm -f $(PC_SRCS:.pc=.lis)
	rm -f $(PC_SRCS:.pc=.ln)
	for f in $(RETEK_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.lint; \
		rm -f $$n; \
	done

retek-clobber: retek-clean FORCE
	rm -f $(RETEK_EXECUTABLES)
	rm -f $(RETEK_LIBS)
	for f in $(RETEK_LIBS); \
	do \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		rm -f $$n; \
	done

retek-install: FORCE
	-@RETEK_INCLUDES="$(RETEK_INCLUDES)"; \
	for f in $$RETEK_INCLUDES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_SRC)/$$f ] || [ $$f -nt $(RETEK_LIB_SRC)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_SRC)/$$f"; \
				cp $$f $(RETEK_LIB_SRC)/$$f; \
				chmod $(MASK) $(RETEK_LIB_SRC)/$$f; \
			fi; \
		fi; \
	done; \
	RETEK_EXECUTABLES="$(RETEK_EXECUTABLES)"; \
	for f in $$RETEK_EXECUTABLES; \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				cp $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
	done; \
	for f in $(RETEK_LIBS); \
	do \
		if [ -x $$f ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$f ] || [ $$f -nt $(RETEK_LIB_BIN)/$$f ]; \
			then \
				echo "Copying $$f to $(RETEK_LIB_BIN)/$$f"; \
				mv $$f $(RETEK_LIB_BIN)/$$f; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$f; \
			fi; \
		fi; \
		n=llib-l`expr $$f : 'lib\(.*\)\.a' \| $$f : 'lib\(.*\)\.$(SHARED_SUFFIX)'`.ln; \
		if [ -x $$n ]; \
		then \
			if [ ! -f $(RETEK_LIB_BIN)/$$n ] || [ $$f -nt $(RETEK_LIB_BIN)/$$n ]; \
			then \
				echo "Copying $$n to $(RETEK_LIB_BIN)/$$n"; \
				cp $$n $(RETEK_LIB_BIN)/$$n; \
				chmod $(MASK) $(RETEK_LIB_BIN)/$$n; \
			fi; \
		fi; \
	done;

retek-lint: FORCE
	-@for l in $(RETEK_LIBS); \
	do \
		n=`expr $$l : 'lib\(.*\)\.a' \| $$l : 'lib\(.*\)\.$(SHARED_SUFFIX)'`; \
		$(MAKE) -f ${MMHOME}/oracle/lib/src/retek.mk llib-l$$n.ln; \
	done

retek-depend: retek.d

retek.d: $(RETEK_C_SRCS) $(RETEK_PC_SRCS) ${MMHOME}/oracle/lib/src/oracle.mk
	@echo "Making dependencies retek.d..."
	@$(MAKEDEPEND) $(RETEK_C_SRCS) $(RETEK_PC_SRCS) | $(MAKEDEPEND_NOT_SYSTEM) >`basename $@`
	@chmod $(MASK) `basename $@`

${MMHOME}/oracle/lib/src/oracle.mk: ${MMHOME}/oracle/lib/src/oramake FORCE
	@${MMHOME}/oracle/lib/src/oramake oracle.$$$$; \
	if cmp -s oracle.$$$$ $@; then \
		rm -f oracle.$$$$; \
		true; \
	else \
		if mv oracle.$$$$ $@; then \
			echo "$@ was out of date."; \
			echo "This has been fixed. But you must re-execute your $(MAKE) command."; \
		else \
			rm -f oracle.$$$$; \
			echo "$@ is out of date."; \
			echo "It must be regenerated with ${MMHOME}/oracle/lib/src/oramake."; \
		fi; \
		false; \
	fi;

FORCE:

#--- Executable targets.

#--- Executable lint targets.

#--- Library targets by product.

retek: libretek.a

#--- Library targets.

libretek.a: $(LIBRETEK_C_OBJS) $(LIBRETEK_PC_OBJS)
	$(AR) $(ARFLAGS) `basename $@` `echo $(LIBRETEK_C_OBJS) $(LIBRETEK_PC_OBJS) | xargs -n1 basename`
	chmod $(MASK) `basename $@`

#--- Library lint targets.

llib-lretek.ln: $(LIBRETEK_C_OBJS:.o=.c) $(LIBRETEK_PC_OBJS:.o=.c)
	$(LINT) $(LINT_FLAGS) -o retek `echo $(LIBRETEK_C_OBJS:.o=.c) $(LIBRETEK_PC_OBJS:.o=.c) | xargs -n1 basename` > `basename $*`.lint 2>&1
	chmod $(MASK) `basename $@` `basename $*`.lint

#--- Object dependencies.

include retek.d
