#--- Object file groupings.

LIBBATCHSUPCSTRR_PC_OBJS = \
	supcstrr.o

LIBSUPCSTRR_PC_OBJS = \
	plsqlscl.o \
	$(LIBBATCHSUPCSTRR_PC_OBJS)

LIBSUPCSTRR_TMP_ARCH = \
	libsupcstrr.tmp.a

LIBBATCHCREATEORD_PC_OBJS = \
	$(LIBDEALORD_PC_OBJS) \
	buyerbld.o \
	ordsplit.o \
	add_line_item.o \
	split_po.o \
	create_po.o

LIBCREATEORD_PC_OBJS = \
	plsqlordcreate.o \
	$(LIBBATCHCREATEORD_PC_OBJS)

LIBCREATEORD_TMP_ARCH = \
	libcreateord.tmp.a

LIBBATCHDEALORD_PC_OBJS = \
	dealordlib.o

LIBDEALORD_PC_OBJS = \
	plsqldealordlib.o \
	$(LIBBATCHDEALORD_PC_OBJS) 

LIBDEALORD_TMP_ARCH = \
	libdealord.tmp.a

LIBBATCHDEALINC_PC_OBJS = \
	dealinclib.o

LIBDEALINC_PC_OBJS = \
	$(LIBBATCHDEALINC_PC_OBJS)

LIBDEALINC_TMP_ARCH = \
	libdealinc.tmp.a

LIBPARTITION_PC_OBJS = \
	partadd.o \
	partcheck.o \
	partrbldu.o

LIBREPLOCI_C_OBJS = \
	ocilib.o \
	ocithread.o

LIBL10N_PC_OBJS = \
	l10nlib.o \
	l10nbaselib.o \

LIBBATCHL10N_PC_OBJS = \
	$(LIBL10N_PC_OBJS)

RMSLIB_C_OBJS = \
	$(LIBREPLOCI_C_OBJS)

RMSLIB_PC_OBJS = \
	$(LIBDEALINC_PC_OBJS) \
	$(LIBSUPCSTRR_PC_OBJS) \
	$(LIBCREATEORD_PC_OBJS) \
	$(LIBDEALORD_PC_OBJS) \
	$(LIBPARTITION_PC_OBJS) \

RMSLIB_C_SRCS =

RMSLIB_PC_SRCS = \
	$(RMSLIB_PC_OBJS:.o=.pc)

#--- Target groupings.

RMSLIB_EXECUTABLES =

RMSLIB_LIBS = \
	libbatchsupcstrr.a \
	libsupcstrr.$(SHARED_SUFFIX) \
	libbatchcreateord.a \
	libcreateord.$(SHARED_SUFFIX) \
	libbatchdealord.a \
	libdealord.$(SHARED_SUFFIX) \
	libbatchdealinc.a \
	libdealinc.$(SHARED_SUFFIX) \
	libreploci.a \
	libpartition.a \
	libreploci.a \
	libbatchl10n.a \
	libl10n.$(SHARED_SUFFIX)

RMSLIB_INCLUDES = \
	supcstrr.h \
	createordlib.h \
	dealordlib.h \
	dealinclib.h \
	partition.h \
	l10nlibobj.h \
	l10nlib.h \
	l10nfinlib.h

RMSLIB_LN = \
	llib-lbatchsupcstrr.ln \
	llib-lsupcstrr.ln \
	llib-lbatchcreateord.ln \
	llib-lcreateord.ln \
	llib-lbatchdealord.ln \
	llib-ldealord.ln \
	llib-lbatchdealinc.ln \
	llib-ldealinc.ln \
	llib-lreploci.ln \
	llib-lpartition.ln \
	llib-lbatchl10n.ln \
	llib-ll10n.ln

#--- Put list of all targets for rmslib.mk here

RMSLIB_TARGETS = \
	$(RMSLIB_EXECUTABLES) \
	$(RMSLIB_EXECUTABLES:=.lint) \
	$(RMSLIB_C_SRCS:.c=.o) \
	$(RMSLIB_PC_SRCS:.pc=.o) \
	$(RMSLIB_PC_SRCS:.pc=.c) \
	$(RMSLIB_LIBS) \
	$(RMSLIB_INCLUDES) \
	$(RMSLIB_LN) \
	rmslib-all \
	rmslib-libs \
	rmslib-includes \
	rmslib-clobber \
	rmslib-clean \
	rmslib-install \
	rmslib-lint \
	rmslib-depend \
	rms \
	orderscale \
	createord \
	dealord \
	dealinc \
	reploci \
	partition \
	l10n
