
/* LIBRARY ROUTINES: ORACLE */

/* BEGIN DOC

Library routine: ORACLE

Function: Performs various standard ORACLE functions.

          Synopsis:
          LOGON(arg count, arg list)
          COMMIT()    * SQL COMMIT WORK
          CHECK_STATUS("Operation")
          LOG_MESSAGE("Message text")
          WRITE_ERROR(Err Code,"Err Type","Program","Function",
		      "Err_data","Table Name","Record","Reject File")
          CHECK_STATUS_NO_LOG(Err Code)
          LOG_ERROR(Err Code, "Program")
          All routines return zero if successful, else -1.

          LOGON parses userid, password, connect string and command line options,
          then connect to database and displays a startup message in the current
          system log (via 'LOG_MESSAGE') if successful.

          CHECK_STATUS checks sqlca.sqlcode and if < 0 displays the following
          message on standard error and in the current system log (via
          'LOG_MESSAGE') and returns -1:
              ("Operation") SQL ERROR CODE -9999

          LOG_MESSAGE writes the date and time followed by the program name and
          message text in the current system log.
          The system log is $MMHOME/log/Mmm_nn.log
              eg. /usr/retek/log/Oct_23.log

          WRITE_ERROR inserts error-related information a flat file and this
          file will be sql loaded later.  If 9999 is passed as an error code,
          this prevents a record from being written to the error file.

          CHECK_STATUS_NO_LOG checks err_code.  If err_code is less
          than 0, and the value of b_err_xref.fatal_err_ind = 'Y', then
          return(-1); else if the sqlca.sqlcode is less than 0 and the value of
          b_err_xref.fatal_err_ind = 'N', return(1); else return(0).  This
          function differs from CHECK_STATUS in that all logic pertaining to
          LOG_MESSAGE has been removed.

  	  LOG_ERROR is like LOG_MESSAGE except an error code and program name
          are passed in and the b_err_xref table is queried to find the
	  corresponding error message and then this information will be
	  printed to the log file.

END DOC */

#ifndef ORACLE_H
#define ORACLE_H

#ifdef ORACLE_MODULE
  #define ORACLE_GLOBAL
#else
  #define ORACLE_GLOBAL extern
#endif

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <std_err.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <std_len.h>
#include <sqlcpr.h>

ORACLE_GLOBAL char PROGRAM[30];
ORACLE_GLOBAL char err_data[1379]; /*MAX_FILENAME_LEN + 355*/
ORACLE_GLOBAL char table[255];

/* Global flag for NO_COMMIT option */
ORACLE_GLOBAL int gi_no_commit;

/* Global flag for RTK_TEST option */
ORACLE_GLOBAL int gi_rtk_test;

/* Global flag for TEST_CASE option */
ORACLE_GLOBAL int gi_test_case;



int LOGON(int argc, char *argv[]);

int COMMIT(void);

int CHECK_STATUS(char * ecode);

int LOG_MESSAGE(char * message_text);

int WRITE_ERROR(long err_code,
                char *function,
                char *table_name,
                char *err_data);

int CHECK_STATUS_NO_LOG(long err_code);

int LOG_ERROR(long error_code,
              char * program,
              char * err_data);

int synonym_trace(char is_synonym_name[NULL_SYNONYM_NAME],
                  char os_table_owner[NULL_TABLE_OWNER],
                  char os_table_name[NULL_TABLE_NAME]);

void getDateTime( char *s );

#endif   /* !defined(ORACLE_H) */

