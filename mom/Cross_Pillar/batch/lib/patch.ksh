#!/usr/bin/ksh
#####################################################################################
#  Because of an Oracle bug, the xalias_level flag must be removed from
#  the $ORACLE_HOME/precomp/lib/env_precomp.mk file on Sun 64-bit systems
#  that use the Forte C Compiler.  
#####################################################################################
cp $ORACLE_HOME/precomp/lib/env_precomp.mk $ORACLE_HOME/precomp/lib/env_precomp.mk.orig
sed -e s/"-xalias_level=weak "// $ORACLE_HOME/precomp/lib/env_precomp.mk.orig > $ORACLE_HOME/precomp/lib/env_precomp.mk
