
# VPATH needs to be in the same order as the include= references in PROCFLAGS
# and -I references in RETEK_CFLAGS.
VPATH = $(PRECOMPADMIN):${MMHOME}/oracle/lib/src:$(PRECOMPHOME)public:${MMHOME}/oracle/proc/src

SHELL = /bin/ksh

STD_TARGETS = all libs includes clobber clean install lint depend

RETEK_LIB_BIN = ${MMHOME}/oracle/lib/bin
RETEK_LIB_SRC = ${MMHOME}/oracle/lib/src

RETEK_PROC_BIN = ${MMHOME}/oracle/proc/bin
RETEK_PROC_SRC = ${MMHOME}/oracle/proc/src

# PLATFORM_PROCFLAGS is used to add options to the Oracle Pro*C Compiler for a
# particular hardware/OS platform.
PLATFORM_PROCFLAGS = define=_PA_RISC2_0

# Some debuggers do not like this, but it is nice for tracking down syntax
# errors. This can be turned off by either commenting these lines out or by
# overriding the value on the make command line:
#	  make -f product.mk PROCDEBUG=
PROCDEBUG = \
	lines=yes

PROCFLAGS = \
	include=$(PRECOMPADMIN) \
	include=$(RETEK_LIB_SRC) \
	include=$(PRECOMPHOME)public \
	include=${ORACLE_HOME}/rdbms/public \
	include=${ORACLE_HOME}/plsql/public \
	include=${ORACLE_HOME}/sqllib/public \
	include=${ORACLE_HOME}/network/public \
	include=${ORACLE_HOME}/rdbms/demo \
	$(PROCDEBUG) \
	select_error=yes \
	release_cursor=no \
	dbms=native \
	sqlcheck=full \
	hold_cursor=yes \
	char_map=string \
	code=ansi_c \
	common_parser=yes \
	$(PRODUCT_PROCFLAGS) \
	$(PLATFORM_PROCFLAGS) \
	USERID=${UP} \
	$(MYPROCFLAGS)

# Uncomment these lines for testing and debugging.
# Comment them for production.
GFLAG = -g
OPTIMIZE = 

# PLATFORM_CFLAGS is used to add options to the C Compiler for a particular
# hardware/OS platform.
PLATFORM_CFLAGS = -D_HPUX_SOURCE

# PRODUCT_CFLAGS is a hook for the various Retek product makefiles.  It is
# how they should add arguements/options to the C compile line.  This should
# only include -I, -D and -U options that are completely local to the product.
# Options that are hardware/OS dependent (like allowing POSIX features) should
# be added to PLATFORM_CFLAGS above.
RETEK_CFLAGS = \
	-DNDEBUG \
	$(I_SYM). \
	$(I_SYM)$(RETEK_LIB_SRC) \
	$(I_SYM)$(PRECOMPHOME)public \
	$(I_SYM)${ORACLE_HOME}/rdbms/public \
	$(I_SYM)${ORACLE_HOME}/plsql/public \
	$(I_SYM)${ORACLE_HOME}/sqllib/public \
	$(I_SYM)${ORACLE_HOME}/network/public \
	$(I_SYM)${ORACLE_HOME}/rdbms/demo \
	$(PRODUCT_CFLAGS) $(PLATFORM_CFLAGS) $(MYCFLAGS)

CC = cc +DD64
LD = cc +DD64

LINT = lint
LINT_FLAGS = \
	-b -x -s -Ae \
	`echo $(RETEK_CFLAGS) $(CFLAGS) | sed 's/-[^IDU][ 	]*[^-]*/ /g'` \
	$(MYLINTFLAGS)
LINT_LDFLAGS = $(PRODUCT_LINT_FLAGS) $(RETEK_LIB_BIN)/llib-lretek.ln -lm

ORACLE_LDFLAGS = -L$(LIBHOME) $(PROLDLIBS) $(CFLAGS)

#RETEK_LDFLAGS = $(RETEK_LIB_BIN)/retek.a
# This assumes that retek.a has been renamed libretek.a.
RETEK_LDFLAGS = -L$(RETEK_LIB_BIN) -lretek

# PRODUCT_LDFLAGS is a hook for the various Retek product makefiles.  It is
# how they should add things to the link line (ex: product specific libraries).
LDFLAGS = $(PRODUCT_LDFLAGS) $(RETEK_LDFLAGS) $(ORACLE_LDFLAGS) $(MYLDFLAGS)

# The SHARED_* variables say how to create and use shared libraries.
SHARED_AR = ld
SHARED_ARFLAGS = -b -o
SHARED_SUFFIX = sl
SHARED_LIB_RUN = -Wl,+b

# Permissions to give to all things that are built (targets).
MASK = 755

# Commands to generate makefile dependencies.
MAKEDEPEND = makedepend -f- -- $(RETEK_CFLAGS) -I${MMHOME}/oracle/proc/src --
# We generally do not care about system and Oracle include files, so let's ignore them.
# Also, let's strip the pathname from files in the Retek include directory.
# Finally, since we have removed some of the dependencies, let's get rid of blank dependency lines.
MAKEDEPEND_NOT_SYSTEM = sed \
   -e 's! /usr/include/[^ ]*!!g' \
   -e 's! ${ORACLE_HOME}/[^ ]*!!g' \
   -e 's!${MMHOME}/oracle/lib/src/!!' \
   -e '/^[^:]*: *$$/d'

AR = ar
ARFLAGS = -r
