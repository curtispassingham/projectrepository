#!/bin/ksh

#
# The oramake shell script creates the oracle.mk file by extracting the make
# variables that Retek uses from Oracle's makefiles.
#

if [ -z "$ORACLE_HOME" ]
then
   print -u2 "ORACLE_HOME is not defined."
   exit 1
fi

ENV_PRECOMP=$ORACLE_HOME/precomp/lib/env_precomp.mk

if [ ! -r $ENV_PRECOMP ]
then
   print -u2 "$ENV_PRECOMP: file not found or is not readable"
   exit 1
fi

test -x /usr/bin/nawk && AWK=nawk || AWK=awk

ORACLEMK=${1:-oracle.mk}

rm -f $ORACLEMK
rm -f demo_rdbms.mk

# Remove comments.
# Join continued lines.
# Make sure that there is a space on either side of the first = or :,
# remove comments introduced while joining the lines,
# remove blank lines.
sed -e 's/#.*//' $ENV_PRECOMP |
	sed -e ':top' -e '/\\$/{' -e N -e 's/\\\n/ /' -e btop -e '}' |
	sed \
		-e 's/^\([$A-Za-z][A-Za-z0-9_()]*\)[ 	]*\([=:]\)[ 	]*/\1 \2 /' \
		-e 's/#.*//' \
		-e '/^[ 	]*$/d' |
	grep -v 'JDBC_ALL_LRGS' |
	$AWK '
		function traverse( v,
		                   vs, str, n, i, s, e, env) # local variables
		{
			if (envs[v] != "X")
			{
				if (vars[v] == "")
					printf( "#%s = \n", v)
				else
					printf( "%s =%s\n", v, vars[v])
				str = vars[v]
				gsub( "\\$", " $", str)
				n = split( str, vs)
				for (i = 1; i <= n; i++)
				{
					s = match( vs[i], /\$\([A-Za-z0-9_]+\)/)
					if (s != 0)
						e = index( vs[i], ")")
					else
					{
						s = match( vs[i], /\$\{[A-Za-z0-9_]+\}/)
						if (s != 0)
							e = index( vs[i], "}")
					}
					if (s != 0)
					{
						env = substr( vs[i], s + 2, e - s - 2)
						traverse( env)
						envs[env] = "X"
					}
				}
			}
		}
		$0 ~ /^	/ { # part of a target script
			next
		}
		$2 == ":" { # build target
			next
		}
		$2 == "=" { # variable
			if (vars[$1] != "") # Want the last definition
				vars[$1] = ""
			for (i = 3; i <= NF; i++)
				vars[$1] = vars[$1] " " $i
		}
		END {
			# These are needed to be compatible with Oracle 8.0.5.
			if (vars["I_SYM"] == "")
				vars["I_SYM"] = " -I"
			if (vars["INCLUDE"] == "")
				vars["INCLUDE"] = " $(PRECOMPSH) $(PLSQLPUBH) $(RDBMSPUBH) $(ORACOREPUBH) $(COREINCLUDE) $(NLSRTLPUBH) $(OTRACEPUBH) $(NETWORKPUBH) $(SLAXPUBH) $(TK2PUBH)"

			traverse( "CFLAGS")
			traverse( "LIBHOME")
			traverse( "PROLDLIBS")
			traverse( "PRECOMPADMIN")
			traverse( "PRECOMPHOME")
			traverse( "PROC")
			traverse( "LIBCLNTSH")
			traverse( "RDBMSHOME")
			traverse( "GENCLNTSH")
			traverse( "GENCLNTSH64")
		}
	' > $ORACLEMK

# If 64-bit linux, do some additional editing of $ORACLEMK
grep -q 'x86_64' $ENV_PRECOMP
if [ $? -eq 0 ]
then
   sed -e 's/AMD32FLAGS *= *-m32/AMD32FLAGS =/g' $ORACLEMK > /tmp/$ORACLEMK
   mv /tmp/$ORACLEMK $ORACLEMK
fi

cmp -s $ORACLE_HOME/rdbms/demo/demo_rdbms.mk demo_rdbms.mk || cp $ORACLE_HOME/rdbms/demo/demo_rdbms.mk .

exit 0
