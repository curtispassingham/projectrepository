.SUFFIXES:
.SUFFIXES: .ln .a .o .pc .c .y .l .sh .ksh .csh .h

# Some VPATH impelementations require white space on either side of the
# builtin internal macros. This conflicts with Oracle not wanting any
# white space around the ='s on the Pro*C command line. We get around this
# by putting white space in the makefile and removing the white space with
# the build shell.

# Since not all make implementations understand the F macro modifier
# (ex: ${*F}, ${@F}, etc), we fall back on using basename to achieve the
# same affect in the build shell.

.pc.o:
	@echo "precompiling $<"
	@$(PROC) $(PROCFLAGS) iname=`echo $< ` oname=`basename $*`.c
	$(CC) $(RETEK_CFLAGS) $(CFLAGS) -c `basename $*.c`

.pc.ln:
	@echo "precompiling $<"
	@$(PROC) $(PROCFLAGS) iname=`echo $< ` oname=`basename $*`.c
	$(LINT) $(LINT_FLAGS) -c `basename $*.c`

.pc.c:
	@echo "precompiling $<"
	@$(PROC) $(PROCFLAGS) iname=`echo $< ` oname=`basename $*`.c

.c.o:
	$(CC) $(RETEK_CFLAGS) $(CFLAGS) -c $< -o `basename $@`

.c.ln:
	$(LINT) $(LINT_FLAGS) -c $*< `basename $@`

.y.o:
	$(YACC) $(YFLAGS) $<
	mv y.tab.c `basename $*.c`
	$(CC) $(RETEK_CFLAGS) $(CFLAGS) -c `basename $*.c`

.y.ln:
	$(YACC) $(YFLAGS) $<
	mv y.tab.c `basename $*.c`
	$(LINT) $(LINT_FLAGS) -c `basename $*.c`

.l.o:
	$(LEX) $(LFLAGS) $<
	mv lex.yy.c `basename $*.c`
	$(CC) $(RETEK_CFLAGS) $(CFLAGS) -c `basename $*.c`

.l.ln:
	$(LEX) $(LFLAGS) $<
	mv lex.yy.c `basename $*.c`
	$(LINT) $(LINT_FLAGS) -c `basename $*.c`

.sh:
	cp $< `basename $@`

.ksh:
	cp $< `basename $@`

.csh:
	cp $< `basename $@`



