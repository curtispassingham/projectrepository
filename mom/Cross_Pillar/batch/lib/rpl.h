/*----------------------------------------------------------------------------*\
 * RPL.H
 * Utility functions to be used by replenishment batch programs:
 *    rplext.pc
 *    reqext.pc
\*----------------------------------------------------------------------------*/
#include <retek.h>


/*------------------------------------------------------------------------------
 * Rounds a given quantity up to a given packsize.  Used in rounding requested
 * order/transfer quantities up to the size of a simple pack, or for rounding a
 * quantity up to a receivable pack size.
 */
double rms_round_to_packsize(double id_qty_to_round,
                             double id_packsize,
                             double id_round_pct)
{
   char *function = "rms_round_to_packsize";

   double ld_dividend;

   if(id_packsize <= 0)
   {
      sprintf(err_data,"Invalid pack size %lf", id_packsize);
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(-1);
   }

   if(id_qty_to_round < 0)
   {
      sprintf(err_data,"Invalid rounding quantity %lf", id_qty_to_round);
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(-1);
   }

   if (id_qty_to_round == 0) return(0);

   /* if the qty to round can be fully divided. not do rounding */
   if (!fmod(id_qty_to_round, id_packsize))
      return (id_qty_to_round);

   ld_dividend = floor(id_qty_to_round / id_packsize);

   if((id_qty_to_round - (ld_dividend * id_packsize) >=
      id_packsize * (id_round_pct / 100)) ||
      (ld_dividend == 0))
      /* round up */
      return (ld_dividend + 1) * id_packsize;
   else
      /* round down */
      return ld_dividend * id_packsize;

} /* end rms_round_to_packsize */


/*------------------------------------------------------------------------------
 * Rounds a given quantity up to a given pallet size.  Used in rounding requested
 * order qty's up to the item pallet size.  The function will only round up to the 
 * given pallet size if the round to pallet pct is reached.  If the pallet pct is 
 * not reached the function will not round down, it will return the qty sent in.
 */
double rms_round_to_pallet_size(double id_qty_to_round,
                                double id_pallet_size,
                                double id_round_pct)
{
   char *function = "rms_round_to_pallet_size";

   double ld_dividend;
   double ld_fullpallet;

   if(id_pallet_size <= 0)
   {
      sprintf(err_data,"Invalid pack size %lf", id_pallet_size);
      WRITE_ERROR(RET_FUNCTION_ERR,function,"",err_data);
      return(-1);
   }
   ld_fullpallet = fmod(id_qty_to_round, id_pallet_size);
   ld_dividend = floor(id_qty_to_round / id_pallet_size);

   if(ld_fullpallet && (id_qty_to_round - (ld_dividend * id_pallet_size) >=
      id_pallet_size * (id_round_pct / 100)))
      /* round up */
      return (ld_dividend + 1) * id_pallet_size;
   else
      /* return the qty that was sent in */
      return id_qty_to_round;

} /* end rms_round_to_pallet_size */


