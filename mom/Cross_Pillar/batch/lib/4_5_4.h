
/* LIBRARY ROUTINES: 4_5_4 */

/* BEGIN DOC

Library routine: 4_5_4

Table          Index     Select     Insert     Update     Delete
----------------------------------------------------------------
SYSTEM_OPTIONS   no       yes         no         no         no
CALENDAR        yes       yes         no         no         no

Indexes: CALENDAR(first_day), (year_454, month_454)

Function: To convert dates between calendar and 4_5_4.
          The routines use either an internal table, following a 28 year
          cycle to convert dates from 1 Jan 1900 through 31 Dec 2099 or
          the CALENDAR table.

          Synopsis:
          int CAL_TO_454(day_cal, month_cal, year_cal,
                         &day_454, &week_454, &month_454, &year_454)

          int CAL_TO_454_WNO(day_cal, month_cal, year_cal,
                             &week_in_half)

          int CAL_TO_454_HALF(day_cal, month_cal, year_cal,
                              &half_no, &month_in_half)

          int CAL_TO_CAL_WNO(day_cal, month_cal, year_cal,
                             &week_in_half)

          int CAL_TO_CAL_HALF(day_cal, week_cal, year_cal,
                              &half_no, &month_in_half)

          int C454_TO_CAL(day_454, week_454, month_454, year_454,
                          &day_cal, &month_cal, &year_cal)

          int CAL_TO_454_FDOM(day_cal, month_cal, year_cal,
                          &fday_cal, &fmonth_cal, &fyear_cal)

          int CAL_TO_454_LDOM(day_cal, month_cal, year_cal,
                          &lday_cal, &lmonth_cal, &lyear_cal)

          int CAL_TO_CAL_FDOM(day_cal, month_cal, year_cal,
                          &fday_cal, &fmonth_cal, &fyear_cal)

          int CAL_TO_CAL_LDOM(day_cal, month_cal, year_cal,
                          &lday_cal, &lmonth_cal, &lyear_cal)

          int HALF_TO_CAL_FDOH(half_no,
                               &day_cal, &month_cal, &year_cal)

          int HALF_TO_CAL_LDOH(half_no,
                               &day_cal, &month_cal, &year_cal)

          int HALF_TO_454_FDOH(half_no,
                               &day_cal, &month_cal, &year_cal)

          int HALF_TO_454_LDOH(half_no,
                               &day_cal, &month_cal, &year_cal)

          int VALIDATE_CAL(day_cal, month_cal, year_cal)

          int day_cal, month_cal, year_cal,     * Year is 4 digits
              fday_cal, fmonth_cal, fyear_cal,  * First day of 454/cal month
              lday_cal, lmonth_cal, lyear_cal,  * Last day of 454/cal month
              day_454, week_454, month_454, year_454, * Day 1 is Monday
              week_in_half               * Halves have up to 28 weeks.
              half_no, month_in_half     * Half_no is 5 digits - year * 10
                                           + 1 (first half) or + 2 (second half)
                                           Month_in_half is 1-6

          All sending fields are passed as integers, all receiving fields are
          passed as pointers to integers. Return values are zero if the date
          is valid and has been converted or -1 if not.

END DOC */

#include "retek_2.h"

#ifndef four54_H
#define four54_H

int leapyear(int year_in);

int CAL_TO_454(int day_cal,
               int month_cal,
               int year_cal,
               int * day_c454,
               int * week_c454,
               int * month_c454,
               int * year_c454);

int table_CAL_TO_454(int day_cal,
                     int month_cal,
                     int year_cal,
                     int * day_c454,
                     int * week_c454,
                     int * month_c454,
                     int * year_c454);

int get_system_options();
int CAL_TO_454_WNO(int day_cal,
                   int month_cal,
                   int year_cal,
                   int * week_in_half);

int table_CAL_TO_454_WNO(int day_cal,
                         int month_cal,
                         int year_cal,
                         int * week_in_half);

int CAL_TO_454_HALF(int day_cal,
                    int month_cal,
                    int year_cal,
                    int * half_no,
                    int * month_in_half);

int CAL_TO_CAL_WNO(int day_cal,
                   int month_cal,
                   int year_cal,
                   int * week_in_half);

int CAL_TO_CAL_HALF(int day_cal,
                    int week_cal,
                    int year_cal,
                    int * half_no,
                    int * month_in_half);

int C454_TO_CAL(int day_454,
                int week_454,
                int month_454,
                int year_454,
                int * day_cal,
                int * month_cal,
                int * year_cal);

int table_C454_TO_CAL(int day_454,
                      int week_454,
                      int month_454,
                      int year_454,
                      int * day_cal,
                      int * month_cal,
                      int * year_cal);

int CAL_TO_454_FDOM(int day_cal,
                    int month_cal,
                    int year_cal,
                    int * fday_cal,
                    int * fmonth_cal,
                    int * fyear_cal);

int CAL_TO_454_LDOM(int day_cal,
                    int month_cal,
                    int year_cal,
                    int * lday_cal,
                    int * lmonth_cal,
                    int * lyear_cal);

int table_CAL_TO_454_LDOM(int day_cal,
                          int month_cal,
                          int year_cal,
                          int * lday_cal,
                          int * lmonth_cal,
                          int * lyear_cal);

int CAL_TO_CAL_FDOM(int day_cal,
                    int month_cal,
                    int year_cal,
                    int * fday_cal,
                    int * fmonth_cal,
                    int * fyear_cal);

int CAL_TO_CAL_LDOM(int day_cal,
                    int month_cal,
                    int year_cal,
                    int * lday_cal,
                    int * lmonth_cal,
                    int * lyear_cal);

int HALF_TO_CAL_FDOH(int half_no,
                     int * day_cal,
                     int * month_cal,
                     int * year_cal);

int HALF_TO_CAL_LDOH(int half_no,
                     int * day_cal,
                     int * month_cal,
                     int * year_cal);

int HALF_TO_454_FDOH(int half_no,
                     int * day_cal,
                     int * month_cal,
                     int * year_cal);

int HALF_TO_454_LDOH(int half_no,
                     int * day_cal,
                     int * month_cal,
                     int * year_cal);

int VALIDATE_CAL(int day_cal,
                 int month_cal,
                 int year_cal);

#endif   /* !defined(four54_H) */
