CREATE OR REPLACE PACKAGE BODY MBL_COMMON_SVC AS
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
FUNCTION GET_VDATE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_vdate            OUT PERIOD.VDATE%TYPE)
RETURN NUMBER IS
   L_program  VARCHAR2(61) := 'MBL_COMMON_SVC.GET_VDATE';

BEGIN

   select period.vdate into O_vdate from period;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_VDATE;
--------------------------------------------------------------------------------
FUNCTION GET_PAGE_BOUNDRIES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_row_start       IN OUT NUMBER,
                            O_row_end         IN OUT NUMBER,
                            I_page_size       IN     NUMBER DEFAULT NULL,
                            I_page_number     IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_PAGE_BOUNDRIES';

BEGIN

   if I_page_number = 1 then
      O_row_start := 1;
   else
      O_row_start := I_page_size * I_page_number - I_page_size + 1;
   end if;
   --
   O_row_end   := O_row_start + I_page_size;
   --
   if O_row_start is null then
      O_row_start := 1;
   end if;
   --
   if O_row_end is null then
      O_row_end := 100000;
   end if;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_PAGE_BOUNDRIES;
--------------------------------------------------------------------------------
FUNCTION GET_PAGE_NUMBERS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_pagination        IN OUT MBL_PAGINATION_REC,
                          I_total_returned_rec IN     NUMBER DEFAULT NULL)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_PAGE_NUMBERS';

BEGIN

   if IO_pagination is NULL then
      IO_pagination := new MBL_PAGINATION_REC(NULL, NULL, NULL, NULL, NULL);
   end if;
   --
   IO_pagination.O_number_rec_found := I_total_returned_rec;
   --
   if I_total_returned_rec        <= 0    or
      I_total_returned_rec        <= IO_pagination.I_page_size or
      IO_pagination.I_page_size   is NULL or 
      IO_pagination.I_page_number is NULL then
      IO_pagination.O_previous_page := MBL_CONSTANTS.DUMMY_NUMBER;
      IO_pagination.O_next_page     := MBL_CONSTANTS.DUMMY_NUMBER;
   elsif IO_pagination.I_page_number = 1 then
      IO_pagination.O_previous_page := MBL_CONSTANTS.DUMMY_NUMBER;
      IO_pagination.O_next_page     := 2;
   elsif CEIL(I_total_returned_rec/IO_pagination.I_page_size) = IO_pagination.I_page_number then
      IO_pagination.O_previous_page := IO_pagination.I_page_number - 1;
      IO_pagination.O_next_page     := MBL_CONSTANTS.DUMMY_NUMBER;
   else
      IO_pagination.O_previous_page := IO_pagination.I_page_number - 1;
      IO_pagination.O_next_page     := IO_pagination.I_page_number + 1;
   end if;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_PAGE_NUMBERS;
--------------------------------------------------------------------------------
FUNCTION GET_FUNCTIONAL_CONFIG_OPTIONS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_fun_config_options_rec    OUT MBL_FUNCTIONAL_CONFIG_OPT_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_FUNCTIONAL_CONFIG_OPTIONS';
   
   cursor C_GET_REC is
      select MBL_FUNCTIONAL_CONFIG_OPT_REC(CONTRACT_IND 
                                         , ELC_IND
                                         , IMPORT_IND
                                         , ORG_UNIT_IND 
                                         , SUPPLIER_SITES_IND)
        from functional_config_options;

BEGIN

   open  C_GET_REC;
   fetch C_GET_REC into O_fun_config_options_rec;
   close C_GET_REC;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_REC%ISOPEN then
         close C_GET_REC;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_FUNCTIONAL_CONFIG_OPTIONS;
--------------------------------------------------------------------------------
FUNCTION GET_PROCUREMENT_UNIT_OPTIONS(O_error_message                IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_procurement_unit_options_rec    OUT MBL_PROCUREMENT_UNIT_OPT_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_PROCUREMENT_UNIT_OPTIONS';
   
   cursor C_GET_REC is
      select MBL_PROCUREMENT_UNIT_OPT_REC(BACKPOST_RCA_RUA_IND 
                                        , CALC_NEGATIVE_INCOME
                                        , COPY_PO_CURR_RATE
                                        , COST_LEVEL 
                                        , CREDIT_MEMO_LEVEL 
                                        , DEAL_AGE_PRIORITY 
                                        , DEAL_LEAD_DAYS 
                                        , DEAL_TYPE_PRIORITY 
                                        , DEPT_LEVEL_ORDERS 
                                        , EDI_COST_OVERRIDE_IND 
                                        , EXPIRY_DELAY_PRE_ISSUE
                                        , GEN_CONSIGNMENT_INVC_FREQ 
                                        , GEN_CON_INVC_ITM_SUP_LOC_IND 
                                        , LATEST_SHIP_DAYS 
                                        , ORD_APPR_CLOSE_DELAY 
                                        , ORD_APPR_AMT_CODE 
                                        , ORD_AUTO_CLOSE_PART_RCVD_IND 
                                        , ORD_PART_RCVD_CLOSE_DELAY 
                                        , ORDER_BEFORE_DAYS 
                                        , ORDER_EXCH_IND 
                                        , OTB_SYSTEM_IND 
                                        , RCV_COST_ADJ_TYPE 
                                        , RECLASS_APPR_ORDER_IND 
                                        , REDIST_FACTOR 
                                        , SOFT_CONTRACT_IND 
                                        , WAC_RECALC_ADJ_IND)
        from procurement_unit_options;

BEGIN

   open  C_GET_REC;
   fetch C_GET_REC into O_procurement_unit_options_rec;
   close C_GET_REC;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_REC%ISOPEN then
         close C_GET_REC;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_PROCUREMENT_UNIT_OPTIONS;
--------------------------------------------------------------------------------
FUNCTION GET_INV_MOVE_UNIT_OPTIONS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_inv_move_unit_options_rec    OUT MBL_INV_MOVE_UNIT_OPT_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_INV_MOVE_UNIT_OPTIONS';
   
   cursor C_GET_REC is
      select MBL_INV_MOVE_UNIT_OPT_REC(ALLOC_METHOD 
                                     , APPLY_PROF_PRES_STOCK 
                                     , AUTO_RCV_STORE 
                                     , CLOSE_OPEN_SHIP_DAYS 
                                     , COST_MONEY
                                     , COST_OUT_STORAGE
                                     , COST_OUT_STORAGE_MEAS 
                                     , COST_OUT_STORAGE_UOM
                                     , COST_WH_STORAGE
                                     , COST_WH_STORAGE_MEAS 
                                     , COST_WH_STORAGE_UOM
                                     , DEFAULT_ALLOC_CHRG_IND 
                                     , DEFAULT_ORDER_TYPE
                                     , DEFAULT_SIZE_PROFILE 
                                     , DEPT_LEVEL_TRANSFERS 
                                     , DISTRIBUTION_RULE 
                                     , DUPLICATE_RECEIVING_IND 
                                     , INCREASE_TSF_QTY_IND 
                                     , INTERCOMPANY_TRANSFER_BASIS 
                                     , INV_HIST_LEVEL 
                                     , LOC_ACTIVITY_IND 
                                     , LOC_DLVRY_IND 
                                     , LOOK_AHEAD_DAYS 
                                     , MAX_SCALING_ITERATIONS
                                     , MAX_WEEKS_SUPPLY 
                                     , ORD_WORKSHEET_CLEAN_UP_DELAY 
                                     , RAC_RTV_TSF_IND 
                                     , REJECT_STORE_ORD_IND
                                     , REPL_ORDER_DAYS 
                                     , RTV_NAD_LEAD_TIME
                                     , RTV_UNIT_COST_IND 
                                     , SHIP_RCV_STORE 
                                     , SHIP_RCV_WH 
                                     , STORAGE_TYPE 
                                     , STORE_PACK_COMP_RCV_IND 
                                     , WF_DEFAULT_WH
                                     , TARGET_ROI
                                     , TSF_AUTO_CLOSE_STORE 
                                     , TSF_AUTO_CLOSE_WH 
                                     , TSF_CLOSE_OVERDUE 
                                     , SIM_FORCE_CLOSE_IND
                                     , TSF_FORCE_CLOSE_IND
                                     , TSF_OVER_RECEIPT_IND
                                     , TSF_MD_STORE_TO_STORE_SND_RCV 
                                     , TSF_MD_STORE_TO_WH_SND_RCV 
                                     , TSF_MD_WH_TO_STORE_SND_RCV 
                                     , TSF_MD_WH_TO_WH_SND_RCV 
                                     , TSF_PRICE_EXCEED_WAC_IND 
                                     , SS_AUTO_CLOSE_DAYS 
                                     , WS_AUTO_CLOSE_DAYS 
                                     , SW_AUTO_CLOSE_DAYS 
                                     , WW_AUTO_CLOSE_DAYS 
                                     , WF_ORDER_LEAD_DAYS 
                                     , WH_CROSS_LINK_IND 
                                     , WRONG_ST_RECEIPT_IND)
        from inv_move_unit_options;

BEGIN

   open  C_GET_REC;
   fetch C_GET_REC into O_inv_move_unit_options_rec;
   close C_GET_REC;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_REC%ISOPEN then
         close C_GET_REC;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_INV_MOVE_UNIT_OPTIONS;
--------------------------------------------------------------------------------
FUNCTION GET_FINANCIAL_UNIT_OPTIONS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_financial_unit_options_rec    OUT MBL_FINANCIAL_UNIT_OPT_REC)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_FINANCIAL_UNIT_OPTIONS';
   
   cursor C_GET_REC is
      select MBL_FINANCIAL_UNIT_OPT_REC(BUD_SHRINK_IND
                                      , CLOSE_MTH_WITH_OPN_CNT_IND
                                      , ELC_INCLUSIVE_IND_WF_STORE
                                      , ELC_INCLUSIVE_IND_COMP_STORE
                                      , GL_ROLLUP
                                      , MARGIN_IMPACT_HIST_RECS
                                      , MAX_CUM_MARKON_PCT
                                      , MIN_CUM_MARKON_PCT
                                      , START_OF_HALF_MONTH
                                      , STD_AV_IND
                                      , STKLDGR_VAT_INCL_RETL_IND 
                                      , STOCK_LEDGER_TIME_LEVEL_CODE)
        from financial_unit_options;

BEGIN

   open  C_GET_REC;
   fetch C_GET_REC into O_financial_unit_options_rec;
   close C_GET_REC;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_REC%ISOPEN then
         close C_GET_REC;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_FINANCIAL_UNIT_OPTIONS;
--------------------------------------------------------------------------------
FUNCTION GET_CURRENCIES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_currencies_tbl    OUT MBL_CURRENCIES_TBL)
RETURN NUMBER IS
   L_program            VARCHAR2(61) := 'MBL_COMMON_SVC.GET_CURRENCIES';
   
   cursor C_GET_CURRENCIES is
      select MBL_CURRENCIES_REC(CURRENCY_CODE
                              , CURRENCY_DESC
                              , CURRENCY_COST_FMT
                              , CURRENCY_RTL_FMT
                              , CURRENCY_COST_DEC
                              , CURRENCY_RTL_DEC)
        from CURRENCIES;
   
BEGIN

   open  C_GET_CURRENCIES;
   fetch C_GET_CURRENCIES bulk collect into O_currencies_tbl;
   close C_GET_CURRENCIES;
   --
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if C_GET_CURRENCIES%ISOPEN then
         close C_GET_CURRENCIES;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_CURRENCIES;
--------------------------------------------------------------------------------
FUNCTION GET_CODE_DETAIL(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                         I_code_type     IN     CODE_HEAD.CODE_TYPE%TYPE,
                         O_result        IN OUT MBL_CODE_DETAIL_TBL)
RETURN NUMBER IS

   L_program VARCHAR2(61) := 'MBL_COMMON_SVC.GET_CODE_DETAIL';   

   cursor c_code is
      select MBL_CODE_DETAIL_REC(cd.code,
                                 cd.code_desc,
                                 cd.code_seq)
        from v_code_detail_tl cd             
       where cd.code_type     = I_code_type
       order by cd.code_seq;

BEGIN

   open c_code;
   fetch c_code bulk collect into O_result;
   close c_code;
   
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      if c_code%ISOPEN then
         close c_code;
      end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_CODE_DETAIL;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_error_message       IN OUT  VARCHAR2,
                        I_program              IN      VARCHAR2)
IS

   L_error_message VARCHAR2(1000) := IO_error_message;
   L_error_type    VARCHAR2(5)   := NULL;

BEGIN

  /* Create initial error message to be parsed in SQL_LIB.
   * This error message may alread be created */
   if L_error_message is NULL then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            I_program,
                                            to_char(SQLCODE));
   end if;
   --
   /* Pass out parsed error message and error type */
   SQL_LIB.API_MSG(L_error_type,
                   L_error_message);
   ---
   IO_error_message := L_error_message;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                             'MBL_COMMON_SVC.HANDLE_ERRORS',
                                             to_char(SQLCODE));

      SQL_LIB.API_MSG(L_error_type,
                      L_error_message);
      IO_error_message := L_error_message;
END HANDLE_ERRORS;
--------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ITEM_IMAGE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_default_item_image    OUT VARCHAR2)
RETURN NUMBER IS
   L_program  VARCHAR2(61) := 'MBL_COMMON_SVC.GET_DEFAULT_ITEM_IMAGE';
   
   cursor C_GET_DEFAULT_IMAGE is
      select image_path||default_item_image
        from ui_config_options;

BEGIN

   open C_GET_DEFAULT_IMAGE;
   fetch C_GET_DEFAULT_IMAGE into O_default_item_image;
   close C_GET_DEFAULT_IMAGE;
   ---
   RETURN MBL_CONSTANTS.SUCCESS;

EXCEPTION
   when OTHERS then
      
   DBMS_OUTPUT.PUT_LINE('in EXCEPTION GET_DEFAULT_ITEM_IMAGE');
      if C_GET_DEFAULT_IMAGE%ISOPEN then close C_GET_DEFAULT_IMAGE; end if;
      HANDLE_ERRORS(O_error_message, L_program);
      RETURN MBL_CONSTANTS.FAILURE;
END GET_DEFAULT_ITEM_IMAGE;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END MBL_COMMON_SVC;
/
