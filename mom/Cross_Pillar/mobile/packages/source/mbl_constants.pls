CREATE OR REPLACE PACKAGE MBL_CONSTANTS AUTHID CURRENT_USER AS

   ITEM_SEARCH_TYPE_ITEM       CONSTANT VARCHAR2(5)    := 'ITEM';
   ITEM_SEARCH_TYPE_VPN        CONSTANT VARCHAR2(5)    := 'VPN';
   --
   DUMMY_NUMBER                CONSTANT NUMBER(4)      := -999;
   --
   SUCCESS                     CONSTANT NUMBER(1)      := 1;
   FAILURE                     CONSTANT NUMBER(1)      := 0;
   --
   PROGRAM_ERROR               EXCEPTION;
   --
   SEARCH_FROM                 CONSTANT VARCHAR2(5)    := 'FROM';
   SEARCH_TO                   CONSTANT VARCHAR2(5)    := 'TO';
   --
   ORDER_TYPE_ARB              CONSTANT VARCHAR2(3)    := 'ARB';
   ORDER_TYPE_BRB              CONSTANT VARCHAR2(3)    := 'BRB';
   ORDER_TYPE_NB               CONSTANT VARCHAR2(3)    := 'N/B';
   --
   DUMMY_STRING                CONSTANT VARCHAR2(5)    := '-999';
   --
   PARENT_LVL                  CONSTANT VARCHAR2(20)   := 'PARENT_LEVEL';
   PARENT_DIFF_LVL             CONSTANT VARCHAR2(20)   := 'PARENT_DIFF_LEVEL';
   TRAN_LVL                    CONSTANT VARCHAR2(20)   := 'TRAN_LEVEL';
   YES_IND                     CONSTANT VARCHAR2(1)    := 'Y';
   NO_IND                      CONSTANT VARCHAR2(1)    := 'N';
   --
   LOC_TYPE_S                  CONSTANT VARCHAR2(1)    := 'S';
   LOC_TYPE_W                  CONSTANT VARCHAR2(1)    := 'W';
   STORE_TYPE_C                CONSTANT VARCHAR2(1)    := 'C';
   --
   TSF_TYPE_IC                 CONSTANT VARCHAR2(2)    := 'IC';
   TSF_TYPE_CO                 CONSTANT VARCHAR2(2)    := 'CO';
   TSF_TYPE_NS                 CONSTANT VARCHAR2(2)    := 'NS';
   TSF_TYPE_BT                 CONSTANT VARCHAR2(2)    := 'BT';
   TSF_TYPE_RAC                CONSTANT VARCHAR2(3)    := 'RAC';
   TSF_TYPE_RV                 CONSTANT VARCHAR2(2)    := 'RV';
   --
   PARTNER_TYPE_E              CONSTANT VARCHAR2(1)    := 'E';
   --
   CODE_TYPE_TRST              CONSTANT VARCHAR2(4)    := 'TRST';
   CODE_TYPE_TR4E              CONSTANT VARCHAR2(4)    := 'TR4E';
   CODE_TYPE_TR2E              CONSTANT VARCHAR2(4)    := 'TR2E';
   CODE_TYPE_TRS1              CONSTANT VARCHAR2(4)    := 'TRS1';
   --
   FINISHER_TYPE_E             CONSTANT VARCHAR2(1)    := 'E';
   FINISHER_TYPE_I             CONSTANT VARCHAR2(1)    := 'I';
   --
   TSF_STATUS_A                CONSTANT VARCHAR2(1)    := 'A';
   TSF_STATUS_I                CONSTANT VARCHAR2(1)    := 'I';
   TSF_STATUS_C                CONSTANT VARCHAR2(1)    := 'C';
   --
   ADD_IND                     CONSTANT VARCHAR2(1)    := 'A';
   --
   TSF_ENTITY_TYPE_A           CONSTANT VARCHAR2(1)    := 'A';
   TSF_ENTITY_TYPE_E           CONSTANT VARCHAR2(1)    := 'E';

END MBL_CONSTANTS;
/

