CREATE OR REPLACE PACKAGE MBL_COMMON_SVC AUTHID CURRENT_USER AS
--------------------------------------------------------------------------------
FUNCTION GET_VDATE(O_error_message IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                   O_vdate            OUT PERIOD.VDATE%TYPE)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_PAGE_BOUNDRIES(O_error_message   IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                            O_row_start       IN OUT NUMBER,
                            O_row_end         IN OUT NUMBER,
                            I_page_size       IN     NUMBER DEFAULT NULL,
                            I_page_number     IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_PAGE_NUMBERS(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                          IO_pagination        IN OUT MBL_PAGINATION_REC,
                          I_total_returned_rec IN     NUMBER DEFAULT NULL)
RETURN NUMBER;
--------------------------------------------------------------------
FUNCTION GET_FUNCTIONAL_CONFIG_OPTIONS(O_error_message          IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                       O_fun_config_options_rec    OUT MBL_FUNCTIONAL_CONFIG_OPT_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_PROCUREMENT_UNIT_OPTIONS(O_error_message                IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                      O_procurement_unit_options_rec    OUT MBL_PROCUREMENT_UNIT_OPT_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_INV_MOVE_UNIT_OPTIONS(O_error_message             IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                   O_inv_move_unit_options_rec    OUT MBL_INV_MOVE_UNIT_OPT_REC)
RETURN NUMBER;
--------------------------------------------------------------------------
FUNCTION GET_FINANCIAL_UNIT_OPTIONS(O_error_message              IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                    O_financial_unit_options_rec    OUT MBL_FINANCIAL_UNIT_OPT_REC)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_CURRENCIES(O_error_message  IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                        O_currencies_tbl    OUT MBL_CURRENCIES_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
FUNCTION GET_CODE_DETAIL(O_error_message   IN OUT   RTK_ERRORS.RTK_TEXT%TYPE,
                         I_code_type       IN       CODE_HEAD.CODE_TYPE%TYPE,
                         O_result          IN OUT   MBL_CODE_DETAIL_TBL)
RETURN NUMBER;
--------------------------------------------------------------------------------
PROCEDURE HANDLE_ERRORS(IO_error_message       IN OUT  VARCHAR2,
                        I_program              IN      VARCHAR2);
--------------------------------------------------------------------------------
FUNCTION GET_DEFAULT_ITEM_IMAGE(O_error_message      IN OUT RTK_ERRORS.RTK_TEXT%TYPE,
                                O_default_item_image    OUT VARCHAR2)
RETURN NUMBER;
--------------------------------------------------------------------------------
END MBL_COMMON_SVC;
/

