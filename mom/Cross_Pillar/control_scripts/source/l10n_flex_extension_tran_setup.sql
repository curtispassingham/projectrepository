
PROMPT Merge into EXT_ENTITY_KEY_DESCS

---------------Merge into EXT_ENTITY_KEY_DESCS------------------------
merge into ext_entity_key_descs kl using ( select
'TSF_ENTITY' BASE_RMS_TABLE, 'TSF_ENTITY_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Transfer Entity' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'WH' BASE_RMS_TABLE, 'WH' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Warehouse' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'STORE' BASE_RMS_TABLE, 'STORE' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Store' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'OUTLOC' BASE_RMS_TABLE, 'OUTLOC_TYPE' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Location Type' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'OUTLOC' BASE_RMS_TABLE, 'OUTLOC_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Location' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'SUPS' BASE_RMS_TABLE, 'SUPPLIER' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 
decode((select supplier_sites_ind from system_options), 'Y', 'Supplier Site', 'N', 'Supplier') KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'PARTNER' BASE_RMS_TABLE, 'PARTNER_TYPE' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Partner Type' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'PARTNER' BASE_RMS_TABLE, 'PARTNER_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Partner' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_COUNTRY' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Item' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_COUNTRY' BASE_RMS_TABLE, 'COUNTRY_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Country Code' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ORDHEAD' BASE_RMS_TABLE, 'ORDER_NO' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Order No.' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'TSFHEAD' BASE_RMS_TABLE, 'TSF_NO' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Transfer' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'MRT' BASE_RMS_TABLE, 'MRT_NO' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'MRT No.' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'COUNTRY' BASE_RMS_TABLE, 'COUNTRY_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Country Code' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'STORE_ADD' BASE_RMS_TABLE, 'STORE' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Store' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'FIF_GL_SETUP' BASE_RMS_TABLE, 'SET_OF_BOOKS_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Set of Books ID' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ORDCUST' BASE_RMS_TABLE, 'CUST_ID' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Customer ID' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ORDCUST' BASE_RMS_TABLE, 'ORDCUST_SEQ_NO' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Customer Sequence No.' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_SUPPLIER' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Item' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_SUPPLIER' BASE_RMS_TABLE, 'SUPPLIER' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 
decode((select supplier_sites_ind from system_options), 'Y', 'Supplier Site', 'N', 'Supplier') KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_LOC' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Item' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------
merge into ext_entity_key_descs kl using ( select
'ITEM_LOC' BASE_RMS_TABLE, 'LOCATION' KEY_COL, 1 LANG, 'Y' DEFAULT_LANG_IND, 'Location' KEY_DESC
from dual ) use_this
on (kl.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and kl.KEY_COL = use_this.KEY_COL and kl.LANG = use_this.LANG)
when matched then update
set kl.DEFAULT_LANG_IND = use_this.DEFAULT_LANG_IND, kl.KEY_DESC = use_this.KEY_DESC
when not matched then insert (BASE_RMS_TABLE, KEY_COL, LANG, DEFAULT_LANG_IND, KEY_DESC)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.LANG, use_this.DEFAULT_LANG_IND, use_this.KEY_DESC);
---------------------

COMMIT;
