SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   non_merch_code_head.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Refreshes non_merch_code_head  and non_merch_code_head_tl tables.
REM
REM ------------------------------------------------------------------------------
REM ******************************************************************************
PROMPT Updating/inserting into NON_MERCH_CODE_HEAD and NON_MERCH_CODE_HEAD_TL
REM ******************************************************************************
-----------------------------------------------------------------------------------
MERGE INTO NON_MERCH_CODE_HEAD NONMERCH USING ( SELECT
'M' NON_MERCH_CODE, 'N' SERVICE_IND
FROM DUAL) USE_THIS
ON (NONMERCH.NON_MERCH_CODE = USE_THIS.NON_MERCH_CODE)
WHEN MATCHED THEN UPDATE
SET NONMERCH.SERVICE_IND = USE_THIS.SERVICE_IND
WHEN NOT MATCHED THEN INSERT (NON_MERCH_CODE, SERVICE_IND)
VALUES (USE_THIS.NON_MERCH_CODE, USE_THIS.SERVICE_IND);

MERGE INTO NON_MERCH_CODE_HEAD_TL NONMERCHTL USING ( SELECT
'M' NON_MERCH_CODE, '1' LANG, 'Miscellaneous' NON_MERCH_CODE_DESC, 'Y' ORIG_LANG_IND, 'N' REVIEWED_IND , GET_USER CREATE_ID, SYSDATE CREATE_DATETIME, GET_USER LAST_UPDATE_ID, SYSDATE LAST_UPDATE_DATETIME
FROM DUAL) USE_THIS
ON (NONMERCHTL.NON_MERCH_CODE = USE_THIS.NON_MERCH_CODE AND NONMERCHTL.LANG = USE_THIS.LANG)
WHEN MATCHED THEN UPDATE
SET NONMERCHTL.NON_MERCH_CODE_DESC = USE_THIS.NON_MERCH_CODE_DESC, NONMERCHTL.LAST_UPDATE_ID = GET_USER, NONMERCHTL.LAST_UPDATE_DATETIME = USE_THIS.LAST_UPDATE_DATETIME
WHEN NOT MATCHED THEN INSERT (NON_MERCH_CODE, LANG, NON_MERCH_CODE_DESC, ORIG_LANG_IND, REVIEWED_IND, CREATE_ID, CREATE_DATETIME, LAST_UPDATE_ID, LAST_UPDATE_DATETIME)
VALUES (USE_THIS.NON_MERCH_CODE, USE_THIS.LANG, USE_THIS.NON_MERCH_CODE_DESC, USE_THIS.ORIG_LANG_IND, USE_THIS.REVIEWED_IND, USE_THIS.CREATE_ID, USE_THIS.CREATE_DATETIME, USE_THIS.LAST_UPDATE_ID, USE_THIS.LAST_UPDATE_DATETIME);
-----------------------------------------------------------------------------------
COMMIT;