SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   coresvc_item_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into coresvc_costchg_config table if entry does not exist.
REM
REM ------------------------------------------------------------------------------

DECLARE
   L_costchg_config_exist   VARCHAR2(1);

   cursor C_COSTCHG_CONFIG_EXIST is
      select 'x'
        from coresvc_costchg_config;

BEGIN
   open C_COSTCHG_CONFIG_EXIST;
   fetch C_COSTCHG_CONFIG_EXIST into L_costchg_config_exist;
   close C_COSTCHG_CONFIG_EXIST;

   if L_costchg_config_exist is NULL then
      insert into coresvc_costchg_config(max_chunk_size,
                                         variance_comparison_basis)
                                  values(5,
                                         'B');
   else
      update coresvc_costchg_config
         set max_chunk_size = NVL(max_chunk_size, 5),
             variance_comparison_basis = 'B';
            
   end if; 
END;
/
REM ****** (commiting...) ********************************************
COMMIT;