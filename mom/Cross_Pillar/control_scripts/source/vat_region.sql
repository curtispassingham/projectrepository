SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   vat_region.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Updating existing Vat Regions with vat_calc_type as S for NULL values.
REM
REM ------------------------------------------------------------------------------

BEGIN
   update vat_region set vat_calc_type = 'S' where vat_calc_type is NULL;
END;
/
REM ****** (commiting...) ********************************************
COMMIT;