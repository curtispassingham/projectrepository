SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   item_induct_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into item_induct_config table if entry does not exist.
REM
REM ------------------------------------------------------------------------------

DECLARE
   L_item_ind_conf_exist   VARCHAR2(1);

   cursor C_ITEM_IND_CONF_EXIST is
      select 'x'
        from item_induct_config;

BEGIN
   open C_ITEM_IND_CONF_EXIST;
   fetch C_ITEM_IND_CONF_EXIST into L_item_ind_conf_exist;
   close C_ITEM_IND_CONF_EXIST;

   if L_item_ind_conf_exist is NULL then
      INSERT INTO ITEM_INDUCT_CONFIG (max_items_for_dnld,
                                      max_items_for_sync_dnld,
                                      max_file_size_for_upld,
                                      max_file_size_for_sync_upld
                                     )
                             VALUES (5000,
                                     100,
                                     500000,
                                     100000);
   end if;
END;
/
REM ****** (commiting...) ********************************************
COMMIT;