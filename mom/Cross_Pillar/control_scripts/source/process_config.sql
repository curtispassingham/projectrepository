SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
--------------------------------------------------------
-- Copyright (c) 2000, Oracle All rights reserved.
-- $Workfile:   process_config.sql  $
-- $Revision:   $
-- $Modtime:    $
--------------------------------------------------------

REM ------------------------------------------------------------------
REM
REM  This script will refresh the data in the process_config table
REM  The tables merged into are:
REM  - PROCESS_CONFIG
REM
REM ------------------------------------------------------------------


REM -------------------------------------------------
REM
REM  Refreshes PROCESS_CONFIG table.
REM
REM -------------------------------------------------
REM -------------------------------------------------
set define "^";
REM ****************************************************************
PROMPT updating/inserting into PROCESS_CONFIG
REM ****************************************************************
MERGE INTO PROCESS_CONFIG PC USING
(SELECT 'LIKE_STORE' AS PROCESS_NAME,'ASYNC' AS PROCESS_MODE FROM DUAL
) SQ ON (PC.PROCESS_NAME =SQ.PROCESS_NAME)
WHEN MATCHED THEN
  UPDATE SET PROCESS_MODE = SQ.PROCESS_MODE WHEN NOT MATCHED THEN
  INSERT
    (
      PROCESS_NAME,
      PROCESS_MODE
    )
    VALUES
    (
      SQ.PROCESS_NAME,
      SQ.PROCESS_MODE
    );
set define "&";
COMMIT;