SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   s9t_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into s9t_config table if entry does not exist.
REM
REM ------------------------------------------------------------------------------

DECLARE
   L_s9t_conf_exist   VARCHAR2(1);

   cursor C_s9t_CONF_EXIST is
      select 'x'
        from s9t_config;

BEGIN
   open C_s9t_CONF_EXIST;
   fetch C_s9t_CONF_EXIST into L_s9t_conf_exist;
   close C_s9t_CONF_EXIST;

   if L_s9t_conf_exist is NULL then
      INSERT INTO S9T_CONFIG (DROP_DOWN_DEPTH,
                                      REQUIRED_VISUAL_COLOR_CODE
                                     )
                             VALUES (10000,
                                     '#ffffcc');
   end if;
END;
/
REM ****** (commiting...) ********************************************
COMMIT;
