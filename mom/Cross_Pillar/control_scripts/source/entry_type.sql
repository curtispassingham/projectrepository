
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
-- :   entry_type.sql  $
-- :   1.0  $
-- :   Wed Apr 11 18:02:11 CDT 2012  $
--------------------------------------------------------
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
REM -------------------------------------------------
REM
REM This script contains ONLY DATA REQUIRED by RTM 
REM for the system to run. This must be run after
REM the System Options table has been populated. 
REM 
REM Tables loaded in this script include:
REM ENTRY_TYPE
REM
REM The last line of this script issues a commit.
REM
REM -------------------------------------------------
REM -------------------------------------------------
set define "^";
REM ****************************************************************
PROMPT updating/inserting into entry_type
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'00' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'01' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption - Free and Dutiable' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'02' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption - Quota / Visa' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'03' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption - Antidumping / Countervailing Duty' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'04' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Appraisement' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'05' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Vessel - Repair' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'06' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption - Foreign Trade Zone FTZ)' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'07' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Consumption - Antidumping / Countervailing Duty and Quota / Visa Combination' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'08' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'NAFTA Duty Deferral' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'10' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Informal Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'11' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Informal - Free and Dutiable' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'12' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Informal - Quota / Visa other than textiles' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'20' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'21' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'22' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Re-Warehouse' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'23' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Temporary Importation Bond (TIB)' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'24' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Trade Fair' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'25' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Permanent Exhibition' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'26' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse - Foreign Trade Zone (FTZ) (Admission)' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'30' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'31' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal Consumption' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'32' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal - Quota' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'33' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Aircraft and Vessel Supply (For Immediate Exportation)' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'34' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal Antidumping / Countervailing Duty' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'35' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal for Transportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'36' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal for Immediate Exportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'37' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal for Transportation and Exportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'38' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Warehouse Withdrawal - Antidumping / Countervailing Duty and Quota / Visa Combination' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'40' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Drawback Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'41' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Direct Identification Manufacturing Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'42' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Direct Identification Unused Merchandise Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'43' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Rejected Merchandise Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'44' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Substitution Manufacturer Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'45' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Substitution Unused Merchandise Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'46' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Other Drawback' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'50' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Government Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'51' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Defense Contract Administration Service Region (DCASR)' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'52' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Government - Dutiable' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'60' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Transportation Category' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'61' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Immediate Transportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'62' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Transportation and Exportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'63' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Immediate Exportation' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'64' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Barge Movement' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'65' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Permit to Proceed' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'66' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Baggage' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
MERGE INTO ENTRY_TYPE ERR USING (SELECT
'90' ENTRY_TYPE, BASE_COUNTRY_ID IMPORT_COUNTRY_ID, 'Special Entry Processing - Not in Use' ENTRY_TYPE_DESC
FROM SYSTEM_OPTIONS) USE_THIS
ON (ERR.ENTRY_TYPE = USE_THIS.ENTRY_TYPE AND ERR.IMPORT_COUNTRY_ID = USE_THIS.IMPORT_COUNTRY_ID)
WHEN MATCHED THEN UPDATE
SET ERR.ENTRY_TYPE_DESC = USE_THIS.ENTRY_TYPE_DESC
WHEN NOT MATCHED THEN INSERT (ENTRY_TYPE,IMPORT_COUNTRY_ID,ENTRY_TYPE_DESC)
VALUES (USE_THIS.ENTRY_TYPE,USE_THIS.IMPORT_COUNTRY_ID,USE_THIS.ENTRY_TYPE_DESC);
------------------------------------------------------------------------
set define "&";
commit;
