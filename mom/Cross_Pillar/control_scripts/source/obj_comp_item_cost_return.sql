WHENEVER SQLERROR EXIT FAILURE ROLLBACK
CREATE OR REPLACE FUNCTION OBJ_COMP_ITEM_COST_RETURN (I_collection IN OBJ_COMP_ITEM_COST_TBL)
   RETURN OBJ_COMP_ITEM_COST_TBL IS
BEGIN
   RETURN I_collection;
END OBJ_COMP_ITEM_COST_RETURN;
/
CREATE OR REPLACE TYPE OBJ_COMP_ITEM_COST_RETURN_INT AS OBJECT (

   dummy_attribute NUMBER,

   STATIC FUNCTION ODCIGetInterfaces (
                   O_interfaces OUT SYS.ODCIObjectList
                   ) RETURN NUMBER,

   STATIC FUNCTION ODCIStatsTableFunction (
                   I_function   IN  SYS.ODCIFuncInfo,
                   O_stats      OUT SYS.ODCITabFuncStats,
                   I_args       IN  SYS.ODCIArgDescList,
                   I_collection IN  OBJ_COMP_ITEM_COST_TBL
                   ) RETURN NUMBER

);
/
CREATE OR REPLACE TYPE BODY OBJ_COMP_ITEM_COST_RETURN_INT AS

   STATIC FUNCTION ODCIGetInterfaces (
                   O_interfaces OUT SYS.ODCIObjectList
                   ) RETURN NUMBER IS
   BEGIN
      O_interfaces := SYS.ODCIObjectList(
                         SYS.ODCIObject ('SYS', 'ODCISTATS2')
                         );
      RETURN ODCIConst.success;
   END ODCIGetInterfaces;

   STATIC FUNCTION ODCIStatsTableFunction (
                   I_function   IN  SYS.ODCIFuncInfo,
                   O_stats      OUT SYS.ODCITabFuncStats,
                   I_args       IN  SYS.ODCIArgDescList,
                   I_collection IN  OBJ_COMP_ITEM_COST_TBL
                   ) RETURN NUMBER IS
   BEGIN
      O_stats := SYS.ODCITabFuncStats(I_collection.COUNT);
      RETURN ODCIConst.success;
   END ODCIStatsTableFunction;

END;
/
set serveroutput on;
declare
   no_association EXCEPTION;
   PRAGMA EXCEPTION_INIT(no_association, -29931);
begin
   execute immediate 'DISASSOCIATE STATISTICS FROM FUNCTIONS OBJ_COMP_ITEM_COST_RETURN';
exception
   when no_association then
      null;
   when others then
      DBMS_OUTPUT.PUT_LINE (SQLERRM);
end;
/
ASSOCIATE STATISTICS WITH FUNCTIONS OBJ_COMP_ITEM_COST_RETURN USING OBJ_COMP_ITEM_COST_RETURN_INT;
--CREATE PUBLIC SYNONYM OBJ_COMP_ITEM_COST_RETURN FOR OBJ_COMP_ITEM_COST_RETURN;
--CREATE PUBLIC SYNONYM OBJ_COMP_ITEM_COST_RETURN_INT FOR OBJ_COMP_ITEM_COST_RETURN_INT;
GRANT EXECUTE ON OBJ_COMP_ITEM_COST_RETURN TO PUBLIC;
GRANT EXECUTE ON OBJ_COMP_ITEM_COST_RETURN_INT TO PUBLIC;
