
PROMPT Merge into EXT_ENTITY

---------------------Merge into EXT_ENTITY-----------------------------
merge into ext_entity ext using ( select
1 EXT_ENTITY_ID, 'TSF_ENTITY' BASE_RMS_TABLE, 'TSF_ENTITY_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
2 EXT_ENTITY_ID, 'WH' BASE_RMS_TABLE, 'WH_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
3 EXT_ENTITY_ID, 'STORE' BASE_RMS_TABLE, 'STORE_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
4 EXT_ENTITY_ID, 'OUTLOC' BASE_RMS_TABLE, 'OUTLOC_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
5 EXT_ENTITY_ID, 'SUPS' BASE_RMS_TABLE, 'SUPS_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
6 EXT_ENTITY_ID, 'PARTNER' BASE_RMS_TABLE, 'PARTNER_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
7 EXT_ENTITY_ID, 'ITEM_COUNTRY' BASE_RMS_TABLE, 'ITEM_COUNTRY_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
8 EXT_ENTITY_ID, 'ORDHEAD' BASE_RMS_TABLE, 'ORDHEAD_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
9 EXT_ENTITY_ID, 'TSFHEAD' BASE_RMS_TABLE, 'TSFHEAD_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
10 EXT_ENTITY_ID, 'MRT' BASE_RMS_TABLE, 'MRT_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
11 EXT_ENTITY_ID, 'COUNTRY' BASE_RMS_TABLE, 'COUNTRY_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
12 EXT_ENTITY_ID, 'STORE_ADD' BASE_RMS_TABLE, 'STORE_ADD_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
13 EXT_ENTITY_ID, 'FIF_GL_SETUP' BASE_RMS_TABLE, 'FIF_GL_SETUP_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
14 EXT_ENTITY_ID, 'ORDCUST' BASE_RMS_TABLE, 'ORDCUST_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
15 EXT_ENTITY_ID, 'ITEM_SUPPLIER' BASE_RMS_TABLE, 'ITEM_SUPPLIER_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);
---------------------
merge into ext_entity ext using ( select
16 EXT_ENTITY_ID, 'ITEM_LOC' BASE_RMS_TABLE, 'ITEM_LOC_L10N_EXT' L10N_EXT_TABLE, 'Y' BASE_IND
from dual) use_this
on (ext.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE)
when not matched then insert (EXT_ENTITY_ID, BASE_RMS_TABLE, L10N_EXT_TABLE, BASE_IND)
values (use_this.EXT_ENTITY_ID, use_this.BASE_RMS_TABLE, use_this.L10N_EXT_TABLE, use_this.BASE_IND);

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

PROMPT Merge into EXT_ENTITY_KEY

-------------------Merge into EXT_ENTITY_KEY---------------------------
merge into ext_entity_key key using ( select
'TSF_ENTITY' BASE_RMS_TABLE, 'TSF_ENTITY_ID' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_TSF_ENTITY_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'WH' BASE_RMS_TABLE, 'WH' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_WH_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'STORE' BASE_RMS_TABLE, 'STORE' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_STORE_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'OUTLOC' BASE_RMS_TABLE, 'OUTLOC_TYPE' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'OUTLOC' BASE_RMS_TABLE, 'OUTLOC_ID' KEY_COL, 2 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_OUTLOC_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'SUPS' BASE_RMS_TABLE, 'SUPPLIER' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_SUPPLIER_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'PARTNER' BASE_RMS_TABLE, 'PARTNER_TYPE' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'PARTNER' BASE_RMS_TABLE, 'PARTNER_ID' KEY_COL, 2 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_PARTNER_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_COUNTRY' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_ITEM_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_COUNTRY' BASE_RMS_TABLE, 'COUNTRY_ID' KEY_COL, 2 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_COUNTRY_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ORDHEAD' BASE_RMS_TABLE, 'ORDER_NO' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'TSFHEAD' BASE_RMS_TABLE, 'TSF_NO' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'MRT' BASE_RMS_TABLE, 'MRT_NO' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'COUNTRY' BASE_RMS_TABLE, 'COUNTRY_ID' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_COUNTRY_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'STORE_ADD' BASE_RMS_TABLE, 'STORE' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_STORE_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'FIF_GL_SETUP' BASE_RMS_TABLE, 'SET_OF_BOOKS_ID' KEY_COL, 1 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_SOB_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ORDCUST' BASE_RMS_TABLE, 'CUST_ID' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ORDCUST' BASE_RMS_TABLE, 'ORDCUST_SEQ_NO' KEY_COL, 2 KEY_NUMBER, 'NUMBER'  DATA_TYPE, '' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_SUPPLIER' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_ITEM_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_SUPPLIER' BASE_RMS_TABLE, 'SUPPLIER' KEY_COL, 2 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_SUPPLIER_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_LOC' BASE_RMS_TABLE, 'ITEM' KEY_COL, 1 KEY_NUMBER, 'VARCHAR2'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_ITEM_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------
merge into ext_entity_key key using ( select
'ITEM_LOC' BASE_RMS_TABLE, 'LOCATION' KEY_COL, 2 KEY_NUMBER, 'NUMBER'  DATA_TYPE, 'L10N_BR_FND_SQL.GET_LOC_DESC' DESCRIPTION_CODE, 'N' L10N_DEPENDENCY_IND
from dual) use_this
on (key.BASE_RMS_TABLE = use_this.BASE_RMS_TABLE and key.KEY_COL = use_this.KEY_COL)
when matched then update
set key.DESCRIPTION_CODE = use_this.DESCRIPTION_CODE
when not matched then insert (BASE_RMS_TABLE, KEY_COL, KEY_NUMBER, DATA_TYPE, DESCRIPTION_CODE, L10N_DEPENDENCY_IND)
values (use_this.BASE_RMS_TABLE, use_this.KEY_COL, use_this.KEY_NUMBER, use_this.DATA_TYPE, use_this.DESCRIPTION_CODE, use_this.L10N_DEPENDENCY_IND);
---------------------

COMMIT;
