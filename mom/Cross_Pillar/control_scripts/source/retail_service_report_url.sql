
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Corp.  All rights reserved.
-- :   retail_service_report_url.sql  $
-- :   1.0  $
-- :   Mon Jun 11 14:32:30 CDT 2012  $
--------------------------------------------------------
REM -------------------------------------------------
REM
REM  Refreshes retail_service_report_url table.
REM
REM -------------------------------------------------
REM -------------------------------------------------
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
set define "^";
REM ****************************************************************
PROMPT updating/inserting into retail_service_report_url
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'CRDMEC' RS_CODE, 'Credit Memo cost document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'CRDMEQ' RS_CODE, 'Credit Memo quantity document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'CRDNT' RS_CODE, 'Credit Note document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'DEBMEC' RS_CODE, 'Debit Memo cost document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'DEBMEQ' RS_CODE, 'Debit Memo quantity document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'DEBMEV' RS_CODE, 'Debit Memo VAT document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'GFD' RS_CODE, 'GL Fixed Deal data Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'GID' RS_CODE, 'GL Item rollup Daily data Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'GIM' RS_CODE, 'GL Item rollup Monthly data Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'GIT' RS_CODE, 'GL ITem level data Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'GSA' RS_CODE, 'GL Sales Audit data Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'MRCHI' RS_CODE, 'Merchandise invoice document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'NMRCHI' RS_CODE, 'Non-Merchandise invoice document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'RAV' RS_CODE, 'Retail Account Validation Service' RS_NAME, 'S' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'RDF' RS_CODE, 'Retail DrillForward Service' RS_NAME, 'S' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
MERGE INTO RETAIL_SERVICE_REPORT_URL ERR USING ( SELECT
'RWO' RS_CODE, 'Receipt Write Off document Report' RS_NAME, 'R' RS_TYPE
FROM DUAL) USE_THIS
ON (ERR.RS_CODE = USE_THIS.RS_CODE)
WHEN NOT MATCHED THEN INSERT (RS_CODE,RS_NAME,RS_TYPE)
VALUES (USE_THIS.RS_CODE,USE_THIS.RS_NAME,USE_THIS.RS_TYPE);
------------------------------------------------------------------------
set define "&";
commit;
