SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   po_induct_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into po_induct_config table 
REM
REM ------------------------------------------------------------------------------

BEGIN
   delete from po_induct_config;

   insert into po_induct_config(max_po_for_dnld,
                                max_po_for_sync_dnld,
                                max_file_size_for_upld, 
                                max_file_size_for_sync_upld)
                         values(5000,
                                500,
                                500000,
                                100000);
                      
END;
/
REM ****** (commiting...) ********************************************
COMMIT;
