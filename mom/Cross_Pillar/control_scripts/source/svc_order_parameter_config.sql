SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2014, Oracle Inc.  All rights reserved.
-- :   svc_order_parameter_config.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Insert into svc_order_parameter_config table 
REM
REM ------------------------------------------------------------------------------

BEGIN
   delete from svc_order_parameter_config;

   insert into svc_order_parameter_config(max_chunk_size,
                                          max_threads,
                                          wait_btwn_threads, 
                                          max_order_no_qty,
                                          max_order_expiry_days,
                                          apply_scaling,
                                          apply_deals,
                                          recalc_replenishment,
                                          apply_brackets,
                                          cancel_alloc,
                                          otb_override,
                                          skip_open_shipment,
                                          override_manl_cost_src)
                                   values(5,
                                          5,
                                          1000,
                                          5,
                                          10,
                                          'N',
                                          'Y',
                                          'Y',
                                          'Y',
                                          'N',
                                          'Y',
                                          'N',
                                          'N');

END;
/
REM ****** (commiting...) ********************************************
COMMIT;
