SET FEEDBACK ON
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
------------------------------------------------------------------------------------------
/*  File   : xxadeo_rms_async_job.sql                                                   */ 
/*  Desc   : This script will change the subscriber of the ITEM_INDUCT queue            */ 
/*  Company: XXADEO_RMS                                                                 */ 
/*  Created: Elsa Barros                                                                */ 
/*  Date   : 16-08-2018                                                                 */ 
----------------------------------------------------------------------------------------*/ 
declare
  --
  L_error_message       RTK_ERRORS.RTK_TEXT%TYPE;
  L_job_type            RMS_ASYNC_JOB.JOB_TYPE%TYPE;
  L_register_event_ind  VARCHAR2(1);
  --
begin
  -- Call the function
  L_job_type           := 'ITEM_INDUCT';
  L_register_event_ind := 'Y';
  --
  update rms_async_job
     set dequeue_event = 'XXADEO_RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT'
   where job_type      = L_job_type;
  --
  --
  -- change the queue subscriber of the ITEM_INDUCT queue
  --
  if rms_async_queue_sql.create_queue_subscriber(O_error_message => L_error_message,
                                                 i_job_type => L_job_type,
                                                 i_register_event_ind => L_register_event_ind) = FALSE then
    --
    DBMS_OUTPUT.PUT_LINE('Change of the subcriber, failed: '||L_error_message);
    --
  end if;
  --
EXCEPTION
  --
  when OTHERS then
    --
     DBMS_OUTPUT.PUT_LINE('exception: '||SQLCODE||', '||SQLERRM);
    --
end;
/
