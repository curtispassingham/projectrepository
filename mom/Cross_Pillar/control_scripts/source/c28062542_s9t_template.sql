--------------------------------------------------------
-- Copyright (c) 2017, Oracle Corp.  All rights reserved.
-- $Workfile$
-- $Revision$
-- $Modtime$
--------------------------------------------------------


----------------------------------------------------------------------------
--  	ATTENTION: This script DOES NOT preserve data.
--
--	The customer DBA is responsible to review this script to ensure
--	data is preserved as desired.
--
----------------------------------------------------------------------------

whenever sqlerror exit

--------------------------------------
--       Modifying Table               
--------------------------------------

PROMPT Modifying Table 'S9T_TEMPLATE'
UPDATE S9T_TEMPLATE SET TEMPLATE_NAME='Seasons-Phases' ,TEMPLATE_DESC='Seasons-Phases' WHERE TEMPLATE_KEY IN('SEASONS_DATA');

/

COMMIT
/

