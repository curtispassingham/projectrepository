SET FEEDBACK ON
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET SERVEROUTPUT ON
----------------------------------------------------------------------------------
-- Copyright (c) 2012, Oracle Inc.  All rights reserved.
-- :   rms_async_job.sql  $
----------------------------------------------------------------------------------
REM ------------------------------------------------------------------------------
REM
REM  Refreshes RMS_ASYNC_JOB table.  And creates the queues.
REM
REM ------------------------------------------------------------------------------
REM ******************************************************************************
PROMPT Deleting/Updating/inserting into RMS_ASYNC_JOB
REM ******************************************************************************
-----------------------------------------------------------------------------------
DECLARE
   L_count_old  NUMBER;
   L_count_new  NUMBER;

   L_register_event_ind   VARCHAR2(1)  := 'Y';
   L_job_type             RMS_ASYNC_JOB.JOB_TYPE%TYPE;
   L_queue_table          RMS_ASYNC_JOB.QUEUE_TABLE%TYPE;
   L_queue_name           RMS_ASYNC_JOB.QUEUE_NAME%TYPE;
   L_recipient            RMS_ASYNC_JOB.RECIPIENT_NAME%TYPE;
   L_queue_payload_type   RMS_ASYNC_JOB.QUEUE_PAYLOAD_TYPE%TYPE;
   L_event                RMS_ASYNC_JOB.DEQUEUE_EVENT%TYPE;
   L_user                 RMS_ASYNC_STATUS.CREATE_ID%TYPE   := USER;

   L_queue_count          NUMBER        := 0;
   L_queue_table_count    NUMBER        := 0;
   L_subscriber_count     NUMBER        := 0;

   cursor C_JOB_QUEUE is
      select queue_table,
             queue_name,
             recipient_name,
             queue_payload_type,
             dequeue_event,
             job_type
        from rms_async_job,
             system_config_options;

   cursor C_SUBSCRIBER_EXISTS is
      select count(1)
        from dba_queue_subscribers
       where owner      = L_user
         and queue_name = L_queue_name;

   cursor C_QUEUE_EXISTS is
      select count(1)
        from dba_queues
       where owner = L_user
         and name  = L_queue_name;

   cursor C_QUEUE_TABLE_EXISTS is
      select count(1)
        from dba_queue_tables
       where owner       = L_user
         and queue_table = L_queue_table;

BEGIN

   select count(*)
     into L_count_old
     from rms_async_job;
	 
  ------------------------------------------------------------------------------------
   DELETE FROM RMS_ASYNC_JOB 
      WHERE JOB_TYPE ='RMS_NOTIFICATION';
  -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'NEW_ITEM_LOC' JOB_TYPE,
               'Performs new item location ranging.' JOB_DESCRIPTION,
               'NEW_ITEM_LOC_QTAB' QUEUE_TABLE,
               'NEW_ITEM_LOC_QUEUE' QUEUE_NAME,
               'NEW_ITEM_LOC_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_NEW_ITEM_LOC' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/item/view/itemsetup/itemmaster/flow/MaintainItemFlow.xml#MaintainItemFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'UPDATE_ITEM_LOC' JOB_TYPE,
               'Updates item location details.' JOB_DESCRIPTION,
               'UPDATE_ITEM_LOC_QTAB' QUEUE_TABLE,
               'UPDATE_ITEM_LOC_QUEUE' QUEUE_NAME,
               'UPDATE_ITEM_LOC_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_UPDATE_ITEM_LOC' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/asyncjoblog/MaintainAsyncJobLogFlow.xml#MaintainAsyncJobLogFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'STORE_ADD' JOB_TYPE,
               'Processing to add a store.' JOB_DESCRIPTION,
               'STORE_ADD_QTAB' QUEUE_TABLE,
               'STORE_ADD_QUEUE' QUEUE_NAME,
               'STORE_ADD_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_STORE_ADD' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/asyncjoblog/MaintainAsyncJobLogFlow.xml#MaintainAsyncJobLogFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'WH_ADD' JOB_TYPE,
               'Performs warehouse add processing.' JOB_DESCRIPTION,
               'WH_ADD_QTAB' QUEUE_TABLE,
               'WH_ADD_QUEUE' QUEUE_NAME,
               'WH_ADD_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_WH_ADD' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/asyncjoblog/MaintainAsyncJobLogFlow.xml#MaintainAsyncJobLogFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'STKLGR_INSERT' JOB_TYPE,
               'Performs stock ledger insert processing.' JOB_DESCRIPTION,
               'STKLGR_INSERT_QTAB' QUEUE_TABLE,
               'STKLGR_INSERT_QUEUE' QUEUE_NAME,
               'STKLGR_INSERT_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_STKLGR_INSERT' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/asyncjoblog/MaintainAsyncJobLogFlow.xml#MaintainAsyncJobLogFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'ITEM_INDUCT' JOB_TYPE,
               'Processing of an item induction upload/download.' JOB_DESCRIPTION,
               'ITEM_INDUCT_QTAB' QUEUE_TABLE,
               'ITEM_INDUCT_QUEUE' QUEUE_NAME,
               'ITEM_INDUCT_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_ITEM_INDUCT' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/induction/status/flow/MaintainDataLoadingStatusFlow.xml#MaintainDataLoadingStatusFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   MERGE INTO RMS_ASYNC_JOB USING
      ( SELECT 'PO_INDUCT' JOB_TYPE,
               'Processing of a PO induction upload/download.' JOB_DESCRIPTION,
               'PO_INDUCT_QTAB' QUEUE_TABLE,
               'PO_INDUCT_QUEUE' QUEUE_NAME,
               'PO_INDUCT_RECIPIENT' RECIPIENT_NAME,
               'RMS_ASYNC_MSG' QUEUE_PAYLOAD_TYPE,
               'RMS_ASYNC_PROCESS_SQL.NOTIFY_PO_INDUCT' DEQUEUE_EVENT,
               '/WEB-INF/oracle/retail/apps/rms/foundation/other/view/induction/status/flow/MaintainDataLoadingStatusFlow.xml#MaintainDataLoadingStatusFlow' TASKFLOW_URL,
               'Y' NOTIFICATION_LAUNCH_IND
          FROM DUAL) USE_THIS
   ON (RMS_ASYNC_JOB.JOB_TYPE = USE_THIS.JOB_TYPE)
   WHEN MATCHED THEN
   UPDATE
      SET JOB_DESCRIPTION = USE_THIS.JOB_DESCRIPTION,
          QUEUE_TABLE = USE_THIS.QUEUE_TABLE,
          QUEUE_NAME = USE_THIS.QUEUE_NAME,
          RECIPIENT_NAME = USE_THIS.RECIPIENT_NAME,
          QUEUE_PAYLOAD_TYPE = USE_THIS.QUEUE_PAYLOAD_TYPE,
          DEQUEUE_EVENT = USE_THIS.DEQUEUE_EVENT,
          TASKFLOW_URL = USE_THIS.TASKFLOW_URL,
          NOTIFICATION_LAUNCH_IND = USE_THIS.NOTIFICATION_LAUNCH_IND
   WHEN NOT MATCHED THEN
   INSERT (JOB_TYPE, JOB_DESCRIPTION, QUEUE_TABLE, QUEUE_NAME, RECIPIENT_NAME, QUEUE_PAYLOAD_TYPE, DEQUEUE_EVENT, TASKFLOW_URL, NOTIFICATION_LAUNCH_IND)
   VALUES (USE_THIS.JOB_TYPE, USE_THIS.JOB_DESCRIPTION, USE_THIS.QUEUE_TABLE, USE_THIS.QUEUE_NAME, USE_THIS.RECIPIENT_NAME,
           USE_THIS.QUEUE_PAYLOAD_TYPE, USE_THIS.DEQUEUE_EVENT, USE_THIS.TASKFLOW_URL, USE_THIS.NOTIFICATION_LAUNCH_IND);
   -----------------------------------------------------------------------------------
   COMMIT;

   select count(*)
     into L_count_new
     from rms_async_job;

   /*If the count in RMS_ASYNC_JOB changed, it means a new queue has been added,
     therefore this portion of the script will run to create the queues.*/
   if nvl(L_count_new, 0) > nvl(L_count_old, 0) then
      DBMS_OUTPUT.PUT_LINE('Start of queue creation...');

      open C_JOB_QUEUE;

      Loop

         fetch C_JOB_QUEUE into L_queue_table,
                                L_queue_name,
                                L_recipient,
                                L_queue_payload_type,
                                L_event,
                                L_job_type;

         if C_JOB_QUEUE%NOTFOUND then
            EXIT;
         end if;

         DBMS_OUTPUT.PUT_LINE('--------Creating Queue: '||L_queue_name||'---------------');

         ----------------------------------------------------------------------------------
         -- DROP EXISTING QUEUE SUBSCRIBERS
         ----------------------------------------------------------------------------------
         open C_SUBSCRIBER_EXISTS;
         fetch C_SUBSCRIBER_EXISTS into L_subscriber_count;
         close C_SUBSCRIBER_EXISTS;

         if L_subscriber_count > 0 then
            DBMS_AQADM.REMOVE_SUBSCRIBER(QUEUE_NAME => L_user||'.'||L_queue_name,
                                         SUBSCRIBER => sys.aq$_agent(L_recipient,NULL,NULL));
         end if;

         ----------------------------------------------------------------------------------
         -- STOP AND DROP EXISTING QUEUES
         ----------------------------------------------------------------------------------
         open C_QUEUE_EXISTS;
         fetch C_QUEUE_EXISTS into L_queue_count;
         close C_QUEUE_EXISTS;

         if L_queue_count > 0 then
            DBMS_AQADM.STOP_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);

            DBMS_AQADM.DROP_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);
         end if;

         ----------------------------------------------------------------------------------
         -- DROP EXISTING QUEUE TABLE
         ----------------------------------------------------------------------------------
         open C_QUEUE_TABLE_EXISTS;
         fetch C_QUEUE_TABLE_EXISTS into L_queue_table_count;
         close C_QUEUE_TABLE_EXISTS;

         if L_queue_table_count > 0 then
            DBMS_AQADM.DROP_QUEUE_TABLE(QUEUE_TABLE => L_user||'.'||L_queue_table);
         end if;

         DBMS_OUTPUT.PUT_LINE('Creating queue table: '||L_queue_table);

         ----------------------------------------------------------------------------------
         -- CREATE QUEUE TABLE
         ----------------------------------------------------------------------------------
         DBMS_AQADM.CREATE_QUEUE_TABLE(QUEUE_TABLE        => L_user||'.'||L_queue_table,
                                       QUEUE_PAYLOAD_TYPE => L_user||'.'||L_queue_payload_type,
                                       MULTIPLE_CONSUMERS => TRUE);

         ----------------------------------------------------------------------------------
         -- CREATE AND START QUEUES
         ----------------------------------------------------------------------------------
         DBMS_AQADM.CREATE_QUEUE(QUEUE_NAME  => L_user||'.'||L_queue_name,
                                 QUEUE_TABLE => L_user||'.'||L_queue_table);

         DBMS_AQADM.START_QUEUE(QUEUE_NAME => L_user||'.'||L_queue_name);

         ----------------------------------------------------------------------------------
         -- CREATE AND REGISTER SUBSCRIBERS TO PL/SQL NOTIFY PACKAGES
         ----------------------------------------------------------------------------------
         DBMS_AQADM.ADD_SUBSCRIBER
            (QUEUE_NAME => L_user||'.'||L_queue_name,
             SUBSCRIBER => sys.aq$_agent(L_recipient,NULL,NULL));

         --Only register events that will have a specific PL/SQL callback procedure.
         --Otherwise, do NOT register it. This is to handle things like notification queue,
         --which will be registered with rtkstrt on form runtime using the Events object property.
         if L_register_event_ind = 'Y' then
            DBMS_AQ.REGISTER
               (sys.aq$_reg_info_list(
                  sys.aq$_reg_info(L_user||'.'||L_queue_name||':'||L_recipient,
                                   DBMS_AQ.NAMESPACE_AQ,
                                   'plsql://'||L_user||'.'||L_event,
                                   HEXTORAW('FF'))),
                1);
         end if;

         DBMS_OUTPUT.PUT_LINE('Successfully created: '||L_queue_name);
         DBMS_OUTPUT.PUT_LINE('   ');

      End Loop;

      close C_JOB_QUEUE;

      DBMS_OUTPUT.PUT_LINE('End of queue creation...');
   end if;

EXCEPTION
   when OTHERS then
      DBMS_OUTPUT.PUT_LINE('exception: '||SQLCODE||', '||SQLERRM);
END;
/
