SET DEFINE OFF;
---------------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM CODE_DETAIL_TL
      WHERE CODE_TYPE = 'CCOR'
        AND CODE      = 'ITEM';
---------------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM CODE_DETAIL
      WHERE CODE_TYPE = 'CCOR'
        AND CODE      = 'ITEM';
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL ERR USING ( SELECT
'CCOR' CODE_TYPE, 'SKU' CODE, 'Y' REQUIRED_IND, '1' CODE_SEQ,
'By Item' CODE_DESC
FROM DUAL) USE_THIS
ON (ERR.CODE_TYPE = USE_THIS.CODE_TYPE AND ERR.CODE = USE_THIS.CODE)
WHEN MATCHED THEN UPDATE
SET ERR.CODE_DESC = USE_THIS.CODE_DESC, ERR.CODE_SEQ = USE_THIS.CODE_SEQ
WHEN NOT MATCHED THEN INSERT (CODE_TYPE, CODE, REQUIRED_IND, CODE_SEQ, CODE_DESC)
VALUES (USE_THIS.CODE_TYPE, USE_THIS.CODE, USE_THIS.REQUIRED_IND, USE_THIS.CODE_SEQ, USE_THIS.CODE_DESC);
---------------------------------------------------------------------------------------------------------------------------------------------
-- GERMAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 2 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Nach Artikel' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- FRENCH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 3 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Par article' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- SPANISH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 4 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Por artículo' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- JAPANESE
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 5 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'アイテム別' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- KOREAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 6 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, '품목 기준' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- RUSSIAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 7 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'По товару' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- CHINESE-SIMPLIFIED
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 8 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, '按商品' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- TURKISH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 9 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Ürüne Göre' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- HUNGARIAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 10 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Cikk szerint' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- CHINESE-TRADITIONAL
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 11 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, '依據料號' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- PORTUGUESE-BRAZIL
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 12 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Por Item' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- CROATIAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 15 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Po artiklu' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- DUTCH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 18 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Op artikel' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- GREEK
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 20 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Ανά είδος' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- ITALIAN
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 22 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Per articolo' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- POLISH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 26 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Wg pozycji' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
-- SWEDISH
---------------------------------------------------------------------------------------------------------------------------------------------
MERGE INTO CODE_DETAIL_TL TL USING
(SELECT  LANG,  CODE_TYPE,  CODE,  CODE_DESC
FROM  (SELECT 31 LANG, 'CCOR' CODE_TYPE, 'SKU' CODE, 'Per artikel' CODE_DESC FROM DUAL) DL
WHERE EXISTS  (SELECT 1 FROM CODE_DETAIL base where dl.CODE_TYPE = base.CODE_TYPE and dl.CODE = base.CODE)) USE_THIS
ON ( tl.LANG = use_this.LANG and tl.CODE_TYPE = use_this.CODE_TYPE and tl.CODE = use_this.CODE)
WHEN MATCHED THEN UPDATE  SET tl.CODE_DESC = use_this.CODE_DESC, tl.LAST_UPDATE_DATETIME = sysdate, tl.LAST_UPDATE_ID = user
WHEN NOT MATCHED THEN INSERT (LANG, CODE_TYPE, CODE, CODE_DESC, CREATE_DATETIME, CREATE_ID, LAST_UPDATE_DATETIME, LAST_UPDATE_ID)
VALUES (use_this.LANG, use_this.CODE_TYPE, use_this.CODE, use_this.CODE_DESC, sysdate, user, sysdate, user);
---------------------------------------------------------------------------------------------------------------------------------------------
COMMIT;
