
--------------------------------------------------------
-- Copyright (c) 2012, Oracle Corp.  All rights reserved.
-- :   uom_x_conversion.sql  $
-- :   1.0  $
-- :   Wed Apr 25 11:22:10 CDT 2012  $
--------------------------------------------------------
SET FEEDBACK OFF
SET ECHO OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK
SET DEFINE ^
REM -------------------------------------------------
REM
REM  Refreshes uom_x_conversion table.
REM
REM -------------------------------------------------
REM -------------------------------------------------
REM ****************************************************************
PROMPT updating/inserting into uom_x_conversion
REM ****************************************************************
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (1 / isd.width)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	  = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / isd.length * isd.width)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select (isd.weight / (isd.length * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    =isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select ((isu.value * isc.supp_pack_size) / (isd.length * isd.width))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.item 	     = isc.item
   and isd.dim_object    = ''CA''
   and isc.supplier	     = :s
   and isu.supplier	     = isc.supplier
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.uom	      = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / (isd.length * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select (isc.supp_pack_size / (isd.length * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'AREA' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select isd.height
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	      = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select isd.width
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	      = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / isd.length)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    =
isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select (isd.weight / isd.length)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select ((isu.value * isc.supp_pack_size) / isd.length)
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.supplier	  = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / isd.length)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	  = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select (isc.supp_pack_size / isd.length)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'DIMEN' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select (isd.height * isd.width)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select (isd.length * isd.width / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (isd.length / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
	or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select (isd.weight / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select ((isu.value * isc.supp_pack_size) / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.supplier	     = isc.supplier
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isd.origin_country    = isc.origin_country_id
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and
isd.origin_country    = isc.origin_country_id
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select (isc.supp_pack_size / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isd.origin_country    = isc.origin_country_id
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'LVOL' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select (isd.length * isd.width * isd.height / isd.liquid_volume)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and
isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select ((isd.length * isd.width) / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (isd.length / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select ((isu.value * isc.supp_pack_size) / isd.weight)
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.supplier	  = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	  = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select (isc.supp_pack_size / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MASS' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select ((isd.length * isd.height * isd.width) / isd.weight)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isc.supplier      = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select ((isd.length * isd.width) / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.item 	     = isc.item
   and isd.dim_object    = ''CA''
   and isc.supplier	     = :s
   and isu.supplier	     = isc.supplier
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (isd.length / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     = isc.item
   and isd.supplier	  = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     =
isc.item
   and isd.supplier	     = isc.supplier
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isd.origin_country    = isc.origin_country_id
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select (isd.weight / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item 	     = isc.item
and isd.supplier	  = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu
 where isc.item     = :i
   and isu.item     = isc.item
   and isc.supplier = :s
   and isu.supplier = isc.supplier
   and isu.uom	    = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
    or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select isu.value
  from item_supp_country isc,
       item_supp_uom isu
 where isc.item     = :i
   and isu.item     = isc.item
   and isc.supplier = :s
   and isu.supplier = isc.supplier
   and isu.uom	    = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'MISC' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select ((isd.length * isd.height * isd.width) / (isu.value * isc.supp_pack_size))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isu.item = isc.item
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select (isd.length * isd.width)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select isd.length
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	      = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select isd.liquid_volume
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	  = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select isd.weight
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	      = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select (isu.value * isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_uom isu
 where isc.item     = :i
   and isu.item     = isc.item
   and isc.supplier = :s
   and isu.supplier = isc.supplier
   and isu.uom	    = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select supp_pack_size
  from item_supp_country
 where item	= :i
   and supplier = :s
   and :u      is NULL
   and ((:o    is NULL
	 and primary_country_ind = ''Y'')
       or (origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'PACK' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select (isd.length * isd.height * isd.width)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select ((isd.length * isd.width) / isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (isd.length / isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
	or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'GMAS' TO_UOM_CLASS,
'select (isd.weight / isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item = isc.item
   and isd.dim_object = ''CA''
   and isc.item = :i
   and isd.supplier = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier = :s
   and :u is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isd.origin_country    = isc.origin_country_id
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
		 'select (isd.weight / isc.supp_pack_size)'||chr(10)||
                 '  from item_supp_country isc,'||chr(10)||
                 '       item_supp_country_dim isd'||chr(10)||
                 ' where isd.item              = isc.item'||chr(10)||
                 '   and isd.dim_object        = ''CA'''||chr(10)||
                 '   and isc.item              = :i'||chr(10)||
                 '   and isd.supplier          = isc.supplier'||chr(10)||
                 '   and isd.origin_country    = isc.origin_country_id'||chr(10)||
                 '   and isc.supplier          = :s'||chr(10)||
                 '   and :u   is NULL'||chr(10)||
                 '   and ((:o is NULL'||chr(10)||
                 '         and isc.primary_country_ind = ''Y'')'||chr(10)||
                 '       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select isu.value
  from item_supp_country isc,
       item_supp_uom isu
 where isc.item     = :i
   and isu.item     = isc.item
   and isc.supplier = :s
   and isu.supplier = isc.supplier
   and isu.uom	    = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or
(isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / supp_pack_size)
  from item_supp_country
 where item	= :i
   and supplier = :s
   and :u      is NULL
   and ((:o    is NULL
	 and primary_country_ind = ''Y'')
       or (origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'QTY' FROM_UOM_CLASS, 'VOL' TO_UOM_CLASS,
'select ((isd.length * isd.height * isd.width) / isc.supp_pack_size)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and
isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'AREA' TO_UOM_CLASS,
'select (1 / isd.height)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
and isc.supplier	  = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'DIMEN' TO_UOM_CLASS,
'select (1 / (isd.height * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'LVOL' TO_UOM_CLASS,
'select (isd.liquid_volume / isd.length * isd.width * isd.height)
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and
isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'MASS' TO_UOM_CLASS,
'select (isd.weight / (isd.length * isd.height * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and
isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'MISC' TO_UOM_CLASS,
'select ((isu.value * isc.supp_pack_size) / (isd.length * isd.height * isd.width))
  from item_supp_country isc,
       item_supp_uom isu,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isu.item 	     = isc.item
   and isc.item 	     = :i
   and isd.dim_object	      = ''CA''
   and isd.supplier	     = isc.supplier
   and isd.origin_country    = isc.origin_country_id
   and isu.supplier	     = isc.supplier
   and isc.supplier	     = :s
   and isu.uom		     = :u
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'PACK' TO_UOM_CLASS,
'select (1 / (isd.length * isd.height * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and isd.origin_country = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
MERGE INTO UOM_X_CONVERSION ERR USING ( SELECT
'VOL' FROM_UOM_CLASS, 'QTY' TO_UOM_CLASS,
'select (isc.supp_pack_size / (isd.length * isd.height * isd.width))
  from item_supp_country isc,
       item_supp_country_dim isd
 where isd.item 	     = isc.item
   and isd.dim_object	     = ''CA''
   and isc.item 	     = :i
   and isd.supplier	     = isc.supplier
   and
isd.origin_country    = isc.origin_country_id
   and isc.supplier	     = :s
   and :u   is NULL
   and ((:o is NULL
	 and isc.primary_country_ind = ''Y'')
       or (isc.origin_country_id = :o))' CONVERT_SQL
FROM DUAL) USE_THIS
ON (ERR.FROM_UOM_CLASS = USE_THIS.FROM_UOM_CLASS and ERR.TO_UOM_CLASS = USE_THIS.TO_UOM_CLASS)
WHEN MATCHED THEN UPDATE
SET ERR.CONVERT_SQL = USE_THIS.CONVERT_SQL
WHEN NOT MATCHED THEN INSERT (FROM_UOM_CLASS,TO_UOM_CLASS,CONVERT_SQL)
VALUES (USE_THIS.FROM_UOM_CLASS,USE_THIS.TO_UOM_CLASS,USE_THIS.CONVERT_SQL);
------------------------------------------------------------------------
SET DEFINE &
commit;
